package study3

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

class SummarizeResult {

    static void main(String[] args){
        def folder = "D:\\Downloads\\TestInterfaceEvaluationWithDeps-master\\results"
        def files = Util.findFilesFromDirectory(folder)
        def dependencyResults = files.findAll{
            it.contains("${File.separator}dependency_based${File.separator}") && it.endsWith("-relevant.csv")
        }
        println "dependencyResults: ${dependencyResults.size()}"
        dependencyResults.each{ println it}

        def originalResults = files.findAll{
            it.contains("${File.separator}original${File.separator}") && it.endsWith("-relevant.csv")
        }
        println "originalResults: ${originalResults.size()}"
        originalResults.each{ println it}

        dependencyResults.each{ r ->
            def index = r.lastIndexOf(File.separator)
            def newName = "output" + File.separator + "study3" + File.separator + r.substring(index+1) - "-relevant.csv" + "-2.csv"
            List<String[]> lines = CsvUtil.read(r)
            def project = lines[0][1]
            lines = lines.subList(13, lines.size())
            String[] header = ["Project", "Task", "TestI", "TaskI", "Precision", "Recall", "F2"]
            List<String[]> content = []
            content += header
            lines.each{
                content += [project, it[0], it[7], it[8], it[11], it[12], it[32]] as String[]
            }
            CsvUtil.write(newName, content)
            println "file: ${r}; tasks: ${lines.size()}"
        }

        originalResults.each{ r ->
            def index = r.lastIndexOf(File.separator)
            def newName = "output" + File.separator + "study3" + File.separator + r.substring(index+1) - "-relevant.csv" + "-1.csv"
            List<String[]> lines = CsvUtil.read(r)
            def project = lines[0][1]
            lines = lines.subList(13, lines.size())
            String[] header = ["Project", "Task", "TestI", "TaskI", "Precision", "Recall", "F2"]
            List<String[]> content = []
            content += header
            lines.each{
                content += [project, it[0], it[7], it[8], it[11], it[12], it[32]] as String[]
            }
            CsvUtil.write(newName, content)
            println "file: ${r}; tasks: ${lines.size()}"
        }

    }



}
