package study3

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

class OrganizeDependencyResult {

    static void main(String[] args){
        def folder = "output\\study3"
        def files = Util.findFilesFromDirectory(folder)
        def dependencyFiles = files.findAll{ it.endsWith("-2.csv") }

        List<String[]> buffer = []
        dependencyFiles.each{ file ->
            List<String[]> lines = CsvUtil.read(file)
            if(buffer.empty) buffer += lines[0]
            lines = lines.subList(1, lines.size())
            buffer += lines.sort{ it[1] as double }
        }
        println "buffer: ${buffer.size()-1}"


        CsvUtil.write("${folder}${File.separator}dependency_result.csv", buffer)
    }

}
