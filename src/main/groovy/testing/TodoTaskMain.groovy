package testing

import br.ufpe.cin.tan.analysis.itask.ITest
import br.ufpe.cin.tan.analysis.task.TodoTask
import br.ufpe.cin.tan.similarity.test.TestSimilarityAnalyser

class TodoTaskMain {

    static void main(String[] args){
        def repository = "https://github.com/rapidftr/RapidFTR.git"

        /* Task 1 */
        def id1 = 1
        def tests1 = [ [path:"features/enquiry_file_export.feature", lines:[29]],
                      [path:"features/file_export.feature", lines:[63]]
        ]
        TodoTask todoTask1 = new TodoTask(repository, id1, tests1)
        ITest itest1 = todoTask1.computeTestBasedInterface()
        println "ITest(T1):"
        itest1.files.each{ file ->
            println file
        }

        /* Task 2 */
        def id2 = 32
        def tests2 = [[path:"features/enquiry_filtering.feature", lines:[24, 30, 34, 39]],
                      [path:"features/duplicate_child_record.feature", lines:[31, 49]],
                      [path:"features/form_section_operation.feature", lines:[230]],
                      [path:"features/enquiry_potential_matches.feature", lines:[165]],
                      [path:"features/child_potential_matches.feature", lines:[45, 62]]
        ]
        TodoTask todoTask2 = new TodoTask(repository, id2, tests2)
        ITest itest2 = todoTask2.computeTestBasedInterface()
        println "ITest(T2):"
        itest2.files.each{ file ->
            println file
        }

        /* Similarity */
        def similarityAnalyser = new TestSimilarityAnalyser(itest1, itest2)
        double cosine = similarityAnalyser.calculateSimilarityByCosine() //value in [0,1]
        println "Cosine similarity between T1 and T2: $cosine"
        double jaccard = similarityAnalyser.calculateSimilarityByJaccard() //value in [0,1]
        println "Jaccard similarity between T1 and T2: $jaccard"
    }

}
