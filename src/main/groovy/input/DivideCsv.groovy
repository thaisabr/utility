package input

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

/*
* Divide um CSV de entrada muito extenso em múltiplos arquivos menores (até 100 linhas).
* */

class DivideCsv {

    static void main(String[] args){
        //divideByFolders()
        divide()
    }

    static divideByFolders(){
        def folders = Util.findFoldersFromDirectory("tasks\\temp\\divide")
        folders.each{ folder ->
            def files = Util.findFilesFromDirectory(folder)
            files.each{ file ->
                println "Dividing file: $file"
                organize(file)
            }
        }
    }

    static divide(){
        def folder = "D:\\Dropbox\\Thaís\\phd_study2_final_result\\2_independent_tasks_at_least_organized_for_compute_testi"
        def files = Util.findFilesFromDirectory(folder)
        files.each{ file ->
            println "Dividing file: $file"
            organize(file)
        }
    }

    private static organize(String filename){
        def size = 50
        List<String[]> entries = CsvUtil.read(filename)
        entries.remove(0)
        int entrySize = entries.size()
        println "all entries: ${entrySize}"
        int groups = entrySize.intdiv(size)
        println "groups: ${groups}"
        def remainder = entrySize%size
        println "remainder: ${remainder}"

        if(groups==0 || (groups==1 && remainder==0)) return

        def i = 0, j = size, analysedGroups = 0, counter = 0
        while(analysedGroups<groups) {
            counter++
            def values = entries.subList(i, j)
            generateCsv(filename-".csv"+"_${counter}.csv", values)
            i=j
            j+=size
            analysedGroups++
        }

        if(remainder>0){
            def k = i+remainder
            def values = entries.subList(i, k)
            generateCsv(filename-".csv"+"_${++counter}.csv", values)
        }
        new File(filename).delete()
    }

    private static generateCsv(String filename, List<String[]> entries){
        List<String[]> content = []
        String[] text = ["REPO_URL","TASK_ID","#HASHES","HASHES","#PROD_FILES","#TEST_FILES","LAST"]
        content += text
        content += entries
        CsvUtil.write(filename, content)
    }

}
