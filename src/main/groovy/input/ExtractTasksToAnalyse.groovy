package input

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

/*
* Gera um novo CSV de entrada de tarefas considerando apenas as tarefas previamente analisadas, de interesse.
*
* */

class ExtractTasksToAnalyse {

    static void main(String[] args){
        newSample()

        //PARTE 1
        /*String relevantTasksFile = "tasks${File.separator}temp${File.separator}alphagov_whitehall_cucumber_coverage_candidates_11-relevant.csv"
        String inputTasksFile = "tasks${File.separator}temp${File.separator}alphagov_whitehall_cucumber_coverage_candidates_11_original.csv"
        String inputSelectedTasksFile = "tasks${File.separator}temp${File.separator}alphagov_whitehall_cucumber_coverage_candidates_11-selected.csv"

        List<String[]> entries = CsvUtil.read(relevantTasksFile)
        def content = entries.subList(13,entries.size())
        def ids = content.collect{ it[0] }.sort()
        println "extracted ids:"
        ids.each{ println it }

        List<String[]> tasks = CsvUtil.read(inputTasksFile)
        def tasksOfInterest = tasks.findAll{ it[1] in ids }.sort{ it[1] }
        List<String[]> lines = []
        lines += tasks.get(0)
        lines += tasksOfInterest//.subList(0,10)

        CsvUtil.write(inputSelectedTasksFile, lines)*/

        //PARTE 2
        /*String relevantTasksFile = "tasks${File.separator}temp${File.separator}result.csv"
        String inputTasksFile = "tasks${File.separator}temp${File.separator}tasks-selfContained.csv"
        String inputSelectedTasksFile = "tasks${File.separator}temp${File.separator}selected.csv"

        List<String[]> entries = CsvUtil.read(relevantTasksFile)
        def content = entries.subList(1,entries.size())
        def ids = content.collect{ [project: it[0], id: it[1]] }.sort()
        println "extracted ids:"
        ids.each{ println it }

        List<String[]> tasks = CsvUtil.read(inputTasksFile)
        def tasksOfInterest = tasks.findAll{ t ->
            def aux = ids.find{ t[0].toLowerCase().contains(it.project) && it.id==t[1] }
            if(aux) t
        }.sort{ it[0] }
        List<String[]> lines = []
        lines += tasks.get(0)
        lines += tasksOfInterest//.subList(0,10)

        CsvUtil.write(inputSelectedTasksFile, lines)*/
    }

    /* Método para extrair tarefas na nova amostra, em que os resultados estão espalhados em múltiplas pastas. */
    static newSample(){
        def folder = "tasks\\temp\\cucumber_sample"
        def files = Util.findFilesFromDirectory(folder)

        def taskFiles = files.findAll{
            it.contains("${File.separator}tasks${File.separator}") && it.endsWith("_candidates.csv") &&
                    !it.contains("${File.separator}result${File.separator}") &&
                    !it.contains("${File.separator}candidates${File.separator}") &&
                    !it.contains("${File.separator}original${File.separator}")
        }
        println "Tasks files: ${taskFiles.size()}"

        def entryTasks = []
        def header = null
        taskFiles.each{ file ->
            def lines = CsvUtil.read(file)
            if(header==null) header = lines.get(0)
            lines.remove(0)
            entryTasks += lines
        }
        println "entryTasks: ${entryTasks.size()}"
        entryTasks.each{ println "${it[0]}, ${it[1]}" }

        def selectedTasks = []
        def lines = CsvUtil.read("tasks\\temp\\cucumber_sample_task_interfaces\\output\\itest_all.csv")
        lines = lines.subList(1, lines.size()-15)
        selectedTasks += lines.collect{
            def project = it[0]
            def index = project.lastIndexOf("_")
            [project.substring(index+1), it[1]]
        }
        println "selectedTasks: ${selectedTasks.size()}"
        selectedTasks.each{ println it }

        def matches = []
        selectedTasks.each{ task ->
            def temp = entryTasks.find{
                def url = it[0].toLowerCase()
                url.endsWith(task[0]+".git") && it[1]==task[1]
            }
            if(temp) matches += temp
        }
        println "matches: ${matches.size()}"

        List<String[]> content = []
        content += header
        content += matches
        CsvUtil.write("tasks\\temp\\tasksPerSample\\463\\484_tasks.csv", content)
    }

}
