package input

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path

/* Gera um único CSV de tarefas a partir de um conjunto de CSVs. */
class JoinCsv {

    boolean advanced
    List<String[]> buffer
    String outputFile
    List<String> files

    String mainFolder
    String relevantFolder
    String relevantControllerFolder
    String candidatesFolder
    String invalidFolder
    String finalResultFolder
    String resultFolder
    String detailedFolder

    List<String[]> header

    static void main(String[] args){
        //def obj = new JoinCsv("D:\\Dropbox\\Thaís\\phd_study2_fixed_result\\testi", true)
        def obj = new JoinCsv("C:\\Users\\tatab\\Desktop\\testi_given_exclude_nov20\\result", true)
        obj.organize()
    }

    JoinCsv(String folder, boolean advanced){
        this.mainFolder = folder
        buffer = []
        files = Util.findFilesFromDirectory(folder)
        if(advanced) configureFolders(folder)
    }

    JoinCsv(String folder, String sufix){
        this.mainFolder = folder
        buffer = []
        files = Util.findFilesFromDirectory(folder)
        def auxName = files.get(0)
        def index = auxName.lastIndexOf("_")
        def name = ""
        if(auxName.contains("_candidates")){
            def temp = auxName.find(/_\d+_candidates.csv/)
            name = auxName - temp
        } else {
            name = auxName.substring(0,index)
        }
        if (name.endsWith("_")) name = name.substring(0, name.size()-1)
        name = name + sufix
        this.outputFile = name
        println "outputFile: $outputFile"
    }

    def organize(){
        joinAllInFolderByProject()
        organizeFolders()
        collapsePerProject()
        organizeFinalFolder()
        showFinalResult()
    }

    private showFinalResult(){
        def finalFiles = Util.findFilesFromDirectory(finalResultFolder)
        finalFiles.each{
            def headerSize = 13
            if(it.endsWith("-detailed.csv")) headerSize = 19
            List<String[]> lines = CsvUtil.read(it)
            println "final file: ${it}; entries: ${lines.size()-headerSize}"
        }
    }

    private joinAllInFolder(){
        updateBuffer()
        generateCsv()
    }

    private joinInvalidsInFolder(){
        updateInvalidBuffer()
        generateCsv()
    }

    private updateInvalidBuffer(){
        header = null
        files.each{ file ->
            updateInvalidBuffer(file)
        }
    }

    private updateInvalidBuffer(String filename){
        List<String[]> entries = CsvUtil.read(filename)
        if(header==null) header = entries.subList(0, 19)
        header.subList(1, 19).each{
            it[1] = ""
        }
        entries = entries.subList(19, entries.size())
        buffer += entries
    }

    private collapsePerProject(){
        println "collapsing files per project: "
        new File(candidatesFolder).eachDir() { dir ->
            println "folder: ${dir.getPath()}"
            def obj = new JoinCsv(dir.getPath(), "_candidates.csv")
            obj.joinAllInFolder()
        }

        new File(relevantFolder).eachDir() { dir ->
            println "folder: ${dir.getPath()}"
            def obj = new JoinCsv(dir.getPath(), "-relevant.csv")
            obj.joinAllInFolder()
        }

        new File(relevantControllerFolder).eachDir() { dir ->
            println "folder: ${dir.getPath()}"
            def obj = new JoinCsv(dir.getPath(), "-relevant-controller.csv")
            obj.joinAllInFolder()
        }

        new File(invalidFolder).eachDir() { dir ->
            println "folder: ${dir.getPath()}"
            def obj = new JoinCsv(dir.getPath(), "-invalid.csv")
            obj.joinInvalidsInFolder()
        }

        new File(detailedFolder).eachDir() { dir ->
            println "folder: ${dir.getPath()}"
            def obj = new JoinCsv(dir.getPath(), "-relevant-detailed.csv")
            obj.joinInvalidsInFolder()
        }
    }

    private organizeFilesByType(){
        def candidates = files.findAll{ it.endsWith("_candidates.csv") }
        candidates.each{ file ->
            Path source = FileSystems.getDefault().getPath(file)
            Path target = FileSystems.getDefault().getPath(candidatesFolder)
            Files.copy(source, target.resolve(source.getFileName()))
        }

        def relevants = files.findAll{ it.endsWith("-relevant.csv") }
        relevants.each{ file ->
            Path source = FileSystems.getDefault().getPath(file)
            Path target = FileSystems.getDefault().getPath(relevantFolder)
            Files.copy(source, target.resolve(source.getFileName()))
        }

        def controllerRelevants = files.findAll{ it.endsWith("-relevant-controller.csv") }
        controllerRelevants.each{ file ->
            Path source = FileSystems.getDefault().getPath(file)
            Path target = FileSystems.getDefault().getPath(relevantControllerFolder)
            Files.copy(source, target.resolve(source.getFileName()))
        }

        def invalid = files.findAll{ it.endsWith("-invalid.csv") }
        invalid.each{ file ->
            Path source = FileSystems.getDefault().getPath(file)
            Path target = FileSystems.getDefault().getPath(invalidFolder)
            Files.copy(source, target.resolve(source.getFileName()))
        }

        def detailed = files.findAll{ it.endsWith("-relevant-detailed.csv") }
        detailed.each{ file ->
            Path source = FileSystems.getDefault().getPath(file)
            Path target = FileSystems.getDefault().getPath(detailedFolder)
            Files.copy(source, target.resolve(source.getFileName()))
        }
    }

    private organizeFinalFolder(){
        def relevants = Util.findFilesFromDirectory(relevantFolder).findAll{
            !it.find(/_\d+-relevant.csv/)
        }
        relevants.each{ file ->
            Path source = FileSystems.getDefault().getPath(file)
            Path target = FileSystems.getDefault().getPath(finalResultFolder)
            Files.copy(source, target.resolve(source.getFileName()))
        }

        def relevantControllers = Util.findFilesFromDirectory(relevantControllerFolder).findAll{
            !it.find(/_\d+-relevant-controller.csv/)
        }
        relevantControllers.each{ file ->
            Path source = FileSystems.getDefault().getPath(file)
            Path target = FileSystems.getDefault().getPath(finalResultFolder)
            Files.copy(source, target.resolve(source.getFileName()))
        }

        validateRelevantControllerResult(relevantControllers)

        def detailedFiles = Util.findFilesFromDirectory(detailedFolder).findAll{
            !it.find(/_\d+-relevant-detailed.csv/)
        }
        detailedFiles.each{ file ->
            Path source = FileSystems.getDefault().getPath(file)
            Path target = FileSystems.getDefault().getPath(finalResultFolder)
            Files.copy(source, target.resolve(source.getFileName()))
        }
    }

    private validateRelevantControllerResult(List relevantControllers){
        relevantControllers.each{ file ->
            def lines = CsvUtil.read(file)
            def header = lines.subList(0, 13)
            lines = lines.subList(13, lines.size())
            def controllerTasks = lines.findAll {
                def itest = it[9].tokenize(',[]')*.trim()
                def ireal = it[10].tokenize(',[]')*.trim()
                if(hasControllers(itest) && hasControllers(ireal)) it
                else null
            }
            controllerTasks = controllerTasks.unique() - [null]
            def index = file.lastIndexOf(File.separator)
            def name = file.substring(index+1)
            def newFileName = finalResultFolder + File.separator + name - ".csv" + "-final.csv"
            generateControllersCsv(header, controllerTasks, newFileName)
        }
    }

    private static generateControllersCsv(List header, List tasks, String file){
        List<String[]> content = []
        content += header
        content += tasks.sort{
            if(it[0].isInteger()) it[0] as double
            else it[1] as double
        }
        CsvUtil.write(file, content)
        println "final file: ${file}; entries: ${content.size()-header.size()}"
    }

    private static hasControllers(List<String> list) {
        def controllers = list.findAll { Util.isControllerFile(it) }
        !controllers.empty
    }

    private organizeFolders(){
        organizeCandidates()
        organizeRelevants()
        //organizeRelevantControllers()
        organizeInvalids()
        organizeDetailed()
    }

    private organizeCandidates(){
        def candidates = Util.findFilesFromDirectory(candidatesFolder)
        def projects = candidates.collect{ candidate ->
            def sufix = candidate.find(/_\d+_candidates.csv/)
            if(sufix) candidate - sufix
            else ""
        }
        projects = projects.unique() - ""
        projects = projects.collect{
            def index = it.lastIndexOf(File.separator)
            it.substring(index+1)
        }
        projects.each{ project ->
            def folder = candidatesFolder + File.separator + project
            createFolder(folder)
            def filtered = candidates.findAll{ it.contains(project) }
            filtered.each{ file ->
                Path source = FileSystems.getDefault().getPath(file)
                Path target = FileSystems.getDefault().getPath(folder)
                Files.move(source, target.resolve(source.getFileName()))
            }
        }
    }

    private organizeRelevants(){
        def relevants = Util.findFilesFromDirectory(relevantFolder)
        def projects = relevants.collect{ relevant ->
            def sufix = relevant.find(/_\d+-relevant.csv/)
            if(sufix) relevant - sufix
            else ""
        }
        projects = projects.unique() - ""
        projects = projects.collect{
            def index = it.lastIndexOf(File.separator)
            it.substring(index+1)
        }
        projects.each{ project ->
            def folder = relevantFolder + File.separator + project
            createFolder(folder)
            def filtered = relevants.findAll{ it.contains(project) }
            filtered.each{ file ->
                Path source = FileSystems.getDefault().getPath(file)
                Path target = FileSystems.getDefault().getPath(folder)
                Files.move(source, target.resolve(source.getFileName()))
            }
        }
    }

    private organizeRelevantControllers(){
        def relevants = Util.findFilesFromDirectory(relevantControllerFolder)
        def projects = relevants.collect{ relevant ->
            def sufix = relevant.find(/_\d+-relevant-controller.csv/)
            if(sufix) relevant - sufix
            else ""
        }
        projects = projects.unique() - ""
        projects = projects.collect{
            def index = it.lastIndexOf(File.separator)
            it.substring(index+1)
        }
        projects.each{ project ->
            def folder = relevantControllerFolder + File.separator + project
            createFolder(folder)
            def filtered = relevants.findAll{ it.contains(project) }
            filtered.each{ file ->
                Path source = FileSystems.getDefault().getPath(file)
                Path target = FileSystems.getDefault().getPath(folder)
                Files.move(source, target.resolve(source.getFileName()))
            }
        }
    }

    private organizeInvalids(){
        def invalids = Util.findFilesFromDirectory(invalidFolder)
        def projects = invalids.collect{ invalid ->
            def sufix = invalid.find(/_\d+-invalid.csv/)
            if(sufix) invalid - sufix
            else ""
        }
        projects = projects.unique() - ""
        projects = projects.collect{
            def index = it.lastIndexOf(File.separator)
            it.substring(index+1)
        }
        projects.each{ project ->
            def folder = invalidFolder + File.separator + project
            createFolder(folder)
            def filtered = invalids.findAll{ it.contains(project) }
            filtered.each{ file ->
                Path source = FileSystems.getDefault().getPath(file)
                Path target = FileSystems.getDefault().getPath(folder)
                Files.move(source, target.resolve(source.getFileName()))
            }
        }
    }

    private organizeDetailed(){
        def relevants = Util.findFilesFromDirectory(detailedFolder)
        def projects = relevants.collect{ relevant ->
            def sufix = relevant.find(/_\d+-relevant-detailed.csv/)
            if(sufix) relevant - sufix
            else ""
        }
        projects = projects.unique() - ""
        projects = projects.collect{
            def index = it.lastIndexOf(File.separator)
            it.substring(index+1)
        }
        projects.each{ project ->
            def folder = detailedFolder + File.separator + project
            createFolder(folder)
            def filtered = relevants.findAll{ it.contains(project) }
            filtered.each{ file ->
                Path source = FileSystems.getDefault().getPath(file)
                Path target = FileSystems.getDefault().getPath(folder)
                Files.move(source, target.resolve(source.getFileName()))
            }
        }
    }

    private joinAllInFolderByProject(){
        organizeFilesByType()
        def candidatesFiles = Util.findFilesFromDirectory(candidatesFolder)
        println "candidatesFiles: ${candidatesFiles.size()}"
        candidatesFiles.each{ println it }

        def relevantFiles = Util.findFilesFromDirectory(relevantFolder)
        println "relevantFiles: ${relevantFiles.size()}"
        relevantFiles.each{ println it }

        def relevantControllerFiles = Util.findFilesFromDirectory(relevantControllerFolder)
        println "relevantControllerFiles: ${relevantControllerFiles.size()}"
        relevantControllerFiles.each{ println it }

        def invalidFiles = Util.findFilesFromDirectory(invalidFolder)
        println "invalidFiles: ${invalidFiles.size()}"
        invalidFiles.each{ println it }

        def detailedFiles = Util.findFilesFromDirectory(detailedFolder)
        println "detailedFiles: ${detailedFiles.size()}"
        detailedFiles.each{ println it }
    }

    private configureFolders(String folder){
        resultFolder = "$folder${File.separator}org"
        relevantFolder = "$resultFolder${File.separator}relevant"
        relevantControllerFolder = "$resultFolder${File.separator}relevant-controller"
        candidatesFolder = "$resultFolder${File.separator}candidates"
        invalidFolder = "$resultFolder${File.separator}invalid"
        finalResultFolder = "$resultFolder${File.separator}final-result"
        detailedFolder = "$resultFolder${File.separator}relevant-detailed"
        createFolder(resultFolder)
        createFolder(relevantFolder)
        createFolder(relevantControllerFolder)
        createFolder(candidatesFolder)
        createFolder(invalidFolder)
        createFolder(finalResultFolder)
        createFolder(detailedFolder)
    }

    private static createFolder(String folder){
        File folderManager = new File(folder)
        if(folderManager.exists()) Util.deleteFolder(folder)
        else folderManager.mkdir()
    }

    private updateBuffer(){
        header = null
        files.each{ file ->
            println "Reading file: $file"
            updateBuffer(file)
        }
    }

    private updateBuffer(String filename){
        List<String[]> entries = CsvUtil.read(filename)
        if(entries[0][0] == "Repository" && entries.size()>13){//analysis result
            if(header==null) header = entries.subList(0, 13)
            header.subList(1, 12).each{
                it[1] = ""
            }
            entries = entries.subList(13, entries.size())
        } else {
            if(header==null) header = [entries.get(0)]
            entries.remove(0)
        }
        buffer += entries
    }

    private generateCsv(){
        List<String[]> content = []
        content += header
        content += buffer.sort{
            if(it[0].isInteger()) it[0] as double
            else it[1] as double
        }
        CsvUtil.write(outputFile, content)
    }

}
