package input

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

/*
 * Recebe um csv de tarefas derivadas a partir de merges e então conta quntas tarefas há e quantas são P&T.
 * Também extrai informações de interesse do log da execução da extração de tarefas, como o número total de commits de
 * merge de um projeto, a quantidade de merges selecionados e a quantidade de tarefas definidas.
 * */

class InputCounter {

    static void main (String[] args){
        def folder = "tasks\\merges"
        def files = Util.findFilesFromDirectory(folder).findAll{ it.endsWith(".csv") }
        files.each{ file ->
            println "couting: $file"
            List<String[]> entries = CsvUtil.read(file)
            entries.remove(0)
            def valid = entries.findAll{ it[0].startsWith("https://github.com/") }
            int entrySize = entries.size()
            def ptTasks = entries.findAll{ (it[4] as Integer)>0 && (it[5] as Integer)>0 }
            println "$file; all entries: ${entrySize}; pt entries: ${ptTasks.size()}; valid: ${valid.size()}"
        }

        def file = new File("$folder\\execution.log")
        def lines = file.readLines()
        println "println ${file}"
        def selected = lines.findAll{
            it.contains("Extracting merge commit from project ") ||
                    it.contains("All merge commits:") ||
                    it.contains("Selected merges:") ||
                    it.contains("Found merge tasks:")
        }
        selected.each{ println it }
        countTasks("tasks\\temp\\tasks-filtered")
        countTasks("tasks\\temp\\tasks-coverage-filtered")
    }

    static countTasks(String folder){
        def files = Util.findFilesFromDirectory(folder).findAll{ it.endsWith(".csv") }
        files.each{ file ->
            List<String[]> entries = CsvUtil.read(file)
            entries.remove(0)
            println "$file; tasks: ${entries.size()}"
        }
    }


}
