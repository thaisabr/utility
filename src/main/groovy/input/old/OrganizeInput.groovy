package input.old

import au.com.bytecode.opencsv.CSVWriter
import trying.others.ResultsComparator

/*
* Recebe CSV antigo de entrada, que listava os arquivos de produção e testes alterados
* e gera um novo CSV substituindo esses valores pela quantidade de arquivos.
* */

class OrganizeInput {

    static organize(String filename){
        List<String[]> entries = ResultsComparator.readInputCSV(filename)
        CSVWriter writer = new CSVWriter(new FileWriter(filename-".csv"+"-organized.csv"))
        String[] text = ["index","repository_url","task_id","commits_hash","changed_prod_files","changed_test_files"]
        writer.writeNext(text)
        entries.each{
            def prod_values = it[4].tokenize(',[]')*.trim().size()
            def text_values = it[5].tokenize(',[]')*.trim().size()
            writer.writeNext(it[0], it[1], it[2], it[3], prod_values as String, text_values as String)
        }
        writer.close()
    }

    static void main(String[] args){
        organize("tasks\\tasks_hrt.csv")
        organize("tasks\\tasks_meurio.csv")
        organize("tasks\\tasks_meurio_accounts.csv")
        organize("tasks\\tasks_rigse.csv")
        organize("tasks\\tasks_solar.csv")
        organize("tasks\\tasks_leihs.csv")
    }

}
