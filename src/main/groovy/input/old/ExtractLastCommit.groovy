package input.old

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

/* 
* Recebe um csv de tarefas e gera outro que contém a identificação do commit mais atual.
* */
class ExtractLastCommit {

    static void main(String[] args){
        def csvs = Util.findFilesFromDirectory("tasks${File.separator}last")
        csvs.each{ csv -> extractLastCommitFromTasks(csv)}
    }

    static extractLastCommitFromTasks(String csv){
        println "CSV: $csv"
        String inputTasksFile = csv
        String name = csv - ".csv"
        String lastCommitFile = "${name}-last.csv"

        List<String[]> entries = CsvUtil.read(inputTasksFile)
        def first = entries.first()
        String[] header = [first[0], first[1], first[2], first[3], first[4], first[5], "LAST"]
        def content = entries.subList(1,entries.size())
        List<String[]> lines = []
        lines += header
        content.each{ entry ->
            def hashes = entry[3].tokenize(',[]')*.trim()
            def last = hashes.first()
            String[] line = [entry[0], entry[1], entry[2], entry[3], entry[4], entry[5], last]
            lines += line
        }
        CsvUtil.write(lastCommitFile, lines)
        println "DONE!"
    }

}
