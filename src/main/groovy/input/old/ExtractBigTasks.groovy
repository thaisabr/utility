package input.old

import br.ufpe.cin.tan.util.CsvUtil

/*
* Recebe um csv de tarefas e gera outro apenas com tarefas grandes (mais de 500 commits).
* */

class ExtractBigTasks {

    static void main(String[] args){
        String inputTasksFile = "tasks${File.separator}temp${File.separator}localsupport_5_original.csv"
        String inputSelectedTasksFile = "tasks${File.separator}temp${File.separator}localsupport_5-selected.csv"

        List<String[]> entries = CsvUtil.read(inputTasksFile)
        def tasks = entries.subList(1, entries.size())
        def bigTasks = tasks.findAll{ (it[2] as int) > 500 }.sort{ it[1] }
        List<String[]> lines = []
        lines += entries.get(0)
        lines += bigTasks

        def ids = bigTasks.collect{ it[1] }.sort()
        println "Found big tasks: ${bigTasks.size()}"
        ids.each{ println it }

        CsvUtil.write(inputSelectedTasksFile, lines)
    }

}
