package input.old

import br.ufpe.cin.tan.util.ConstantData
import br.ufpe.cin.tan.util.CsvUtil

/*
* Filtra tarefas por similaridade de testes.
* */

class FilterTasksByTestSimilarity {

    static filter(String csv){
        List<String[]> entries = CsvUtil.read(csv)
        def content = entries.subList(2,entries.size()).sort{ it[0] as int }
        def ids = content.collect{ it[0] as int }
        println "extracted ids: ${ids.size()}"
        ids.each{ println it }

        def filtered = content.unique{ it[5] }
        List<String[]> lines = []
        lines += entries.subList(0,2)
        lines += filtered
        def name = csv - ConstantData.CSV_FILE_EXTENSION + "-filtered${ConstantData.CSV_FILE_EXTENSION}"
        CsvUtil.write(name, lines)
        name
    }

    static intersection(String csv1, String csv2){
        List<String[]> entries1 = CsvUtil.read(csv1)
        def content1 = entries1.subList(2,entries1.size()).sort{ it[0] as int }
        List<String[]> entries2 = CsvUtil.read(csv2)
        def content2 = entries2.subList(2,entries2.size()).sort{ it[0] as int }

        def ids1 = content1.collect{ it[0] as int }
        def ids2 = content2.collect{ it[0] as int }

        def common = ids1.intersect(ids2)
        println "common ids: ${common.size()}"
        common.each{ println it }
        def commonContent1 = content1.findAll{ (it[0] as int) in common }
        def commonContent2 = content2.findAll{ (it[0] as int) in common }

        List<String[]> lines = []
        lines += entries1.subList(0,2)
        lines += commonContent1
        def name1 = csv1 - ConstantData.CSV_FILE_EXTENSION + "-intersection${ConstantData.CSV_FILE_EXTENSION}"
        CsvUtil.write(name1, lines)

        lines = []
        lines += entries2.subList(0,2)
        lines += commonContent2
        def name2 = csv2 - ConstantData.CSV_FILE_EXTENSION + "-intersection${ConstantData.CSV_FILE_EXTENSION}"
        CsvUtil.write(name2, lines)
        [name1, name2]
    }

    static concatFiles(String... csvs){
        def name = ""
        def header = []
        def buffer = []
        csvs.each{ csv ->
            List<String[]> entries = CsvUtil.read(csv)
            if(header.empty) header = entries.subList(0,2)
            if(name.empty) name = csv - ConstantData.CSV_FILE_EXTENSION + "-concat.csv"
            buffer += entries.subList(2,entries.size()).sort{ it[0] as int }
        }
        def finalContent = buffer.unique{ it[0] as int }.unique{ it[5] }
        List<String[]> lines = []
        lines += header
        lines += finalContent
        CsvUtil.write(name, lines)
        name
    }

    static void main(String[] args){
        String testCsv1 = "tasks${File.separator}temp${File.separator}sequencescape_3-tests-added.csv"
        String testCsv2 = "tasks${File.separator}temp${File.separator}sequencescape_3-tests-all.csv"
        String testCsv3 = "tasks${File.separator}temp${File.separator}sequencescape_4-tests-added.csv"
        String testCsv4 = "tasks${File.separator}temp${File.separator}sequencescape_4-tests-all.csv"
        String testCsv5 = "tasks${File.separator}temp${File.separator}sequencescape_5-tests-added.csv"
        String testCsv6 = "tasks${File.separator}temp${File.separator}sequencescape_5-tests-all.csv"

        def concatAdded = concatFiles(testCsv1, testCsv3, testCsv5)
        def concatAll = concatFiles(testCsv2, testCsv4, testCsv6)

        def filteredConcatAdd = filter(concatAdded)
        def filteredConcatAll = filter(concatAll)

        def files = intersection(filteredConcatAdd, filteredConcatAll)
        println files
    }

}
