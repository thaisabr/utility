package study4

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

class EvaluatingFileChangePredictions463 {

    def testiTasks

    def importTasks(String file){
        testiTasks = []
        def lines = CsvUtil.read(file)
        lines.remove(0)

        if(lines.size()>0){
            lines.each{ line ->
                testiTasks += [url: convertProjectToUrl(line[0]), id: line[1], precision: line[2] as double, recall: line[3] as double,
                           f2: line[11] as double]
            }
        }

    }

    private static convertProjectToUrl(String project){
        def url = ""
        switch (project){
            case "8bitpal_hackful": url = "https://github.com/8bitpal/hackful"
                break
            case "agileventures_metplus_pets": url = "https://github.com/AgileVentures/MetPlus_PETS"
                break
            case "agileventures_websiteone": url = "https://github.com/AgileVentures/WebsiteOne"
                break
            case "alphagov_whitehall": url = "https://github.com/alphagov/whitehall"
                break
            case "cul-it_blacklight-cornell": url = "https://github.com/cul-it/blacklight-cornell"
                break
            case "diaspora_diaspora": url = "https://github.com/diaspora/diaspora"
                break
            case "efforg_action-center-platform": url = "https://github.com/EFForg/action-center-platform"
                break
            case "iboard_cba": url = "https://github.com/iboard/CBA"
                break
            case "jpatel531_folioapp": url = "https://github.com/jpatel531/folioapp"
                break
            case "oneclickorgs_one-click-orgs": url = "https://github.com/oneclickorgs/one-click-orgs"
                break
            case "opengovernment_opengovernment": url = "https://github.com/opengovernment/opengovernment"
                break
            case "otwcode_otwarchive": url = "https://github.com/otwcode/otwarchive"
                break
            case "qiushibaike_moumentei": url = "https://github.com/qiushibaike/moumentei"
                break
            case "rapidftr_rapidftr": url = "https://github.com/rapidftr/RapidFTR"
                break
            case "sameersharma25_time_stack": url = "https://github.com/sameersharma25/time_stack"
                break
            case "theodinproject_theodinproject": url = "https://github.com/TheOdinProject/theodinproject"
                break
            case "tip4commit_tip4commit": url = "https://github.com/tip4commit/tip4commit"
                break
            case "tracksapp_tracks": url = "https://github.com/TracksApp/tracks"
                break
        }
        url
    }

    def generateStatistics(){
        def precision = new DescriptiveStatistics(testiTasks.collect{it.precision} as double[])
        def recall = new DescriptiveStatistics(testiTasks.collect{it.recall} as double[])
        def f2 = new DescriptiveStatistics(testiTasks.collect{it.f2} as double[])
        println "Summary"

        println "PRECISION"
        println "Mean: ${precision.mean}"
        println "Median: ${precision.getPercentile(50.0)}"
        println "SD: ${precision.standardDeviation}"

        println "RECALL"
        println "Mean: ${recall.mean}"
        println "Median: ${recall.getPercentile(50.0)}"
        println "SD: ${recall.standardDeviation}"

        println "F2"
        println "Mean: ${f2.mean}"
        println "Median: ${f2.getPercentile(50.0)}"
        println "SD: ${f2.standardDeviation}"
    }

    def resultPerProject(){
        List<String[]> content = []
        def header = ["Project", "Precision_Mean", "Precision_Median", "Precision_SD",
                      "Recall_Mean", "Recall_Median", "Recall_SD",
                      "F2_Mean", "F2_Median", "F2_SD", "#Tasks"] as String[]
        content += header

        def urls = testiTasks.collect{it.url}.unique()

        urls.each{ url ->
            def tasks = testiTasks.findAll{ it.url == url }

            def precision = new DescriptiveStatistics(tasks.collect{it.precision} as double[])
            def recall = new DescriptiveStatistics(tasks.collect{it.recall} as double[])
            def f2 = new DescriptiveStatistics(tasks.collect{it.f2} as double[])
            println "Summary: ${url}; tasks: ${tasks.size()}"

            println "PRECISION"
            println "Mean: ${precision.mean}"
            println "Median: ${precision.getPercentile(50.0)}"
            println "SD: ${precision.standardDeviation}"

            println "RECALL"
            println "Mean: ${recall.mean}"
            println "Median: ${recall.getPercentile(50.0)}"
            println "SD: ${recall.standardDeviation}"

            println "F2"
            println "Mean: ${f2.mean}"
            println "Median: ${f2.getPercentile(50.0)}"
            println "SD: ${f2.standardDeviation}"

            content += [url, precision.mean, precision.getPercentile(50.0), precision.standardDeviation,
                        recall.mean, recall.getPercentile(50.0), recall.standardDeviation,
                        f2.mean, f2.getPercentile(50.0), f2.standardDeviation, tasks.size()] as String[]
        }

        def file = "D:\\Downloads\\evaluation_changePrediction_463.csv"
        CsvUtil.write(file, content)
    }

    def importTestiTasks(){
        def file = "D:\\Downloads\\tasks_data_463.csv"
        importTasks(file)
        println "Imported TestI tasks: ${testiTasks.size()}"
    }

    static void main(String[] args){
        EvaluatingFileChangePredictions463 obj = new EvaluatingFileChangePredictions463()
        obj.importTestiTasks()
        obj.generateStatistics()
        obj.resultPerProject()
    }

}
