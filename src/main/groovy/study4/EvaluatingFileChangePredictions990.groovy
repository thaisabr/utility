package study4

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

class EvaluatingFileChangePredictions990 {

    def selectedTasks
    def testiTasks

    def importTasks(String folder){
        def relevantTasks = Util.findFilesFromDirectory(folder).findAll{
            it.endsWith("-tasks-relevant.csv")
        }
        println "Relevant task files: ${relevantTasks.size()}"

        testiTasks = []
        relevantTasks.each{ file ->
            def lines = CsvUtil.read(file)
            if(lines.size()>0){
                def url = lines[0][1] - ".git"
                lines = lines.subList(13, lines.size())
                lines.each{ line ->
                    def temp = [url: url, id: line[0], precision: line[11] as double, recall: line[12] as double,
                               f2: line[32] as double]
                    def found = selectedTasks.find{
                        it.url==temp.url && it.id==temp.id
                    }
                    if(found!=null) testiTasks += temp
                }
            }
        }
    }

    def importSelectedTasks(){
        def file = "D:\\Onedrive\\Documentos\\phd_study2_final_result\\resultados_apendice\\tasks_data_990.csv"
        def lines = CsvUtil.read(file)
        lines.remove(0)
        selectedTasks = []
        if(lines.size()>0){
            lines.each{ line ->
                selectedTasks += [url: line[0], id: line[1]]
            }
        }
        println "Imported selected tasks: ${selectedTasks.size()}"
    }

    def generateStatistics(){
        def precision = new DescriptiveStatistics(testiTasks.collect{it.precision} as double[])
        def recall = new DescriptiveStatistics(testiTasks.collect{it.recall} as double[])
        def f2 = new DescriptiveStatistics(testiTasks.collect{it.f2} as double[])
        println "Summary"

        println "PRECISION"
        println "Mean: ${precision.mean}"
        println "Median: ${precision.getPercentile(50.0)}"
        println "SD: ${precision.standardDeviation}"

        println "RECALL"
        println "Mean: ${recall.mean}"
        println "Median: ${recall.getPercentile(50.0)}"
        println "SD: ${recall.standardDeviation}"

        println "F2"
        println "Mean: ${f2.mean}"
        println "Median: ${f2.getPercentile(50.0)}"
        println "SD: ${f2.standardDeviation}"
    }

    def resultPerProject(){
        List<String[]> content = []
        def header = ["Project", "Precision_Mean", "Precision_Median", "Precision_SD",
                      "Recall_Mean", "Recall_Median", "Recall_SD",
                      "F2_Mean", "F2_Median", "F2_SD", "#Tasks"] as String[]
        content += header

        def urls = testiTasks.collect{it.url}.unique()

        urls.each{ url ->
            def tasks = testiTasks.findAll{ it.url == url }

            def precision = new DescriptiveStatistics(tasks.collect{it.precision} as double[])
            def recall = new DescriptiveStatistics(tasks.collect{it.recall} as double[])
            def f2 = new DescriptiveStatistics(tasks.collect{it.f2} as double[])
            println "Summary: ${url}; tasks: ${tasks.size()}"

            println "PRECISION"
            println "Mean: ${precision.mean}"
            println "Median: ${precision.getPercentile(50.0)}"
            println "SD: ${precision.standardDeviation}"

            println "RECALL"
            println "Mean: ${recall.mean}"
            println "Median: ${recall.getPercentile(50.0)}"
            println "SD: ${recall.standardDeviation}"

            println "F2"
            println "Mean: ${f2.mean}"
            println "Median: ${f2.getPercentile(50.0)}"
            println "SD: ${f2.standardDeviation}"

            content += [url, precision.mean, precision.getPercentile(50.0), precision.standardDeviation,
                        recall.mean, recall.getPercentile(50.0), recall.standardDeviation,
                        f2.mean, f2.getPercentile(50.0), f2.standardDeviation, tasks.size()] as String[]
        }

        def file = "D:\\Onedrive\\Documentos\\phd_study2_final_result\\resultados_apendice\\evaluation_changePrediction_990.csv"
        CsvUtil.write(file, content)
    }

    def importTestiTasks(){
        def folder = "D:\\Onedrive\\Documentos\\phd_study2_final_result\\testi\\result_organized\\final-result"
        importTasks(folder)
        println "Imported TestI tasks: ${testiTasks.size()}"
    }

    static void main(String[] args){
        EvaluatingFileChangePredictions990 obj = new EvaluatingFileChangePredictions990()
        obj.importSelectedTasks()
        obj.importTestiTasks()
        obj.generateStatistics()
        obj.resultPerProject()
    }

}
