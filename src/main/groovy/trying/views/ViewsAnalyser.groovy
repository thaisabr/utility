package trying.views

import au.com.bytecode.opencsv.CSVReader
import au.com.bytecode.opencsv.CSVWriter
import br.ufpe.cin.tan.analysis.task.DoneTask
import br.ufpe.cin.tan.util.ConstantData
import br.ufpe.cin.tan.util.Util

/*
* Código para verificar os tipos de arquivos de views da amostra de projetos. (setembro 2016)
* */

class ViewsAnalyser {

    static List<String[]> readInputCSV(String filename) {
        CSVReader reader = new CSVReader(new FileReader(filename))
        List<String[]> entries = reader.readAll()
        reader.close()
        entries
    }

    static List<DoneTask> extractTaskList(String organizedCsv) {
        List<String[]> entries = readInputCSV(organizedCsv)
        def url = entries.first()[1]
        def tasks = entries.subList(16, entries.size())
        List<DoneTask> result = []
        tasks.each {
            def hashes = it[24].tokenize(',[]')*.trim()
            result += new DoneTask(url, it[0] as int, hashes, true)
        }
        result
    }

    static analyseProject(String csv) {
        int index = csv.lastIndexOf(File.separator)
        def filename = csv.substring(index + 1) - "organized.csv" + "views.csv"
        def writer = new CSVWriter(new FileWriter("csv_views_result" + File.separator + filename))
        String[] header = ["TASK", "VIEW FILES", "ERB", "HAML", "SLIM", "JAVASCRIPT", "JSON", "PLAIN HTML", "OTHERS"]
        writer.writeNext(header)

        def tasks = extractTaskList(csv)
        def others = []
        tasks.each { task ->
            task.gitRepository.reset(task.commits?.last()?.hash)
            def files = Util.findFilesFromDirectory(task?.gitRepository?.localPath + File.separator + Util.VIEWS_FILES_RELATIVE_PATH)
            task.gitRepository.reset()

            files = files.collect {
                index = it.lastIndexOf(File.separator)
                it.substring(index + 1)
            }

            def javascript = files.findAll { it.contains(".js.") || it.endsWith(".js") }
            def json = files.findAll { it.contains(".json.") || it.endsWith(".json") }
            def erbFiles = files.findAll { it.endsWith(".erb") }
            def hamlFiles = files.findAll { it.endsWith(".haml") }
            def slimFiles = files.findAll { it.endsWith(".slim") }
            def plainHtml = files.findAll { it.endsWith(".html") }

            String[] result = [task.id, files.size(), erbFiles.size(), hamlFiles.size(), slimFiles.size(),
                               javascript.size(), json.size(), plainHtml.size(), others.size()]

            others += files - javascript - json - erbFiles - hamlFiles - slimFiles - plainHtml

            println "task #${task.id}; view files: ${files.size()}; ERB: ${erbFiles.size()}; HAML: ${hamlFiles.size()}; " +
                    "SLIM: ${slimFiles.size()}; JAVASCRIPT: ${javascript.size()}; JSON: ${json.size()}; " +
                    "PLAIN HTML: ${plainHtml.size()}; " + "others: ${others.size()}"
            println others
            writer.writeNext(result)
        }

        others.unique().each { writer.writeNext(it) }
        writer.close()
    }

    static analyseAllForMultipleProjects(folder) {
        def cvsFiles = Util.findFilesFromDirectory(folder).findAll { it.endsWith(ConstantData.CSV_FILE_EXTENSION) }
        cvsFiles?.each {
            analyseProject(it)
        }
    }

    static extractHamlFiles(folder){
        println "Extracting haml files from: ${folder}"
        def files = Util.findFilesFromDirectory(folder).findAll { it.endsWith(".haml") }
        def out = (folder - "spg_repos\\") - "\\app\\views"
        def outFolder = new File("hamls\\"+out)
        outFolder.mkdir()
        files.each{ f ->
            def src = new File(f)
            def dst = new File(outFolder.path+"\\"+src.name)
            dst << src.text
        }
        println "Finished!"
    }

    static void main(String[] args) {
        //def addedFolder = "csv_views"
        //analyseAllForMultipleProjects(addedFolder)

        extractHamlFiles("spg_repos\\concord-consortium_rigse\\app\\views")
        extractHamlFiles("spg_repos\\meurio_meurio\\app\\views")
        extractHamlFiles("spg_repos\\meurio_meurio_accounts\\app\\views")
        extractHamlFiles("spg_repos\\siyelo_hrt\\app\\views")
        extractHamlFiles("spg_repos\\wwagner33_solar\\app\\views")

    }

}
