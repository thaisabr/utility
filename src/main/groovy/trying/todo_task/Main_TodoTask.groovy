package trying.todo_task

import br.ufpe.cin.tan.analysis.task.TodoTask


class Main_TodoTask {

    static void main(String[] args) {
        def repositoryUrl = "https://github.com/spgroup/rgms"

        /* TASK 1: Autofill de dissertacao */
        def featurePath = "Dissertacao.feature"
        def t1 = new TodoTask(repositoryUrl, true, "1", [[path: featurePath, lines: [87]]])
        def taskInterface1 = t1.computeTestBasedInterface()
        println "TAREFA REMOTA 1"
        println taskInterface1

        println "\n\n"

        /* TASK 6: Crud de Book, incluindo importação via XML. ********************************************************/
        featurePath = "Book.feature"
        def t2 = new TodoTask(repositoryUrl, true, "2", [[path: featurePath, lines: [7, 12, 17, 22, 27, 32]]])
        def taskInterface2 = t2.computeTestBasedInterface()
        println taskInterface2

        /*
        def localPath = "${System.getProperty("user.home")}${File.separator}Documents${File.separator}GitHub${File.separator}OriginalRgms"
        def t2 = new TodoTask(localPath, false, "2", [[path:featurePath, lines:[87]]])
        def taskInterface2 = t2.computeTestBasedInterface()
        println "TAREFA LOCAL"
        println taskInterface2*/
    }
}
