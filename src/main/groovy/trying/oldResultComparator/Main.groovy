package trying.oldResultComparator

import au.com.bytecode.opencsv.CSVReader
import au.com.bytecode.opencsv.CSVWriter
import groovy.util.logging.Slf4j

/*
* O que há de mais atual para avaliar os resultados está no pacote util.
* Esse código é antigo e está aqui para caso seja necessário avaliar algum dos aspectos avaliados aqui
* que ainda não são avaliados no código novo.
* */

@Slf4j
class Main {

    final String ext = ".csv"
    int headerSize = 7//16
    int taskColumn = 0
    int gherkinTests = 6
    int implGherkinTests = 7
    int stepMatchError = 12
    int astError = 14
    int gherkinAstError = 16
    int stepAstError = 18
    int viewsColumn = 22
    int itestSizeColumn = 23
    int irealSizeColumn = 24
    int precisionColumn = 27
    int recallColumn = 28

    static read(String csv){
        List<String[]> entries = []
        try {
            CSVReader reader = new CSVReader(new FileReader(csv))
            entries = reader.readAll()
            reader.close()
        } catch (Exception ex) {
            log.error ex.message
        }
        entries
    }

    def readTasks(String csv) {
        def result = []
        List<String[]> entries = read(csv)
        if (entries.size() > headerSize) {
            def data = entries.subList(headerSize, entries.size())
            data.each{ entry ->
                result += [[entry[taskColumn], entry[viewsColumn] as int, entry[precisionColumn] as double,
                            entry[recallColumn] as double, entry[itestSizeColumn] as double, entry[irealSizeColumn] as double]]
            }
        } else result = entries

        result
    }

    def readTasks(String csv, int i, int headerSize) {
        def result = []
        List<String[]> entries = read(csv)
        if (entries.size() > headerSize) {
            def data = entries.subList(headerSize, entries.size())
            data.each{ entry ->
                result += [[entry[taskColumn], entry[gherkinTests] as double, entry[implGherkinTests] as double,
                            entry[stepMatchError] as double, entry[astError] as double, entry[gherkinAstError] as double,
                            entry[stepAstError] as double, entry[viewsColumn+i] as int, entry[itestSizeColumn] as double,
                            entry[irealSizeColumn] as double, entry[precisionColumn+i] as double, entry[recallColumn+i] as double]]
            }
        } else result = entries

        result
    }

    def readTasks(String csv, int i) {
        def result = []
        List<String[]> entries = read(csv)
        if (entries.size() > headerSize) {
            def data = entries.subList(headerSize, entries.size())
            data.each{ entry ->
                result += [[entry[taskColumn], entry[viewsColumn+i] as int, entry[precisionColumn+i] as double, entry[recallColumn+i] as double]]
            }
        } else result = entries

        result
    }

    def readInfo(String csv, int column) {
        def result = []
        List<String[]> entries = read(csv)
        if (entries.size() > headerSize) {
            def data = entries.subList(headerSize, entries.size())
            data.each{ entry ->
                result += [[entry[taskColumn], entry[column] as int]]
            }
        } else result = entries

        result
    }

    def readInfo(String csv, int headerSize, int column) {
        def result = []
        List<String[]> entries = read(csv)
        if (entries.size() > headerSize) {
            def data = entries.subList(headerSize, entries.size())
            data.each{ entry ->
                result += [[entry[taskColumn], entry[column] as int]]
            }
        } else result = entries

        result
    }

    static compare(oldValue, newValue){
        def n1 = oldValue?.round(3)
        def n2 = newValue?.round(3)
        def result = ""
        if(n2 == n1) result = "equals ($n1, $n2)"
        else if(n2 > n1) result = "yes ($n1, $n2, +${n2-n1})"
        else result = "no ($n1, $n2, -${n1-n2})"
        result
    }

    static inverseCompare(oldValue, newValue){
        def result = ""
        if(newValue == oldValue) result = "equals ($oldValue, $newValue)"
        else if(newValue < oldValue) result = "yes ($oldValue, $newValue, -${oldValue-newValue})"
        else result = "no ($oldValue, $newValue, +${newValue-oldValue})"
        result
    }

    private configureFileName(String oldCsv, String newCsv){
        def name = ""
        if(oldCsv.contains(File.separator)){
            def index = oldCsv.lastIndexOf(File.separator)
            def path = oldCsv.substring(0, index+1)
            name = path + oldCsv.substring(index+1) - ext + "_"
            index = newCsv.lastIndexOf(File.separator)
            name += newCsv.substring(index+1)
        } else{
            name = oldCsv - ext + "_"+ newCsv
        }
        name
    }

    def compareRefactoring = { String oldCsv, String newCsv ->
        def allTasks1 = readTasks(oldCsv,0, 7)?.sort{ a, b -> a [taskColumn] <=> b [taskColumn]}
        def tasks2 = readTasks(newCsv,0, 15)?.sort{ a, b -> a [taskColumn] <=> b [taskColumn]}
        def idsTasks2 = tasks2.collect{ it[taskColumn] }
        def tasks1 = allTasks1.findAll{ it[taskColumn] in idsTasks2 }?.sort{ a, b -> a [taskColumn] <=> b [taskColumn]}

        println "tasks1: ${allTasks1.collect{it[taskColumn]}}"
        println "tasks2: ${tasks2.collect{it[taskColumn]}}"
        println "tasks1 equals tasks2: ${tasks1.collect{it[taskColumn]}}"

        def name = configureFileName(oldCsv, newCsv)
        def writer = new CSVWriter(new FileWriter(name))
        String[] text = ["Task", "Gherkin Tests", "Impl Gherkin Tests", "Step Match Error", "AST error", "Gherkin AST error",
                         "Step AST error", "Improved views", "ITest size", "IReal size", "Improved precision",
                         "Improved recall"]
        writer.writeNext(text)

        for(int i=0; i<tasks2.size(); i++){
            def t1 = tasks1.get(i)
            def t2 = tasks2.get(i)
            if(t1[taskColumn] == t2[taskColumn]){
                def gherkinTests = compare(t1[1], t2[1])
                def implGherkinTests = compare(t1[2], t2[2])
                def stepMatchError = compare(t1[3], t2[3])
                def astError = compare(t1[4], t2[4])
                def gherkinAstError = compare(t1[5], t2[5])
                def stepAstError = compare(t1[6], t2[6])
                def views = inverseCompare(t1[7], t2[7])
                def itestSize = compare(t1[8], t2[8])
                def irealSize = compare(t1[9], t2[9])
                def precision = compare(t1[10], t2[10])
                def recall = compare(t1[11], t2[11])
                String[] line = [t1[taskColumn], gherkinTests, implGherkinTests, stepMatchError, astError, gherkinAstError,
                                 stepAstError, views, itestSize, irealSize, precision, recall]
                writer.writeNext(line)
            } else {
                println "It is not possible to compare tasks with different ID!"
            }
        }
        writer.close()
    }

    def compareViewsPrecisionRecall = { String oldCsv, String newCsv ->
        def tasks1 = readTasks(oldCsv)
        def tasks2 = readTasks(newCsv)

        def name = configureFileName(oldCsv, newCsv)

        def writer = new CSVWriter(new FileWriter(name))
        String[] text = ["Task", "Improved views", "Improved precision", "Improved recall", "Increased ITest", "Increased IReal"]
        writer.writeNext(text)

        for(int i=0; i<tasks1.size(); i++){
            def t1 = tasks1.get(i)
            def t2 = tasks2.get(i)
            def views = inverseCompare(t1[1], t2[1])
            def precision = compare(t1[2], t2[2])
            def recall = compare(t1[3], t2[3])
            def itestSize = compare(t1[4], t2[4])
            def irealSize = compare(t1[5], t2[5])
            String[] line = [t1[0], views, precision, recall, itestSize, irealSize]
            writer.writeNext(line)
        }

        writer.close()
    }

    def compareMatchErrors = { String oldCsv, String newCsv ->
        def tasks1 = readInfo(oldCsv, 11)
        def tasks2 = readInfo(newCsv, 7, 11)

        def name = configureFileName(oldCsv, newCsv)

        def writer = new CSVWriter(new FileWriter(name))
        String[] text = ["Task", "Improved errors"]
        writer.writeNext(text)

        for(int i=0; i<tasks1.size(); i++){
            def t1 = tasks1.get(i)
            def t2 = tasks2.get(i)
            def errors = inverseCompare(t1[1], t2[1])
            String[] line = [t1[0], errors]
            writer.writeNext(line)
        }

        writer.close()
    }

    def compareGherkinTests = { String oldCsv, String newCsv ->
        def tasks1 = readInfo(oldCsv, 7, 6)
        println "tasks1 size: ${tasks1.size()}"
        def tasks2 = readInfo(newCsv, 7, 6)
        println "tasks2 size: ${tasks2.size()}"

        if(tasks1.size() != tasks2.size()){
            println "Planilhas com quantidade diferente de tarefas"
        } else {
            def name = configureFileName(oldCsv, newCsv)

            def writer = new CSVWriter(new FileWriter(name))
            String[] text = ["Task", "Reduced"]
            writer.writeNext(text)

            for(int i=0; i<tasks1.size(); i++){
                def t1 = tasks1.get(i)
                def t2 = tasks2.get(i)
                def errors = inverseCompare(t1[1], t2[1])
                String[] line = [t1[0], errors]
                writer.writeNext(line)
            }

            writer.close()
        }
        println ""
    }

    static compareResults(String rootFolder, Closure comparator){
        File dir = new File(rootFolder)
        def internalFolders = dir?.listFiles()?.findAll{ it.isDirectory() }*.absolutePath
        internalFolders.each{ folder ->
            println "Folder: $folder"
            File intDir = new File(folder)
            def files = intDir?.listFiles()
            println "Files: ${files*.name}"
            def path = files*.absolutePath
            if(files?.size()==2) comparator(path.get(0), path.get(1))
        }
    }

     static void main(String[] args){
         Main obj = new Main()

         //compareResults("results", obj.compareRefactoring)

         //compareResults("results", obj.compareGherkinTests)
         compareResults("results", obj.compareViewsPrecisionRecall)
         //compareResults("results", obj.compareMatchErrors)

        /*def csv1 = "hrt28.csv"
        def csv2 = "hrt29.csv"
        compareResults(csv1, csv2)*/
    }

}
