package trying.projects

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

class ProjectManager {

    static void main(String[] args){
        def folder = "tasks\\rails"
        def files = Util.findFilesFromDirectory(folder)
        def projects = [] as Set
        files.each{ file ->
            List<String[]> entries = CsvUtil.read(file)
            entries.remove(0)
            projects += entries.collect{ it[0] }.sort()
        }

        projects = projects.sort().unique()
        println "Projects: ${projects.size()}"
        projects.each{ println it }

        def outputfile = new File("tasks\\rails\\projects.txt")
        outputfile.withWriter { writer ->
            projects.each{ p-> writer.write("$p\n") }
        }

    }

}
