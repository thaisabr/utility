package trying.others

import br.ufpe.cin.tan.test.ruby.RubyTestCodeVisitor
import org.jrubyparser.CompatVersion
import org.jrubyparser.Parser
import org.jrubyparser.parser.ParserConfiguration

class VisitorTest {

    static void main(String[] args) {

        //def path = "repositories\\siyelo_hrt\\features\\step_definitions\\app_steps.rb"
        def path = "repositories\\meurio_meurio\\config\\routes.rb"
        FileReader reader = new FileReader(path)
        Parser rubyParser = new Parser()
        CompatVersion version = CompatVersion.RUBY2_0
        ParserConfiguration config = new ParserConfiguration(0, version)
        def node = null

        try {
            node = rubyParser.parse("<code>", reader, config)
            //node.childNodes().each{ println it }
            RubyTestCodeVisitor visitor = new RubyTestCodeVisitor(path)
            node.accept(visitor)
        } catch (Exception ex) {
            println "Problem to visit file $path: ${ex.message}"
        }
        finally {
            reader?.close()
        }

    }

}
