package trying.others

import br.ufpe.cin.tan.commit.GitRepository


class MergeCommits {

    static void main(String[] args){

        GitRepository gitRepository = GitRepository.getRepository("https://github.com/meurio/meurio_accounts")
        gitRepository.findMergeScenarios()

    }

}
