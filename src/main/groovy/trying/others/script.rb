#!/usr/bin/ruby
require 'rubygems'
require 'active_support/inflector'

#puts "pencils".singularize
#puts "dog".pluralize

def plural(text)
  text.pluralize
end

def singular(text)
  text.singularize
end