package trying.others

import org.jruby.embed.PathType
import org.jruby.embed.ScriptingContainer

class CallRubyFromGroovy {

    static void main(String[] args) {

        ScriptingContainer container = new ScriptingContainer()

        //se o script usa alguma gem, ela tem que ser adicionada no loadpath
        container.loadPaths.add("C:\\jruby-9.1.5.0\\lib\\ruby\\gems\\shared\\gems")
        container.loadPaths.add("C:\\jruby-9.1.5.0\\lib\\ruby\\gems\\shared\\gems\\activesupport-inflector-0.1.0\\lib")
        container.loadPaths.add("C:\\jruby-9.1.5.0\\lib\\ruby\\gems\\shared\\gems\\i18n-0.7.0\\lib")
        container.loadPaths.add("C:\\jruby-9.1.5.0\\lib\\ruby\\gems\\shared\\gems\\actionpack-5.0.0.1\\lib")

        //executar o script inteiro
        container.runScriptlet(PathType.ABSOLUTE, "actionpack.rb") //do arquivo
        //container.runScriptlet("puts 'hello world'") //da string

        //chamar método
        //Object receiver = container.runScriptlet(PathType.ABSOLUTE, "inflector.rb") //método em arquivo
        //println container.callMethod(receiver, "plural", "clazzes")
        //println container.callMethod(receiver, "singular", "clazzes")



    }


}
