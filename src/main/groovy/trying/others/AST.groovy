package trying.others

import groovy.util.logging.Slf4j
import org.jrubyparser.CompatVersion
import org.jrubyparser.Parser
import org.jrubyparser.parser.ParserConfiguration


@Slf4j
class AST {

    static void main(String[] args) {
        def code = "link_to 'Edit', edit_otml_category_path(@otml_category)"

        StringReader reader = new StringReader(code)
        Parser rubyParser = new Parser()
        CompatVersion version = CompatVersion.RUBY2_0
        ParserConfiguration config = new ParserConfiguration(0, version)
        def result = null

        try {
            result = rubyParser.parse("<code>", reader, config)
        } catch (Exception ex) {
            log.error "Problem to visit file $code: ${ex.message}"
        }
        finally {
            reader?.close()
        }
        println result
    }
}
