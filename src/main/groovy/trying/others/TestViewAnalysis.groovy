package trying.others

import br.ufpe.cin.tan.test.ruby.views.ViewCodeExtractor

class TestViewAnalysis {

    static void main(String[] args) {
        ViewCodeExtractor viewCodeExtractor = new ViewCodeExtractor()
        def code = viewCodeExtractor.extractCode("error\\show.html.haml")
        println "code:"
        println code
    }

}
