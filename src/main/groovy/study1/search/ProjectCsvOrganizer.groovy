package study1.search

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

class ProjectCsvOrganizer {

    def folders
    String projectFile
    def projects
    def outputFolder
    def files

    ProjectCsvOrganizer(){
        outputFolder = "search${File.separator}entry${File.separator}"
        files = Util.findFilesFromDirectory(outputFolder)
    }

    ProjectCsvOrganizer(String entryPoint){
        configureFolders(entryPoint)
        projectFile = "${File.separator}1-github${File.separator}projects.csv"
        outputFolder = "search${File.separator}entry${File.separator}"
        extractAllProjects()
    }

    def collapseProjectCsv(){
        List<String []> content = []
        content += ["URL","MASTER_BRANCH","CREATED_AT","STARS","SIZE","DESCRIPTION"] as String[]
        List<String []> partial = []

        files.each{ file ->
            def lines = CsvUtil.read(file)
            lines.remove(0)
            partial += lines
        }
        content += partial.unique{ [it[0], it[1]] }.sort{ it[0] }
        println "projects quantity: ${content.size()-1}"
        CsvUtil.write("${outputFolder}projects_final.csv", content)
    }

    def generateCsv(String output){
        println "projects quantity: ${projects.size()}"
        List<String []> content = []
        content += ["URL","MASTER_BRANCH","CREATED_AT","STARS","SIZE","DESCRIPTION"] as String[]
        projects.each{ project ->
            content += project as String []
        }
        CsvUtil.write("$outputFolder$output", content)
    }

    private extractAllProjects(){
        projects = []
        folders.each{ folder ->
            projects += extractProjectsFromFolder(folder)
        }
        projects = projects.unique{ [it[0], it[1]] }.sort{ it[0] }
        projects -= projects.findAll{ !it[0].startsWith("http") }
    }

    private extractProjectsFromFolder(String folder){
        def result = []
        def filename = folder + projectFile
        def lines = CsvUtil.read(filename)
        if(lines.size()>1) {
            result = lines?.subList(1,lines.size())?.collect{ it }
        }
        result
    }

    private configureFolders(String entryPoint){
        folders = Util.findFoldersFromDirectory(entryPoint).findAll{
            //it.endsWith("_cucumber") || it.endsWith("_simplecov")
            //it.endsWith("_simplecov") || it.endsWith("_coveralls")
            it.endsWith("coveralls")
        }
    }

    static void main(String[] args){
        ProjectCsvOrganizer obj1 = new ProjectCsvOrganizer("search${File.separator}search4")
        obj1.generateCsv("projects_4.csv")
        //ProjectCsvOrganizer obj = new ProjectCsvOrganizer()
        //obj.collapseProjectCsv()
    }
}
