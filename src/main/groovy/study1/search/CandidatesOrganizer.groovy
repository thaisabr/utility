package study1.search

import br.ufpe.cin.tan.util.CsvUtil

class CandidatesOrganizer {
    String resultFolder

    String candidatesFile
    String coverallsFile
    String cucumberFile
    String railsFile
    String simplecovFile

    List candidates
    List railsProjects
    List cucumberProjects
    List coverallsProjects
    List simplecovProjects
    List cucumberAndCoverallsProjects
    List cucumberAndSimplecovProjects

    CandidatesOrganizer(String entryPoint){
        resultFolder = "$entryPoint${File.separator}2-filtered${File.separator}"
        candidatesFile = "${resultFolder}candidate-projects.csv"
        coverallsFile = "${resultFolder}coveralls-projects.csv"
        cucumberFile = "${resultFolder}cucumber-projects.csv"
        railsFile = "${resultFolder}rails-projects.csv"
        simplecovFile = "${resultFolder}simplecov-projects.csv"
    }

    def extract(){
        extractRailsProjects()
        extractCucumberProjects()
        extractCoverallsProjects()
        extractSimplecovProjects()
        extractCucumberAndCoverallsProjects()
        extractCucumberAndSimplecovProjects()
        extractCandidates()
        saveCandidates()
    }

    def saveCandidates(){
        List<String []> content = []
        content += ["URL","MASTER_BRANCH","CREATED_AT","STARS","SIZE","DESCRIPTION"] as String[]
        content += candidates
        CsvUtil.write(candidatesFile, content)
    }

    static collapseCandidates(){
        def folder = "search${File.separator}paper-sample${File.separator}"
        def outputFile1 = "${folder}candidates_final.csv"
        def outputFile2 = "${folder}candidates_final_paper.csv"
        def files = ["${folder}coveralls${File.separator}2-filtered${File.separator}candidate-projects.csv",
                     "${folder}simplecov${File.separator}2-filtered${File.separator}candidate-projects.csv"]

        List<String[]> partial = []
        files.each{ file ->
            def lines = CsvUtil.read(file)
            lines.remove(0)
            partial += lines
        }

        List<String[]> finalSet = []
        partial.each{ project ->
            def others = partial - project
            def duplicate = others.find{ it[0] == project[0] }
            if(duplicate){
                def stars = (duplicate[3] as BigDecimal) > (project[3] as BigDecimal)? duplicate[3] : project[3]
                def size = (duplicate[4] as BigDecimal) > (project[4] as BigDecimal)? duplicate[4] : project[4]
                def gems1 = duplicate[6]
                gems1 = gems1.substring(1, gems1.size()-1).tokenize(",")*.trim()
                def gems2 = project[6]
                gems2 = gems2.substring(1, gems2.size()-1).tokenize(",")*.trim()
                def gems = (gems1 + gems2)?.unique()?.sort()
                def newProject = [project[0], project[1], project[2], stars, size, project[5], gems] as String[]
                finalSet += newProject
                partial -= duplicate
            } else {
                finalSet += project
            }
        }

        finalSet = finalSet.unique{ [it[0], it[1]] }.sort{ a, b -> (b[3] as Integer) <=> (a[3] as Integer) }
        println "final candidates quantity: ${finalSet.size()}"
        finalSet.each{ println it[0] + "; " + it[1] }

        List<String []> content = []
        content += ["URL","MASTER_BRANCH","CREATED_AT","STARS","SIZE","DESCRIPTION", "GEMS"] as String[]
        content += finalSet
        CsvUtil.write(outputFile1, content)

        content = []
        content += ["URL", "CREATED_AT","STARS","SIZE"] as String[]
        content += finalSet.collect{ [it[0], it[2], it[3], it[4]] as String [] }
        CsvUtil.write(outputFile2, content)
    }

    private extractCoverallsProjects(){
        coverallsProjects = extractContent(coverallsFile)
        println "coverallsProjects: ${coverallsProjects.size()}"
    }

    private extractCucumberProjects(){
        cucumberProjects = extractContent(cucumberFile)
        println "cucumberProjects: ${cucumberProjects.size()}"
    }

    private extractRailsProjects(){
        railsProjects = extractContent(railsFile)
        println "railsProjects: ${railsProjects.size()}"
    }

    private extractSimplecovProjects(){
        simplecovProjects = extractContent(simplecovFile)
        println "simplecovProjects: ${simplecovProjects.size()}"
    }

    private extractCucumberAndCoverallsProjects(){
        cucumberAndCoverallsProjects = []
        if(!cucumberProjects.empty && !coverallsProjects.empty) {
            //cucumberAndCoverallsProjects = cucumberProjects.intersect(coverallsProjects)
            def diff1 = cucumberProjects - coverallsProjects
            def diff2 = coverallsProjects - cucumberProjects
            cucumberAndCoverallsProjects = cucumberProjects + coverallsProjects - diff1 - diff2
            cucumberAndCoverallsProjects = cucumberAndCoverallsProjects.unique{ [it[0], it[1]] }
        }

        println "cucumberAndCoverallsProjects: ${cucumberAndCoverallsProjects.size()}"
        cucumberAndCoverallsProjects.sort{it[0]}.each{ println it[0] + "; " + it[1] }
    }

    private extractCucumberAndSimplecovProjects(){
        cucumberAndSimplecovProjects = []
        if(!cucumberProjects.empty && !simplecovProjects.empty) {
            //cucumberAndSimplecovProjects = cucumberProjects.intersect(simplecovProjects)
            def diff1 = cucumberProjects - simplecovProjects
            def diff2 = simplecovProjects - cucumberProjects
            cucumberAndSimplecovProjects = cucumberProjects + simplecovProjects - diff1 - diff2
            cucumberAndSimplecovProjects = cucumberAndSimplecovProjects.unique{ [it[0], it[1]] }
        }

        println "cucumberAndSimplecovProjects: ${cucumberAndSimplecovProjects.size()}"
        cucumberAndSimplecovProjects.sort{it[0]}.each{ println it[0] + "; " + it[1] }
    }

    private extractCandidates(){
        candidates = (cucumberAndCoverallsProjects + cucumberAndSimplecovProjects).unique{ [it[0],it[1]]}.sort{ it[0] }
        println "candidates: ${candidates.size()}"
        candidates.each{ println it[0] }
    }

    private extractContent = { file ->
        def values = CsvUtil.read(file)
        values.remove(0)
        values
    }

    static void main(String[] args){
        /*CandidatesOrganizer candidatesOrganizer = new CandidatesOrganizer("search${File.separator}paper-search4")
        candidatesOrganizer.extract()
        candidatesOrganizer = new CandidatesOrganizer("search${File.separator}paper-search2")
        candidatesOrganizer.extract()
        candidatesOrganizer = new CandidatesOrganizer("search${File.separator}paper-search3")
        candidatesOrganizer.extract()*/
        collapseCandidates()
    }
}
