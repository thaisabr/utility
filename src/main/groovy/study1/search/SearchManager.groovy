package study1.search

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

class SearchManager {

    def folders
    String projectFile
    String candidateFile
    String logFile
    String railsFile
    Set projects
    Set candidates
    Set railsProjects
    Set logs

    SearchManager(String entryPoint){
        configureFolders(entryPoint)
        projectFile = "${File.separator}1-github${File.separator}projects.csv"
        candidateFile = "${File.separator}2-filtered${File.separator}candidate-projects.csv"
        railsFile = "${File.separator}2-filtered${File.separator}rails-projects.csv"
        logFile = "${File.separator}output${File.separator}execution.log"
        extractAllLogs()
    }

    def extractAllProjects(){
        projects = [] as Set
        folders.each{ folder ->
            projects += extractProjectsFromFolder(folder)
        }
    }

    def extractAllCandidates(){
        candidates = [] as Set
        folders.each{ folder ->
            candidates += extractCandidatesFromFolder(folder)
        }
    }

    def extractAllRailsProjects(){
        railsProjects = [] as Set
        folders.each{ folder ->
            railsProjects += extractRailsProjectsFromFolder(folder)
        }
    }

    def extractAllLogs(){
        logs = [] as Set
        folders.each{ folder ->
            logs += folder + logFile
        }
    }

    private extractRailsProjectsFromFolder(String folder){
        def result = []
        def filename = folder + railsFile
        def lines = CsvUtil.read(filename)
        if(lines.size()>1) {
            //result = lines?.subList(1,lines.size())?.collect{ it[1] }?.unique()
            result = lines?.subList(1,lines.size())?.collect{ it[0] }?.unique()
        }
        result
    }

    private extractCandidatesFromFolder(String folder){
        def result = []
        def filename = folder + candidateFile
        def lines = CsvUtil.read(filename)
        if(lines.size()>1) {
            //result = lines?.subList(1,lines.size())?.collect{ it[1] }?.unique()
            result = lines?.subList(1,lines.size())?.collect{ it[0] }?.unique()
        }
        result
    }

    private extractProjectsFromFolder(String folder){
        def result = []
        def filename = folder + projectFile
        def lines = CsvUtil.read(filename)
        if(lines.size()>1) {
            result = lines?.subList(1,lines.size())?.collect{ it[0] }?.unique()
        }
        result
    }

    private configureFolders(String entryPoint){
        folders = Util.findFoldersFromDirectory(entryPoint).findAll{
            //it.endsWith("_cucumber") || it.endsWith("_cucumber_simplecov")
            it.endsWith("_coveralls") || it.endsWith("_simplecov")
        }
    }

    static void main(String[] args){
        //SearchManager searchManager = new SearchManager("search${File.separator}search1")
        SearchManager searchManager = new SearchManager("search${File.separator}search2")
        searchManager.extractAllProjects()
        searchManager.extractAllCandidates()
        searchManager.extractAllRailsProjects()

        println "All found projects: ${searchManager.projects.size()}"
        searchManager.projects.each{ println it }

        println "All found rails projects: ${searchManager.railsProjects.size()}"
        searchManager.railsProjects.each{ println it }

        println "All found candidates: ${searchManager.candidates.size()}"
        searchManager.candidates.each{ println it }

        println "All log files: ${searchManager.logs.size()}"
        searchManager.logs.each{ println it }

    }


}
