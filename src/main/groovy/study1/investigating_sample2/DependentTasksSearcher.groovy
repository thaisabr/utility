package study1.investigating_sample2

import br.ufpe.cin.tan.analysis.data.ExporterUtil
import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util
import groovy.util.logging.Slf4j

@Slf4j
class DependentTasksSearcher {

    int idIndex //index no csv de tarefas relevantes
    int hashesIndex //index no csv de tarefas relevantes
    List<String[]> tasks
    def tasksFolder
    def files
    List<String[]> selfContained
    List<String[]> notSelfContained
    def identical
    List<String[]> identicalTasks
    List<String[]> selectedTasks
    List<String[]> tasksToDiscard

    DependentTasksSearcher(String tasksFolder){
        this.tasks = []
        this.identical = []
        this.identicalTasks = []
        this.selfContained = []
        this.notSelfContained = []
        this.selectedTasks = []
        this.tasksToDiscard = []
        this.idIndex = 1
        this.hashesIndex = 5
        this.tasksFolder = tasksFolder
        this.files = Util.findFilesFromDirectory(tasksFolder)
    }

    static void main(String[] args){
        def tasksFolder = "tasks\\study2-investigation-dependentTasks"

        def searcher = new DependentTasksSearcher(tasksFolder)
        searcher.analyse()
        searcher.exportSelectedTasks()
    }

    def showIdenticalTasks(){
        log.info "identical tasks: ${identicalTasks.size()}"
        /*identicalTasks.each{
            log.info "url: ${it[0]}, id: ${it[1]}, #hashes: ${it[4]}, hashes: ${it[5]}"
        }*/
        log.info "identical task pairs: ${identical.size()}"
        /*identical.eachWithIndex{ value, index ->
            log.info "pair ${index+1}:"
            value.value.each{
                log.info "url: ${it[0]}, id: ${it[1]}, #hashes: ${it[4]}, hashes: ${it[5]}"
            }
        }*/
    }

    def identifyIdenticalTasks(){
        identical = tasks.groupBy { it[hashesIndex] }
        identical = identical.findAll{ it.value.size()>1 }
        identicalTasks = []
        identical.each{
            identicalTasks += it.value
        }
        showIdenticalTasks()

    }

    def analyse(){
        tasks = []
        files.each{ file ->
            log.info "File: $file"
            tasks += extractTasks(file)
            def tasksId = tasks.collect{ it[idIndex] as int }.sort()
            log.info "tasks: ${tasksId.size()}"
        }

        identifyIdenticalTasks()

        filterSelfContainedTasks()
        log.info "selfContained: ${selfContained.size()}"

        def intersection = selfContained.intersect(identicalTasks)
        log.info "intersection selfContained and indenticalTasks: ${intersection.size()}"

        def temp = (selfContained + identicalTasks)?.unique()
        notSelfContained = (tasks - temp)?.unique()
        log.info "not selfContained: ${notSelfContained.size()}"

        selectIdenticalTasksToDiscard()
        log.info "tasksToDiscard: ${tasksToDiscard.size()}"

        selectedTasks = (selfContained - tasksToDiscard)?.sort{ [it[0], it[1] as int] }
        log.info "selectedTasks: ${selectedTasks.size()}"
        selectedTasks.each{
            log.info "url: ${it[0]}, id: ${it[1]}, #hashes: ${it[4]}"
        }
    }

    def selectIdenticalTasksToDiscard(){
        tasksToDiscard = []
        log.info "identical task pairs: ${identical.size()}"
        identical.each{ pair ->
            def temp = pair.value.sort{ [it[0], it[1]] }
            tasksToDiscard.add(temp.last())
        }
    }

    private static extractTasks(String file){
        List<String[]> entries = CsvUtil.read(file)
        def url = entries[0][1]
        //log.info "url: ${url}"
        List<String[]> tasks = entries.subList(13, entries.size())
        for(int i=0; i<tasks.size(); i++){
            tasks[i] = ([url] + tasks[i]).flatten() as String[]
        }
        //log.info "tasks into extractTasks(): ${tasks.size()}"
        //tasks.each{ log.info "${it[0]}, ${it[1]}"}
        tasks
    }

    private filterSelfContainedTasks(){
        selfContained = []
        //["task_a", "hashes_a", "task_b", "hashes_b" , "intersection", "%_a", "%_b"]
        def hashesSimilarity = computeHashSimilarity()
        if(hashesSimilarity.empty) return
        def maxSimResult = []
        def pairsMaxSimilarity = hashesSimilarity.findAll{ (it[5]==1) || (it[6]==1) }
        def ids = tasks.collect{ it[idIndex] }
        ids.each{ id ->
            def n = pairsMaxSimilarity.findAll{ it[0]==id || it[2]==id }
            if(n.size()>0) {
                def temp = []
                n.each{ pair ->
                    if(pair[1]==pair[3]){
                        def commitsNumber = pair[1] as String
                        def idPair = [pair[0] as int, pair[2] as int].sort()
                        temp.add(tasks.find{ it[idIndex] == (idPair[0] as String) && it[2] == commitsNumber })
                    } else if(pair[0]==id && pair[1]<pair[3] ){
                        temp.add(tasks.find{ it[idIndex] == pair[0] })
                    } else if(pair[2]==id && pair[3]<pair[1]){
                        temp.add(tasks.find{ it[idIndex] == pair[2] })
                    }
                }
                maxSimResult += temp
            }
        }
        maxSimResult = maxSimResult.unique()
        //log.info "maxSimResult: ${maxSimResult.size()}"
        /*maxSimResult.sort{ it[0] }.sort{ it[1] as int }.each{
            println "${it[0]}, ${it[1]}"
        }*/

        selfContained = (tasks - maxSimResult).sort{ it[idIndex] as double }
    }

    private computeHashSimilarity(){
        def hashesSimilarity = []
        def taskPairs = ExporterUtil.computeTaskPairs(tasks)
        if(taskPairs.empty) return hashesSimilarity
        taskPairs?.each { item ->
            def task = item.task
            def hashes1 = task[hashesIndex].tokenize(',[]')*.trim()
            item.pairs?.each { other ->
                def hashes2 = other[hashesIndex].tokenize(',[]')*.trim()
                def intersection = hashes1.intersect(hashes2).size()
                hashesSimilarity.add([task[idIndex], hashes1.size(), other[idIndex], hashes2.size(), intersection,
                                      intersection/hashes1.size(), intersection/hashes2.size()])
            }
        }
        hashesSimilarity
    }

    private exportSelectedTasks(){
        List<String[]> content = []
        content += ["URL","Task","Dates","#Devs","#Commits","Hashes","#Gherkin_Tests","#Sted_defs",
                    "#TestI","#TaskI","TestI","TaskI","Precision","Recall","Rails","#visit_call",
                    "Lost_visit_call","#Views_TestI","#Code_View_Analysis","Code_View_Analysis",
                    "Methods_Unknown_Type","Renamed_Files","Deleted_Files","NotFound_Views",
                    "#NotFound_Views","Timestamp","Has_Merge","#FP","#FN","FP","FN","#Hits","Hits","f2"] as String[]
        content += selectedTasks
        CsvUtil.write("output${File.separator}selected_tasks_after_dependence_analysis.csv", content)
    }

}
