package study1.investigating_sample2

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

class CountingMerges {

    static void main(String[] args){
        def folder = "tasks${File.separator}study2-countingMerges"
        def files = Util.findFilesFromDirectory(folder)
        files.each{ file ->
            List<String[]> entries = CsvUtil.read(file)
            def url = entries[0]
            def merges = entries.subList(2, entries.size())
            println "project: $url"
            println "merges: ${merges.size()}\n"
        }
    }
}
