package study1.investigating_sample2

import br.ufpe.cin.tan.commit.GitRepository
import br.ufpe.cin.tan.util.ConstantData
import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util
import gherkin.ast.Background

/* Código para extrair a quantidade de testes Gherkin de um projeto (algo solicitado pelos revisores do artigo JSS 2019).*/

class RepositoryTestsNumber {
    GitRepository gitRepository
    List<String> urls

    RepositoryTestsNumber(String csv){
        urls = CsvUtil.read(csv).collect{ it[0] }
    }

    def extractInfo(){
        List<String[]> content = []
        content += ["url", "scenarios"] as String[]
        urls.each{ url ->
            def counter = 0
            println "Url: $url"
            if(url.endsWith(ConstantData.GIT_EXTENSION)) gitRepository = GitRepository.getRepository(url)
            else gitRepository = GitRepository.getRepository(url+ConstantData.GIT_EXTENSION)

            def gherkinFiles = Util.findFilesFromDirectory(gitRepository.localPath).findAll { Util.isGherkinFile(it) }
            println "gherkin files: ${gherkinFiles.size()}"
            gherkinFiles.each{ file ->
                def result = gitRepository.parseGherkinFile(file) //[feature: feature, content: content]
                def tests = result?.feature?.getChildren()?.findAll { !(it instanceof Background) }
                if(tests) counter += tests.size()
            }
            content += [url, counter] as String[]
        }
        CsvUtil.write("output${File.separator}projects_scenarios.csv", content)
    }

    static void main(String[] args){
        RepositoryTestsNumber manager = new RepositoryTestsNumber("url.csv")
        manager.extractInfo()
    }
}
