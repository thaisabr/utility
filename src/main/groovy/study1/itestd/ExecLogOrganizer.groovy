package study1.itestd

import br.ufpe.cin.tan.util.ruby.RubyUtil

/*
* Código para extrair ITestD do log da execução do(s) teste(s) da tarefa (março 2017)
* */

class ExecLogOrganizer {

    static void main(String[] args){
        //File file = new File("itestd${File.separator}log_teste_leihs_43122757.log")
        //File file = new File("itestd${File.separator}log_teste_leihs_52700935.log")
        File file = new File("itestd${File.separator}log_teste_leihs_52700989.log")

        /*File file = new File("itestd${File.separator}1-features_borrow_timeoutpage_feature42.txt")
        processingPieces(file)
        file = new File("itestd${File.separator}2-features_borrow_timeoutpage_feature54.txt")
        processingPieces(file)
        file = new File("itestd${File.separator}3-features_borrow_verfuegbarkeit.feature10.txt")*/

        //File file = new File("itestd${File.separator}log_teste_leihs_53461159.log")
        //File file = new File("itestd${File.separator}log_teste_leihs_55072594.log")
        //File file = new File("itestd${File.separator}log_teste_leihs_62647952.log")

        processingPieces(file)
        //processingSequential(file)
    }

    static processingPieces(File file){
        def lines = file.readLines()
        def controllersAndActions = lines.findAll{ it.trim().startsWith("Processing by ") }?.unique()?.collect{
            (it.trim() - "Processing by ") - " as HTML"
        }
        def views = (lines - controllersAndActions).findAll{ it.trim().startsWith("Rendered ") }?.unique()?.collect{
            def r = it.trim() - "Rendered "
            int i = r.indexOf(" ")
            r.substring(0,i)
        }?.unique()?.sort()
        def models = (lines - (controllersAndActions + views)).findAll{ it.contains(" Load ") }?.unique()?.collect{
            it = it.replaceAll(/\u001B/, "")
            def i = it.indexOf(" Load ")
            def j = findUpperLetter(it)
            def r = it.substring(j, i)
            r.trim()
        }?.unique()?.sort()

        println "controllers and actions:"
        controllersAndActions.each{ println it }

        def controllers = controllersAndActions.collect {
            int i = it.indexOf("#")
            def value = it.substring(0,i)
            ("controllers/${RubyUtil.camelCaseToUnderscore(value)}.rb").replaceAll("\\\\", "/")
        }?.unique()?.sort()
        models = models?.collect { ("models/${RubyUtil.camelCaseToUnderscore(it)}.rb").replaceAll("\\\\", "/") }?.unique()
        views = views?.collect{ "views/$it" }?.unique()
        println "\nITestD:"
        controllers?.each{ println it }
        models?.each{ println it }
        views?.each{ println it }

        println "\nITestD (list format):"
        def itestd = controllers+ models+ views
        println itestd
    }

    static findUpperLetter(String word){
        def index = -1
        for(int i=0; i< word.size(); i++){
            if(word[i] ==~/[A-Z]/){
                index = i
                break
            }
        }
        index
    }

    static processingSequential(File file){
        def result = []

        def lines = file.readLines()
        lines.each{ line ->
            def r = line.trim()

            //controller
            if(r.startsWith("Processing by ")){
                r = (r - "Processing by ") - " as HTML"
            }

            //view
            else if(r.startsWith("Rendered ")){
                r = r - "Rendered "
                int i = r.indexOf(" ")
                r = r.substring(0,i)
            }

            //model
            else if(r.contains(" Load ")){
                r = r.replaceAll(/\u001B/, "")
                def i = r.indexOf(" Load ")
                def j = findUpperLetter(r)
                r = r.substring(j, i).trim()
            }

            else{
                r = null
            }
            if(r) result += r
        }

        println "\nSequential util:"
        result.each{ println it }
    }


}
