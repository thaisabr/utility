package study1.itestd

import au.com.bytecode.opencsv.CSVWriter
import br.ufpe.cin.tan.util.Util

class ItestdEvaluation {

    private static calculateTruePositives(Set set1, Set set2) {
        (set1.intersect(set2)).size()
    }

    static double calculatePrecision(Set ITest, Set IReal) {
        double result = 0
        if (!ITest || ITest.empty || !IReal || IReal.empty) return 0
        def truePositives = calculateTruePositives(ITest, IReal)
        if (truePositives > 0) result = (double) truePositives / ITest.size()
        result
    }

    static double calculateRecall(Set ITest, Set IReal) {
        double result = 0
        if (!ITest || ITest.empty || !IReal || IReal.empty) return 0
        def truePositives = calculateTruePositives(ITest, IReal)
        if (truePositives > 0) result = (double) truePositives / IReal.size()
        result
    }

    static analyseTaskNoView(TaskInterface taskInterface){
        def id = taskInterface.id
        def itestd_all = taskInterface.ITestD.collect{"zhdk_leihs/app/${it}"} as Set
        def itests_all = taskInterface.ITestS_noview.collect{"zhdk_leihs/app/${it}"} as Set
        def ireal_all = taskInterface.IReal.collect{"zhdk_leihs/app/${it}"} as Set

        def precision_all_sr = calculatePrecision(itests_all, ireal_all)
        def recall_all_sr = calculateRecall(itests_all, ireal_all)

        def itests_controllers = itests_all.findAll{ Util.isControllerFile(it) } as Set
        def ireal_controllers = ireal_all.findAll{ Util.isControllerFile(it) } as Set
        def precision_controllers_sr = calculatePrecision(itests_controllers, ireal_controllers)
        def recall_controllers_sr = calculateRecall(itests_controllers, ireal_controllers)

        def precision_all_dr = calculatePrecision(itestd_all, ireal_all)
        def recall_all_dr = calculateRecall(itestd_all, ireal_all)

        def itestd_controllers = itestd_all.findAll{ Util.isControllerFile(it) } as Set
        def precision_controllers_dr = calculatePrecision(itestd_controllers, ireal_controllers)
        def recall_controllers_dr = calculateRecall(itestd_controllers, ireal_controllers)

        def precision_all_sd = calculatePrecision(itests_all, itestd_all)
        def recall_all_sd = calculateRecall(itests_all, itestd_all)

        def precision_controllers_sd = calculatePrecision(itests_controllers, itestd_controllers)
        def recall_controllers_sd = calculateRecall(itests_controllers, itestd_controllers)

        println "\nTASK $id"
        println "(1) ITestS x IReal - no view analysis, all files"
        println "precision: $precision_all_sr"
        println "recall: $recall_all_sr"

        println "(2) ITestS x IReal - no view analysis, controllers"
        println "precision: $precision_controllers_sr"
        println "recall: $recall_controllers_sr"

        println "(3) ITestD x IReal - no view analysis, all files"
        println "precision: $precision_all_dr"
        println "recall: $recall_all_dr"

        println "(4) ITestD x IReal - no view analysis, controllers"
        println "precision: $precision_controllers_dr"
        println "recall: $recall_controllers_dr"

        println "(5) ITestS x ITestD - no view analysis, all files"
        println "precision: $precision_all_sd"
        println "recall: $recall_all_sd"

        println "(6) ITestS x ITestD - no view analysis, controllers"
        println "precision: $precision_controllers_sd"
        println "recall: $recall_controllers_sd"

        println "itests_all: ${itests_all}"
        println "itests_controllers: ${itests_controllers}"
        println "itestd_all: ${itestd_all}"
        println "itestd_controllers: ${itestd_controllers}"
        println "ireal_all: ${ireal_all}"
        println "ireal_controllers: ${ireal_controllers}"

        String[] line = [id, itests_all, ireal_all, precision_all_sr, recall_all_sr, itests_controllers, ireal_controllers, precision_controllers_sr, recall_controllers_sr,
         itestd_all, ireal_all, precision_all_dr, recall_all_dr, itestd_controllers, ireal_controllers, precision_controllers_dr, recall_controllers_dr,
         itests_all, itestd_all, precision_all_sd, recall_all_sd, itests_controllers, itestd_controllers, precision_controllers_sd, recall_controllers_sd]
        return line
    }

    static analyseTaskView(TaskInterface taskInterface){
        def id = taskInterface.id
        def itestd_all = taskInterface.ITestD.collect{"zhdk_leihs/app/${it}"} as Set
        def itests_all = taskInterface.ITestS_view.collect{"zhdk_leihs/app/${it}"} as Set
        def ireal_all = taskInterface.IReal.collect{"zhdk_leihs/app/${it}"} as Set

        def precision_all_sr = calculatePrecision(itests_all, ireal_all)
        def recall_all_sr = calculateRecall(itests_all, ireal_all)

        def itests_controllers = itests_all.findAll{ Util.isControllerFile(it) } as Set
        def ireal_controllers = ireal_all.findAll{ Util.isControllerFile(it) } as Set
        def precision_controllers_sr = calculatePrecision(itests_controllers, ireal_controllers)
        def recall_controllers_sr = calculateRecall(itests_controllers, ireal_controllers)

        def precision_all_dr = calculatePrecision(itestd_all, ireal_all)
        def recall_all_dr = calculateRecall(itestd_all, ireal_all)

        def itestd_controllers = itestd_all.findAll{ Util.isControllerFile(it) } as Set
        def precision_controllers_dr = calculatePrecision(itestd_controllers, ireal_controllers)
        def recall_controllers_dr = calculateRecall(itestd_controllers, ireal_controllers)

        def precision_all_sd = calculatePrecision(itests_all, itestd_all)
        def recall_all_sd = calculateRecall(itests_all, itestd_all)

        def precision_controllers_sd = calculatePrecision(itests_controllers, itestd_controllers)
        def recall_controllers_sd = calculateRecall(itests_controllers, itestd_controllers)

        println "\nTASK $id"
        println "(1) ITestS x IReal - view analysis, all files"
        println "precision: $precision_all_sr"
        println "recall: $recall_all_sr"

        println "(2) ITestS x IReal - view analysis, controllers"
        println "precision: $precision_controllers_sr"
        println "recall: $recall_controllers_sr"

        println "(3) ITestD x IReal - view analysis, all files"
        println "precision: $precision_all_dr"
        println "recall: $recall_all_dr"

        println "(4) ITestD x IReal - view analysis, controllers"
        println "precision: $precision_controllers_dr"
        println "recall: $recall_controllers_dr"

        println "(5) ITestS x ITestD - view analysis, all files"
        println "precision: $precision_all_sd"
        println "recall: $recall_all_sd"

        println "(6) ITestS x ITestD - view analysis, controllers"
        println "precision: $precision_controllers_sd"
        println "recall: $recall_controllers_sd"

        println "itests_all: ${itests_all}"
        println "itests_controllers: ${itests_controllers}"
        println "itestd_all: ${itestd_all}"
        println "itestd_controllers: ${itestd_controllers}"
        println "ireal_all: ${ireal_all}"
        println "ireal_controllers: ${ireal_controllers}"

        String[] line = [id, itests_all, ireal_all, precision_all_sr, recall_all_sr, itests_controllers, ireal_controllers, precision_controllers_sr, recall_controllers_sr,
                         itestd_all, ireal_all, precision_all_dr, recall_all_dr, itestd_controllers, ireal_controllers, precision_controllers_dr, recall_controllers_dr,
                         itests_all, itestd_all, precision_all_sd, recall_all_sd, itests_controllers, itestd_controllers, precision_controllers_sd, recall_controllers_sd]
        return line
    }

    static generateCSV_noviews(tasks){
        CSVWriter writer = new CSVWriter(new FileWriter("itestd${File.separator}interfaces_noviews.csv"))
        String[] header1 = ["", "ITestS x IReal", "", "", "", "", "", "", "", "ITestD x IReal", "", "", "", "", "", "", "",
                            "ITestS x ITestD", "", "", "", "", "", "", ""]
        String[] header2 = ["", "noviews_all", "", "", "", "noviews_controllers", "", "", "", "noviews_all", "", "",
                            "", "noviews_controllers", "", "", "", "noviews_all", "", "", "", "noviews_controllers", "", "", ""]
        String[] header3 = ["Task","ITestS","IReal", "Precision", "Recall", "ITestS", "IReal", "Precision", "Recall",
                            "ITestD", "IReal", "Precision", "Recall", "ITestD", "IReal", "Precision", "Recall",
                            "ITestS", "ITestD", "Precision", "Recall", "ITestS", "ITestD", "Precision", "Recall"]
        writer.writeNext(header1)
        writer.writeNext(header2)
        writer.writeNext(header3)

        tasks?.each{ task ->
            def r = analyseTaskNoView(task)
            writer.writeNext(r)
        }

        writer.close()
    }

    static generateCSV_views(tasks){
        CSVWriter writer = new CSVWriter(new FileWriter("itestd${File.separator}interfaces_views.csv"))
        String[] header1 = ["", "ITestS x IReal", "", "", "", "", "", "", "", "ITestD x IReal", "", "", "", "", "", "", "",
                            "ITestS x ITestD", "", "", "", "", "", "", ""]
        String[] header2 = ["", "views_all", "", "", "", "views_controllers", "", "", "", "views_all", "", "",
                            "", "views_controllers", "", "", "", "views_all", "", "", "", "views_controllers", "", "", ""]
        String[] header3 = ["Task","ITestS","IReal", "Precision", "Recall", "ITestS", "IReal", "Precision", "Recall",
                            "ITestD", "IReal", "Precision", "Recall", "ITestD", "IReal", "Precision", "Recall",
                            "ITestS", "ITestD", "Precision", "Recall", "ITestS", "ITestD", "Precision", "Recall"]
        writer.writeNext(header1)
        writer.writeNext(header2)
        writer.writeNext(header3)

        tasks?.each{ task ->
            def r = analyseTaskView(task)
            writer.writeNext(r)
        }

        writer.close()
    }

    private static TaskInterface configureT1(){
        def itestd_43122757 = ["controllers/application_controller.rb",
                               "controllers/authenticator/database_authentication_controller.rb",
                               "controllers/backend/backend_controller.rb",
                               "controllers/backend/inventory_pools_controller.rb",
                               "controllers/backend/settings_controller.rb",
                               "controllers/borrow/application_controller.rb",
                               "controllers/sessions_controller.rb",
                               "models/access_right.rb",
                               "models/authentication_system.rb",
                               "models/building.rb",
                               "models/category.rb",
                               "models/contract.rb",
                               "models/contract_line.rb",
                               "models/database_authentication.rb",
                               "models/group.rb",
                               "models/holiday.rb",
                               "models/inventory_pool.rb",
                               "models/item.rb",
                               "models/language.rb",
                               "models/location.rb",
                               "models/model.rb",
                               "models/model_group.rb",
                               "models/model_group_link.rb",
                               "models/order.rb",
                               "models/purpose.rb",
                               "models/role.rb",
                               "models/setting.rb",
                               "models/user.rb",
                               "views/application/_flash.html.haml",
                               "views/application/_footer.html.haml",
                               "views/application/_languages.html.haml",
                               "views/authenticator/database_authentication/login.html.haml",
                               "views/backend/inventory_pools/_inventory_pool.html.haml",
                               "views/backend/inventory_pools/index.html.haml",
                               "views/backend/settings/edit.html.haml",
                               "views/borrow/_current_order_basket.html.haml",
                               "views/borrow/_tabs.html.haml",
                               "views/borrow/_topbar.html.haml",
                               "views/borrow/application/_topbar_search.html.haml",
                               "views/borrow/application/start.html.haml",
                               "views/layouts/backend/_ipselection.html.haml",
                               "views/layouts/backend/_javascript.haml",
                               "views/layouts/backend/_navigation.html.haml",
                               "views/layouts/backend/_search.html.haml",
                               "views/layouts/backend/_tabs.html.haml",
                               "views/layouts/backend/_topbar.html.haml",
                               "views/layouts/frontend/_content_box_foot.html.erb",
                               "views/layouts/frontend/_content_box_head_fullwidth.html.erb",
                               "views/splash/_topbar.html.haml",
                               "views/splash/show.html.haml"] as Set
        def itests_noview_43122757 = ["controllers/application_controller.rb",
                                      "controllers/backend/settings_controller.rb",
                                      "controllers/borrow/application_controller.rb",
                                      "controllers/sessions_controller.rb", "models/language.rb", "models/mailer/user.rb",
                                      "models/setting.rb", "models/user.rb", "views/application/index.html.haml",
                                      "views/backend/settings/edit.html.haml", "views/sessions/access_denied.html.erb",
                                      "views/sessions/new.html.erb", "lib/persona.rb"] as Set
        def itests_view_43122757 = ["controllers/application_controller.rb", "controllers/backend/settings_controller.rb",
                                    "controllers/borrow/application_controller.rb", "controllers/sessions_controller.rb",
                                    "models/language.rb", "models/mailer/user.rb", "models/setting.rb", "models/user.rb",
                                    "views/application/index.html.haml", "views/backend/settings/edit.html.haml",
                                    "views/sessions/access_denied.html.erb", "views/sessions/new.html.erb", "lib/persona.rb",
                                    "views/layouts/frontend/_content_box_foot.html.erb",
                                    "views/layouts/frontend/_content_box_head_fullwidth.html.erb"] as Set
        def ireal_43122757 = ["controllers/application_controller.rb", "controllers/authenticator/hslu_authentication_controller.rb",
                              "controllers/backend/backend_controller.rb", "controllers/backend/contracts_controller.rb",
                              "controllers/backend/inventory_controller.rb", "controllers/backend/locations_controller.rb",
                              "controllers/backend/mails_controller.rb", "controllers/backend/models_controller.rb",
                              "controllers/backend/orders_controller.rb", "controllers/backend/settings_controller.rb",
                              "controllers/backend/templates_controller.rb", "controllers/backend/users_controller.rb",
                              "controllers/backend/visits_controller.rb", "helpers/backend/backend_helper.rb",
                              "models/mailer/order.rb", "models/mailer/user.rb", "models/notification.rb",
                              "models/setting.rb", "models/user.rb", "views/backend/backend/focused_search.html.haml",
                              "views/backend/contracts/index.html.haml", "views/backend/orders/index.html.haml",
                              "views/backend/settings/edit.html.haml", "views/backend/visits/index.html.haml",
                              "views/layouts/backend/_javascript.haml", "views/layouts/backend/_navigation.html.haml",
                              "views/layouts/frontend.html.haml", "views/mailer/order/approved.html.erb",
                              "views/mailer/order/changed.html.erb", "views/mailer/order/received.html.erb",
                              "views/mailer/order/rejected.html.erb", "views/mailer/order/submitted.html.erb",
                              "views/mailer/user/deadline_soon_reminder.html.erb", "views/mailer/user/remind.html.erb"] as Set
       new TaskInterface(id:"43122757", ITestD:itestd_43122757, ITestS_noview:itests_noview_43122757,
               ITestS_view: itests_view_43122757, IReal:ireal_43122757)
    }

    private static TaskInterface configureT2(){
        def itestd_52700935 = ["controllers/application_controller.rb",
                               "controllers/authenticator/database_authentication_controller.rb",
                               "controllers/backend/backend_controller.rb",
                               "controllers/backend/inventory_pools_controller.rb",
                               "controllers/borrow/application_controller.rb",
                               "controllers/borrow/orders_controller.rb",
                               "controllers/borrow/returns_controller.rb",
                               "controllers/borrow/to_pick_up_controller.rb",
                               "controllers/sessions_controller.rb",
                               "models/access_right.rb",
                               "models/authentication_system.rb",
                               "models/category.rb",
                               "models/contract.rb",
                               "models/contract_line.rb",
                               "models/database_authentication.rb",
                               "models/group.rb",
                               "models/holiday.rb",
                               "models/inventory_pool.rb",
                               "models/item.rb",
                               "models/language.rb",
                               "models/model.rb",
                               "models/model_group.rb",
                               "models/model_group_link.rb",
                               "models/order.rb",
                               "models/order_line.rb",
                               "models/purpose.rb",
                               "models/role.rb",
                               "models/user.rb",
                               "models/visit.rb",
                               "views/application/_flash.html.haml",
                               "views/application/_footer.html.haml",
                               "views/application/_languages.html.haml",
                               "views/authenticator/database_authentication/login.html.haml",
                               "views/backend/inventory_pools/_inventory_pool.html.haml",
                               "views/backend/inventory_pools/index.html.haml",
                               "views/borrow/_current_order_basket.html.haml",
                               "views/borrow/_tabs.html.haml, views/borrow/_topbar.html.haml",
                               "views/borrow/application/_topbar_search.html.haml",
                               "views/borrow/application/start.html.haml",
                               "views/borrow/orders/index.html.haml",
                               "views/borrow/orders/submitted/_merged_lines.html.haml",
                               "views/borrow/returns/_line.html.haml",
                               "views/borrow/returns/index.html.haml",
                               "views/borrow/to_pick_up/_merged_lines.html.haml",
                               "views/borrow/to_pick_up/index.html.haml",
                               "views/layouts/backend/_ipselection.html.haml",
                               "views/layouts/backend/_javascript.haml",
                               "views/layouts/backend/_navigation.html.haml",
                               "views/layouts/backend/_search.html.haml",
                               "views/layouts/backend/_tabs.html.haml",
                               "views/layouts/backend/_topbar.html.haml",
                               "views/layouts/frontend/_content_box_foot.html.erb",
                               "views/layouts/frontend/_content_box_head_fullwidth.html.erb",
                               "views/splash/_topbar.html.haml",
                               "views/splash/show.html.haml"] as Set
        def itests_noview_52700935 = ["controllers/application_controller.rb", "controllers/backend/take_back_controller.rb",
                               "controllers/borrow/application_controller.rb", "controllers/borrow/returns_controller.rb",
                               "controllers/borrow/to_pick_up_controller.rb", "controllers/sessions_controller.rb",
                               "models/access_right.rb", "models/address.rb", "models/availability/model.rb",
                               "models/building.rb", "models/contract.rb", "models/document.rb", "models/group.rb",
                               "models/inventory_pool.rb", "models/item.rb", "models/item_line.rb", "models/language.rb",
                               "models/location.rb", "models/mailer/user.rb", "models/model.rb", "models/model_group.rb",
                               "models/option.rb", "models/option_line.rb", "models/order.rb", "models/order_line.rb",
                               "models/purpose.rb", "models/role.rb", "models/user.rb", "views/application/index.html.haml",
                               "views/backend/take_back/show.html.haml", "views/borrow/application/start.html.haml",
                               "views/borrow/returns/index.html.haml", "views/borrow/to_pick_up/index.html.haml",
                               "views/sessions/access_denied.html.erb", "views/sessions/new.html.erb", "lib/persona.rb"] as Set
        def itests_view_52700935 = ["controllers/application_controller.rb", "controllers/backend/take_back_controller.rb",
                                    "controllers/borrow/application_controller.rb", "controllers/borrow/returns_controller.rb",
                                    "controllers/borrow/to_pick_up_controller.rb", "controllers/sessions_controller.rb",
                                    "models/access_right.rb", "models/address.rb", "models/availability/model.rb",
                                    "models/building.rb", "models/contract.rb", "models/document.rb",
                                    "models/group.rb", "models/inventory_pool.rb",
                                    "models/item.rb", "models/item_line.rb",
                                    "models/language.rb", "models/location.rb",
                                    "models/mailer/user.rb", "models/model.rb",
                                    "models/model_group.rb", "models/option.rb",
                                    "models/option_line.rb", "models/order.rb",
                                    "models/order_line.rb", "models/purpose.rb",
                                    "models/role.rb", "models/user.rb",
                                    "views/application/index.html.haml",
                                    "views/backend/take_back/show.html.haml",
                                    "views/borrow/application/start.html.haml",
                                    "views/borrow/returns/index.html.haml",
                                    "views/borrow/to_pick_up/index.html.haml",
                                    "views/sessions/access_denied.html.erb",
                                    "views/sessions/new.html.erb",
                                    "lib/persona.rb", "views/borrow/_current_order_basket.html.haml",
                                    "views/borrow/_tabs.html.haml", "views/borrow/returns/_line.html.haml",
                                    "views/borrow/to_pick_up/_merged_lines.html.haml",
                                    "views/layouts/frontend/_content_box_foot.html.erb",
                                    "views/layouts/frontend/_content_box_head_fullwidth.html.erb"] as Set

        def ireal_52700935 = ["controllers/borrow/orders_controller.rb", "controllers/borrow/to_pick_up_controller.rb",
                              "models/line_modules/grouped_and_merged_lines.rb", "views/borrow/_topbar.html.haml",
                              "views/borrow/orders/current.html.haml", "views/borrow/orders/index.html.haml",
                              "views/borrow/orders/submitted/_merged_lines.html.haml",
                              "views/borrow/orders/unsubmitted/_merged_lines.html.haml", "views/borrow/returns/_line.html.haml",
                              "views/borrow/returns/index.html.haml", "views/borrow/to_pick_up/_merged_lines.html.haml",
                              "views/borrow/to_pick_up/index.html.haml"] as Set
        new TaskInterface(id:"52700935", ITestD:itestd_52700935, ITestS_noview:itests_noview_52700935,
                ITestS_view: itests_view_52700935, IReal:ireal_52700935)
    }

    private static TaskInterface configureT3(){
        def itestd_52700989 = ["controllers/application_controller.rb",
                               "controllers/authenticator/database_authentication_controller.rb",
                               "controllers/borrow/application_controller.rb",
                               "controllers/borrow/inventory_pools_controller.rb",
                               "controllers/sessions_controller.rb",
                               "models/access_right.rb",
                               "models/authentication_system.rb",
                               "models/category.rb",
                               "models/contract_line.rb",
                               "models/database_authentication.rb",
                               "models/inventory_pool.rb",
                               "models/language.rb",
                               "models/model_group.rb",
                               "models/order.rb",
                               "models/role.rb",
                               "models/user.rb",
                               "views/application/_flash.html.haml",
                               "views/application/_footer.html.haml",
                               "views/application/_languages.html.haml",
                               "views/authenticator/database_authentication/login.html.haml",
                               "views/borrow/_current_order_basket.html.haml",
                               "views/borrow/_tabs.html.haml",
                               "views/borrow/_topbar.html.haml",
                               "views/borrow/application/_topbar_search.html.haml",
                               "views/borrow/application/start.html.haml",
                               "views/borrow/inventory_pools/index.html.haml",
                               "views/splash/_topbar.html.haml",
                               "views/splash/show.html.haml"] as Set
        def itests_noview_52700989 = ["controllers/application_controller.rb", "controllers/borrow/application_controller.rb",
                               "controllers/borrow/inventory_pools_controller.rb", "controllers/sessions_controller.rb",
                               "models/language.rb", "models/mailer/user.rb", "models/user.rb", "views/application/index.html.haml",
                               "views/borrow/application/start.html.haml", "views/borrow/inventory_pools/index.html.haml",
                               "views/sessions/access_denied.html.erb", "views/sessions/new.html.erb", "lib/persona.rb"] as Set
        def itests_view_52700989 = ["controllers/application_controller.rb", "controllers/borrow/application_controller.rb",
                                    "controllers/borrow/inventory_pools_controller.rb", "controllers/sessions_controller.rb",
                                    "models/availability/inventory_pool.rb", "models/inventory_pool.rb", "models/language.rb",
                                    "models/mailer/user.rb", "models/user.rb", "views/application/index.html.haml",
                                    "views/borrow/application/start.html.haml", "views/borrow/inventory_pools/index.html.haml",
                                    "views/sessions/access_denied.html.erb", "views/sessions/new.html.erb", "lib/persona.rb",
                                    "views/borrow/_current_order_basket.html.haml", "views/borrow/_tabs.html.haml",
                                    "views/layouts/frontend/_content_box_foot.html.erb",
                                    "views/layouts/frontend/_content_box_head_fullwidth.html.erb"] as Set
        def ireal_52700989 = ["controllers/borrow/inventory_pools_controller.rb", "views/borrow/inventory_pools/index.html.haml",
                              "views/borrow/models/show/_compatibles.html.haml"] as Set
        new TaskInterface(id:"52700989", ITestD:itestd_52700989, ITestS_noview:itests_noview_52700989,
                ITestS_view: itests_view_52700989, IReal:ireal_52700989)
    }

    private static TaskInterface configureT4(){
        def itestd_53390651 = ["controllers/application_controller.rb",
                               "controllers/authenticator/database_authentication_controller.rb",
                               "controllers/backend/backend_controller.rb",
                               "controllers/backend/inventory_pools_controller.rb",
                               "controllers/borrow/application_controller.rb",
                               "controllers/borrow/orders_controller.rb",
                               "controllers/sessions_controller.rb",
                               "models/access_right.rb",
                               "models/authentication_system.rb",
                               "models/category.rb",
                               "models/database_authentication.rb",
                               "models/holiday.rb",
                               "models/inventory_pool.rb",
                               "models/item.rb",
                               "models/language.rb",
                               "models/model.rb",
                               "models/model_group.rb",
                               "models/order.rb",
                               "models/order_line.rb",
                               "models/partitions_with_general.rb",
                               "models/purpose.rb",
                               "models/role.rb",
                               "models/running_line.rb",
                               "models/user.rb",
                               "models/workday.rb",
                               "views/application/_flash.html.haml",
                               "views/application/_footer.html.haml",
                               "views/application/_languages.html.haml",
                               "views/authenticator/database_authentication/login.html.haml",
                               "views/backend/inventory_pools/_bar_chart.html.haml",
                               "views/backend/inventory_pools/show.html.haml",
                               "views/borrow/_current_order_basket.html.haml",
                               "views/borrow/_tabs.html.haml",
                               "views/borrow/_topbar.html.haml",
                               "views/borrow/application/_topbar_search.html.haml",
                               "views/borrow/application/start.html.haml",
                               "views/borrow/orders/current.html.haml",
                               "views/borrow/orders/unsubmitted/_merged_lines.html.haml",
                               "views/layouts/backend/_content_box_head_fullwidth_round.html.erb",
                               "views/layouts/backend/_ipselection.html.haml",
                               "views/layouts/backend/_javascript.haml",
                               "views/layouts/backend/_navigation.html.haml",
                               "views/layouts/backend/_search.html.haml",
                               "views/layouts/backend/_tabs.html.haml",
                               "views/layouts/backend/_topbar.html.haml",
                               "views/layouts/frontend/_content_box_foot.html.erb",
                               "views/layouts/frontend/_content_box_head_fullwidth.html.erb",
                               "views/splash/_topbar.html.haml",
                               "views/splash/show.html.haml"] as Set
        def itests_noview_53390651 = ["controllers/application_controller.rb", "controllers/borrow/application_controller.rb",
         "controllers/borrow/orders_controller.rb", "controllers/sessions_controller.rb", "models/availability/document_line.rb",
         "models/availability/inventory_pool.rb", "models/contract.rb", "models/document.rb", "models/inventory_pool.rb",
         "models/language.rb", "models/mailer/user.rb", "models/model.rb", "models/order.rb", "models/purpose.rb",
         "models/user.rb", "models/visit.rb", "views/application/index.html.haml", "views/borrow/application/start.html.haml",
         "views/borrow/orders/_merged_lines.html.haml", "views/borrow/orders/current.html.haml",
         "views/borrow/orders/index.html.haml", "views/borrow/orders/submitted/_merged_lines.html.haml",
         "views/borrow/orders/unsubmitted/_merged_lines.html.haml", "views/sessions/access_denied.html.erb",
         "views/sessions/new.html.erb", "lib/persona.rb"] as Set
        def itests_view_53390651 = ["controllers/application_controller.rb",
                                    "controllers/borrow/application_controller.rb",
                                    "controllers/borrow/orders_controller.rb",
                                    "controllers/sessions_controller.rb",
                                    "models/availability/document_line.rb",
                                    "models/availability/inventory_pool.rb",
                                    "models/contract.rb, models/document.rb",
                                    "models/inventory_pool.rb",
                                    "models/language.rb",
                                    "models/mailer/user.rb",
                                    "models/model.rb",
                                    "models/order.rb",
                                    "models/purpose.rb",
                                    "models/user.rb",
                                    "models/visit.rb",
                                    "views/application/index.html.haml",
                                    "views/borrow/application/start.html.haml",
                                    "views/borrow/orders/_merged_lines.html.haml",
                                    "views/borrow/orders/current.html.haml",
                                    "views/borrow/orders/index.html.haml",
                                    "views/borrow/orders/submitted/_merged_lines.html.haml",
                                    "views/borrow/orders/unsubmitted/_merged_lines.html.haml",
                                    "views/sessions/access_denied.html.erb",
                                    "views/sessions/new.html.erb",
                                    "lib/persona.rb",
                                    "views/borrow/_current_order_basket.html.haml",
                                    "views/borrow/_tabs.html.haml",
                                    "views/layouts/frontend/_content_box_foot.html.erb",
                                    "views/layouts/frontend/_content_box_head_fullwidth.html.erb"] as Set
        def ireal_53390651 = ["controllers/borrow/application_controller.rb", "controllers/borrow/orders_controller.rb",
         "views/borrow/orders/current.html.haml"] as Set
        new TaskInterface(id:"53390651", ITestD:itestd_53390651, ITestS_noview:itests_noview_53390651,
                ITestS_view: itests_view_53390651, IReal:ireal_53390651)
    }

    private static TaskInterface configureT5(){
        def itestd_53461159 = ["controllers/application_controller.rb",
                               "controllers/authenticator/database_authentication_controller.rb",
                               "controllers/backend/backend_controller.rb",
                               "controllers/backend/inventory_pools_controller.rb",
                               "controllers/borrow/application_controller.rb",
                               "controllers/borrow/orders_controller.rb",
                               "controllers/borrow/returns_controller.rb",
                               "controllers/borrow/to_pick_up_controller.rb",
                               "controllers/sessions_controller.rb",
                               "models/access_right.rb",
                               "models/authentication_system.rb",
                               "models/category.rb",
                               "models/contract.rb",
                               "models/contract_line.rb",
                               "models/database_authentication.rb",
                               "models/inventory_pool.rb",
                               "models/item.rb",
                               "models/language.rb",
                               "models/model.rb",
                               "models/model_group.rb",
                               "models/order.rb",
                               "models/order_line.rb",
                               "models/role.rb",
                               "models/user.rb",
                               "models/visit.rb",
                               "views/application/_flash.html.haml",
                               "views/application/_footer.html.haml",
                               "views/application/_languages.html.haml",
                               "views/authenticator/database_authentication/login.html.haml",
                               "views/backend/inventory_pools/_inventory_pool.html.haml",
                               "views/backend/inventory_pools/index.html.haml",
                               "views/borrow/_current_order_basket.html.haml",
                               "views/borrow/_tabs.html.haml",
                               "views/borrow/_topbar.html.haml",
                               "views/borrow/application/_topbar_search.html.haml",
                               "views/borrow/application/start.html.haml",
                               "views/borrow/orders/index.html.haml",
                               "views/borrow/orders/submitted/_merged_lines.html.haml",
                               "views/borrow/returns/_line.html.haml",
                               "views/borrow/returns/index.html.haml",
                               "views/borrow/to_pick_up/_merged_lines.html.haml",
                               "views/borrow/to_pick_up/index.html.haml",
                               "views/layouts/backend/_ipselection.html.haml",
                               "views/layouts/backend/_javascript.haml",
                               "views/layouts/backend/_navigation.html.haml",
                               "views/layouts/backend/_search.html.haml",
                               "views/layouts/backend/_tabs.html.haml",
                               "views/layouts/backend/_topbar.html.haml",
                               "views/layouts/frontend/_content_box_foot.html.erb",
                               "views/layouts/frontend/_content_box_head_fullwidth.html.erb",
                               "views/splash/_topbar.html.haml",
                               "views/splash/show.html.haml"] as Set
        def itests_noview_53461159 = ["controllers/application_controller.rb", "controllers/backend/take_back_controller.rb",
                               "controllers/borrow/application_controller.rb", "controllers/borrow/returns_controller.rb",
                               "controllers/borrow/to_pick_up_controller.rb", "controllers/sessions_controller.rb",
                               "models/access_right.rb", "models/address.rb", "models/availability/model.rb",
                               "models/building.rb", "models/contract.rb", "models/document.rb", "models/group.rb",
                               "models/inventory_pool.rb", "models/item.rb", "models/item_line.rb", "models/language.rb",
                               "models/location.rb", "models/mailer/user.rb", "models/model.rb", "models/model_group.rb",
                               "models/option.rb", "models/option_line.rb", "models/order.rb", "models/order_line.rb",
                               "models/purpose.rb", "models/role.rb", "models/user.rb", "views/application/index.html.haml",
                               "views/backend/take_back/show.html.haml", "views/borrow/application/start.html.haml",
                               "views/borrow/returns/index.html.haml", "views/borrow/to_pick_up/index.html.haml",
                               "views/sessions/access_denied.html.erb", "views/sessions/new.html.erb", "lib/persona.rb"] as Set
        def itests_view_53461159 = ["controllers/application_controller.rb",
                                    "controllers/backend/take_back_controller.rb",
                                    "controllers/borrow/application_controller.rb",
                                    "controllers/borrow/returns_controller.rb",
                                    "controllers/borrow/to_pick_up_controller.rb",
                                    "controllers/sessions_controller.rb",
                                    "models/access_right.rb",
                                    "models/address.rb",
                                    "models/availability/model.rb",
                                    "models/building.rb",
                                    "models/contract.rb",
                                    "models/document.rb",
                                    "models/group.rb",
                                    "models/inventory_pool.rb",
                                    "models/item.rb",
                                    "models/item_line.rb",
                                    "models/language.rb",
                                    "models/location.rb",
                                    "models/mailer/user.rb",
                                    "models/model.rb",
                                    "models/model_group.rb",
                                    "models/option.rb",
                                    "models/option_line.rb",
                                    "models/order.rb",
                                    "models/order_line.rb",
                                    "models/purpose.rb",
                                    "models/role.rb",
                                    "models/user.rb",
                                    "views/application/index.html.haml",
                                    "views/backend/take_back/show.html.haml",
                                    "views/borrow/application/start.html.haml",
                                    "views/borrow/returns/index.html.haml",
                                    "views/borrow/to_pick_up/index.html.haml",
                                    "views/sessions/access_denied.html.erb",
                                    "views/sessions/new.html.erb",
                                    "lib/persona.rb",
                                    "views/borrow/_current_order_basket.html.haml",
                                    "views/borrow/_tabs.html.haml", "views/borrow/returns/_line.html.haml",
                                    "views/borrow/to_pick_up/_merged_lines.html.haml",
                                    "views/layouts/frontend/_content_box_foot.html.erb",
                                    "views/layouts/frontend/_content_box_head_fullwidth.html.erb"] as Set

        def ireal_53461159 = ["controllers/borrow/orders_controller.rb", "controllers/borrow/to_pick_up_controller.rb",
                              "models/line_modules/grouped_and_merged_lines.rb", "views/borrow/_topbar.html.haml",
                              "views/borrow/orders/current.html.haml", "views/borrow/orders/index.html.haml",
                              "views/borrow/orders/submitted/_merged_lines.html.haml",
                              "views/borrow/orders/unsubmitted/_merged_lines.html.haml",
                              "views/borrow/returns/_line.html.haml", "views/borrow/returns/index.html.haml",
                              "views/borrow/to_pick_up/_merged_lines.html.haml", "views/borrow/to_pick_up/index.html.haml"] as Set
        new TaskInterface(id:"53461159", ITestD:itestd_53461159, ITestS_noview:itests_noview_53461159,
                ITestS_view: itests_view_53461159, IReal:ireal_53461159)
    }

    private static TaskInterface configureT6(){
        def itestd_55072594 = ["controllers/application_controller.rb",
                               "controllers/authenticator/database_authentication_controller.rb",
                               "controllers/borrow/application_controller.rb",
                               "controllers/borrow/models_controller.rb",
                               "controllers/sessions_controller.rb",
                               "models/access_right.rb",
                               "models/authentication_system.rb",
                               "models/category.rb",
                               "models/database_authentication.rb",
                               "models/inventory_pool.rb",
                               "models/language.rb",
                               "models/model.rb",
                               "models/model_group.rb",
                               "models/order.rb",
                               "models/role.rb",
                               "models/template.rb",
                               "models/user.rb",
                               "views/application/_flash.html.haml",
                               "views/application/_footer.html.haml",
                               "views/application/_languages.html.haml",
                               "views/authenticator/database_authentication/login.html.haml",
                               "views/borrow/_current_order_basket.html.haml",
                               "views/borrow/_global_javascript_injection.html.haml",
                               "views/borrow/_tabs.html.haml",
                               "views/borrow/_topbar.html.haml",
                               "views/borrow/application/_topbar_search.html.haml",
                               "views/borrow/application/start.html.haml",
                               "views/borrow/models/index.html.haml",
                               "views/borrow/models/index/_explorative_search.html.haml",
                               "views/borrow/models/index/_ip_selector.html.haml",
                               "views/borrow/models/index/_line.html.haml",
                               "views/borrow/models/index/_list.html.haml",
                               "views/borrow/models/index/_period.html.haml",
                               "views/borrow/models/index/_reset.html.haml",
                               "views/borrow/models/index/_search.html.haml",
                               "views/borrow/models/index/_sorting.html.haml",
                               "views/borrow/models/index/explorative_search/_child_category.html.haml",
                               "views/borrow/models/index/explorative_search/_grand_child_category.html.haml",
                               "views/splash/_topbar.html.haml",
                               "views/splash/show.html.haml"] as Set
        def itests_noview_55072594 = ["controllers/application_controller.rb", "controllers/borrow/application_controller.rb",
                               "controllers/borrow/models_controller.rb", "controllers/sessions_controller.rb",
                               "models/category.rb", "models/language.rb", "models/mailer/user.rb", "models/user.rb",
                               "views/application/index.html.haml", "views/borrow/models/index.html.haml",
                               "views/borrow/models/index/_explorative_search.html.haml",
                               "views/borrow/models/index/_ip_selector.html.haml",
                               "views/borrow/models/index/_line.html.haml",
                               "views/borrow/models/index/_list.html.haml",
                               "views/borrow/models/index/_period.html.haml",
                               "views/borrow/models/index/_reset.html.haml",
                               "views/borrow/models/index/_search.html.haml",
                               "views/borrow/models/index/_sorting.html.haml",
                               "views/borrow/models/index/explorative_search/_child_category.html.haml",
                               "views/borrow/models/index/explorative_search/_grand_child_category.html.haml",
                               "views/sessions/access_denied.html.erb", "views/sessions/new.html.erb", "lib/persona.rb"] as Set
        def itests_view_55072594 = ["controllers/application_controller.rb",
                                    "controllers/borrow/application_controller.rb",
                                    "controllers/borrow/models_controller.rb",
                                    "controllers/sessions_controller.rb",
                                    "models/availability/inventory_pool.rb",
                                    "models/category.rb",
                                    "models/inventory_pool.rb",
                                    "models/language.rb",
                                    "models/mailer/user.rb",
                                    "models/user.rb",
                                    "views/application/index.html.haml",
                                    "views/borrow/models/index.html.haml",
                                    "views/borrow/models/index/_explorative_search.html.haml",
                                    "views/borrow/models/index/_ip_selector.html.haml",
                                    "views/borrow/models/index/_line.html.haml",
                                    "views/borrow/models/index/_list.html.haml",
                                    "views/borrow/models/index/_period.html.haml",
                                    "views/borrow/models/index/_reset.html.haml",
                                    "views/borrow/models/index/_search.html.haml",
                                    "views/borrow/models/index/_sorting.html.haml",
                                    "views/borrow/models/index/explorative_search/_child_category.html.haml",
                                    "views/borrow/models/index/explorative_search/_grand_child_category.html.haml",
                                    "views/sessions/access_denied.html.erb",
                                    "views/sessions/new.html.erb",
                                    "lib/persona.rb",
                                    "views/borrow/_current_order_basket.html.haml",
                                    "views/borrow/_tabs.html.haml",
                                    "views/layouts/frontend/_content_box_foot.html.erb",
                                    "views/layouts/frontend/_content_box_head_fullwidth.html.erb"] as Set

        def ireal_55072594 = ["controllers/application_controller.rb"] as Set
        new TaskInterface(id:"55072594", ITestD:itestd_55072594, ITestS_noview:itests_noview_55072594,
                ITestS_view: itests_view_55072594, IReal:ireal_55072594)
    }

    private static TaskInterface configureT7(){
        def itestd_62647952= ["controllers/application_controller.rb",
                              "controllers/authenticator/database_authentication_controller.rb",
                              "controllers/borrow/application_controller.rb",
                              "controllers/borrow/inventory_pools_controller.rb",
                              "controllers/sessions_controller.rb",
                              "models/access_right.rb",
                              "models/authentication_system.rb",
                              "models/category.rb",
                              "models/contract.rb",
                              "models/database_authentication.rb",
                              "models/inventory_pool.rb",
                              "models/language.rb",
                              "models/model_group.rb",
                              "models/role.rb",
                              "models/template.rb",
                              "models/user.rb",
                              "views/application/_flash.html.haml",
                              "views/application/_footer.html.haml",
                              "views/application/_logo.html.haml",
                              "views/application/_topbar.html.haml",
                              "views/application/root.html.haml",
                              "views/authenticator/database_authentication/login.html.haml",
                              "views/borrow/_basket.html.haml",
                              "views/borrow/_global_javascript_injection.html.haml",
                              "views/borrow/_tabs.html.haml",
                              "views/borrow/_topbar.html.haml",
                              "views/borrow/application/_topbar_search.html.haml",
                              "views/borrow/application/root.html.haml",
                              "views/borrow/inventory_pools/index.html.haml"] as Set
        def itests_noview_62647952 = ["controllers/admin/application_controller.rb", "controllers/application_controller.rb",
                               "controllers/borrow/application_controller.rb",
                               "controllers/borrow/inventory_pools_controller.rb",
                               "controllers/manage/application_controller.rb",
                               "controllers/sessions_controller.rb",
                               "models/language.rb", "models/mailer/user.rb", "models/user.rb", "views/application/root.html.haml",
                               "views/borrow/application/root.html.haml", "views/borrow/inventory_pools/index.html.haml",
                               "views/sessions/access_denied.html.erb", "views/sessions/new.html.erb", "lib/persona.rb"] as Set
        def itests_view_62647952 = ["controllers/admin/application_controller.rb",
                                    "controllers/application_controller.rb",
                                    "controllers/borrow/application_controller.rb",
                                    "controllers/borrow/inventory_pools_controller.rb",
                                    "controllers/manage/application_controller.rb",
                                    "controllers/sessions_controller.rb",
                                    "models/availability/inventory_pool.rb",
                                    "models/inventory_pool.rb",
                                    "models/language.rb",
                                    "models/mailer/user.rb",
                                    "models/user.rb",
                                    "views/application/root.html.haml",
                                    "views/borrow/application/root.html.haml",
                                    "views/borrow/inventory_pools/index.html.haml",
                                    "views/sessions/access_denied.html.erb",
                                    "views/sessions/new.html.erb",
                                    "lib/persona.rb",
                                    "views/borrow/_basket.html.haml",
                                    "views/borrow/_tabs.html.haml"] as Set
        def ireal_62647952 = ["controllers/borrow/inventory_pools_controller.rb", "models/user.rb",
                              "views/borrow/inventory_pools/index.html.haml"] as Set
        new TaskInterface(id:"62647952", ITestD:itestd_62647952, ITestS_noview:itests_noview_62647952,
                ITestS_view: itests_view_62647952, IReal:ireal_62647952)
    }

    static void main (String[] args){
        def task_43122757 = configureT1()
        def task_52700935 = configureT2()
        def task_52700989 = configureT3()
        def task_53390651 = configureT4()
        def task_53461159 = configureT5()
        def task_55072594 = configureT6()
        def task_62647952 = configureT7()
        def tasks = [task_43122757, task_52700935, task_52700989, task_53390651, task_53461159, task_55072594, task_62647952]
        generateCSV_noviews(tasks)
        generateCSV_views(tasks)
    }

}
