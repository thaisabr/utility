package study1.investigating_samples

import br.ufpe.cin.tan.util.CsvUtil
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

/* Script criado para gerar tabela de resultados organizados por projeto, de acordo com diferentes configurações, conforme solicitado pelos revisores do artigo.
* TestI - todas as configurações
* RandomI - NF e CF
* TextI - NF e CF
* Isso é para a amostra maior*/
class EvaluateResultsPerProjectLargerSample {

    String outputTestiAddedFile
    String outputTestiAddedControllerFile
    String outputTestiAddedWhenFile
    String outputTestiAddedWhenControllerFile
    String outputTestiAllFile
    String outputTestiAllControllerFile
    String outputTestiAllWhenFile
    String outputTestiAllWhenControllerFile
    String outputRandomiFile
    String outputRandomiControllerFile
    String outputTextiFile
    String outputTextiControllerFile

    Map testiAddedResult
    Map testiAddedControllerResult
    Map testiAddedWhenResult
    Map testiAddedWhenControllerResult
    Map testiAllResult
    Map testiAllControllerResult
    Map testiAllWhenResult
    Map testiAllWhenControllerResult
    Map randomiResult
    Map randomiControllerResult
    Map textiResult
    Map textiControllerResult

    final int PROJECT_INDEX = 0
    final int TASK_INDEX = 1
    final int PRECISION_INDEX = 2
    final int RECALL_INDEX = 3

    List projects

    def results
    String outputFolder
    String outputFile

    EvaluateResultsPerProjectLargerSample(){
        outputFolder = "tasks${File.separator}temp${File.separator}cucumber_sample_task_interfaces_463"
        outputFile = "${outputFolder}${File.separator}resultPerProject.csv"
        results = []
        configureOutputFiles()
        importResults()
        configureProjects()
    }

    static void main(String[] args){
        def evaluator = new EvaluateResultsPerProjectLargerSample()
        evaluator.organizeResultsPerProject()
        println "results: ${evaluator.results.size()}"
        evaluator.results.each{
            println it
        }
        evaluator.exportResult()
    }

    private static importFileResults(String file){
        println "file: $file"
        def tasks = CsvUtil.read(file)
        tasks.remove(0)
        tasks//tasks.subList(0, tasks.size()-15)
    }

    private static formatProjectName(project){
        def name = project.toLowerCase()
        if(name ==~ /.+_\d+/ ){
            def index = name.lastIndexOf("_")
            name = name.substring(0, index)
        }
        if(name.contains("_")) {
            def index = name.indexOf("_")
            name = name.substring(index+1)
        }
        name
    }

    private static generateStatistics(double[] values){
        def statistics = new DescriptiveStatistics(values)
        [mean: statistics.mean, median: statistics.getPercentile(50.0), sd: statistics.standardDeviation]
    }

    private configureOutputFiles(){
        outputTestiAddedFile = "${outputFolder}${File.separator}itest_added.csv"
        outputTestiAddedControllerFile = "${outputFolder}${File.separator}itest_added_controller.csv"
        outputTestiAddedWhenFile = "${outputFolder}${File.separator}itest_added_when.csv"
        outputTestiAddedWhenControllerFile = "${outputFolder}${File.separator}itest_added_when_controller.csv"
        outputTestiAllFile = "${outputFolder}${File.separator}itest_all.csv"
        outputTestiAllControllerFile = "${outputFolder}${File.separator}itest_all_controller.csv"
        outputTestiAllWhenFile = "${outputFolder}${File.separator}itest_all_when.csv"
        outputTestiAllWhenControllerFile = "${outputFolder}${File.separator}itest_all_when_controller.csv"
        outputRandomiFile = "${outputFolder}${File.separator}irandom.csv"
        outputRandomiControllerFile = "${outputFolder}${File.separator}irandom-controller.csv"
        outputTextiFile = "${outputFolder}${File.separator}itext.csv"
        outputTextiControllerFile = "${outputFolder}${File.separator}itext_controller.csv"
    }

    private importResults(){
        testiAddedResult = importFileResults(outputTestiAddedFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[PRECISION_INDEX] as double,
                            recall:it[RECALL_INDEX] as double] }?.groupBy{ it.project }
        println "testiAddedResult: ${testiAddedResult.size()}"

        testiAddedControllerResult = importFileResults(outputTestiAddedControllerFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[PRECISION_INDEX] as double,
                            recall:it[RECALL_INDEX] as double] }?.groupBy{ it.project }
        println "testiAddedControllerResult: ${testiAddedControllerResult.size()}"

        testiAddedWhenResult = importFileResults(outputTestiAddedWhenFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[PRECISION_INDEX] as double,
                            recall:it[RECALL_INDEX] as double] }?.groupBy{ it.project }
        println "testiAddedWhenResult: ${testiAddedWhenResult.size()}"

        testiAddedWhenControllerResult = importFileResults(outputTestiAddedWhenControllerFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]),
                            id:it[TASK_INDEX] as int, precision:it[PRECISION_INDEX] as double,
                            recall:it[RECALL_INDEX] as double] }?.groupBy{ it.project }
        println "testiAddedWhenControllerResult: ${testiAddedWhenControllerResult.size()}"

        testiAllResult = importFileResults(outputTestiAllFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[PRECISION_INDEX] as double,
                            recall:it[RECALL_INDEX] as double] }?.groupBy{ it.project }
        println "testiAllResult: ${testiAllResult.size()}"

        testiAllControllerResult = importFileResults(outputTestiAllControllerFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[PRECISION_INDEX] as double,
                            recall:it[RECALL_INDEX] as double] }?.groupBy{ it.project }
        println "testiAllControllerResult: ${testiAllControllerResult.size()}"

        testiAllWhenResult = importFileResults(outputTestiAllWhenFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[PRECISION_INDEX] as double,
                            recall:it[RECALL_INDEX] as double] }?.groupBy{ it.project }
        println "testiAllWhenResult: ${testiAllWhenResult.size()}"

        testiAllWhenControllerResult = importFileResults(outputTestiAllWhenControllerFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[PRECISION_INDEX] as double,
                            recall:it[RECALL_INDEX] as double] }?.groupBy{ it.project }
        println "testiAllWhenControllerResult: ${testiAllWhenControllerResult.size()}"

        randomiResult = importFileResults(outputRandomiFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[PRECISION_INDEX] as double,
                            recall:it[RECALL_INDEX] as double] }?.groupBy{ it.project }
        println "randomiResult: ${randomiResult.size()}"

        randomiControllerResult = importFileResults(outputRandomiControllerFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[PRECISION_INDEX] as double,
                            recall:it[RECALL_INDEX] as double] }?.groupBy{ it.project }
        println "randomiControllerResult: ${randomiControllerResult.size()}"

        textiResult = importFileResults(outputTextiFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[PRECISION_INDEX] as double,
                            recall:it[RECALL_INDEX] as double] }?.groupBy{ it.project }
        println "textiResult: ${textiResult.size()}"

        textiControllerResult = importFileResults(outputTextiControllerFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[PRECISION_INDEX] as double,
                            recall:it[RECALL_INDEX] as double] }?.groupBy{ it.project }
        println "textiControllerResult: ${textiControllerResult.size()}"
    }

    private configureProjects(){
        projects = testiAddedResult.keySet() as List
        println "projects: ${projects.size()}"
        projects.each{ println it }
    }

    def organizeResultsPerProject(){
        projects.each{ project ->
            def adPrecision = testiAddedResult.get(project).collect{it.precision as double} as double[]
            def adRecall = testiAddedResult.get(project).collect{it.recall as double} as double[]
            def ad = [precision: generateStatistics(adPrecision), recall: generateStatistics(adRecall)]

            def adcPrecision = testiAddedControllerResult.get(project).collect{it.precision as double} as double[]
            def adcRecall = testiAddedControllerResult.get(project).collect{it.recall as double} as double[]
            def adc = [precision: generateStatistics(adcPrecision), recall: generateStatistics(adcRecall)]

            def adwPrecision = testiAddedWhenResult.get(project).collect{it.precision as double} as double[]
            def adwRecall = testiAddedWhenResult.get(project).collect{it.recall as double} as double[]
            def adw = [precision: generateStatistics(adwPrecision), recall: generateStatistics(adwRecall)]

            def adwcPrecision = testiAddedWhenControllerResult.get(project).collect{it.precision as double} as double[]
            def adwcRecall = testiAddedWhenControllerResult.get(project).collect{it.recall as double} as double[]
            def adwc = [precision: generateStatistics(adwcPrecision), recall: generateStatistics(adwcRecall)]

            def cPrecision = testiAllResult.get(project).collect{it.precision as double} as double[]
            def cRecall = testiAllResult.get(project).collect{it.recall as double} as double[]
            def c = [precision: generateStatistics(cPrecision), recall: generateStatistics(cRecall)]

            def ccPrecision = testiAllControllerResult.get(project).collect{it.precision as double} as double[]
            def ccRecall = testiAllControllerResult.get(project).collect{it.recall as double} as double[]
            def cc = [precision: generateStatistics(ccPrecision), recall: generateStatistics(ccRecall)]

            def cwPrecision = testiAllWhenResult.get(project).collect{it.precision as double} as double[]
            def cwRecall = testiAllWhenResult.get(project).collect{it.recall as double} as double[]
            def cw = [precision: generateStatistics(cwPrecision), recall: generateStatistics(cwRecall)]

            def cwcPrecision = testiAllWhenControllerResult.get(project).collect{it.precision as double} as double[]
            def cwcRecall = testiAllWhenControllerResult.get(project).collect{it.recall as double} as double[]
            def cwc = [precision: generateStatistics(cwcPrecision), recall: generateStatistics(cwcRecall)]

            def randomiPrecision = randomiResult.get(project).collect{it.precision as double} as double[]
            def randomiRecall = randomiResult.get(project).collect{it.recall as double} as double[]
            def randomi = [precision: generateStatistics(randomiPrecision), recall: generateStatistics(randomiRecall)]

            def crandomiPrecision = randomiControllerResult.get(project).collect{it.precision as double} as double[]
            def crandomiRecall = randomiControllerResult.get(project).collect{it.recall as double} as double[]
            def crandomi = [precision: generateStatistics(crandomiPrecision), recall: generateStatistics(crandomiRecall)]

            def textiPrecision = textiResult.get(project).collect{it.precision as double} as double[]
            def textiRecall = textiResult.get(project).collect{it.recall as double} as double[]
            def texti = [precision: generateStatistics(textiPrecision), recall: generateStatistics(textiRecall)]

            def ctextiPrecision = textiControllerResult.get(project).collect{it.precision as double} as double[]
            def ctextiRecall = textiControllerResult.get(project).collect{it.recall as double} as double[]
            def ctexti = [precision: generateStatistics(ctextiPrecision), recall: generateStatistics(ctextiRecall)]

            def n = testiAddedResult.get(project).size()
            results += [project:project, n:n, ad:ad, adc:adc, adw:adw, adwc:adwc, c:c, cc:cc, cw:cw, cwc:cwc,
                        randomi:randomi, crandomi:crandomi, texti:texti, ctexti:ctexti]
        }
    }

    def exportResult(){
        List<String[]> content = []
        content += ["Project", "Tasks",
                    "TESTI_AD_P_Mean", "TESTI_AD_P_Median", "TESTI_AD_P_SD",
                    "TESTI_AD_R_Mean", "TESTI_AD_R_Median", "TESTI_AD_R_SD",
                    "TESTI_ADC_P_Mean", "TESTI_ADC_P_Median", "TESTI_ADC_P_SD",
                    "TESTI_ADC_R_Mean", "TESTI_ADC_R_Median", "TESTI_ADC_R_SD",
                    "TESTI_ADW_P_Mean", "TESTI_ADW_P_Median", "TESTI_ADW_P_SD",
                    "TESTI_ADW_R_Mean", "TESTI_ADW_R_Median", "TESTI_ADW_R_SD",
                    "TESTI_ADWC_P_Mean", "TESTI_ADWC_P_Median", "TESTI_ADWC_P_SD",
                    "TESTI_ADWC_R_Mean", "TESTI_ADWC_R_Median", "TESTI_ADWC_R_SD",
                    "TESTI_NF_P_Mean", "TESTI_NF_P_Median", "TESTI_NF_P_SD",
                    "TESTI_NF_R_Mean", "TESTI_NF_R_Median", "TESTI_NF_R_SD",
                    "TESTI_CF_P_Mean", "TESTI_CF_P_Median", "TESTI_CF_P_SD",
                    "TESTI_CF_R_Mean", "TESTI_CF_R_Median", "TESTI_CF_R_SD",
                    "TESTI_WF_P_Mean", "TESTI_WF_P_Median", "TESTI_WF_P_SD",
                    "TESTI_WF_R_Mean", "TESTI_WF_R_Median", "TESTI_WF_R_SD",
                    "TESTI_CWF_P_Mean", "TESTI_CWF_P_Median", "TESTI_CWF_P_SD",
                    "TESTI_CWF_R_Mean", "TESTI_CWF_R_Median", "TESTI_CWF_R_SD",
                    "RANDOMI_NF_P_Mean", "RANDOMI_NF_P_Median", "RANDOMI_NF_P_SD",
                    "RANDOMI_NF_R_Mean", "RANDOMI_NF_R_Median", "RANDOMI_NF_R_SD",
                    "RANDOMI_CF_P_Mean", "RANDOMI_CF_P_Median", "RANDOMI_CF_P_SD",
                    "RANDOMI_CF_R_Mean", "RANDOMI_CF_R_Median", "RANDOMI_CF_R_SD",
                    "TEXTI_NF_P_Mean", "TEXTI_NF_P_Median", "TEXTI_NF_P_SD",
                    "TEXTI_NF_R_Mean", "TEXTI_NF_R_Median", "TEXTI_NF_R_SD",
                    "TEXTI_CF_P_Mean", "TEXTI_CF_P_Median", "TEXTI_CF_P_SD",
                    "TEXTI_CF_R_Mean", "TEXTI_CF_R_Median", "TEXTI_CF_R_SD"] as String[]
        results.each{ r ->
            content += [r.project, r.n,

                        r.ad.precision.mean,
                        r.ad.precision.median,
                        r.ad.precision.sd,
                        r.ad.recall.mean,
                        r.ad.recall.median,
                        r.ad.recall.sd,

                        r.adc.precision.mean,
                        r.adc.precision.median,
                        r.adc.precision.sd,
                        r.adc.recall.mean,
                        r.adc.recall.median,
                        r.adc.recall.sd,

                        r.adw.precision.mean,
                        r.adw.precision.median,
                        r.adw.precision.sd,
                        r.adw.recall.mean,
                        r.adw.recall.median,
                        r.adw.recall.sd,

                        r.adwc.precision.mean,
                        r.adwc.precision.median,
                        r.adwc.precision.sd,
                        r.adwc.recall.mean,
                        r.adwc.recall.median,
                        r.adwc.recall.sd,

                        r.c.precision.mean,
                        r.c.precision.median,
                        r.c.precision.sd,
                        r.c.recall.mean,
                        r.c.recall.median,
                        r.c.recall.sd,

                        r.cc.precision.mean,
                        r.cc.precision.median,
                        r.cc.precision.sd,
                        r.cc.recall.mean,
                        r.cc.recall.median,
                        r.cc.recall.sd,

                        r.cw.precision.mean,
                        r.cw.precision.median,
                        r.cw.precision.sd,
                        r.cw.recall.mean,
                        r.cw.recall.median,
                        r.cw.recall.sd,

                        r.cwc.precision.mean,
                        r.cwc.precision.median,
                        r.cwc.precision.sd,
                        r.cwc.recall.mean,
                        r.cwc.recall.median,
                        r.cwc.recall.sd,

                        r.randomi.precision.mean,
                        r.randomi.precision.median,
                        r.randomi.precision.sd,
                        r.randomi.recall.mean,
                        r.randomi.recall.median,
                        r.randomi.recall.sd,

                        r.crandomi.precision.mean,
                        r.crandomi.precision.median,
                        r.crandomi.precision.sd,
                        r.crandomi.recall.mean,
                        r.crandomi.recall.median,
                        r.crandomi.recall.sd,

                        r.texti.precision.mean,
                        r.texti.precision.median,
                        r.texti.precision.sd,
                        r.texti.recall.mean,
                        r.texti.recall.median,
                        r.texti.recall.sd,

                        r.ctexti.precision.mean,
                        r.ctexti.precision.median,
                        r.ctexti.precision.sd,
                        r.ctexti.recall.mean,
                        r.ctexti.recall.median,
                        r.ctexti.recall.sd] as String[]
        }
        CsvUtil.write(outputFile, content)
    }

}
