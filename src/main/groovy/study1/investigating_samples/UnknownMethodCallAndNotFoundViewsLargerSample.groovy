package study1.investigating_samples

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

class UnknownMethodCallAndNotFoundViewsLargerSample {

    final int PROJECT_INDEX = 0
    final int TASK_INDEX = 1
    final int UNKNOWN_METHODS_INDEX = 20
    final int NOT_FOUND_VIEWS_INDEX = 24
    List detailedResultFiles
    List output
    String tasksFile
    String inputFolder
    String outputFileName
    List validTasks

    UnknownMethodCallAndNotFoundViewsLargerSample(String tasksFile, String inputFolder, String outputFileName){
        this.tasksFile = tasksFile
        this.inputFolder = inputFolder
        this.outputFileName = outputFileName
        initDetailedResultFiles()
        initValidTasks()
    }

    static void main(String[] args){
        def tasksFile = "tasks\\temp\\tasksPerSample\\463\\463_tasks.csv"
        def folder = "D:\\Bitbucket\\utility\\tasks\\temp\\cucumber_sample"
        def outputFileName = "output${File.separator}unknownMethodCall_larger_sample_463.csv"
        def analyser = new UnknownMethodCallAndNotFoundViewsLargerSample(tasksFile, folder, outputFileName)
        analyser.extractResults()
        analyser.exportResult()
    }

    private static importFileResults(String file){
        def tasks = CsvUtil.read(file)
        tasks.subList(1, tasks.size()-15)
    }

    private static formatProjectName(String project){
        def name = project.toLowerCase()
        if(name ==~ /.+_\d+/ ){
            def index = name.lastIndexOf("_")
            name = name.substring(0, index)
        }
        if(name.contains("_")) {
            def index = name.indexOf("_")
            name = name.substring(index+1)
        }
        name
    }

    private static formatRepoUrl(String url){
        def name = url.toLowerCase() - "https://github.com/"
        name = name - ".git"
        def index = name.lastIndexOf("/")
        if(index>-1) name = name.substring(index+1)
        name
    }

    private static generateStatistics(double[] values){
        def statistics = new DescriptiveStatistics(values)
        [mean: statistics.mean, median: statistics.getPercentile(50.0), sd: statistics.standardDeviation]
    }

    private initValidTasks(){
        def temp = CsvUtil.read(tasksFile)
        temp.remove(0)
        validTasks = temp.collect{ [project:it[0], task:it[1] as int] }
        validTasks.each{ vt ->
            vt.project = formatRepoUrl(vt.project)
        }
        println "validTasks: ${validTasks.size()}"
        //def projects = validTasks.collect{ it.project }.unique()
        //println "projects from validTasks: ${projects.size()}"
        //projects.each{ println it }
    }

    private initDetailedResultFiles(){
        def aux = Util.findFilesFromDirectory(inputFolder)
        detailedResultFiles = aux.findAll{
            it.contains("${File.separator}result${File.separator}") &&
            it.endsWith("selected_all.csv")
        }
        println "detailedResultFiles: ${detailedResultFiles.size()}"
        detailedResultFiles.each{ println it }
    }

    def extractResults(){
        output = []
        detailedResultFiles.each{ file ->
            def tasks = importFileResults(file)
            tasks?.each{
                def project = formatProjectName(it[PROJECT_INDEX])
                def id = it[TASK_INDEX] as int
                def calls = it[UNKNOWN_METHODS_INDEX].tokenize(',[]')*.trim().unique()
                def notFoundViews = it[NOT_FOUND_VIEWS_INDEX] as int
                def foundMatch = validTasks.find{ it.project==project && it.task==id }
                if(foundMatch){
                    output += [project:project, task:id, number:calls.size(), methods:calls, notFoundViews:notFoundViews]
                }
            }
        }
        output = output.sort{ [it.project, it.task] }
        println "output: ${output.size()}"
        def projects = output.collect{ it.project }.unique()
        //println "projects from output: ${projects.size()}"
        //projects.each{ println it }
    }

    def exportResult(){
        List<String[]> result = []
        result += ["Project", "Task", "#Unknown_methods", "Unknown_methods", "#Not_found_views"] as String[]
        output.each{
            result += [it.project, it.task, it.number, it.methods, it.notFoundViews] as String[]
        }
        def methodsStats = generateStatistics(output.collect{it.number as double} as double[])
        def viewsStats = generateStatistics(output.collect{it.notFoundViews as double} as double[])
        result += ["", "Mean", methodsStats.mean, "", viewsStats.mean] as String[]
        result += ["", "Median", methodsStats.median, "", viewsStats.median] as String[]
        result += ["", "SD", methodsStats.sd, "", viewsStats.sd] as String[]
        CsvUtil.write(outputFileName, result)
    }

}
