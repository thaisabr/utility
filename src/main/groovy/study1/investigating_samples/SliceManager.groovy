package study1.investigating_samples

import br.ufpe.cin.tan.test.ruby.routes.Inflector
import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

class SliceManager {

    static final int PROJECT_INDEX = 0
    static final int TASK_INDEX = 1
    static final int TESTI_INDEX = 9//ITest
    static final int TASKI_INDEX = 10//IReal
    static final String MODEL_SLICE = "app/models"
    static final String VIEW_SLICE = "app/views"
    static final String CONTROLLER_SLICE = "app/controllers"
    static final String SHARED_APP = "app/"

    List<String[]> data
    List outputPredictor

    Inflector inflector

    SliceManager(String csv){
        data = CsvUtil.read(csv)
        data.remove(0)
        //data = data.subList(1, data.size()-15)
        outputPredictor = []
        inflector = new Inflector()
    }

    static void main(String[] args){
        SliceManager manager = new SliceManager("tasks\\temp\\cucumber_sample_task_interfaces_463\\itest_all.csv")
        manager.run()

        println "resultados: ${manager.outputPredictor.size()}"
        //def result = manager.outputPredictor.first()
        /*manager.outputPredictor.each { result ->
            println "${result.project}: ${result.task}"
            println "testi(${result.testi.size()}): ${result.testi}"
            println "testiSlices (${result.testiSlices.size()}): ${result.testiSlices}"
            println "taski(${result.taski.size()}): ${result.taski}"
            println "taskiSlices (${result.taskiSlices.size()}): ${result.taskiSlices}\n"
            println "predictor: ${result.predictor}\n"
        }*/

        def predictorTrue = manager.outputPredictor.count{ it.predictor == true }
        def predictorFalse = manager.outputPredictor.count{ it.predictor == false }
        def precision = manager.outputPredictor.collect{ it.precision as double} as double[]
        def precisionRate = generateStatistics(precision)
        def recall = manager.outputPredictor.collect{ it.recall as double} as double[]
        def recallRate = generateStatistics(recall)
        println "predictorTrue: ${predictorTrue}"
        println "predictorFalse: ${predictorFalse}"
        println "Conclusão: Para ${predictorTrue/manager.outputPredictor.size()*100}% das tarefas, controllers identificados " +
                "em TestI são preditor dos slices alterados pela tarefa."

        println "precisionRate: ${precisionRate}"
        println "Do total de controllers em TestI, quantos identificam slices alterados: ${precisionRate}"

        println "recallRate: ${recallRate}"
        println "Do total de slices alterados, quantos são identificados pelos controllers em TestI: ${recallRate}"
    }

    def run(){
        int counterTestiNoControllers = 0
        data?.each{ line ->
            def testi = line[TESTI_INDEX].tokenize(',[]')*.trim()
            def testiSlices = identifySlices(testi)
            def taski = line[TASKI_INDEX].tokenize(',[]')*.trim()
            def taskiSlices = identifySlices(taski)
            if(hasControllers(testi)){
                def intersection = testiSlices.controllers.intersect(taskiSlices.slices)
                def predictor = !intersection.empty
                def precision =  0
                def recall = 0
                if(testiSlices.controllers.size()>0) precision = intersection.size()/testiSlices.controllers.size()
                if(taskiSlices.slices.size()>0) recall = intersection.size()/taskiSlices.slices.size()
                outputPredictor += [project:line[PROJECT_INDEX], task:line[TASK_INDEX], testiSlices:testiSlices,
                           taskiSlices:taskiSlices, testi:testi, taski:taski, predictor:predictor, precision:precision,
                                    recall:recall]
            } else { //TestI não possui controllers
                counterTestiNoControllers++
            }
        }
        println "Tarefas cujo TestI não possui controllers: ${counterTestiNoControllers}"
        exportPredictionResult()
    }

    private static generateStatistics(double[] values){
        def statistics = new DescriptiveStatistics(values)
        [mean: statistics.mean, median:statistics.getPercentile(50.0), sd:statistics.standardDeviation]
    }

    private static boolean hasControllers(List files){
        def controllers = files.findAll{ Util.isControllerFile(it) }
        !controllers.empty
    }

    private identifySlices(def files){
        def countAppShared = 0
        def countConfig = 0

        def modelsSlices = []
        def viewsSlices = []
        def controllersSlices = []

        files.each { file ->
            if (file.startsWith(MODEL_SLICE)){
                modelsSlices += extractModelName(file)
            }
            else if (file.startsWith(VIEW_SLICE)){
                def tmp = extractViewName(file)
                if (!tmp.empty){
                    viewsSlices += tmp
                }
            }
            else if (file.startsWith(CONTROLLER_SLICE)){
                def tmp = extractControllerName(file)
                controllersSlices += tmp
            }
            else if (file.startsWith(SHARED_APP)) countAppShared++
            else countConfig++
        }

        modelsSlices = modelsSlices.unique()
        viewsSlices = viewsSlices.unique()
        controllersSlices = controllersSlices.unique()

        def slices = (modelsSlices + viewsSlices + controllersSlices).unique()
        def intersectionModelAndView = modelsSlices.intersect(viewsSlices)
        def intersectionModelAndController = modelsSlices.intersect(controllersSlices)

        [models: modelsSlices, views:viewsSlices, controllers:controllersSlices, slices:slices,
         interMV:intersectionModelAndView, interMC:intersectionModelAndController, countAppShared:countAppShared,
         countConfig:countConfig]
    }

    private static extractModelName(file){
        def nomeModelo = file.substring(MODEL_SLICE.size()+1)
        nomeModelo.substring(0, nomeModelo.indexOf("."))
    }

    private extractViewName(file){
        def nomeView = file.substring(VIEW_SLICE.size()+1)
        def tmp = ""
        def index = nomeView.indexOf("/")
        if (index > -1){
            tmp = nomeView.substring(0, index)
        }
        inflector.singularize(tmp)
    }

    private extractControllerName(file){
        def nomeController = file.substring(CONTROLLER_SLICE.size()+1)
        def tmp = nomeController.substring(0, nomeController.indexOf("."))
        if (tmp.contains("_")){
            tmp = tmp.substring(0, tmp.indexOf("_"))
        }
        def index = tmp.lastIndexOf("/")
        if(index>-1) tmp = tmp.substring(index+1)
        inflector.singularize(tmp)
    }

    private exportPredictionResult(){
        List<String[]> result = []
        result += ["Project", "Task", "TestI_slices", "TaskI_Slices", "TestI", "TaskI", "Predictor", "Precision", "Recall"] as String[]
        outputPredictor.each{
            result += [it.project, it.task, it.testiSlices.toString(), it.taskiSlices.toString(), it.testi.toString(),
            it.taski.toString(), it.predictor, it.precision, it.recall] as String[]
        }

        CsvUtil.write("output${File.separator}predictionResult_463.csv", result)
    }

}
