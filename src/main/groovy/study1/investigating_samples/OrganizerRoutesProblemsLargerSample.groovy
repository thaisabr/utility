package study1.investigating_samples

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

class OrganizerRoutesProblemsLargerSample {

    String outputFileName
    String tasksFile
    List problemsResult
    String inputFolder
    List validTasks

    OrganizerRoutesProblemsLargerSample(String tasksFile, String inputFolder, String outputFileName){
        this.tasksFile = tasksFile
        this.inputFolder = inputFolder
        this.outputFileName = outputFileName
        verifyProjects()
        initValidTasks()
    }

    static void main(String[] args){
        def tasksFile = "tasks\\temp\\tasksPerSample\\463\\463_tasks.csv"
        def folder = "tasks\\temp\\routes-problems-larger-sample-fixed-463"
        def outputFileName = "output${File.separator}routeProblems_larger_sample_final_463.csv"
        def organizer = new OrganizerRoutesProblemsLargerSample(tasksFile, folder, outputFileName)
        organizer.initProblemsResult()
        organizer.exportResult()
    }

    private static formatRepoUrl(String url){
        def name = url.toLowerCase() - "https://github.com/"
        name = name - ".git"
        def index = name.lastIndexOf("/")
        if(index>-1) name = name.substring(index+1)
        name
    }

    private initValidTasks(){
        def temp = CsvUtil.read(tasksFile)
        temp.remove(0)
        validTasks = temp.collect{ [project:it[0], task:it[1] as int] }
        validTasks.each{ vt ->
            vt.project = formatRepoUrl(vt.project)
        }
        println "validTasks: ${validTasks.size()}"
        def projects = validTasks.collect{ it.project }.unique().sort()
        println "projects from validTasks: ${projects.size()}"
        projects.each{ println it }
    }

    def initProblemsResult(){
        problemsResult = []
        def files = Util.findFilesFromDirectory(inputFolder)
        println "files: ${files.size()}"
        files.each{ file ->
            def fileResult = []
            println "file: $file"
            def lines =  CsvUtil.read(file)
            lines.remove(0)
            lines.each{ line ->
                def foundMatch = validTasks.find{ it.project==line[0] && it.task==(line[1] as int) }
                if(foundMatch) {
                    fileResult += line
                }
            }
            println "matches: ${fileResult.size()}"
            problemsResult += fileResult.unique()
        }
        problemsResult = problemsResult.unique().sort{ it[0] }.sort{ it[1] as int }
        println "problemsResult antes: ${problemsResult.size()}"
        eliminateDuplicates()
        println "problemsResult depois: ${problemsResult.size()}"
    }

    private eliminateDuplicates(){
        def result = []
        def duplicates = problemsResult.groupBy { [it[0], it[1]] }
        println "duplicates: ${duplicates.size()}"
        duplicates.each{ d ->
            println "group: ${d.key} (${d.value.size()})"
            d.value.each{
                println "${it[2] as int}, ${it[3] as int}, ${it[4] as int}, ${it[5] as int}, ${it[6] as int}"
            }
            if(d.value.size()==1) result += d.value.first()
            else{
                def max = findMaxValue(d, 2)
                println "max: $max"
                if(max) result += max
            }
        }
        problemsResult = result
    }

    static findMaxValue(def group, int index){
        group.value.max{ it[index] as int }
    }

    def verifyProjects(){
        def files = Util.findFilesFromDirectory(inputFolder)
        def result = []
        files.each{
            def lines = CsvUtil.read(it)
            lines.remove(0)
            def projects = lines.collect{ it[0] }.unique()
            result += projects
        }
        result = result.unique().sort()
        println "projects from problems: ${result.size()}"
        result.each{ println it }
    }

    def exportResult(){
        List<String[]> result = []
        result += ["Project", "Task", "Problematic_routes", "Path_methods_no_data",
                   "Not_found_direct_accessed_views", "Error_extract_code_from_view",
                   "Calls_from_views_no_deal"] as String[]
        problemsResult.each{ pr ->
            result += [pr[0], pr[1], pr[2], pr[3], pr[4], pr[5], pr[6]] as String[]
        }
        CsvUtil.write(outputFileName, result)
    }

}
