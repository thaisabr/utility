package study1.investigating_samples

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

class RoutesProblemsLargerSample {

    String inputFolder
    String outputFileName
    List logFiles
    String tasksFile
    List validTasks

    List nfResults
    List tasksPerFile
    List errorsPerFile

    static final String REGEX_TASK_ID = /.+ TASK ID: \d+/
    static final String REGEX_PROJECT = /.+ < Running all config from:.*/
    static final String REGEX_CONFIGURATION = /.+ < Running changed configuration... >/
    static final String REGEX_PROBLEMATIC_ROUTES = /.+ Problematic routes \(\d+\):.*/
    static final String REGEX_RAILS_PATH_METHODS_NO_DATA = /.+ Rails path methods with no data \(\d+\):.*/
    static final String REGEX_NOT_FOUND_DIRECT_ACCESSED_VIEWS = /.+ Not found direct accessed views: \d+.*/
    static final String REGEX_ERROR_EXTRACT_CODE_FROM_VIEW = /.+ Error to extract code from view file:.*/
    static final String REGEX_CALLS_FROM_VIEWS_WE_CANT_DEAL = /.+ Calls from view file\(s\) that we can not deal with: \d.*/
    static final String REGEX_FILE_END = /.+ Task interfaces were computed for .*/

    RoutesProblemsLargerSample(String tasksFile, String inputFolder, String outputFileName){
        this.tasksFile = tasksFile
        this.inputFolder = inputFolder
        this.outputFileName = outputFileName
        this.nfResults = []
        this.tasksPerFile = []
        this.errorsPerFile = []
        initDetailedResultFiles()
        initValidTasks()
    }

    static void main(String[] args){
        //PARA RODAR NO SERVIDOR:
        //def tasksFile = "tasks${File.separator}465_tasks.csv"
        //def folder = "sample_analysis"

        //PARA RODAR LOCALMENTE:
        def tasksFile = "tasks\\temp\\tasksPerSample\\463\\463_tasks.csv"
        def folder = "D:\\current\\study1-paper-sample\\sample_analysis"
        def outputFileName = "output${File.separator}routeProblems_larger_sample_463.csv"
        def analyser = new RoutesProblemsLargerSample(tasksFile, folder, outputFileName)
        analyser.extractResults()
        analyser.exportResult()
    }

    private static extractProjectFromLine(String line){
        def project = ""
        def index = line.lastIndexOf("/") //file separator
        if(index>-1) project = line.substring(index+1)
        index = project.indexOf(".")
        if(index>-1) project = project.substring(0, index)
        formatProjectName(project)
    }

    private static formatProjectName(String project){
        def name = project.toLowerCase()
        if(name ==~ /.+_\d+/ ){
            def index = name.lastIndexOf("_")
            name = name.substring(0, index)
        }
        if(name.contains("_")) {
            def index = name.indexOf("_")
            name = name.substring(index+1)
        }
        name
    }

    private static formatRepoUrl(String url){
        def name = url.toLowerCase() - "https://github.com/"
        name = name - ".git"
        def index = name.lastIndexOf("/")
        if(index>-1) name = name.substring(index+1)
        name
    }

    private initDetailedResultFiles(){
        def aux = Util.findFilesFromDirectory(inputFolder)
        logFiles = aux.findAll{
            !it.contains("${File.separator}bsmi${File.separator}") &&
            !it.contains("${File.separator}openproject${File.separator}") &&
            !it.contains("${File.separator}rigse${File.separator}") &&
            !it.contains("${File.separator}makrio${File.separator}") &&
            !it.contains("${File.separator}rails3book_ticketee${File.separator}") &&
            !it.contains("${File.separator}twers_re-education${File.separator}") &&
            !it.contains("${File.separator}wearefriday_spectre${File.separator}") &&
            !it.contains("${File.separator}problematic${File.separator}") &&
            it.endsWith("evaluation.log")
        }
        println "logFiles: ${logFiles.size()}"
        logFiles.each{ println it }
    }

    private initValidTasks(){
        def temp = CsvUtil.read(tasksFile)
        temp.remove(0)
        validTasks = temp.collect{ [project:it[0], task:it[1] as int] }
        validTasks.each{ vt ->
            vt.project = formatRepoUrl(vt.project)
        }
        println "validTasks: ${validTasks.size()}"
        def projects = validTasks.collect{ it.project }.unique()
        println "projects from validTasks: ${projects.size()}"
        projects.each{ println it }
    }

    private static verifyFileSize(String file){
        def fileReader = new File(file)
        def counter = 0
        fileReader.eachLine{ counter ++ }
        counter
    }

    private static extractLinesSubset(String file, def begin, def end){
        def fileReader = new File(file)
        def result = []
        fileReader.eachLine{ line, i ->
            if(i>=begin && i<=end) result += line
        }
        result
    }

    private static findResultEnding(String file, def begin){
        def init = begin + 1
        def end = -1
        new File(file).eachLine { line, i ->
            if (i>init && end==-1 && line ==~ REGEX_PROJECT) end = i
        }
        if(end==-1) end = verifyFileSize(file)-1
        end
    }

    private extractResultFromNF(){
        nfResults = []
        logFiles.each{ file ->
            println "Extracting NF result from: ${file}"
            def project = ""
            new File(file).eachLine{ line, i ->
                if (line ==~ REGEX_PROJECT) {
                    project = extractProjectFromLine(line)
                }
                if (line ==~ REGEX_CONFIGURATION && !project.empty) {
                    def j = findResultEnding(file, i)
                    def linesOfInterest = extractLinesSubset(file, i, j)
                    println "lines: $i, $j"
                    nfResults += [project: project, lines: linesOfInterest]
                    project = ""
                }
            }
        }
        println "nfResults: ${nfResults.size()}"
        def projects = nfResults.collect{ it.project }.unique()
        println "projects from nfResults: ${projects.size()}"
        projects.each{ println it }
    }

    private splitResultPerTask(){
        nfResults.each{ r ->
            def tasks = []
            println "Extracting tasks from: ${r.project}"
            for(def i=0; i<r.lines.size(); i++){
                def line = r.lines.get(i)
                if(line ==~ REGEX_TASK_ID){
                    def index = line.lastIndexOf(" ")
                    def id = line.substring(index+1) as int

                    def foundMatch = validTasks.find{ it.project==r.project && it.task==id }
                    if(foundMatch){
                        def finalIndex = findNextTask(r.lines, i)
                        if(finalIndex==-1) finalIndex = r.lines.size()
                        def content = r.lines.subList(i, finalIndex)
                        tasks += [id:id, content:content]
                    }
                }
            }

            if(!tasks.empty) tasks.last().content = configureContentOfLastTask(tasks.last().content)

            println "Number of tasks: ${tasks.size()}"
            this.tasksPerFile += [project:r.project, tasks:tasks]
        }
    }

    private static configureContentOfLastTask(List content){
        def result = content
        for(def i=0; i<content.size(); i++){
            def line = content.get(i)
            if(line ==~ REGEX_FILE_END){
                result = content.subList(0, i)
                break
            }
        }
        result
    }

    private static findNextTask(List lines, int index){
        int j = -1
        for(def i=index+1; i<lines.size(); i++) {
            def line = lines.get(i)
            if(line ==~ REGEX_TASK_ID){
                j = i
                break
            }
        }
        j
    }

    private extractErrorsFromTask(){
        errorsPerFile = []
        tasksPerFile.each{ tpf ->
            println "Extracting errors from tasks of project: ${tpf.project}"
            def errors = []
            tpf.tasks.each{ task ->
                println "Extracting errors from task: ${task.id}"
                int problematicRoutes = 0
                int pathMethodsNoData = 0
                int notFoundDirectAccessedViews = 0
                int errorExtractCodeFromView = 0
                int callsFromViewsNoDeal = 0
                for(def i=0; i<task.content.size(); i++) {
                    def line = task.content.get(i)
                    if(line ==~ REGEX_PROBLEMATIC_ROUTES){
                        def index1 = line.indexOf("(")
                        def index2 = line.indexOf(")")
                        problematicRoutes += line.substring(index1+1, index2) as int
                    }
                    if(line ==~ REGEX_RAILS_PATH_METHODS_NO_DATA){
                        def index1 = line.indexOf("(")
                        def index2 = line.indexOf(")")
                        pathMethodsNoData += line.substring(index1+1, index2) as int
                    }
                    if(line ==~ REGEX_NOT_FOUND_DIRECT_ACCESSED_VIEWS){
                        def index = line.lastIndexOf(" ")
                        notFoundDirectAccessedViews += line.substring(index+1, line.size()) as int
                    }
                    if(line ==~ REGEX_ERROR_EXTRACT_CODE_FROM_VIEW){
                        errorExtractCodeFromView++
                    }
                    if(line ==~ REGEX_CALLS_FROM_VIEWS_WE_CANT_DEAL){
                        def index = line.lastIndexOf(" ")
                        callsFromViewsNoDeal += line.substring(index+1, line.size()) as int
                    }

                }
                errors += [id: task.id, problematicRoutes:problematicRoutes,
                           pathMethodsNoData:pathMethodsNoData,
                           notFoundDirectAccessedViews:notFoundDirectAccessedViews,
                           errorExtractCodeFromView:errorExtractCodeFromView,
                           callsFromViewsNoDeal:callsFromViewsNoDeal]
            }
            errorsPerFile += [project:tpf.project, errors:errors]
        }

        println "errorsPerFile: ${errorsPerFile.size()}"
        errorsPerFile.each{ epf ->
            println "project: ${epf.project}"
            epf.errors.each{ error ->
                println error
            }
        }
    }

    def extractResults(){
        extractResultFromNF()
        splitResultPerTask()
        extractErrorsFromTask()
    }

    def exportResult(){
        List<String[]> result = []
        result += ["Project", "Task", "Problematic_routes", "Path_methods_no_data",
                   "Not_found_direct_accessed_views", "Error_extract_code_from_view",
                   "Calls_from_views_no_deal"] as String[]
        errorsPerFile.each{ epf ->
            epf.errors.each{ e ->
                result += [epf.project, e.id, e.problematicRoutes, e.pathMethodsNoData,
                           e.notFoundDirectAccessedViews, e.errorExtractCodeFromView, e.callsFromViewsNoDeal] as String[]
            }
        }
        CsvUtil.write(outputFileName, result)
    }

}
