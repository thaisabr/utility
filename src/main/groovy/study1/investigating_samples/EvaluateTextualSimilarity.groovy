package study1.investigating_samples

import br.ufpe.cin.tan.util.CsvUtil
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

class EvaluateTextualSimilarity {

    List result //project,task,similar,level
    List projects
    List organizedResult
    List organizedResultPerProject
    List tasks

    EvaluateTextualSimilarity(String file){
        result = CsvUtil.read(file)
        result.remove(0)
        projects = result.collect{ it[0] }.unique()
        organizedResult = []
        organizedResultPerProject = []
        tasks = CsvUtil.read("tasks\\temp\\cucumber_sample_task_interfaces_463\\itest_all.csv")
        tasks.remove(0)
        tasks = tasks.collect{ [it[0], it[1]] }
    }

    static void main(String[] args){
        String file = "tasks${File.separator}temp${File.separator}textual-similarity_463${File.separator}textual_similarity_all.csv"
        EvaluateTextualSimilarity evaluator = new EvaluateTextualSimilarity(file)
        evaluator.evaluate()
        evaluator.evaluatePerProject()
    }

    def evaluate(){
        organizedResult = []
        projects.each{ project ->
            println "project: $project"
            def resultsPerProject = result.findAll{ it[0] == project }
            println "resultsPerProject: ${resultsPerProject.size()}"
            def tasksPerProject = resultsPerProject.collect{ it[1] }?.unique()
            println "tasksPerProject: ${tasksPerProject.size()}"
            tasksPerProject.each{ task ->
                def resultsOfAnSpecificTask = resultsPerProject.findAll{ it[1] == task }?.collect{ it[3] as double }
                def stats = new DescriptiveStatistics(resultsOfAnSpecificTask as double[])
                def mean = stats.mean
                def median = stats.getPercentile(50.0)
                def sd = stats.standardDeviation
                organizedResult += [project:project, task: task, mean:mean, median:median, sd:sd]
            }
        }

        organizedResult = organizedResult.findAll{ org ->
            def found = tasks.find{ it[0]== org.project && it[1]==org.task }
            found != null
        }
        exportResult()
    }

    def evaluatePerProject(){
        organizedResultPerProject = []
        projects.each{ project ->
            def results = organizedResult.findAll{ it.project == project }?.collect{ it.mean as double }
            def stats = new DescriptiveStatistics(results as double[])
            def mean = stats.mean
            def median = stats.getPercentile(50.0)
            def sd = stats.standardDeviation
            organizedResultPerProject += [project:project, mean:mean, median:median, sd:sd]
        }
        exportResultPerProject()
    }


    def exportResult(){
        String file = "tasks${File.separator}temp${File.separator}textual-similarity_463${File.separator}" +
                "textual_similarity_all_organized.csv"
        List<String[]> content = []
        String[] firstLine = ["Project", "Task", "Mean", "Median", "SD"] as String[]
        content += firstLine
        println "organizedResult: ${organizedResult.size()}"
        organizedResult.each{ orgResult ->
            content += [orgResult.project, orgResult.task, orgResult.mean, orgResult.median, orgResult.sd] as String[]
        }
        CsvUtil.write(file, content)
    }

    def exportResultPerProject(){
        String file = "tasks${File.separator}temp${File.separator}textual-similarity_463${File.separator}" +
                "textual_similarity_all_organized_per_project.csv"
        List<String[]> content = []
        String[] firstLine = ["Project", "Mean", "Median", "SD"] as String[]
        content += firstLine
        println "organizedResultPerProject: ${organizedResultPerProject.size()}"
        organizedResultPerProject.each{ orgResult ->
            content += [orgResult.project, orgResult.mean, orgResult.median, orgResult.sd] as String[]
        }
        CsvUtil.write(file, content)
    }


}
