package study1.investigating_samples

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

class RoutesProblemsSmallerSample {

    String inputFolder
    String outputFileName
    List logFiles

    List nfResults
    List tasksPerFile
    List errorsPerFile

    static final String REGEX_TASK_ID = /.+ TASK ID: \d+/
    static final String REGEX_AD_FILTER = /.+ <  Restrict gherkin changes: 'false'  >/
    static final String REGEX_W_FILTER = /.+ <  Filter when steps: 'false'  >/
    static final String REGEX_PROBLEMATIC_ROUTES = /.+ Problematic routes \(\d+\):/
    static final String REGEX_RAILS_PATH_METHODS_NO_DATA = /.+ Rails path methods with no data \(\d+\):.*/
    static final String REGEX_NOT_FOUND_DIRECT_ACCESSED_VIEWS = /.+ Not found direct accessed views: \d+.*/
    static final String REGEX_ERROR_EXTRACT_CODE_FROM_VIEW = /.+ Error to extract code from view file:.*/
    static final String REGEX_CALLS_FROM_VIEWS_WE_CANT_DEAL = /.+ Calls from view file\(s\) that we can not deal with: \d+.*/
    static final String REGEX_FILE_END = /.+ Task interfaces were computed for .*/

    RoutesProblemsSmallerSample(String inputFolder, String outputFileName){
        this.inputFolder = inputFolder
        this.outputFileName = outputFileName
        this.nfResults = []
        this.tasksPerFile = []
        this.errorsPerFile = []
        initDetailedResultFiles()
    }

    static void main(String[] args){
        def folder = "D:\\current\\task_organization\\task_analysis\\4-results-sample4\\individual-calculatedRoutes"
        def outputFileName = "output${File.separator}routeProblems_smaller_sample.csv"
        def analyser = new RoutesProblemsSmallerSample(folder, outputFileName)
        analyser.extractResults()
        analyser.exportResult()
    }

    private static extractProjectFromFile(String file){
        def project = ""
        def index = file.indexOf("${File.separator}output")
        if(index>-1) project = file.substring(0, index)
        index = project.lastIndexOf(File.separator)
        if(index>-1) project = project.substring(index+1)
        project
    }

    private static generateStatistics(double[] values){
        def statistics = new DescriptiveStatistics(values)
        [mean: statistics.mean, median: statistics.getPercentile(50.0), sd: statistics.standardDeviation]
    }

    private initDetailedResultFiles(){
        def aux = Util.findFilesFromDirectory(inputFolder)
        logFiles = aux.findAll{
            !it.contains("${File.separator}localsupport${File.separator}") &&
            it.endsWith("-evaluation.log")
        }
        println "logFiles: ${logFiles.size()}"
        logFiles.each{ println it }
    }

    private extractResultFromNF(){
        nfResults = []
        logFiles.each{ file ->
            println "Extracting NF result from: ${file}"
            def lines = new File(file).readLines()
            def project = extractProjectFromFile(file)
            int index = -1
            for (def i = 0; i < lines.size(); i++) {
                if ((i + 1) > (lines.size() - 1)) break
                if (lines.get(i) ==~ REGEX_AD_FILTER && lines.get(i + 1) ==~ REGEX_W_FILTER) {
                    index = i
                    break
                }
            }
            if (index > -1) {
                def linesOfInterest = lines.subList(index, lines.size())
                nfResults += [project: project, lines: linesOfInterest]
            } else {
                println "Error while extracting NF result from: ${file}"
            }
        }
    }

    private splitResultPerTask(){
        nfResults.each{ r ->
            def tasks = []
            println "Extracting tasks from: ${r.project}"
            for(def i=0; i<r.lines.size(); i++){
                def line = r.lines.get(i)
                if(line ==~ REGEX_TASK_ID){
                    def index = line.lastIndexOf(" ")
                    def id = line.substring(index+1) as int
                    def finalIndex = findNextTask(r.lines, i)
                    if(finalIndex==-1) finalIndex = r.lines.size()
                    def content = r.lines.subList(i, finalIndex)
                    tasks += [id:id, content:content]
                }
            }

            tasks.last().content = configureContentOfLastTask(tasks.last().content)

            println "Number of tasks: ${tasks.size()}"
            /*tasks.each{ t ->
                println "TASK ID: ${t.id}"
                println "CONTENT: ${t.content.size()} lines"
                t.content.each{
                    println it
                }
            }*/
            this.tasksPerFile += [project:r.project, tasks:tasks]
        }
    }

    private static configureContentOfLastTask(List content){
        def result = content
        for(def i=0; i<content.size(); i++){
            def line = content.get(i)
            if(line ==~ REGEX_FILE_END){
                result = content.subList(0, i)
                break
            }
        }
        result
    }

    private static findNextTask(List lines, int index){
        int j = -1
        for(def i=index+1; i<lines.size(); i++) {
            def line = lines.get(i)
            if(line ==~ REGEX_TASK_ID){
                j = i
                break
            }
        }
        j
    }

    private extractErrorsFromTask(){
        errorsPerFile = []
        tasksPerFile.each{ tpf ->
            println "Extracting errors from tasks of project: ${tpf.project}"
            def errors = []
            tpf.tasks.each{ task ->
                println "Extracting errors from task: ${task.id}"
                int problematicRoutes = 0
                int pathMethodsNoData = 0
                int notFoundDirectAccessedViews = 0
                int errorExtractCodeFromView = 0
                int callsFromViewsNoDeal = 0
                for(def i=0; i<task.content.size(); i++) {
                    def line = task.content.get(i)
                    if(line ==~ REGEX_PROBLEMATIC_ROUTES){
                        def index1 = line.indexOf("(")
                        def index2 = line.indexOf(")")
                        problematicRoutes += line.substring(index1+1, index2) as int
                    }
                    if(line ==~ REGEX_RAILS_PATH_METHODS_NO_DATA){
                        def index1 = line.indexOf("(")
                        def index2 = line.indexOf(")")
                        pathMethodsNoData += line.substring(index1+1, index2) as int
                    }
                    if(line ==~ REGEX_NOT_FOUND_DIRECT_ACCESSED_VIEWS){
                        def index = line.lastIndexOf(" ")
                        notFoundDirectAccessedViews += line.substring(index+1, line.size()) as int
                    }
                    if(line ==~ REGEX_ERROR_EXTRACT_CODE_FROM_VIEW){
                        errorExtractCodeFromView++
                    }
                    if(line ==~ REGEX_CALLS_FROM_VIEWS_WE_CANT_DEAL){
                        def index = line.lastIndexOf(" ")
                        callsFromViewsNoDeal += line.substring(index+1, line.size()) as int
                    }

                }
                errors += [id: task.id, problematicRoutes:problematicRoutes,
                           pathMethodsNoData:pathMethodsNoData,
                           notFoundDirectAccessedViews:notFoundDirectAccessedViews,
                           errorExtractCodeFromView:errorExtractCodeFromView,
                           callsFromViewsNoDeal:callsFromViewsNoDeal]
            }
            errorsPerFile += [project:tpf.project, errors:errors]
        }

        println "errorsPerFile: ${errorsPerFile.size()}"
        errorsPerFile.each{ epf ->
            println "project: ${epf.project}"
            epf.errors.each{ error ->
                println error
            }
        }
    }

    def extractResults(){
        extractResultFromNF()
        splitResultPerTask()
        extractErrorsFromTask()
    }

    def exportResult(){
        List<String[]> result = []
        result += ["Project", "Task", "Problematic_routes", "Path_methods_no_data",
                   "Not_found_direct_accessed_views", "Error_extract_code_from_view",
                   "Calls_from_views_no_deal"] as String[]
        errorsPerFile.each{ epf ->
            epf.errors.each{ e ->
                result += [epf.project, e.id, e.problematicRoutes, e.pathMethodsNoData,
                           e.notFoundDirectAccessedViews, e.errorExtractCodeFromView, e.callsFromViewsNoDeal] as String[]
            }
        }
        CsvUtil.write(outputFileName, result)
    }

}
