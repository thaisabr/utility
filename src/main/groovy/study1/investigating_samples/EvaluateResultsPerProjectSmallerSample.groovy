package study1.investigating_samples

import br.ufpe.cin.tan.util.CsvUtil
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

/* Script criado para gerar tabela de resultados organizados por projeto, de acordo com diferentes configurações, conforme solicitado pelos revisores do artigo.
* TestI - todas as configurações
* RandomI - NF e CF
* TextI - NF e CF
* Isso é para a amostra maior*/

class EvaluateResultsPerProjectSmallerSample {

    String outputTestiAndRandomiFile
    String outputDtestiFile
    String outputDtestiControllerFile
    String outputTextiFile
    String outputTextiControllerFile

    Map testiAddedResult
    Map testiAddedControllerResult
    Map testiAddedWhenResult
    Map testiAddedWhenControllerResult
    Map testiAllResult
    Map testiAllControllerResult
    Map testiAllWhenResult
    Map testiAllWhenControllerResult
    Map randomiResult
    Map randomiControllerResult
    Map dtestiResult
    Map dtestiControllerResult
    Map textiResult
    Map textiControllerResult

    final int PROJECT_INDEX = 0
    final int TASK_INDEX = 1

    List projects

    def results
    String outputFolder
    String outputFile

    EvaluateResultsPerProjectSmallerSample(){
        outputFolder = "tasks${File.separator}temp${File.separator}smaller_sample_task_interfaces"
        outputFile = "output${File.separator}resultPerProject_smaller_sample.csv"
        results = []
        configureOutputFiles()
        importResults()
        configureProjects()
    }

    static void main(String[] args){
        def evaluator = new EvaluateResultsPerProjectSmallerSample()
        evaluator.organizeResultsPerProject()
        println "results: ${evaluator.results.size()}"
        evaluator.results.each{
            println it
        }
        evaluator.exportResult()
    }

    private static importFileResults(String file){
        def tasks = CsvUtil.read(file)
        tasks.remove(0)
        tasks
    }

    private static formatProjectName(project){
        def name = project.toLowerCase()
        if(name ==~ /.+_\d+/ ){
            def index = name.lastIndexOf("_")
            name = name.substring(0, index)
        }
        if(name.contains("_")) {
            def index = name.indexOf("_")
            name = name.substring(index+1)
        }
        if(name.contains("-")) {
            name = name.replaceAll("-","")
        }
        name
    }

    private static generateStatistics(double[] values){
        def statistics = new DescriptiveStatistics(values)
        [mean: statistics.mean, median: statistics.getPercentile(50.0), sd: statistics.standardDeviation]
    }

    private configureOutputFiles(){
        outputTestiAndRandomiFile = "${outputFolder}${File.separator}precision_recall.csv"
        outputDtestiFile = "${outputFolder}${File.separator}itestd.csv"
        outputDtestiControllerFile = "${outputFolder}${File.separator}itestd-controller.csv"
        outputTextiFile = "${outputFolder}${File.separator}itext.csv"
        outputTextiControllerFile = "${outputFolder}${File.separator}itext-controller.csv"
    }

    private importResults(){
        testiAddedResult = importFileResults(outputTestiAndRandomiFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[4] as double,
                            recall:it[5] as double] }?.groupBy{ it.project }
        println "testiAddedResult: ${testiAddedResult.size()}"

        testiAddedControllerResult = importFileResults(outputTestiAndRandomiFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[6] as double,
                            recall:it[7] as double] }?.groupBy{ it.project }
        println "testiAddedControllerResult: ${testiAddedControllerResult.size()}"

        testiAddedWhenResult = importFileResults(outputTestiAndRandomiFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[12] as double,
                            recall:it[13] as double] }?.groupBy{ it.project }
        println "testiAddedWhenResult: ${testiAddedWhenResult.size()}"

        testiAddedWhenControllerResult = importFileResults(outputTestiAndRandomiFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[16] as double,
                            recall:it[17] as double] }?.groupBy{ it.project }
        println "testiAddedWhenControllerResult: ${testiAddedWhenControllerResult.size()}"

        testiAllResult = importFileResults(outputTestiAndRandomiFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[2] as double,
                            recall:it[3] as double] }?.groupBy{ it.project }
        println "testiAllResult: ${testiAllResult.size()}"

        testiAllControllerResult = importFileResults(outputTestiAndRandomiFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[6] as double,
                            recall:it[7] as double] }?.groupBy{ it.project }
        println "testiAllControllerResult: ${testiAllControllerResult.size()}"

        testiAllWhenResult = importFileResults(outputTestiAndRandomiFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[10] as double,
                            recall:it[11] as double] }?.groupBy{ it.project }
        println "testiAllWhenResult: ${testiAllWhenResult.size()}"

        testiAllWhenControllerResult = importFileResults(outputTestiAndRandomiFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[14] as double,
                            recall:it[15] as double] }?.groupBy{ it.project }
        println "testiAllWhenControllerResult: ${testiAllWhenControllerResult.size()}"

        randomiResult = importFileResults(outputTestiAndRandomiFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[18] as double,
                            recall:it[19] as double] }?.groupBy{ it.project }
        println "randomiResult: ${randomiResult.size()}"

        randomiControllerResult = importFileResults(outputTestiAndRandomiFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[20] as double,
                            recall:it[21] as double] }?.groupBy{ it.project }
        println "randomiControllerResult: ${randomiControllerResult.size()}"

        dtestiResult = importFileResults(outputDtestiFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[6] as double,
                            recall:it[7] as double] }?.groupBy{ it.project }
        println "dtestiResult: ${dtestiResult.size()}"

        dtestiControllerResult = importFileResults(outputDtestiControllerFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[6] as double,
                            recall:it[7] as double] }?.groupBy{ it.project }
        println "dtestiControllerResult: ${dtestiControllerResult.size()}"

        textiResult = importFileResults(outputTextiFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[2] as double,
                            recall:it[3] as double] }?.groupBy{ it.project }
        println "textiResult: ${textiResult.size()}"

        textiControllerResult = importFileResults(outputTextiControllerFile)
                ?.collect{ [project:formatProjectName(it[PROJECT_INDEX]), id:it[TASK_INDEX] as int,
                            precision:it[2] as double,
                            recall:it[3] as double] }?.groupBy{ it.project }
        println "textiControllerResult: ${textiControllerResult.size()}"
    }

    private configureProjects(){
        projects = testiAddedResult.keySet() as List
        println "projects: ${projects.size()}"
        projects.each{ println it }
    }

    def organizeResultsPerProject(){
        projects.each{ project ->
            def adPrecision = testiAddedResult.get(project).collect{it.precision as double} as double[]
            def adRecall = testiAddedResult.get(project).collect{it.recall as double} as double[]
            def ad = [precision: generateStatistics(adPrecision), recall: generateStatistics(adRecall)]

            def adcPrecision = testiAddedControllerResult.get(project).collect{it.precision as double} as double[]
            def adcRecall = testiAddedControllerResult.get(project).collect{it.recall as double} as double[]
            def adc = [precision: generateStatistics(adcPrecision), recall: generateStatistics(adcRecall)]

            def adwPrecision = testiAddedWhenResult.get(project).collect{it.precision as double} as double[]
            def adwRecall = testiAddedWhenResult.get(project).collect{it.recall as double} as double[]
            def adw = [precision: generateStatistics(adwPrecision), recall: generateStatistics(adwRecall)]

            def adwcPrecision = testiAddedWhenControllerResult.get(project).collect{it.precision as double} as double[]
            def adwcRecall = testiAddedWhenControllerResult.get(project).collect{it.recall as double} as double[]
            def adwc = [precision: generateStatistics(adwcPrecision), recall: generateStatistics(adwcRecall)]

            def cPrecision = testiAllResult.get(project).collect{it.precision as double} as double[]
            def cRecall = testiAllResult.get(project).collect{it.recall as double} as double[]
            def c = [precision: generateStatistics(cPrecision), recall: generateStatistics(cRecall)]

            def ccPrecision = testiAllControllerResult.get(project).collect{it.precision as double} as double[]
            def ccRecall = testiAllControllerResult.get(project).collect{it.recall as double} as double[]
            def cc = [precision: generateStatistics(ccPrecision), recall: generateStatistics(ccRecall)]

            def cwPrecision = testiAllWhenResult.get(project).collect{it.precision as double} as double[]
            def cwRecall = testiAllWhenResult.get(project).collect{it.recall as double} as double[]
            def cw = [precision: generateStatistics(cwPrecision), recall: generateStatistics(cwRecall)]

            def cwcPrecision = testiAllWhenControllerResult.get(project).collect{it.precision as double} as double[]
            def cwcRecall = testiAllWhenControllerResult.get(project).collect{it.recall as double} as double[]
            def cwc = [precision: generateStatistics(cwcPrecision), recall: generateStatistics(cwcRecall)]

            def randomiPrecision = randomiResult.get(project).collect{it.precision as double} as double[]
            def randomiRecall = randomiResult.get(project).collect{it.recall as double} as double[]
            def randomi = [precision: generateStatistics(randomiPrecision), recall: generateStatistics(randomiRecall)]

            def crandomiPrecision = randomiControllerResult.get(project).collect{it.precision as double} as double[]
            def crandomiRecall = randomiControllerResult.get(project).collect{it.recall as double} as double[]
            def crandomi = [precision: generateStatistics(crandomiPrecision), recall: generateStatistics(crandomiRecall)]

            def textiPrecision = textiResult.get(project).collect{it.precision as double} as double[]
            def textiRecall = textiResult.get(project).collect{it.recall as double} as double[]
            def texti = [precision: generateStatistics(textiPrecision), recall: generateStatistics(textiRecall)]

            def ctextiPrecision = textiControllerResult.get(project).collect{it.precision as double} as double[]
            def ctextiRecall = textiControllerResult.get(project).collect{it.recall as double} as double[]
            def ctexti = [precision: generateStatistics(ctextiPrecision), recall: generateStatistics(ctextiRecall)]

            def dtestiPrecision = dtestiResult.get(project).collect{it.precision as double} as double[]
            def dtestiRecall = dtestiResult.get(project).collect{it.recall as double} as double[]
            def dtesti = [precision: generateStatistics(dtestiPrecision), recall: generateStatistics(dtestiRecall)]

            def cdtestiPrecision = dtestiControllerResult.get(project).collect{it.precision as double} as double[]
            def cdtestiRecall = dtestiControllerResult.get(project).collect{it.recall as double} as double[]
            def cdtesti = [precision: generateStatistics(cdtestiPrecision), recall: generateStatistics(cdtestiRecall)]

            def n = testiAddedResult.get(project).size()
            results += [project:project, n:n, ad:ad, adc:adc, adw:adw, adwc:adwc, c:c, cc:cc, cw:cw, cwc:cwc,
                        randomi:randomi, crandomi:crandomi, texti:texti, ctexti:ctexti, dtesti:dtesti, cdtesti:cdtesti]
        }
    }

    def exportResult(){
        List<String[]> content = []
        content += ["", "", "AD", "", "", "", "", "", "ADC", "", "", "", "", "", "ADW", "", "", "", "", "",
                    "ADWC", "", "", "", "", "", "C", "", "", "", "", "", "CC", "", "", "", "", "",
                    "CW", "", "", "", "", "", "CWC", "", "", "", "", "", "R", "", "", "", "", "",
                    "CR", "", "", "", "", "", "T", "", "", "", "", "", "CT", "", "", "", "", "",
                    "D", "", "", "", "", "", "CD", "", "", "", "", ""] as String[]
        content += [" ", " ",
                    "Precision", "", "", "Recall",  "", "", "Precision",  "", "", "Recall",  "", "", "Precision",  "", "",
                    "Recall",  "", "", "Precision",  "", "", "Recall", "", "", "Precision",  "", "", "Recall",  "", "",
                    "Precision",  "", "", "Recall",  "", "", "Precision",  "", "", "Recall",  "", "", "Precision",  "", "",
                    "Recall", "", "", "Precision",  "", "", "Recall", "", "", "Precision",  "", "", "Recall", "", "",
                    "Precision",  "", "", "Recall", "", "", "Precision",  "", "", "Recall", "", "",
                    "Precision",  "", "", "Recall", "", "", "Precision",  "", "", "Recall", "", ""] as String[]
        content += ["Project", "Tasks",
                    "Mean", "Median", "SD", "Mean", "Median", "SD", "Mean", "Median", "SD", "Mean", "Median", "SD",
                    "Mean", "Median", "SD", "Mean", "Median", "SD", "Mean", "Median", "SD", "Mean", "Median", "SD",
                    "Mean", "Median", "SD", "Mean", "Median", "SD", "Mean", "Median", "SD", "Mean", "Median", "SD",
                    "Mean", "Median", "SD", "Mean", "Median", "SD", "Mean", "Median", "SD", "Mean", "Median", "SD",
                    "Mean", "Median", "SD", "Mean", "Median", "SD", "Mean", "Median", "SD", "Mean", "Median", "SD",
                    "Mean", "Median", "SD", "Mean", "Median", "SD", "Mean", "Median", "SD", "Mean", "Median", "SD",
                    "Mean", "Median", "SD", "Mean", "Median", "SD", "Mean", "Median", "SD", "Mean", "Median", "SD"] as String[]
        results.each{ r ->
            content += [r.project, r.n,

                        r.ad.precision.mean,
                        r.ad.precision.median,
                        r.ad.precision.sd,
                        r.ad.recall.mean,
                        r.ad.recall.median,
                        r.ad.recall.sd,

                        r.adc.precision.mean,
                        r.adc.precision.median,
                        r.adc.precision.sd,
                        r.adc.recall.mean,
                        r.adc.recall.median,
                        r.adc.recall.sd,

                        r.adw.precision.mean,
                        r.adw.precision.median,
                        r.adw.precision.sd,
                        r.adw.recall.mean,
                        r.adw.recall.median,
                        r.adw.recall.sd,

                        r.adwc.precision.mean,
                        r.adwc.precision.median,
                        r.adwc.precision.sd,
                        r.adwc.recall.mean,
                        r.adwc.recall.median,
                        r.adwc.recall.sd,

                        r.c.precision.mean,
                        r.c.precision.median,
                        r.c.precision.sd,
                        r.c.recall.mean,
                        r.c.recall.median,
                        r.c.recall.sd,

                        r.cc.precision.mean,
                        r.cc.precision.median,
                        r.cc.precision.sd,
                        r.cc.recall.mean,
                        r.cc.recall.median,
                        r.cc.recall.sd,

                        r.cw.precision.mean,
                        r.cw.precision.median,
                        r.cw.precision.sd,
                        r.cw.recall.mean,
                        r.cw.recall.median,
                        r.cw.recall.sd,

                        r.cwc.precision.mean,
                        r.cwc.precision.median,
                        r.cwc.precision.sd,
                        r.cwc.recall.mean,
                        r.cwc.recall.median,
                        r.cwc.recall.sd,

                        r.randomi.precision.mean,
                        r.randomi.precision.median,
                        r.randomi.precision.sd,
                        r.randomi.recall.mean,
                        r.randomi.recall.median,
                        r.randomi.recall.sd,

                        r.crandomi.precision.mean,
                        r.crandomi.precision.median,
                        r.crandomi.precision.sd,
                        r.crandomi.recall.mean,
                        r.crandomi.recall.median,
                        r.crandomi.recall.sd,

                        r.texti.precision.mean,
                        r.texti.precision.median,
                        r.texti.precision.sd,
                        r.texti.recall.mean,
                        r.texti.recall.median,
                        r.texti.recall.sd,

                        r.ctexti.precision.mean,
                        r.ctexti.precision.median,
                        r.ctexti.precision.sd,
                        r.ctexti.recall.mean,
                        r.ctexti.recall.median,
                        r.ctexti.recall.sd,

                        r.dtesti.precision.mean,
                        r.dtesti.precision.median,
                        r.dtesti.precision.sd,
                        r.dtesti.recall.mean,
                        r.dtesti.recall.median,
                        r.dtesti.recall.sd,

                        r.cdtesti.precision.mean,
                        r.cdtesti.precision.median,
                        r.cdtesti.precision.sd,
                        r.cdtesti.recall.mean,
                        r.cdtesti.recall.median,
                        r.cdtesti.recall.sd] as String[]
        }
        CsvUtil.write(outputFile, content)
    }

}
