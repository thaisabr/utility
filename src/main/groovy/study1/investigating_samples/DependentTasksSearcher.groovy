package study1.investigating_samples

import br.ufpe.cin.tan.analysis.data.ExporterUtil
import br.ufpe.cin.tan.analysis.task.DoneTask
import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

class DependentTasksSearcher {

    int idIndex //index no csv de tarefas
    int hashesIndex //index no csv de tarefas
    List tasks
    def tasksFolder
    def files

    DependentTasksSearcher(String tasksFolder){
        this.idIndex = 1
        this.hashesIndex = 3
        this.tasksFolder = tasksFolder
        this.files = Util.findFilesFromDirectory(tasksFolder).findAll{
            it.endsWith("74_tasks.csv") || it.endsWith("463_tasks.csv") }
    }

    static void main(String[] args){
        def smallerTasksFolder = "tasks\\temp\\tasksPerSample\\smaller"
        def largerTasksFolder = "tasks\\temp\\tasksPerSample\\463"

        def searcher = new DependentTasksSearcher(smallerTasksFolder)
        searcher.analyse()

        searcher = new DependentTasksSearcher(largerTasksFolder)
        searcher.analyse()

    }

    def analyse(){
        files.each{ file ->
            println "File: $file"
            updateTasks(file)
            def tasksId = tasks.collect{ it[idIndex] as int }.sort()
            println "tasks: ${tasksId.size()}"

            def selfContained = filterSelfContainedTasks()
            def selfContainedId = selfContained.collect{ it[idIndex]  as int }.sort()
            println "selfContained: ${selfContainedId.size()}"

            def notSelfContained = tasks - selfContained
            def notSelfContainedId = notSelfContained.collect{ it[idIndex]  as int }.sort()
            println "not selfContained: ${notSelfContainedId.size()}"

            def identical = tasks.groupBy { it[hashesIndex] }
            identical = identical.findAll{ it.value.size()>1 }
            println "identical tasks: ${identical.size()}"
            identical.eachWithIndex{ value, index ->
                println "pair ${index+1}:"
                value.value.each{
                    println "url: ${it[0]}, id: ${it[1]}, #hashes: ${it[2]}, hashes: ${it[3]}"
                }
            }
        }
    }

    private updateTasks(String file){
        tasks = []
        def entries = CsvUtil.read(file)
        entries.remove(0)
        tasks += entries
    }

    private filterSelfContainedTasks(){
        def selfContained = []
        //["task_a", "hashes_a", "task_b", "hashes_b" , "intersection", "%_a", "%_b"]
        def hashesSimilarity = computeHashSimilarity()
        if(hashesSimilarity.empty) return
        List<DoneTask> maxSimResult = []
        def pairsMaxSimilarity = hashesSimilarity.findAll{ (it[5]==1) || (it[6]==1) }
        def ids = tasks.collect{ it[idIndex] }
        ids.each{ id ->
            def n = pairsMaxSimilarity.findAll{ it[0]==id || it[2]==id }
            if(n.size()>0) {
                List<DoneTask> temp = []
                n.each{ pair ->
                    if(pair[1]==pair[3]){
                        def commitsNumber = pair[1] as String
                        def idPair = [pair[0] as int, pair[2] as int].sort()
                        temp += tasks.find{ it[idIndex] == (idPair[0] as String) && it[2] == commitsNumber }
                    } else if(pair[0]==id && pair[1]<pair[3] ){
                        temp += tasks.find{ it[idIndex] == pair[0] }
                    } else if(pair[2]==id && pair[3]<pair[1]){
                        temp += tasks.find{ it[idIndex] == pair[2] }
                    }
                }
                maxSimResult += temp//.unique()
            }
        }
        maxSimResult = maxSimResult.unique()
        println "maxSimResult: ${maxSimResult.size()}"
        maxSimResult.sort{ it[0] }.sort{it[1] as int }.each{ println it }

        selfContained = (tasks - maxSimResult).sort{ it[idIndex] as double }
        selfContained
    }

    private computeHashSimilarity(){
        def hashesSimilarity = []
        def taskPairs = ExporterUtil.computeTaskPairs(tasks)
        if(taskPairs.empty) return hashesSimilarity
        taskPairs?.each { item ->
            def task = item.task
            def hashes1 = task[hashesIndex].tokenize(',[]')*.trim()
            item.pairs?.each { other ->
                def hashes2 = other[hashesIndex].tokenize(',[]')*.trim()
                def intersection = hashes1.intersect(hashes2).size()
                hashesSimilarity.add([task[idIndex], hashes1.size(), other[idIndex], hashes2.size(), intersection,
                                      intersection/hashes1.size(), intersection/hashes2.size()])
            }
        }
        hashesSimilarity
    }

}
