package study1.investigating_samples

import br.ufpe.cin.tan.util.CsvUtil
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

class IntersectionTestiTexti {

    static final int PROJECT_INDEX = 0
    static final int TASK_INDEX = 1
    static final int ITEXT_INDEX = 9
    static final int ITEST_INDEX = 9

    static void main(String[] args){
        def itextFile1 = "tasks\\temp\\cucumber_sample_task_interfaces_463\\itext.csv"
        def itestFile1 = "tasks\\temp\\cucumber_sample_task_interfaces_463\\itest_all.csv"
        analyse(itextFile1, itestFile1)

        def itextFile2 = "tasks\\temp\\cucumber_sample_task_interfaces_463\\itext_controller.csv"
        def itestFile2 = "tasks\\temp\\cucumber_sample_task_interfaces_463\\itest_all_controller.csv"
        analyse(itextFile2, itestFile2)
    }

    static analyse(String itextFile, String itestFile){
        println "itextFile: ${itextFile}"
        println "itestFile: ${itestFile}"

        def itextResult = CsvUtil.read(itextFile)
        itextResult.remove(0)
        //itextResult = itextResult.subList(1, itextResult.size()-15)
        def itestResult = CsvUtil.read(itestFile)
        itestResult.remove(0)
        //itestResult = itestResult.subList(1, itestResult.size()-15)

        def pairs = []
        itextResult.each{ r ->
            def match = itestResult.find {
                it[PROJECT_INDEX] == r[PROJECT_INDEX] &&
                        it[TASK_INDEX] == r[TASK_INDEX]
            }
            if(match){
                def itext = []
                if(r[ITEXT_INDEX].size()>2) itext = r[ITEXT_INDEX].tokenize(',[]')*.trim()
                def itest = []
                if(match[ITEST_INDEX].size()>2) itest = match[ITEST_INDEX].tokenize(',[]')*.trim()
                def inter = itext.intersect(itest)
                def union = (itext+itest) - inter
                def rateIText = 0
                if(!itext.empty) rateIText = inter.size()/itext.size()
                def rateITest = 0
                if(!itest.empty) rateITest = inter.size()/itest.size()
                pairs += [project:r[PROJECT_INDEX], task:r[TASK_INDEX], itext:itext, itest:itest, inter:inter,
                          union:union, rateIText:rateIText, rateITest:rateITest]
            }
        }

        println "pairs: ${pairs.size()}"
        def rateIText = pairs.collect{ it.rateIText as double } as double[]
        def statsRateIText = generateStatistics(rateIText)
        println "statsRateIText: ${statsRateIText}"
        def rateITest = pairs.collect{ it.rateITest as double } as double[]
        def statsRateITest = generateStatistics(rateITest)
        println "statsRateITest: ${statsRateITest}\n"
    }

    private static generateStatistics(double[] values){
        def statistics = new DescriptiveStatistics(values)
        [mean: statistics.mean, median:statistics.getPercentile(50.0), sd:statistics.standardDeviation]
    }

}
