package study1.investigating_samples

import br.ufpe.cin.tan.util.CsvUtil

class IntersectionAmongSamples {

    static void main(String[] args){
        def sample_74 = CsvUtil.read("tasks\\temp\\tasksPerSample\\smaller\\74_tasks.csv")
        sample_74.remove(0)
        def sample_463 = CsvUtil.read("tasks\\temp\\tasksPerSample\\463\\463_tasks.csv")
        sample_463.remove(0)

        //def intersection = sample_74.intersect(sample_463)
        def intersection = []
        sample_74.each{ task ->
            def found = sample_463.find{
                it[0]==task[0] &&
                it[2]==task[2] &&
                it[3]==task[3] &&
                it[4]==task[4] &&
                it[5]==task[5] &&
                it[6]==task[6]
            }
            if(found){
                intersection += [project: task[0], taskLargerSample: found[1], taskSmallerSample: task[1]]
            }
        }

        println "intersection: ${intersection.size()}"
        intersection.each{
            println it
        }
    }

}
