package study1.investigating_samples

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

class UnknownMethodCallAndNotFoundViewsSmallerSample {

    final int TASK_INDEX = 0
    final int UNKNOWN_METHODS_INDEX = 10
    final int NOT_FOUND_VIEWS_INDEX = 23
    List detailedResultFiles
    List output
    String inputFolder
    String outputFileName

    UnknownMethodCallAndNotFoundViewsSmallerSample(String inputFolder, String outputFileName){
        this.inputFolder = inputFolder
        this.outputFileName = outputFileName
        initDetailedResultFiles()
    }

    static void main(String[] args){
        def folder = "D:\\current\\task_organization\\task_analysis\\4-results-sample4\\individual-calculatedRoutes"
        def outputFileName = "output${File.separator}unknownMethodCall_smaller_sample.csv"
        def analyser = new UnknownMethodCallAndNotFoundViewsSmallerSample(folder, outputFileName)
        analyser.extractResults()
        analyser.exportResult()
    }

    private static importFileResults(String file){
        def tasks = CsvUtil.read(file)
        def project = tasks[0][1]
        [project:project, tasks:tasks.subList(2, tasks.size())]
    }

    private initDetailedResultFiles(){
        def aux = Util.findFilesFromDirectory(inputFolder)
        detailedResultFiles = aux.findAll{
            !it.contains("${File.separator}localsupport${File.separator}") &&
            it.contains("${File.separator}output_all${File.separator}") &&
            it.contains("${File.separator}selected${File.separator}") &&
            it.endsWith("-relevant-detailed.csv")
        }
        println "detailedResultFiles: ${detailedResultFiles.size()}"
        detailedResultFiles.each{ println it }
    }

    private static generateStatistics(double[] values){
        def statistics = new DescriptiveStatistics(values)
        [mean: statistics.mean, median: statistics.getPercentile(50.0), sd: statistics.standardDeviation]
    }

    def extractResults(){
        output = []
        detailedResultFiles.each{ file ->
            def r = importFileResults(file)
            def project = r.project
            r.tasks?.each{
                def id = it[TASK_INDEX] as int
                def calls = it[UNKNOWN_METHODS_INDEX].tokenize(',[]')*.trim().unique()
                def notFoundViews = it[NOT_FOUND_VIEWS_INDEX] as int
                output += [project:project, task:id, number:calls.size(), methods:calls, notFoundViews:notFoundViews]
            }
        }
        output = output.sort{ [it.project, it.task] }
        println "output: ${output.size()}"
        output.each{ println "${it.project}, ${it.task}, ${it.number}"}
    }

    def exportResult(){
        List<String[]> result = []
        result += ["Project", "Task", "#Unknown_methods", "Unknown_methods", "#Not_found_views"] as String[]
        output.each{
            result += [it.project, it.task, it.number, it.methods, it.notFoundViews] as String[]
        }
        def methodsStats = generateStatistics(output.collect{it.number as double} as double[])
        def viewsStats = generateStatistics(output.collect{it.notFoundViews as double} as double[])
        result += ["", "Mean", methodsStats.mean, "", viewsStats.mean] as String[]
        result += ["", "Median", methodsStats.median, "", viewsStats.median] as String[]
        result += ["", "SD", methodsStats.sd, "", viewsStats.sd] as String[]
        CsvUtil.write(outputFileName, result)
    }

}
