package study1.input

import br.ufpe.cin.tan.util.CsvUtil

/*
* Estabelece a relação entre 2 csvs de tarefa que possuem tarefas em comum, mas que estão identificadas por ID diferentes.
* O csv mais atual contém X + Y tarefas, enquanto que o mais antigo só contém Y, que são tarefas mais antigas que X. Como
* o csv ordena por ordem decrescente de data de criação, as tarefas mais atuais estão na frente, havendo esse descompasso
* de id.
* */

class OrganizeDataInput {

    String oldCsv
    List<String[]> dataOldCsv
    String newCsv
    List<String[]> dataNewCsv

    OrganizeDataInput(String oldCsv, String newCsv) {
        this.oldCsv = oldCsv
        this.newCsv = newCsv
        dataOldCsv = extractData(oldCsv)
        dataNewCsv = extractData(newCsv)
    }

    static extractData(String csv){
        List<String[]> entries = CsvUtil.read(csv)
        entries.subList(1,entries.size()).sort{ it[1] as int }
    }

    //"REPO_URL","TASK_ID","#HASHES","HASHES","#PROD_FILES","#TEST_FILES","LAST"
    def compareEntries(){
        def matches = []
        List<String[]> result = []
        dataOldCsv.each{ task ->
            def r = dataNewCsv.find{ it[2] == task[2] && it[3] == task[3] && it[4] == task[4] && it[5] == task[5] &&
                    it[6] == task[6] }
            if(r) {
                matches += [old:task[1], new:r[1]]
                result += r
            }
        }

        def name = "tasks\\temp\\whitehall-newTasks1.csv"
        CsvUtil.write(name, result)
        matches
    }

    static void main(String[] args){
        def oldfile = "tasks\\temp\\cucumber_sample\\websiteone\\tasks_correspondencia\\websiteone_running_old.csv"
        def newfile = "tasks\\temp\\cucumber_sample\\websiteone\\tasks_correspondencia\\AgileVentures_WebsiteOne.csv"
        OrganizeDataInput organizeDataInput = new OrganizeDataInput(oldfile, newfile)
        def matches = organizeDataInput.compareEntries()
        println "matches: ${matches.size()}"
        matches.each{ println it }
    }


}
