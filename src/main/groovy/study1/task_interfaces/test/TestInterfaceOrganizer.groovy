package study1.task_interfaces.test

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

class TestInterfaceOrganizer {

    String folder
    List<String> controllerFiles
    List<String> files
    String outputControllerFile
    String outputFile

    TestInterfaceOrganizer(String folder){
        this.folder = folder
        outputControllerFile = "$folder${File.separator}itest_all-controller.csv"
        outputFile = "$folder${File.separator}itest_all.csv"
        findRelevantControllerFiles()
        findRelevantFiles()
    }

    def summarize(){
        summarizeResults()
        summarizeControllerResults()
    }

    private static extractProjectName(String name){
        def i = name.indexOf("-relevant")
        def j = name.lastIndexOf(File.separator)
        name.substring(j+1, i)
    }

    private summarizeControllerResults(){
        def content = summarizeData(controllerFiles)
        CsvUtil.write(outputControllerFile, content)
    }

    private summarizeResults(){
        def content = summarizeData(files)
        CsvUtil.write(outputFile, content)
    }

    private static summarizeData(resultFiles){
        /*
        "TASK","#DAYS","#DEVS","#COMMITS","HASHES","#GHERKIN_TESTS","#STEP_DEFS","#ITest","#IReal","ITest","IReal",
        "Precision","Recall","RAILS","#visit_call","lost_visit_call","#ITest_views","#view_analysis_code",
        "view_analysis_code","methods_no_origin","renamed_files","deleted_files","noFound_views","#noFound_views",
        "TIMESTAMP","has_merge","#FP","#FN","FP","FN","#Hits","Hits","f2"
        */
        String[] firstLine = ["Project", "Task", "Precision", "Recall", "ITest_size", "IReal_size", "FP_size", "FN_size",
                              "Hits_size", "ITest", "IReal", "F2"] as String[]
        List<String[]> content = []
        List<String[]> data = []
        content +=  firstLine
        resultFiles.each{ file ->
            String[] values = []
            values += extractProjectName(file)
            def lines = CsvUtil.read(file)
            lines = lines.subList(13, lines.size())
            def temp = lines.collect {
                def interest = [it[0], it[11], it[12], it[7], it[8], it[26], it[27], it[30], it[9], it[10], it[32]] as String[]
                (values + interest) as String[]
            }
            data += temp.sort{ it[1] as double }
        }

        def precision = generateStatistics(data.collect{ it[2] as double } as double[], "precision")
        def recall = generateStatistics(data.collect{ it[3] as double } as double[], "recall")
        def fp = generateStatistics(data.collect{ it[6] as double } as double[], "#fp")
        def fn = generateStatistics(data.collect{ it[7] as double } as double[], "#fn")
        def f2 = generateStatistics(data.collect{ it[11] as double } as double[], "f2")
        content +=  data
        content +=  precision
        content +=  recall
        content +=  fp
        content +=  fn
        content += f2
        content
    }

    private static generateStatistics(double[] values, String text){
        List<String[]> content = []
        def mean = ["Mean "+text]
        def median = ["Median "+text]
        def sd = ["SD "+text]
        def statistics = new DescriptiveStatistics(values)
        mean.add(statistics.mean as String)
        median.add(statistics.getPercentile(50.0) as String)
        sd.add(statistics.standardDeviation as String)
        content += mean as String[]
        content += median as String[]
        content += sd as String[]
        content
    }

    private findRelevantControllerFiles(){
        def aux = Util.findFilesFromDirectory(folder)
        controllerFiles = aux.findAll{
            it.endsWith("-relevant-controller.csv") &&
            !it.contains("${File.separator}evaluation${File.separator}") &&
            it.contains("${File.separator}output_all${File.separator}") &&
            it.contains("${File.separator}selected${File.separator}")
        }
        println "relevant controllerFiles: ${controllerFiles.size()}"
        controllerFiles.each{ println it }
    }

    private findRelevantFiles(){
        def aux = Util.findFilesFromDirectory(folder)
        files = aux.findAll{
            it.endsWith("-relevant.csv") &&
            !it.contains("${File.separator}evaluation${File.separator}") &&
            it.contains("${File.separator}output_all${File.separator}") &&
            it.contains("${File.separator}selected${File.separator}")
        }
        println "relevant files: ${files.size()}"
        files.each{ println it }
    }

    static void main(String[] args){
        def folder1 = "results${File.separator}sample4${File.separator}random1-calculatedRoutes"
        TestInterfaceOrganizer evaluator1 = new TestInterfaceOrganizer(folder1)
        evaluator1.summarize()

        def folder2 = "results${File.separator}sample4${File.separator}random1-railsRoutes"
        TestInterfaceOrganizer evaluator2 = new TestInterfaceOrganizer(folder2)
        evaluator2.summarize()

        def folder3 = "results${File.separator}sample4${File.separator}random2-calculatedRoutes"
        TestInterfaceOrganizer evaluator3 = new TestInterfaceOrganizer(folder3)
        evaluator3.summarize()

        def folder4 = "results${File.separator}sample4${File.separator}random2-railsRoutes"
        TestInterfaceOrganizer evaluator4 = new TestInterfaceOrganizer(folder4)
        evaluator4.summarize()

        /*
        def files = Util.findFilesFromDirectory("results${File.separator}sample4").findAll{
            !it.contains("${File.separator}evaluation${File.separator}") &&
                    (it.endsWith("itest.csv") || it.endsWith("itest-controller.csv"))
        }
        println "files: ${files.size()}"
        files.each{
            def index = it.lastIndexOf(File.separator)
            def oldName = it.substring(index+1)
            def newName = oldName.replace("itest", "itest_all")
            def finalName = it.substring(0, index+1) + newName
            //println it
            println finalName
            File file = new File(it)
            def result = file.renameTo(finalName)
            println result
        }*/
    }

}
