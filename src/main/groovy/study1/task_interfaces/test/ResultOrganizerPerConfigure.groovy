package study1.task_interfaces.test

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

class ResultOrganizerPerConfigure {

    String folder

    List<String> addedFiles
    List<String> addedControllerFiles
    List<String> addedWhenFiles
    List<String> addedWhenControllerFiles
    List<String> allFiles
    List<String> allControllerFiles
    List<String> allWhenFiles
    List<String> allWhenControllerFiles

    String outputAddedFile
    String outputAddedControllerFile
    String outputAddedWhenFile
    String outputAddedWhenControllerFile
    String outputAllFile
    String outputAllControllerFile
    String outputAllWhenFile
    String outputAllWhenControllerFile

    ResultOrganizerPerConfigure(String folder){
        this.folder = folder

        outputAddedFile = "$folder${File.separator}selected_added.csv"
        outputAddedControllerFile = "$folder${File.separator}selected_added_controller.csv"
        outputAddedWhenFile = "$folder${File.separator}selected_added_when.csv"
        outputAddedWhenControllerFile = "$folder${File.separator}selected_added_when_controller.csv"
        outputAllFile = "$folder${File.separator}selected_all.csv"
        outputAllControllerFile = "$folder${File.separator}selected_all_controller.csv"
        outputAllWhenFile = "$folder${File.separator}selected_all_when.csv"
        outputAllWhenControllerFile = "$folder${File.separator}selected_all_when_controller.csv"

        findRelevantFiles()
        findRelevantControllerFiles()
    }

    def summarize(){
        def content = summarizeData(addedFiles)
        CsvUtil.write(outputAddedFile, content)

        content = summarizeData(addedControllerFiles)
        CsvUtil.write(outputAddedControllerFile, content)

        content = summarizeData(addedWhenFiles)
        CsvUtil.write(outputAddedWhenFile, content)

        content = summarizeData(addedWhenControllerFiles)
        CsvUtil.write(outputAddedWhenControllerFile, content)

        content = summarizeData(allFiles)
        CsvUtil.write(outputAllFile, content)

        content = summarizeData(allControllerFiles)
        CsvUtil.write(outputAllControllerFile, content)

        content = summarizeData(allWhenFiles)
        CsvUtil.write(outputAllWhenFile, content)

        content = summarizeData(allWhenControllerFiles)
        CsvUtil.write(outputAllWhenControllerFile, content)
    }

    private static extractProjectName(String name){
        def i = name.indexOf("-relevant")
        def j = name.lastIndexOf(File.separator)
        name.substring(j+1, i)
    }

    private static countLostVisitCalls(String calls){
        def values = calls.substring(1, calls.size()-1)
        values = values.tokenize('{')*.trim().collect{ "{" + it.replace("},", "}") }
        def result = []
        values.each{
            def index1 = it.indexOf(",")
            def init1 = "{path="
            def init2 = ", line="
            def index2 = it.indexOf(init2)
            result += [path: it.substring(init1.size(), index1), line:it.substring(index2+init2.size(), it.size()-1) as double]
        }
        result = result.unique()
        result
    }

    private static summarizeData(resultFiles){
        /*33
        "TASK","#DAYS","#DEVS","#COMMITS","HASHES","#GHERKIN_TESTS","#STEP_DEFS","#ITest","#IReal","ITest","IReal",
        "Precision","Recall","RAILS","#visit_call","lost_visit_call","#ITest_views","#view_analysis_code",
        "view_analysis_code","methods_no_origin","renamed_files","deleted_files","noFound_views","#noFound_views",
        "TIMESTAMP","has_merge","#FP","#FN","FP","FN","#Hits","Hits","f2"
        */
        int lost_visit = 15
        String[] firstLine = ["project", "task", "days", "devs", "commits", "hashes", "tests", "stepdef", "itest_size",
                              "ireal_size", "itest", "ireal", "precision", "recall", "rails", "visit_number",
                              "lost_visit", "itest_views_number", "files_view_analysis_number", "files_view_analysis",
                              "methods_no_origin", "renamed_files", "deleted_files", "noFound_views",
                              "noFound_views_number", "timestamp", "has_merge", "fp_size", "fn_size", "fp", "fn",
                              "hits_size", "hits", "f2", "lost_visit_number"] as String[]
        List<String[]> content = []
        List<String[]> data = []
        content +=  firstLine
        resultFiles.each{ file ->
            String[] values = []
            values += extractProjectName(file)
            def lines = CsvUtil.read(file)
            lines = lines.subList(13, lines.size())
            def temp = lines.collect {
                def lostCalls = countLostVisitCalls(it[lost_visit])
                (values + it + [lostCalls.size()]) as String[]
            }
            data += temp.sort{ it[1] as double }
        }

        def precision = generateStatistics(data.collect{ it[12] as double } as double[], "precision")
        def recall = generateStatistics(data.collect{ it[13] as double } as double[], "recall")
        def fp = generateStatistics(data.collect{ it[27] as double } as double[], "#fp")
        def fn = generateStatistics(data.collect{ it[28] as double } as double[], "#fn")
        def f2 = generateStatistics(data.collect{ it[33] as double } as double[], "f2")
        content +=  data
        content +=  precision
        content +=  recall
        content +=  fp
        content +=  fn
        content += f2
        content
    }

    private static generateStatistics(double[] values, String text){
        List<String[]> content = []
        def mean = ["Mean "+text]
        def median = ["Median "+text]
        def sd = ["SD "+text]
        def statistics = new DescriptiveStatistics(values)
        mean.add(statistics.mean as String)
        median.add(statistics.getPercentile(50.0) as String)
        sd.add(statistics.standardDeviation as String)
        content += mean as String[]
        content += median as String[]
        content += sd as String[]
        content
    }

    private static findControllerFilesInFolder(List<String> files, String subfolder){
        files.findAll{
            !it.contains("${File.separator}evaluation${File.separator}") &&
                    it.contains("${File.separator}$subfolder${File.separator}") &&
                    it.contains("${File.separator}selected${File.separator}") &&
                    it.endsWith("-relevant-controller.csv")
        }
    }

    private findRelevantControllerFiles(){
        def aux = Util.findFilesFromDirectory(folder)
        addedControllerFiles = findControllerFilesInFolder(aux, "output_added")
        addedWhenControllerFiles = findControllerFilesInFolder(aux, "output_added_when")
        allControllerFiles = findControllerFilesInFolder(aux, "output_all")
        allWhenControllerFiles = findControllerFilesInFolder(aux, "output_all_when")
        println "added controller files: ${addedControllerFiles.size()}"
        println "added when controller files: ${addedWhenControllerFiles.size()}"
        println "all controller files: ${allControllerFiles.size()}"
        println "all when controller files: ${allWhenControllerFiles.size()}"
    }

    private static findFilesInFolder(List<String> files, String subfolder){
        files.findAll{
            !it.contains("${File.separator}evaluation${File.separator}") &&
                    it.contains("${File.separator}$subfolder${File.separator}") &&
                    it.contains("${File.separator}selected${File.separator}") &&
                    it.endsWith("-relevant.csv")
        }
    }

    private findRelevantFiles(){
        def aux = Util.findFilesFromDirectory(folder)
        addedFiles = findFilesInFolder(aux, "output_added")
        addedWhenFiles = findFilesInFolder(aux, "output_added_when")
        allFiles = findFilesInFolder(aux, "output_all")
        allWhenFiles = findFilesInFolder(aux, "output_all_when")
        println "added files: ${addedFiles.size()}"
        println "added when files: ${addedWhenFiles.size()}"
        println "all files: ${allFiles.size()}"
        println "all when files: ${allWhenFiles.size()}"
    }

    static void main(String[] args){
        def files = Util.findFilesFromDirectory("results${File.separator}sample5-noViews")
        println "files: ${files.size()}"
        files.each {
            File file = new File(it)
            if (it.contains("diaspora_diaspora_cucumber_coverage_selected")){
                def index = it.lastIndexOf(File.separator)
                def oldName = it.substring(index+1)
                def newName = oldName.replace("diaspora_diaspora_cucumber_coverage_selected",
                        "diaspora")
                def finalName = it.substring(0, index+1) + newName
                file.renameTo(finalName)
            }
            if (it.contains("AgileVentures_LocalSupport_cucumber_coverage_selected")){
                def index = it.lastIndexOf(File.separator)
                def oldName = it.substring(index+1)
                def newName = oldName.replace("AgileVentures_LocalSupport_cucumber_coverage_selected",
                        "localsupport")
                def finalName = it.substring(0, index+1) + newName
                file.renameTo(finalName)
            }
            else if (it.contains("TheOdinProject_theodinproject_cucumber_coverage_selected")){
                def index = it.lastIndexOf(File.separator)
                def oldName = it.substring(index+1)
                def newName = oldName.replace("TheOdinProject_theodinproject_cucumber_coverage_selected",
                        "odin")
                def finalName = it.substring(0, index+1) + newName
                file.renameTo(finalName)
            }
            else if (it.contains("oneclickorgs_one-click-orgs_cucumber_coverage_selected")){
                def index = it.lastIndexOf(File.separator)
                def oldName = it.substring(index+1)
                def newName = oldName.replace("oneclickorgs_one-click-orgs_cucumber_coverage_selected",
                        "oneclickorgs")
                def finalName = it.substring(0, index+1) + newName
                file.renameTo(finalName)
            }
            else if (it.contains("otwcode_otwarchive_cucumber_coverage_selected")){
                def index = it.lastIndexOf(File.separator)
                def oldName = it.substring(index+1)
                def newName = oldName.replace("otwcode_otwarchive_cucumber_coverage_selected",
                        "otwarchive")
                def finalName = it.substring(0, index+1) + newName
                file.renameTo(finalName)
            }
            else if (it.contains("rapidftr_RapidFTR_cucumber_coverage_selected")){
                def index = it.lastIndexOf(File.separator)
                def oldName = it.substring(index+1)
                def newName = oldName.replace("rapidftr_RapidFTR_cucumber_coverage_selected",
                        "rapidftr")
                def finalName = it.substring(0, index+1) + newName
                file.renameTo(finalName)
            }
            else if (it.contains("tip4commit_tip4commit_cucumber_coverage_selected")){
                def index = it.lastIndexOf(File.separator)
                def oldName = it.substring(index+1)
                def newName = oldName.replace("tip4commit_tip4commit_cucumber_coverage_selected",
                        "tip4commit")
                def finalName = it.substring(0, index+1) + newName
                file.renameTo(finalName)
            }
            else if (it.contains("TracksApp_tracks_cucumber_coverage_selected")){
                def index = it.lastIndexOf(File.separator)
                def oldName = it.substring(index+1)
                def newName = oldName.replace("TracksApp_tracks_cucumber_coverage_selected",
                        "tracks")
                def finalName = it.substring(0, index+1) + newName
                file.renameTo(finalName)
            }
            else if (it.contains("AgileVentures_WebsiteOne_cucumber_coverage_selected")){
                def index = it.lastIndexOf(File.separator)
                def oldName = it.substring(index+1)
                def newName = oldName.replace("AgileVentures_WebsiteOne_cucumber_coverage_selected",
                        "websiteone")
                def finalName = it.substring(0, index+1) + newName
                file.renameTo(finalName)
            }
            else if (it.contains("alphagov_whitehall_cucumber_coverage_selected")){
                def index = it.lastIndexOf(File.separator)
                def oldName = it.substring(index+1)
                def newName = oldName.replace("alphagov_whitehall_cucumber_coverage_selected",
                        "whitehall")
                def finalName = it.substring(0, index+1) + newName
                file.renameTo(finalName)
            }
        }

        def folder1 = "results${File.separator}sample5-noViews"
        ResultOrganizerPerConfigure evaluator1 = new ResultOrganizerPerConfigure(folder1)
        evaluator1.summarize()
    }

}
