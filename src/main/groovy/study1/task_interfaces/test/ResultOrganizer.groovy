package study1.task_interfaces.test

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

class ResultOrganizer {

    String folder
    List<String> controllerFiles
    List<String> files
    String outputControllerFile
    String outputFile

    ResultOrganizer(String folder){
        this.folder = folder
        outputControllerFile = "$folder${File.separator}selected_relevant_detailed-controller.csv"
        outputFile = "$folder${File.separator}selected_relevant_detailed.csv"
        findRelevantControllerFiles()
        findRelevantFiles()
    }

    def summarize(){
        summarizeResults()
        summarizeControllerResults()
    }

    private static extractProjectName(String name){
        def i = name.indexOf("-relevant")
        def j = name.lastIndexOf(File.separator)
        name.substring(j+1, i)
    }

    private summarizeControllerResults(){
        def content = summarizeData(controllerFiles)
        CsvUtil.write(outputControllerFile, content)
    }

    private summarizeResults(){
        def content = summarizeData(files)
        CsvUtil.write(outputFile, content)
    }

    private static countLostVisitCalls(String calls){
        def values = calls.substring(1, calls.size()-1)
        values = values.tokenize('{')*.trim().collect{ "{" + it.replace("},", "}") }
        def result = []
        values.each{
            def index1 = it.indexOf(",")
            def init1 = "{path="
            def init2 = ", line="
            def index2 = it.indexOf(init2)
            result += [path: it.substring(init1.size(), index1), line:it.substring(index2+init2.size(), it.size()-1) as double]
        }
        result = result.unique()
        result
    }

    private static summarizeData(resultFiles){
        /*33
        "TASK","#DAYS","#DEVS","#COMMITS","HASHES","#GHERKIN_TESTS","#STEP_DEFS","#ITest","#IReal","ITest","IReal",
        "Precision","Recall","RAILS","#visit_call","lost_visit_call","#ITest_views","#view_analysis_code",
        "view_analysis_code","methods_no_origin","renamed_files","deleted_files","noFound_views","#noFound_views",
        "TIMESTAMP","has_merge","#FP","#FN","FP","FN","#Hits","Hits","f2"
        */
        int lost_visit = 15
        String[] firstLine = ["project", "task", "days", "devs", "commits", "hashes", "tests", "stepdef", "itest_size",
                              "ireal_size", "itest", "ireal", "precision", "recall", "rails", "visit_number",
                              "lost_visit", "itest_views_number", "files_view_analysis_number", "files_view_analysis",
                              "methods_no_origin", "renamed_files", "deleted_files", "noFound_views",
                              "noFound_views_number", "timestamp", "has_merge", "fp_size", "fn_size", "fp", "fn",
                              "hits_size", "hits", "f2", "lost_visit_number"] as String[]
        List<String[]> content = []
        List<String[]> data = []
        content +=  firstLine
        resultFiles.each{ file ->
            String[] values = []
            values += extractProjectName(file)
            def lines = CsvUtil.read(file)
            lines = lines.subList(13, lines.size())
            def temp = lines.collect {
                def lostCalls = countLostVisitCalls(it[lost_visit])
                (values + it + [lostCalls.size()]) as String[]
            }
            data += temp.sort{ it[1] as double }
        }

        def precision = generateStatistics(data.collect{ it[12] as double } as double[], "precision")
        def recall = generateStatistics(data.collect{ it[13] as double } as double[], "recall")
        def fp = generateStatistics(data.collect{ it[27] as double } as double[], "#fp")
        def fn = generateStatistics(data.collect{ it[28] as double } as double[], "#fn")
        def f2 = generateStatistics(data.collect{ it[33] as double } as double[], "f2")
        content +=  data
        content +=  precision
        content +=  recall
        content +=  fp
        content +=  fn
        content += f2
        content
    }

    private static generateStatistics(double[] values, String text){
        List<String[]> content = []
        def mean = ["Mean "+text]
        def median = ["Median "+text]
        def sd = ["SD "+text]
        def statistics = new DescriptiveStatistics(values)
        mean.add(statistics.mean as String)
        median.add(statistics.getPercentile(50.0) as String)
        sd.add(statistics.standardDeviation as String)
        content += mean as String[]
        content += median as String[]
        content += sd as String[]
        content
    }

    private findRelevantControllerFiles(){
        def aux = Util.findFilesFromDirectory(folder)
        controllerFiles = aux.findAll{
            !it.contains("${File.separator}evaluation${File.separator}") &&
            it.contains("${File.separator}output_all${File.separator}") &&
            it.contains("${File.separator}selected${File.separator}") &&
            it.endsWith("-relevant-controller.csv")
        }
        println "relevant controllerFiles: ${controllerFiles.size()}"
        controllerFiles.each{ println it }
    }

    private findRelevantFiles(){
        def aux = Util.findFilesFromDirectory(folder)
        files = aux.findAll{
            !it.contains("${File.separator}evaluation${File.separator}") &&
            it.contains("${File.separator}output_all${File.separator}") &&
            it.contains("${File.separator}selected${File.separator}") &&
            it.endsWith("-relevant.csv")
        }
        println "relevant files: ${files.size()}"
        files.each{ println it }
    }

    static void main(String[] args){
        def folder1 = "results${File.separator}sample4${File.separator}random1-calculatedRoutes"
        ResultOrganizer evaluator1 = new ResultOrganizer(folder1)
        evaluator1.summarize()

        def folder2 = "results${File.separator}sample4${File.separator}random1-railsRoutes"
        ResultOrganizer evaluator2 = new ResultOrganizer(folder2)
        evaluator2.summarize()

        def folder3 = "results${File.separator}sample4${File.separator}random2-calculatedRoutes"
        ResultOrganizer evaluator3 = new ResultOrganizer(folder3)
        evaluator3.summarize()

        def folder4 = "results${File.separator}sample4${File.separator}random2-railsRoutes"
        ResultOrganizer evaluator4 = new ResultOrganizer(folder4)
        evaluator4.summarize()
    }

}
