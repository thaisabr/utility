package study1.task_interfaces.routes

import br.ufpe.cin.tan.similarity.test.TestSimilarityAnalyser
import br.ufpe.cin.tan.util.CsvUtil
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

class RoutesEvaluator {

    String calculatedRoutesFile
    String railsRoutesFile
    String outputFile

    RoutesEvaluator(String folder1, String folder2){
        calculatedRoutesFile = "${folder1}${File.separator}itest_all.csv"
        railsRoutesFile = "${folder2}${File.separator}itest_all.csv"
        outputFile = "output${File.separator}routes_similarity.csv"
    }

    static void main(String[] args){
        def folder1 = "results${File.separator}sample4${File.separator}random2-calculatedRoutes"
        def folder2 = "results${File.separator}sample4${File.separator}random2-railsRoutes"
        def routesEvaluator = new RoutesEvaluator(folder1, folder2)
        routesEvaluator.generateAggregatedItestdResult()
    }

    def generateAggregatedItestdResult(){
        //"Project","Task","Precision","Recall","ITest_size","IReal_size","FP_size","FN_size","Hits_size","ITest","IReal","F2"
        List<String[]> content = []
        List<String[]> data = []
        String[] firstLine = ["Project", "Task", "Jaccard", "Cosine"]
        content +=  firstLine

        def calculatedRoutesResult = CsvUtil.read(calculatedRoutesFile)
        calculatedRoutesResult.remove(0)
        calculatedRoutesResult = calculatedRoutesResult.subList(0, calculatedRoutesResult.size()-15)
        def railsRoutesResult = CsvUtil.read(railsRoutesFile)
        railsRoutesResult.remove(0)
        railsRoutesResult = railsRoutesResult.subList(0, railsRoutesResult.size()-15)

        calculatedRoutesResult.eachWithIndex{ String[] entry, int i ->
            def calculatedInterface = entry[9].tokenize(',[]')*.trim()
            def railsInterface = railsRoutesResult[i][9].tokenize(',[]')*.trim()
            def similarityAnalyser = new TestSimilarityAnalyser(calculatedInterface, railsInterface)
            def jaccard = similarityAnalyser.calculateSimilarityByJaccard()
            def cosine = similarityAnalyser.calculateSimilarityByCosine()
            data += [entry[0], entry[1], jaccard, cosine] as String[]
        }

        def jaccard = generateStatistics(data.collect{ it[2] as double } as double[], "jaccard")
        def cosine = generateStatistics(data.collect{ it[3] as double } as double[], "cosine")
        content +=  data
        content +=  jaccard
        content +=  cosine
        CsvUtil.write(outputFile, content)
    }

    private static generateStatistics(double[] values, String text){
        List<String[]> content = []
        def mean = ["Mean "+text]
        def median = ["Median "+text]
        def sd = ["SD "+text]
        def statistics = new DescriptiveStatistics(values)
        mean.add(statistics.mean as String)
        median.add(statistics.getPercentile(50.0) as String)
        sd.add(statistics.standardDeviation as String)
        content += mean as String[]
        content += median as String[]
        content += sd as String[]
        content
    }

}
