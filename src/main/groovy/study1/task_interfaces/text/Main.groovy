package study1.task_interfaces.text

import br.ufpe.cin.tan.util.Util

class Main {

    static void main(String[] args){
        def folders = Util.findFoldersFromDirectory("results${File.separator}sample4")
        folders = folders.findAll{
            it.contains("random2-railsRoutes${File.separator}output_all${File.separator}") && it.contains("selected")
        }.sort()

        folders.each{ folder ->
            def analysedTasksFile = Util.findFilesFromDirectory(folder).find{ it.endsWith("-relevant.csv")}
            //println "Folder: $folder"
            println "analysedTasksFile: $analysedTasksFile"
            IText iText = new IText(analysedTasksFile)
            iText.generate()
        }

        def folder1 = "results${File.separator}sample4${File.separator}random1-calculatedRoutes"
        TextInterfaceOrganizer evaluator1 = new TextInterfaceOrganizer(folder1)
        evaluator1.summarize()

        def folder2 = "results${File.separator}sample4${File.separator}random1-railsRoutes"
        TextInterfaceOrganizer evaluator2 = new TextInterfaceOrganizer(folder2)
        evaluator2.summarize()

        def folder3 = "results${File.separator}sample4${File.separator}random2-calculatedRoutes"
        TextInterfaceOrganizer evaluator3 = new TextInterfaceOrganizer(folder3)
        evaluator3.summarize()

        def folder4 = "results${File.separator}sample4${File.separator}random2-railsRoutes"
        TextInterfaceOrganizer evaluator4 = new TextInterfaceOrganizer(folder4)
        evaluator4.summarize()
    }

}
