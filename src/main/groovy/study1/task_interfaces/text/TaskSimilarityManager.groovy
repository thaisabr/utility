package study1.task_interfaces.text

import br.ufpe.cin.tan.analysis.data.ExporterUtil
import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util
import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path

class TaskSimilarityManager {

    String mainFolder
    String tasksFile

    TaskSimilarityManager(mainFolder, tasksFile){
        this.mainFolder = mainFolder
        this.tasksFile = tasksFile
        configureTasksFile()
    }

    static void main(String[] args){
        //generateResultsForSample4()
        generateResultsForSample5()
    }

    static void generateResultsForSample4(){
        def mainFolder = "results${File.separator}sample4-alternativesForIText"
        def tasksFile = "tasks${File.separator}sample4${File.separator}tasks.csv"
        TaskSimilarityManager manager = new TaskSimilarityManager(mainFolder, tasksFile)
        manager.verifyHashSimilarityPerProject()
        manager.extractTasksWithUniqueHashes()
        manager.extractTasksWithUniqueLastHash()
    }

    static void generateResultsForSample5(){
        def mainFolder = "results${File.separator}sample5-alternativesForIText"
        def tasksFile = "tasks${File.separator}sample5${File.separator}tasks.csv"
        //FixMultipleResultsPerProject.organizeFiles(mainFolder)
        TaskSimilarityManager manager = new TaskSimilarityManager(mainFolder, tasksFile)
        manager.verifyHashSimilarityPerProject()
        manager.extractTasksWithUniqueHashes()
        manager.extractTasksWithUniqueLastHash()
    }

    def verifyHashSimilarityPerProject(){
        def outputFolder = new File("${mainFolder}${File.separator}hash_similarity")
        if(!outputFolder.exists()) outputFolder.mkdir()
        def files = Util.findFilesFromDirectory(mainFolder)
        def resultFiles = files.findAll{
            it.contains("${File.separator}selected${File.separator}") && it.endsWith("-relevant.csv")
        }.sort()

        def buffer = []
        resultFiles.each{ analysedTasksFile ->
            List<String[]> entries = CsvUtil.read(analysedTasksFile)
            List<String[]> content = []
            String[] resultHeader = ["task_a", "hashes_a", "task_b", "hashes_b" , "intersection", "%_a", "%_b"]
            def allTasks = entries.subList(ExporterUtil.INITIAL_TEXT_SIZE_SHORT_HEADER, entries.size())
            def ids = allTasks.collect{ it[0] }
            def taskPairs = ExporterUtil.computeTaskPairs(allTasks)

            //compute similarity
            List<String[]> data = []
            taskPairs?.each { item ->
                def task = item.task
                def hashes1 = task[4]?.substring(1, task[4].size()-1)?.split(", ") as List
                item.pairs?.each { other ->
                    def hashes2 = other[4]?.substring(1, other[4].size()-1)?.split(", ") as List
                    def intersection = hashes1.intersect(hashes2).size()

                    data += [task[0], hashes1.size(), other[0], hashes2.size(), intersection,
                             intersection/hashes1.size(), intersection/hashes2.size()] as String[]
                }
            }

            //save similarity data
            content += resultHeader
            content += data
            def similarityFile = analysedTasksFile - "-relevant.csv" + "-hashes.csv"
            CsvUtil.write(similarityFile, content)
            Path source = FileSystems.getDefault().getPath(similarityFile)
            Path target = FileSystems.getDefault().getPath(outputFolder.path)
            Files.move(source, target.resolve(source.getFileName()))

            //export tasks with exclusive hashes set
            //["task_a", "hashes_a", "task_b", "hashes_b" , "intersection", "%_a", "%_b"]
            List<String[]> noSimResult = []
            def pairsNoSimilarity = data.findAll{ it[4]=="0" }
            ids.each{ id ->
                def n = pairsNoSimilarity.findAll{ it[0]==id || it[2]==id }.size()
                if(n == allTasks.size()-1) {
                    noSimResult += allTasks.find{ it[0] == id }
                }
            }
            exportTasksWithUniqueHashes(entries, analysedTasksFile, noSimResult.sort{ it[0] as double})

            //export strong similar tasks
            //["task_a", "hashes_a", "task_b", "hashes_b" , "intersection", "%_a", "%_b"]
            List<String[]> maxSimResult = []
            def pairsMaxSimilarity = data.findAll{ (it[5]=="1") || (it[6]=="1") }
            ids.each{ id ->
                def n = pairsMaxSimilarity.findAll{ it[0]==id || it[2]==id }
                if(n.size()>0) {
                    List<String[]> temp = []
                    n.each{ pair ->
                        if(pair[0]==id && (pair[1] as int)<(pair[3] as int) ){
                            temp += allTasks.find{ it[0] == id }
                        } else if(pair[2]==id && (pair[3] as int)<(pair[1] as int)){
                            temp += allTasks.find{ it[0] == id }
                        }

                    }
                    maxSimResult += temp.unique()
                }
            }
            maxSimResult = maxSimResult.unique().sort{ it[0] as double}
            exportTasksWithMaxSimilarHashes(entries, analysedTasksFile, maxSimResult)
            def selfContainedTasks = allTasks - maxSimResult
            def url = entries[0][1]
            buffer += selfContainedTasks.collect{ [url, it[0]] }
            exportSelfContainedTasks(entries, analysedTasksFile, selfContainedTasks)
        }
        def index = tasksFile.lastIndexOf(File.separator)
        def newname = tasksFile.substring(0, index+1)+"tasks-selfContained.csv"
        generateTaskCsv(newname, buffer)
        collapseSelfContainedFiles(mainFolder)
    }

    def extractTasksWithUniqueHashes(){
        def files = Util.findFilesFromDirectory(mainFolder)
        def resultFiles = files.findAll{
            it.endsWith("-unique.csv")
        }.sort()

        def buffer = []
        resultFiles.each{
            def tasks = CsvUtil.read(it)
            def url = tasks[0][1]
            def allTasks = tasks.subList(ExporterUtil.INITIAL_TEXT_SIZE_SHORT_HEADER, tasks.size())
            buffer += allTasks.collect{ [url, it[0]] }
        }

        def index = tasksFile.lastIndexOf(File.separator)
        def newname = tasksFile.substring(0, index+1)+"tasks-uniqueHashes.csv"
        generateTaskCsv(newname, buffer)

        collapseUniqueHashesFiles(mainFolder)
    }

    def extractTasksWithUniqueLastHash(){
        def uniqueLastHashTaskFile = exportTasksWithUniqueLastHash(tasksFile)
        def uniqueLastHash = CsvUtil.read(uniqueLastHashTaskFile)
        uniqueLastHash.remove(0)
        def identityUniqueLastHash = uniqueLastHash.collect{ [it[0], it[1]] }

        def files = Util.findFilesFromDirectory(mainFolder)
        def resultFiles = files.findAll{
            it.contains("${File.separator}selected${File.separator}") && it.endsWith("-relevant.csv")
        }.sort()

        resultFiles.each{ file ->
            List<String[]> selectedTasks = []
            List<String[]> entries = CsvUtil.read(file)
            List<String[]> finalContent = []
            def url = entries[0][1]
            def possible = identityUniqueLastHash.findAll{ it[0] == url }
            def allTasks = entries.subList(ExporterUtil.INITIAL_TEXT_SIZE_SHORT_HEADER, entries.size())
            allTasks.each{ task ->
                def s = possible.findAll{ it[1]==task[0] }
                if(!s.empty) selectedTasks += task
            }
            finalContent += entries.subList(0, ExporterUtil.INITIAL_TEXT_SIZE_SHORT_HEADER)
            finalContent += selectedTasks
            def outputFile = file - "-relevant.csv" + "-unique-last-hash.csv"
            CsvUtil.write(outputFile, finalContent)
            Path source = FileSystems.getDefault().getPath(outputFile)
            Path target = FileSystems.getDefault().getPath(mainFolder)
            Files.copy(source, target.resolve(source.getFileName()))
        }

        collapseUniqueLastHashFiles(mainFolder)
    }

    private configureTasksFile(){
        File f = new File(tasksFile)
        if(!f.exists()){
            def index = tasksFile.lastIndexOf(File.separator)
            def folder = tasksFile.substring(0, index)
            organizeTaskFile(folder)
        }
    }

    private static collapseSelfContainedFiles(entry){
        def filesFromSelectedFolder = Util.findFilesFromDirectory(entry)
        def selectedFiles = filesFromSelectedFolder.findAll{ it.endsWith("-selfContained.csv") &&
                !it.contains("${File.separator}output_all${File.separator}")
        }
        def finalSelectedFiles = "${entry}${File.separator}selected-selfContained.csv"
        List<String[]> content = []
        List<String[]> buffer = []
        selectedFiles.each{ file ->
            def lines = CsvUtil.read(file)
            if(content.empty){
                def temp = lines.subList(0, 12)
                temp.each{ it[1] = ""}
                content += temp
                content += lines.get(12)
            }
            if(!lines.empty || lines.size()>13) buffer += lines.subList(13, lines.size())
        }
        buffer = buffer.sort{ it[0] }
        content += buffer
        CsvUtil.write(finalSelectedFiles, content)

        //remove multiple files
        selectedFiles.each { file ->
            File f = new File(file)
            f.delete()
        }
    }

    private exportSelfContainedTasks(List<String[]> entries, String analysedTasksFile,
                                               List<String[]> selfContainedTasks){
        def header = entries.subList(0,ExporterUtil.INITIAL_TEXT_SIZE_SHORT_HEADER)
        def outputFile = analysedTasksFile - "-relevant.csv" + "-selfContained.csv"
        CsvUtil.write(outputFile, header+selfContainedTasks)
        Path source = FileSystems.getDefault().getPath(outputFile)
        Path target = FileSystems.getDefault().getPath(mainFolder)
        Files.copy(source, target.resolve(source.getFileName()))
    }

    private exportTasksWithUniqueHashes(List<String[]> entries, String analysedTasksFile,
                                               List<String[]> noSimTasks){
        def header = entries.subList(0,ExporterUtil.INITIAL_TEXT_SIZE_SHORT_HEADER)
        def outputFile = analysedTasksFile - "-relevant.csv" + "-unique.csv"
        CsvUtil.write(outputFile, header+noSimTasks)
        Path source = FileSystems.getDefault().getPath(outputFile)
        Path target = FileSystems.getDefault().getPath(mainFolder)
        Files.copy(source, target.resolve(source.getFileName()))
    }

    private exportTasksWithMaxSimilarHashes(entries, analysedTasksFile, maxSimResult){
        def outputFolder = new File("${mainFolder}${File.separator}hash_equal")
        if(!outputFolder.exists()) outputFolder.mkdir()
        def header = entries.subList(0,ExporterUtil.INITIAL_TEXT_SIZE_SHORT_HEADER)
        def outputFile = analysedTasksFile - "-relevant.csv" + "-same.csv"
        CsvUtil.write(outputFile, header+maxSimResult)
        Path source = FileSystems.getDefault().getPath(outputFile)
        Path target = FileSystems.getDefault().getPath(outputFolder.path)
        Files.move(source, target.resolve(source.getFileName()))
    }

    //tasksOfInterest = [url, it[0]]
    private generateTaskCsv(newTaskFile, tasksOfInterest){
        List<String[]> result = []
        def tasksForAnalysis = CsvUtil.read(tasksFile)
        def header = tasksForAnalysis.get(0)
        tasksForAnalysis.remove(0)
        tasksForAnalysis.each{ task ->
            def s = tasksOfInterest.findAll{ it[0] == task[0] && it[1] == task[1]}
            if(!s.empty) result += task
        }
        List<String> content = []
        content += header
        content += result
        CsvUtil.write(newTaskFile, content)
    }

    private static collapseUniqueHashesFiles(entry){
        def filesFromSelectedFolder = Util.findFilesFromDirectory(entry)
        def selectedFiles = filesFromSelectedFolder.findAll{ it.endsWith("-unique.csv") &&
            !it.contains("${File.separator}output_all${File.separator}")
        }
        def finalSelectedFiles = "${entry}${File.separator}selected-unique.csv"
        List<String[]> content = []
        List<String[]> buffer = []
        selectedFiles.each{ file ->
            def lines = CsvUtil.read(file)
            if(content.empty){
                def temp = lines.subList(0, 12)
                temp.each{ it[1] = ""}
                content += temp
                content += lines.get(12)
            }
            if(!lines.empty || lines.size()>13) buffer += lines.subList(13, lines.size())
        }
        buffer = buffer.sort{ it[0] }
        content += buffer
        CsvUtil.write(finalSelectedFiles, content)

        //remove multiple files
        selectedFiles.each { file ->
            File f = new File(file)
            f.delete()
        }
    }

    private static collapseUniqueLastHashFiles(entry){
        def filesFromSelectedFolder = Util.findFilesFromDirectory(entry)
        def selectedFiles = filesFromSelectedFolder.findAll{ it.endsWith("-unique-last-hash.csv") &&
                !it.contains("${File.separator}output_all${File.separator}")
        }
        def finalSelectedFiles = "${entry}${File.separator}selected-unique-last-hash.csv"
        List<String[]> content = []
        List<String[]> buffer = []
        selectedFiles.each{ file ->
            def lines = CsvUtil.read(file)
            if(content.empty){
                def temp = lines.subList(0, 12)
                temp.each{ it[1] = ""}
                content += temp
                content += lines.get(12)
            }
            if(!lines.empty || lines.size()>13) buffer += lines.subList(13, lines.size())
        }
        buffer = buffer.sort{ it[0] }
        content += buffer
        CsvUtil.write(finalSelectedFiles, content)

        //remove multiple files
        selectedFiles.each { file ->
            File f = new File(file)
            f.delete()
        }
    }

    private static exportTasksWithUniqueLastHash(tasksFile){
        def tasks = CsvUtil.read(tasksFile)
        def header = tasks.get(0)
        tasks.remove(0)
        def uniqueLastHash = tasks.unique{ it[6] }

        List<String> content = []
        content += header
        content += uniqueLastHash
        def index = tasksFile.lastIndexOf(File.separator)
        def filename = tasksFile.substring(0, index+1) + "tasks-uniqueLastHash.csv"
        CsvUtil.write(filename, content)
        filename
    }

    private static organizeTaskFile(String folder){
        def outputFile = "${folder}${File.separator}tasks.csv"
        List<String[]> content = []
        content += ["REPO_URL", "TASK_ID", "#HASHES", "HASHES", "#PROD_FILES", "#TEST_FILES", "LAST"] as String[]
        def files = Util.findFilesFromDirectory(folder).sort()
        files.each{ file ->
            def lines = CsvUtil.read(file)
            lines.remove(0)
            content += lines
        }
        CsvUtil.write(outputFile, content)
        outputFile
    }

}
