package study1.task_interfaces.text

import br.ufpe.cin.tan.evaluation.TaskInterfaceEvaluator
import br.ufpe.cin.tan.similarity.test.TestSimilarityAnalyser
import br.ufpe.cin.tan.util.Util

class TextResult {

    def id
    String url
    Collection files
    Collection ireal
    double precision
    double recall
    double f2
    int falsePositives
    int falseNegatives
    int hits

    TextResult(def id, String url, List files, List ireal){
        this.ireal = ireal
        this.id = id
        this.url = url
        setFiles(files)
    }

    void setFiles(def files){
        this.files = files
        setPrecision()
        setRecall()
        f2Measure()
        setHitMetrics()
    }

    void setPrecision(){
        /*if (Util.SIMILARITY_ANALYSIS) {
            def similarityAnalyser = new TestSimilarityAnalyser(files, ireal)
            precision = similarityAnalyser.calculateSimilarityByJaccard()
        } else {*/
            precision = TaskInterfaceEvaluator.calculateFilesPrecision(files as Set, ireal as Set)
        //}
    }

    void setRecall(){
        /*if (Util.SIMILARITY_ANALYSIS) {
            def similarityAnalyser = new TestSimilarityAnalyser(files, ireal)
            recall = similarityAnalyser.calculateSimilarityByCosine()
        } else {*/
            recall = TaskInterfaceEvaluator.calculateFilesRecall(files as Set, ireal as Set)
        //}
    }

    double f2Measure(){
        def denominator = 4*precision+recall
        if(denominator==0) f2 = 0
        else f2 = 5 * ((precision*recall)/denominator)
    }

    void setHitMetrics(){
        falsePositives = (files - ireal).size()
        falseNegatives = (ireal - files).size()
        hits = files.intersect(ireal).size()
    }

    def parseResultToArray() {
        [url, id, precision, recall, files.size(), ireal.size(), falsePositives, falseNegatives, hits, files, ireal,
         f2] as String[]
    }

}
