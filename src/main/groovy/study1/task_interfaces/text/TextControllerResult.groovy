package study1.task_interfaces.text

import br.ufpe.cin.tan.evaluation.TaskInterfaceEvaluator
import br.ufpe.cin.tan.similarity.test.TestSimilarityAnalyser
import br.ufpe.cin.tan.util.Util

class TextControllerResult {

    def id
    String url
    Collection controllers
    Collection irealControllers
    double precision
    double recall
    double f2
    int falsePositives
    int falseNegatives
    int hits

    TextControllerResult(TextResult result){
        id = result.id
        this.url = result.url
        irealControllers = result.ireal.findAll{ Util.isControllerFile(it) }
        controllers = result.files.findAll{ Util.isControllerFile(it) }
        setPrecision()
        setRecall()
        f2Measure()
        setHitMetrics()
    }

    void setPrecision(){
        if (Util.SIMILARITY_ANALYSIS) {
            def similarityAnalyser = new TestSimilarityAnalyser(controllers, irealControllers)
            precision = similarityAnalyser.calculateSimilarityByJaccard()
        } else {
            precision = TaskInterfaceEvaluator.calculateFilesPrecision(controllers as Set, irealControllers as Set)
        }
    }

    void setRecall(){
        if (Util.SIMILARITY_ANALYSIS) {
            def similarityAnalyser = new TestSimilarityAnalyser(controllers, irealControllers)
            recall = similarityAnalyser.calculateSimilarityByCosine()
        } else {
            recall = TaskInterfaceEvaluator.calculateFilesRecall(controllers as Set, irealControllers as Set)
        }
    }

    double f2Measure(){
        def denominator = 4*precision+recall
        if(denominator==0) f2 = 0
        else f2 = 5 * ((precision*recall)/denominator)
    }

    void setHitMetrics(){
        falsePositives = (controllers - irealControllers).size()
        falseNegatives = (irealControllers - controllers).size()
        hits = controllers.intersect(irealControllers).size()
    }

    def parseResultToArray() {
        [url, id, precision, recall, controllers.size(), irealControllers.size(), falsePositives, falseNegatives, hits,
         controllers, irealControllers, f2] as String[]
    }

}
