package study1.task_interfaces.text

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util
import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path

class FixMultipleResultsPerProject {

    static void main(String[] args){
        //generateITextForSample4()
        //generateITextForSample5()
        generateSelfContentSample5NoViews()
    }

    private static generateSelfContentSample5NoViews(){
        def path = "results${File.separator}sample5-itext-selfContained"
        def folders = Util.findFoldersFromDirectory(path)
        folders = folders.findAll{
            it.contains("${File.separator}output_all${File.separator}") && it.contains("selected")
        }.sort()

        folders.each{ folder ->
            def analysedTasksFile = Util.findFilesFromDirectory(folder).find{ it.endsWith("-selfContained.csv")}
            println "analysedTasksFile: $analysedTasksFile"
            IText iText = new IText(analysedTasksFile)
            iText.generate()
        }
        SelfContainedTextInterfaceOrganizer organizer = new SelfContainedTextInterfaceOrganizer(path)
        organizer.summarize()
    }

    private static extractProjectsWithMultipleResults(String entry){
        def folders = Util.findFoldersFromDirectory(entry)
        def multiplyFolders = folders.findAll{ it =~ /.*_\d+$/ }.sort()
        def projects = multiplyFolders.collect{
            def indexInit = it.lastIndexOf(File.separator)
            def indexEnd = it.lastIndexOf("_")
            if(indexEnd<0 || indexInit<0 || indexEnd<indexInit) ""
            else it.substring(indexInit+1, indexEnd)
        }.sort().unique()
        [folders:multiplyFolders, projects:projects]
    }

    private static moveTextFiles(String projectFolder){
        def index = projectFolder.lastIndexOf("_")
        def outputFolder = projectFolder.substring(0, index) + "${File.separator}text"
        def textFiles = Util.findFilesFromDirectory("${projectFolder}${File.separator}text")
        textFiles.each{ file ->
            Path source = FileSystems.getDefault().getPath(file)
            Path target = FileSystems.getDefault().getPath(outputFolder)
            Files.move(source, target.resolve(source.getFileName()))
        }
    }

    private static moveSelectedFiles(String projectFolder){
        def index = projectFolder.lastIndexOf("_")
        def outputFolder = projectFolder.substring(0, index) + "${File.separator}selected"
        def selectedFiles = Util.findFilesFromDirectory("${projectFolder}${File.separator}selected")
        selectedFiles.each{ file ->
            Path source = FileSystems.getDefault().getPath(file)
            Path target = FileSystems.getDefault().getPath(outputFolder)
            Files.move(source, target.resolve(source.getFileName()))
        }
    }

    private static moveFiles(result, entry){
        def projects = result.projects
        def multiplyFolders = result.folders

        projects.each{ project ->
            File projectFolder = new File("${entry}${File.separator}${project}")
            if(!projectFolder.exists()) {
                projectFolder.mkdir()
                File selectedFolder = new File(projectFolder.path+"${File.separator}selected")
                selectedFolder.mkdir()
                File textFolder = new File(projectFolder.path+"${File.separator}text")
                textFolder.mkdir()
            }
            def results = multiplyFolders.findAll{ it.contains(project) }
            results.each{
                moveTextFiles(it)
                moveSelectedFiles(it)
            }
        }
    }

    private static excludeOldFolders(List folders){
        folders.each{
            Util.deleteFolder(it)
        }
    }

    private static collapseRelevantFiles(entry){
        collapseFiles(entry, "-relevant.csv", 13)
    }

    private static collapseDetailedFiles(entry){
        collapseFiles(entry, "-relevant-detailed.csv", 2)
    }

    private static collapseFiles(entry, sufix, headerSize){
        def projects = ["diaspora", "localsupport", "odin", "oneclickorgs", "otwarchive",
                        "rapidftr", "tip4commit", "tracks", "websiteone", "whitehall" ]
        projects.each{ project ->
            def folder = "${entry}${File.separator}${project}${File.separator}selected"
            def filesFromSelectedFolder = Util.findFilesFromDirectory(folder)
            def selectedFiles = filesFromSelectedFolder.findAll{ it.endsWith(sufix) }
            if(selectedFiles.size()>1){ //"multiply entries project
                def finalSelectedFiles = "${folder}${File.separator}${project}${sufix}"
                List<String[]> content = []
                List<String[]> buffer = []
                selectedFiles.each{ file ->
                    def lines = CsvUtil.read(file)
                    if(content.empty){
                        def temp = lines.subList(0, headerSize-1)
                        temp.subList(1,temp.size()).each{ it[1] = ""}
                        content += temp
                        content += lines.get(headerSize-1)
                    }
                    if(!lines.empty || lines.size()>headerSize) buffer += lines.subList(headerSize, lines.size())
                }
                buffer = buffer.sort{ it[0] }
                content += buffer
                CsvUtil.write(finalSelectedFiles, content)

                //remove multiple files
                selectedFiles.each { file ->
                    File f = new File(file)
                    f.delete()
                }
            }
        }
    }

    static organizeFiles(path){
        def entries = ["${path}${File.separator}output_added", "${path}${File.separator}output_added_when",
                       "${path}${File.separator}output_all", "${path}${File.separator}output_all_when"]
        entries.each{ entry ->
            def result = extractProjectsWithMultipleResults(entry)
            moveFiles(result, entry)
            excludeOldFolders(result.folders)
            collapseRelevantFiles(entry)
            collapseDetailedFiles(entry)
        }
    }

    private static generateTemporalSample5(){
        def path = "results${File.separator}sample5-itext-temporal"
        def folders = Util.findFoldersFromDirectory(path)
        folders = folders.findAll{
            it.contains("${File.separator}output_all${File.separator}") && it.contains("selected")
        }.sort()

        //IText for all tasks; respecting tasks temporal order - tarefas passadas (pelo menos 1 dia)
        folders.each{ folder ->
            def analysedTasksFile = Util.findFilesFromDirectory(folder).find{ it.endsWith("-relevant.csv")}
            println "analysedTasksFile: $analysedTasksFile"
            IText iText = new IText(analysedTasksFile)
            iText.generate()
        }
        TextInterfaceOrganizer organizer = new TextInterfaceOrganizer(path)
        organizer.summarize()
    }

    private static generateSelfContentSample5(){
        def path = "results${File.separator}sample5-itext-selfContained"
        def folders = Util.findFoldersFromDirectory(path)
        folders = folders.findAll{
            it.contains("${File.separator}output_all${File.separator}") && it.contains("selected")
        }.sort()

        folders.each{ folder ->
            def analysedTasksFile = Util.findFilesFromDirectory(folder).find{ it.endsWith("-selfContained.csv")}
            println "analysedTasksFile: $analysedTasksFile"
            IText iText = new IText(analysedTasksFile)
            iText.generate()
        }
        SelfContainedTextInterfaceOrganizer organizer = new SelfContainedTextInterfaceOrganizer(path)
        organizer.summarize()
    }

    private static generateUniqueHahesSample5(){
        def path = "results${File.separator}sample5-itext-uniqueHashes"
        def folders = Util.findFoldersFromDirectory(path)
        folders = folders.findAll{
            it.contains("${File.separator}output_all${File.separator}") && it.contains("selected")
        }.sort()

        folders.each{ folder ->
            def analysedTasksFile = Util.findFilesFromDirectory(folder).find{ it.endsWith("-unique.csv")}
            println "analysedTasksFile: $analysedTasksFile"
            IText iText = new IText(analysedTasksFile)
            iText.generate()
        }
        UniqueHashesTextInterfaceOrganizer organizer = new UniqueHashesTextInterfaceOrganizer(path)
        organizer.summarize()
    }

    private static generateUniqueLastHashSample5(){
        def path = "results${File.separator}sample5-itext-uniqueLastHash"
        def folders = Util.findFoldersFromDirectory(path)
        folders = folders.findAll{
            it.contains("${File.separator}output_all${File.separator}") && it.contains("selected")
        }.sort()

        folders.each{ folder ->
            def analysedTasksFile = Util.findFilesFromDirectory(folder).find{ it.endsWith("-unique-last-hash.csv")}
            println "analysedTasksFile: $analysedTasksFile"
            IText iText = new IText(analysedTasksFile)
            iText.generate()
        }
        UniqueLastHashTextInterfaceOrganizer organizer = new UniqueLastHashTextInterfaceOrganizer(path)
        organizer.summarize()
    }

    static generateITextForSample5(){
        generateTemporalSample5()
        generateSelfContentSample5()
        generateUniqueHahesSample5()
        generateUniqueLastHashSample5()
    }

    private static generateTemporalSample4(){
        def path = "results${File.separator}sample4-itext-temporal"
        def folders = Util.findFoldersFromDirectory(path)
        folders = folders.findAll{
            it.contains("${File.separator}output_all${File.separator}") && it.contains("selected")
        }.sort()

        //IText for all tasks; respecting tasks temporal order - tarefas passadas (pelo menos 1 dia)
        folders.each{ folder ->
            def analysedTasksFile = Util.findFilesFromDirectory(folder).find{ it.endsWith("-relevant.csv")}
            println "analysedTasksFile: $analysedTasksFile"
            IText iText = new IText(analysedTasksFile)
            iText.generate()
        }
        TextInterfaceOrganizer organizer = new TextInterfaceOrganizer(path)
        organizer.summarize()
    }

    private static generateSelfContentSample4(){
        def path = "results${File.separator}sample4-itext-selfContained"
        def folders = Util.findFoldersFromDirectory(path)
        folders = folders.findAll{
            it.contains("${File.separator}output_all${File.separator}") && it.contains("selected")
        }.sort()

        folders.each{ folder ->
            def analysedTasksFile = Util.findFilesFromDirectory(folder).find{ it.endsWith("-selfContained.csv")}
            println "analysedTasksFile: $analysedTasksFile"
            IText iText = new IText(analysedTasksFile)
            iText.generate()
        }
        SelfContainedTextInterfaceOrganizer organizer = new SelfContainedTextInterfaceOrganizer(path)
        organizer.summarize()
    }

    private static generateUniqueHahesSample4(){
        def path = "results${File.separator}sample4-itext-uniqueHashes"
        def folders = Util.findFoldersFromDirectory(path)
        folders = folders.findAll{
            it.contains("${File.separator}output_all${File.separator}") && it.contains("selected")
        }.sort()

        folders.each{ folder ->
            def analysedTasksFile = Util.findFilesFromDirectory(folder).find{ it.endsWith("-unique.csv")}
            println "analysedTasksFile: $analysedTasksFile"
            IText iText = new IText(analysedTasksFile)
            iText.generate()
        }
        UniqueHashesTextInterfaceOrganizer organizer = new UniqueHashesTextInterfaceOrganizer(path)
        organizer.summarize()
    }

    private static generateUniqueLastHashSample4(){
        def path = "results${File.separator}sample4-itext-uniqueLastHash"
        def folders = Util.findFoldersFromDirectory(path)
        folders = folders.findAll{
            it.contains("${File.separator}output_all${File.separator}") && it.contains("selected")
        }.sort()

        folders.each{ folder ->
            def analysedTasksFile = Util.findFilesFromDirectory(folder).find{ it.endsWith("-unique-last-hash.csv")}
            println "analysedTasksFile: $analysedTasksFile"
            IText iText = new IText(analysedTasksFile)
            iText.generate()
        }
        UniqueLastHashTextInterfaceOrganizer organizer = new UniqueLastHashTextInterfaceOrganizer(path)
        organizer.summarize()
    }

    static generateITextForSample4(){
        generateTemporalSample4()
        generateSelfContentSample4()
        generateUniqueHahesSample4()
        generateUniqueLastHashSample4()
    }

    /*
    * LEMBRETE
    * O procedimento é primeiro organizar os arquivos para gerar IText, caso queira variar para gerar somente para tarefas
    * que não possui hash em comum com nenhuma outra, essas coisas.
    * Depois vem aqui e manda gerar a Itext, confirando o tipo do arquivo de entrada para a geração da interface.
    * */

}
