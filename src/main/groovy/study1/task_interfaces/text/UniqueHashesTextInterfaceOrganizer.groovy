package study1.task_interfaces.text

import br.ufpe.cin.tan.util.Util

class UniqueHashesTextInterfaceOrganizer extends TextInterfaceOrganizer {

    UniqueHashesTextInterfaceOrganizer(String folder){
        super(folder)
    }

    @Override
    def extractProjectName(String name){
        def i = name.indexOf("-unique-text")
        def j = name.lastIndexOf(File.separator)
        name.substring(j+1, i)
    }

    @Override
    def findItextControllerFiles(){
        def aux = Util.findFilesFromDirectory(folder)
        controllerFiles = aux.findAll{
            it.endsWith("-unique-text-controller.csv") && !it.contains("${File.separator}evaluation${File.separator}")
        }
        println "itext controllerFiles: ${controllerFiles.size()}"
        controllerFiles.each{ println it }
    }

    @Override
    def findItextFiles(){
        def aux = Util.findFilesFromDirectory(folder)
        files = aux.findAll{
            it.endsWith("unique-text.csv") && !it.contains("${File.separator}evaluation${File.separator}")
        }
        println "itext files: ${files.size()}"
        files.each{ println it }
    }

    @Override
    def findItextNoEmptyControllerFiles(){
        def aux = Util.findFilesFromDirectory(folder)
        noemptyControllerFiles = aux.findAll{
            it.endsWith("-unique-text-controller-noempty.csv") && !it.contains("${File.separator}evaluation${File.separator}")
        }
        println "itext no empty controllerFiles: ${noemptyControllerFiles.size()}"
        noemptyControllerFiles.each{ println it }
    }

    @Override
    def findItextNoEmptyFiles(){
        def aux = Util.findFilesFromDirectory(folder)
        noemptyFiles = aux.findAll{
            it.endsWith("-unique-text-noempty.csv") && !it.contains("${File.separator}evaluation${File.separator}")
        }
        println "itext no empty files: ${noemptyFiles.size()}"
        noemptyFiles.each{ println it }
    }

}
