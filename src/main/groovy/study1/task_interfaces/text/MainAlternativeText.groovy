package study1.task_interfaces.text

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

class MainAlternativeText {

    static void main(String[] args){
        def file = "tasks${File.separator}tasks.csv"
        //def folder =  "tasks${File.separator}sample5"
        //def file = organizeTaskFile(folder)
        //IText iText = new IText(file, "output")
        //iText.generateAndSaveAlternativeIrandom()

        /*def folders = Util.findFoldersFromDirectory("results${File.separator}sample5-reorganized")
        folders = folders.findAll{
            it.contains("random2-railsRoutes${File.separator}output_all${File.separator}") && it.contains("selected")
        }.sort()

        folders.each{ folder ->
            def analysedTasksFile = Util.findFilesFromDirectory(folder).find{ it.endsWith("-relevant.csv")}
            //println "Folder: $folder"
            println "analysedTasksFile: $analysedTasksFile"
            IText iText = new IText(analysedTasksFile)
            iText.generate()
        }*/

        def folder = "results${File.separator}sample4-analysis"
        organizeResult(folder)

        /*folder = "results${File.separator}sample4-itext-selfcontained"
        organizeSelfContainedResult(folder)

        folder = "results${File.separator}sample5-itext-selfcontained"
        organizeSelfContainedResult(folder)*/
    }

    static organizeResult(String folder){
        println "ITEXT TEMPORAL RESULT"
        TextInterfaceOrganizer organizer = new TextInterfaceOrganizer(folder)
        organizer.summarize()
    }

    static renameUniqueHashesFiles(String folder){
        def allFiles = Util.findFilesFromDirectory(folder)
        def files = allFiles.findAll{ it.endsWith("-unique.csv-text.csv") }
        files.each{ file ->
            def f = new File(file)
            def newname = file - "-unique.csv-text.csv" + "-unique-text.csv"
            f.renameTo(newname)
        }
        def controllerFiles = allFiles.findAll{ it.endsWith("-unique-text.csv-controller.csv") }
        controllerFiles.each{ file ->
            def f = new File(file)
            def newname = file - "-unique-text.csv-controller.csv" + "-unique-text-controller.csv"
            f.renameTo(newname)
        }
    }

    static renameLastHashFiles(String folder){
        def allFiles = Util.findFilesFromDirectory(folder)
        def files = allFiles.findAll{ it.endsWith("-last-hash.csv-text.csv") }
        files.each{ file ->
            def f = new File(file)
            def newname = file - "-last-hash.csv-text.csv" + "-last-hash-text.csv"
            f.renameTo(newname)
        }
        def controllerFiles = allFiles.findAll{ it.endsWith("-last-hash-text.csv-controller.csv") }
        controllerFiles.each{ file ->
            def f = new File(file)
            def newname = file - "-last-hash-text.csv-controller.csv" + "-last-hash-text-controller.csv"
            f.renameTo(newname)
        }
    }

    static organizeLastHashResult(String folder){
        //renameLastHashFiles(folder)
        println "ITEXT UNIQUE LAST HASH RESULT"
        UniqueLastHashTextInterfaceOrganizer organizer = new UniqueLastHashTextInterfaceOrganizer(folder)
        organizer.summarize()
    }

    static organizeUniqueHashesResult(String folder){
        //renameUniqueHashesFiles(folder)
        println "ITEXT UNIQUE HASHES RESULT"
        UniqueHashesTextInterfaceOrganizer organizer = new UniqueHashesTextInterfaceOrganizer(folder)
        organizer.summarize()
    }

    static organizeSelfContainedResult(String folder){
        //renameUniqueHashesFiles(folder)
        println "ITEXT SELF-CONTAINED RESULT"
        SelfContainedTextInterfaceOrganizer organizer = new SelfContainedTextInterfaceOrganizer(folder)
        organizer.summarize()
    }

    static organizeTaskFile(String folder){
        def outputFile = "${folder}${File.separator}tasks.csv"
        List<String[]> content = []
        content += ["REPO_URL", "TASK_ID", "#HASHES", "HASHES", "#PROD_FILES", "#TEST_FILES", "LAST"] as String[]
        def files = Util.findFilesFromDirectory(folder).sort()
        files.each{ file ->
            println file
            def lines = CsvUtil.read(file)
            lines.remove(0)
            content += lines
        }
        CsvUtil.write(outputFile, content)
        outputFile
    }

}
