package study1.task_interfaces.text

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

class TextInterfaceOrganizer {

    String folder
    List<String> controllerFiles
    List<String> files
    List<String> noemptyControllerFiles
    List<String> noemptyFiles
    String outputControllerFile
    String outputFile
    String outputNoEmptyControllerFile
    String outputNoEmptyFile

    TextInterfaceOrganizer(String folder){
        this.folder = folder
        outputControllerFile = "$folder${File.separator}itext-controller.csv"
        outputFile = "$folder${File.separator}itext.csv"
        outputNoEmptyControllerFile = "$folder${File.separator}itext-controller-noempty.csv"
        outputNoEmptyFile = "$folder${File.separator}itext-noempty.csv"
        findItextControllerFiles()
        findItextFiles()
        findItextNoEmptyControllerFiles()
        findItextNoEmptyFiles()
    }

    def summarize(){
        summarizeResults()
        summarizeControllerResults()
        summarizeNoEmptyResults()
        summarizeNoEmptyControllerResults()
    }

    def extractProjectName(String name){
        def i = name.indexOf("-text")
        def j = name.lastIndexOf(File.separator)
        name.substring(j+1, i)
    }

    private summarizeControllerResults(){
        def content = summarizeData(controllerFiles)
        CsvUtil.write(outputControllerFile, content)
    }

    private summarizeResults(){
        def content = summarizeData(files)
        CsvUtil.write(outputFile, content)
    }

    private summarizeNoEmptyControllerResults(){
        def content = summarizeData(noemptyControllerFiles)
        CsvUtil.write(outputNoEmptyControllerFile, content)
    }

    private summarizeNoEmptyResults(){
        def content = summarizeData(noemptyFiles)
        CsvUtil.write(outputNoEmptyFile, content)
    }

    private summarizeData(resultFiles){
        //"TASK","Precision","Recall","#ITextResult","#IReal","#FP","#FN","#Hits","ITextResult","IReal","F2"

        String[] firstLine = ["Project", "Task", "Precision", "Recall", "IText_size", "IReal_size", "FP_size", "FN_size",
                              "Hits_size", "IText", "IReal", "F2"] as String[]
        List<String[]> content = []
        List<String[]> data = []
        content +=  firstLine
        resultFiles.each{ file ->
            String[] values = []
            values += extractProjectName(file)
            def lines = CsvUtil.read(file)
            lines = lines.subList(11, lines.size())
            def temp = lines.collect{ (values + it) as String[] }
            data += temp.sort{ it[1] as double }
        }
        def precision = generateStatistics(data.collect{ it[2] as double } as double[], "precision")
        def recall = generateStatistics(data.collect{ it[3] as double } as double[], "recall")
        def fp = generateStatistics(data.collect{ it[6] as double } as double[], "#fp")
        def fn = generateStatistics(data.collect{ it[7] as double } as double[], "#fn")
        def f2 = generateStatistics(data.collect{ it[11] as double } as double[], "f2")
        content +=  data
        content +=  precision
        content +=  recall
        content +=  fp
        content +=  fn
        content += f2
        content
    }

    private static generateStatistics(double[] values, String text){
        List<String[]> content = []
        def mean = ["Mean "+text]
        def median = ["Median "+text]
        def sd = ["SD "+text]
        def statistics = new DescriptiveStatistics(values)
        mean.add(statistics.mean as String)
        median.add(statistics.getPercentile(50.0) as String)
        sd.add(statistics.standardDeviation as String)
        content += mean as String[]
        content += median as String[]
        content += sd as String[]
        content
    }

    def findItextControllerFiles(){
        def aux = Util.findFilesFromDirectory(folder)
        controllerFiles = aux.findAll{
            it.endsWith("-text-controller.csv") && !it.contains("${File.separator}evaluation${File.separator}")
        }
        println "itext controllerFiles: ${controllerFiles.size()}"
        controllerFiles.each{ println it }
    }

    def findItextFiles(){
        def aux = Util.findFilesFromDirectory(folder)
        files = aux.findAll{
            it.endsWith("-text.csv") && !it.contains("${File.separator}evaluation${File.separator}")
        }
        println "itext files: ${files.size()}"
        files.each{ println it }
    }

    def findItextNoEmptyControllerFiles(){
        def aux = Util.findFilesFromDirectory(folder)
        noemptyControllerFiles = aux.findAll{
            it.endsWith("-text-controller-noempty.csv") && !it.contains("${File.separator}evaluation${File.separator}")
        }
        println "itext no empty controllerFiles: ${noemptyControllerFiles.size()}"
        noemptyControllerFiles.each{ println it }
    }

    def findItextNoEmptyFiles(){
        def aux = Util.findFilesFromDirectory(folder)
        noemptyFiles = aux.findAll{
            it.endsWith("-text-noempty.csv") && !it.contains("${File.separator}evaluation${File.separator}")
        }
        println "itext no empty files: ${noemptyFiles.size()}"
        noemptyFiles.each{ println it }
    }

}
