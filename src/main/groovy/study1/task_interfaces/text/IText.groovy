package study1.task_interfaces.text

import br.ufpe.cin.tan.analysis.data.ExporterUtil
import br.ufpe.cin.tan.similarity.test.TestSimilarityAnalyser
import br.ufpe.cin.tan.similarity.text.TextualSimilarityAnalyser
import br.ufpe.cin.tan.util.ConstantData
import br.ufpe.cin.tan.util.CsvUtil
import groovy.util.logging.Slf4j

import java.text.SimpleDateFormat
import java.util.regex.Matcher

/*
* Gera interface de tarefa com base no IReal das tarefas mais similares textualmente.
* */

@Slf4j
class IText {

    List tasks
    List pairs
    List similarityResult //t1, ireal1, t2, ireal2, similarity, testSimJaccard, realSimJaccard, testSimCosine, realSimCosine
    List ranking
    final int MAX
    String analysedTasksFile //csv that contains analysed tasks (itest and ireal data)
    String detailedAnalysedTasksFile //csv that contains date information
    String itextEvaluationFile
    String itextControllerEvaluationFile
    String textualSimilarityTasksFile
    List<TextResult> results
    List<TextResult> noemptyResults
    List<TextControllerResult> controllerResults
    List<TextControllerResult> noemptyControllerResults
    List datePerTask
    static int LIMIT = 15 //quantidade de linhas com dados estatísticos no final do arquivo de resultados
    String textFilesFolder
    List<String[]> similarityOutput

    IText(String analysedTasksFile){
        this(analysedTasksFile, 3)
    }

    IText(String analysedTasksFile, int max){
        def initialIndex = analysedTasksFile.lastIndexOf(File.separator)
        def path = analysedTasksFile.substring(0, initialIndex+1)
        def name = analysedTasksFile.substring(initialIndex+1)
        def finalIndex = name.indexOf("-")
        def mainName = name.substring(0, finalIndex)
        this.detailedAnalysedTasksFile = "${path}${mainName}-relevant-detailed.csv"
        this.textualSimilarityTasksFile = "${path}${mainName}-text-similarity.csv"
        this.analysedTasksFile = analysedTasksFile
        configureDatePerTask()
        configureOutputFile()
        MAX = max
        tasks = []
        pairs = []
        similarityResult = []
        similarityOutput = []
        ranking = []
        results = []
        noemptyResults = []
        controllerResults = []
        noemptyControllerResults = []
        extractTasks()
        log.info "ITEXT info"
        log.info "input file: $analysedTasksFile"
        log.info "date info file: $detailedAnalysedTasksFile"
        log.info "itext output file: $itextEvaluationFile"
        log.info "controller itext output file: $itextControllerEvaluationFile"
        log.info "textual similarity file: $textualSimilarityTasksFile\n"
    }

    IText(String analysedTasksFile, String detailedAnalysedTasksFile, String textFilesFolder, int max){
        this.textualSimilarityTasksFile = "tasks\\temp\\textual-similarity_463\\textual_similarity_all.csv"
        this.analysedTasksFile = analysedTasksFile
        this.detailedAnalysedTasksFile = detailedAnalysedTasksFile
        this.textFilesFolder = textFilesFolder
        configureDatePerTask()
        configureOutputFile()
        MAX = max
        tasks = []
        pairs = []
        similarityResult = []
        similarityOutput = []
        ranking = []
        results = []
        noemptyResults = []
        controllerResults = []
        noemptyControllerResults = []
        extractTasks()
        log.info "TASKS INFO: ${tasks.size()}"
        log.info "ITEXT INFO:"
        log.info "input file: $analysedTasksFile"
        log.info "date info file: $detailedAnalysedTasksFile"
        log.info "itext output file: $itextEvaluationFile"
        log.info "controller itext output file: $itextControllerEvaluationFile"
        log.info "textual similarity file: $textualSimilarityTasksFile\n"
    }

    IText(String analysedTasksFile, String detailedAnalysedTasksFile, String textFilesFolder, int max,
          List similarityResult, List<String[]> similarityOutput){
        this.textualSimilarityTasksFile = "tasks\\temp\\textual-similarity_463\\textual_similarity_all.csv"
        this.analysedTasksFile = analysedTasksFile
        this.detailedAnalysedTasksFile = detailedAnalysedTasksFile
        this.textFilesFolder = textFilesFolder
        configureDatePerTask()
        configureOutputFile()
        MAX = max
        tasks = []
        pairs = []
        this.similarityResult = similarityResult
        this.similarityOutput = similarityOutput
        ranking = []
        results = []
        noemptyResults = []
        controllerResults = []
        noemptyControllerResults = []
        extractTasks()
        log.info "TASKS INFO: ${tasks.size()}"
        log.info "ITEXT INFO:"
        log.info "input file: $analysedTasksFile"
        log.info "date info file: $detailedAnalysedTasksFile"
        log.info "itext output file: $itextEvaluationFile"
        log.info "controller itext output file: $itextControllerEvaluationFile"
        log.info "textual similarity file: $textualSimilarityTasksFile\n"
    }

    def generate(){
        computeTaskPairs()
        log.info "pairs: ${pairs.size()}"
        computeSimilarity()
        log.info "similarityResult: ${similarityResult.size()}"
        rankingSimilarity()
        computeIntersectionAmongSimilarTasks()
        exportResult()
        generateControllerResult()
        exportControllerResult()
        exportNoEmptyResults()
    }

    private configureDatePerTask(){
        datePerTask = [] //[id:, date:]
        if (!detailedAnalysedTasksFile || detailedAnalysedTasksFile.empty ||
                !(new File(detailedAnalysedTasksFile).exists())) return
        List<String[]> entries = CsvUtil.read(detailedAnalysedTasksFile)
        if (entries.size() <= 1) return
        def lines = entries.subList(1, entries.size())
        datePerTask = lines.collect{ l ->
            def datesString = l[2].substring(1, l[2].size()-1).split(", ")
            def lastDate = datesString?.last()
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy")
            def date = sdf.parse(lastDate)

            def project = formatProjectName(l[0])
            if(project ==~ /.+_\d+/ ){
                def index = project.lastIndexOf("_")
                project = project.substring(0, index)
            }
            [project:project, id:l[1], date:date]
        }
        datePerTask = datePerTask.sort{ [it.project, it.id] }
    }

    private static formatProjectName(String value){
        def project = value.toLowerCase()
        if(project ==~ /.+_\d+/ ){
            def index = project.lastIndexOf("_")
            project = project.substring(0, index)
        }
        project
    }

    private exportNoEmptyResults(){
        def ids = noemptyResults.collect{ it.id }
        def idsController = noemptyControllerResults.collect{ it.id }
        def selectedResult = ids.intersect(idsController)
        if(!selectedResult.empty){
            def filename = itextEvaluationFile - ".csv" + "-noempty.csv"
            def iTextExporter = new TextExporter(filename, noemptyResults.findAll{ it.id in selectedResult})
            iTextExporter.save()

            def controllerFilename = itextControllerEvaluationFile - ".csv" + "-noempty.csv"
            def textControllerExporter = new TextControllerExporter(controllerFilename,
                    controllerResults.findAll{ it.id in selectedResult})
            textControllerExporter.save()
        }
    }

    private generateControllerResult(){
        controllerResults = []
        results.each{ result ->
            controllerResults += new TextControllerResult(result)
        }
        noemptyControllerResults = controllerResults.findAll{ !it.controllers.empty }
    }

    private exportControllerResult() {
        def textControllerExporter = new TextControllerExporter(itextControllerEvaluationFile, controllerResults)
        textControllerExporter.save()
    }

    private configureOutputFile(){
        def file = new File(analysedTasksFile)
        def index = file.path.lastIndexOf(File.separator)
        def path = file.path.substring(0, index+1)
        def name = ""
        if(analysedTasksFile.contains("-relevant.csv")) name = file.name - "-relevant.csv"
        else name = file.name - ".csv"
        itextEvaluationFile = "${path}${name}-text.csv"
        itextControllerEvaluationFile = itextEvaluationFile - ConstantData.CSV_FILE_EXTENSION + ConstantData.CONTROLLER_FILE_SUFIX
    }

    private extractTasks(){
        if (!analysedTasksFile || analysedTasksFile.empty || !(new File(analysedTasksFile).exists())) return
        List<String[]> entries = CsvUtil.read(analysedTasksFile)
        if (entries.size() <= 1) return
        tasks = entries.subList(1, entries.size()-LIMIT)
        tasks.each{ task ->
            task[0] = formatProjectName(task[0])
        }
    }

    private computeTaskPairs() {
        //datePerTask [project:, id:, date:[]]
        pairs = []
        if (!tasks || tasks.empty) return tasks
        if (tasks.size() == 1){
            pairs.add([task: tasks.first(), pairs: []])
        } else {
            def projects = datePerTask.collect{ it.project }.unique()
            projects.each{ project ->
                log.info "project: $project"
                def tasksFromProject = tasks.findAll{ it[0] == project }
                tasksFromProject.eachWithIndex { v, k ->
                    def next = tasksFromProject - v//tasksFromProject.drop(k+1)
                    def minDate = datePerTask.find{ it.project==v[0] && it.id == v[1] }.date
                    def othersProjectAndId = next.collect{ [it[0], it[1]] }
                    def others = datePerTask.findAll{ [it.project, it.id] in othersProjectAndId }
                    def previousTasks = others.findAll{ it.date.before(minDate) /*|| it.date==minDate*/ }*.id
                    def candidates = tasks.findAll{ it[0]==project && it[1] in previousTasks }.unique()
                    log.info "task: ${v[0]}, ${v[1]}; similar candidates (${candidates.size()}): ${candidates.collect{ [it[0], it[1]]}}"
                    pairs.add([project: project, task: v, pairs: candidates])
                }
            }
            pairs = pairs.unique()
        }
    }

    private extractProjectFolder(project){
        def folder = new File(textFilesFolder)
        def projectfolders = folder.listFiles().findAll{ it.directory }
        def projectFolder = projectfolders.find{ it.name.endsWith(project) }
        projectFolder
    }

    private extractTaskText(projectFolder, taskId) {
        def text = ""
        def filename = "${projectFolder}${File.separator}${taskId}.txt"
        File file = new File(filename)
        if (file.exists()) {
            file.withReader("utf-8") { reader ->
                text = reader.text
            }
        } else {
            log.warn "Text file '${filename}' not found!"
        }
        text
    }

    //Poderia extrair do CSV de similaridade
    private computeSimilarity(){
        //pairs = [project: project, task: v, pairs: candidates]
        pairs?.each { item ->
            def task = item.task
            def projectFolder = extractProjectFolder(task[0])
            def taskText = extractTaskText(projectFolder, task[1])
            def itest1 = task[ExporterUtil.ITEST_INDEX_SHORT_HEADER+1]?.replace(File.separator, Matcher.quoteReplacement(File.separator))
                    ?.substring(1, task[ExporterUtil.ITEST_INDEX_SHORT_HEADER+1].size() - 1)?.split(", ") as List
            def ireal1 = task[ExporterUtil.IREAL_INDEX_SHORT_HEADER+1]?.replace(File.separator, Matcher.quoteReplacement(File.separator))
                    ?.substring(1, task[ExporterUtil.IREAL_INDEX_SHORT_HEADER+1].size() - 1)?.split(", ") as List

            if(!taskText.empty) {
                item.pairs?.each { other ->
                    def otherText = extractTaskText(projectFolder, other[1])
                    if(!otherText.empty) {
                        def textualSimilarityAnalyser = new TextualSimilarityAnalyser()
                        def textSimilarity = textualSimilarityAnalyser.calculateSimilarity(taskText, otherText)
                        def itest2 = other[ExporterUtil.ITEST_INDEX_SHORT_HEADER + 1]
                                ?.replace(File.separator, Matcher.quoteReplacement(File.separator))
                                ?.substring(1, other[ExporterUtil.ITEST_INDEX_SHORT_HEADER + 1].size() - 1)?.split(", ") as List
                        def ireal2 = other[ExporterUtil.IREAL_INDEX_SHORT_HEADER + 1]
                                ?.replace(File.separator, Matcher.quoteReplacement(File.separator))
                                ?.substring(1, other[ExporterUtil.IREAL_INDEX_SHORT_HEADER + 1].size() - 1)?.split(", ") as List

                        def similarityAnalyser = new TestSimilarityAnalyser(itest1, itest2)
                        def testSimJaccard = similarityAnalyser.calculateSimilarityByJaccard()
                        def testSimCosine = similarityAnalyser.calculateSimilarityByCosine()

                        similarityAnalyser = new TestSimilarityAnalyser(ireal1, ireal2)
                        def realSimJaccard = similarityAnalyser.calculateSimilarityByJaccard()
                        def realSimCosine = similarityAnalyser.calculateSimilarityByCosine()

                        similarityResult += [project       : item.project, t1: task[1] as int, ireal1: ireal1, t2: other[1] as int, ireal2: ireal2,
                                             similarity    : textSimilarity, testSimJaccard: testSimJaccard,
                                             realSimJaccard: realSimJaccard,
                                             testSimCosine : testSimCosine, realSimCosine: realSimCosine]
                    }
                }
            }
        }

        eliminateDuplicates()
    }

    private eliminateDuplicates(){
        def result = []
        similarityResult.each{ sr ->
            def others = similarityResult - sr
            def find = others.find{
                (it.project==sr.project && it.t1==sr.t1 && it.t2==sr.t2) ||
                        (it.project==sr.project && it.t1==sr.t2 && it.t2==sr.t1)
            }
            if(!find) result += sr
        }
        similarityResult = result
    }

    //gera o csv de similaridade da pasta tasks\\temp\\textual-similarity\\textual_similarity_all.csv
    private rankingSimilarity(){
        if(similarityOutput.empty) {
            similarityOutput += ["project", "task", "similar", "level"] as String[]
        }
        ranking = []
        def projects = tasks.collect{ it[0] }.unique()
        projects.each{ project ->
            def idList = tasks.findAll{ it[0] == project }.collect{ it[1] as int }.sort()
            idList.each{ id ->
                def results = similarityResult.findAll{ it.project==project && it.t1==id }
                def resultsToFix = results.findAll{ it.project==project && it.t2 == id }
                def finalResult = results - resultsToFix
                resultsToFix.each{ r ->
                    finalResult += [project:project, t1: r.t2, ireal1: r.ireal2, t2: r.t1, ireal2: r.ireal1, similarity: r.similarity,
                                    testSimJaccard: r.testSimJaccard, realSimJaccard: r.realSimJaccard,
                                    testSimCosine: r.testSimCosine, realSimCosine: r.realSimCosine]
                }
                finalResult = finalResult.sort{ a, b -> b.similarity <=> a.similarity }

                def mostSimilarResults = []
                if(finalResult.size() >= MAX){
                    mostSimilarResults = finalResult.subList(0, MAX)
                } else if(!finalResult.empty){
                    mostSimilarResults = finalResult
                }
                if(!mostSimilarResults.empty){
                    mostSimilarResults.each{
                        similarityOutput += [project, id, it.t2, it.similarity] as String[]
                    }
                    ranking += [project: project, id: id, result: mostSimilarResults]
                }
            }
        }
        log.info "similarityOutput: ${similarityOutput.size()-1}"
        CsvUtil.write(textualSimilarityTasksFile, similarityOutput)
    }

    private computeIntersectionAmongSimilarTasks(){
        results = []
        def projectList = pairs*.task*.getAt(0).flatten().unique()
        projectList.each{ project ->
            def tasksPerProject = pairs.findAll{ it.project == project }
            def idList = tasksPerProject*.task*.getAt(1).flatten() as int[]
            idList.each{ id ->
                def value = ranking.find{ (it.project == project) && (it.id == id) }
                if(value){
                    def result = value.result
                    def ireal = result.get(0).ireal1
                    def othersIreal = result*.ireal2*.collect()
                    def intersection = othersIreal.get(0)
                    othersIreal.each {
                        intersection.retainAll(it)
                    }
                    results += new TextResult(id, project, intersection, ireal)
                } else {
                    results += new TextResult(id, project, [], [])
                }
            }
        }
        noemptyResults = results.findAll{ !it.files.empty }
    }

    private exportResult(){
        def iTextExporter = new TextExporter(itextEvaluationFile, results)
        iTextExporter.save()
    }

}
