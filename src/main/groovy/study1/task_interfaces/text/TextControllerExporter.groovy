package study1.task_interfaces.text

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

class TextControllerExporter {

    String filename
    String folder
    List<TextControllerResult> tasks
    String[] csvHeader

    TextControllerExporter(String filename, List<TextControllerResult> tasks) {
        this.filename = filename
        configureCsvHeader()
        def index = filename.lastIndexOf(File.separator)
        folder = filename.substring(0, index)
        if (!tasks || tasks.empty) {
            this.tasks = []
        }
        else {
            this.tasks = tasks
        }
    }

    def save() {
        if (!tasks || tasks.empty) return
        List<String[]> content = []
        content += csvHeader
        tasks?.each { content += it.parseResultToArray() }
        CsvUtil.write(filename, content)
    }

    private configureCsvHeader(){
        def measure1, measure2
        if (Util.SIMILARITY_ANALYSIS) {
            measure1 = "Jaccard"
            measure2 = "Cosine"
        } else {
            measure1 = "Precision"
            measure2 = "Recall"
        }
        csvHeader = ["Project", "Task", measure1, measure2, "#ITextResult", "#IReal", "#FP", "#FN", "#Hits",
                     "ITextResult", "IReal", "F2"] as String[]
    }

}
