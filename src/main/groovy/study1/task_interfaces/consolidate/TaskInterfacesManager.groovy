package study1.task_interfaces.consolidate

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path

/*
* Entrada: Resultados de ITest, IRandom e IText, gerados a partir de subamostras de uma amostra em comum.
* Objetivo: Gerar um novo resultado para cada tipo de interface considerando apenas as tarefas em comum entre os resultados.
* */
class TaskInterfacesManager {

    String selfContainedTasksCsv
    String mainFolder
    String inputFolder
    String outputFolder

    List<String> irandomFiles
    List<String> itestFiles
    List<String> itextFiles

    //project = index 0; task = index 1
    List<String[]> irandomTasks
    List<String[]> irandomControllerTasks
    List<String[]> itestAddedTasks
    List<String[]> itestAddedControllerTasks
    List<String[]> itestAddedWhenTasks
    List<String[]> itestAddedWhenControllerTasks
    List<String[]> itestAllTasks
    List<String[]> itestAllControllerTasks
    List<String[]> itestAllWhenTasks
    List<String[]> itestAllWhenControllerTasks
    List<String[]> itextTasks
    List<String[]> itextControllerTasks

    List commonTasks //[project, id]

    String[] irandomHeader
    String[] itestHeader
    String[] itextHeader

    String outputIrandomFile
    String outputIrandomControllerFile
    String outputItestAddedFile
    String outputItestAddedControllerFile
    String outputItestAddedWhenFile
    String outputItestAddedWhenControllerFile
    String outputItestAllFile
    String outputItestAllControllerFile
    String outputItestAllWhenFile
    String outputItestAllWhenControllerFile
    String outputResultFile

    final int IRANDOM_PRECISION_INDEX = 2
    final int IRANDOM_RECALL_INDEX = 3
    final int IRANDOM_FP_INDEX = 6
    final int IRANDOM_FN_INDEX = 7
    final int IRANDOM_F2_INDEX = 9

    final int ITEXT_PRECISION_INDEX = 2
    final int ITEXT_RECALL_INDEX = 3
    final int ITEXT_FP_INDEX = 6
    final int ITEXT_FN_INDEX = 7
    final int ITEXT_F2_INDEX = 11

    final int ITEST_PRECISION_INDEX = 12
    final int ITEST_RECALL_INDEX = 13
    final int ITEST_FP_INDEX = 27
    final int ITEST_FN_INDEX = 28
    final int ITEST_F2_INDEX = 33

    List<String[]> selectedIrandomTasks
    List<String[]> selectedIrandomControllerTasks
    List<String[]> selectedItestAddedTasks
    List<String[]> selectedItestAddedControllerTasks
    List<String[]> selectedItestAddedWhenTasks
    List<String[]> selectedItestAddedWhenControllerTasks
    List<String[]> selectedItestAllTasks
    List<String[]> selectedItestAllControllerTasks
    List<String[]> selectedItestAllWhenTasks
    List<String[]> selectedItestAllWhenControllerTasks

    TaskInterfacesManager(String entry, String selfContainedTasksCsv){
        this.selfContainedTasksCsv = selfContainedTasksCsv
        mainFolder = entry
        inputFolder = "${mainFolder}${File.separator}input"
        outputFolder = "${mainFolder}${File.separator}output"
        File f = new File(outputFolder)
        if(f.exists()){ f.deleteDir() }
        f.mkdir()

        configureOutputFiles()
        findIrandomTasks()
        findItestTasks()
        findItextTasks()
    }

    static void main(String[] args){
        def folder = "results${File.separator}sample5-noviews-task_interfaces"
        def selfContainedTasksCsv = "results${File.separator}sample5-task_interfaces${File.separator}output${File.separator}result.csv"
        TaskInterfacesManager manager = new TaskInterfacesManager(folder, selfContainedTasksCsv)
        manager.findIntersectionAmongResults()
    }

    def findIntersectionAmongResults(){
        commonTasks = itextTasks?.collect {
            if(it.size()>2) [project: it[0], id: it[1]]
        }
        commonTasks?.removeAll([null])

        if(itextTasks.empty){ //não hoa resultado de itext ou irandom gerado, então considerar tarefas auto-contidas
            def temp = itestAllTasks?.collect {
                if(it.size()>2) [project: it[0], id: it[1]]
            }
            temp?.removeAll([null])

            def selfContainedTasksInput = CsvUtil.read(selfContainedTasksCsv)
            selfContainedTasksInput.remove(0)
            def selfContainedTasks = selfContainedTasksInput.collect{ [url:it[0].toLowerCase(), id:it[1]] }
            temp?.each{ t ->
                def match = selfContainedTasks.find{ it.url.contains(t.project) && it.id==t.id }
                if(match) commonTasks += t
            }
        }
        println "commonTasks: ${commonTasks.size()}"
        organizeIrandomResult()
        organizeItestResult()
        organizeItextResult()
        collapseResults()
    }

    private collectData(){
        def projectsColumn = commonTasks.collect {
            def aux = it.project
            if (aux.contains("_")) {
                def index = aux.indexOf("_")
                aux = aux.substring(0, index)
            }
            aux
        }
        def tasksColumn = commonTasks.collect{ it.id }
        def precision_all = selectedItestAllTasks.collect{ it[ITEST_PRECISION_INDEX] } //1
        def recall_all = selectedItestAllTasks.collect{ it[ITEST_RECALL_INDEX] } //1
        def precision_added = selectedItestAddedTasks.collect{ it[ITEST_PRECISION_INDEX] } //2
        def recall_added = selectedItestAddedTasks.collect{ it[ITEST_RECALL_INDEX] } //2
        def precision_all_controller = selectedItestAllControllerTasks.collect{ it[ITEST_PRECISION_INDEX] } //3
        def recall_all_controller = selectedItestAllControllerTasks.collect{ it[ITEST_RECALL_INDEX] as Double } //3
        def precision_added_controller = selectedItestAddedControllerTasks.collect{ it[ITEST_PRECISION_INDEX] } //4
        def recall_added_controller = selectedItestAddedControllerTasks.collect{ it[ITEST_RECALL_INDEX]} //4
        def precision_all_when = selectedItestAllWhenTasks.collect{ it[ITEST_PRECISION_INDEX] } //5
        def recall_all_when = selectedItestAllWhenTasks.collect{ it[ITEST_RECALL_INDEX] } //5
        def precision_added_when = selectedItestAddedWhenTasks.collect{ it[ITEST_PRECISION_INDEX] }
        def recall_added_when = selectedItestAddedWhenTasks.collect{ it[ITEST_RECALL_INDEX] }
        def precision_all_when_controller = selectedItestAllWhenControllerTasks.collect{ it[ITEST_PRECISION_INDEX] }
        def recall_all_when_controller = selectedItestAllWhenControllerTasks.collect{ it[ITEST_RECALL_INDEX] }
        def precision_added_when_controller = selectedItestAddedWhenControllerTasks.collect{ it[ITEST_PRECISION_INDEX] }
        def recall_added_when_controller = selectedItestAddedWhenControllerTasks.collect{ it[ITEST_RECALL_INDEX] }

        def precision_random = selectedIrandomTasks?.collect{ it[IRANDOM_PRECISION_INDEX] }
        def recall_random = selectedIrandomTasks?.collect{ it[IRANDOM_RECALL_INDEX] }
        def precision_random_controller = selectedIrandomControllerTasks?.collect{ it[IRANDOM_PRECISION_INDEX] }
        def recall_random_controller = selectedIrandomControllerTasks?.collect{ it[IRANDOM_RECALL_INDEX] }

        def precision_text = itextTasks?.collect{ it[ITEXT_PRECISION_INDEX] }
        def recall_text = itextTasks?.collect{ it[ITEXT_RECALL_INDEX] }
        def precision_text_controller = itextControllerTasks?.collect{ it[ITEXT_PRECISION_INDEX] }
        def recall_text_controller = itextControllerTasks?.collect{ it[ITEXT_RECALL_INDEX] }

        def result
        if(precision_random== null || precision_random.empty || precision_text==null || precision_text.empty){
            result = GroovyCollections.transpose(projectsColumn, tasksColumn, precision_all, recall_all, precision_added,
                    recall_added, precision_all_controller, recall_all_controller, precision_added_controller,
                    recall_added_controller, precision_all_when, recall_all_when, precision_added_when, recall_added_when,
                    precision_all_when_controller, recall_all_when_controller, precision_added_when_controller,
                    recall_added_when_controller)
        }
        else { //caso de só existir resultado de itest
            result = GroovyCollections.transpose(projectsColumn, tasksColumn, precision_all, recall_all, precision_added,
                    recall_added, precision_all_controller, recall_all_controller, precision_added_controller,
                    recall_added_controller, precision_all_when, recall_all_when, precision_added_when, recall_added_when,
                    precision_all_when_controller, recall_all_when_controller, precision_added_when_controller,
                    recall_added_when_controller, precision_random, recall_random, precision_random_controller,
                    recall_random_controller, precision_text, recall_text, precision_text_controller, recall_text_controller)
        }

        List<String[]> content = []
        result?.each{
            content += it as String[]
        }
        content
    }

    private organizeIrandomResult(){
        if(irandomTasks==null || irandomTasks.empty) return
        selectedIrandomTasks = findIntersection(irandomTasks)
        CsvUtil.write(outputIrandomFile,
                [irandomHeader] + generateStatisticsForIrandom(selectedIrandomTasks) )

        selectedIrandomControllerTasks = findIntersection(irandomControllerTasks)
        CsvUtil.write(outputIrandomControllerFile,
                [irandomHeader] + generateStatisticsForIrandom(selectedIrandomControllerTasks) )
    }

    private organizeItestResult(){
        selectedItestAddedTasks = findIntersection(itestAddedTasks)
        CsvUtil.write(outputItestAddedFile,
                [itestHeader] + generateStatisticsForItest(selectedItestAddedTasks))

        selectedItestAddedControllerTasks = findIntersection(itestAddedControllerTasks)
        CsvUtil.write(outputItestAddedControllerFile,
                [itestHeader] + generateStatisticsForItest(selectedItestAddedControllerTasks))

        selectedItestAddedWhenTasks = findIntersection(itestAddedWhenTasks)
        CsvUtil.write(outputItestAddedWhenFile,
                [itestHeader] + generateStatisticsForItest(selectedItestAddedWhenTasks))

        selectedItestAddedWhenControllerTasks = findIntersection(itestAddedWhenControllerTasks)
        CsvUtil.write(outputItestAddedWhenControllerFile,
                [itestHeader] + generateStatisticsForItest(selectedItestAddedWhenControllerTasks))

        selectedItestAllTasks = findIntersection(itestAllTasks)
        CsvUtil.write(outputItestAllFile,
                [itestHeader] + generateStatisticsForItest(selectedItestAllTasks))

        selectedItestAllControllerTasks = findIntersection(itestAllControllerTasks)
        CsvUtil.write(outputItestAllControllerFile,
                [itestHeader] + generateStatisticsForItest(selectedItestAllControllerTasks))

        selectedItestAllWhenTasks = findIntersection(itestAllWhenTasks)
        CsvUtil.write(outputItestAllWhenFile,
                [itestHeader] + generateStatisticsForItest(selectedItestAllWhenTasks))

        selectedItestAllWhenControllerTasks = findIntersection(itestAllWhenControllerTasks)
        CsvUtil.write(outputItestAllWhenControllerFile,
                [itestHeader] + generateStatisticsForItest(selectedItestAllWhenControllerTasks))
    }

    private organizeItextResult(){
        if(itextFiles==null || itextFiles.empty) return
        itextFiles.each{
            if(it.endsWith("itext-noempty.csv") || it.endsWith("itext-controller-noempty.csv")){
                Path source = FileSystems.getDefault().getPath(it)
                Path target = FileSystems.getDefault().getPath(outputFolder)
                Files.copy(source, target.resolve(source.getFileName()))
            }
        }
    }

    private configureOutputFiles(){
        outputIrandomFile = "${outputFolder}${File.separator}irandom.csv"
        outputIrandomControllerFile = "${outputFolder}${File.separator}irandom-controller.csv"
        outputItestAddedFile = "${outputFolder}${File.separator}itest_added.csv"
        outputItestAddedControllerFile = "${outputFolder}${File.separator}itest_added_controller.csv"
        outputItestAddedWhenFile = "${outputFolder}${File.separator}itest_added_when.csv"
        outputItestAddedWhenControllerFile = "${outputFolder}${File.separator}itest_added_when_controller.csv"
        outputItestAllFile = "${outputFolder}${File.separator}itest_all.csv"
        outputItestAllControllerFile = "${outputFolder}${File.separator}itest_all_controller.csv"
        outputItestAllWhenFile = "${outputFolder}${File.separator}itest_all_when.csv"
        outputItestAllWhenControllerFile = "${outputFolder}${File.separator}itest_all_when_controller.csv"
        outputResultFile = "${outputFolder}${File.separator}result.csv"
    }

    private findIrandomTasks(){
        irandomFiles = Util.findFilesFromDirectory("${inputFolder}${File.separator}irandom")?.findAll{
            it.endsWith(".csv")
        }
        def controllers = irandomFiles?.find{ it.endsWith("irandom-controller.csv") }
        irandomControllerTasks = findTasks(controllers)
        def other = irandomFiles?.find{ it.endsWith("irandom.csv") }
        irandomTasks = findTasks(other)
        irandomHeader = findHeader(other)
    }

    //o nome dos arquivos tem que ser exatamente como definido
    private findItestTasks(){
        itestFiles = Util.findFilesFromDirectory("${inputFolder}${File.separator}itest")?.findAll{
            it.endsWith(".csv")
        }?.sort()

        itestAddedTasks = findTasks(itestFiles?.get(0))
        itestAddedControllerTasks = findTasks(itestFiles?.get(1))
        itestAddedWhenTasks = findTasks(itestFiles?.get(2))
        itestAddedWhenControllerTasks = findTasks(itestFiles?.get(3))
        itestAllTasks = findTasks(itestFiles?.get(4))
        itestAllControllerTasks = findTasks(itestFiles?.get(5))
        itestAllWhenTasks = findTasks(itestFiles?.get(6))
        itestAllWhenControllerTasks = findTasks(itestFiles?.get(7))

        itestHeader = findHeader(itestFiles?.get(0)) //pode usar qualquer arquivo desses acima
    }

    private findItextTasks(){
        itextFiles = Util.findFilesFromDirectory("${inputFolder}${File.separator}itext")?.findAll{
            it.endsWith(".csv")
        }

        def controllers = itextFiles?.find{ it.endsWith("itext-controller-noempty.csv") }
        itextControllerTasks = findTasks(controllers)
        def other = itextFiles?.find{ it.endsWith("itext-noempty.csv") }
        itextTasks = findTasks(other)
        itextHeader = findHeader(other)
    }

    private static findTasks(String file){
        if(file == null || file.empty) return []
        def tasks = CsvUtil.read(file)
        tasks.remove(0)
        tasks = tasks.subList(0, tasks.size()-15)
        tasks.each { t ->
            if(t[0].contains("_")) {
                def index = t[0].indexOf("_")
                t[0] = t[0].substring(0, index)
            }
        }
        tasks
    }

    private static findHeader(String file){
        if(file == null || file.empty) return []
        def tasks = CsvUtil.read(file)
        tasks.get(0)
    }

    private findIntersection(List tasks){
        List<String[]> selected = []
        commonTasks?.each{ ct ->
            def found = tasks.find{ it[0]==ct.project && it[1]==ct.id }
            if(found) {
                selected += found
            }
        }
        selected
    }

    private generateStatisticsForIrandom(List<String[]> selected){
        List<String[]> content = []
        def precision = generateStatistics(selected.collect{ it[IRANDOM_PRECISION_INDEX] as double } as double[], "precision")
        def recall = generateStatistics(selected.collect{ it[IRANDOM_RECALL_INDEX] as double } as double[], "recall")
        def fp = generateStatistics(selected.collect{ it[IRANDOM_FP_INDEX] as double } as double[], "#fp")
        def fn = generateStatistics(selected.collect{ it[IRANDOM_FN_INDEX] as double } as double[], "#fn")
        def f2 = generateStatistics(selected.collect{ it[IRANDOM_F2_INDEX] as double } as double[], "f2")
        content += selected
        content += precision
        content += recall
        content += fp
        content += fn
        content += f2
        content
    }

    private generateStatisticsForItest(List<String[]> selected){
        List<String[]> content = []
        def precision = generateStatistics(selected.collect{ it[ITEST_PRECISION_INDEX] as double } as double[], "precision")
        def recall = generateStatistics(selected.collect{ it[ITEST_RECALL_INDEX] as double } as double[], "recall")
        def fp = generateStatistics(selected.collect{ it[ITEST_FP_INDEX] as double } as double[], "#fp")
        def fn = generateStatistics(selected.collect{ it[ITEST_FN_INDEX] as double } as double[], "#fn")
        def f2 = generateStatistics(selected.collect{ it[ITEST_F2_INDEX] as double } as double[], "f2")
        content += selected
        content += precision
        content += recall
        content += fp
        content += fn
        content += f2
        content
    }

    private static generateStatistics(double[] values, String text){
        List<String[]> content = []
        def mean = ["Mean "+text]
        def median = ["Median "+text]
        def sd = ["SD "+text]
        def statistics = new DescriptiveStatistics(values)
        mean.add(statistics.mean as String)
        median.add(statistics.getPercentile(50.0) as String)
        sd.add(statistics.standardDeviation as String)
        content += mean as String[]
        content += median as String[]
        content += sd as String[]
        content
    }

    /*
        1  - changed
        2  - added
        3  - changed_controller
        4  - added_controller
        5  - changed_when
        6  - added_when
        7  - changed_when_controller
        8  - added_when_controller
        9  - random
        10 - random_controller
        11 - text
        12 - text_controller
     */
    private collapseResults(){
        List<String[]> content = []
        String[] header = ["Project","Task","P_1","R_1","P_2","R_2","P_3","R_3","P_4","R_4","P_5","R_5","P_6",
                                 "R_6","P_7","R_7","P_8","R_8","P_9","R_9","P_10","R_10","P_11","R_11","P_12","R_12"]
        content += header
        content += collectData()
        CsvUtil.write(outputResultFile, content)
    }

}
