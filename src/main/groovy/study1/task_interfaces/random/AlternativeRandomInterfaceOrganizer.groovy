package study1.task_interfaces.random

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

class AlternativeRandomInterfaceOrganizer {

    String folder
    List<String> controllerFiles
    List<String> files
    String outputControllerFile
    String outputFile

    AlternativeRandomInterfaceOrganizer(String folder){
        this.folder = folder
        outputControllerFile = "$folder${File.separator}irandom-controller.csv"
        outputFile = "$folder${File.separator}irandom.csv"
        findIrandomControllerFiles()
        findIrandomFiles()
    }

    def summarize(){
        summarizeResults()
        summarizeControllerResults()
    }

    private summarizeControllerResults(){
        def content = summarizeData(controllerFiles)
        CsvUtil.write(outputControllerFile, content)
    }

    private summarizeResults(){
        def content = summarizeData(files)
        CsvUtil.write(outputFile, content)
    }

    private static summarizeData(resultFiles){
        String[] firstLine = ["project", "task", "precision", "recall", "irandom_size", "ireal_size", "fp_size",
                              "fn_size", "hits_size", "f2"] as String[]
        List<String[]> content = []
        List<String[]> data = []
        content +=  firstLine
        resultFiles.each{ file ->
            def lines = CsvUtil.read(file)
            lines.remove(0)
            lines.each{
                if(it[0] == "one-click-orgs") it[0] = "oneclickorgs"
                else if(it[0] == "theodinproject") it[0] = "odin"
            }
            data += lines
        }

        def precision = generateStatistics(data.collect{ it[2] as double } as double[], "precision")
        def recall = generateStatistics(data.collect{ it[3] as double } as double[], "recall")
        def fp = generateStatistics(data.collect{ it[6] as double } as double[], "#fp")
        def fn = generateStatistics(data.collect{ it[7] as double } as double[], "#fn")
        def f2 = generateStatistics(data.collect{ it[9] as double } as double[], "f2")
        content +=  data.sort{ it[0] } //data.sort{ [it[0], (it[1] as double)] }
        content +=  precision
        content +=  recall
        content +=  fp
        content +=  fn
        content += f2
        content
    }

    private static generateStatistics(double[] values, String text){
        List<String[]> content = []
        def mean = ["Mean "+text]
        def median = ["Median "+text]
        def sd = ["SD "+text]
        def statistics = new DescriptiveStatistics(values)
        mean.add(statistics.mean as String)
        median.add(statistics.getPercentile(50.0) as String)
        sd.add(statistics.standardDeviation as String)
        content += mean as String[]
        content += median as String[]
        content += sd as String[]
        content
    }

    private findIrandomControllerFiles(){
        def aux = Util.findFilesFromDirectory(folder)
        controllerFiles = aux.findAll{ it =~ /.*-random-controller_\d+.csv$/ }
    }

    private findIrandomFiles(){
        def aux = Util.findFilesFromDirectory(folder)
        files = aux.findAll{ it =~ /.*-random_\d+.csv$/ }
    }

}
