package study1.task_interfaces.random

import br.ufpe.cin.tan.analysis.data.TaskImporter
import br.ufpe.cin.tan.util.ConstantData
import br.ufpe.cin.tan.util.CsvUtil
import groovy.util.logging.Slf4j

/*
* Calcula interface randômica segundo 2 estratégias.
* */

@Slf4j
class IRandom {

    List<RandomResult> results
    List<RandomControllerResult> controllerResults
    File file
    String randomTasksFile
    String randomControllerTasksFile
    TaskImporter taskImporter
    String projectFolder

    IRandom(String filename, String projectFolder){
        file = new File(filename)
        taskImporter = new TaskImporter(file)
        setProjectFolder(projectFolder)
    }

    def generateRandomResult() {
        generateIrandom()
        exportRandomResult()
        generateRandomControllerResult()
        exportControllerRandomResult()
    }

    def generateAlternativeRandomResult() {
        generateAlternativeIrandom()
        exportRandomResult()
        generateRandomControllerResult()
        exportControllerRandomResult()
    }

    def generateAndSaveAlternativeIrandom(){
        def randomExporter = new RandomExporter(randomTasksFile)
        def controllerRandomExporter = new RandomControllerExporter(randomControllerTasksFile)

        log.info "<  Generating random result for tasks from '${file.path}'... >"
        taskImporter.extractPtTasks(true)
        def tasks = taskImporter.candidateTasks.sort{
            def name = it.gitRepository.name.toLowerCase()
            def index = name.lastIndexOf("_")
            if(index>0) name = name.substring(index+1)
            [name, it.id]
        }

        tasks.each { task ->
            log.info "Task ${task.id}"
            def ireal = task.computeRealInterface().findFilteredFiles() as List
            def buffer = task.computeRandomInterfaceSortingEachFile(10)
            def name = task.gitRepository.name.toLowerCase()
            def index = name.lastIndexOf("_")
            if(index>0) name = name.substring(index+1)
            def result = new RandomResult(buffer, ireal, task.id, name)
            randomExporter.append(result)
            def controllerResult = new RandomControllerResult(result)
            controllerRandomExporter.append(controllerResult)
        }
        log.info "Random task interfaces were computed for ${taskImporter.candidateTasks.size()} tasks!"
    }

    private void setProjectFolder(String projectFolder){
        //this.projectFolder = "results" + File.separator + "random"
        this.projectFolder = projectFolder
        File folder = new File(projectFolder)
        if (!folder.exists()) folder.mkdir()

        this.projectFolder += File.separator + (file.name - ConstantData.CSV_FILE_EXTENSION)
        folder = new File(this.projectFolder)
        if (!folder.exists()) folder.mkdir()

        def evaluationFile = folder.path + File.separator + file.name
        def name = evaluationFile - ConstantData.CSV_FILE_EXTENSION
        randomTasksFile = name + ConstantData.RANDOM_RESULTS_FILE_SUFIX
        randomControllerTasksFile = randomTasksFile - ConstantData.CSV_FILE_EXTENSION + ConstantData.CONTROLLER_FILE_SUFIX
    }

    private generateIrandom(){
        results = []
        log.info "<  Generating random result for tasks from '${file.path}'... >"
        taskImporter.extractPtTasks(true)
        taskImporter.candidateTasks.each { task ->
            log.info "Task ${task.id}"
            def ireal = task.computeRealInterface().findFilteredFiles() as List
            def buffer = task.computeRandomInterface(10)
            results += new RandomResult(buffer, ireal, task.id, task.gitRepository.url)
        }
        log.info "Random task interfaces were computed for ${taskImporter.candidateTasks.size()} tasks!"
    }

    private generateAlternativeIrandom(){
        results = []
        log.info "<  Generating random result for tasks from '${file.path}'... >"
        taskImporter.extractPtTasks(true)
        taskImporter.candidateTasks.each { task ->
            log.info "Task ${task.id}"
            def ireal = task.computeRealInterface().findFilteredFiles() as List
            def buffer = task.computeRandomInterfaceSortingEachFile(10)
            results += new RandomResult(buffer, ireal, task.id, task.gitRepository.url)
        }
        log.info "Random task interfaces were computed for ${taskImporter.candidateTasks.size()} tasks!"
    }

    private exportRandomResult() {
        def randomResultExporter = new RandomExporter(randomTasksFile, results)
        randomResultExporter.save()
    }

    private generateRandomControllerResult(){
        controllerResults = []
        results.each{ result ->
            controllerResults += new RandomControllerResult(result)
        }
    }

    private exportControllerRandomResult() {
        def controllerRandomExporter = new RandomControllerExporter(randomControllerTasksFile, controllerResults)
        controllerRandomExporter.save()
    }

}
