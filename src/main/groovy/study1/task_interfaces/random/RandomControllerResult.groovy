package study1.task_interfaces.random

import br.ufpe.cin.tan.evaluation.TaskInterfaceEvaluator
import br.ufpe.cin.tan.similarity.test.TestSimilarityAnalyser
import br.ufpe.cin.tan.util.Util
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

class RandomControllerResult {

    int id
    String url
    double precision
    double recall
    double f2measure
    double size
    double falsePositives
    double falseNegatives
    double hits
    def hitMetrics
    def controllers
    def irealControllers

    RandomControllerResult(RandomResult result) {
        id = result.id
        url = result.url
        irealControllers = result.ireal.findAll{ Util.isControllerFile(it) }
        extractControllers(result.results)
        setPrecision()
        setRecall()
        setSize()
        setF2Measure()
        setHitMetrics()
    }

    def parseRandomResultToArray() {
        [id, precision, recall, size, getRealSize(), falsePositives, falseNegatives, hits, f2measure] as String[]
    }

    def parseRandomResultToArrayWithUrl() {
        [url, id, precision, recall, size, getRealSize(), falsePositives, falseNegatives, hits, f2measure] as String[]
    }

    private getRealSize(){
        irealControllers.size() as double
    }

    private extractControllers(Collection buffer){
        controllers = []
        buffer.each{ result ->
            controllers.add(result.findAll{ Util.isControllerFile(it) })
        }
    }

    void setPrecision(){
        def precision = controllers.collect {
            if (Util.SIMILARITY_ANALYSIS) {
                def similarityAnalyser = new TestSimilarityAnalyser(it, irealControllers)
                similarityAnalyser.calculateSimilarityByJaccard()
            } else {
                TaskInterfaceEvaluator.calculateFilesPrecision(it as Set, irealControllers as Set)
            }
        }
        def precisionStats = new DescriptiveStatistics(precision as double[])
        this.precision = precisionStats.mean
    }

    void setRecall(){
        def recall = controllers.collect {
            if (Util.SIMILARITY_ANALYSIS) {
                def similarityAnalyser = new TestSimilarityAnalyser(it, irealControllers)
                similarityAnalyser.calculateSimilarityByCosine()
            } else {
                TaskInterfaceEvaluator.calculateFilesRecall(it as Set, irealControllers as Set)
            }
        }
        def recallStats = new DescriptiveStatistics(recall as double[])
        this.recall = recallStats.mean
    }

    void setSize(){
        def size = controllers.collect{ it.size() as double }
        def sizeStats = new DescriptiveStatistics(size as double[])
        this.size = sizeStats.mean
    }

    void setF2Measure(){
        def p = precision
        def r = recall
        def denominator = 4*p+r
        if(denominator==0) f2measure = 0
        else f2measure = 5 * ((p*r)/denominator)
    }

    void setHitMetrics(){
        hitMetrics = []
        controllers.each{ result ->
            def falsePositives = result - irealControllers
            def falseNegatives = irealControllers - result
            def hits = result.intersect(irealControllers)
            hitMetrics += [fp: falsePositives.size(), fn: falseNegatives.size(), hits: hits.size()]
        }
        setFalsePositives()
        setFalseNegatives()
        setHits()
    }

    void setFalsePositives(){
        def falsePositives = hitMetrics.collect{ it.fp as double }
        def fpStats = new DescriptiveStatistics(falsePositives as double[])
        this.falsePositives = fpStats.mean
    }

    void setFalseNegatives(){
        def falseNegatives = hitMetrics.collect{ it.fn as double }
        def fnStats = new DescriptiveStatistics(falseNegatives as double[])
        this.falseNegatives = fnStats.mean
    }

    void setHits(){
        def hits = hitMetrics.collect{ it.hits as double }
        def hitsStats = new DescriptiveStatistics(hits as double[])
        this.hits = hitsStats.mean
    }

}
