package study1.task_interfaces.random

import br.ufpe.cin.tan.util.Util

class Main {

    static void main(String[] args){
        def files = Util.findFilesFromDirectory("tasks\\temp\\random-makrio-check")
        files.each{ file ->
            println file
            IRandom iRandom = new IRandom(file, "random")
            iRandom.generateAlternativeRandomResult()
        }

        /*def files = Util.findFilesFromDirectory("tasks${File.separator}sample4")
        files.each{ file ->
            println file
            IRandom iRandom = new IRandom(file, "results" + File.separator + "random1")
            iRandom.generateRandomResult()
            iRandom.projectFolder = "results" + File.separator + "random2"
            iRandom.generateAlternativeRandomResult()
        }*/

        /*def folder1 = "tasks\\temp\\cucumber_sample_random"
        RandomInterfaceOrganizer evaluator1 = new RandomInterfaceOrganizer(folder1)
        evaluator1.summarize()*/

        /*def folder2 = "results${File.separator}sample4${File.separator}random${File.separator}random2"
        RandomInterfaceOrganizer evaluator2 = new RandomInterfaceOrganizer(folder2)
        evaluator2.summarize()*/

    }

}
