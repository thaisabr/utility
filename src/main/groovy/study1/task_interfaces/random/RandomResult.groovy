package study1.task_interfaces.random

import br.ufpe.cin.tan.evaluation.TaskInterfaceEvaluator
import br.ufpe.cin.tan.similarity.test.TestSimilarityAnalyser
import br.ufpe.cin.tan.util.Util
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

class RandomResult {

    int id
    String url
    double precision
    double recall
    double f2measure
    double size
    double falsePositives
    double falseNegatives
    double hits
    def hitMetrics
    Collection results
    Collection ireal

    RandomResult(List buffer, List ireal, int id, String url){
        this.ireal = ireal
        this.id = id
        this.url = url
        setResults(buffer)
    }

    void setResults(def results){
        this.results = results
        setPrecision()
        setRecall()
        setSize()
        setHitMetrics()
        setF2Measure()
    }

    void setPrecision(){
        def precision = results.collect {
            if (Util.SIMILARITY_ANALYSIS) {
                def similarityAnalyser = new TestSimilarityAnalyser(it, ireal)
                similarityAnalyser.calculateSimilarityByJaccard()
            } else {
                TaskInterfaceEvaluator.calculateFilesPrecision(it as Set, ireal as Set)
            }
        }
        println "precision: $precision"
        def precisionStats = new DescriptiveStatistics(precision as double[])
        this.precision = precisionStats.mean
        setF2Measure()
    }

    void setRecall(){
        def recall = results.collect {
            if (Util.SIMILARITY_ANALYSIS) {
                def similarityAnalyser = new TestSimilarityAnalyser(it, ireal)
                similarityAnalyser.calculateSimilarityByCosine()
            } else {
                TaskInterfaceEvaluator.calculateFilesRecall(it as Set, ireal as Set)
            }
        }
        println "recall: $recall"
        def recallStats = new DescriptiveStatistics(recall as double[])
        this.recall = recallStats.mean
        setF2Measure()
    }

    void setSize(){
        def size = results.collect{ it.size() as double }
        def sizeStats = new DescriptiveStatistics(size as double[])
        this.size = sizeStats.mean
    }

    void setF2Measure(){
        def p = precision
        def r = recall
        double denominator = 4*p+r
        if(denominator==0) f2measure = (0 as double)
        else f2measure = (5 * ((p*r)/denominator)) as double
    }

    double getRealSize(){
        ireal.size() as double
    }

    void setHitMetrics(){
        hitMetrics = []
        results.each{ result ->
            def falsePositives = result - ireal
            def falseNegatives = ireal - result
            def hits = result.intersect(ireal)
            hitMetrics += [fp: falsePositives.size(), fn: falseNegatives.size(), hits: hits.size()]
        }

        setFalsePositives()
        setFalseNegatives()
        setHits()
    }

    void setFalsePositives(){
        def falsePositives = hitMetrics.collect{ it.fp as double }
        def fpStats = new DescriptiveStatistics(falsePositives as double[])
        this.falsePositives = fpStats.mean
    }

    void setFalseNegatives(){
        def falseNegatives = hitMetrics.collect{ it.fn as double }
        def fnStats = new DescriptiveStatistics(falseNegatives as double[])
        this.falseNegatives = fnStats.mean
    }

    void setHits(){
        def hits = hitMetrics.collect{ it.hits as double }
        def hitsStats = new DescriptiveStatistics(hits as double[])
        this.hits = hitsStats.mean
    }

    def parseRandomResultToArray() {
        [id, precision, recall, size, getRealSize(), falsePositives, falseNegatives, hits, f2measure] as String[]
    }

    def parseRandomResultToArrayWithUrl() {
        [url, id, precision, recall, size, getRealSize(), falsePositives, falseNegatives, hits, f2measure] as String[]
    }

}
