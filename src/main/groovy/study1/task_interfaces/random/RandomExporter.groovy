package study1.task_interfaces.random

import br.ufpe.cin.tan.analysis.data.ExporterUtil
import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

class RandomExporter {

    String filename
    String folder
    String url
    List<RandomResult> tasks
    String[] csvHeader

    RandomExporter(String filename, List<RandomResult> tasks) {
        this.filename = filename
        configureCsvHeader()
        def index = filename.lastIndexOf(File.separator)
        folder = filename.substring(0, index)
        if (!tasks || tasks.empty) {
            this.tasks = []
            this.url = ""
        }
        else {
            this.tasks = tasks
            this.url = tasks.first().url
        }
    }

    RandomExporter(String filename) {
        this.filename = filename
        File file = new File(filename)
        if(file.exists()) file.delete()
        csvHeader = ["project","task", "precision", "recall", "irandom_size", "ireal_size", "fp_size", "fn_size",
                     "hits_size", "f2"] as String[]
        List<String[]> content = []
        content += csvHeader
        CsvUtil.append(filename, content)
    }

    def append(RandomResult task){
        List<String[]> content = []
        content += task.parseRandomResultToArrayWithUrl()
        CsvUtil.append(filename, content)
    }

    def save() {
        if (!tasks || tasks.empty) return
        List<String[]> content = []
        content += ["Repository", url] as String[]
        content += generateNumeralData()
        content += csvHeader
        tasks?.each { content += it.parseRandomResultToArray() }
        CsvUtil.write(filename, content)
    }

    private generateNumeralData() {
        if (!tasks || tasks.empty) return []
        double[] precisionValues = tasks.collect { it.precision }
        double[] recallValues = tasks.collect { it.recall }
        double[] f2Values = tasks.collect{ it.f2measure }
        ExporterUtil.generateStatistics(precisionValues, recallValues, f2Values)
    }

    private configureCsvHeader(){
        def measure1, measure2
        if (Util.SIMILARITY_ANALYSIS) {
            measure1 = "Jaccard"
            measure2 = "Cosine"
        } else {
            measure1 = "Precision"
            measure2 = "Recall"
        }
        csvHeader = ["TASK", measure1, measure2, "#IRandom", "#IReal", "#FP", "#FN", "#Hits", "F2"] as String[]
    }

}
