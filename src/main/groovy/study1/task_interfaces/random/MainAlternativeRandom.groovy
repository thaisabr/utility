package study1.task_interfaces.random

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

class MainAlternativeRandom {

    static void main(String[] args){
        //def file = "tasks${File.separator}tasks.csv"
       // def folder =  "tasks${File.separator}sample5"
        //def file = organizeTaskFile(folder)
        //IRandom iRandom = new IRandom(file, "output")
        //iRandom.generateAndSaveAlternativeIrandom()
        def resultFolder = "results${File.separator}sample5-irandom"
        organizeResultFiles(resultFolder)
    }

    static organizeTaskFile(String folder){
        def outputFile = "${folder}${File.separator}tasks.csv"
        List<String[]> content = []
        content += ["REPO_URL", "TASK_ID", "#HASHES", "HASHES", "#PROD_FILES", "#TEST_FILES", "LAST"] as String[]
        def files = Util.findFilesFromDirectory(folder).sort()
        files.each{ file ->
            println file
            def lines = CsvUtil.read(file)
            lines.remove(0)
            content += lines
        }
        CsvUtil.write(outputFile, content)
        outputFile
    }

    static organizeResultFiles(String resultFolder){
        AlternativeRandomInterfaceOrganizer organizer = new AlternativeRandomInterfaceOrganizer(resultFolder)
        organizer.summarize()
    }

}
