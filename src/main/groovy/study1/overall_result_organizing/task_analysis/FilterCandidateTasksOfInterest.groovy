package study1.overall_result_organizing.task_analysis

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

class FilterCandidateTasksOfInterest {

    String folder
    String interestFile
    List<String> files
    List results
    List<String[]> linesOfInterest

    FilterCandidateTasksOfInterest(String folder){
        this.folder = folder
        def files = Util.findFilesFromDirectory(folder)
        interestFile = files.find{ it.endsWith("-interest.csv") }
        this.files = files - interestFile
        initLinesOfInterest()
    }

    private initLinesOfInterest(){
        linesOfInterest = CsvUtil.read(interestFile)
        linesOfInterest.remove(0)
        println "tasks of interest: ${linesOfInterest.size()}"
    }

    def filter(){
        files.each{ file ->
            List<String[]> lines = CsvUtil.read(file)
            def header = lines.get(0)
            lines.remove(0)
            println "file: ${file}; lines: ${lines.size()}"
            List<String[]> output = []
            lines.each{ line ->
                def r = linesOfInterest.find{ it == line }
                if(r != null) output += line
            }
            println "output: ${output.size()}"
            List<String[]> content = []
            content += header
            content += output
            def outputFile = file - ".csv" + "-filtered.csv"
            CsvUtil.write(outputFile, content)
            println "file: ${outputFile}; lines: ${output.size()}"
        }
    }

    static void main(String[] args){
        def folder = "tasks\\temp\\cucumber_sample\\makrio\\tasks-filtered"
        def filter = new FilterCandidateTasksOfInterest(folder)
        filter.filter()
    }

}
