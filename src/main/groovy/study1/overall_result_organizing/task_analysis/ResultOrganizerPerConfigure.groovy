package study1.overall_result_organizing.task_analysis

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

class ResultOrganizerPerConfigure {

    String folder

    List<String> addedFiles
    List<String> addedControllerFiles
    List<String> addedWhenFiles
    List<String> addedWhenControllerFiles
    List<String> allFiles
    List<String> allControllerFiles
    List<String> allWhenFiles
    List<String> allWhenControllerFiles
    List<String> allDetailedFiles

    String outputAddedFile
    String outputAddedControllerFile
    String outputAddedWhenFile
    String outputAddedWhenControllerFile
    String outputAllFile
    String outputAllControllerFile
    String outputAllWhenFile
    String outputAllWhenControllerFile
    String outputAllDetailedFile

    ResultOrganizerPerConfigure(String folder){
        this.folder = folder

        outputAddedFile = "$folder${File.separator}selected_added.csv"
        outputAddedControllerFile = "$folder${File.separator}selected_added_controller.csv"
        outputAddedWhenFile = "$folder${File.separator}selected_added_when.csv"
        outputAddedWhenControllerFile = "$folder${File.separator}selected_added_when_controller.csv"
        outputAllFile = "$folder${File.separator}selected_all.csv"
        outputAllControllerFile = "$folder${File.separator}selected_all_controller.csv"
        outputAllWhenFile = "$folder${File.separator}selected_all_when.csv"
        outputAllWhenControllerFile = "$folder${File.separator}selected_all_when_controller.csv"
        outputAllDetailedFile = "$folder${File.separator}selected_all_detailed.csv"
        findRelevantFiles()
        findRelevantControllerFiles()
    }

    def summarize(){
        def content = summarizeData(addedFiles)
        CsvUtil.write(outputAddedFile, content)

        content = summarizeData(addedControllerFiles)
        CsvUtil.write(outputAddedControllerFile, content)

        content = summarizeData(addedWhenFiles)
        CsvUtil.write(outputAddedWhenFile, content)

        content = summarizeData(addedWhenControllerFiles)
        CsvUtil.write(outputAddedWhenControllerFile, content)

        content = summarizeData(allFiles)
        CsvUtil.write(outputAllFile, content)

        content = summarizeData(allControllerFiles)
        CsvUtil.write(outputAllControllerFile, content)

        content = summarizeData(allWhenFiles)
        CsvUtil.write(outputAllWhenFile, content)

        content = summarizeData(allWhenControllerFiles)
        CsvUtil.write(outputAllWhenControllerFile, content)

        summarizeDetailedData()
    }

    private static extractProjectName(String name){
        def i = name.indexOf("-relevant")
        def j = name.lastIndexOf(File.separator)
        name.substring(j+1, i)
    }

    private static countLostVisitCalls(String calls){
        def values = calls.substring(1, calls.size()-1)
        values = values.tokenize('{')*.trim().collect{ "{" + it.replace("},", "}") }
        def result = []
        values.each{
            def index1 = it.indexOf(",")
            def init1 = "{path="
            def init2 = ", line="
            def index2 = it.indexOf(init2)
            result += [path: it.substring(init1.size(), index1), line:it.substring(index2+init2.size(), it.size()-1) as double]
        }
        result = result.unique()
        result
    }

    private static summarizeData(resultFiles){
        /*33
        "TASK","#DAYS","#DEVS","#COMMITS","HASHES","#GHERKIN_TESTS","#STEP_DEFS","#ITest","#IReal","ITest","IReal",
        "Precision","Recall","RAILS","#visit_call","lost_visit_call","#ITest_views","#view_analysis_code",
        "view_analysis_code","methods_no_origin","renamed_files","deleted_files","noFound_views","#noFound_views",
        "TIMESTAMP","has_merge","#FP","#FN","FP","FN","#Hits","Hits","f2"
        */
        int lost_visit = 15
        String[] firstLine = ["project", "task", "days", "devs", "commits", "hashes", "tests", "stepdef", "itest_size",
                              "ireal_size", "itest", "ireal", "precision", "recall", "rails", "visit_number",
                              "lost_visit", "itest_views_number", "files_view_analysis_number", "files_view_analysis",
                              "methods_no_origin", "renamed_files", "deleted_files", "noFound_views",
                              "noFound_views_number", "timestamp", "has_merge", "fp_size", "fn_size", "fp", "fn",
                              "hits_size", "hits", "f2", "lost_visit_number"] as String[]
        List<String[]> content = []
        List<String[]> data = []
        content +=  firstLine
        resultFiles.each{ file ->
            String[] values = []
            values += extractProjectName(file)
            def lines = CsvUtil.read(file)
            if(lines.size()>13) {
                lines = lines.subList(13, lines.size())
                def temp = lines.collect {
                    def lostCalls = countLostVisitCalls(it[lost_visit])
                    (values + it + [lostCalls.size()]) as String[]
                }
                data += temp.sort { it[1] as double }
            }
        }

        def precision = generateStatistics(data.collect{ it[12] as double } as double[], "precision")
        def recall = generateStatistics(data.collect{ it[13] as double } as double[], "recall")
        def fp = generateStatistics(data.collect{ it[27] as double } as double[], "#fp")
        def fn = generateStatistics(data.collect{ it[28] as double } as double[], "#fn")
        def f2 = generateStatistics(data.collect{ it[33] as double } as double[], "f2")
        content +=  data.unique{ [it[0], it[1]] }
        content +=  precision
        content +=  recall
        content +=  fp
        content +=  fn
        content += f2
        content
    }

    private summarizeDetailedData(){
        /* 41
        "Task","Date","#Days","#Commits","Commit_Message","#Devs","#Gherkin_Tests","#Impl_Gherkin_Tests","#StepDef",
        "#Impl_StepDef","Methods_Unknown_Type","#Step_Call","Step_Match_Errors","#Step_Match_Error","AST_Errors",
        "#AST_Errors","Gherkin_AST_Errors","#Gherkin_AST_Errors","Steps_AST_Errors","#Steps_AST_Errors","Renamed_Files",
        "Deleted_Files","NotFound_Views","#Views","#ITest","#IReal","ITest","IReal","Precision","Recall","Hashes",
        "Timestamp","Rails","Gems","#Visit_Call","Lost_visit_call","#Views_ITest","#Code_View_Analysis",
        "Code_View_Analysis","Has_Merge","F2"
         */
        int lost_visit = 35

        String[] firstLine = ["project", "task", "date", "days", "commits", "commit_message", "devs", "gherkin_tests",
                              "impl_gherkin_tests", "stepdef", "impl_stepdef", "methods_no_origin", "step_call",
                              "step_match_errors", "step_match_error_number", "ast_errors", "ast_errors_number",
                              "gherkin_ast_errors", "gherkin_ast_errors_number", "steps_ast_errors", "steps_ast_errors_number",
                              "renamed_files", "deleted_Files", "noFound_views", "noFound_views_number", "itest_size",
                              "ireal_size", "itest", "ireal", "precision", "recall", "hashes", "timestamp", "rails",
                              "gems", "visit_call_number", "lost_visit_number", "views_itest_number",
                              "code_view_analysis_number", "code_view_analysis", "has_merge", "f2"] as String[]
        List<String[]> content = []
        List<String[]> data = []
        content +=  firstLine
        allDetailedFiles.each{ file ->
            String[] values = []
            values += extractProjectName(file)
            def lines = CsvUtil.read(file)
            if(lines.size()>2) {
                lines = lines.subList(2, lines.size())
                def temp = lines.collect {
                    def lostCalls = countLostVisitCalls(it[lost_visit])
                    (values + it + [lostCalls.size()]) as String[]
                }
                data += temp.sort { it[1] as double }
            }
        }
        content +=  data.unique{ [it[0], it[1]] }
        CsvUtil.write(outputAllDetailedFile, content)
    }

    private static generateStatistics(double[] values, String text){
        List<String[]> content = []
        def mean = ["Mean "+text]
        def median = ["Median "+text]
        def sd = ["SD "+text]
        def statistics = new DescriptiveStatistics(values)
        mean.add(statistics.mean as String)
        median.add(statistics.getPercentile(50.0) as String)
        sd.add(statistics.standardDeviation as String)
        content += mean as String[]
        content += median as String[]
        content += sd as String[]
        content
    }

    private static findControllerFilesInFolder(List<String> files, String subfolder){
        files.findAll{
            !it.contains("${File.separator}evaluation${File.separator}") &&
                    it.contains("${File.separator}$subfolder${File.separator}") &&
                    it.contains("${File.separator}selected${File.separator}") &&
                    it.endsWith("-relevant-controller.csv")
        }
    }

    private findRelevantControllerFiles(){
        def aux = Util.findFilesFromDirectory(folder)
        addedControllerFiles = findControllerFilesInFolder(aux, "output_added")
        addedWhenControllerFiles = findControllerFilesInFolder(aux, "output_added_when")
        allControllerFiles = findControllerFilesInFolder(aux, "output_all")
        allWhenControllerFiles = findControllerFilesInFolder(aux, "output_all_when")
        println "added controller files: ${addedControllerFiles.size()}"
        println "added when controller files: ${addedWhenControllerFiles.size()}"
        println "all controller files: ${allControllerFiles.size()}"
        println "all when controller files: ${allWhenControllerFiles.size()}"
    }

    private static findDetailedFilesInFolder(List<String> files){
        files.findAll{
            !it.contains("${File.separator}evaluation${File.separator}") &&
                    it.contains("${File.separator}output_all${File.separator}") &&
                    it.contains("${File.separator}selected${File.separator}") &&
                    it.endsWith("-relevant-detailed.csv")
        }
    }

    private static findFilesInFolder(List<String> files, String subfolder){
        files.findAll{
            !it.contains("${File.separator}evaluation${File.separator}") &&
                    it.contains("${File.separator}$subfolder${File.separator}") &&
                    it.contains("${File.separator}selected${File.separator}") &&
                    it.endsWith("-relevant.csv")
        }
    }

    private findRelevantFiles(){
        def aux = Util.findFilesFromDirectory(folder)
        addedFiles = findFilesInFolder(aux, "output_added")
        addedWhenFiles = findFilesInFolder(aux, "output_added_when")
        allFiles = findFilesInFolder(aux, "output_all")
        allWhenFiles = findFilesInFolder(aux, "output_all_when")
        allDetailedFiles = findDetailedFilesInFolder(aux)
        println "added files: ${addedFiles.size()}"
        println "added when files: ${addedWhenFiles.size()}"
        println "all files: ${allFiles.size()}"
        println "all when files: ${allWhenFiles.size()}"
        println "all detailed files: ${allDetailedFiles.size()}"
    }

    private findAllInvalid(){
        List<String[]> header = null
        List<String[]> buffer = []
        def files = Util.findFilesFromDirectory(folder)
        def invalidFiles = files.findAll{ it.endsWith("-invalid.csv") }
        invalidFiles.each{ file ->
            println "invalid tasks file: ${file}"
            List<String[]> entries = CsvUtil.read(file)
            if(entries.size()>3){//analysis result
                if(header==null) header = entries.subList(0, 3)
                entries = entries.subList(3, entries.size())
                buffer += entries
            }
        }
        List<String[]> content = []
        content += header
        def aux = buffer.unique().sort{ it[0] as double }
        println "All invalid tasks: ${aux.size()}"
        content += aux
        if(aux.size()>0) CsvUtil.write("${folder}\\invalid.csv", content)
    }

    static void main(String[] args){
        def folder = "tasks\\temp\\cucumber_sample\\others\\result"
        ResultOrganizerPerConfigure evaluator1 = new ResultOrganizerPerConfigure(folder)
        evaluator1.summarize()
        evaluator1.findAllInvalid()
    }

}
