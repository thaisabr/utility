package study1.overall_result_organizing.task_analysis

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

/* Gera um único CSV de tarefas a partir de um conjunto de CSVs. */
class JoinCsv {

    boolean advanced
    List<String[]> buffer
    String outputFile
    List<String> files
    String mainFolder
    List<String[]> header

    static void main(String[] args){
        def mainFolder = "tasks\\temp\\cucumber_sample"

        def obj1 = new JoinCsv("${mainFolder}\\diaspora\\tasks\\candidates", "_candidates.csv")
        obj1.joinAllInFolder()

        def obj2 = new JoinCsv("${mainFolder}\\diaspora\\tasks\\original", "_original.csv")
        obj2.joinAllInFolder()
    }

    JoinCsv(String folder, String sufix){
        this.mainFolder = folder
        buffer = []
        files = Util.findFilesFromDirectory(folder)
        def auxName = files.get(0)
        def index = auxName.lastIndexOf("_")
        def name = auxName.contains(sufix) ? auxName.substring(0,index-2): auxName.substring(0,index)
        if (name.endsWith("_")) name = name.substring(0, name.size()-1)
        name = name + sufix
        this.outputFile = name
    }

    def joinAllInFolder(){
        updateBuffer()
        generateCsv()
    }

    private updateBuffer(){
        header = null
        files.each{ file ->
            println "Reading file: $file"
            updateBuffer(file)
        }
    }

    private updateBuffer(String filename){
        List<String[]> entries = CsvUtil.read(filename)
        if(entries[0][0] == "Repository" && entries.size()>13){//analysis result
            if(header==null) header = entries.subList(0, 13)
            header.subList(1, 13).each{
                it[1] = ""
            }
            entries = entries.subList(13, entries.size())
        } else {
            if(header==null) header = [entries.get(0)]
            entries.remove(0)
        }
        buffer += entries
    }

    private generateCsv(){
        List<String[]> content = []
        content += header
        content += buffer.sort{
            if(it[0].isInteger()) it[0] as double
            else it[1] as double
        }
        CsvUtil.write(outputFile, content)
        println "Output file: ${outputFile} (${content?.size()-1} entries)"
    }

}
