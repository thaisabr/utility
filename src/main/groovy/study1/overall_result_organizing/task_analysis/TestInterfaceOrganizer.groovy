package study1.overall_result_organizing.task_analysis

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

/*
* Resume os resultados dos múltiplos projetos.
*
* */
class TestInterfaceOrganizer {

    String folder

    List<String> addedFiles
    List<String> addedControllerFiles
    List<String> addedWhenFiles
    List<String> addedWhenControllerFiles
    List<String> allFiles
    List<String> allControllerFiles
    List<String> allWhenFiles
    List<String> allWhenControllerFiles

    String outputAddedFile
    String outputAddedControllerFile
    String outputAddedWhenFile
    String outputAddedWhenControllerFile
    String outputAllFile
    String outputAllControllerFile
    String outputAllWhenFile
    String outputAllWhenControllerFile

    static int LIMIT = 15

    static void main(String[] args){
        def projectsFolder = "tasks${File.separator}temp${File.separator}cucumber_sample"
        TestInterfaceOrganizer organizer = new TestInterfaceOrganizer(projectsFolder)
        organizer.summarize()
    }

    TestInterfaceOrganizer(String folder){
        this.folder = folder

        outputAddedFile = "$folder${File.separator}itest_added.csv"
        outputAddedControllerFile = "$folder${File.separator}itest_added_controller.csv"
        outputAddedWhenFile = "$folder${File.separator}itest_added_when.csv"
        outputAddedWhenControllerFile = "$folder${File.separator}itest_added_when_controller.csv"
        outputAllFile = "$folder${File.separator}itest_all.csv"
        outputAllControllerFile = "$folder${File.separator}itest_all_controller.csv"
        outputAllWhenFile = "$folder${File.separator}itest_all_when.csv"
        outputAllWhenControllerFile = "$folder${File.separator}itest_all_when_controller.csv"

        findItestFiles()
        findItestControllerFiles()
    }

    def summarize(){
        summarizeResults()
        summarizeControllerResults()
    }

    private summarizeControllerResults(){
        def content = summarizeData(addedControllerFiles)
        CsvUtil.write(outputAddedControllerFile, content)
        content = summarizeData(addedWhenControllerFiles)
        CsvUtil.write(outputAddedWhenControllerFile, content)
        content = summarizeData(allControllerFiles)
        CsvUtil.write(outputAllControllerFile, content)
        content = summarizeData(allWhenControllerFiles)
        CsvUtil.write(outputAllWhenControllerFile, content)
    }

    private summarizeResults(){
        def content = summarizeData(addedFiles)
        CsvUtil.write(outputAddedFile, content)
        content = summarizeData(addedWhenFiles)
        CsvUtil.write(outputAddedWhenFile, content)
        content = summarizeData(allFiles)
        CsvUtil.write(outputAllFile, content)
        content = summarizeData(allWhenFiles)
        CsvUtil.write(outputAllWhenFile, content)
    }

    private static summarizeData(resultFiles){
        /*
        "project","task","days","devs","commits","hashes","tests","stepdef","itest_size","ireal_size","itest","ireal",
        "precision","recall","rails","visit_number","lost_visit","itest_views_number","files_view_analysis_number",
        "files_view_analysis","methods_no_origin","renamed_files","deleted_files","noFound_views","noFound_views_number",
        "timestamp","has_merge","fp_size","fn_size","fp","fn","hits_size","hits","f2","lost_visit_number"
        */
        String[] firstLine = ["Project", "Task", "Precision", "Recall", "ITest_size", "IReal_size", "FP_size", "FN_size",
                              "Hits_size", "ITest", "IReal", "F2", "lost_visit_number", "tests_number"] as String[]
        List<String[]> content = []
        List<String[]> data = []
        content +=  firstLine
        resultFiles.each{ file ->
            def lines = CsvUtil.read(file)
            lines = lines.subList(1, lines.size()-LIMIT)
            def temp = lines.collect {
                [it[0], it[1], it[12], it[13], it[8], it[9], it[27], it[28], it[31], it[10], it[11], it[33], it[34], it[6]] as String[]
            }
            data += temp.sort{ it[0] }
        }

        def precision = generateStatistics(data.collect{ it[2] as double } as double[], "precision")
        def recall = generateStatistics(data.collect{ it[3] as double } as double[], "recall")
        def fp = generateStatistics(data.collect{ it[6] as double } as double[], "#fp")
        def fn = generateStatistics(data.collect{ it[7] as double } as double[], "#fn")
        def f2 = generateStatistics(data.collect{ it[11] as double } as double[], "f2")
        content +=  data
        content +=  precision
        content +=  recall
        content +=  fp
        content +=  fn
        content += f2
        content
    }

    private static generateStatistics(double[] values, String text){
        List<String[]> content = []
        def mean = ["Mean "+text]
        def median = ["Median "+text]
        def sd = ["SD "+text]
        def statistics = new DescriptiveStatistics(values)
        mean.add(statistics.mean as String)
        median.add(statistics.getPercentile(50.0) as String)
        sd.add(statistics.standardDeviation as String)
        content += mean as String[]
        content += median as String[]
        content += sd as String[]
        content
    }

    def findItestControllerFiles(){
        def files = Util.findFilesFromDirectory(folder)

        addedControllerFiles = files.findAll{ it.endsWith("${File.separator}selected_added_controller.csv") }
        println "itest added controller files: ${addedControllerFiles.size()}"
        addedControllerFiles.each{ println it }

        addedWhenControllerFiles = files.findAll{ it.endsWith("${File.separator}selected_added_when_controller.csv") }
        println "itest added when controller files: ${addedWhenControllerFiles.size()}"
        addedWhenControllerFiles.each{ println it }

        allControllerFiles = files.findAll{ it.endsWith("${File.separator}selected_all_controller.csv") }
        println "itest all controller files: ${allControllerFiles.size()}"
        allControllerFiles.each{ println it }

        allWhenControllerFiles = files.findAll{ it.endsWith("${File.separator}selected_all_when_controller.csv") }
        println "itest all when controller files: ${allWhenControllerFiles.size()}"
        allWhenControllerFiles.each{ println it }
    }

    def findItestFiles(){
        def files = Util.findFilesFromDirectory(folder)

        addedFiles = files.findAll{ it.endsWith("${File.separator}selected_added.csv") }
        println "itest added files: ${addedFiles.size()}"
        addedFiles.each{ println it }

        addedWhenFiles = files.findAll{ it.endsWith("${File.separator}selected_added_when.csv") }
        println "itest added when files: ${addedWhenFiles.size()}"
        addedWhenFiles.each{ println it }

        allFiles = files.findAll{ it.endsWith("${File.separator}selected_all.csv") }
        println "itest all files: ${allFiles.size()}"
        allFiles.each{ println it }

        allWhenFiles = files.findAll{ it.endsWith("${File.separator}selected_all_when.csv") }
        println "itest all when files: ${allWhenFiles.size()}"
        allWhenFiles.each{ println it }
    }

}
