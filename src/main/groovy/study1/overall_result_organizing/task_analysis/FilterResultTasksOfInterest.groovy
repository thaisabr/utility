package study1.overall_result_organizing.task_analysis

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

class FilterResultTasksOfInterest {

    String folder
    String interestFile
    List<String> resultFiles
    List results
    List ids

    FilterResultTasksOfInterest(String folder){
        this.folder = folder
        def files = Util.findFilesFromDirectory(folder)
        interestFile = files.find{ it.endsWith("-interest.csv") }
        resultFiles = files - interestFile
        initIds()
    }

    private initIds(){
        def lines = CsvUtil.read(interestFile)
        lines.remove(0)
        ids = lines.collect{ it[1] }
        println "tasks of interest: ${ids.size()}"
    }

    def filter(){
        resultFiles.each{ file ->
            def lines = CsvUtil.read(file)
            def header = lines.get(0)
            lines.remove(0)
            def selectedLines = lines.findAll{ it[1] in ids }
            List<String[]> content = []
            content += header
            content += selectedLines
            def outputFile = file - ".csv" + "-filtered.csv"
            CsvUtil.write(outputFile, content)
        }
    }

    def printOutputInfo(){
        def outputFiles = Util.findFilesFromDirectory(folder).findAll{ it.endsWith("-filtered.csv") }
        outputFiles.each{ file ->
            def lines = CsvUtil.read(file)
            lines.remove(0)
            println "File: ${file} (${lines.size()} entries)"
        }
    }

    static void main(String[] args){
        def folder = "tasks\\temp\\cucumber_sample\\cornell\\result-filtered"
        def filter = new FilterResultTasksOfInterest(folder)
        filter.filter()
        filter.printOutputInfo()
    }

}
