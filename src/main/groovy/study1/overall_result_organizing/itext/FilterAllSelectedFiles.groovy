package study1.overall_result_organizing.itext

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

import java.nio.file.FileAlreadyExistsException
import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path

class FilterAllSelectedFiles {

    String projectsFolder
    String textFilesFolder

    FilterAllSelectedFiles(String projectsFolder, String textFilesFolder){
        this.projectsFolder = projectsFolder
        this.textFilesFolder = textFilesFolder
    }

    private static copyTextFiles(List<String> files, String folder){
        files?.each{ file ->
            Path source = FileSystems.getDefault().getPath(file)
            Path target = FileSystems.getDefault().getPath(folder)
            try{
                Files.copy(source, target.resolve(source.getFileName()))
            } catch(FileAlreadyExistsException ex){
                println ex.message
            }
        }
    }

    private createMainTasksFolder(){
        File folderManager = new File(textFilesFolder)
        if(!folderManager.exists()) folderManager.mkdir()
    }

    private static createFolder(String folder){
        File folderManager = new File(folder)
        if(folderManager.exists()) Util.deleteFolder(folder)
        else folderManager.mkdir()
    }

    def filterResultFiles(){
        def files = Util.findFilesFromDirectory(projectsFolder)
        files = files.findAll {
            it.contains("${File.separator}result${File.separator}") &&
            it.endsWith("selected_all.csv") &&
            !it.contains("${File.separator}makrio${File.separator}")
        }.sort()

        def filesOfInterest = files.findAll{ file ->
            def lines = CsvUtil.read(file)
            lines.size()>1
        }

        filesOfInterest.sort()
    }

    def filterDetailedResultFiles(){
        def files = Util.findFilesFromDirectory(projectsFolder)
        files = files.findAll {
            it.contains("${File.separator}result${File.separator}") &&
            it.endsWith("selected_all_detailed.csv") &&
            !it.contains("${File.separator}makrio${File.separator}")
        }.sort()

        def filesOfInterest = files.findAll{ file ->
            def lines = CsvUtil.read(file)
            lines.size()>1
        }

        filesOfInterest.sort()
    }

    def organizeTextFilesPerProject(List<String> projects){
        createMainTasksFolder()
        projects.each{ project ->
            def textFolder = "${textFilesFolder}${File.separator}${project.toLowerCase()}"
            createFolder(textFolder)
            def files = Util.findFilesFromDirectory("${projectsFolder}${File.separator}${project}" +
                    "${File.separator}result")
            def textFiles = files.findAll{
                it.contains("${File.separator}output_all${File.separator}") &&
                it.contains("${File.separator}text${File.separator}") &&
                        it ==~ /.+(\\|\\/).+\d+\.txt/
            }
            println "Project: $project; Text files: ${textFiles.size()}"
            copyTextFiles(textFiles, textFolder)
        }
    }

    def organizeTextFilesPerProjectForMultiProjectResults(String folder){
        createMainTasksFolder()
        def mainFolder = "${projectsFolder}${File.separator}${folder}${File.separator}result${File.separator}output_all"
        def folderManager = new File(mainFolder)
        folderManager.eachDir { dir ->
            def name = dir.name
            def textFolder = "${textFilesFolder}${File.separator}${name.toLowerCase()}"
            createFolder(textFolder)
            def files = Util.findFilesFromDirectory("${dir}${File.separator}text")
            def textFiles = files.findAll{ it ==~ /.+(\\|\\/).+\d+\.txt/ }
            println "Project: $name; Text files: ${textFiles.size()}"
            copyTextFiles(textFiles, textFolder)
        }
    }

}
