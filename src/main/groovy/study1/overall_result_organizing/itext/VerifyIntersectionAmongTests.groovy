package study1.overall_result_organizing.itext

import br.ufpe.cin.tan.util.CsvUtil

class VerifyIntersectionAmongTests {

    static verifyIntersection(String file, List tasksOfInterest){
        def lines = CsvUtil.read(file)
        lines.remove(0)
        lines.remove(0)
        def tasks = lines.findAll{ (it[0] as int) in tasksOfInterest }
        def testsPerTask = tasks.collect{ task ->
            [id: task[0] as int, tests: task[5].split(";") as List]
        }

        def result = []
        def firstTask = tasksOfInterest.first()
        def testsSet = testsPerTask.find{ it.id == firstTask }?.tests
        def others = tasksOfInterest - firstTask
        others.each{ other ->
            def otherTests = testsPerTask.find{ it.id == other }?.tests
            def intersection = otherTests.intersect(testsSet).size()
            def rate = intersection/testsSet.size()*100
            result += [t1: firstTask, tests1: testsSet.size(), t2:other, tests2:otherTests.size(),
                       intersection:intersection, rate:rate]
        }
        result.each{ println it }
    }

    static void main(String[] args){
        def folder = "D:\\current\\study1-paper-sample\\sample_analysis\\"
        def file
        def tasksOfInterest

        println "Project Hackful"
        file = "${folder}others\\output_all\\8bitpal_hackful\\selected\\8bitpal_hackful-tests.csv"
        tasksOfInterest = [1, 6, 7, 3]
        verifyIntersection(file, tasksOfInterest)

        println "Project Pets"
        file = "${folder}pets\\output_all\\AgileVentures_MetPlus_PETS_3\\selected\\AgileVentures_MetPlus_PETS_3-tests.csv"
        tasksOfInterest = [422, 424]
        verifyIntersection(file, tasksOfInterest)

        println "Project Websiteone"
        file = "${folder}websiteone\\try1\\output_all\\AgileVentures_WebsiteOne_4\\selected\\AgileVentures_WebsiteOne_4-tests.csv"
        tasksOfInterest = [504, 493, 503, 518]
        verifyIntersection(file, tasksOfInterest)

        println "Project Whitehall"
        file = "${folder}whitehall\\try1\\output_all\\alphagov_whitehall_5\\selected\\alphagov_whitehall_5-tests.csv"
        tasksOfInterest = [2465, 2469, 2655, 2633]
        verifyIntersection(file, tasksOfInterest)

        println "Project Cornell"
        file = "${folder}cornell\\output_all\\cul-it_blacklight-cornell_5\\selected\\cul-it_blacklight-cornell_5-tests.csv"
        tasksOfInterest = [1454, 1459, 1474, 1703]
        verifyIntersection(file, tasksOfInterest)

        println "Project Diaspora"
        file = "${folder}diaspora\\try1\\output_all\\diaspora_diaspora_5\\selected\\diaspora_diaspora_5-tests.csv"
        tasksOfInterest = [1395, 1413, 1417, 1439]
        verifyIntersection(file, tasksOfInterest)

        println "Project CBA"
        file = "${folder}cba\\output_all\\iboard_CBA_1\\selected\\iboard_CBA_1-tests.csv"
        tasksOfInterest = [12, 140, 141, 152]
        verifyIntersection(file, tasksOfInterest)

        println "Project Folioapp"
        file = "${folder}folioapp\\output_all\\jpatel531_folioapp\\selected\\jpatel531_folioapp-tests.csv"
        tasksOfInterest = [41, 52, 53, 55]
        verifyIntersection(file, tasksOfInterest)

        println "Project Makrio"
        file = "${folder}makrio\\output_all\\makrio_makrio_3\\selected\\makrio_makrio_3-tests.csv"
        tasksOfInterest = [846, 857, 861, 962]
        verifyIntersection(file, tasksOfInterest)

        println "Project Oneclickorgs"
        file = "${folder}oneclickorgs\\output_all\\oneclickorgs_one-click-orgs_1\\selected\\oneclickorgs_one-click-orgs_1-tests.csv"
        tasksOfInterest = [26, 30, 34, 38]
        verifyIntersection(file, tasksOfInterest)

        println "Project Otwarchive"
        file = "${folder}otwarchive\\try1\\output_all\\otwcode_otwarchive_5\\selected\\otwcode_otwarchive_5-tests.csv"
        tasksOfInterest = [668, 675, 670, 678]
        verifyIntersection(file, tasksOfInterest)

        println "Project Rapidftr"
        file = "${folder}rapidftr\\output_all\\rapidftr_RapidFTR_2\\selected\\rapidftr_RapidFTR_2-tests.csv"
        tasksOfInterest = [1366, 1382, 1393, 1389]
        verifyIntersection(file, tasksOfInterest)

        println "Project Timestack"
        file = "${folder}stack\\output_all\\sameersharma25_time_stack\\selected\\sameersharma25_time_stack-tests.csv"
        tasksOfInterest = [92, 100, 97, 122]
        verifyIntersection(file, tasksOfInterest)

        println "Project Odin"
        file = "${folder}odin\\output_all\\TheOdinProject_theodinproject\\selected\\TheOdinProject_theodinproject-tests.csv"
        tasksOfInterest = [332, 337, 388, 394]
        verifyIntersection(file, tasksOfInterest)

        println "Project Tip4commit"
        file = "${folder}tip4commit\\output_all\\tip4commit_tip4commit\\selected\\tip4commit_tip4commit-tests.csv"
        tasksOfInterest = [51, 118, 119]
        verifyIntersection(file, tasksOfInterest)

        println "Project Tracks"
        file = "${folder}tracks\\output_all\\TracksApp_tracks\\selected\\TracksApp_tracks-tests.csv"
        tasksOfInterest = [272, 304, 314, 310]
        verifyIntersection(file, tasksOfInterest)
    }

}
