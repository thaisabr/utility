package study1.overall_result_organizing.itext

import groovy.util.logging.Slf4j
import study1.task_interfaces.text.IText

@Slf4j
class Main {

    static organizeFiles(FilterAllSelectedFiles filter){
        //organiza arquivos texto necessários para calcular similaridade textual entre tarefas
        def projects = ["cba", "cornell", "diaspora", "folioapp", "makrio", "odin", "oneclickorgs", "otwarchive",
                        "pets", "rapidftr", "stack", "tip4commit", "tracks", "websiteone", "whitehall"]
        filter.organizeTextFilesPerProject(projects)

        //caso especial de diretório de resultado com dados de múltiplos projetos
        def folderWithMultipleProjects = "others"
        filter.organizeTextFilesPerProjectForMultiProjectResults(folderWithMultipleProjects)
    }

    static void main(String[] args) {
        def projectsFolder = "tasks${File.separator}temp${File.separator}cucumber_sample"
        def textFilesFolder = "tasks${File.separator}temp${File.separator}tasks_text_by_project_463"
        def filter = new FilterAllSelectedFiles(projectsFolder, textFilesFolder)

        //organiza arquivos texto necessários para calcular similaridade textual entre tarefas
        //organizeFiles(filter)

        /* renomear as pastas em tasks\\temp\\tasks_text_by_project para corresponder ao nome dos projetos tal como
           aparece no arquivo de tarefas analisadas :
        8bitpal_hackful
        agileventures_metplus_pets
        agileventures_websiteone
        alphagov_whitehall
        cul-it_blacklight-cornell
        diaspora_diaspora
        efforg_action-center-platform
        iboard_cba
        jpatel531_folioapp
        makrio_makrio
        oneclickorgs_one-click-orgs
        opengovernment_opengovernment
        otwcode_otwarchive
        qiushibaike_moumentei
        rails3book_ticketee
        rapidftr_rapidftr
        sameersharma25_time_stack
        theodinproject_theodinproject
        tip4commit_tip4commit
        tracksapp_tracks */

        //pegar arquivos com resultado da análise (arquivos selected/relevant)
        def resultFiles = filter.filterResultFiles()
        log.info "Found selected files: ${resultFiles.size()}"
        resultFiles.each{ log.info it.toString() }

        //pegar arquivos com resultado detalhado da análise
        def detailedResultFiles = filter.filterDetailedResultFiles()
        log.info "Found detailed selected files: ${detailedResultFiles.size()}"
        detailedResultFiles.each{ log.info it.toString() }

        //calcular IText
        IText iText
        resultFiles.eachWithIndex{ resultFile, i ->
            log.info "result file: $resultFile"
            def detailedResultFile = detailedResultFiles.get(i)
            log.info "detailed result file: ${detailedResultFile}"
            if(iText) iText = new IText(resultFile, detailedResultFile, textFilesFolder, 3, iText.similarityResult,
                    iText.similarityOutput)
            else iText = new IText(resultFile, detailedResultFile, textFilesFolder, 3)
            iText.generate()
        }

        //resumir resultado
        //TextInterfaceOrganizer organizer = new TextInterfaceOrganizer(projectsFolder1)
        //organizer.summarize()
    }

}
