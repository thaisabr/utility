package study1.overall_result_organizing.project_search

import br.ufpe.cin.tan.util.CsvUtil

class ProjectAnalysis {

    static void main(String[] args){
        def allCucumber = CsvUtil.read("tasks\\project_to_analyze\\cucumber-projects-final.csv")
        def header = allCucumber.first()
        allCucumber.remove(0)
        println "allCucumber: ${allCucumber.size()}"

        def analyzed = CsvUtil.read("tasks\\project_to_analyze\\cucumber-projects-analyzed.csv")
        analyzed.remove(0)
        println "analyzed: ${analyzed.size()}"

        def analyzedProjects = analyzed.collect{ it[0] }
        def toAnalyze = allCucumber.findAll{ !(it[0] in analyzedProjects) }
        println "toAnalyze: ${toAnalyze.size()}"

        List<String[]> content = []
        content += header
        content += toAnalyze
        CsvUtil.write("tasks\\project_to_analyze\\forAnalysis.csv", content)

    }

}
