package study1.overall_result_organizing.project_search

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path

/* Dada a seleção de projetos reorganizada para simplificar a história contada no paper,
* extrair de resultados anteriores as planilhas de merges e tarefas, bem como o resultado a análise. */
class TaskOrganizer {

    static void main(String[] args){
        def mainFolder = "paper_sample_construction\\paper_sample\\analysis\\organized"
        createFolder(mainFolder)

        def projects = CsvUtil.read("paper_sample_construction\\paper_sample\\filtering\\cucumber-projects.csv")
        def header = projects.get(0)
        projects.remove(0)

        def cucumberProjects = projects.collect {
            (it[0] - "https://github.com/").replaceAll("/", "_") + "_merges.csv"
        }
        println "cucumberProjects: ${cucumberProjects.size()}"

        def mergesFiles = Util.findFilesFromDirectory("paper_sample_construction\\paper_sample\\analysis\\1-merges")
        def selectedMergesFiles = []
        def absentFiles = []
        cucumberProjects.each{ project ->
            def temp = mergesFiles.findAll{ it.endsWith(project) }
            if(temp == null || temp.empty) absentFiles += project
            else selectedMergesFiles += temp
        }
        println "selectedMergesFiles: ${selectedMergesFiles.size()}"
        selectedMergesFiles.each{ println it }

        List<String[]> absentProjects = []
        absentProjects += header
        absentFiles.each{ absentFile ->
            def name = "https://github.com/"+ ((absentFile - "_merges.csv").replaceAll("_", "/"))
            if(absentFile == "sameersharma25_time_stack_merges.csv"){
                name = "https://github.com/sameersharma25/time_stack"
            } else if(absentFile == "AgileVentures_MetPlus_PETS_merges.csv"){
                name = "https://github.com/AgileVentures/MetPlus_PETS"
            }
            absentProjects += projects.findAll{ it[0] == name }
        }
        println "absent projects: ${absentProjects.size()}"
        absentProjects.each{ println it }
        CsvUtil.write("${mainFolder}\\absent_projects.csv", absentProjects)

        println "absent files: ${absentFiles.size()}"
        absentFiles.each{ println it }

        def taskFiles = Util.findFilesFromDirectory("paper_sample_construction\\paper_sample\\analysis\\2-tasks")
        def selectedTaskFiles = []
        selectedMergesFiles.each{ String file ->
            def name = file
            def index = file.lastIndexOf(File.separator)
            if(index>-1) name = file.substring(index+1)
            name = name - "_merges.csv"
            selectedTaskFiles += taskFiles.findAll{
                if(it.contains(name)) it
                else null
            }
        }
        selectedTaskFiles = selectedTaskFiles.unique() - null
        println "selectedTaskFiles: ${selectedTaskFiles.size()}"
        selectedTaskFiles.each{ println it }

        def taskRenamedFiles = Util.findFilesFromDirectory("paper_sample_construction\\paper_sample\\analysis\\3-tasks-renamed")
        def selectedTaskRenamedFiles = []
        selectedTaskFiles.each{ String file ->
            def name = file
            def index = file.lastIndexOf(File.separator)
            if(index>-1) name = file.substring(index+1)
            name = name - "_cucumber.csv"
            selectedTaskRenamedFiles += taskRenamedFiles.findAll{
                if(it.contains(name)) it
                else null
            }
        }
        selectedTaskRenamedFiles = selectedTaskRenamedFiles.unique() - null
        println "selectedTaskRenamedFiles: ${selectedTaskRenamedFiles.size()}"
        selectedTaskRenamedFiles.each{ println it }

        copyFiles(selectedMergesFiles, "${mainFolder}\\merges")
        copyFiles(selectedTaskFiles, "${mainFolder}\\tasks")
        copyFiles(selectedTaskRenamedFiles, "${mainFolder}\\tasks-renamed")
    }

    static copyFiles(def files, def targetFolder){
        createFolder(targetFolder)

        files?.each{ file ->
            Path source = FileSystems.getDefault().getPath(file)
            Path target = FileSystems.getDefault().getPath(targetFolder)
            Files.copy(source, target.resolve(source.getFileName()))
        }
    }

    private static createFolder(String folder){
        File folderManager = new File(folder)
        if(folderManager.exists()) Util.deleteFolder(folder)
        else folderManager.mkdir()
    }

}
