package study1.overall_result_organizing.project_search

import br.ufpe.cin.tan.util.CsvUtil

class OrganizingProjectsByRound {

    static void main(String[] args){
        def folder = "paper_sample_construction\\paper_sample\\filtering\\search"
        def searchIndex = "1"
        def entryProjects = CsvUtil.read("${folder}${searchIndex}\\1-github\\projects.csv")
        entryProjects.remove(0)
        println "entryProjects: ${entryProjects.size()}"

        def rails = CsvUtil.read("${folder}${searchIndex}\\2-filtered\\rails-projects.csv")
        def header = rails.get(0)
        rails.remove(0)
        println "rails: ${rails.size()}"

        def cucumber = CsvUtil.read("${folder}${searchIndex}\\2-filtered\\cucumber-projects.csv")
        cucumber.remove(0)
        println "cucumber: ${cucumber.size()}"

        def simplecov = CsvUtil.read("${folder}${searchIndex}\\2-filtered\\simplecov-projects.csv")
        simplecov.remove(0)
        println "simplecov: ${simplecov.size()}"

        def coveralls = CsvUtil.read("${folder}${searchIndex}\\2-filtered\\coveralls-projects.csv")
        coveralls.remove(0)
        println "coveralls: ${coveralls.size()}"

        def cucumberProjects = cucumber.collect{ it[0] }
        def simplecovAndCucumber = simplecov.findAll{ it[0] in cucumberProjects }
        println "simplecovAndCucumber: ${simplecovAndCucumber.size()}"
        List<String[]> content = []
        content += header
        content += simplecovAndCucumber
        CsvUtil.write("${folder}${searchIndex}\\2-filtered\\simplecov&cucumber-projects.csv", content)

        def coverallsAndCucumber = coveralls.findAll{ it[0] in cucumberProjects }
        println "coverallsAndCucumber: ${coverallsAndCucumber.size()}"
        content = []
        content += header
        content += coverallsAndCucumber
        CsvUtil.write("${folder}${searchIndex}\\2-filtered\\coveralls&cucumber-projects.csv", content)

        def coverageAndCucumber = (simplecovAndCucumber + coverallsAndCucumber).unique()
        println "coverageAndCucumber: ${coverageAndCucumber.size()}"
        content = []
        content += header
        content += coverageAndCucumber
        CsvUtil.write("${folder}${searchIndex}\\2-filtered\\coverage&cucumber-projects.csv", content)
    }
}
