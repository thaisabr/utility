package study1.overall_result_organizing.task_search

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path

/*
* Como estamos reorganizando os dados de um estudo passado, precisamos alinhar as planilhas de tarefa para que a data da tarefa esteja alinhada ao período em que o estudo foi concebido no passado. Esse código filtra tarefas com base na planilha de merges de onde ela foi extraída que, por sua vez, já está filtrada pela data limite, 30/09/17.
* */

class TaskManager {
    String mergeFolder
    String taskFolder
    List<String> mergeFiles
    List<String> taskFiles
    List fileGroups
    String outputFolder

    static void main(String[] args){
        String mergeFolder = "tasks\\temp\\merges-finalSeptember"
        String taskFolder = "tasks\\temp\\tasks-coverage"
        def taskManager = new TaskManager(mergeFolder, taskFolder)
        taskManager.filterTasksByDate()
        taskManager.renameOutput()
        taskManager.countTasksOriginalFolder()
        taskManager.countTasksFilteredFolder()

        //renameOutput("tasks\\temp\\tasks-filtered-forAnalysis")
    }

    TaskManager(String mergeFolder, String taskFolder){
        this.mergeFolder = mergeFolder
        this.taskFolder = taskFolder
        this.mergeFiles = Util.findFilesFromDirectory(mergeFolder)
        this.taskFiles = Util.findFilesFromDirectory(taskFolder)
        groupFiles()
        this.outputFolder = "tasks\\temp\\tasks-coverage-filtered"
    }

    private groupFiles(){
        fileGroups = []
        mergeFiles.each{ mergeFile ->
            def name = mergeFile
            def index = mergeFile.lastIndexOf(File.separator)
            if(index>-1) name = mergeFile.substring(index+1)
            name = name.replace("_merges", "_cucumber_coverage")
            def taskFile = taskFiles.find{ it.endsWith(name) }
            if(taskFile!=null){
                fileGroups += [mergeFile: mergeFile, taskFile: taskFile]
            }
        }
    }

    def filterTasksByDate(){
        fileGroups.each{ group ->
            println "mergeFile: ${group.mergeFile}"
            println "taskFile: ${group.taskFile}"

            def mergeLines = CsvUtil.read(group.mergeFile)
            mergeLines = mergeLines.subList(2, mergeLines.size())
            def leftMerges = mergeLines.collect{ it[1] }
            def rightMerges = mergeLines.collect{ it[2] }
            def lastMerges = (leftMerges + rightMerges).unique()

            def tasks = CsvUtil.read(group.taskFile)
            List<String> content = []
            def header = tasks.get(0)
            content += header
            tasks = tasks.subList(1, tasks.size())
            tasks.each{ task ->
                def lastHash = task.getAt(6)
                if(lastHash in lastMerges){
                    content += task
                }
            }
            if(content.size()>1){
                def outputFile = group.taskFile - ".csv" + "-filtered.csv"
                CsvUtil.write(outputFile, content)
                Path source = FileSystems.getDefault().getPath(outputFile)
                Path target = FileSystems.getDefault().getPath(outputFolder)
                Files.move(source, target.resolve(source.getFileName()))
            }

        }
    }

    def renameOutput(){
        def outputFiles = Util.findFilesFromDirectory(outputFolder)
        println "outputFiles: ${outputFiles.size()}"
        outputFiles.each{ file ->
            File obj = new File(file)
            def newname = file - "-filtered.csv" + ".csv"
            obj.renameTo(newname)
        }
    }

    static renameOutput(String folder){
        def outputFiles = Util.findFilesFromDirectory(folder)
        println "outputFiles: ${outputFiles.size()}"
        outputFiles.each{ file ->
            File obj = new File(file)
            def newname = file - "_cucumber"
            obj.renameTo(newname)
        }
    }

    def countTasksOriginalFolder(){
        println "ORIGINAL TASK FILES: ${taskFiles.size()}"
        taskFiles.each{ file ->
            def lines = CsvUtil.read(file)
            println "file: ${file}; number of tasks: ${lines.size()-1}"
        }
    }

    def countTasksFilteredFolder(){
        def outputFiles = Util.findFilesFromDirectory(outputFolder)
        println "FILTERED TASK FILES: ${outputFiles.size()}"
        outputFiles.each{ file ->
            println "file: ${file}"
            def lines = CsvUtil.read(file)
            println "number of tasks: ${lines.size()-1}"
        }
    }
}
