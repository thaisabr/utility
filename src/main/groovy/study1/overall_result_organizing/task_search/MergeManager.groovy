package study1.overall_result_organizing.task_search

import br.ufpe.cin.tan.commit.GitRepository
import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util
import org.eclipse.jgit.revwalk.RevCommit

import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path

/*
* Como estamos reorganizando os dados de um estudo passado, precisamos alinhar as planilhas de merge, de onde
* são extraídas as tarefas, para que a data da tarefa esteja alinhada ao período em que o estudo foi concebido no passado. Esse código filtra os csv de merges para admitir apenas merges até 06/09/2017, que foi a data em que a busca de projetos foi feita. Depois me toquei que o correto seria considerar até 30/09/17, pois a busca de tarefas foi posterior à seleção dos projetos. A primeira data diz respeito à seleção dos projetos.
* */

class MergeManager {

    String folder
    List<String> files
    Date limitDate
    String outputFolder

    static void main(String[] args){
        Date limitDate = new Date(2017-1900, 9-1, 30)
        println "limitDate: $limitDate"

        def mergeManager = new MergeManager("tasks\\temp\\merges", limitDate)
        //mergeManager.filterMergesByDate()
        //mergeManager.renameOutput()
        mergeManager.countMergesOriginalFolder()
        mergeManager.countMergesFilteredFolder()
    }

    MergeManager(String folder, Date limitDate){
        this.folder = folder
        this.files = Util.findFilesFromDirectory(folder)
        this.limitDate = limitDate.clearTime()
        this.outputFolder = "tasks\\temp\\merges-finalSeptember"
    }

    def filterMergesByDate(){
        files.each{ file ->
            println "file: ${file}"
            List<String[]> content = []
            def lines = CsvUtil.read(file)
            def header = lines.subList(0,2)
            content += header
            def url = lines[0][0]
            println "url: $url"
            def gitRepository
            try{
                gitRepository = GitRepository.getRepository(url)
            } catch(Exception ex){
                println ex.message
                ex.printStackTrace()
                return
            }
            println "clonou!"
            lines = lines.subList(2,lines.size())
            lines.each{ line ->
                def hash = line.getAt(0)
                println "hash: $hash"
                Iterable<RevCommit> result = gitRepository.searchAllRevCommitsBySha(hash)
                def commit = result.getAt(0)
                def date = new Date((long)commit.commitTime * 1000).clearTime()
                if(date.before(limitDate) || date.equals(limitDate)){
                    content += line
                }
            }
            if(content.size()>2){
                def outputFile = file - ".csv" + "-date.csv"
                CsvUtil.write(outputFile, content)
                Path source = FileSystems.getDefault().getPath(outputFile)
                Path target = FileSystems.getDefault().getPath(outputFolder)
                Files.move(source, target.resolve(source.getFileName()))
            }
        }


    }

    def renameOutput(){
        def outputFiles = Util.findFilesFromDirectory(outputFolder)
        println "outputFiles: ${outputFiles.size()}"
        outputFiles.each{ file ->
            File obj = new File(file)
            def newname = file - "-date.csv" + ".csv"
            println "file: $file"
            println "new file name: ${newname}"
            obj.renameTo(newname)
        }
    }

    def countMergesOriginalFolder(){
        println "ORIGINAL MERGE FILES: ${files.size()}"
        files.each{ file ->
            def lines = CsvUtil.read(file)
            println "file: ${file}; number of merges: ${lines.size()-2}"
        }
    }

    def countMergesFilteredFolder(){
        def outputFiles = Util.findFilesFromDirectory(outputFolder)
        println "FILTERED MERGE FILES: ${outputFiles.size()}"
        outputFiles.each{ file ->
            println "file: ${file}"
            def lines = CsvUtil.read(file)
            println "number of merges: ${lines.size()-2}"
        }
    }

}
