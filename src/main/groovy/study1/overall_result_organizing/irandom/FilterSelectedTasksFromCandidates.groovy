package study1.overall_result_organizing.irandom

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

/*
* Esse script organiza o resultado na pasta tasks/temp/cucumber_sample para ser possível mandar calcular IRandom usando
* o script task_interfaces.random.Main.
*
* A organização consiste em fazer o match entre as tarefas analisadas e selecionadas, e os CSV de tarefas de entrada.
* O script para calcular IRandom precisa do csv de tarefas de entrada. Mas não queremos de todas as tarefas, apenas
* das que foram selecionadas após a análise.
*
* */
class FilterSelectedTasksFromCandidates {

    static configureOutputFileName(String inputTasksFile){
        def name = inputTasksFile
        def index = name.lastIndexOf(File.separator)
        if(index>-1) name = name.substring(index+1)
        name = name - "_candidates.csv"
        def outputFolder = "tasks${File.separator}temp${File.separator}tasks_selected"
        String inputSelectedTasksFile = "${outputFolder}${File.separator}${name}.csv"
        inputSelectedTasksFile
    }

    static extractTasksOfInterest(String inputTasksFile, String relevantTasksFile){
        println "relevantTasksFile: $relevantTasksFile"
        List<String[]> entries = CsvUtil.read(relevantTasksFile)
        def content = entries.subList(1,entries.size())
        def ids = content.collect{
            def project = it[0].toLowerCase()
            if(project ==~ /.+_\d+/ ){
                def index = project.lastIndexOf("_")
                project = project.substring(0, index)
            }
            [project: project, id: it[1]]
        }.sort()
        println "extracted ids:"
        ids.each{ println it }

        println "inputTasksFile: $inputTasksFile"
        List<String[]> tasks = CsvUtil.read(inputTasksFile)
        def tasksOfInterest = tasks.findAll{ t ->
            def aux = ids.find{
                t[0].toLowerCase().replaceAll(/(\\|\/)/,"_").contains(it.project) && it.id==t[1]
            }
            if(aux) t
        }.sort{ it[0] }
        List<String[]> lines = []
        lines += tasks.get(0)
        lines += tasksOfInterest//.subList(0,10)

        if(relevantTasksFile.contains("diaspora")){
            def foundIds = tasksOfInterest.collect{ it[1] }.unique()
            def relevantIds = ids.collect{ it.id }.unique()
            println "relevantIds: ${relevantIds.size()}"
            println "foundIds: ${foundIds.size()}"
            def notfoundids = relevantIds - foundIds
            println "notfoundids: ${notfoundids.size()}"
            notfoundids.each{ println it }
        }

        String inputSelectedTasksFile = configureOutputFileName(inputTasksFile)
        if(tasksOfInterest.empty){
            println "empty inputSelectedTasksFile: $inputSelectedTasksFile"
        } else{
            println "inputSelectedTasksFile: $inputSelectedTasksFile"
            CsvUtil.write(inputSelectedTasksFile, lines)
        }

    }

    static void main(String[] args){
        def mainFolder = "tasks\\temp\\cucumber_sample"
        def folders = Util.findFoldersFromDirectory(mainFolder)
        folders = folders.findAll{ it.count(File.separator) == 6 }
        println "number of folders: ${folders.size()}"
        folders.each{ folder ->
            println "folder: $folder"
            String relevantTasksFile = "${folder}${File.separator}result${File.separator}selected_all.csv"
            println "relevantTasksFile: $relevantTasksFile"
            File relevantFile = new File(relevantTasksFile)
            if(relevantFile.exists()){
                def taskFiles = Util.findFilesFromDirectory("${folder}${File.separator}tasks")
                def inputTasksFiles = taskFiles.findAll{ it.endsWith("_candidates.csv") &&
                        !it.contains("${File.separator}candidates${File.separator}")
                }
                println "inputTasksFiles: ${inputTasksFiles.size()}"
                if(inputTasksFiles.size()==1) {
                    def inputTasksFile = inputTasksFiles.first()
                    extractTasksOfInterest(inputTasksFile, relevantTasksFile)
                } else {
                    inputTasksFiles.each{ inputTasksFile ->
                        extractTasksOfInterest(inputTasksFile, relevantTasksFile)
                    }
                }
            }
        }
    }

}
