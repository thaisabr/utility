package study1.overall_result_organizing.task_interfaces

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

/*
* Entrada: Resultados de ITest, IRandom e IText, gerados a partir de subamostras de uma amostra em comum.
* Objetivo: Gerar um novo resultado para cada tipo de interface considerando apenas as tarefas em comum entre os resultados.
* */

class TaskInterfacesManager {

    String mainFolder
    String outputFolder

    List<String> entryFiles

    //project = index 0; task = index 1
    List<String[]> irandomTasks
    List<String[]> irandomControllerTasks
    List<String[]> itestAddedTasks
    List<String[]> itestAddedControllerTasks
    List<String[]> itestAddedWhenTasks
    List<String[]> itestAddedWhenControllerTasks
    List<String[]> itestAllTasks
    List<String[]> itestAllControllerTasks
    List<String[]> itestAllWhenTasks
    List<String[]> itestAllWhenControllerTasks
    List<String[]> itextTasks
    List<String[]> itextControllerTasks

    List commonTasks //[project, id]

    String[] irandomHeader
    String[] itestHeader
    String[] itextHeader

    String outputIrandomFile
    String outputIrandomControllerFile
    String outputItestAddedFile
    String outputItestAddedControllerFile
    String outputItestAddedWhenFile
    String outputItestAddedWhenControllerFile
    String outputItestAllFile
    String outputItestAllControllerFile
    String outputItestAllWhenFile
    String outputItestAllWhenControllerFile
    String outputResultFile
    String outputItextFile
    String outputItextControllerFile

    final int IRANDOM_PRECISION_INDEX = 2
    final int IRANDOM_RECALL_INDEX = 3
    final int IRANDOM_FP_INDEX = 6
    final int IRANDOM_FN_INDEX = 7
    final int IRANDOM_F2_INDEX = 9

    final int ITEXT_PRECISION_INDEX = 2
    final int ITEXT_RECALL_INDEX = 3
    final int ITEXT_FP_INDEX = 6
    final int ITEXT_FN_INDEX = 7
    final int ITEXT_F2_INDEX = 11

    final int ITEST_PRECISION_INDEX = 2
    final int ITEST_RECALL_INDEX = 3
    final int ITEST_FP_INDEX = 6
    final int ITEST_FN_INDEX = 7
    final int ITEST_F2_INDEX = 11

    List<String[]> selectedIrandomTasks
    List<String[]> selectedIrandomControllerTasks
    List<String[]> selectedItestAddedTasks
    List<String[]> selectedItestAddedControllerTasks
    List<String[]> selectedItestAddedWhenTasks
    List<String[]> selectedItestAddedWhenControllerTasks
    List<String[]> selectedItestAllTasks
    List<String[]> selectedItestAllControllerTasks
    List<String[]> selectedItestAllWhenTasks
    List<String[]> selectedItestAllWhenControllerTasks
    List<String[]> selectedItextTasks
    List<String[]> selectedItextControllerTasks

    TaskInterfacesManager(String entry){
        mainFolder = entry
        entryFiles = Util.findFilesFromDirectory(mainFolder)
        println "entryFiles: ${entryFiles.size()}"
        entryFiles.each{ println it }
        outputFolder = "${mainFolder}${File.separator}output"
        File f = new File(outputFolder)
        if(f.exists()){ f.deleteDir() }
        f.mkdir()
        configureOutputFiles()
        findIrandomTasks()
        findItestTasks()
        findItextTasks()
    }

    static void main(String[] args){
        def folder = "tasks\\temp\\cucumber_sample_task_interfaces"
        TaskInterfacesManager manager = new TaskInterfacesManager(folder)
        manager.findIntersectionAmongResults()
    }

    def findIntersectionAmongResults(){
        //as tarefas com irandom são todas auto-contidas (não há o problema de tarefas espalhadas em múltiplos csvs)
        commonTasks = irandomTasks?.sort{ it[0] }?.collect {
            if(it.size()>2) [project: it[0], id: it[1]]
        }
        commonTasks?.removeAll([null])

        println "commonTasks partial: ${commonTasks.size()}"
        commonTasks.each{ println it }

        //as tarefas com itext possuem tarefas passadas similares
        def textTasks = itextTasks?.sort{ it[0] }?.collect {
            if(it.size()>2) [project: it[0], id: it[1]]
        }
        textTasks?.removeAll([null])
        println "textTasks: ${textTasks.size()}"
        textTasks.each{ println it }

        //o conjunto final de tarefas consiste em tarefas auto-contidas e que possuem tarefas passadas similares
        def finalTasks = []
        commonTasks.each{ t ->
            def result = textTasks.find{ (it.project == t.project) && (it.id == t.id) }
            if(result) finalTasks += t
        }
        commonTasks = finalTasks.sort{ it.project }
        println "commonTasks: ${commonTasks.size()}"
        commonTasks.each{ println it }

        organizeIrandomResult()
        organizeItestResult()
        organizeItextResult()
        collapseResults()
    }

    private collectData(){
        def projectsColumn = commonTasks.collect { it.project }
        def tasksColumn = commonTasks.collect{ it.id }
        def precision_all = selectedItestAllTasks.collect{ it[ITEST_PRECISION_INDEX] } //1
        def recall_all = selectedItestAllTasks.collect{ it[ITEST_RECALL_INDEX] } //1
        def precision_added = selectedItestAddedTasks.collect{ it[ITEST_PRECISION_INDEX] } //2
        def recall_added = selectedItestAddedTasks.collect{ it[ITEST_RECALL_INDEX] } //2
        def precision_all_controller = selectedItestAllControllerTasks.collect{ it[ITEST_PRECISION_INDEX] } //3
        def recall_all_controller = selectedItestAllControllerTasks.collect{ it[ITEST_RECALL_INDEX] as Double } //3
        def precision_added_controller = selectedItestAddedControllerTasks.collect{ it[ITEST_PRECISION_INDEX] } //4
        def recall_added_controller = selectedItestAddedControllerTasks.collect{ it[ITEST_RECALL_INDEX]} //4
        def precision_all_when = selectedItestAllWhenTasks.collect{ it[ITEST_PRECISION_INDEX] } //5
        def recall_all_when = selectedItestAllWhenTasks.collect{ it[ITEST_RECALL_INDEX] } //5
        def precision_added_when = selectedItestAddedWhenTasks.collect{ it[ITEST_PRECISION_INDEX] }
        def recall_added_when = selectedItestAddedWhenTasks.collect{ it[ITEST_RECALL_INDEX] }
        def precision_all_when_controller = selectedItestAllWhenControllerTasks.collect{ it[ITEST_PRECISION_INDEX] }
        def recall_all_when_controller = selectedItestAllWhenControllerTasks.collect{ it[ITEST_RECALL_INDEX] }
        def precision_added_when_controller = selectedItestAddedWhenControllerTasks.collect{ it[ITEST_PRECISION_INDEX] }
        def recall_added_when_controller = selectedItestAddedWhenControllerTasks.collect{ it[ITEST_RECALL_INDEX] }

        def precision_random = selectedIrandomTasks?.collect{ it[IRANDOM_PRECISION_INDEX] }
        def recall_random = selectedIrandomTasks?.collect{ it[IRANDOM_RECALL_INDEX] }
        def precision_random_controller = selectedIrandomControllerTasks?.collect{ it[IRANDOM_PRECISION_INDEX] }
        def recall_random_controller = selectedIrandomControllerTasks?.collect{ it[IRANDOM_RECALL_INDEX] }

        def precision_text = selectedItextTasks?.collect{ it[ITEXT_PRECISION_INDEX] }
        def recall_text = selectedItextTasks?.collect{ it[ITEXT_RECALL_INDEX] }
        def precision_text_controller = selectedItextControllerTasks?.collect{ it[ITEXT_PRECISION_INDEX] }
        def recall_text_controller = selectedItextControllerTasks?.collect{ it[ITEXT_RECALL_INDEX] }

        def result
        if(precision_random== null || precision_random.empty || precision_text==null || precision_text.empty){
            result = GroovyCollections.transpose(projectsColumn, tasksColumn, precision_all, recall_all, precision_added,
                    recall_added, precision_all_controller, recall_all_controller, precision_added_controller,
                    recall_added_controller, precision_all_when, recall_all_when, precision_added_when, recall_added_when,
                    precision_all_when_controller, recall_all_when_controller, precision_added_when_controller,
                    recall_added_when_controller)
        }
        else { //caso de só existir resultado de itest
            result = GroovyCollections.transpose(projectsColumn, tasksColumn, precision_all, recall_all, precision_added,
                    recall_added, precision_all_controller, recall_all_controller, precision_added_controller,
                    recall_added_controller, precision_all_when, recall_all_when, precision_added_when, recall_added_when,
                    precision_all_when_controller, recall_all_when_controller, precision_added_when_controller,
                    recall_added_when_controller, precision_random, recall_random, precision_random_controller,
                    recall_random_controller, precision_text, recall_text, precision_text_controller, recall_text_controller)
        }

        List<String[]> content = []
        result?.each{
            content += it as String[]
        }
        content
    }

    private organizeIrandomResult(){
        if(irandomTasks==null || irandomTasks.empty) return
        selectedIrandomTasks = findIntersection(irandomTasks)
        CsvUtil.write(outputIrandomFile,
                [irandomHeader] + generateStatisticsForIrandom(selectedIrandomTasks) )

        selectedIrandomControllerTasks = findIntersection(irandomControllerTasks)
        CsvUtil.write(outputIrandomControllerFile,
                [irandomHeader] + generateStatisticsForIrandom(selectedIrandomControllerTasks) )
    }

    private organizeItestResult(){
        selectedItestAddedTasks = findIntersection(itestAddedTasks)
        CsvUtil.write(outputItestAddedFile,
                [itestHeader] + generateStatisticsForItest(selectedItestAddedTasks))

        selectedItestAddedControllerTasks = findIntersection(itestAddedControllerTasks)
        CsvUtil.write(outputItestAddedControllerFile,
                [itestHeader] + generateStatisticsForItest(selectedItestAddedControllerTasks))

        selectedItestAddedWhenTasks = findIntersection(itestAddedWhenTasks)
        CsvUtil.write(outputItestAddedWhenFile,
                [itestHeader] + generateStatisticsForItest(selectedItestAddedWhenTasks))

        selectedItestAddedWhenControllerTasks = findIntersection(itestAddedWhenControllerTasks)
        CsvUtil.write(outputItestAddedWhenControllerFile,
                [itestHeader] + generateStatisticsForItest(selectedItestAddedWhenControllerTasks))

        selectedItestAllTasks = findIntersection(itestAllTasks)
        CsvUtil.write(outputItestAllFile,
                [itestHeader] + generateStatisticsForItest(selectedItestAllTasks))

        selectedItestAllControllerTasks = findIntersection(itestAllControllerTasks)
        CsvUtil.write(outputItestAllControllerFile,
                [itestHeader] + generateStatisticsForItest(selectedItestAllControllerTasks))

        selectedItestAllWhenTasks = findIntersection(itestAllWhenTasks)
        CsvUtil.write(outputItestAllWhenFile,
                [itestHeader] + generateStatisticsForItest(selectedItestAllWhenTasks))

        selectedItestAllWhenControllerTasks = findIntersection(itestAllWhenControllerTasks)
        CsvUtil.write(outputItestAllWhenControllerFile,
                [itestHeader] + generateStatisticsForItest(selectedItestAllWhenControllerTasks))
    }

    private organizeItextResult(){
        if(itextTasks==null || itextTasks.empty) return
        selectedItextTasks = findIntersection(itextTasks)
        CsvUtil.write(outputItextFile,
                [itextHeader] + generateStatisticsForItext(selectedItextTasks) )

        selectedItextControllerTasks = findIntersection(itextControllerTasks)
        CsvUtil.write(outputItextControllerFile,
                [itextHeader] + generateStatisticsForItext(selectedItextControllerTasks) )
    }

    private configureOutputFiles(){
        outputIrandomFile = "${outputFolder}${File.separator}irandom.csv"
        outputIrandomControllerFile = "${outputFolder}${File.separator}irandom-controller.csv"
        outputItestAddedFile = "${outputFolder}${File.separator}itest_added.csv"
        outputItestAddedControllerFile = "${outputFolder}${File.separator}itest_added_controller.csv"
        outputItestAddedWhenFile = "${outputFolder}${File.separator}itest_added_when.csv"
        outputItestAddedWhenControllerFile = "${outputFolder}${File.separator}itest_added_when_controller.csv"
        outputItestAllFile = "${outputFolder}${File.separator}itest_all.csv"
        outputItestAllControllerFile = "${outputFolder}${File.separator}itest_all_controller.csv"
        outputItestAllWhenFile = "${outputFolder}${File.separator}itest_all_when.csv"
        outputItestAllWhenControllerFile = "${outputFolder}${File.separator}itest_all_when_controller.csv"
        outputResultFile = "${outputFolder}${File.separator}result.csv"
        outputItextFile = "${outputFolder}${File.separator}itext.csv"
        outputItextControllerFile = "${outputFolder}${File.separator}itext_controller.csv"
    }

    private findIrandomTasks(){
        def randomFile = entryFiles?.find{ it.endsWith("irandom.csv") }
        println "randomFile: $randomFile"
        irandomTasks = findTasks(randomFile)
        def randomControllerFile =entryFiles?.find{ it.endsWith("irandom-controller.csv") }
        println "randomControllerFile: $randomControllerFile"
        irandomControllerTasks = findTasks(randomControllerFile)
        irandomHeader = findHeader(randomFile)
    }

    private findItestTasks(){
        itestAddedTasks = findTasks(entryFiles?.find{ it.endsWith("itest_added.csv") })
        itestAddedControllerTasks = findTasks(entryFiles?.find{ it.endsWith("itest_added_controller.csv") })
        itestAddedWhenTasks = findTasks(entryFiles?.find{ it.endsWith("itest_added_when.csv") })
        itestAddedWhenControllerTasks = findTasks(entryFiles?.find{ it.endsWith("itest_added_when_controller.csv") })
        itestAllTasks = findTasks(entryFiles?.find{ it.endsWith("itest_all.csv") })
        itestAllControllerTasks = findTasks(entryFiles?.find{ it.endsWith("itest_all_controller.csv") })
        itestAllWhenTasks = findTasks(entryFiles?.find{ it.endsWith("itest_all_when.csv") })
        itestAllWhenControllerTasks = findTasks(entryFiles?.find{ it.endsWith("itest_all_when_controller.csv") })
        itestHeader = findHeader(entryFiles?.find{ it.endsWith("itest_added.csv") })
    }

    private findItextTasks(){
        def itextFile = entryFiles?.find{ it.endsWith("itext-noempty.csv") }
        itextTasks = findTasks(itextFile)
        itextControllerTasks = findTasks(entryFiles?.find{ it.endsWith("itext-controller-noempty.csv") })
        itextHeader = findHeader(itextFile)
    }

    private static findTasks(String file){
        if(file == null || file.empty) return []
        def tasks = CsvUtil.read(file)
        tasks.remove(0)
        tasks = tasks.subList(0, tasks.size()-15)
        tasks.each { t ->
            def project = t[0].toLowerCase()
            if(project ==~ /.+_\d+/ ){
                def index = project.lastIndexOf("_")
                project = project.substring(0, index)
            }
            t[0] = project
        }
        tasks
    }

    private static findHeader(String file){
        if(file == null || file.empty) return []
        def tasks = CsvUtil.read(file)
        tasks.get(0)
    }

    private findIntersection(List tasks){
        List<String[]> selected = []
        commonTasks?.each{ ct ->
            def found = tasks.find{ it[0]==ct.project && it[1]==ct.id }
            if(found) {
                selected += found
            }
        }
        selected
    }

    private generateStatisticsForIrandom(List<String[]> selected){
        List<String[]> content = []
        def precision = generateStatistics(selected.collect{ it[IRANDOM_PRECISION_INDEX] as double } as double[], "precision")
        def recall = generateStatistics(selected.collect{ it[IRANDOM_RECALL_INDEX] as double } as double[], "recall")
        def fp = generateStatistics(selected.collect{ it[IRANDOM_FP_INDEX] as double } as double[], "#fp")
        def fn = generateStatistics(selected.collect{ it[IRANDOM_FN_INDEX] as double } as double[], "#fn")
        def f2 = generateStatistics(selected.collect{ it[IRANDOM_F2_INDEX] as double } as double[], "f2")
        content += selected
        content += precision
        content += recall
        content += fp
        content += fn
        content += f2
        content
    }

    private generateStatisticsForItext(List<String[]> selected){
        List<String[]> content = []
        def precision = generateStatistics(selected.collect{ it[ITEXT_PRECISION_INDEX] as double } as double[], "precision")
        def recall = generateStatistics(selected.collect{ it[ITEXT_RECALL_INDEX] as double } as double[], "recall")
        def fp = generateStatistics(selected.collect{ it[ITEXT_FP_INDEX] as double } as double[], "#fp")
        def fn = generateStatistics(selected.collect{ it[ITEXT_FN_INDEX] as double } as double[], "#fn")
        def f2 = generateStatistics(selected.collect{ it[ITEXT_F2_INDEX] as double } as double[], "f2")
        content += selected
        content += precision
        content += recall
        content += fp
        content += fn
        content += f2
        content
    }

    private generateStatisticsForItest(List<String[]> selected){
        List<String[]> content = []
        def precision = generateStatistics(selected.collect{ it[ITEST_PRECISION_INDEX] as double } as double[], "precision")
        def recall = generateStatistics(selected.collect{ it[ITEST_RECALL_INDEX] as double } as double[], "recall")
        def fp = generateStatistics(selected.collect{ it[ITEST_FP_INDEX] as double } as double[], "#fp")
        def fn = generateStatistics(selected.collect{ it[ITEST_FN_INDEX] as double } as double[], "#fn")
        def f2 = generateStatistics(selected.collect{ it[ITEST_F2_INDEX] as double } as double[], "f2")
        content += selected
        content += precision
        content += recall
        content += fp
        content += fn
        content += f2
        content
    }

    private static generateStatistics(double[] values, String text){
        List<String[]> content = []
        def mean = ["Mean "+text]
        def median = ["Median "+text]
        def sd = ["SD "+text]
        def statistics = new DescriptiveStatistics(values)
        mean.add(statistics.mean as String)
        median.add(statistics.getPercentile(50.0) as String)
        sd.add(statistics.standardDeviation as String)
        content += mean as String[]
        content += median as String[]
        content += sd as String[]
        content
    }

    /*
        1  - changed
        2  - added
        3  - changed_controller
        4  - added_controller
        5  - changed_when
        6  - added_when
        7  - changed_when_controller
        8  - added_when_controller
        9  - random
        10 - random_controller
        11 - text
        12 - text_controller
     */
    private collapseResults(){
        List<String[]> content = []
        String[] header = ["Project","Task","P_1","R_1","P_2","R_2","P_3","R_3","P_4","R_4","P_5","R_5","P_6",
                                 "R_6","P_7","R_7","P_8","R_8","P_9","R_9","P_10","R_10","P_11","R_11","P_12","R_12"]
        content += header
        content += collectData()
        CsvUtil.write(outputResultFile, content)
    }

}
