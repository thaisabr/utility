package study1.result.correlation

import br.ufpe.cin.tan.util.CsvUtil
import groovy.util.logging.Slf4j
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import study1.result.configEvaluation.Evaluator
import study1.result.configEvaluation.InputFilesManager

/*
* Avalia o tamanho de ITest nas diferentes configurações.
* */

@Slf4j
class ITestSizeEvaluator {

    InputFilesManager result
    def projects
    def outputFolder

    ITestSizeEvaluator(InputFilesManager result){
        this.result = result
        projects = this.result.filesPerProject
        InputFilesManager.createFolder(Evaluator.INPUT_FOLDER)
        InputFilesManager.createFolder(Evaluator.OUTPUT_FOLDER)
        outputFolder = "${Evaluator.EVALUATION_FOLDER}${File.separator}correlation"
        InputFilesManager.createFolder(outputFolder)
    }

    def evaluate(){
        log.info "<< #ITest EVALUATION >>"

        projects?.each{ project ->
            log.info "Project: ${project.project}"
            evaluateProject(project)
        }
    }

    private evaluateProject(project){
        Evaluator evaluator = new Evaluator()
        def keys = project.keySet()
        keys -= ["project"]
        def results = []
        keys.each { k ->
            def values
            if(k < 9) values = evaluator.extractITestSize(project[k])
            else if (k==9 || k==10) values = evaluator.extractIRandomSize(project[k])
            double[] itestSize = values.collect { it[1] } as double[]
            def stats = new DescriptiveStatistics(itestSize)

            results.add([config: k, tasks: values.collect{it[0]}, values: values.collect{it[1]}, statistic: stats])

        }

        List<String[]> content = []
        content += ["Task" ,"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"] as String[]
        def tasks = results*.tasks.flatten().unique()
        def values = results*.values
        def statistic = results*.statistic

        tasks.eachWithIndex{ task, i ->
            def v = [task]
            (0..9).each{ j ->
                v.add(values[j][i])
            }
            content += v as String[]
        }

        def mean = ["Mean"]
        (0..9).each{ j ->
            mean.add(statistic[j].mean)
        }
        content += mean as String[]

        def median = ["Median"]
        (0..9).each{ j ->
            median.add(statistic[j].getPercentile(50.0))
        }
        content += median as String[]

        def sd = ["Standard deviation"]
        (0..9).each{ j ->
            sd.add(statistic[j].standardDeviation)
        }
        content += sd as String[]

        def outputFile = "${outputFolder}${File.separator}${project.project}_#itest.csv"
        CsvUtil.write(outputFile, content)
    }

}
