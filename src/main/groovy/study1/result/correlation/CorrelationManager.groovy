package study1.result.correlation

import br.ufpe.cin.tan.evaluation.TaskInterfaceEvaluator
import br.ufpe.cin.tan.util.CsvUtil
import study1.result.configEvaluation.CsvIndexes
import study1.result.configEvaluation.Evaluator
import study1.result.configEvaluation.InputFilesManager

class CorrelationManager {

    String outputFolder
    InputFilesManager configUtil
    CorrelationUtil correlationUtil
    def projectsConfig
    def projectsSimilarity

    def correlationJaccard
    def correlationCosine

    def correlationTestsPrecision
    def correlationTestsRecall

    def correlationITestPrecision
    def correlationITestRecall

    def correlationTestsITest

    CorrelationManager(InputFilesManager configUtil){
        InputFilesManager.createFolder(Evaluator.EVALUATION_FOLDER)
        outputFolder = "${Evaluator.EVALUATION_FOLDER}${File.separator}correlation"
        InputFilesManager.createFolder(outputFolder)
        this.configUtil = configUtil
        correlationUtil = new CorrelationUtil()
        projectsConfig = configUtil.filesPerProject
        projectsSimilarity = correlationUtil.filesPerProject
    }

    def evaluate(){
        computePerProjectBasedOnPrecisionAndRecall()
        computePerProjectBasedOnF2()
        computeCorrelationAmongITestAndResultsForProjects()
        computeCorrelationAmongITestAndResultsForProjectsBasedOnF2()
        computeCorrelationAmongITextAndIRealForProjects()
        computeCorrelationAmongITestAndIRealForProjects()
    }

    private computePerProjectBasedOnPrecisionAndRecall(){
        projectsConfig.each{ project ->
            def c1 = computeCorrelationAmongITestAndResults([project[1]])
            def c2 = computeCorrelationAmongITestAndResults([project[2]])
            def c3 = computeCorrelationAmongITestAndResults([project[3]])
            def c4 = computeCorrelationAmongITestAndResults([project[4]])
            def c5 = computeCorrelationAmongITestAndResults([project[5]])
            def c6 = computeCorrelationAmongITestAndResults([project[6]])
            def c7 = computeCorrelationAmongITestAndResults([project[7]])
            def c8 = computeCorrelationAmongITestAndResults([project[8]])

            List<String[]> content = []
            content += ["" ,"1", "2", "3", "4", "5", "6", "7", "8"] as String[]
            for(int i=0; i<5; i++){
                content += [c1[i][0], c1[i][1], c2[i][1], c3[i][1], c4[i][1], c5[i][1], c6[i][1],c7[i][1], c8[i][1]] as String[]
            }

            def filename = "${outputFolder}${File.separator}${project.project}-correlation-test.csv"
            CsvUtil.write(filename, content)

            def s1 = computeCorrelationAmongITextandIReal(projectsSimilarity['changed'])
            def s2 = computeCorrelationAmongITextandIReal(projectsSimilarity['added'])

            content = []
            content += ["", "(1) Changed", "(2) Added"] as String[]
            for(int i=0; i<2; i++){
                content += [s1[i][0], s1[i][1], s2[i][1]] as String[]
            }

            filename = "${outputFolder}${File.separator}${project.project}-correlation-text.csv"
            CsvUtil.write(filename, content)

            def s3 = computeCorrelationAmongITestAndIReal(projectsSimilarity['changed'])
            def s4 = computeCorrelationAmongITestAndIReal(projectsSimilarity['added'])
            def s5 = computeCorrelationAmongITestAndIReal(projectsSimilarity['changedWhen'])
            def s6 = computeCorrelationAmongITestAndIReal(projectsSimilarity['addedWhen'])

            content = []
            content += ["", "(1) Changed", "(2) Added", "(5) Changed when", "(6) Added when"] as String[]
            for(int i=0; i<2; i++){
                content += [s3[i][0], s3[i][1], s4[i][1], s5[i][1], s6[i][1]] as String[]
            }

            filename = "${outputFolder}${File.separator}${project.project}-correlation-test-real.csv"
            CsvUtil.write(filename, content)
        }
    }

    private computePerProjectBasedOnF2(){
        projectsConfig.each{ project ->
            def c1 = computeCorrelationAmongITestAndF2([project[1]])
            def c2 = computeCorrelationAmongITestAndF2([project[2]])
            def c3 = computeCorrelationAmongITestAndF2([project[3]])
            def c4 = computeCorrelationAmongITestAndF2([project[4]])
            def c5 = computeCorrelationAmongITestAndF2([project[5]])
            def c6 = computeCorrelationAmongITestAndF2([project[6]])
            def c7 = computeCorrelationAmongITestAndF2([project[7]])
            def c8 = computeCorrelationAmongITestAndF2([project[8]])

            List<String[]> content = []
            content += ["" ,"1", "2", "3", "4", "5", "6", "7", "8"] as String[]
            for(int i=0; i<3; i++){
                content += [c1[i][0], c1[i][1], c2[i][1], c3[i][1], c4[i][1], c5[i][1], c6[i][1],c7[i][1], c8[i][1]] as String[]
            }

            def filename = "${outputFolder}${File.separator}${project.project}-correlation-test-f2.csv"
            CsvUtil.write(filename, content)

            def s1 = computeCorrelationAmongITextandIReal(projectsSimilarity['changed'])
            def s2 = computeCorrelationAmongITextandIReal(projectsSimilarity['added'])

            content = []
            content += ["", "(1) Changed", "(2) Added"] as String[]
            for(int i=0; i<2; i++){
                content += [s1[i][0], s1[i][1], s2[i][1]] as String[]
            }

            filename = "${outputFolder}${File.separator}${project.project}-correlation-text.csv"
            CsvUtil.write(filename, content)

            def s3 = computeCorrelationAmongITestAndIReal(projectsSimilarity['changed'])
            def s4 = computeCorrelationAmongITestAndIReal(projectsSimilarity['added'])
            def s5 = computeCorrelationAmongITestAndIReal(projectsSimilarity['changedWhen'])
            def s6 = computeCorrelationAmongITestAndIReal(projectsSimilarity['addedWhen'])

            content = []
            content += ["", "(1) Changed", "(2) Added", "(5) Changed when", "(6) Added when"] as String[]
            for(int i=0; i<2; i++){
                content += [s3[i][0], s3[i][1], s4[i][1], s5[i][1], s6[i][1]] as String[]
            }

            filename = "${outputFolder}${File.separator}${project.project}-correlation-test-real.csv"
            CsvUtil.write(filename, content)
        }
    }

    private computeCorrelationAmongITestAndIReal(List<String> files){
        List<String[]> content = []
        def dataTestJaccard = []
        def dataTestCosine = []
        def dataRealJaccard = []
        def dataRealCosine = []

        files?.each { file ->
            List<String[]> entries = CsvUtil.read(file)
            if (entries.size() > CsvIndexes.SIMILARITY_HEADER_SIZE) {
                def data = entries.subList(CsvIndexes.SIMILARITY_HEADER_SIZE, entries.size())
                dataTestJaccard += data.collect { it[CsvIndexes.TEST_JACCARD_INDEX] as double }
                dataTestCosine += data.collect { it[CsvIndexes.TEST_COSINE_INDEX] as double }
                dataRealJaccard += data.collect { it[CsvIndexes.REAL_JACCARD_INDEX] as double }
                dataRealCosine += data.collect { it[CsvIndexes.REAL_COSINE_INDEX] as double }
            }
        }

        dataTestJaccard = dataTestJaccard.flatten()
        dataRealJaccard = dataRealJaccard.flatten()
        dataRealCosine = dataRealCosine.flatten()

        correlationJaccard = TaskInterfaceEvaluator.calculateCorrelation(dataTestJaccard as double[], dataRealJaccard as double[])
        correlationCosine = TaskInterfaceEvaluator.calculateCorrelation(dataTestCosine as double[], dataRealCosine as double[])
        content += ["Correlation Jaccard Test-Real", correlationJaccard.toString()] as String[]
        content += ["Correlation Cosine Test-Real", correlationCosine.toString()] as String[]
        content
    }

    private computeCorrelationAmongITestAndIRealForProjects(){
        def c1 = computeCorrelationAmongITestAndIReal(projectsSimilarity.collect{it['changed']})
        def c2 = computeCorrelationAmongITestAndIReal(projectsSimilarity.collect{it['added']})
        def c3 = computeCorrelationAmongITestAndIReal(projectsSimilarity.collect{it['changedWhen']})
        def c4 = computeCorrelationAmongITestAndIReal(projectsSimilarity.collect{it['addedWhen']})

        List<String[]> content = []
        content += ["", "(1) Changed", "(2) Added", "(5) Changed when", "(6) Added when"] as String[]
        for(int i=0; i<2; i++){
            content += [c1[i][0], c1[i][1], c2[i][1], c3[i][1], c4[i][1]] as String[]
        }

        def filename = "${outputFolder}${File.separator}projects-correlation-test-real.csv"
        CsvUtil.write(filename, content)
    }

    private computeCorrelationAmongITextAndIRealForProjects(){
        def s1 = computeCorrelationAmongITextandIReal(projectsSimilarity.collect{it['changed']})
        def s2 = computeCorrelationAmongITextandIReal(projectsSimilarity.collect{it['added']})

        List<String[]> content = []
        content += ["", "(1) Changed", "(2) Added"] as String[]
        for(int i=0; i<2; i++){
            content += [s1[i][0], s1[i][1], s2[i][1]] as String[]
        }

        def filename = "${outputFolder}${File.separator}projects-correlation-text.csv"
        CsvUtil.write(filename, content)
    }

    private computeCorrelationAmongITestAndResultsForProjects(){
        def c1 = computeCorrelationAmongITestAndResults(projectsConfig.collect{it[1]})
        def c2 = computeCorrelationAmongITestAndResults(projectsConfig.collect{it[2]})
        def c3 = computeCorrelationAmongITestAndResults(projectsConfig.collect{it[3]})
        def c4 = computeCorrelationAmongITestAndResults(projectsConfig.collect{it[4]})
        def c5 = computeCorrelationAmongITestAndResults(projectsConfig.collect{it[5]})
        def c6 = computeCorrelationAmongITestAndResults(projectsConfig.collect{it[6]})
        def c7 = computeCorrelationAmongITestAndResults(projectsConfig.collect{it[7]})
        def c8 = computeCorrelationAmongITestAndResults(projectsConfig.collect{it[8]})

        List<String[]> content = []
        content += ["", "(1) Changed", "(2) Added", "(3) Changed controller", "(4) Added controller",
                    "(5) Changed when", "(6) Added when", "(7) Changed when controller",
                    "(8) Added when controller"] as String[]
        for(int i=0; i<5; i++){
            content += [c1[i][0], c1[i][1], c2[i][1], c3[i][1], c4[i][1], c5[i][1], c6[i][1], c7[i][1], c8[i][1]] as String[]
        }

        def filename = "${outputFolder}${File.separator}projects-correlation-test.csv"
        CsvUtil.write(filename, content)
    }

    private computeCorrelationAmongITestAndResultsForProjectsBasedOnF2(){
        def c1 = computeCorrelationAmongITestAndF2(projectsConfig.collect{it[1]})
        def c2 = computeCorrelationAmongITestAndF2(projectsConfig.collect{it[2]})
        def c3 = computeCorrelationAmongITestAndF2(projectsConfig.collect{it[3]})
        def c4 = computeCorrelationAmongITestAndF2(projectsConfig.collect{it[4]})
        def c5 = computeCorrelationAmongITestAndF2(projectsConfig.collect{it[5]})
        def c6 = computeCorrelationAmongITestAndF2(projectsConfig.collect{it[6]})
        def c7 = computeCorrelationAmongITestAndF2(projectsConfig.collect{it[7]})
        def c8 = computeCorrelationAmongITestAndF2(projectsConfig.collect{it[8]})

        List<String[]> content = []
        content += ["" ,"1", "2", "3", "4", "5", "6", "7", "8"] as String[]
        for(int i=0; i<3; i++){
            content += [c1[i][0], c1[i][1], c2[i][1], c3[i][1], c4[i][1], c5[i][1], c6[i][1], c7[i][1], c8[i][1]] as String[]
        }

        def filename = "${outputFolder}${File.separator}projects-correlation-test-f2.csv"
        CsvUtil.write(filename, content)
    }

    private computeCorrelationAmongITextandIReal(List<String> files) {
        List<String[]> content = []
        def textSimilarity = []
        def dataRealJaccard = []
        def dataRealCosine = []

        files?.each { file ->
            List<String[]> entries = CsvUtil.read(file)
            if (entries.size() > CsvIndexes.SIMILARITY_HEADER_SIZE) {
                def data = entries.subList(CsvIndexes.SIMILARITY_HEADER_SIZE, entries.size())
                textSimilarity += data.collect { it[CsvIndexes.TEXT_SIMILARITY_INDEX] as double }
                dataRealJaccard += data.collect { it[CsvIndexes.REAL_JACCARD_INDEX] as double }
                dataRealCosine += data.collect { it[CsvIndexes.REAL_COSINE_INDEX] as double }
            }
        }

        textSimilarity = textSimilarity.flatten()
        dataRealJaccard = dataRealJaccard.flatten()
        dataRealCosine = dataRealCosine.flatten()

        correlationJaccard = TaskInterfaceEvaluator.calculateCorrelation(textSimilarity as double[], dataRealJaccard as double[])
        correlationCosine = TaskInterfaceEvaluator.calculateCorrelation(textSimilarity as double[], dataRealCosine as double[])
        content += ["Correlation Jaccard Text-Real", correlationJaccard.toString()] as String[]
        content += ["Correlation Cosine Text-Real", correlationCosine.toString()] as String[]
        content
    }

    private computeCorrelationAmongITestAndResults(List<String> files) {
        List<String[]> content = []
        def tests = []
        def precisionValues = []
        def recallValues = []
        def itestSize = []

        files?.each { file ->
            List<String[]> entries = CsvUtil.read(file)
            if (entries.size() > CsvIndexes.HEADER_SIZE) {
                def data = entries.subList(CsvIndexes.HEADER_SIZE, entries.size())
                tests += data.collect { it[CsvIndexes.GHERKIN_TESTS_N] as double }
                itestSize += data.collect { it[CsvIndexes.ITEST_SIZE] as double }
                precisionValues += data.collect { it[CsvIndexes.PRECISION] as double }
                recallValues += data.collect { it[CsvIndexes.RECALL] as double }
            }
        }

        tests = tests.flatten()
        itestSize = itestSize.flatten()
        precisionValues = precisionValues.flatten()
        recallValues = recallValues.flatten()

        correlationTestsPrecision = TaskInterfaceEvaluator.calculateCorrelation(tests as double[], precisionValues as double[])
        correlationTestsRecall = TaskInterfaceEvaluator.calculateCorrelation(tests as double[], recallValues as double[])
        content += ["Correlation #Tests-Precision", correlationTestsPrecision.toString()] as String[]
        content += ["Correlation #Tests-Recall", correlationTestsRecall.toString()] as String[]
        correlationITestPrecision = TaskInterfaceEvaluator.calculateCorrelation(itestSize as double[], precisionValues as double[])
        correlationITestRecall = TaskInterfaceEvaluator.calculateCorrelation(itestSize as double[], recallValues as double[])
        content += ["Correlation #ITest-Precision", correlationITestPrecision.toString()] as String[]
        content += ["Correlation #ITest-Recall", correlationITestRecall.toString()] as String[]
        correlationTestsITest = TaskInterfaceEvaluator.calculateCorrelation(tests as double[], itestSize as double[])
        content += ["Correlation #Tests-#ITest", correlationTestsITest.toString()] as String[]

        content
    }

    private computeCorrelationAmongITestAndF2(List<String> files) {
        List<String[]> content = []
        def tests = []
        def f2Values = []
        def itestSize = []

        files?.each { file ->
            List<String[]> entries = CsvUtil.read(file)
            if (entries.size() > CsvIndexes.HEADER_SIZE) {
                def data = entries.subList(CsvIndexes.HEADER_SIZE, entries.size())
                tests += data.collect { it[CsvIndexes.GHERKIN_TESTS_N] as double }
                itestSize += data.collect { it[CsvIndexes.ITEST_SIZE] as double }
                f2Values += data.collect { it[CsvIndexes.F2_MEASURE] as double }
            }
        }

        tests = tests.flatten()
        itestSize = itestSize.flatten()
        f2Values = f2Values.flatten()

        correlationTestsPrecision = TaskInterfaceEvaluator.calculateCorrelation(tests as double[], f2Values as double[])
        content += ["Correlation #Tests-F2", correlationTestsPrecision.toString()] as String[]
        correlationITestPrecision = TaskInterfaceEvaluator.calculateCorrelation(itestSize as double[], f2Values as double[])
        content += ["Correlation #ITest-F2", correlationITestPrecision.toString()] as String[]
        correlationTestsITest = TaskInterfaceEvaluator.calculateCorrelation(tests as double[], itestSize as double[])
        content += ["Correlation #Tests-#ITest", correlationTestsITest.toString()] as String[]
        content
    }
}
