package study1.result.correlation

import br.ufpe.cin.tan.util.CsvUtil
import groovy.util.logging.Slf4j
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import study1.result.configEvaluation.Evaluator
import study1.result.configEvaluation.InputFilesManager


@Slf4j
class AllTestQuantityEvaluator {

    InputFilesManager result
    def projects
    def outputFile
    List<String[]> content

    AllTestQuantityEvaluator(InputFilesManager result){
        this.result = result
        projects = this.result.filesPerProject
        InputFilesManager.createFolder(Evaluator.INPUT_FOLDER)
        InputFilesManager.createFolder(Evaluator.OUTPUT_FOLDER)
        def outputFolder = "${Evaluator.EVALUATION_FOLDER}${File.separator}overview"
        outputFile = "${outputFolder}${File.separator}tests_number.csv"
        InputFilesManager.createFolder(outputFolder)
        content = [] as String[]
    }

    def evaluate(){
        log.info "<< ALL #TESTS EVALUATION >>"
        content += ["Project", "Task", "GHERKIN_C", "GHERKIN_AD", "LOST", "STEPDEF_C", "STEPDEF_AD", "LOST"] as String[]
        projects?.each{ project ->
            log.info "Project: ${project.project}"
            evaluateProject(project)
        }
        computeStatistics()
        CsvUtil.write(outputFile, content)
    }

    private evaluateProject(project){
        Evaluator evaluator = new Evaluator(project[1], project[2], "")
        def lines = evaluator.extractTestsData()
        lines.each{ line ->
            content.add(([project.project] + line).flatten() as String[])
        }
    }

    private computeStatistics(){
        def lines = content.subList(1, content.size())
        double[] changedGherkin = lines.collect { it[2] as double } as double[]
        def stats1 = new DescriptiveStatistics(changedGherkin)
        double[] addedGherkin = lines.collect { it[3] as double } as double[]
        def stats2 = new DescriptiveStatistics(addedGherkin)
        double[] changedStepdef = lines.collect { it[5] as double } as double[]
        def stats3 = new DescriptiveStatistics(changedStepdef)
        double[] addedStepDef = lines.collect { it[6] as double } as double[]
        def stats4 = new DescriptiveStatistics(addedStepDef)
        content += ["","Mean", stats1.mean, stats2.mean, Evaluator.inverseCompare(stats1.mean, stats2.mean),
                    stats3.mean, stats4.mean, Evaluator.inverseCompare(stats3.mean, stats4.mean)] as String[]
        content += ["","Median", stats1.getPercentile(50.0), stats2.getPercentile(50.0), "",
                    stats3.getPercentile(50.0), stats4.getPercentile(50.0)] as String[]
        content += ["","SD", stats1.standardDeviation, stats2.standardDeviation, "",
                    stats3.standardDeviation, stats4.standardDeviation] as String[]
    }

}
