package study1.result.correlation

import groovy.util.logging.Slf4j
import study1.result.configEvaluation.Evaluator
import study1.result.configEvaluation.InputFilesManager

/*
* Avalia a quantidade de testes da tarefa entre as configurações changed e added.
*
* */

@Slf4j
class TestQuantityEvaluator {

    InputFilesManager result
    def projects
    def outputFolder

    TestQuantityEvaluator(InputFilesManager result){
        this.result = result
        projects = this.result.filesPerProject
        InputFilesManager.createFolder(Evaluator.INPUT_FOLDER)
        InputFilesManager.createFolder(Evaluator.OUTPUT_FOLDER)
        outputFolder = "${Evaluator.EVALUATION_FOLDER}${File.separator}correlation"
        InputFilesManager.createFolder(outputFolder)
    }

    def evaluate(){
        log.info "<< #TESTS EVALUATION >>"

        projects?.each{ project ->
            log.info "Project: ${project.project}"
            evaluateProject(project)
        }
    }

    private evaluateProject(project){
        Evaluator evaluator = new Evaluator(project[1], project[2],
                "${outputFolder}${File.separator}${project.project}_#tests.csv")
       evaluator.compareTestInfo()
    }

}
