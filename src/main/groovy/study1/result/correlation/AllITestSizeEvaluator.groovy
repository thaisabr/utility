package study1.result.correlation

import br.ufpe.cin.tan.util.CsvUtil
import groovy.util.logging.Slf4j
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import study1.result.configEvaluation.Evaluator
import study1.result.configEvaluation.InputFilesManager


@Slf4j
class AllITestSizeEvaluator {

    InputFilesManager result
    def projects
    def outputFile
    List<String[]> content
    def results

    AllITestSizeEvaluator(InputFilesManager result){
        this.result = result
        projects = this.result.filesPerProject
        InputFilesManager.createFolder(Evaluator.INPUT_FOLDER)
        InputFilesManager.createFolder(Evaluator.OUTPUT_FOLDER)
        def outputFolder = "${Evaluator.EVALUATION_FOLDER}${File.separator}overview"
        outputFile = "${outputFolder}${File.separator}itest_size.csv"
        InputFilesManager.createFolder(outputFolder)
        content = [] as String[]
        results = []
    }

    def evaluate(){
        results = []
        log.info "<< ALL #ITest EVALUATION >>"
        content += ["Project", "Task" ,"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"] as String[]
        projects?.each{ project ->
            log.info "Project: ${project.project}"
            evaluateProject(project)
        }
        computeStatistics()
        CsvUtil.write(outputFile, content)
    }

    private evaluateProject(project){
        Evaluator evaluator = new Evaluator()
        def keys = project.keySet()
        keys -= ["project"]
        def results = []
        keys.each { k ->
            def values
            if(k < 9) values = evaluator.extractITestSize(project[k])
            else if (k==9 || k==10) values = evaluator.extractIRandomSize(project[k])
            results.add([config: k, tasks: values.collect{it[0]}, values: values.collect{it[1]}])

        }
        def tasks = results*.tasks.flatten().unique()
        def values = results*.values
        tasks.eachWithIndex{ task, i ->
            def v = [task]
            (0..9).each{ j ->
                v.add([values[j][i]])
            }
            content += ([project.project] + v.flatten()) as String[]
        }
    }

    private computeStatistics(){
        def measures = content.subList(1, content.size())
        def mean = ["", "Mean"]
        def median = ["", "Median"]
        def sd = ["", "Standard deviation"]

        (2..11).each{ i->
            double[] column = measures.collect { it[i] as double }
            def statistics = new DescriptiveStatistics(column)
            mean.add(statistics.mean as String)
            median.add(statistics.getPercentile(50.0) as String)
            sd.add(statistics.standardDeviation as String)
        }
        content += mean as String[]
        content += median as String[]
        content += sd as String[]
    }

}
