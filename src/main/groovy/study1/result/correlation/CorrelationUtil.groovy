package study1.result.correlation

import br.ufpe.cin.tan.util.ConstantData
import br.ufpe.cin.tan.util.Util
import study1.result.configEvaluation.Evaluator

class CorrelationUtil {

    def allFiles
    def addedFiles
    def allWhenFiles
    def addedWhenFiles

    def projects
    def filesPerProject

    private final String allFolder = "${Evaluator.INPUT_FOLDER}${File.separator}output_all"
    private final String allWhenFolder = "${Evaluator.INPUT_FOLDER}${File.separator}output_all_when"
    private final String addedFolder = "${Evaluator.INPUT_FOLDER}${File.separator}output_added"
    private final String addedWhenFolder = "${Evaluator.INPUT_FOLDER}${File.separator}output_added_when"

    CorrelationUtil() {
        def allFolderFiles = Util.findFilesFromDirectory(allFolder)
        allFiles = allFolderFiles.findAll { it.endsWith("-relevant" + ConstantData.SIMILARITY_FILE_SUFIX) }

        def allWhenFolderFiles = Util.findFilesFromDirectory(allWhenFolder)
        allWhenFiles = allWhenFolderFiles.findAll { it.endsWith("-relevant" + ConstantData.SIMILARITY_FILE_SUFIX)  }

        def addedFolderFiles = Util.findFilesFromDirectory(addedFolder)
        addedFiles = addedFolderFiles.findAll { it.endsWith("-relevant" + ConstantData.SIMILARITY_FILE_SUFIX)  }

        def addedWhenFolderFiles = Util.findFilesFromDirectory(addedWhenFolder)
        addedWhenFiles = addedWhenFolderFiles.findAll { it.endsWith("-relevant" + ConstantData.SIMILARITY_FILE_SUFIX)  }

        extractProjects()
        organizeFilesPerProject()
    }

    static createFolder(String folder) {
        File file = new File(folder)
        if (!file.exists()) {
            file.mkdirs()
        }
    }

    private extractProjects(){
        def folders = Util.findFoldersFromDirectory(allFolder)
        def aux = folders?.collect{
            def index = it.lastIndexOf(File.separator)
            it.substring(index+1)
        }
        aux?.removeAll { it == "text" }
        projects = aux
    }

    private organizeFilesPerProject(){
        def result = []
        projects?.each{ project ->
            def file1 = allFiles.find{ it.contains("${File.separator}${project}${File.separator}")}
            def file2 = addedFiles.find{ it.contains("${File.separator}${project}${File.separator}")}
            def file3 = allWhenFiles.find{ it.contains("${File.separator}${project}${File.separator}")}
            def file4 = addedWhenFiles.find{ it.contains("${File.separator}${project}${File.separator}")}
            result += [project:project, changed:file1, added:file2, changedWhen:file3, addedWhen:file4]
        }
        filesPerProject = result
    }

}
