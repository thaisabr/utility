package study1.result.configEvaluation

import br.ufpe.cin.tan.util.ConstantData
import br.ufpe.cin.tan.util.Util

class InputFilesManager {

    def allFiles //1
    def addedFiles //2
    def allControllerFiles //3
    def addedControllerFiles //4
    def allWhenFiles //5
    def addedWhenFiles //6
    def allWhenControllerFiles //7
    def addedWhenControllerFiles //8
    def allRandomFiles //9
    def allRandomControllerFiles //10
    def allTextFiles //11
    def addedTextFiles //12
    def allTextControllerFiles //13
    def addedTextControllerFiles //14
    def allTestFiles //15
    def addedTestFiles //16

    def projects
    def filesPerProject

    private String allFolder
    private String allWhenFolder
    private String addedFolder
    private String addedWhenFolder

    boolean special

    InputFilesManager(String folder){
        Evaluator.INPUT_FOLDER = folder
        Evaluator.EVALUATION_FOLDER = "${Evaluator.INPUT_FOLDER}${File.separator}evaluation"
        Evaluator.OUTPUT_FOLDER = "$Evaluator.EVALUATION_FOLDER${File.separator}overview"
        init()
    }

    //apagar depois porque só foi criado para teste rápido no main
    InputFilesManager(boolean special, String folder){
        this.special = special
        Evaluator.INPUT_FOLDER = folder
        allFolder = "${Evaluator.INPUT_FOLDER}"
        allWhenFolder = "${Evaluator.INPUT_FOLDER}"
        addedFolder = "${Evaluator.INPUT_FOLDER}"
        addedWhenFolder = "${Evaluator.INPUT_FOLDER}"
        configuration()
    }

    InputFilesManager() {
        init()
    }

    private init(){
        allFolder = "${Evaluator.INPUT_FOLDER}${File.separator}output_all"
        allWhenFolder = "${Evaluator.INPUT_FOLDER}${File.separator}output_all_when"
        addedFolder = "${Evaluator.INPUT_FOLDER}${File.separator}output_added"
        addedWhenFolder = "${Evaluator.INPUT_FOLDER}${File.separator}output_added_when"
        createFolder(Evaluator.OUTPUT_FOLDER)
        configuration()
    }

    private configuration(){
        def allFolderFiles
        if(special) allFolderFiles = Util.findFilesFromDirectory(allFolder)
        else allFolderFiles = Util.findFilesFromDirectory(allFolder).findAll{
            it.contains("${File.separator}${ConstantData.SELECTED_TASKS_BY_CONFIGS_FOLDER}${File.separator}")
        }
        allFiles = allFolderFiles.findAll { it.endsWith(ConstantData.RELEVANT_TASKS_FILE_SUFIX) }
        allControllerFiles = allFolderFiles.findAll { it.endsWith("-relevant" + ConstantData.CONTROLLER_FILE_SUFIX) }
        allTestFiles = allFolderFiles.findAll { it.endsWith(ConstantData.TEST_EXECUTION_FILE_SUFIX) }

        //antes usava sufixo '-relevant-random'
        allRandomFiles = allFolderFiles.findAll { it.endsWith(ConstantData.RANDOM_RESULTS_FILE_SUFIX) }
        allRandomControllerFiles = allFolderFiles.findAll { it.endsWith("-random" + ConstantData.CONTROLLER_FILE_SUFIX ) }

        def allWhenFolderFiles = Util.findFilesFromDirectory(allWhenFolder)
        allWhenFiles = allWhenFolderFiles.findAll { it.endsWith(ConstantData.RELEVANT_TASKS_FILE_SUFIX) }
        allWhenControllerFiles = allWhenFolderFiles.findAll { it.endsWith("-relevant" + ConstantData.CONTROLLER_FILE_SUFIX) }

        def addedFolderFiles = Util.findFilesFromDirectory(addedFolder)
        addedFiles = addedFolderFiles.findAll { it.endsWith(ConstantData.RELEVANT_TASKS_FILE_SUFIX) }
        addedControllerFiles = addedFolderFiles.findAll { it.endsWith("-relevant" + ConstantData.CONTROLLER_FILE_SUFIX) }
        addedTestFiles = addedFolderFiles.findAll { it.endsWith(ConstantData.TEST_EXECUTION_FILE_SUFIX) }

        def addedWhenFolderFiles = Util.findFilesFromDirectory(addedWhenFolder)
        addedWhenFiles = addedWhenFolderFiles.findAll { it.endsWith(ConstantData.RELEVANT_TASKS_FILE_SUFIX) }
        addedWhenControllerFiles = addedWhenFolderFiles.findAll { it.endsWith("-relevant" + ConstantData.CONTROLLER_FILE_SUFIX) }

        allTextFiles = allFolderFiles.findAll { it.endsWith("-text.csv") }
        addedTextFiles = addedFolderFiles.findAll { it.endsWith("-text.csv") }
        allTextControllerFiles = allFolderFiles.findAll { it.endsWith("-text" + ConstantData.CONTROLLER_FILE_SUFIX ) }
        addedTextControllerFiles = addedFolderFiles.findAll { it.endsWith("-text" + ConstantData.CONTROLLER_FILE_SUFIX) }

        extractProjects()
        organizeFilesPerProject()
    }

    static createFolder(String folder) {
        File file = new File(folder)
        if (!file.exists()) {
            file.mkdirs()
        }
    }

    private extractProjects(){
        def folders = Util.findFoldersFromDirectory(addedWhenFolder)
        def aux = folders?.collect{
            def index = it.lastIndexOf(File.separator)
            it.substring(index+1)
        }
        aux?.removeAll { it == "text" }
        aux?.removeAll { it == "selected" }
        projects = aux
    }

    private organizeFilesPerProject(){
        def result = []
        projects?.each{ project ->
            def file1 = allFiles.find{ it.contains("${File.separator}${project}${File.separator}")}
            def file2 = addedFiles.find{ it.contains("${File.separator}${project}${File.separator}")}
            def file3 = allControllerFiles.find{ it.contains("${File.separator}${project}${File.separator}")}
            def file4 = addedControllerFiles.find{ it.contains("${File.separator}${project}${File.separator}")}
            def file5 = allWhenFiles.find{ it.contains("${File.separator}${project}${File.separator}")}
            def file6 = addedWhenFiles.find{ it.contains("${File.separator}${project}${File.separator}")}
            def file7 = allWhenControllerFiles.find{ it.contains("${File.separator}${project}${File.separator}")}
            def file8 = addedWhenControllerFiles.find{ it.contains("${File.separator}${project}${File.separator}")}
            def file9 = allRandomFiles.find{ it.contains("${File.separator}${project}${File.separator}")}
            def file10 = allRandomControllerFiles.find{ it.contains("${File.separator}${project}${File.separator}")}
            def file11 = allTextFiles.find{ it.contains("${File.separator}${project}${File.separator}")}
            def file12 = addedTextFiles.find{ it.contains("${File.separator}${project}${File.separator}")}
            def file13 = allTextControllerFiles.find{ it.contains("${File.separator}${project}${File.separator}")}
            def file14 = addedTextControllerFiles.find{ it.contains("${File.separator}${project}${File.separator}")}
            def file15 = allTestFiles.find{ it.contains("${File.separator}${project}${File.separator}")}
            def file16 = addedTestFiles.find{ it.contains("${File.separator}${project}${File.separator}")}
            result += [project:project, 1:file1, 2:file2, 3:file3, 4:file4, 5:file5, 6:file6, 7:file7, 8:file8,
                       9:file9, 10:file10, 11:file11, 12:file12, 13:file13, 14:file14, 15:file15, 16:file16]
        }
        filesPerProject = result
    }

}
