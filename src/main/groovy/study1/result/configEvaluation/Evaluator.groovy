package study1.result.configEvaluation

import br.ufpe.cin.tan.similarity.test.TestSimilarityAnalyser
import br.ufpe.cin.tan.util.CsvUtil
import groovy.util.logging.Slf4j
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

@Slf4j
class Evaluator {

    public static final String PRECISION_TEXT = "precision" //"jaccard"
    public static final String RECALL_TEXT = "recall"//"cosine"
    public static String INPUT_FOLDER = "results"
    public static String EVALUATION_FOLDER = "${INPUT_FOLDER}${File.separator}evaluation"
    public static String OUTPUT_FOLDER = "$EVALUATION_FOLDER${File.separator}overview"
    String csv1
    String csv2
    String output

    Evaluator(){ }

    Evaluator(csv1, csv2, output){
        this.csv1 = csv1
        this.csv2 = csv2
        this.output = output
    }

    def compareViews(){
        def tasks1 = extractMeasuresAndViews(csv1)
        def tasks2 = extractMeasuresAndViews(csv2)

        if(tasks1.empty || tasks2.empty || tasks1.size()!=tasks2.size()) {
            log.warn "Error: It is not possible to compare data. Check the size of each entry file."
            return
        }

        def idsTask1 = tasks1.collect{it[CsvIndexes.TASK_ID]}
        def idsTask2 = tasks2.collect{it[CsvIndexes.TASK_ID]}
        if(idsTask1.intersect(idsTask2).size() != idsTask1.size()){
            log.warn "Error: It is not possible to compare data. Check the ids sequence in each entry file."
            return
        }

        List<String[]> content = []
        String[] text = ["Task", "Improved views", "Improved precision", "Improved recall", "Improved FP", "Improved FN"]
        content += text
        for(int i=0; i<tasks1.size(); i++){
            def t1 = tasks1.get(i)
            def t2 = tasks2.get(i)
            def views = inverseCompare(t1[1], t2[1])
            def precision = compare(t1[2], t2[2])
            def recall = compare(t1[3], t2[3])
            def fp = inverseCompare(t1[4], t2[4])
            def fn = inverseCompare(t1[5], t2[5])
            String[] line = [t1[0], views, precision, recall, fp, fn]
            content += line
        }

        double[] viewsCsv1 = tasks1.collect { it[1] } as double[]
        def viewsStats1 = new DescriptiveStatistics(viewsCsv1)
        double[] viewsCsv2 = tasks2.collect { it[1] } as double[]
        def viewsStats2 = new DescriptiveStatistics(viewsCsv2)
        double[] precisionCsv1 = tasks1.collect { it[2] } as double[]
        def precisionStats1 = new DescriptiveStatistics(precisionCsv1)
        double[] precisionCsv2 = tasks2.collect { it[2] } as double[]
        def precisionStats2 = new DescriptiveStatistics(precisionCsv2)
        double[] recallCsv1 = tasks1.collect { it[3] } as double[]
        def recallStats1 = new DescriptiveStatistics(recallCsv1)
        double[] recallCsv2 = tasks2.collect { it[3] } as double[]
        def recallStats2 = new DescriptiveStatistics(recallCsv2)
        double[] fpCsv1 = tasks1.collect { it[4] } as double[]
        def fpStats1 = new DescriptiveStatistics(fpCsv1)
        double[] fpCsv2 = tasks2.collect { it[4] } as double[]
        def fpStats2 = new DescriptiveStatistics(fpCsv2)
        double[] fnCsv1 = tasks1.collect { it[5] } as double[]
        def fnStats1 = new DescriptiveStatistics(fnCsv1)
        double[] fnCsv2 = tasks2.collect { it[5] } as double[]
        def fnStats2 = new DescriptiveStatistics(fnCsv2)

        def viewsMean = inverseCompare(viewsStats1.mean, viewsStats2.mean)
        def precisionMean = compare(precisionStats1.mean, precisionStats2.mean)
        def recallMean = compare(recallStats1.mean, recallStats2.mean)
        def fpMean = inverseCompare(fpStats1.mean, fpStats2.mean)
        def fnMean = inverseCompare(fnStats1.mean, fnStats2.mean)

        content += ["(old) Lost views mean", viewsStats1.mean] as String[]
        content += ["(old) Lost views median", viewsStats1.getPercentile(50.0)] as String[]
        content += ["(old) Lost views standard deviation", viewsStats1.standardDeviation] as String[]
        content += ["(new) Lost views mean", viewsStats2.mean] as String[]
        content += ["(new) Lost views median", viewsStats2.getPercentile(50.0)] as String[]
        content += ["(new) Lost views standard deviation", viewsStats2.standardDeviation] as String[]
        content += ["Improved mean of lost views", viewsMean] as String[]
        content += [""] as String[]
        content += ["(old) ${PRECISION_TEXT} mean", precisionStats1.mean] as String[]
        content += ["(old) ${PRECISION_TEXT} median", precisionStats1.getPercentile(50.0)] as String[]
        content += ["(old) ${PRECISION_TEXT} standard deviation", precisionStats1.standardDeviation] as String[]
        content += ["(new) ${PRECISION_TEXT} mean", precisionStats2.mean] as String[]
        content += ["(new) ${PRECISION_TEXT} median", precisionStats2.getPercentile(50.0)] as String[]
        content += ["(new) ${PRECISION_TEXT} standard deviation", precisionStats2.standardDeviation] as String[]
        content += ["Improved mean ${PRECISION_TEXT}", precisionMean] as String[]
        content += [""] as String[]
        content += ["(old) ${RECALL_TEXT} mean", recallStats1.mean] as String[]
        content += ["(old) ${RECALL_TEXT} median", recallStats1.getPercentile(50.0)] as String[]
        content += ["(old) ${RECALL_TEXT} standard deviation", recallStats1.standardDeviation] as String[]
        content += ["(new) ${RECALL_TEXT} mean", recallStats2.mean] as String[]
        content += ["(new) ${RECALL_TEXT} median", recallStats2.getPercentile(50.0)] as String[]
        content += ["(new) ${RECALL_TEXT} standard deviation", recallStats2.standardDeviation] as String[]
        content += ["Improved mean ${RECALL_TEXT}", recallMean] as String[]
        content += [""] as String[]
        content += ["(old) False positives mean", fpStats1.mean] as String[]
        content += ["(old) False positives median", fpStats1.getPercentile(50.0)] as String[]
        content += ["(old) False positives standard deviation", fpStats1.standardDeviation] as String[]
        content += ["(new) False positives mean", fpStats2.mean] as String[]
        content += ["(new) False positives median", fpStats2.getPercentile(50.0)] as String[]
        content += ["(new) False positives standard deviation", fpStats2.standardDeviation] as String[]
        content += ["Improved mean false positives", fpMean] as String[]
        content += [""] as String[]
        content += ["(old) False negatives mean", fnStats1.mean] as String[]
        content += ["(old) False negatives median", fnStats1.getPercentile(50.0)] as String[]
        content += ["(old) False negatives standard deviation", fnStats1.standardDeviation] as String[]
        content += ["(new) False negatives mean", fnStats2.mean] as String[]
        content += ["(new) False negatives median", fnStats2.getPercentile(50.0)] as String[]
        content += ["(new) False negatives standard deviation", fnStats2.standardDeviation] as String[]
        content += ["Improved mean false negatives", fnMean] as String[]

        CsvUtil.write(output, content)
        [oldViews:viewsCsv1, newViews:viewsCsv2, oldPrecision:precisionCsv1, oldRecall:recallCsv1,
         newPrecision:precisionCsv2, newRecall:recallCsv2, oldFn:fnCsv1, newFn:fnCsv2, oldFp:fpCsv1, newFp:fpCsv2]
    }

    def compareTestInfo(){
        def tasks1 = extractTestInfo(csv1)
        def tasks2 = extractTestInfo(csv2)

        if(tasks1.empty || tasks2.empty || tasks1.size()!=tasks2.size()) {
            log.warn "Error: It is not possible to compare data. Check the size of each entry file."
            return
        }

        def idsTask1 = tasks1.collect{it[CsvIndexes.TASK_ID]}
        def idsTask2 = tasks2.collect{it[CsvIndexes.TASK_ID]}
        if(idsTask1.intersect(idsTask2).size() != idsTask1.size()){
            log.warn "Error: It is not possible to compare data. Check the ids sequence in each entry file."
            return
        }

        List<String[]> content = []
        String[] text = ["Task", "#GHERKIN_C", "#GHERKIN_AD", "LOST", "#STEPDEF_C", "#STEPDEF_AD", "LOST"]
        content += text
        for(int i=0; i<tasks1.size(); i++){
            def t1 = tasks1.get(i)
            def t2 = tasks2.get(i)
            def gherkin = inverseCompare(t1[1], t2[1])
            def stepdef = inverseCompare(t1[2], t2[2])
            String[] line = [t1[0], t1[1], t2[1], gherkin, t1[2], t2[2], stepdef]
            content += line
        }

        double[] changedGherkin = tasks1.collect { it[1] } as double[]
        def stats1 = new DescriptiveStatistics(changedGherkin)
        double[] addedGherkin = tasks2.collect { it[1] } as double[]
        def stats2 = new DescriptiveStatistics(addedGherkin)
        double[] changedStepdef = tasks1.collect { it[2] } as double[]
        def stats3 = new DescriptiveStatistics(changedStepdef)
        double[] addedStepDef = tasks2.collect { it[2] } as double[]
        def stats4 = new DescriptiveStatistics(addedStepDef)
        content += ["Mean", stats1.mean, stats2.mean, inverseCompare(stats1.mean, stats2.mean),
                    stats3.mean, stats4.mean, inverseCompare(stats3.mean, stats4.mean)] as String[]
        content += ["Median", stats1.getPercentile(50.0), stats2.getPercentile(50.0), "",
                    stats3.getPercentile(50.0), stats4.getPercentile(50.0)] as String[]
        content += ["Standard deviation", stats1.standardDeviation, stats2.standardDeviation, "",
                    stats3.standardDeviation, stats4.standardDeviation] as String[]
        CsvUtil.write(output, content)
    }

    def extractTestsData(){
        def tasks1 = extractTestInfo(csv1)
        def tasks2 = extractTestInfo(csv2)

        if(tasks1.empty || tasks2.empty || tasks1.size()!=tasks2.size()) {
            log.warn "Error: It is not possible to compare data. Check the size of each entry file."
            return
        }

        def idsTask1 = tasks1.collect{it[CsvIndexes.TASK_ID]}
        def idsTask2 = tasks2.collect{it[CsvIndexes.TASK_ID]}
        if(idsTask1.intersect(idsTask2).size() != idsTask1.size()){
            log.warn "Error: It is not possible to compare data. Check the ids sequence in each entry file."
            return
        }

        List<String[]> content = []
        for(int i=0; i<tasks1.size(); i++){
            def t1 = tasks1.get(i)
            def t2 = tasks2.get(i)
            def gherkin = inverseCompare(t1[1], t2[1])
            def stepdef = inverseCompare(t1[2], t2[2])
            String[] line = [t1[0], t1[1], t2[1], gherkin, t1[2], t2[2], stepdef]
            content += line
        }
        content
    }

    def comparePrecisionAndRecall(){
        def tasksResult = []
        def tasks1 = extractPrecisionAndRecall(csv1)
        def tasks2 = extractPrecisionAndRecall(csv2)

        if(tasks1.empty || tasks2.empty || tasks1.size()!=tasks2.size()) {
            log.warn "Error: It is not possible to compare data. Check the size of each entry file."
            return
        }

        def idsTask1 = tasks1.collect{it[CsvIndexes.TASK_ID]}
        def idsTask2 = tasks2.collect{it[CsvIndexes.TASK_ID]}
        if(idsTask1.intersect(idsTask2).size() != idsTask1.size()){
            log.warn "Error: It is not possible to compare data. Check the ids sequence in each entry file."
            return
        }

        List<String[]> content = []
        String[] text = ["Task", "Improved ${PRECISION_TEXT}", "Improved ${RECALL_TEXT}"]
        content += text
        for(int i=0; i<tasks1.size(); i++){
            def t1 = tasks1.get(i)
            def t2 = tasks2.get(i)
            tasksResult += [id:t1[0], p1:t1[1], p2:t2[1], r1:t1[2], r2:t2[2]]
            def precision = compare(t1[1], t2[1])
            def recall = compare(t1[2], t2[2])
            String[] line = [t1[0], precision, recall]
            content += line
        }

        double[] precisionCsv1 = tasks1.collect { it[1] } as double[]
        def precisionStats1 = new DescriptiveStatistics(precisionCsv1)
        double[] precisionCsv2 = tasks2.collect { it[1] } as double[]
        def precisionStats2 = new DescriptiveStatistics(precisionCsv2)
        double[] recallCsv1 = tasks1.collect { it[2] } as double[]
        def recallStats1 = new DescriptiveStatistics(recallCsv1)
        double[] recallCsv2 = tasks2.collect { it[2] } as double[]
        def recallStats2 = new DescriptiveStatistics(recallCsv2)
        def precisionMean = compare(precisionStats1.mean, precisionStats2.mean)
        def recallMean = compare(recallStats1.mean, recallStats2.mean)

        content += ["(1) ${PRECISION_TEXT} mean", precisionStats1.mean] as String[]
        content += ["(1) ${PRECISION_TEXT} median", precisionStats1.getPercentile(50.0)] as String[]
        content += ["(1) ${PRECISION_TEXT} standard deviation", precisionStats1.standardDeviation] as String[]
        content += ["(2) ${PRECISION_TEXT} mean", precisionStats2.mean] as String[]
        content += ["(2) ${PRECISION_TEXT} median", precisionStats2.getPercentile(50.0)] as String[]
        content += ["(2) ${PRECISION_TEXT} standard deviation", precisionStats2.standardDeviation] as String[]
        content += ["Improved mean ${PRECISION_TEXT}", precisionMean] as String[]
        content += [""] as String[]
        content += ["(1) ${RECALL_TEXT} mean", recallStats1.mean] as String[]
        content += ["(1) ${RECALL_TEXT} median", recallStats1.getPercentile(50.0)] as String[]
        content += ["(1) ${RECALL_TEXT} standard deviation", recallStats1.standardDeviation] as String[]
        content += ["(2) ${RECALL_TEXT} mean", recallStats2.mean] as String[]
        content += ["(2) ${RECALL_TEXT} median", recallStats2.getPercentile(50.0)] as String[]
        content += ["(2) ${RECALL_TEXT} standard deviation", recallStats2.standardDeviation] as String[]
        content += ["Improved mean ${RECALL_TEXT}", recallMean] as String[]
        CsvUtil.write(output, content)

        [p:precisionMean, r:recallMean, result:tasksResult]
    }

    def compareRandomPrecisionAndRecall(){
        def tasksResult = []
        def tasks1 = extractPrecisionAndRecall(csv1)
        def tasks2 = extractRandomPrecisionAndRecall(csv2)

        if(tasks1.empty || tasks2.empty || tasks1.size()!=tasks2.size()) {
            log.warn "Error: It is not possible to compare data. Check the size of each entry file."
            return
        }

        def idsTask1 = tasks1.collect{it[CsvIndexes.TASK_ID]}
        def idsTask2 = tasks2.collect{it[CsvIndexes.TASK_ID]}
        if(idsTask1.intersect(idsTask2).size() != idsTask1.size()){
            log.warn "Error: It is not possible to compare data. Check the ids sequence in each entry file."
            return
        }

        List<String[]>  content = []
        String[] text = ["Task", "Improved ${PRECISION_TEXT}", "Improved ${RECALL_TEXT}"]
        content += text
        for(int i=0; i<tasks1.size(); i++){
            def t1 = tasks1.get(i)
            def t2 = tasks2.get(i)
            tasksResult += [id:t1[0], p1:t1[1], p2:t2[1], r1:t1[2], r2:t2[2]]
            def precision = compare(t1[1], t2[1])
            def recall = compare(t1[2], t2[2])
            String[] line = [t1[0], precision, recall]
            content += line
        }

        double[] precisionCsv1 = tasks1.collect { it[1] } as double[]
        def precisionStats1 = new DescriptiveStatistics(precisionCsv1)
        double[] precisionCsv2 = tasks2.collect { it[1] } as double[]
        def precisionStats2 = new DescriptiveStatistics(precisionCsv2)
        double[] recallCsv1 = tasks1.collect { it[2] } as double[]
        def recallStats1 = new DescriptiveStatistics(recallCsv1)
        double[] recallCsv2 = tasks2.collect { it[2] } as double[]
        def recallStats2 = new DescriptiveStatistics(recallCsv2)
        def precisionMean = compare(precisionStats1.mean, precisionStats2.mean)
        def recallMean = compare(recallStats1.mean, recallStats2.mean)

        content += ["(1) ${PRECISION_TEXT} mean", precisionStats1.mean] as String[]
        content += ["(1) ${PRECISION_TEXT} median", precisionStats1.getPercentile(50.0)] as String[]
        content += ["(1) ${PRECISION_TEXT} standard deviation", precisionStats1.standardDeviation] as String[]
        content += ["(2) ${PRECISION_TEXT} mean", precisionStats2.mean] as String[]
        content += ["(2) ${PRECISION_TEXT} median", precisionStats2.getPercentile(50.0)] as String[]
        content += ["(2) ${PRECISION_TEXT} standard deviation", precisionStats2.standardDeviation] as String[]
        content += ["Improved mean ${PRECISION_TEXT}", precisionMean] as String[]
        content += [""] as String[]
        content += ["(1) ${RECALL_TEXT} mean", recallStats1.mean] as String[]
        content += ["(1) ${RECALL_TEXT} median", recallStats1.getPercentile(50.0)] as String[]
        content += ["(1) ${RECALL_TEXT} standard deviation", recallStats1.standardDeviation] as String[]
        content += ["(2) ${RECALL_TEXT} mean", recallStats2.mean] as String[]
        content += ["(2) ${RECALL_TEXT} median", recallStats2.getPercentile(50.0)] as String[]
        content += ["(2) ${RECALL_TEXT} standard deviation", recallStats2.standardDeviation] as String[]
        content += ["Improved mean ${RECALL_TEXT}", recallMean] as String[]
        CsvUtil.write(output, content)

        [p:precisionMean, r:recallMean, result:tasksResult]
    }

    def compareF2(){
        def tasksResult = []
        def tasks1 = extractF2(csv1)
        def tasks2 = extractF2(csv2)

        if(tasks1.empty || tasks2.empty || tasks1.size()!=tasks2.size()) {
            log.warn "Error: It is not possible to compare data. Check the size of each entry file."
            return
        }

        def idsTask1 = tasks1.collect{it[CsvIndexes.TASK_ID]}
        def idsTask2 = tasks2.collect{it[CsvIndexes.TASK_ID]}
        if(idsTask1.intersect(idsTask2).size() != idsTask1.size()){
            log.warn "Error: It is not possible to compare data. Check the ids sequence in each entry file."
            return
        }

        List<String[]> content = []
        String[] text = ["Task", "Improved F2"]
        content += text
        for(int i=0; i<tasks1.size(); i++){
            def t1 = tasks1.get(i)
            def t2 = tasks2.get(i)
            tasksResult += [id:t1[0], f2_1:t1[1], f2_2:t2[1]]
            def f2 = compare(t1[1], t2[1])
            String[] line = [t1[0], f2]
            content += line
        }

        double[] f2Csv1 = tasks1.collect { it[1] } as double[]
        def f2Stats1 = new DescriptiveStatistics(f2Csv1)
        double[] f2Csv2 = tasks2.collect { it[1] } as double[]
        def f2Stats2 = new DescriptiveStatistics(f2Csv2)
        def f2Mean = compare(f2Stats1.mean, f2Stats2.mean)

        content += ["(1) F2 mean", f2Stats1.mean] as String[]
        content += ["(1) F2 median", f2Stats1.getPercentile(50.0)] as String[]
        content += ["(1) F2 sd", f2Stats1.standardDeviation] as String[]
        content += ["(2) F2 mean", f2Stats2.mean] as String[]
        content += ["(2) F2 median", f2Stats2.getPercentile(50.0)] as String[]
        content += ["(2) F2 sd", f2Stats2.standardDeviation] as String[]
        content += ["Improved mean ${PRECISION_TEXT}", f2Mean] as String[]
        CsvUtil.write(output, content)

        [f2:f2Mean, result:tasksResult]
    }

    def compareRandomF2(){
        def tasksResult = []
        def tasks1 = extractF2(csv1)
        def tasks2 = extractRandomF2(csv2)

        if(tasks1.empty || tasks2.empty || tasks1.size()!=tasks2.size()) {
            log.warn "Error: It is not possible to compare data. Check the size of each entry file."
            return
        }

        def idsTask1 = tasks1.collect{it[CsvIndexes.TASK_ID]}
        def idsTask2 = tasks2.collect{it[CsvIndexes.TASK_ID]}
        if(idsTask1.intersect(idsTask2).size() != idsTask1.size()){
            log.warn "Error: It is not possible to compare data. Check the ids sequence in each entry file."
            return
        }

        List<String[]> content = []
        String[] text = ["Task", "Improved F2"]
        content += text
        for(int i=0; i<tasks1.size(); i++){
            def t1 = tasks1.get(i)
            def t2 = tasks2.get(i)
            tasksResult += [id:t1[0], f2_1:t1[1], f2_2:t2[1]]
            def f2 = compare(t1[1], t2[1])
            String[] line = [t1[0], f2]
            content += line
        }

        double[] f2Csv1 = tasks1.collect { it[1] } as double[]
        def f2Stats1 = new DescriptiveStatistics(f2Csv1)
        double[] f2Csv2 = tasks2.collect { it[1] } as double[]
        def f2Stats2 = new DescriptiveStatistics(f2Csv2)
        def f2Mean = compare(f2Stats1.mean, f2Stats2.mean)

        content += ["(1) F2 mean", f2Stats1.mean] as String[]
        content += ["(1) F2 median", f2Stats1.getPercentile(50.0)] as String[]
        content += ["(1) F2 sd", f2Stats1.standardDeviation] as String[]
        content += ["(2) F2 mean", f2Stats2.mean] as String[]
        content += ["(2) F2 median", f2Stats2.getPercentile(50.0)] as String[]
        content += ["(2) F2 sd", f2Stats2.standardDeviation] as String[]
        content += ["Improved mean ${PRECISION_TEXT}", f2Mean] as String[]
        CsvUtil.write(output, content)

        [f2:f2Mean, result:tasksResult]
    }

    def extractF2(String filename){
        if(!filename || filename.empty) return []
        def lines = CsvUtil.read(filename)
        def result = []
        if(lines.size()==0 || lines.size()<=CsvIndexes.HEADER_SIZE) return result
        def relevantLines = lines?.subList(CsvIndexes.HEADER_SIZE,lines.size())?.sort{ it[CsvIndexes.TASK_ID] as int }
        relevantLines?.each{
            result.add([ it[CsvIndexes.TASK_ID], it[CsvIndexes.F2_MEASURE] as double])
        }
        result
    }

    def extractRandomF2(String filename){
        if(!filename || filename.empty) return []
        def lines = CsvUtil.read(filename)
        def result = []
        if(lines.size()==0 || lines.size()<=CsvIndexes.RANDOM_HEADER_SIZE) return result
        def relevantLines = lines?.subList(CsvIndexes.RANDOM_HEADER_SIZE,lines.size())?.sort{ it[CsvIndexes.TASK_ID] as int }
        relevantLines?.each{
            result.add([ it[CsvIndexes.TASK_ID], it[CsvIndexes.RANDOM_F2_MEASURE] as double])
        }
        result
    }

    def extractITestSize(String filename){
        if(!filename || filename.empty) return []
        def lines = CsvUtil.read(filename)
        def result = []
        if(lines.size()==0 || lines.size()<=CsvIndexes.HEADER_SIZE) return result
        def relevantLines = lines?.subList(CsvIndexes.HEADER_SIZE,lines.size())?.sort{ it[CsvIndexes.TASK_ID] as int }
        relevantLines?.each{
            result.add([ it[CsvIndexes.TASK_ID], it[CsvIndexes.ITEST_SIZE] as double ])
        }
        result
    }

    def extractIRandomSize(String filename){
        if(!filename || filename.empty) return []
        def lines = CsvUtil.read(filename)
        def result = []
        if(lines.size()==0 || lines.size()<=CsvIndexes.RANDOM_HEADER_SIZE) return result
        def relevantLines = lines?.subList(CsvIndexes.RANDOM_HEADER_SIZE,lines.size())?.sort{ it[CsvIndexes.TASK_ID] as int }
        relevantLines?.each{
            result.add([ it[CsvIndexes.TASK_ID], it[CsvIndexes.IRANDOM_SIZE] as double ])
        }
        result
    }

    def extractPrecisionAndRecall(String filename){
        if(!filename || filename.empty) return []
        def lines = CsvUtil.read(filename)
        def result = []
        if(lines.size()==0 || lines.size()<=CsvIndexes.HEADER_SIZE) return result
        def relevantLines = lines?.subList(CsvIndexes.HEADER_SIZE,lines.size())?.sort{ it[CsvIndexes.TASK_ID] as int }
        relevantLines?.each{
            result.add([ it[CsvIndexes.TASK_ID], it[CsvIndexes.PRECISION] as double, it[CsvIndexes.RECALL] as double])
        }
        result
    }

    def extractRandomPrecisionAndRecall(String filename){
        if(!filename || filename.empty) return []
        def lines = CsvUtil.read(filename)
        def result = []
        if(lines.size()==0 || lines.size()<= CsvIndexes.RANDOM_HEADER_SIZE) return result
        def relevantLines = lines?.subList(CsvIndexes.RANDOM_HEADER_SIZE,lines.size())?.sort{ it[CsvIndexes.TASK_ID] as int }
        relevantLines?.each{
            result.add([it[CsvIndexes.TASK_ID], it[CsvIndexes.RANDOM_PRECISION_INDEX] as double, it[CsvIndexes.RANDOM_RECALL_INDEX] as double ])
        }
        result
    }

    def extractFpAndFn(String filename){
        if(!filename || filename.empty) return []
        def lines = CsvUtil.read(filename)
        def result = []
        if(lines.size()==0 || lines.size()<=CsvIndexes.HEADER_SIZE) return result
        def relevantLines = lines?.subList(CsvIndexes.HEADER_SIZE,lines.size())?.sort{ it[CsvIndexes.TASK_ID] as int }
        relevantLines?.each{
            result.add([ it[CsvIndexes.TASK_ID], it[CsvIndexes.FALSE_POSITIVES_SIZE] as double,
                         it[CsvIndexes.FALSE_NEGATIVES_SIZE] as double])
        }
        result
    }

    def extractRandomFpAndFn(String filename){
        if(!filename || filename.empty) return []
        def lines = CsvUtil.read(filename)
        def result = []
        if(lines.size()==0 || lines.size()<= CsvIndexes.RANDOM_HEADER_SIZE) return result
        def relevantLines = lines?.subList(CsvIndexes.RANDOM_HEADER_SIZE,lines.size())?.sort{ it[CsvIndexes.TASK_ID] as int }
        relevantLines?.each{
            result.add([it[CsvIndexes.TASK_ID], it[CsvIndexes.RANDOM_FALSE_POSITIVES_SIZE] as double,
                        it[CsvIndexes.RANDOM_FALSE_NEGATIVES_SIZE] as double ])
        }
        result
    }

    def replaceMeasures(String filename){
        def entries = extractInterfaces(filename)
        def lines = CsvUtil.read(filename)
        def content = lines.subList(0, CsvIndexes.RANDOM_HEADER_SIZE-1)
        def header = lines.get(CsvIndexes.RANDOM_HEADER_SIZE-1)
        header[CsvIndexes.RANDOM_PRECISION_INDEX] = "Jaccard"
        header[CsvIndexes.RANDOM_RECALL_INDEX] = "Cosine"
        content += header
        def relevantLines = lines?.subList(CsvIndexes.RANDOM_HEADER_SIZE,lines.size())?.sort{ it[CsvIndexes.TASK_ID] as int }
        relevantLines?.eachWithIndex{ line, i ->
            def value = line
            value[CsvIndexes.RANDOM_PRECISION_INDEX] = entries[i][1]
            value[CsvIndexes.RANDOM_RECALL_INDEX] = entries[i][2]
            content += value as String[]
        }
        CsvUtil.write(filename, content)
    }

    private extractInterfaces(String filename){
        if(!filename || filename.empty) return []
        def lines = CsvUtil.read(filename)
        def result = []
        if(lines.size()==0 || lines.size()<= CsvIndexes.RANDOM_HEADER_SIZE) return result
        def relevantLines = lines?.subList(CsvIndexes.RANDOM_HEADER_SIZE,lines.size())?.sort{ it[CsvIndexes.TASK_ID] as int }
        relevantLines?.each{
            def irandom = it[CsvIndexes.RANDOM_INTERFACE]
            irandom = irandom.substring(1, irandom.size()-1).tokenize(",")*.trim()
            def ireal = it[CsvIndexes.RANDOM_IREAL_SIZE]
            ireal = ireal.substring(1,ireal.size()-1).tokenize(",")*.trim()
            def similarityAnalyser = new TestSimilarityAnalyser(irandom, ireal)
            def jaccard = similarityAnalyser.calculateSimilarityByJaccard()
            def cosine = similarityAnalyser.calculateSimilarityByCosine()
            result.add([it[CsvIndexes.TASK_ID], jaccard, cosine])
        }
        result
    }

    private extractMeasuresAndViews(String filename){
        if(!filename || filename.empty) return []
        def lines = CsvUtil.read(filename)
        def result = []
        if(lines.size()==0 || lines.size()<=CsvIndexes.HEADER_SIZE) return result
        def relevantLines = lines?.subList(CsvIndexes.HEADER_SIZE,lines.size())?.sort{ it[CsvIndexes.TASK_ID] as int }
        relevantLines?.each{
            result.add([ it[CsvIndexes.TASK_ID], it[CsvIndexes.NOT_FOUND_VIEWS_N] as int,
                         it[CsvIndexes.PRECISION] as double, it[CsvIndexes.RECALL] as double,
                         it[CsvIndexes.FALSE_POSITIVES_SIZE] as double, it[CsvIndexes.FALSE_NEGATIVES_SIZE] as double])
        }
        result
    }

    private extractTestInfo(String filename){
        if(!filename || filename.empty) return []
        def lines = CsvUtil.read(filename)
        def result = []
        if(lines.size()==0 || lines.size()<=CsvIndexes.HEADER_SIZE) return result
        def relevantLines = lines?.subList(CsvIndexes.HEADER_SIZE,lines.size())?.sort{ it[CsvIndexes.TASK_ID] as int }
        relevantLines?.each{
            result.add([ it[CsvIndexes.TASK_ID], it[CsvIndexes.GHERKIN_TESTS_N] as double, it[CsvIndexes.STEP_DEFS_N] as double])
        }
        result
    }

    static compare(oldValue, newValue){
        def n1 = oldValue?.round(3)
        def n2 = newValue?.round(3)
        def result = ""
        if(n2 == n1) result = "equals ($n1, $n2)"
        else if(n2 > n1) result = "yes ($n1, $n2, +${n2-n1})"
        else result = "no ($n1, $n2, -${n1-n2})"
        result
    }

    static inverseCompare(oldValue, newValue){
        def result = ""
        if(newValue == oldValue) result = "equals ($oldValue, $newValue)"
        else if(newValue < oldValue) result = "yes ($oldValue, $newValue, -${oldValue-newValue})"
        else result = "no ($oldValue, $newValue, +${newValue-oldValue})"
        result
    }

}
