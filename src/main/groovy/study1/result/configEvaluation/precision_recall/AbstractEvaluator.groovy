package study1.result.configEvaluation.precision_recall

import br.ufpe.cin.tan.util.CsvUtil
import groovy.util.logging.Slf4j
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import study1.result.configEvaluation.Evaluator
import study1.result.configEvaluation.InputFilesManager

@Slf4j
abstract class AbstractEvaluator {

    protected InputFilesManager inputFilesManager
    protected projects
    protected outputFolder
    protected aggregatedOutputFolder
    protected projectsResult
    protected projectsDetailedResult
    protected bufferResult
    protected aggregatedResult
    protected String title
    protected indexes

    AbstractEvaluator(InputFilesManager inputFilesManager, String outputFolderName, String title, def indexes){
        this.inputFilesManager = inputFilesManager
        this.title = title.toLowerCase()
        this.indexes = indexes
        projects = this.inputFilesManager.filesPerProject
        InputFilesManager.createFolder(Evaluator.INPUT_FOLDER)
        outputFolder = "${Evaluator.EVALUATION_FOLDER}${File.separator}${outputFolderName}"
        aggregatedOutputFolder = "$outputFolder${File.separator}aggregated"
        InputFilesManager.createFolder(outputFolder)
        InputFilesManager.createFolder(aggregatedOutputFolder)
        cleanEvaluationResult()
    }

    def cleanEvaluationResult(){
        projectsResult = []
        projectsDetailedResult = []
        bufferResult = []
        aggregatedResult = []
    }

    def evaluate(){
        cleanEvaluationResult()

        log.info "<< ${title.toUpperCase()} - INDIVIDUAL EVALUATION >>"
        projects?.each{ project ->
            log.info "Project: ${project.project}"
            evaluateProject(project)
        }

        log.info "<< ${title.toUpperCase()} - AGGREGATED EVALUATION >>"
        generateAggregatedResult()
        evaluateAggregatedResult()
        compareAllProjects()
    }

    def evaluateProject(project){
        Evaluator evaluator1 = new Evaluator(project[indexes.r1[0]], project[indexes.r1[1]],
                "${outputFolder}${File.separator}${project.project}_${indexes.r1[0]}&${indexes.r1[1]}.csv")
        def result1 = evaluator1.comparePrecisionAndRecall()

        Evaluator evaluator2 = new Evaluator(project[indexes.r2[0]], project[indexes.r2[1]],
                "${outputFolder}${File.separator}${project.project}_${indexes.r2[0]}&${indexes.r2[1]}.csv")
        def result2 = evaluator2.comparePrecisionAndRecall()

        Evaluator evaluator3 = new Evaluator(project[indexes.r3[0]], project[indexes.r3[1]],
                "${outputFolder}${File.separator}${project.project}_${indexes.r3[0]}&${indexes.r3[1]}.csv")
        def result3 = evaluator3.comparePrecisionAndRecall()

        Evaluator evaluator4 = new Evaluator(project[indexes.r4[0]], project[indexes.r4[1]],
                "${outputFolder}${File.separator}${project.project}_${indexes.r4[0]}&${indexes.r4[1]}.csv")
        def result4 = evaluator4.comparePrecisionAndRecall()

        def results = [result1, result2, result3, result4]
        def precision = [result1.p, result2.p, result3.p, result4.p]
        def recall = [result1.r, result2.r, result3.r, result4.r]
        def pResult = [equals:precision.count{it.contains("equals")},
                       yes:precision.count{it.contains("yes")},
                       no:precision.count{it.contains("no")}]
        def pMaxResult = pResult.max{it.value}.key
        def rResult = [equals:recall.count{it.contains("equals")},
                       yes:recall.count{it.contains("yes")},
                       no:recall.count{it.contains("no")}]
        def rMaxResult = rResult.max{it.value}.key

        saveProjectEvaluationResult(project, results, pMaxResult, pResult, rMaxResult, rResult)
        bufferResult += [project: project.project, (indexes.r1.join()): result1.result, (indexes.r2.join()): result2.result,
          (indexes.r3.join()): result3.result, (indexes.r4.join()): result4.result]
        projectsDetailedResult += [project:project.project, pEvaluation: pResult, rEvaluation: rResult,
          precision: [(indexes.r1.join()):result1.p, (indexes.r2.join()):result2.p ,(indexes.r3.join()):result3.p, (indexes.r4.join()): result4.p],
          recall:[(indexes.r1.join()):result1.r, (indexes.r2.join()):result2.r, (indexes.r3.join()):result3.r, (indexes.r4.join()):result4.r]
        ]
        projectsResult += [project:project.project, precision:pResult, recall:rResult]
    }

    def compareAllProjects(){
        def groups = (projectsResult*.project).unique()
        def resultByProject = []
        groups.each{ group ->
            def values = projectsResult.findAll{ it.project == group }
            def pEquals = values.collect{ it.precision.equals as int }.sum()
            def pYes = values.collect{ it.precision.yes as int }.sum()
            def pNo = values.collect{ it.precision.no as int }.sum()
            def rEquals = values.collect{ it.recall.equals as int }.sum()
            def rYes = values.collect{ it.recall.yes as int }.sum()
            def rNo = values.collect{ it.recall.no as int }.sum()
            resultByProject += [project:group, precision:[equals:pEquals, yes:pYes, no:pNo],
                                recall:[equals:rEquals, yes:rYes, no:rNo]]
        }

        def precision = resultByProject*.precision
        def pResult = [equals:(precision.collect{ it.equals as int }.sum()),
                       yes:(precision.collect{ it.yes as int }.sum()),
                       no:(precision.collect{ it.no as int }.sum())]
        def pMaxResult = pResult.max{it.value}.key

        def recall = resultByProject*.recall
        def rResult = [equals:(recall.collect{ it.equals as int }.sum()),
                       yes:(recall.collect{ it.yes as int }.sum()),
                       no:(recall.collect{ it.no as int }.sum())]
        def rMaxResult = rResult.max{it.value}.key

        saveAggregatedData(pMaxResult,pResult, rMaxResult, rResult, groups, resultByProject)
    }

    def generateAggregatedResult(){
        if(bufferResult.empty) return

        def keys = (bufferResult.get(0).keySet() + bufferResult.get(1).keySet()).unique()
        def configs = keys - ["project"]
        configs.each{ config ->
            def aux = config.toString()
            log.info "Configuration: ${aux.getAt(0)}&${aux.substring(1)}"
            def values = bufferResult.findAll{ it.containsKey(config) }
            values = values.collect{ [project: it.project, (config):it[config]] }
            values.each{ v ->
                def tasks = v[config]
                tasks.each{ task ->
                    aggregatedResult += [project: v.project, conf:config, id: task.id, p1:task.p1, p2:task.p2, r1:task.r1, r2:task.r2]
                }
            }
        }
    }

    def evaluateAggregatedResult(){
        def configurations = aggregatedResult.collect{ it.conf }.unique()
        configurations.each{ config ->
            def values = aggregatedResult.findAll{ it.conf == config }

            double[] precision1 = values.collect { it.p1 } as double[]
            def precisionStats1 = new DescriptiveStatistics(precision1)
            def resultPrecision1 = [mean:precisionStats1.mean, median:precisionStats1.getPercentile(50.0), sd:precisionStats1.standardDeviation]

            double[] precision2 = values.collect { it.p2 } as double[]
            def precisionStats2 = new DescriptiveStatistics(precision2)
            def resultPrecision2 = [mean:precisionStats2.mean, median:precisionStats2.getPercentile(50.0), sd:precisionStats2.standardDeviation]

            double[] recall1 = values.collect { it.r1 } as double[]
            def recallStats1 = new DescriptiveStatistics(recall1)
            def resultRecall1 = [mean:recallStats1.mean, median:recallStats1.getPercentile(50.0), sd:recallStats1.standardDeviation]

            double[] recall2 = values.collect { it.r2 } as double[]
            def recallStats2 = new DescriptiveStatistics(recall2)
            def resultRecall2 = [mean:recallStats2.mean, median:recallStats2.getPercentile(50.0), sd:recallStats2.standardDeviation]

            def precisionEvaluation = Evaluator.compare(precisionStats1.mean, precisionStats2.mean)
            def recallEvaluation = Evaluator.compare(recallStats1.mean, recallStats2.mean)

            def configStatistics = [config:config,
                                    p1:resultPrecision1, p2:resultPrecision2, pEvaluation: precisionEvaluation,
                                    r1:resultRecall1, r2:resultRecall2, rEvaluation:recallEvaluation]
            saveAggregatedResult(values, configStatistics)
        }
    }

    def saveAggregatedResult(values, configStatistics){
        def aux = configStatistics.config.toString()
        def configName = aux.getAt(0)+"&" + aux.substring(1)
        def filename = "$aggregatedOutputFolder${File.separator}${title}_overview_aggregated_${configName}.csv"

        List<String[]> content = []
        content += ["PROJECT", "TASK_ID", "P1", "P2", "R1", "R2"] as String[]

        values.each{ value ->
            content += [value.project, value.id, value.p1, value.p2, value.r1, value.r2] as String[]
        }

        content += ["(1) ${Evaluator.PRECISION_TEXT} mean", configStatistics.p1.mean] as String[]
        content += ["(1) ${Evaluator.PRECISION_TEXT} median", configStatistics.p1.median] as String[]
        content += ["(1) ${Evaluator.PRECISION_TEXT} standard deviation", configStatistics.p1.sd] as String[]
        content += ["(2) ${Evaluator.PRECISION_TEXT} mean", configStatistics.p2.mean] as String[]
        content += ["(2) ${Evaluator.PRECISION_TEXT} median", configStatistics.p2.median] as String[]
        content += ["(2) ${Evaluator.PRECISION_TEXT} standard deviation", configStatistics.p2.sd] as String[]
        content += ["Improved mean ${Evaluator.PRECISION_TEXT}", configStatistics.pEvaluation] as String[]

        content += [""] as String[]
        content += ["(1) ${Evaluator.RECALL_TEXT} mean", configStatistics.r1.mean] as String[]
        content += ["(1) ${Evaluator.RECALL_TEXT} median", configStatistics.r1.median] as String[]
        content += ["(1) ${Evaluator.RECALL_TEXT} standard deviation", configStatistics.r1.sd] as String[]
        content += ["(2) ${Evaluator.RECALL_TEXT} mean", configStatistics.r2.mean] as String[]
        content += ["(2) ${Evaluator.RECALL_TEXT} median", configStatistics.r2.median] as String[]
        content += ["(2) ${Evaluator.RECALL_TEXT} standard deviation", configStatistics.r2.sd] as String[]
        content += ["Improved mean ${Evaluator.RECALL_TEXT}", configStatistics.rEvaluation] as String[]
        CsvUtil.write(filename, content)
    }

    def saveAggregatedData(pMaxResult, pResult, rMaxResult, rResult, groups, resultByProject){
        List<String[]> content = []
        content += generatePrecisionData(pMaxResult, pResult, groups, resultByProject)
        content += generateRecallData(rMaxResult, rResult, groups, resultByProject)
        def overviewFile = "$aggregatedOutputFolder${File.separator}${title}_overview_byProject.csv"
        CsvUtil.write(overviewFile, content)
    }

    def generatePrecisionData(pMaxResult, pResult, groups, resultByProject){
        List<String[]> content = []
        content += ["${title} improved mean ${Evaluator.PRECISION_TEXT} (all projects, each 4 configurations):",
                    "${pMaxResult} (${pResult})"] as String[]
        groups.each{ group ->
            def evaluation = resultByProject.find{ it.project == group }
            content += [evaluation.project, evaluation.precision.toString()] as String[]
            def detailedResults = projectsDetailedResult.find{ it.project == group }
            def drPrecisionList = detailedResults.precision
            drPrecisionList.each{ drp ->
                def aux = drp.key.toString()
                content += ["${aux.getAt(0)}&${aux.substring(1)}", drp.value] as String[]
            }
        }
        content
    }

    def generateRecallData(rMaxResult, rResult, groups, resultByProject){
        List<String[]> content = []
        content += ["${title} improved mean ${Evaluator.RECALL_TEXT} (all projects, each 4 configurations):",
                    "${rMaxResult} (${rResult})"] as String[]
        groups.each{ group ->
            def evaluation = resultByProject.find{ it.project == group }
            content += [evaluation.project, evaluation.recall.toString()] as String[]
            def detailedResults = projectsDetailedResult.find{ it.project == group }
            def drRecallList = detailedResults.recall
            drRecallList.each{ drp ->
                def aux = drp.key.toString()
                content += ["${aux.getAt(0)}&${aux.substring(1)}", drp.value] as String[]
            }
        }
        content
    }

    def saveProjectEvaluationResult(project, results, pMaxResult, pResult, rMaxResult, rResult){
        List<String[]> content = []
        content += ["${title} improved mean ${Evaluator.PRECISION_TEXT}:", "${pMaxResult} (${pResult})"] as String[]
        content += ["(${indexes.r1[0]},${indexes.r1[1]})", results[0]?.p] as String[]
        content += ["(${indexes.r2[0]},${indexes.r2[1]})", results[1]?.p] as String[]
        content += ["(${indexes.r3[0]},${indexes.r3[1]})", results[2]?.p] as String[]
        content += ["(${indexes.r4[0]},${indexes.r4[1]})", results[3]?.p] as String[]
        content += ["${title} improved mean ${Evaluator.RECALL_TEXT}:", "${rMaxResult} (${rResult})"] as String[]
        content += ["(${indexes.r1[0]},${indexes.r1[1]})", results[0]?.r] as String[]
        content += ["(${indexes.r2[0]},${indexes.r2[1]})", results[1]?.r] as String[]
        content += ["(${indexes.r3[0]},${indexes.r3[1]})", results[2]?.r] as String[]
        content += ["(${indexes.r4[0]},${indexes.r4[1]})", results[3]?.r] as String[]

        def overviewFile = "${outputFolder}${File.separator}${project.project}_${title}_overview.csv"
        CsvUtil.write(overviewFile, content)
    }

}
