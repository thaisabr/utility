package study1.result.configEvaluation.precision_recall

import groovy.util.logging.Slf4j
import study1.result.configEvaluation.InputFilesManager

/*
* Avalia se filtrar por when é vantajoso ou não.
* A comparação é feita entre tarefas relevantes (usadas no estudo envolvendo ITestD).
* */
@Slf4j
class WhenEvaluator extends AbstractEvaluator {

    WhenEvaluator(InputFilesManager inputFilesManager){
        super(inputFilesManager, "whenEvaluation", "WHEN", [r1:[1,5], r2:[2,6], r3:[3,7], r4:[4,8]])
    }

}
