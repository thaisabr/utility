package study1.result.configEvaluation.precision_recall

import groovy.util.logging.Slf4j
import study1.result.configEvaluation.InputFilesManager

/*
* Avalia se filtrar por added scenarios é vantajoso ou não.
* A comparação é feita entre tarefas relevantes (usadas no estudo envolvendo ITestD).
* */
@Slf4j
class AddedEvaluator extends AbstractEvaluator {

    AddedEvaluator(InputFilesManager inputFilesManager){
        super(inputFilesManager, "addedEvaluation", "ADDED", [r1:[1,2], r2:[3,4], r3:[5,6], r4:[7,8]])
    }

}
