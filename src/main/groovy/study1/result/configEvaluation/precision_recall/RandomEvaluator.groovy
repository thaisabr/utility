package study1.result.configEvaluation.precision_recall

import br.ufpe.cin.tan.util.CsvUtil
import groovy.util.logging.Slf4j
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import study1.result.configEvaluation.Evaluator
import study1.result.configEvaluation.InputFilesManager

/*
* Avalia se o resultado randômico consegue ser melhor que ITestS, em suas diferentes configurações.
* A comparação é feita entre tarefas relevantes (usadas no estudo envolvendo ITestD).
* Há 2 estratégias para calcular IRandom:
*    (i) sortear o tamanho de IRandom e seu conteúdo;
*    (ii) sortear se cada arquivo candidato será incluído em IRandom ou não.
* Para a avaliação rodar, o arquivo com IRandom deve estar na pasta "results/output_all".
* */

@Slf4j
class RandomEvaluator extends AbstractEvaluator {

    RandomEvaluator(InputFilesManager inputFilesManager){
        super(inputFilesManager, "randomEvaluation", "RANDOM",
                [r1:[1,9], r2:[2,9], r3:[5,9], r4:[6,9], r5:[3,10], r6:[4,10], r7:[7,10], r8:[8,10]])
    }

    RandomEvaluator(InputFilesManager inputFilesManager, String outputFolderName, String title, def indexes){
        super(inputFilesManager, outputFolderName, title, indexes)
    }

    @Override
    def evaluate(){
        cleanEvaluationResult()

        log.info "<< ${title.toUpperCase()} - INDIVIDUAL EVALUATION >>"
        projects.each{ project ->
            log.info "Project: ${project.project}"
            evaluateProject(project)
            evaluateProjectController(project)
        }

        log.info "<< ${title.toUpperCase()} - AGGREGATED EVALUATION >>"
        generateAggregatedResult()
        evaluateAggregatedResult()
        compareAllProjects()
    }

    @Override
    def evaluateProject(project){
        Evaluator evaluator1 = new Evaluator(project[indexes.r1[0]], project[indexes.r1[1]],
                "${outputFolder}${File.separator}${project.project}_${indexes.r1[0]}&${indexes.r1[1]}.csv")
        def result1 = evaluator1.compareRandomPrecisionAndRecall()

        Evaluator evaluator2 = new Evaluator(project[indexes.r2[0]], project[indexes.r2[1]],
                "${outputFolder}${File.separator}${project.project}_${indexes.r2[0]}&${indexes.r2[1]}.csv")
        def result2 = evaluator2.compareRandomPrecisionAndRecall()

        Evaluator evaluator3 = new Evaluator(project[indexes.r3[0]], project[indexes.r3[1]],
                "${outputFolder}${File.separator}${project.project}_${indexes.r3[0]}&${indexes.r3[1]}.csv")
        def result3 = evaluator3.compareRandomPrecisionAndRecall()

        Evaluator evaluator4 = new Evaluator(project[indexes.r4[0]], project[indexes.r4[1]],
                "${outputFolder}${File.separator}${project.project}_${indexes.r4[0]}&${indexes.r4[1]}.csv")
        def result4 = evaluator4.compareRandomPrecisionAndRecall()

        if(!result1 || !result2 || !result3 || !result4) return

        def results = [result1, result2, result3, result4]
        def precision = [result1.p, result2.p, result3.p, result4.p]
        def recall = [result1.r, result2.r, result3.r, result4.r]
        def pResult = [equals:precision.count{it.contains("equals")},
                       yes:precision.count{it.contains("yes")},
                       no:precision.count{it.contains("no")}]
        def pMaxResult = pResult.max{it.value}.key
        def rResult = [equals:recall.count{it.contains("equals")},
                       yes:recall.count{it.contains("yes")},
                       no:recall.count{it.contains("no")}]
        def rMaxResult = rResult.max{it.value}.key

        saveProjectEvaluationResult(project, results, pMaxResult, pResult, rMaxResult, rResult)
        bufferResult += [project: project.project, (indexes.r1.join()): result1.result, (indexes.r2.join()): result2.result,
           (indexes.r3.join()): result3.result, (indexes.r4.join()): result4.result]
        projectsDetailedResult += [project:project.project, pEvaluation: pResult, rEvaluation: rResult,
            precision: [(indexes.r1.join()):result1.p, (indexes.r2.join()):result2.p ,(indexes.r3.join()):result3.p, (indexes.r4.join()): result4.p],
            recall:[(indexes.r1.join()):result1.r, (indexes.r2.join()):result2.r, (indexes.r3.join()):result3.r, (indexes.r4.join()):result4.r]
        ]
        projectsResult += [project:project.project, precision:pResult, recall:rResult]
    }

    @Override
    def generatePrecisionData(pMaxResult, pResult, groups, resultByProject){
        List<String[]> content = []
        content += ["${title} improved mean ${Evaluator.PRECISION_TEXT} (all projects, each 8 configurations):",
                    "${pMaxResult} (${pResult})"] as String[]
        groups.each{ group ->
            def evaluation = resultByProject.find{ it.project == group }
            content += [evaluation.project, evaluation.precision.toString()] as String[]
            def detailedResults = projectsDetailedResult.findAll{ it.project == group }
            def drPrecision = (detailedResults*.precision).flatten()
            def drPrecisionList = drPrecision.get(0) + drPrecision.get(1)
            drPrecisionList.each{ drp ->
                def aux = drp.key.toString()
                content += ["${aux.getAt(0)}&${aux.substring(1)}", drp.value] as String[]
            }
        }
        content
    }

    @Override
    def generateRecallData(rMaxResult, rResult, groups, resultByProject){
        List<String[]> content = []
        content += ["${title} improved mean ${Evaluator.RECALL_TEXT} (all projects, each 8 configurations):",
                    "${rMaxResult} (${rResult})"] as String[]
        groups.each{ group ->
            def evaluation = resultByProject.find{ it.project == group }
            content += [evaluation.project, evaluation.recall.toString()] as String[]
            def detailedResults = projectsDetailedResult.findAll{ it.project == group }
            def drRecall = (detailedResults*.recall).flatten()
            def drRecallList = drRecall.get(0) + drRecall.get(1)
            drRecallList.each{ drp ->
                def aux = drp.key.toString()
                content += ["${aux.getAt(0)}&${aux.substring(1)}", drp.value] as String[]
            }
        }
        content
    }

    def evaluateAggregatedResult(){
        def result = []
        def configurations = aggregatedResult.collect{ it.conf }.unique()
        configurations.each{ config ->
            def values = aggregatedResult.findAll{ it.conf == config }
            double[] precision1 = values.collect { it.p1 } as double[]
            def precisionStats1 = new DescriptiveStatistics(precision1)
            def resultPrecision1 = [mean:precisionStats1.mean, median:precisionStats1.getPercentile(50.0), sd:precisionStats1.standardDeviation]

            double[] precision2 = values.collect { it.p2 } as double[]
            def precisionStats2 = new DescriptiveStatistics(precision2)
            def resultPrecision2 = [mean:precisionStats2.mean, median:precisionStats2.getPercentile(50.0), sd:precisionStats2.standardDeviation]

            double[] recall1 = values.collect { it.r1 } as double[]
            def recallStats1 = new DescriptiveStatistics(recall1)
            def resultRecall1 = [mean:recallStats1.mean, median:recallStats1.getPercentile(50.0), sd:recallStats1.standardDeviation]

            double[] recall2 = values.collect { it.r2 } as double[]
            def recallStats2 = new DescriptiveStatistics(recall2)
            def resultRecall2 = [mean:recallStats2.mean, median:recallStats2.getPercentile(50.0), sd:recallStats2.standardDeviation]

            def precisionEvaluation = Evaluator.compare(precisionStats1.mean, precisionStats2.mean)
            def recallEvaluation = Evaluator.compare(recallStats1.mean, recallStats2.mean)

            def configStatistics = [config:config,
                                p1:resultPrecision1, p2:resultPrecision2, pEvaluation: precisionEvaluation,
                                r1:resultRecall1, r2:resultRecall2, rEvaluation:recallEvaluation]
            saveAggregatedResult(values, configStatistics)
            result += configStatistics
        }

        saveAggregatedEvaluation(result)
    }

    protected saveAggregatedEvaluation(result){
        def overviewFile = "$aggregatedOutputFolder${File.separator}random_overview_aggregated.csv"
        List<String[]> content = []
        content += ["CONFIGURATION", "PRECISION (ITestS)", "", "", "PRECISION (IRandom)", "", "", "PRECISION EVALUATION",
                    "RECALL (ITestS)", "", "", "RECALL (IRandom)", "", "", "RECALL EVALUATION" ] as String[]
        content += ["", "MEAN", "MEDIAN", "SD", "MEAN", "MEDIAN", "SD", "",
                    "MEAN", "MEDIAN", "SD", "MEAN", "MEDIAN", "SD", "" ] as String[]
        result.each {
            def aux = it.config.toString()
            content += ["${aux.getAt(0)}&${aux.substring(1)}",
                        it.p1.mean, it.p1.median, it.p1.sd,
                        it.p2.mean, it.p2.median, it.p2.sd,
                        it.pEvaluation,
                        it.r1.mean, it.r1.median, it.r1.sd,
                        it.r2.mean, it.r2.median, it.r2.sd,
                        it.rEvaluation
            ] as String[]
        }
        CsvUtil.write(overviewFile, content)
    }

    protected saveControllerProjectEvalulationResult(project, results, pMaxResult, pResult, rMaxResult, rResult){
        List<String[]> content = []
        content += ["${title} controller improved mean ${Evaluator.PRECISION_TEXT}", "${pMaxResult} (${pResult})"] as String[]
        content += ["(3,10)", results[0]?.p] as String[]
        content += ["(4,10)", results[1]?.p] as String[]
        content += ["(7,10)", results[2]?.p] as String[]
        content += ["(8,10)", results[3]?.p] as String[]
        content += ["${title} controller improved mean ${Evaluator.RECALL_TEXT}", "${rMaxResult} (${rResult})"] as String[]
        content += ["(3,10)", results[0]?.r] as String[]
        content += ["(4,10)", results[1]?.r] as String[]
        content += ["(7,10)", results[2]?.r] as String[]
        content += ["(8,10)", results[3]?.r] as String[]
        def overviewFile = "${outputFolder}${File.separator}${project.project}_${title}_overview.csv"
        CsvUtil.append(overviewFile, content)
    }

    protected evaluateProjectController(project){
        Evaluator reader1 = new Evaluator(project[3], project[10],
                "${outputFolder}${File.separator}${project.project}_3&10.csv")
        def result3_10 = reader1.compareRandomPrecisionAndRecall()

        Evaluator reader2 = new Evaluator(project[4], project[10],
                "${outputFolder}${File.separator}${project.project}_4&10.csv")
        def result4_10 = reader2.compareRandomPrecisionAndRecall()

        Evaluator reader3 = new Evaluator(project[7], project[10],
                "${outputFolder}${File.separator}${project.project}_7&10.csv")
        def result7_10 = reader3.compareRandomPrecisionAndRecall()

        Evaluator reader4 = new Evaluator(project[8], project[10],
                "${outputFolder}${File.separator}${project.project}_8&10.csv")
        def result8_10 = reader4.compareRandomPrecisionAndRecall()

        if(!result3_10 || !result4_10 || !result7_10 || !result8_10) return

        def results = [result3_10, result4_10, result7_10, result8_10]
        def precision = [result3_10.p, result4_10.p, result7_10.p, result8_10.p]
        def recall = [result3_10.r, result4_10.r, result7_10.r, result8_10.r]
        def pResult = [equals:precision.count{it.contains("equals")},
                       yes:precision.count{it.contains("yes")},
                       no:precision.count{it.contains("no")}]
        def pMaxResult = pResult.max{it.value}.key
        def rResult = [equals:recall.count{it.contains("equals")},
                       yes:recall.count{it.contains("yes")},
                       no:recall.count{it.contains("no")}]
        def rMaxResult = rResult.max{it.value}.key

        saveControllerProjectEvalulationResult(project, results, pMaxResult, pResult, rMaxResult, rResult)
        bufferResult += [project: project.project, 310: result3_10.result, 410: result4_10.result,
                         710: result7_10.result, 810: result8_10.result]
        projectsDetailedResult += [project:project.project, pEvaluation: pResult, rEvaluation: rResult,
                                   precision: [310: result3_10.p, 410: result4_10.p, 710: result7_10.p, 810: result8_10.p],
                                   recall:[310: result3_10.r, 410: result4_10.r, 710:result7_10.r, 810: result8_10.r]
        ]
        projectsResult += [project:project.project, precision:pResult, recall:rResult]
    }

}
