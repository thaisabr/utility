package study1.result.configEvaluation.precision_recall

import groovy.util.logging.Slf4j
import study1.result.configEvaluation.InputFilesManager

/*
* Avalia se filtrar por controller é vantajoso ou não.
* A comparação é feita entre tarefas relevantes (usadas no estudo envolvendo ITestD).
* */
@Slf4j
class ControllerEvaluator extends AbstractEvaluator {

    ControllerEvaluator(InputFilesManager inputFilesManager){
        super(inputFilesManager, "controllerEvaluation", "CONTROLLER", [r1:[1,3], r2:[2,4], r3:[5,7], r4:[6,8]])
    }

}
