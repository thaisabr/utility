package study1.result.configEvaluation.precision_recall

import groovy.util.logging.Slf4j
import study1.result.configEvaluation.Evaluator
import study1.result.configEvaluation.InputFilesManager
import study1.result.configEvaluation.precision_recall.RandomEvaluator

/*
*
* Avalia se IText consegue ser melhor que ITestS, em suas diferentes configurações.
* A comparação é feita entre tarefas relevantes (usadas no estudo envolvendo ITestD).
*
* A estratégia para calcular IText é definida em text.IText. Ela consiste no cálculo da interseção de IReal entre
* as 5 tarefas mais parecidas textualmente.
*
* */
@Slf4j
class TextEvaluator extends RandomEvaluator {

    TextEvaluator(InputFilesManager inputFilesManager){
        super(inputFilesManager, "textEvaluation", "TEXT",
                [r1:[1,11], r2:[2,12], r3:[5,11], r4:[6,12], r5:[3,13], r6:[4,14], r7:[7,13], r8:[8,14]])
    }

    @Override
    def evaluateProject(project){
        Evaluator evaluator1 = new Evaluator(project[indexes.r1[0]], project[indexes.r1[1]],
                "${outputFolder}${File.separator}${project.project}_${indexes.r1[0]}&${indexes.r1[1]}.csv")
        def result1 = evaluator1.compareRandomPrecisionAndRecall()

        if(!result1) return

        def results = [result1]
        def precision = [result1.p]
        def recall = [result1.r]
        def pResult = [equals:precision.count{it.contains("equals")},
                       yes:precision.count{it.contains("yes")},
                       no:precision.count{it.contains("no")}]
        def pMaxResult = pResult.max{it.value}.key
        def rResult = [equals:recall.count{it.contains("equals")},
                       yes:recall.count{it.contains("yes")},
                       no:recall.count{it.contains("no")}]
        def rMaxResult = rResult.max{it.value}.key

        saveProjectEvaluationResult(project, results, pMaxResult, pResult, rMaxResult, rResult)
        bufferResult += [project: project.project, (indexes.r1.join()): result1.result]
        projectsDetailedResult += [project:project.project, pEvaluation: pResult, rEvaluation: rResult,
                                   precision: [(indexes.r1.join()):result1.p],
                                   recall:[(indexes.r1.join()):result1.r]
        ]
        projectsResult += [project:project.project, precision:pResult, recall:rResult]
    }

    @Override
    protected evaluateProjectController(project){
        Evaluator reader1 = new Evaluator(project[3], project[10],
                "${outputFolder}${File.separator}${project.project}_3&10.csv")
        def result3_10 = reader1.compareRandomPrecisionAndRecall()

        if(!result3_10) return

        def results = [result3_10]
        def precision = [result3_10.p]
        def recall = [result3_10.r]
        def pResult = [equals:precision.count{it.contains("equals")},
                       yes:precision.count{it.contains("yes")},
                       no:precision.count{it.contains("no")}]
        def pMaxResult = pResult.max{it.value}.key
        def rResult = [equals:recall.count{it.contains("equals")},
                       yes:recall.count{it.contains("yes")},
                       no:recall.count{it.contains("no")}]
        def rMaxResult = rResult.max{it.value}.key

        saveControllerProjectEvalulationResult(project, results, pMaxResult, pResult, rMaxResult, rResult)
        bufferResult += [project: project.project, 310: result3_10.result]
        projectsDetailedResult += [project:project.project, pEvaluation: pResult, rEvaluation: rResult,
                                   precision: [310: result3_10.p], recall:[310: result3_10.r]
        ]
        projectsResult += [project:project.project, precision:pResult, recall:rResult]
    }

    /*InputFilesManager result
    def projects
    def outputFolder
    def projectsResult
    def projectsDetailedResult

    TextEvaluator(InputFilesManager result){
        this.result = result
        projects = this.result.filesPerProject
        InputFilesManager.createFolder(Evaluator.INPUT_FOLDER)
        InputFilesManager.createFolder(Evaluator.OUTPUT_FOLDER)
        outputFolder = "${Evaluator.EVALUATION_FOLDER}${File.separator}textEvaluation"
        InputFilesManager.createFolder(outputFolder)
        projectsResult = []
        projectsDetailedResult = []
    }

    def evaluate(){
        log.info "<< TEXTUAL EVALUATION >>"
        projects?.each{ project ->
            log.info "Project: ${project.project}"
            evaluateProject(project)
            evaluateProjectController(project)
        }
        compareAllProjects()
    }

    private saveProjectEvaluationResult(List results, pMaxResult, pResult, rMaxResult, rResult){
        List<String[]> content = []
        content += ["Text improved mean ${Evaluator.PRECISION_TEXT}", "${pMaxResult} (${pResult})"] as String[]
        content += ["(1,11)", results[0]?.p] as String[]
        content += ["(2,12)", results[1]?.p] as String[]
        content += ["(5,11)", results[2]?.p] as String[]
        content += ["(6,12)", results[3]?.p] as String[]
        content += ["Text improved mean ${Evaluator.RECALL_TEXT}", "${rMaxResult} (${rResult})"] as String[]
        content += ["(1,11)", results[0]?.r] as String[]
        content += ["(2,12)", results[1]?.r] as String[]
        content += ["(5,11)", results[2]?.r] as String[]
        content += ["(6,12)", results[3]?.r] as String[]
        def overviewFile = "${outputFolder}${File.separator}${project.project}_text_overview.csv"
        CsvUtil.write(overviewFile, content)
    }

    private saveControllerProjectEvalulationResult(List results, pMaxResult, pResult, rMaxResult, rResult){
        List<String[]> content = []
        content += ["Text controller improved mean ${Evaluator.PRECISION_TEXT}", "${pMaxResult} (${pResult})"] as String[]
        content += ["(3,13)", results[0]?.p] as String[]
        content += ["(4,14)", results[1]?.p] as String[]
        content += ["(7,13)", results[2]?.p] as String[]
        content += ["(8,14)", results[3]?.p] as String[]
        content += ["Text controller improved mean ${Evaluator.RECALL_TEXT}", "${rMaxResult} (${rResult})"] as String[]
        content += ["(3,13)", results[0]?.r] as String[]
        content += ["(4,14)", results[1]?.r] as String[]
        content += ["(7,13)", results[2]?.r] as String[]
        content += ["(8,14)", results[3]?.r] as String[]
        def overviewFile = "${outputFolder}${File.separator}${project.project}_text_overview.csv"
        CsvUtil.append(overviewFile, content)
    }

    private evaluateProject(project){
        Evaluator reader1 = new Evaluator(project[1], project[11],
                "${outputFolder}${File.separator}${project.project}_1&11.csv")
        def result1_11 = reader1.compareRandomPrecisionAndRecall()

        Evaluator reader2 = new Evaluator(project[2], project[12],
                "${outputFolder}${File.separator}${project.project}_2&12.csv")
        def result2_12 = reader2.compareRandomPrecisionAndRecall()

        Evaluator reader3 = new Evaluator(project[5], project[11],
                "${outputFolder}${File.separator}${project.project}_5&11.csv")
        def result5_11 = reader3.compareRandomPrecisionAndRecall()

        Evaluator reader4 = new Evaluator(project[6], project[12],
                "${outputFolder}${File.separator}${project.project}_6&12.csv")
        def result6_12 = reader4.compareRandomPrecisionAndRecall()

        if(!result1_11 || !result2_12 || !result5_11 || !result6_12) return

        def results = [result1_11, result2_12, result5_11, result6_12]
        def precision = [result1_11.p, result2_12.p, result5_11.p, result6_12.p]
        def recall = [result1_11.r, result2_12.r, result5_11.r, result6_12.r]
        def pResult = [equals:precision.count{it.contains("equals")},
                       yes:precision.count{it.contains("yes")},
                       no:precision.count{it.contains("no")}]
        def pMaxResult = pResult.max{it.value}.key
        def rResult = [equals:recall.count{it.contains("equals")},
                       yes:recall.count{it.contains("yes")},
                       no:recall.count{it.contains("no")}]
        def rMaxResult = rResult.max{it.value}.key

        saveProjectEvaluationResult(results, pMaxResult, pResult, rMaxResult, rResult)
        projectsResult += [project:project.project, precision:pResult, recall:rResult]
    }

    private evaluateProjectController(project){
        Evaluator reader1 = new Evaluator(project[3], project[13],
                "${outputFolder}${File.separator}${project.project}_3&13.csv")
        def result3_13 = reader1.compareRandomPrecisionAndRecall()

        Evaluator reader2 = new Evaluator(project[4], project[14],
                "${outputFolder}${File.separator}${project.project}_4&14.csv")
        def result4_14 = reader2.compareRandomPrecisionAndRecall()

        Evaluator reader3 = new Evaluator(project[7], project[13],
                "${outputFolder}${File.separator}${project.project}_7&13.csv")
        def result7_13 = reader3.compareRandomPrecisionAndRecall()

        Evaluator reader4 = new Evaluator(project[8], project[14],
                "${outputFolder}${File.separator}${project.project}_8&14.csv")
        def result8_14 = reader4.compareRandomPrecisionAndRecall()

        if(!result3_13 || !result4_14 || !result7_13 || !result8_14) return

        def results = [result3_13, result4_14, result7_13, result8_14]
        def precision = [result3_13.p, result4_14.p, result7_13.p, result8_14.p]
        def recall = [result3_13.r, result4_14.r, result7_13.r, result8_14.r]
        def pResult = [equals:precision.count{it.contains("equals")},
                       yes:precision.count{it.contains("yes")},
                       no:precision.count{it.contains("no")}]
        def pMaxResult = pResult.max{it.value}.key
        def rResult = [equals:recall.count{it.contains("equals")},
                       yes:recall.count{it.contains("yes")},
                       no:recall.count{it.contains("no")}]
        def rMaxResult = rResult.max{it.value}.key

        saveControllerProjectEvalulationResult(results, pMaxResult, pResult, rMaxResult, rResult)
        projectsResult += [project:project.project, precision:pResult, recall:rResult]
    }

    private compareAllProjects(){
        def precision = projectsResult*.precision
        def pResult = [equals:(precision.collect{ it.equals as int }.sum()),
                       yes:(precision.collect{ it.yes as int }.sum()),
                       no:(precision.collect{ it.no as int }.sum())]
        def pMaxResult = pResult.max{it.value}.key

        def recall = projectsResult*.recall
        def rResult = [equals:(recall.collect{ it.equals as int }.sum()),
                       yes:(recall.collect{ it.yes as int }.sum()),
                       no:(recall.collect{ it.no as int }.sum())]
        def rMaxResult = rResult.max{it.value}.key

        List<String[]> content = []
        content += ["Text improved mean ${Evaluator.PRECISION_TEXT} (all projects, each 8 configurations):",
                    "${pMaxResult} (${pResult})"] as String[]
        precision.each{ content += [it.toString()] as String[] }
        content += ["Text improved mean ${Evaluator.RECALL_TEXT} (all projects, each 8 configurations):",
                    "${rMaxResult} (${rResult})"] as String[]
        recall.each{ content += [it.toString()] as String[] }
        def overviewFile = "${Evaluator.OUTPUT_FOLDER}${File.separator}text_overview.csv"
        CsvUtil.write(overviewFile, content)
    }*/

}
