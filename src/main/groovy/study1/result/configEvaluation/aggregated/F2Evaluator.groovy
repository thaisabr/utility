package study1.result.configEvaluation.aggregated

import br.ufpe.cin.tan.util.CsvUtil
import groovy.util.logging.Slf4j
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import study1.result.configEvaluation.Evaluator
import study1.result.configEvaluation.InputFilesManager

@Slf4j
class F2Evaluator {

    InputFilesManager inputFilesManager
    def projects
    def outputFile
    List<String[]> content
    def measures
    def resultF2

    F2Evaluator(InputFilesManager inputFilesManager){
        this.inputFilesManager = inputFilesManager
        projects = this.inputFilesManager.filesPerProject.findAll{ it.project != "selected" }
        InputFilesManager.createFolder(Evaluator.INPUT_FOLDER)
        InputFilesManager.createFolder(Evaluator.OUTPUT_FOLDER)
        def outputFolder = "${Evaluator.EVALUATION_FOLDER}${File.separator}overview"
        outputFile = "${outputFolder}${File.separator}f2.csv"
        InputFilesManager.createFolder(outputFolder)
        content = []
        resultF2 = []
    }

    def evaluate(){
        log.info "<< F2 EVALUATION >>"
        content += ["Project","Task","1", "2", "3", "4", "5", "6", "7", "8", "9", "10"] as String[]
        projects?.each{ project ->
            log.info "Project: ${project.project}"
            evaluateProject(project)
        }
        computeStatistics()
        computeBestResult()
        CsvUtil.write(outputFile, content)
    }

    private evaluateProject(project){
        Evaluator evaluator = new Evaluator()
        def keys = project.keySet()
        keys -= ["project"]
        def results = []
        keys.each { k ->
            def values
            if(k < 9) values = evaluator.extractF2(project[k])
            else if(k == 9 || k==10) values = evaluator.extractRandomF2(project[k])
            results.add([config: k, tasks: values.collect{it[0]}, f2: values.collect{it[1]}])
        }
        def tasks = results*.tasks.flatten().unique()
        def f2Values = results*.f2
        tasks.eachWithIndex{ task, i ->
            def v = [task]
            (0..9).each{ j ->
                v.add(f2Values[j][i])
            }
            content += ([project.project] + v.flatten()) as String[]
        }
    }

    private computeStatistics(){
        resultF2 = []
        def measures = content.subList(1, content.size())
        def mean = ["", "Mean"]
        def median = ["", "Median"]
        def sd = ["", "Standard deviation"]

        (2..11).each{ i ->
            double[] column = measures.collect { it[i] as double }
            def statistics = new DescriptiveStatistics(column)
            mean.add(statistics.mean as String)
            median.add(statistics.getPercentile(50.0) as String)
            sd.add(statistics.standardDeviation as String)
            resultF2 += [config: (i as int)-1, statistics:statistics]
        }
        content += mean as String[]
        content += median as String[]
        content += sd as String[]
    }

    private computeBestResult(){
        def bestF2 = resultF2.max{ it.statistics.mean }
        def allBestF2 = resultF2.findAll{ it.statistics.mean == bestF2.statistics.mean }*.config
        log.info "Best F2: ${allBestF2}"
        def aux =  bestF2.statistics as DescriptiveStatistics
        content += [""] as String[]
        content += ["", "Best F2", allBestF2] as String[]
        content += ["", "Mean F2", aux.mean as String] as String[]
        content += ["", "Median F2", aux.getPercentile(50.0) as String] as String[]
        content += ["", "SD F2", aux.standardDeviation as String] as String[]
    }

    static void main(String[] args){
        InputFilesManager inputFilesManager = new InputFilesManager()
        F2Evaluator f2Evaluator = new F2Evaluator(inputFilesManager)
        f2Evaluator.evaluate()

        BestEvaluatorBasedOnF2 bestEvaluatorBasedOnF2 = new BestEvaluatorBasedOnF2(inputFilesManager)
        bestEvaluatorBasedOnF2.evaluate()
    }

}
