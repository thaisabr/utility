package study1.result.configEvaluation.aggregated

import br.ufpe.cin.tan.util.CsvUtil
import groovy.util.logging.Slf4j
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import study1.result.configEvaluation.Evaluator
import study1.result.configEvaluation.InputFilesManager

/*
* Compara as diferentes configurações com base na quantidade de falsos positivos e falsos negativos.
* Por hora, IText não entra na comparação, só IRandom.
* */

@Slf4j
class FalsePositiveFalseNegativeEvaluator {

    InputFilesManager result
    def projects
    def outputFile
    List<String[]> content
    def measures
    def resultFp
    def resultFn

    FalsePositiveFalseNegativeEvaluator(InputFilesManager result){
        this.result = result
        projects = this.result.filesPerProject
        InputFilesManager.createFolder(Evaluator.INPUT_FOLDER)
        InputFilesManager.createFolder(Evaluator.OUTPUT_FOLDER)
        def outputFolder = "${Evaluator.EVALUATION_FOLDER}${File.separator}overview"
        outputFile = "${outputFolder}${File.separator}fp_fn.csv"
        InputFilesManager.createFolder(outputFolder)
        content = []
        resultFp = []
        resultFn = []
    }

    def evaluate(){
        log.info "<< EVALUATION OF FALSE POSITIVES AND FALSE NEGATIVES >>"
        content += ["Project","Task" ,"FP_1", "FN_1", "FP_2", "FN_2", "FP_3", "FN_3", "FP_4", "FN_4", "FP_5", "FN_5",
                    "FP_6", "FN_6", "FP_7", "FN_7", "FP_8", "FN_8", "FP_9", "FN_9", "FP_10", "FN_10"] as String[]
        projects?.each{ project ->
            log.info "Project: ${project.project}"
            evaluateProject(project)
        }
        computeStatistics()
        computeBestResult()
        CsvUtil.write(outputFile, content)
    }

    private evaluateProject(project){
        Evaluator evaluator = new Evaluator()
        def keys = project.keySet()
        keys -= ["project"]
        def results = []
        keys.each { k ->
            def values
            if(k < 9) values = evaluator.extractFpAndFn(project[k])
            else if (k==9 || k==10) values = evaluator.extractRandomFpAndFn(project[k])
            results.add([config: k, tasks: values.collect{it[0]}, precision: values.collect{it[1]}, recall:values.collect{it[2]}])

        }
        def tasks = results*.tasks.flatten().unique()
        def precisionValues = results*.precision
        def recallValues = results*.recall
        tasks.eachWithIndex{ task, i ->
            def v = [task]
            (0..9).each{ j ->
                v.add([precisionValues[j][i], recallValues[j][i]])
            }
            content += ([project.project] + v.flatten()) as String[]
        }
    }

    private computeStatistics(){
        resultFp = []
        resultFn = []
        def measures = content.subList(1, content.size())
        def mean = ["", "Mean"]
        def median = ["", "Median"]
        def sd = ["", "Standard deviation"]

        (2..21).each{ i->
            double[] column = measures.collect { it[i] as double }
            def statistics = new DescriptiveStatistics(column)
            mean.add(statistics.mean as String)
            median.add(statistics.getPercentile(50.0) as String)
            sd.add(statistics.standardDeviation as String)
            if(i%2 == 0) resultFp += [config: (i/2) as int, statistics:statistics]
            else resultFn += [config: (i/2) as int, statistics:statistics]
        }
        content += mean as String[]
        content += median as String[]
        content += sd as String[]
    }

    private computeBestResult(){
        def bestFp = resultFp.min{ it.statistics.mean }
        def allBestFp = resultFp.findAll{ it.statistics.mean == bestFp.statistics.mean }*.config
        log.info "Best false positives: ${allBestFp}"
        def aux =  bestFp.statistics as DescriptiveStatistics
        content += [""] as String[]
        content += ["", "Best false positives", allBestFp] as String[]
        content += ["", "Mean false positives", aux.mean as String] as String[]
        content += ["", "Median false positives", aux.getPercentile(50.0) as String] as String[]
        content += ["", "SD false positives", aux.standardDeviation as String] as String[]

        def bestFn = resultFn.min{ it.statistics.mean }
        def allBestFn = resultFn.findAll{ it.statistics.mean == bestFn.statistics.mean }*.config
        log.info "Best false negatives: ${allBestFn}"
        aux =  bestFn.statistics as DescriptiveStatistics
        content += [""] as String[]
        content += ["", "Best false negatives", allBestFn] as String[]
        content += ["", "Mean false negatives", aux.mean as String] as String[]
        content += ["", "Median false negatives", aux.getPercentile(50.0) as String] as String[]
        content += ["", "SD false negatives", aux.standardDeviation as String] as String[]
    }

}
