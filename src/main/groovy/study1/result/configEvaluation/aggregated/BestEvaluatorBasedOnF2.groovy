package study1.result.configEvaluation.aggregated

import br.ufpe.cin.tan.util.CsvUtil
import groovy.util.logging.Slf4j
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import study1.result.configEvaluation.Evaluator
import study1.result.configEvaluation.InputFilesManager

/*
* Avalia a melhor configuração de execução por projeto.
* A comparação é feita entre tarefas relevantes (usadas no estudo envolvendo ITestD).
* Por hora, IText está fora da comparação, mas IRandom entra.
* */

@Slf4j
class BestEvaluatorBasedOnF2 {
    InputFilesManager util
    def projects
    def outputFolder
    def results = []

    BestEvaluatorBasedOnF2(InputFilesManager util){
        this.util = util
        projects = this.util.filesPerProject
        outputFolder = Evaluator.INPUT_FOLDER
        InputFilesManager.createFolder(outputFolder)
    }

    def evaluate(){
        log.info "<< BEST RESULT EVALUATION (BASED ON F2) >>"
        projects.each{ project ->
            log.info "Project: ${project.project}"
            evaluateProject(project)
            summarizeData(project)
        }
    }

    private summarizeData(project){
        Evaluator evaluator = new Evaluator()
        def tasks1 = evaluator.extractF2(project[1])
        def tasks2 = evaluator.extractF2(project[2])
        def tasks3 = evaluator.extractF2(project[3])
        def tasks4 = evaluator.extractF2(project[4])
        def tasks5 = evaluator.extractF2(project[5])
        def tasks6 = evaluator.extractF2(project[6])
        def tasks7 = evaluator.extractF2(project[7])
        def tasks8 = evaluator.extractF2(project[8])
        def tasks9 = evaluator.extractRandomF2(project[9])
        def tasks10 = evaluator.extractRandomF2(project[10])

        List<String[]> content = []
        content += ["Task","1", "2", "3", "4", "5", "6", "7", "8", "9", "10"] as String[]
        def taskIds = tasks1.collect{it[0]}
        taskIds.eachWithIndex{ def id, int i ->
            content += [id, tasks1[i][1], tasks2[i][1], tasks3[i][1], tasks4[i][1], tasks5[i][1], tasks6[i][1],
                        tasks7[i][1], tasks8[i][1], tasks9[i][1], tasks10[i][1]] as String[]
        }

        def configResults = [tasks1, tasks2, tasks3, tasks4, tasks5, tasks6, tasks7, tasks8, tasks9, tasks10]
        def statistics = []
        configResults.each{ config ->
            double[] f2Values = config.collect { it[1] } as double[]
            def f2Stats = new DescriptiveStatistics(f2Values)
            statistics.add([f2Stats])
        }

        def mean = ["Mean"]
        (0..9).each{ j ->
            def statistic = statistics.get(j)
            mean.add(statistic[0].mean)
        }
        content += mean as String[]

        def median = ["Median"]
        (0..9).each{ j ->
            def statistic = statistics.get(j)
            median.add(statistic[0].getPercentile(50.0))
        }
        content += median as String[]

        def sd = ["Standard deviation"]
        (0..9).each{ j ->
            def statistic = statistics.get(j)
            sd.add(statistic[0].standardDeviation)
        }
        content += sd as String[]

        def file = "${Evaluator.OUTPUT_FOLDER}${File.separator}${project.project}_f2.csv"
        CsvUtil.write(file, content)
    }

    private extractResults(project){
        Evaluator evaluator = new Evaluator()
        results = []
        def keys = project.keySet()
        keys -= ["project"]
        keys.each{ k ->
            def values
            if(k < 9) values = evaluator.extractF2(project[k])
            else if(k == 9 || k==10) values = evaluator.extractRandomF2(project[k])
            double[] f2Values = values.collect { it[1] } as double[]
            def f2Stats = new DescriptiveStatistics(f2Values)
            if(f2Stats.mean!=Double.NaN){
                results.add([config:k, pMean:f2Stats.mean, pMedian: f2Stats.getPercentile(50.0),
                             psd:f2Stats.standardDeviation])
            }
        }
    }

    private evaluateProject(project){
        extractResults(project)

        def maxF2 = findMaxF2()

        List<String[]> content = []
        content += ["Best F2", "Mean", "Median", "Standard deviation"] as String[]
        maxF2.each{
            content += [it.config, it.pMean, it.pMedian, it.psd] as String[]
        }

        def overviewFile = "${Evaluator.OUTPUT_FOLDER}${File.separator}${project.project}_best_f2_overview.csv"
        CsvUtil.write(overviewFile, content)
    }

    private findMaxF2(){
        def max = -1
        results.each{
            if(it.pMean > max){
                max = it.pMean
            }
        }
        results.findAll{ it.pMean == max }
    }

}
