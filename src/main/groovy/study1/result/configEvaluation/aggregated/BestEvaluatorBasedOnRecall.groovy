package study1.result.configEvaluation.aggregated

import br.ufpe.cin.tan.util.CsvUtil
import groovy.util.logging.Slf4j
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import study1.result.configEvaluation.Evaluator
import study1.result.configEvaluation.InputFilesManager

/*
* Avalia a melhor configuração de execução por projeto.
* A comparação é feita entre tarefas relevantes (usadas no estudo envolvendo ITestD).
* Por hora, IText está fora da comparação, mas IRandom entra.
* */
@Slf4j
class BestEvaluatorBasedOnRecall {
    InputFilesManager util
    def projects
    def outputFolder
    def results = []

    BestEvaluatorBasedOnRecall(InputFilesManager util){
        this.util = util
        projects = this.util.filesPerProject
        outputFolder = Evaluator.INPUT_FOLDER
        InputFilesManager.createFolder(outputFolder)
    }

    def evaluate(){
        log.info "<< BEST RESULT EVALUATION (BASED ON PRECISION & RECALL) >>"
        projects.each{ project ->
            log.info "Project: ${project.project}"
            evaluateProject(project)
            summarizeData(project)
        }
    }

    private summarizeData(project){
        Evaluator evaluator = new Evaluator()
        def tasks1 = evaluator.extractPrecisionAndRecall(project[1])
        def tasks2 = evaluator.extractPrecisionAndRecall(project[2])
        def tasks3 = evaluator.extractPrecisionAndRecall(project[3])
        def tasks4 = evaluator.extractPrecisionAndRecall(project[4])
        def tasks5 = evaluator.extractPrecisionAndRecall(project[5])
        def tasks6 = evaluator.extractPrecisionAndRecall(project[6])
        def tasks7 = evaluator.extractPrecisionAndRecall(project[7])
        def tasks8 = evaluator.extractPrecisionAndRecall(project[8])
        def tasks9 = evaluator.extractRandomPrecisionAndRecall(project[9])
        println tasks9
        def tasks10 = evaluator.extractRandomPrecisionAndRecall(project[10])
        println tasks10

        List<String[]> content = []
        content += ["Task", "P_1", "R_1", "P_2", "R_2", "P_3", "R_3", "P_4", "R_4", "P_5", "R_5", "P_6", "R_6", "P_7",
                    "R_7", "P_8", "R_8", "P_9", "R_9", "P_10", "R_10"] as String[]
        def taskIds = tasks1.collect{it[0]}
        taskIds.eachWithIndex{ def id, int i ->
            content += [id, tasks1[i][1], tasks1[i][2], tasks2[i][1], tasks2[i][2], tasks3[i][1], tasks3[i][2],
                        tasks4[i][1], tasks4[i][2], tasks5[i][1], tasks5[i][2], tasks6[i][1], tasks6[i][2], tasks7[i][1],
                        tasks7[i][2], tasks8[i][1], tasks8[i][2], tasks9[i][1], tasks9[i][2], tasks10[i][1],
                        tasks10[i][2]] as String[]
        }

        def configResults = [tasks1, tasks2, tasks3, tasks4, tasks5, tasks6, tasks7, tasks8, tasks9, tasks10]
        def statistics = []
        configResults.each{ config ->
            double[] precision = config.collect { it[1] } as double[]
            def precisionStats = new DescriptiveStatistics(precision)
            double[] recall = config.collect { it[2] } as double[]
            def recallStats = new DescriptiveStatistics(recall)
            statistics.add([precisionStats, recallStats])
        }

        def mean = ["Mean"]
        (0..9).each{ j ->
            def statistic = statistics.get(j)
            mean.add(statistic[0].mean)
            mean.add(statistic[1].mean)
        }
        content += mean as String[]

        def median = ["Median"]
        (0..9).each{ j ->
            def statistic = statistics.get(j)
            median.add(statistic[0].getPercentile(50.0))
            median.add(statistic[1].getPercentile(50.0))
        }
        content += median as String[]

        def sd = ["Standard deviation"]
        (0..9).each{ j ->
            def statistic = statistics.get(j)
            sd.add(statistic[0].standardDeviation)
            sd.add(statistic[1].standardDeviation)
        }
        content += sd as String[]

        def file = "${Evaluator.OUTPUT_FOLDER}${File.separator}${project.project}_p&r.csv"
        CsvUtil.write(file, content)
    }

    private extractResults(project){
        Evaluator evaluator = new Evaluator()
        results = []
        def keys = project.keySet()
        keys -= ["project"]
        keys.each{ k ->
            def values
            if(k < 9) values = evaluator.extractPrecisionAndRecall(project[k])
            else if(k == 9 || k==10) values = evaluator.extractRandomPrecisionAndRecall(project[k])
            double[] precision = values.collect { it[1] } as double[]
            def precisionStats = new DescriptiveStatistics(precision)
            double[] recall = values.collect { it[2] } as double[]
            def recallStats = new DescriptiveStatistics(recall)
            if(precisionStats.mean!=Double.NaN && recallStats.mean!=Double.NaN){
                results.add([config:k, pMean:precisionStats.mean, pMedian: precisionStats.getPercentile(50.0),
                             psd:precisionStats.standardDeviation, rMean:recallStats.mean,
                             rMedian:recallStats.getPercentile(50.0), rsd:recallStats.standardDeviation])
            }
        }
    }

    private evaluateProject(project){
        extractResults(project)

        def maxPrecision = findMaxPrecision()
        def maxRecall = findMaxRecall()

        List<String[]> content = []
        content += ["Best precision", "Mean", "Median", "Standard deviation"] as String[]
        maxPrecision.each{
            content += [it.config, it.pMean, it.pMedian, it.psd] as String[]
        }

        content += ["Best recall", "Mean", "Median", "Standard deviation"] as String[]
        maxRecall.each{
            content += [it.config, it.rMean, it.rMedian, it.psd] as String[]
        }
        def overviewFile = "${Evaluator.OUTPUT_FOLDER}${File.separator}${project.project}_best_p&r_overview.csv"
        CsvUtil.write(overviewFile, content)
    }

    private findMaxPrecision(){
        def max = -1
        results.each{
            if(it.pMean > max){
                max = it.pMean
            }
        }
        results.findAll{ it.pMean == max }
    }

    private findMaxRecall(){
        def max = -1
        results.each{
            if(it.rMean > max){
                max = it.rMean
            }
        }
        results.findAll{ it.rMean == max }
    }

}
