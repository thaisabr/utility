package study1.result.configEvaluation.aggregated

import br.ufpe.cin.tan.util.CsvUtil
import groovy.util.logging.Slf4j
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import study1.result.configEvaluation.Evaluator
import study1.result.configEvaluation.InputFilesManager


@Slf4j
class AllPrecisionRecallEvaluator {

    InputFilesManager inputFilesManager
    def projects
    def outputFile
    List<String[]> content
    def measures
    def resultPrecision
    def resultRecall

    AllPrecisionRecallEvaluator(InputFilesManager inputFilesManager){
        this.inputFilesManager = inputFilesManager
        projects = this.inputFilesManager.filesPerProject
        InputFilesManager.createFolder(Evaluator.INPUT_FOLDER)
        InputFilesManager.createFolder(Evaluator.OUTPUT_FOLDER)
        def outputFolder = "${Evaluator.EVALUATION_FOLDER}${File.separator}overview"
        outputFile = "${outputFolder}${File.separator}precision_recall.csv"
        InputFilesManager.createFolder(outputFolder)
        content = []
        resultPrecision = []
        resultRecall = []
    }

    def evaluate(){
        log.info "<< ALL EVALUATION >>"
        content += ["Project","Task" ,"P_1", "R_1", "P_2", "R_2", "P_3", "R_3", "P_4", "R_4", "P_5", "R_5", "P_6", "R_6",
                    "P_7", "R_7", "P_8", "R_8", "P_9", "R_9", "P_10", "R_10"] as String[]
        projects?.each{ project ->
            log.info "Project: ${project.project}"
            evaluateProject(project)
        }
        computeStatistics()
        computeBestResult()
        CsvUtil.write(outputFile, content)
    }

    private evaluateProject(project){
        Evaluator evaluator = new Evaluator()
        def keys = project.keySet()
        keys -= ["project"]
        def results = []
        keys.each { k ->
            def values
            if(k < 9) values = evaluator.extractPrecisionAndRecall(project[k])
            else if(k == 9 || k==10) values = evaluator.extractRandomPrecisionAndRecall(project[k])
            results.add([config: k, tasks: values.collect{it[0]}, precision: values.collect{it[1]}, recall:values.collect{it[2]}])

        }
        def tasks = results*.tasks.flatten().unique()
        def precisionValues = results*.precision
        def recallValues = results*.recall
        tasks.eachWithIndex{ task, i ->
            def v = [task]
            (0..9).each{ j ->
                v.add([precisionValues[j][i], recallValues[j][i]])
            }
            content += ([project.project] + v.flatten()) as String[]
        }
    }

    private computeStatistics(){
        resultPrecision = []
        resultRecall = []
        def measures = content.subList(1, content.size())
        def mean = ["", "Mean"]
        def median = ["", "Median"]
        def sd = ["", "Standard deviation"]

        (2..21).each{ i->
            double[] column = measures.collect { it[i] as double }
            println "column: ${column.size()}"
            column.each{ println it }
            def statistics = new DescriptiveStatistics(column)
            mean.add(statistics.mean as String)
            median.add(statistics.getPercentile(50.0) as String)
            sd.add(statistics.standardDeviation as String)
            if(i%2 == 0) resultPrecision += [config: (i/2) as int, statistics:statistics]
            else resultRecall += [config: (i/2) as int, statistics:statistics]
        }
        content += mean as String[]
        content += median as String[]
        content += sd as String[]
    }

    private computeBestResult(){
        def bestPrecision = resultPrecision.max{ it.statistics.mean }
        def allBestPrecision = resultPrecision.findAll{ it.statistics.mean == bestPrecision.statistics.mean }*.config
        log.info "Best ${Evaluator.PRECISION_TEXT}: ${allBestPrecision}"
        def aux =  bestPrecision.statistics as DescriptiveStatistics
        content += [""] as String[]
        content += ["", "Best ${Evaluator.PRECISION_TEXT}", allBestPrecision] as String[]
        content += ["", "Mean ${Evaluator.PRECISION_TEXT}", aux.mean as String] as String[]
        content += ["", "Median ${Evaluator.PRECISION_TEXT}", aux.getPercentile(50.0) as String] as String[]
        content += ["", "SD ${Evaluator.PRECISION_TEXT}", aux.standardDeviation as String] as String[]

        def bestRecall = resultRecall.max{ it.statistics.mean }
        def allBestRecall = resultRecall.findAll{ it.statistics.mean == bestRecall.statistics.mean }*.config
        log.info "Best ${Evaluator.RECALL_TEXT}: ${allBestRecall}"
        aux =  bestRecall.statistics as DescriptiveStatistics
        content += [""] as String[]
        content += ["", "Best ${Evaluator.RECALL_TEXT}", allBestRecall] as String[]
        content += ["", "Mean ${Evaluator.RECALL_TEXT}", aux.mean as String] as String[]
        content += ["", "Median ${Evaluator.RECALL_TEXT}", aux.getPercentile(50.0) as String] as String[]
        content += ["", "SD ${Evaluator.RECALL_TEXT}", aux.standardDeviation as String] as String[]
    }

}
