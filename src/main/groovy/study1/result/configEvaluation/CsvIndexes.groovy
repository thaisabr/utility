package study1.result.configEvaluation

class CsvIndexes {

    public static final int TASK_ID = 0
    public static final int GHERKIN_TESTS_N = 5
    public static final int STEP_DEFS_N = 6
    public static final int ITEST_SIZE = 7
    public static final int IREAL_SIZE = 8
    public static final int PRECISION = 11
    public static final int RECALL = 12
    public static final int LOST_VISIT_CALLS_N = 14
    public static final int VIEWS_ITEST_N = 16
    public static final int VIEWS_ANALYSIS_CODE_N = 17
    public static final int NOT_FOUND_VIEWS_N = 23

    public static final int FALSE_POSITIVES_SIZE = 26
    public static final int FALSE_NEGATIVES_SIZE = 27
    public static final int HITS_SIZE = 30
    public static final int F2_MEASURE = 32

    public static final int HEADER_SIZE = 13
    public static final int RANDOM_HEADER_SIZE = 11

    public static final int RANDOM_INTERFACE = 3 //não faz mais sentido usar

    public static final int RANDOM_PRECISION_INDEX = 1
    public static final int RANDOM_RECALL_INDEX = 2
    public static final int IRANDOM_SIZE = 3
    public static final int RANDOM_IREAL_SIZE = 4
    public static final int RANDOM_FALSE_POSITIVES_SIZE = 5
    public static final int RANDOM_FALSE_NEGATIVES_SIZE = 6
    public static final int RANDOM_F2_MEASURE = 8

    public static final String CSV_EXT = ".csv"

    public static final int SIMILARITY_HEADER_SIZE = 4
    public static final int TEXT_SIMILARITY_INDEX = 2
    public static final int TEST_JACCARD_INDEX = 3
    public static final int REAL_JACCARD_INDEX = 4
    public static final int TEST_COSINE_INDEX = 5
    public static final int REAL_COSINE_INDEX = 6
}
