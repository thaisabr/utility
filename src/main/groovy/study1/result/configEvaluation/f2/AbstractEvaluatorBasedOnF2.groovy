package study1.result.configEvaluation.f2

import br.ufpe.cin.tan.util.CsvUtil
import groovy.util.logging.Slf4j
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import study1.result.configEvaluation.Evaluator
import study1.result.configEvaluation.InputFilesManager

@Slf4j
abstract class AbstractEvaluatorBasedOnF2 {

    protected InputFilesManager inputFilesManager
    protected projects
    protected outputFolder
    protected aggregatedOutputFolder
    protected projectsResult
    protected projectsDetailedResult
    protected bufferResult
    protected aggregatedResult
    protected String title
    protected indexes

    AbstractEvaluatorBasedOnF2(InputFilesManager inputFilesManager, String outputFolderName, String title, def indexes){
        this.inputFilesManager = inputFilesManager
        this.title = title.toLowerCase()
        this.indexes = indexes
        projects = this.inputFilesManager.filesPerProject
        InputFilesManager.createFolder(Evaluator.INPUT_FOLDER)
        outputFolder = "${Evaluator.EVALUATION_FOLDER}${File.separator}${outputFolderName}"
        aggregatedOutputFolder = "$outputFolder${File.separator}aggregated"
        InputFilesManager.createFolder(outputFolder)
        InputFilesManager.createFolder(aggregatedOutputFolder)
        cleanEvaluationResult()
    }

    def cleanEvaluationResult(){
        projectsResult = []
        projectsDetailedResult = []
        bufferResult = []
        aggregatedResult = []
    }

    def evaluate(){
        cleanEvaluationResult()

        log.info "<< ${title.toUpperCase()} - INDIVIDUAL EVALUATION (BASED ON F2) >>"
        projects?.each{ project ->
            log.info "Project: ${project.project}"
            evaluateProject(project)
        }

        log.info "<< ${title.toUpperCase()} - AGGREGATED EVALUATION (BASED ON F2) >>"
        generateAggregatedResult()
        evaluateAggregatedResult()
        compareAllProjects()
    }

    def evaluateProject(project){
        Evaluator evaluator1 = new Evaluator(project[indexes.r1[0]], project[indexes.r1[1]],
                "${outputFolder}${File.separator}${project.project}_${indexes.r1[0]}&${indexes.r1[1]}.csv")
        def result1 = evaluator1.compareF2()

        Evaluator evaluator2 = new Evaluator(project[indexes.r2[0]], project[indexes.r2[1]],
                "${outputFolder}${File.separator}${project.project}_${indexes.r2[0]}&${indexes.r2[1]}.csv")
        def result2 = evaluator2.compareF2()

        Evaluator evaluator3 = new Evaluator(project[indexes.r3[0]], project[indexes.r3[1]],
                "${outputFolder}${File.separator}${project.project}_${indexes.r3[0]}&${indexes.r3[1]}.csv")
        def result3 = evaluator3.compareF2()

        Evaluator evaluator4 = new Evaluator(project[indexes.r4[0]], project[indexes.r4[1]],
                "${outputFolder}${File.separator}${project.project}_${indexes.r4[0]}&${indexes.r4[1]}.csv")
        def result4 = evaluator4.compareF2()

        def results = [result1, result2, result3, result4]
        def f2 = [result1.f2, result2.f2, result3.f2, result4.f2]
        def f2Result = [equals:f2.count{it.contains("equals")},
                       yes:f2.count{it.contains("yes")},
                       no:f2.count{it.contains("no")}]
        def f2MaxResult = f2Result.max{it.value}.key

        saveProjectEvaluationResult(project, results, f2MaxResult, f2Result)
        bufferResult += [project: project.project, (indexes.r1.join()): result1.result, (indexes.r2.join()): result2.result,
          (indexes.r3.join()): result3.result, (indexes.r4.join()): result4.result]
        projectsDetailedResult += [project:project.project, f2Evaluation: f2Result,
          f2: [(indexes.r1.join()):result1.f2, (indexes.r2.join()):result2.f2 ,(indexes.r3.join()):result3.f2, (indexes.r4.join()): result4.f2]
        ]
        projectsResult += [project:project.project, f2:f2Result]
    }

    def compareAllProjects(){
        def groups = (projectsResult*.project).unique()
        def resultByProject = []
        groups.each{ group ->
            def values = projectsResult.findAll{ it.project == group }
            def f2Equals = values.collect{ it.f2.equals as int }.sum()
            def f2Yes = values.collect{ it.f2.yes as int }.sum()
            def f2No = values.collect{ it.f2.no as int }.sum()
            resultByProject += [project:group, f2:[equals:f2Equals, yes:f2Yes, no:f2No]]
        }

        def f2 = resultByProject*.f2
        def f2Result = [equals:(f2.collect{ it.equals as int }.sum()),
                       yes:(f2.collect{ it.yes as int }.sum()),
                       no:(f2.collect{ it.no as int }.sum())]
        def f2MaxResult = f2Result.max{it.value}.key

        saveAggregatedData(f2MaxResult,f2Result, groups, resultByProject)
    }

    def generateAggregatedResult(){
        if(bufferResult.empty) return

        def keys = (bufferResult.get(0).keySet() + bufferResult.get(1).keySet()).unique()
        def configs = keys - ["project"]
        configs.each{ config ->
            def aux = config.toString()
            log.info "Configuration: ${aux.getAt(0)}&${aux.substring(1)}"
            def values = bufferResult.findAll{ it.containsKey(config) }
            values = values.collect{ [project: it.project, (config):it[config]] }
            values.each{ v ->
                def tasks = v[config]
                tasks.each{ task ->
                    aggregatedResult += [project: v.project, conf:config, id: task.id, f2_1:task.f2_1, f2_2:task.f2_2]
                }
            }
        }
    }

    def evaluateAggregatedResult(){
        def configurations = aggregatedResult.collect{ it.conf }.unique()
        configurations.each{ config ->
            def values = aggregatedResult.findAll{ it.conf == config }

            double[] f21 = values.collect { it.f2_1 } as double[]
            def f2Stats1 = new DescriptiveStatistics(f21)
            def resultf21 = [mean:f2Stats1.mean, median:f2Stats1.getPercentile(50.0), sd:f2Stats1.standardDeviation]

            double[] f22 = values.collect { it.f2_2 } as double[]
            def f2Stats2 = new DescriptiveStatistics(f22)
            def resultf22 = [mean:f2Stats2.mean, median:f2Stats2.getPercentile(50.0), sd:f2Stats2.standardDeviation]

            def f2Evaluation = Evaluator.compare(f2Stats1.mean, f2Stats2.mean)

            def configStatistics = [config:config, f2_1:resultf21, f2_2:resultf22, f2Evaluation: f2Evaluation]
            saveAggregatedResult(values, configStatistics)
        }
    }

    def saveAggregatedResult(values, configStatistics){
        def aux = configStatistics.config.toString()
        def configName = aux.getAt(0)+"&" + aux.substring(1)
        def filename = "$aggregatedOutputFolder${File.separator}${title}_overview_aggregated_${configName}.csv"

        List<String[]> content = []
        content += ["PROJECT", "TASK_ID", "F2_1", "F2_2"] as String[]

        values.each{ value ->
            content += [value.project, value.id, value.f2_1, value.f2_2] as String[]
        }

        content += ["(1) F2 mean", configStatistics.f2_1.mean] as String[]
        content += ["(1) F2 median", configStatistics.f2_1.median] as String[]
        content += ["(1) F2 sd", configStatistics.f2_1.sd] as String[]
        content += ["(2) F2 mean", configStatistics.f2_2.mean] as String[]
        content += ["(2) F2 median", configStatistics.f2_2.median] as String[]
        content += ["(2) F2 sd", configStatistics.f2_2.sd] as String[]
        content += ["Improved mean F2", configStatistics.f2Evaluation] as String[]

        CsvUtil.write(filename, content)
    }

    def saveAggregatedData(f2MaxResult, f2Result, groups, resultByProject){
        List<String[]> content = []
        content += generateF2Data(f2MaxResult, f2Result, groups, resultByProject)
        def overviewFile = "$aggregatedOutputFolder${File.separator}${title}_overview_byProject.csv"
        CsvUtil.write(overviewFile, content)
    }

    def generateF2Data(f2MaxResult, f2Result, groups, resultByProject){
        List<String[]> content = []
        content += ["${title} improved mean F2 (all projects, each 4 configurations):",
                    "${f2MaxResult} (${f2Result})"] as String[]
        groups.each{ group ->
            def evaluation = resultByProject.find{ it.project == group }
            content += [evaluation.project, evaluation.f2.toString()] as String[]
            def detailedResults = projectsDetailedResult.find{ it.project == group }
            def drF2List = detailedResults.f2
            drF2List.each{ drf2 ->
                def aux = drf2.key.toString()
                content += ["${aux.getAt(0)}&${aux.substring(1)}", drf2.value] as String[]
            }
        }
        content
    }

    def saveProjectEvaluationResult(project, results, f2MaxResult, f2Result){
        List<String[]> content = []
        content += ["${title} improved mean F2:", "${f2MaxResult} (${f2Result})"] as String[]
        content += ["(${indexes.r1[0]},${indexes.r1[1]})", results[0]?.f2] as String[]
        content += ["(${indexes.r2[0]},${indexes.r2[1]})", results[1]?.f2] as String[]
        content += ["(${indexes.r3[0]},${indexes.r3[1]})", results[2]?.f2] as String[]
        content += ["(${indexes.r4[0]},${indexes.r4[1]})", results[3]?.f2] as String[]

        def overviewFile = "${outputFolder}${File.separator}${project.project}_${title}_overview.csv"
        CsvUtil.write(overviewFile, content)
    }

}
