package study1.result.configEvaluation.f2

import groovy.util.logging.Slf4j
import study1.result.configEvaluation.InputFilesManager

/*
* Avalia se filtrar por controller é vantajoso ou não.
* A comparação é feita entre tarefas relevantes (usadas no estudo envolvendo ITestD).
* */

@Slf4j
class ControllerEvaluatorBasedOnF2 extends AbstractEvaluatorBasedOnF2 {

    ControllerEvaluatorBasedOnF2(InputFilesManager inputFilesManager){
        super(inputFilesManager, "controllerEvaluationF2", "CONTROLLER", [r1:[1,3], r2:[2,4], r3:[5,7], r4:[6,8]])
    }

}
