package study1.result.configEvaluation.f2

import br.ufpe.cin.tan.util.CsvUtil
import groovy.util.logging.Slf4j
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import study1.result.configEvaluation.Evaluator
import study1.result.configEvaluation.InputFilesManager

/*
* Avalia se o resultado randômico consegue ser melhor que ITestS, em suas diferentes configurações.
* A comparação é feita entre tarefas relevantes (usadas no estudo envolvendo ITestD).
* Há 2 estratégias para calcular IRandom:
*    (i) sortear o tamanho de IRandom e seu conteúdo;
*    (ii) sortear se cada arquivo candidato será incluído em IRandom ou não.
* Para a avaliação rodar, o arquivo com IRandom deve estar na pasta "results/output_all".
* */

@Slf4j
class RandomEvaluatorBasedOnF2 extends AbstractEvaluatorBasedOnF2 {

    RandomEvaluatorBasedOnF2(InputFilesManager inputFilesManager){
        super(inputFilesManager, "randomEvaluationF2", "RANDOM",
                [r1:[1,9], r2:[2,9], r3:[5,9], r4:[6,9], r5:[3,10], r6:[4,10], r7:[7,10], r8:[8,10]])
    }

    RandomEvaluatorBasedOnF2(InputFilesManager inputFilesManager, String outputFolderName, String title, def indexes){
        super(inputFilesManager, outputFolderName, title, indexes)
    }

    @Override
    def evaluate(){
        cleanEvaluationResult()

        log.info "<< ${title.toUpperCase()} - INDIVIDUAL EVALUATION (BASED ON F2) >>"
        projects.each{ project ->
            log.info "Project: ${project.project}"
            evaluateProject(project)
            evaluateProjectController(project)
        }

        log.info "<< ${title.toUpperCase()} - AGGREGATED EVALUATION (BASED ON F2) >>"
        generateAggregatedResult()
        evaluateAggregatedResult()
        compareAllProjects()
    }

    @Override
    def evaluateProject(project){
        Evaluator evaluator1 = new Evaluator(project[indexes.r1[0]], project[indexes.r1[1]],
                "${outputFolder}${File.separator}${project.project}_${indexes.r1[0]}&${indexes.r1[1]}.csv")
        def result1 = evaluator1.compareRandomF2()

        Evaluator evaluator2 = new Evaluator(project[indexes.r2[0]], project[indexes.r2[1]],
                "${outputFolder}${File.separator}${project.project}_${indexes.r2[0]}&${indexes.r2[1]}.csv")
        def result2 = evaluator2.compareRandomF2()

        Evaluator evaluator3 = new Evaluator(project[indexes.r3[0]], project[indexes.r3[1]],
                "${outputFolder}${File.separator}${project.project}_${indexes.r3[0]}&${indexes.r3[1]}.csv")
        def result3 = evaluator3.compareRandomF2()

        Evaluator evaluator4 = new Evaluator(project[indexes.r4[0]], project[indexes.r4[1]],
                "${outputFolder}${File.separator}${project.project}_${indexes.r4[0]}&${indexes.r4[1]}.csv")
        def result4 = evaluator4.compareRandomF2()

        if(!result1 || !result2 || !result3 || !result4) return

        def results = [result1, result2, result3, result4]
        def f2 = [result1.f2, result2.f2, result3.f2, result4.f2]
        def f2Result = [equals:f2.count{it.contains("equals")},
                       yes:f2.count{it.contains("yes")},
                       no:f2.count{it.contains("no")}]
        def f2MaxResult = f2Result.max{it.value}.key

        saveProjectEvaluationResult(project, results, f2MaxResult, f2Result)
        bufferResult += [project: project.project, (indexes.r1.join()): result1.result, (indexes.r2.join()): result2.result,
           (indexes.r3.join()): result3.result, (indexes.r4.join()): result4.result]
        projectsDetailedResult += [project:project.project, f2Evaluation: f2Result,
            f2: [(indexes.r1.join()):result1.f2, (indexes.r2.join()):result2.f2 ,(indexes.r3.join()):result3.f2,
                 (indexes.r4.join()): result4.f2]
        ]
        projectsResult += [project:project.project, f2:f2Result]
    }

    @Override
    def generateF2Data(f2MaxResult, f2Result, groups, resultByProject){
        List<String[]> content = []
        content += ["${title} improved mean F2 (all projects, each 8 configurations):",
                    "${f2MaxResult} (${f2Result})"] as String[]
        groups.each{ group ->
            def evaluation = resultByProject.find{ it.project == group }
            content += [evaluation.project, evaluation.f2.toString()] as String[]
            def detailedResults = projectsDetailedResult.findAll{ it.project == group }
            def drF2 = (detailedResults*.f2).flatten()
            def drF2List = drF2.get(0) + drF2.get(1)
            drF2List.each{ drf2 ->
                def aux = drf2.key.toString()
                content += ["${aux.getAt(0)}&${aux.substring(1)}", drf2.value] as String[]
            }
        }
        content
    }

    def evaluateAggregatedResult(){
        def result = []
        def configurations = aggregatedResult.collect{ it.conf }.unique()
        configurations.each{ config ->
            def values = aggregatedResult.findAll{ it.conf == config }
            double[] f21 = values.collect { it.f2_1 } as double[]
            def f2Stats1 = new DescriptiveStatistics(f21)
            def resultF21 = [mean:f2Stats1.mean, median:f2Stats1.getPercentile(50.0), sd:f2Stats1.standardDeviation]

            double[] f22 = values.collect { it.f2_2 } as double[]
            def f2Stats2 = new DescriptiveStatistics(f22)
            def resultF22 = [mean:f2Stats2.mean, median:f2Stats2.getPercentile(50.0), sd:f2Stats2.standardDeviation]

            def f2Evaluation = Evaluator.compare(f2Stats1.mean, f2Stats2.mean)

            def configStatistics = [config:config, f2_1:resultF21, f2_2:resultF22, f2Evaluation: f2Evaluation]
            saveAggregatedResult(values, configStatistics)
            result += configStatistics
        }

        saveAggregatedEvaluation(result)
    }

    protected saveAggregatedEvaluation(result){
        def overviewFile = "$aggregatedOutputFolder${File.separator}random_overview_aggregated.csv"
        List<String[]> content = []
        content += ["CONFIGURATION", "F2 (ITestS)", "", "", "F2 (IRandom)", "", "", "F2 EVALUATION"] as String[]
        content += ["", "MEAN", "MEDIAN", "SD", "MEAN", "MEDIAN", "SD", ""] as String[]
        result.each {
            def aux = it.config.toString()
            content += ["${aux.getAt(0)}&${aux.substring(1)}",
                        it.f2_1.mean, it.f2_1.median, it.f2_1.sd,
                        it.f2_2.mean, it.f2_2.median, it.f2_2.sd,
                        it.f2Evaluation
            ] as String[]
        }
        CsvUtil.write(overviewFile, content)
    }

    protected saveControllerProjectEvalulationResult(project, results, f2MaxResult, f2Result){
        List<String[]> content = []
        content += ["${title} controller improved mean F2", "${f2MaxResult} (${f2Result})"] as String[]
        content += ["(3,10)", results[0]?.f2] as String[]
        content += ["(4,10)", results[1]?.f2] as String[]
        content += ["(7,10)", results[2]?.f2] as String[]
        content += ["(8,10)", results[3]?.f2] as String[]
        def overviewFile = "${outputFolder}${File.separator}${project.project}_${title}_overview.csv"
        CsvUtil.append(overviewFile, content)
    }

    protected evaluateProjectController(project){
        Evaluator reader1 = new Evaluator(project[indexes.r5[0]], project[indexes.r5[1]],
                "${outputFolder}${File.separator}${project.project}_3&10.csv")
        def result1 = reader1.compareRandomF2()

        Evaluator reader2 = new Evaluator(project[indexes.r6[0]], project[indexes.r6[1]],
                "${outputFolder}${File.separator}${project.project}_4&10.csv")
        def result2 = reader2.compareRandomF2()

        Evaluator reader3 = new Evaluator(project[indexes.r7[0]], project[indexes.r7[1]],
                "${outputFolder}${File.separator}${project.project}_7&10.csv")
        def result3 = reader3.compareRandomF2()

        Evaluator reader4 = new Evaluator(project[indexes.r8[0]], project[indexes.r8[1]],
                "${outputFolder}${File.separator}${project.project}_8&10.csv")
        def result4 = reader4.compareRandomF2()

        if(!result1 || !result2 || !result3 || !result4) return

        def results = [result1, result2, result3, result4]
        def f2 = [result1.f2, result2.f2, result3.f2, result4.f2]
        def f2Result = [equals:f2.count{it.contains("equals")},
                       yes:f2.count{it.contains("yes")},
                       no:f2.count{it.contains("no")}]
        def f2MaxResult = f2Result.max{it.value}.key

        saveControllerProjectEvalulationResult(project, results, f2MaxResult, f2Result)
        bufferResult += [project: project.project, (indexes.r5.join()): result1.result, (indexes.r6.join()): result2.result,
                         (indexes.r7.join()): result3.result, (indexes.r8.join()): result4.result]
        projectsDetailedResult += [project:project.project, f2Evaluation: f2Result,
            f2: [(indexes.r5.join()): result1.f2, (indexes.r6.join()): result2.f2, (indexes.r7.join()): result3.f2,
                 (indexes.r8.join()): result4.f2]
        ]
        projectsResult += [project:project.project, f2:f2Result]
    }

}
