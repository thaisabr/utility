package study1.result.configEvaluation.f2

import groovy.util.logging.Slf4j
import study1.result.configEvaluation.InputFilesManager

/*
* Avalia se filtrar por added scenarios é vantajoso ou não.
* A comparação é feita entre tarefas relevantes (usadas no estudo envolvendo ITestD).
* */

@Slf4j
class AddedEvaluatorBasedOnF2 extends AbstractEvaluatorBasedOnF2 {

    AddedEvaluatorBasedOnF2(InputFilesManager inputFilesManager){
        super(inputFilesManager, "addedEvaluationF2", "ADDED", [r1:[1,2], r2:[3,4], r3:[5,6], r4:[7,8]])
    }

}
