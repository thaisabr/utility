package study1.result.configEvaluation.f2

import groovy.util.logging.Slf4j
import study1.result.configEvaluation.InputFilesManager
import study1.result.configEvaluation.precision_recall.RandomEvaluator

/*
* (esse código ainda não foi executado nenhuma vez, precisa ser validado!)
*
* Avalia se IText consegue ser melhor que ITestS, em suas diferentes configurações.
* A comparação é feita entre tarefas relevantes (usadas no estudo envolvendo ITestD).
*
* A estratégia para calcular IText é definida em text.IText. Ela consiste no cálculo da interseção de IReal entre
* as 5 tarefas mais parecidas textualmente.
*
* */

@Slf4j
class TextEvaluatorBasedOnF2 extends RandomEvaluatorBasedOnF2 {

    TextEvaluatorBasedOnF2(InputFilesManager inputFilesManager){
        super(inputFilesManager, "textEvaluationF2", "TEXT",
                [r1:[1,11], r2:[2,12], r3:[5,11], r4:[6,12], r5:[3,13], r6:[4,14], r7:[7,13], r8:[8,14]])
    }

}
