package study1.result

import groovy.util.logging.Slf4j
import study1.result.configEvaluation.f2.AddedEvaluatorBasedOnF2
import study1.result.configEvaluation.f2.ControllerEvaluatorBasedOnF2
import study1.result.configEvaluation.f2.RandomEvaluatorBasedOnF2
import study1.result.configEvaluation.f2.WhenEvaluatorBasedOnF2
import study1.result.configEvaluation.precision_recall.AddedEvaluator
import study1.result.configEvaluation.aggregated.AllPrecisionRecallEvaluator
import study1.result.configEvaluation.aggregated.BestEvaluatorBasedOnF2
import study1.result.configEvaluation.aggregated.BestEvaluatorBasedOnRecall
import study1.result.configEvaluation.precision_recall.ControllerEvaluator
import study1.result.configEvaluation.aggregated.F2Evaluator
import study1.result.configEvaluation.aggregated.FalsePositiveFalseNegativeEvaluator
import study1.result.configEvaluation.precision_recall.RandomEvaluator
import study1.result.configEvaluation.InputFilesManager
import study1.result.configEvaluation.precision_recall.TextEvaluator
import study1.result.configEvaluation.precision_recall.WhenEvaluator
import study1.result.correlation.AllITestSizeEvaluator
import study1.result.correlation.AllTestQuantityEvaluator
import study1.result.correlation.CorrelationManager
import study1.result.correlation.ITestSizeEvaluator
import study1.result.correlation.TestQuantityEvaluator

@Slf4j
class Main {

    static void main(String[] args){
        /*def folders = ["results${File.separator}sample4${File.separator}random1-calculatedRoutes",
                       "results${File.separator}sample4${File.separator}random1-RailsRoutes",
                       "results${File.separator}sample4${File.separator}random2-calculatedRoutes",
                       "results${File.separator}sample4${File.separator}random2-RailsRoutes"]*/

        def folders = ["results${File.separator}sample5"]
        folders.each{ folder ->
            InputFilesManager inputFilesManager = new InputFilesManager(folder)

            inputFilesManager.filesPerProject.each{
                log.info it.toString()
            }

            //RandomEvaluator randomEvaluator = new RandomEvaluator(inputFilesManager)
            //randomEvaluator.evaluate()

            AddedEvaluator addedEvaluator = new AddedEvaluator(inputFilesManager)
            addedEvaluator.evaluate()

            ControllerEvaluator controllerEvaluator = new ControllerEvaluator(inputFilesManager)
            controllerEvaluator.evaluate()

            WhenEvaluator whenEvaluator = new WhenEvaluator(inputFilesManager)
            whenEvaluator.evaluate()

            BestEvaluatorBasedOnRecall bestEvaluatorBasedOnRecall = new BestEvaluatorBasedOnRecall(inputFilesManager)
            bestEvaluatorBasedOnRecall.evaluate()

            //RandomEvaluatorBasedOnF2 randomEvaluatorBasedOnF2 = new RandomEvaluatorBasedOnF2(inputFilesManager)
            //randomEvaluatorBasedOnF2.evaluate()

            AddedEvaluatorBasedOnF2 addedEvaluatorBasedOnF2 = new AddedEvaluatorBasedOnF2(inputFilesManager)
            addedEvaluatorBasedOnF2.evaluate()

            ControllerEvaluatorBasedOnF2 controllerEvaluatorBasedOnF2 = new ControllerEvaluatorBasedOnF2(inputFilesManager)
            controllerEvaluatorBasedOnF2.evaluate()

            WhenEvaluatorBasedOnF2 whenEvaluatorBasedOnF2 = new WhenEvaluatorBasedOnF2(inputFilesManager)
            whenEvaluatorBasedOnF2.evaluate()

            BestEvaluatorBasedOnF2 bestEvaluatorBasedOnF2 = new BestEvaluatorBasedOnF2(inputFilesManager)
            bestEvaluatorBasedOnF2.evaluate()

            CorrelationManager correlationManager = new CorrelationManager(inputFilesManager)
            correlationManager.evaluate()

            TestQuantityEvaluator testQuantityEvaluator = new TestQuantityEvaluator(inputFilesManager)
            testQuantityEvaluator.evaluate()

            ITestSizeEvaluator iTestSizeEvaluator = new ITestSizeEvaluator(inputFilesManager)
            iTestSizeEvaluator.evaluate()

            AllPrecisionRecallEvaluator allEvaluator = new AllPrecisionRecallEvaluator(inputFilesManager)
            allEvaluator.evaluate()

            FalsePositiveFalseNegativeEvaluator evaluator = new FalsePositiveFalseNegativeEvaluator(inputFilesManager)
            evaluator.evaluate()

            F2Evaluator f2Evaluator = new F2Evaluator(inputFilesManager)
            f2Evaluator.evaluate()

            AllTestQuantityEvaluator allTestQuantityEvaluator = new AllTestQuantityEvaluator(inputFilesManager)
            allTestQuantityEvaluator.evaluate()

            AllITestSizeEvaluator allITestSizeEvaluator = new AllITestSizeEvaluator(inputFilesManager)
            allITestSizeEvaluator.evaluate()

            //TextEvaluator textEvaluator = new TextEvaluator(inputFilesManager)
            //textEvaluator.evaluate()

            /*ViewsEvaluator viewsEvaluator = new ViewsEvaluator("compare${File.separator}old",
                    "compare${File.separator}new")
            viewsEvaluator.evaluateProject()*/

        }

    }

}
