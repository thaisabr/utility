package study1.result.similarity

import br.ufpe.cin.tan.util.Util
import groovy.util.logging.Slf4j
import study1.result.configEvaluation.Evaluator


@Slf4j
class SimilarityManager {

    def files

    SimilarityManager(def folder){
        files = Util.findFilesFromDirectory(folder)
    }

    def generateSimilarityMeasures(){
        log.info "<< COMPUTING SIMILARITY >>"
        files.each{ file ->
            log.info "File: ${file}"
            Evaluator evaluator = new Evaluator()
            evaluator.replaceMeasures(file)
        }
    }

    static void main(String[] args){
        SimilarityManager similarityManager = new SimilarityManager("results${File.separator}random")
        similarityManager.generateSimilarityMeasures()
    }

}
