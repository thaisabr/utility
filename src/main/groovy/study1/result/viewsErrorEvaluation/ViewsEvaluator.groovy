package study1.result.viewsErrorEvaluation

import br.ufpe.cin.tan.util.CsvUtil
import groovy.util.logging.Slf4j
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import study1.result.configEvaluation.CsvIndexes
import study1.result.configEvaluation.Evaluator
import study1.result.configEvaluation.InputFilesManager

/*
* Comparar correções feitas em análise de views.
* Os arquivos a serem comparados devem estar nas pastas "compare/new" e "compare/old".
* */
@Slf4j
class ViewsEvaluator {

    InputFilesManager resultOld
    InputFilesManager resultNew
    def outputFolder
    def oldProjects
    def newProjects
    def content

    ViewsEvaluator(String folderOld, String folderNew){
        resultOld = new InputFilesManager(folderOld)
        resultNew = new InputFilesManager(folderNew)
        oldProjects = resultOld.filesPerProject
        newProjects = resultNew.filesPerProject
        InputFilesManager.createFolder(Evaluator.INPUT_FOLDER)
        outputFolder = "${Evaluator.EVALUATION_FOLDER}${File.separator}viewsEvaluation"
        InputFilesManager.createFolder(outputFolder)
        content = []
    }

    def evaluateProject() {
        log.info "<< VIEWS EVALUATION >>"
        newProjects.eachWithIndex{ newProject, index ->
            log.info "Project: ${newProject.project}"
            def oldProject = oldProjects.get(index)
            log.info "old file: ${oldProject[1]}"
            log.info "new file: ${newProject[1]}"
            def name = "$outputFolder${File.separator}${newProject.project}${CsvIndexes.CSV_EXT}"
            def evaluator = new Evaluator(oldProject[1], newProject[1], name)
            content += evaluator.compareViews()
        }
        computeStatistics()
    }

    private computeStatistics(){
        double[] viewsCsv1 = content.collect{ it.oldViews as double[] }.flatten() as double[]
        def viewsStats1 = new DescriptiveStatistics(viewsCsv1)
        double[] viewsCsv2 = content.collect{ it.newViews as double[] }.flatten() as double[]
        def viewsStats2 = new DescriptiveStatistics(viewsCsv2)
        double[] precisionCsv1 = content.collect{ it.oldPrecision as double[] }.flatten() as double[]
        def precisionStats1 = new DescriptiveStatistics(precisionCsv1)
        double[] precisionCsv2 = content.collect{ it.newPrecision as double[] }.flatten() as double[]
        def precisionStats2 = new DescriptiveStatistics(precisionCsv2)
        double[] recallCsv1 = content.collect{ it.oldRecall as double[] }.flatten() as double[]
        def recallStats1 = new DescriptiveStatistics(recallCsv1)
        double[] recallCsv2 = content.collect{ it.newRecall as double[] }.flatten() as double[]
        def recallStats2 = new DescriptiveStatistics(recallCsv2)
        double[] fpCsv1 = content.collect { it.oldFp as double[] }.flatten() as double[]
        def fpStats1 = new DescriptiveStatistics(fpCsv1)
        double[] fpCsv2 = content.collect { it.newFp as double[] }.flatten() as double[]
        def fpStats2 = new DescriptiveStatistics(fpCsv2)
        double[] fnCsv1 = content.collect { it.oldFn as double[] }.flatten() as double[]
        def fnStats1 = new DescriptiveStatistics(fnCsv1)
        double[] fnCsv2 = content.collect { it.newFn as double[] }.flatten() as double[]
        def fnStats2 = new DescriptiveStatistics(fnCsv2)

        def precisionMean = Evaluator.compare(precisionStats1.mean, precisionStats2.mean)
        def recallMean = Evaluator.compare(recallStats1.mean, recallStats2.mean)
        def viewsMean = Evaluator.inverseCompare(viewsStats1.mean, viewsStats2.mean)
        def fpMean = Evaluator.inverseCompare(fpStats1.mean, fpStats2.mean)
        def fnMean = Evaluator.inverseCompare(fnStats1.mean, fnStats2.mean)

        List<String> lines = []
        lines += ["(old) Lost views mean (RT)", viewsStats1.mean] as String[]
        lines += ["(old) Lost views median (RT)", viewsStats1.getPercentile(50.0)] as String[]
        lines += ["(old) Lost views standard deviation (RT)", viewsStats1.standardDeviation] as String[]
        lines += ["(new) Lost views mean (RT)", viewsStats2.mean] as String[]
        lines += ["(new) Lost views median (RT)", viewsStats2.getPercentile(50.0)] as String[]
        lines += ["(new) Lost views standard deviation (RT)", viewsStats2.standardDeviation] as String[]
        lines += ["Improved mean of lost views", viewsMean] as String[]
        lines += [""] as String[]
        lines += ["(old) Precision mean (RT)", precisionStats1.mean] as String[]
        lines += ["(old) Precision median (RT)", precisionStats1.getPercentile(50.0)] as String[]
        lines += ["(old) Precision standard deviation (RT)", precisionStats1.standardDeviation] as String[]
        lines += ["(new) Precision mean (RT)", precisionStats2.mean] as String[]
        lines += ["(new) Precision median (RT)", precisionStats2.getPercentile(50.0)] as String[]
        lines += ["(new) Precision standard deviation (RT)", precisionStats2.standardDeviation] as String[]
        lines += ["Improved mean precision", precisionMean] as String[]
        lines += [""] as String[]
        lines += ["(old) Recall mean (RT)", recallStats1.mean] as String[]
        lines += ["(old) Recall median (RT)", recallStats1.getPercentile(50.0)] as String[]
        lines += ["(old) Recall standard deviation (RT)", recallStats1.standardDeviation] as String[]
        lines += ["(new) Recall mean (RT)", recallStats2.mean] as String[]
        lines += ["(new) Recall median (RT)", recallStats2.getPercentile(50.0)] as String[]
        lines += ["(new) Recall standard deviation (RT)", recallStats2.standardDeviation] as String[]
        lines += ["Improved mean recall", recallMean] as String[]
        lines += [""] as String[]
        lines += ["(old) False positives mean (RT)", fpStats1.mean] as String[]
        lines += ["(old) False positives median (RT)", fpStats1.getPercentile(50.0)] as String[]
        lines += ["(old) False positives standard deviation (RT)", fpStats1.standardDeviation] as String[]
        lines += ["(new) False positives mean (RT)", fpStats2.mean] as String[]
        lines += ["(new) False positives median (RT)", fpStats2.getPercentile(50.0)] as String[]
        lines += ["(new) False positives standard deviation (RT)", fpStats2.standardDeviation] as String[]
        lines += ["Improved mean false positives", fpMean] as String[]
        lines += [""] as String[]
        lines += ["(old) False negatives mean (RT)", fnStats1.mean] as String[]
        lines += ["(old) False negatives median (RT)", fnStats1.getPercentile(50.0)] as String[]
        lines += ["(old) False negatives standard deviation (RT)", fnStats1.standardDeviation] as String[]
        lines += ["(new) False negatives mean (RT)", fnStats2.mean] as String[]
        lines += ["(new) False negatives median (RT)", fnStats2.getPercentile(50.0)] as String[]
        lines += ["(new) False negatives standard deviation (RT)", fnStats2.standardDeviation] as String[]
        lines += ["Improved mean false negatives", fnMean] as String[]

        def name = "$outputFolder${File.separator}aggregated_statistics${CsvIndexes.CSV_EXT}"
        CsvUtil.write(name, lines)

    }
}
