package study1.project_search

import br.ufpe.cin.tan.util.CsvUtil

class InvestigatingRoundsToExclude {

    static void main(String[] args){
        def index = "5"
        def obj1 = new JoinInputCsv("tasks\\paper_sample", "projects.csv", "1-github",
                "projects_excluding-search$index")
        obj1.joinAllInFolder()

        def others = CsvUtil.read("tasks\\paper_sample\\projects_excluding-search${index}.csv")
        others.remove(0)
        println "total de projetos excluindo resultado de search $index: ${others.size()}"
        def othersProjects = others.collect{ it[0] }

        def cucumberProjectsSearchIndex = CsvUtil.read("tasks\\paper_sample\\search$index" +
                "\\2-filtered\\cucumber-projects.csv")
        cucumberProjectsSearchIndex.remove(0)
        println "cucumber projects from search $index: ${cucumberProjectsSearchIndex.size()}"
        def intersection = cucumberProjectsSearchIndex.findAll{ it[0] in othersProjects }
        if(intersection.size() == cucumberProjectsSearchIndex.size()){
            println "we can eliminate search $index!"
        } else {
            println "intersection: ${intersection.size()}"
            def diff = cucumberProjectsSearchIndex - intersection
            println "projetos de search $index que não são alcançáveis por outro meio: ${diff.size()}"
            diff.each{ println it }
        }
    }

}
