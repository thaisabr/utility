package study1.project_search

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

class ProjectSearch1Organizer {

    static void main(String[] args){
        //findFilesRelatedToSmallerSampleRound1()
        findFilesLocalsupport()
    }

    static findFilesRelatedToSmallerSampleRound1(){
        def files = Util.findFilesFromDirectory("search\\search1")
        def candidates = files.findAll{ it.endsWith("candidate-projects.csv") }
        println "candidates: ${candidates.size()}"
        def candidatesEmpty = candidates.findAll{
            def lines = CsvUtil.read(it)
            if(lines.size()<=1) it
            else null
        }
        candidatesEmpty = candidatesEmpty.unique() - null
        println "candidatesEmpty: ${candidatesEmpty.size()}"
        def trullyCandidates = candidates - candidatesEmpty
        println "trullyCandidates: ${trullyCandidates.size()}"
        trullyCandidates.each{ println it }

        def filesOfInterest = [
                "https://github.com/TheOdinProject/theodinproject",
                "https://github.com/TracksApp/tracks",
                "https://github.com/alphagov/whitehall",
                "https://github.com/otwcode/otwarchive",
                "https://github.com/tip4commit/tip4commit",
                "https://github.com/rapidftr/RapidFTR"
        ]

        def result = []
        trullyCandidates.each{ c ->
            def lines = CsvUtil.read(c)
            def found = lines.findAll{ it[1] in filesOfInterest }
            found.each{
                result += [file:c, url:it[1]]
            }
        }
        println "selected: ${result.size()}"
        result.each{ println it }
    }

    static findFilesLocalsupport(){
        def path = "search\\search2"
        def url = "https://github.com/AgileVentures/LocalSupport"
        def files = Util.findFilesFromDirectory(path)
        println "files: ${files.size()}"
        def found = files.findAll{ file ->
            def lines = CsvUtil.read(file)
            if(lines.collect{ it[0] }.contains(url)) file
            else null
        }
        found = found.unique() - null
        println "found: ${found.size()}"
        found.each{ println it }
    }

}
