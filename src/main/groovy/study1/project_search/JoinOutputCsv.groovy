package study1.project_search

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

/* Gera um único CSV de projetos a partir de um conjunto de CSVs. */

class JoinOutputCsv {

    boolean advanced
    List<String[]> buffer
    String outputFile
    List<String> files

    String mainFolder

    List<String[]> header

    static void main(String[] args){
        def obj = new JoinOutputCsv("tasks${File.separator}temp")
        obj.joinAllInFolder()
    }

    JoinOutputCsv(String folder){
        this.mainFolder = folder
        buffer = []
        files = Util.findFilesFromDirectory(folder).findAll{
            it.endsWith("cucumber-projects.csv") && it.contains("${File.separator}2-filtered${File.separator}")
        }
        this.outputFile = "$folder${File.separator}cucumber_projects_final.csv"
    }

    def joinAllInFolder(){
        updateBuffer()
        generateCsv()
    }

    private updateBuffer(){
        header = null
        files.each{ file ->
            println "Reading file: $file"
            updateBuffer(file)
        }
    }

    private updateBuffer(String filename){
        List<String[]> entries = CsvUtil.read(filename)
        if(entries.size()<1) return
        if(header==null) header = [entries.get(0)]
        entries.remove(0)
        buffer += entries
        buffer.unique{ it[0] }
    }

    private generateCsv(){

        List<String[]> content = []
        content += header
        content += buffer.sort{ it[0] }
        CsvUtil.write(outputFile, content)
    }

}
