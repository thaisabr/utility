package study1.project_search

import br.ufpe.cin.tan.util.Util

import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path

/* Gera um único CSV de tarefas a partir de um conjunto de CSVs. */

class TaskFilesOrganizer {

    List<String> files
    String mainFolder
    String mergesFolder
    String tasksFolder

    List<String[]> header

    static void main(String[] args){
        //def obj1 = new TaskFilesOrganizer("tasks${File.separator}temp")
        //obj1.joinAllInFolderByProject()
        renameFiles("tasks${File.separator}temp${File.separator}tasks")
    }

    static renameFiles(String folder){
        def files = Util.findFilesFromDirectory(folder)
        files.each{ file ->
            def newName = file
            def index = file.indexOf("_cucumber.csv")
            if(index>-1) {
                newName = file.substring(0, index) + ".csv"
            }
            File f = new File(file)
            f.renameTo(newName)
        }
    }

    TaskFilesOrganizer(String folder){
        this.mainFolder = folder
        files = Util.findFilesFromDirectory(folder)
        configureFolders(folder)
    }

    def joinAllInFolderByProject(){
        organizeFilesByType()
        def mergeFiles = Util.findFilesFromDirectory(mergesFolder)
        println "mergeFiles: ${mergeFiles.size()}"
        mergeFiles.each{ println it }

        def taskFiles = Util.findFilesFromDirectory(tasksFolder)
        println "taskFiles: ${taskFiles.size()}"
        taskFiles.each{ println it }
    }

    private organizeFilesByType(){
        def merges = files.findAll{ it.endsWith("_merges.csv") }
        merges.each{ file ->
            Path source = FileSystems.getDefault().getPath(file)
            Path target = FileSystems.getDefault().getPath(mergesFolder)
            Files.copy(source, target.resolve(source.getFileName()))
        }

        def tasks = files.findAll{ it.endsWith("_cucumber.csv") }
        tasks.each{ file ->
            Path source = FileSystems.getDefault().getPath(file)
            Path target = FileSystems.getDefault().getPath(tasksFolder)
            Files.copy(source, target.resolve(source.getFileName()))
        }
    }

    private configureFolders(String folder){
        mergesFolder = "$folder${File.separator}merges"
        tasksFolder = "$folder${File.separator}tasks"
        createFolder(mergesFolder)
        createFolder(tasksFolder)
    }

    private static createFolder(String folder){
        File folderManager = new File(folder)
        if(folderManager.exists()) Util.deleteFolder(folder)
        else folderManager.mkdir()
    }

}
