package study1.project_search

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util
import groovy.util.logging.Slf4j

/* Gera um único CSV de projetos a partir de um conjunto de CSVs. */

@Slf4j
class JoinInputCsv {

    boolean advanced
    List<String[]> buffer
    String outputFile
    List<String> files

    String mainFolder

    List<String[]> header

    static void main(String[] args){
        //organizingPaperSearch1()
        organizingRealSample()
    }

    JoinInputCsv(String folder, String sufix, String key, String outputFile){
        this.mainFolder = folder
        buffer = []
        files = Util.findFilesFromDirectory(folder).findAll{
            it.endsWith(sufix) && it.contains("${File.separator}${key}${File.separator}")
        }
        this.outputFile = "$folder${File.separator}${outputFile}.csv"
    }

    def joinAllInFolder(){
        updateBuffer()
        generateCsv()
    }

    //organizar o resultado de round 1, dado que este consiste de múltiplas execuções, pois variamos o máximo de estrelas
    //para 2 casos: uso de simplecov e uso de coveralls
    static organizingPaperSearch1(){
        def coverallsFolder = "paper_sample_construction\\paper_sample\\search\\search1\\coveralls"
        def obj1 = new JoinInputCsv(coverallsFolder, "projects.csv", "1-github", "projects")
        obj1.joinAllInFolder()

        def simplecovFolder = "paper_sample_construction\\paper_sample\\search\\search1\\simplecov"
        def obj2 = new JoinInputCsv(simplecovFolder, "projects.csv", "1-github", "projects")
        obj2.joinAllInFolder()

        List<String[]> coveralls = CsvUtil.read("${coverallsFolder}\\projects.csv")
        def header = coveralls.get(0)
        coveralls.remove(0)
        println "coverallsProjects: ${coveralls.size()}"

        List<String[]> simplecov = CsvUtil.read("${simplecovFolder}\\projects.csv")
        simplecov.remove(0)
        println "simplecovProjects: ${simplecov.size()}"

        //tentando usar a interseção, mas tem projeto da amostra menor que fica de fora
        /*def coverallsProjects = coveralls.collect{ it[0] }.unique()
        println "coverallsProjects: ${coverallsProjects.size()}"
        def simplecovProjects = simplecov.collect{ it[0] }.unique()
        println "simplecovProjects: ${simplecovProjects.size()}"
        def intersection = coverallsProjects.intersect(simplecovProjects)
        println "intersection: ${intersection.size()}"
        def selected = simplecov.findAll{ it[0] in intersection }*/

        //usamos a união dos resultados
        def selected = (simplecov + coveralls).unique{ [it[0], it[1]] }.sort{ it[3] }
        println "union: ${selected.size()}"
        List<String[]> content = []
        content += header
        content += selected
        CsvUtil.write("paper_sample_construction\\paper_sample\\search\\search1\\projects.csv", content)
    }

    //organizar o resultado dos diferentes rounds em um único resultado
    static organizingRealSample(){
        def folder = "paper_sample_construction\\paper_sample\\filtering"

        def obj1 = new JoinInputCsv(folder, "projects.csv", "1-github", "projects")
        obj1.joinAllInFolder()
        def lines = CsvUtil.read("${folder}\\projects.csv")
        lines.remove(0)
        println "projects: ${lines.size()}"

        def obj2 = new JoinInputCsv(folder, "rails-projects.csv", "2-filtered", "rails-projects")
        obj2.joinAllInFolder()
        lines = CsvUtil.read("${folder}\\rails-projects.csv")
        lines.remove(0)
        println "rails-projects: ${lines.size()}"

        def obj3 = new JoinInputCsv(folder, "cucumber-projects.csv", "2-filtered", "cucumber-projects")
        obj3.joinAllInFolder()
        lines = CsvUtil.read("${folder}\\cucumber-projects.csv")
        lines.remove(0)
        println "cucumber-projects: ${lines.size()}"

        def obj4 = new JoinInputCsv(folder, "coveralls-projects.csv", "2-filtered", "coveralls-projects")
        obj4.joinAllInFolder()
        lines = CsvUtil.read("${folder}\\coveralls-projects.csv")
        lines.remove(0)
        println "coveralls-projects: ${lines.size()}"

        def obj5 = new JoinInputCsv(folder, "simplecov-projects.csv", "2-filtered", "simplecov-projects")
        obj5.joinAllInFolder()
        lines = CsvUtil.read("${folder}\\simplecov-projects.csv")
        lines.remove(0)
        println "simplecov-projects: ${lines.size()}"

        def obj6 = new JoinInputCsv(folder, "coveralls&cucumber-projects.csv", "2-filtered",
                "coveralls&cucumber-projects")
        obj6.joinAllInFolder()
        lines = CsvUtil.read("${folder}\\coveralls&cucumber-projects.csv")
        lines.remove(0)
        println "coveralls&cucumber-projects: ${lines.size()}"

        def obj7 = new JoinInputCsv(folder, "simplecov&cucumber-projects.csv", "2-filtered",
                "simplecov&cucumber-projects")
        obj7.joinAllInFolder()
        lines = CsvUtil.read("${folder}\\simplecov&cucumber-projects.csv")
        lines.remove(0)
        println "simplecov&cucumber-projects: ${lines.size()}"

        def obj8 = new JoinInputCsv(folder, "coverage&cucumber-projects.csv", "2-filtered",
                "coverage&cucumber-projects")
        obj8.joinAllInFolder()
        lines = CsvUtil.read("${folder}\\coverage&cucumber-projects.csv")
        lines.remove(0)
        println "coverage&cucumber-projects: ${lines.size()}"
    }

    private updateBuffer(){
        header = null
        files.each{ file ->
            log.info "Reading file: $file"
            updateBuffer(file)
        }
    }

    private updateBuffer(String filename){
        List<String[]> entries = CsvUtil.read(filename)
        if(entries.size()<1) return
        if(header==null) header = [entries.get(0)]
        entries.remove(0)
        buffer += entries
    }

    private generateCsv(){
        List<String[]> content = []
        content += header
        def temp = buffer.unique{it[0]}.sort{ it[0] }
        log.info "results: ${temp.size()}"
        content += temp
        CsvUtil.write(outputFile, content)
    }

}
