package conflict

import br.ufpe.cin.tan.util.Util

class VerifyAnalysisOutput {

    static void main(String[] args){
        def folder = "D:\\Dropbox\\Thaís\\phd_study2_fixed_result\\testi"
        def files = Util.findFilesFromDirectory(folder)
        def logfiles = files.findAll{ it.endsWith("evaluation.log") }

        logfiles.each{ file ->
            println "file: $file"
            def lines = new File(file).readLines()
            def bigTasksLines = lines.findAll{ it.contains("Big tasks (more than 500 commits): ") }
            println "big tasks lines: ${bigTasksLines.size()}"
            def counter = 0
            bigTasksLines.findAll{
                def content = it.split(" ")
                def quantity = content.last() as double
                if(quantity>0) counter += quantity
            }
            println "big tasks: ${counter}"

            /*def taskInterfaces = lines.findAll{ it.contains("Task interfaces were computed for") }
            println "task interfaces (lines): ${taskInterfaces.size()}"
            taskInterfaces.each{ println it }

            def relevantTasksLines = lines.findAll{ it.contains("(From candidates) Relevant tasks (valid but with no empty ITest):") }
            println "relevant tasks (lines): ${relevantTasksLines.size()}"
            relevantTasksLines.each{ println it }

            def discardedTasksLines = lines.findAll{ it.contains("Relevant tasks that do not satisfy all selection criteria") }
            println "discarded tasks (lines): ${discardedTasksLines.size()}"
            discardedTasksLines.each{ println it }*/
        }


    }


}
