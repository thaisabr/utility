package conflict

import br.ufpe.cin.tan.util.CsvUtil

/*
* De 13.792 pares, 13.275 (96%) tem ao menos 1 arquivo na interseção de TestI.
* Investigação para entender qual seria o arquivo que mais aparece nessa interseção.
* */

class InvestigatingIntersection {

    /*PROJECT 0
    LEFT_TASK 1
    RIGHT_TASK 2
    CONFLICT 3
    CONFLICT_OF_INTEREST 5
    INTERSECTION_TESTIL_TESTIR 23*/

    static void main(String[] args){
        def file = "D:\\Dropbox\\Thaís\\phd_study2_fixed_result\\conflict\\output\\conflict.csv"
        //def file = "D:\\Dropbox\\Thaís\\phd_study2\\new_sample" +
        //        "\\conflict_risk_by_changed_files_refactored_based_on_intersection\\conflict.csv"
        //verifyMostCommonFiles(file)

        def filesToExclude = ["app/models/user.rb",
                              "app/controllers/users_controller.rb",
                              "app/controllers/home_controller.rb",
                              "app/views/users/show.html.erb",
                              "app/views/users/new.html.erb"]
        //generateResultExcludingFiles(file, filesToExclude)

        verifyMostCommonFilesPerProject(file)
    }

    static verifyMostCommonFiles(String file){
        def entries = CsvUtil.read(file)
        entries.remove(0)
        def intersection = entries.collect{ it[23].substring(1, it[23].size()-1).tokenize(",")*.trim() }
        println "intersection: ${intersection.size()}"

        def emptyIntersection = intersection.findAll{ it.size()==0 }
        println "empty intersection: ${emptyIntersection.size()} (${emptyIntersection.size()/intersection.size()*100}%)"

        def notEmptyIntersection = intersection - emptyIntersection
        println "not empty intersection: ${notEmptyIntersection.size()} (${notEmptyIntersection.size()/intersection.size()*100}%)"

        def userModel = "app/models/user.rb"
        def counterUserOccurrence = 0
        notEmptyIntersection.each{
            if(it.contains(userModel)) counterUserOccurrence++
        }
        println "has user model: ${counterUserOccurrence} (${counterUserOccurrence/notEmptyIntersection.size()*100}% of no empty)"

        def files = notEmptyIntersection.flatten()
        println "files: ${files.size()}"
        def uniqueFiles = files.unique().sort()
        println "unique files: ${uniqueFiles.size()}"

        def frequencies = []
        uniqueFiles.each{ uf ->
            def number = notEmptyIntersection.findAll{ it.contains(uf) }.size()
            frequencies += [file: uf, n:number]
        }

        println "frequency results: ${frequencies.size()}"
        frequencies.each{
            println "${it.file}; ${it.n}"
        }

        def mostFrequent = frequencies.findAll{ it.n >= 1000 }.sort{it.n}.reverse()
        println "most common files: ${mostFrequent.size()}"
        mostFrequent.each{
            println "${it.file}; ${it.n}"
        }
    }

    static verifyIntersection(String file){
        println "Verifying intersection from file: ${file}"
        def entries = CsvUtil.read(file)
        entries.remove(0)
        def intersection = entries.collect{ it[23].substring(1, it[23].size()-1).tokenize(",")*.trim() }
        println "intersection: ${intersection.size()}"

        def emptyIntersection = intersection.findAll{ it.size()==0 }
        println "empty intersection: ${emptyIntersection.size()} (${emptyIntersection.size()/intersection.size()*100}%)"

        def notEmptyIntersection = intersection - emptyIntersection
        println "not empty intersection: ${notEmptyIntersection.size()} (${notEmptyIntersection.size()/intersection.size()*100}%)"
    }

    static generateResultExcludingFiles(String file, List filesToExclude){
        def entries = CsvUtil.read(file)
        List<String[]> data = []
        data += entries.get(0)
        entries.subList(1, entries.size()).each{ entry ->
            def newIntersection = entry[23].substring(1, entry[23].size()-1).tokenize(",")*.trim()
            newIntersection = newIntersection - filesToExclude
            entry[23] = newIntersection as String
            data += entry
        }
        CsvUtil.write("output\\conflict-intersection-update.csv", data)

        verifyIntersection(file)
        verifyIntersection("output\\conflict-intersection-update.csv")
    }

    static verifyMostCommonFilesPerProject(String file){
        def entries = CsvUtil.read(file)
        entries.remove(0)

        def intersection = entries.collect{ it[23].substring(1, it[23].size()-1).tokenize(",")*.trim() }
        println "intersection: ${intersection.size()}"
        def notEmptyIntersection = intersection.findAll{ it.size()!= 0 }

        def entriesPerProject = entries.collect{
            [project: it[0], intersection:it[23].substring(1, it[23].size()-1).tokenize(",")*.trim()]
        }.groupBy { it.project }

        def files = notEmptyIntersection.flatten()
        println "files: ${files.size()}"
        def uniqueFiles = files.unique().sort()
        println "unique files: ${uniqueFiles.size()}"

        def frequenciesPerProject = []
        entriesPerProject.each{ ep ->
            def freq = []
            uniqueFiles.each{ uf ->
                def number = ep.value*.intersection.findAll{ it.contains(uf) }.size()
                if(number>0) freq += [file: uf, n:number]
            }
            frequenciesPerProject += [project: ep.key, frequency: freq.sort{it.n}.reverse() ]
        }

        List<String[]> data = []
        println "frequencies per project: ${frequenciesPerProject.size()}"
        frequenciesPerProject.each{ fpp ->
            println "${fpp.project}: ${fpp.frequency.size()}"
            data += ["${fpp.project}:", fpp.frequency.size() ] as String[]
            fpp.frequency.each{
                println "${it.file}; ${it.n}"
                data += [it.file, it.n ] as String[]
            }
        }
        CsvUtil.write("output\\intersection_files_frequency_per_project.csv", data)
    }

}
