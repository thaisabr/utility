package conflict

class VerifyIntegrationOutput {

    static void main (String[] args){
        def file = "C:\\Users\\tatab\\Desktop\\study2\\5-integration\\fixed" +
                "\\real_scenarios\\output\\real_scenarios.log"
        println "file: $file"
        def lines = new File(file).readLines()
        def selected = lines.findAll{ line ->
            line.contains("- mergeScenariosFile: ") ||
            line.contains("- Project: ") ||
            line.contains("- selectedInputTasks: ") ||
            line.contains("- Pairs: ") ||
            line.contains("scenarios: ")
        }
        selected.each{ println it }

    }
}
