package conflict

import br.ufpe.cin.tan.util.Util

class RenameTaskFile {


    static void main(String[] args){
        renameFiles("C:\\Users\\tatab\\Desktop\\conflict_testi_noempty_texti\\tasks\\taskfile")
    }

    static renameFiles(String inputFolder){
        def files = Util.findFilesFromDirectory(inputFolder)
        println "files: ${files.size()}"
        files.each{ file ->
            File obj = new File(file)
            def newname = file - "-independent-tasks"
            println "file: $file"
            println "new file name: ${newname}"
            obj.renameTo(newname)
        }
    }

}
