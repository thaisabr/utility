package study2

import br.ufpe.cin.tan.similarity.test.TestSimilarityAnalyser
import br.ufpe.cin.tan.util.CsvUtil

/*
* Cálculo de precision e recall para predição de risco de conflito com base na similaridade entre TestI.
* */
class EvaluatingPrecisionRecallBasedOnSimilarity {

    List data //[project, left, right, conflict, conflictOfInterest, jaccard, cosine]
    List jaccardResult
    List cosineResult
    String file
    static final int PROJECT_INDEX = 0
    static final int LEFT_TASK_INDEX = 1
    static final int RIGHT_TASK_INDEX = 2
    static final int CONFLICT_INDEX = 3
    static final int CONFLIC_OF_INTEREST_INDEX = 5
    static final int TESTI_LEFT_INDEX = 7
    static final int TESTI_RIGHT_INDEX = 8
    static final int COMMON_CHANGED_FILES = 9
    static final int COMMON_CHANGED_PROD_FILES = 11

    EvaluatingPrecisionRecallBasedOnSimilarity(String file){
        this.file = file
        data = []
        def lines = CsvUtil.read(file)
        lines.remove(0)
        lines.each{ line ->
            def testiLeft = line[TESTI_LEFT_INDEX].substring(1, line[TESTI_LEFT_INDEX].size()-1).tokenize(",")*.trim()
            def testiRight = line[TESTI_RIGHT_INDEX].substring(1, line[TESTI_RIGHT_INDEX].size()-1).tokenize(",")*.trim()
            def similarityAnalyser = new TestSimilarityAnalyser(testiLeft, testiRight)
            def simjac = similarityAnalyser.calculateSimilarityByJaccard()
            def simcos = similarityAnalyser.calculateSimilarityByCosine()
            def intersectionTesti = testiLeft.intersect(testiRight)
            data += [project: line[PROJECT_INDEX],
                     left: line[LEFT_TASK_INDEX],
                     right: line[RIGHT_TASK_INDEX],
                     conflict: line[CONFLICT_INDEX] as int,
                     conflictOfInterest: line[CONFLIC_OF_INTEREST_INDEX] as int,
                     jaccard: simjac,
                     cosine: simcos,
                     intersectionTesti: intersectionTesti.size(),
                     commonChangedFiles: line[COMMON_CHANGED_FILES] as int,
                     commonChangedProdFiles: line[COMMON_CHANGED_PROD_FILES] as int]
        }
        exportResultsWithSimilarityAndIntersectionInfo()

        this.jaccardResult = []
        this.cosineResult = []
    }

    def exportResultsWithSimilarityAndIntersectionInfo(){
        List<String[]> content = []
        def header = ["Project", "left", "right", "conflict", "conflict_of_interest", "cosine", "intersection",
                      "common_changed_files", "common_changed_prod_files"] as String[]
        content += header
        data.each{
            content += [it.project, it.left, it.right, it.conflict, it.conflictOfInterest, it.cosine, it.intersectionTesti,
            it.commonChangedFiles, it.commonChangedProdFiles] as String[]
        }

        CsvUtil.write("output\\conflict_noempty_similarity_intersection.csv", content)
    }

    static void main(String[] args){
        def folder = "D:\\Dropbox\\Thaís\\phd_study2_final_result\\conflict_prediction\\output\\"
        def file = "${folder}conflict_noemptytexti.csv"

        def evaluator = new EvaluatingPrecisionRecallBasedOnSimilarity(file)

        def metrics1 = evaluator.evaluate(0.1d)
        def metrics2 = evaluator.evaluate(0.2d)
        def metrics3 = evaluator.evaluate(0.3d)
        def metrics4 = evaluator.evaluate(0.4d)
        def metrics5 = evaluator.evaluate(0.5d)
        def metrics6 = evaluator.evaluate(0.6d)
        def metrics7 = evaluator.evaluate(0.7d)
        def metrics8 = evaluator.evaluate(0.8d)
        def metrics9 = evaluator.evaluate(0.9d)

        println "conflict jaccard 0.1: ${metrics1.jaccard}"
        println "conflict jaccard 0.2: ${metrics2.jaccard}"
        println "conflict jaccard 0.3: ${metrics3.jaccard}"
        println "conflict jaccard 0.4: ${metrics4.jaccard}"
        println "conflict jaccard 0.5: ${metrics5.jaccard}"
        println "conflict jaccard 0.6: ${metrics6.jaccard}"
        println "conflict jaccard 0.7: ${metrics7.jaccard}"
        println "conflict jaccard 0.8: ${metrics8.jaccard}"
        println "conflict jaccard 0.9: ${metrics9.jaccard}"

        println "conflict cosine 0.1: ${metrics1.cosine}"
        println "conflict cosine 0.2: ${metrics2.cosine}"
        println "conflict cosine 0.3: ${metrics3.cosine}"
        println "conflict cosine 0.4: ${metrics4.cosine}"
        println "conflict cosine 0.5: ${metrics5.cosine}"
        println "conflict cosine 0.6: ${metrics6.cosine}"
        println "conflict cosine 0.7: ${metrics7.cosine}"
        println "conflict cosine 0.8: ${metrics8.cosine}"
        println "conflict cosine 0.9: ${metrics9.cosine}"
    }

    def evaluate(double similarityRate){
        generateJaccardResult(similarityRate)
        def jaccardPrecision = calculateJaccardPrecision()
        def jaccardRecall = calculateJaccardRecall()
        def jaccardPrecisionOfInterest = calculateJaccardPrecisionForConflictsOfInterest()
        def jaccardRecallOfInterest = calculateJaccardRecallForConflictsOfInterest()
        def jaccardMetrics = [precision: jaccardPrecision, recall: jaccardRecall,
                              precisioni: jaccardPrecisionOfInterest, recalli: jaccardRecallOfInterest]

        def outputFile1 = "output${File.separator}precison_recall_conflict_jaccard"
        if(file.endsWith("-controller.csv")) outputFile1 += "-controller-${similarityRate}.csv"
        else outputFile1 += "-${similarityRate}.csv"
        exportResult(outputFile1, jaccardResult, jaccardMetrics)

        generateCosineResult(similarityRate)
        def cosinePrecision = calculateCosinePrecision()
        def cosineRecall = calculateCosineRecall()
        def cosinePrecisionOfInterest = calculateCosinePrecisionForConflictsOfInterest()
        def cosineRecallOfInterest = calculateCosineRecallForConflictsOfInterest()
        def cosineMetrics = [precision: cosinePrecision, recall: cosineRecall,
                              precisioni: cosinePrecisionOfInterest, recalli: cosineRecallOfInterest]

        def outputFile2 = "output${File.separator}precison_recall_conflict_cosine"
        if(file.endsWith("-controller.csv")) outputFile2 += "-controller-${similarityRate}.csv"
        else outputFile2 += "-${similarityRate}.csv"
        exportResult(outputFile2, cosineResult, cosineMetrics)
        [jaccard:jaccardMetrics, cosine:cosineMetrics]
    }

    def generateJaccardResult(double similarityRate){
        jaccardResult = []
        data?.each{ d ->
            def conflictPrediction = 0
            if(d.jaccard>=similarityRate) conflictPrediction = 1
            jaccardResult += [project: d.project, left: d.left, right: d.right,
                              similarity: d.jaccard,
                              conflict: d.conflict,
                              conflictOfInterest: d.conflictOfInterest,
                              prediction: conflictPrediction]
        }
    }

    def generateCosineResult(double similarityRate){
        cosineResult = []
        data?.each{ d ->
            def conflictPrediction = 0
            if(d.cosine>=similarityRate) conflictPrediction = 1
            cosineResult += [project: d.project, left: d.left, right: d.right,
                             similarity: d.cosine,
                             conflict: d.conflict,
                             conflictOfInterest: d.conflictOfInterest,
                             prediction: conflictPrediction]
        }
    }

    double calculateJaccardPrecision() {
        def conflicts = jaccardResult.findAll{ it.conflict == 1 }?.collect{ [it.project, it.left, it.right] }
        def predictedConflicts = jaccardResult.findAll{ it.prediction == 1 }?.collect{ [it.project, it.left, it.right] }
        double result = 0
        if (invalidInput(predictedConflicts, conflicts)) return result
        def truePositives = predictedConflicts.findAll{ it in conflicts }.size()
        if (truePositives > 0) result = (double) truePositives / predictedConflicts.size()
        result
    }

    double calculateJaccardRecall() {
        def conflicts = jaccardResult.findAll{ it.conflict == 1 }?.collect{ [it.project, it.left, it.right] }
        def predictedConflicts = jaccardResult.findAll{ it.prediction == 1 }?.collect{ [it.project, it.left, it.right] }
        double result = 0
        if (invalidInput(predictedConflicts, conflicts)) return result
        def truePositives = predictedConflicts.findAll{ it in conflicts }.size()
        if (truePositives > 0) result = (double) truePositives / conflicts.size()
        result
    }

    double calculateJaccardPrecisionForConflictsOfInterest() {
        def conflictsOfInterest = jaccardResult.findAll{ it.conflictOfInterest == 1 }?.collect{ [it.project, it.left, it.right] }
        def predictedConflicts = jaccardResult.findAll{ it.prediction == 1 }?.collect{ [it.project, it.left, it.right] }
        double result = 0
        if (invalidInput(predictedConflicts, conflictsOfInterest)) return result
        def truePositives = predictedConflicts.findAll{ it in conflictsOfInterest }.size()
        if (truePositives > 0) result = (double) truePositives / predictedConflicts.size()
        result
    }

    double calculateJaccardRecallForConflictsOfInterest() {
        def conflictsOfInterest = jaccardResult.findAll{ it.conflictOfInterest == 1 }?.collect{ [it.project, it.left, it.right] }
        def predictedConflicts = jaccardResult.findAll{ it.prediction == 1 }?.collect{ [it.project, it.left, it.right] }
        double result = 0
        if (invalidInput(predictedConflicts, conflictsOfInterest)) return result
        def truePositives = predictedConflicts.findAll{ it in conflictsOfInterest }.size()
        if (truePositives > 0) result = (double) truePositives / conflictsOfInterest.size()
        result
    }

    double calculateCosinePrecision() {
        def conflicts = cosineResult.findAll{ it.conflict == 1 }?.collect{ [it.project, it.left, it.right] }
        def predictedConflicts = cosineResult.findAll{ it.prediction == 1 }?.collect{ [it.project, it.left, it.right] }
        double result = 0
        if (invalidInput(predictedConflicts, conflicts)) return result
        def truePositives = predictedConflicts.findAll{ it in conflicts }.size()
        if (truePositives > 0) result = (double) truePositives / predictedConflicts.size()
        result
    }

    double calculateCosineRecall() {
        def conflicts = cosineResult.findAll{ it.conflict == 1 }?.collect{ [it.project, it.left, it.right] }
        def predictedConflicts = cosineResult.findAll{ it.prediction == 1 }?.collect{ [it.project, it.left, it.right] }
        double result = 0
        if (invalidInput(predictedConflicts, conflicts)) return result
        def truePositives = predictedConflicts.findAll{ it in conflicts }.size()
        if (truePositives > 0) result = (double) truePositives / conflicts.size()
        result
    }

    double calculateCosinePrecisionForConflictsOfInterest() {
        def conflictsOfInterest = cosineResult.findAll{ it.conflictOfInterest == 1 }?.collect{ [it.project, it.left, it.right] }
        def predictedConflicts = cosineResult.findAll{ it.prediction == 1 }?.collect{ [it.project, it.left, it.right] }
        double result = 0
        if (invalidInput(predictedConflicts, conflictsOfInterest)) return result
        def truePositives = predictedConflicts.findAll{ it in conflictsOfInterest }.size()
        if (truePositives > 0) result = (double) truePositives / predictedConflicts.size()
        result
    }

    double calculateCosineRecallForConflictsOfInterest() {
        def conflictsOfInterest = cosineResult.findAll{ it.conflictOfInterest == 1 }?.collect{ [it.project, it.left, it.right] }
        def predictedConflicts = cosineResult.findAll{ it.prediction == 1 }?.collect{ [it.project, it.left, it.right] }
        double result = 0
        if (invalidInput(predictedConflicts, conflictsOfInterest)) return result
        def truePositives = predictedConflicts.findAll{ it in conflictsOfInterest }.size()
        if (truePositives > 0) result = (double) truePositives / conflictsOfInterest.size()
        result
    }

    private static invalidInput(prediction, conflictResult) {
        if (!prediction || prediction.empty || !conflictResult || conflictResult.empty) true
        else false
    }

    static exportResult(String outputFile, List data, def metrics){
        List<String[]> content = []
        def header = ["Project", "left", "right", "similarity", "conflict", "conflict_of_interest", "prediction"] as String[]
        content += header
        data.each{
            content += [it.project, it.left, it.right, it.similarity, it.conflict, it.conflictOfInterest, it.prediction] as String[]
        }

        content += ["precision", metrics.precision] as String []
        content += ["recall", metrics.recall] as String []
        content += ["precision of interest", metrics.precisioni] as String []
        content += ["recall of interest", metrics.recalli] as String []
        CsvUtil.write(outputFile, content)
    }

}
