package study2

import br.ufpe.cin.tan.util.CsvUtil

/*
* Cálculo de precision e recall para predição de risco de conflito com base na interseção entre TestI.
* */
class EvaluatingPrecisionRecallBasedOnIntersection {

    List data
    List noEmptyTestiTasks
    List intersectionResult
    String file
    static final int PROJECT_INDEX = 0
    static final int LEFT_TASK_INDEX = 1
    static final int RIGHT_TASK_INDEX = 2
    static final int CONFLICT_INDEX = 3
    static final int CONFLIC_OF_INTEREST_INDEX = 5
    static final int TESTI_LEFT = 7
    static final int TESTI_RIGHT = 8
    static final int INTERSECTION_INDEX = 23
    static final int INTERFACE_SIZE_LIMIT = 10
    boolean isController
    String testiResultForStatisticalAnalysisFile

    EvaluatingPrecisionRecallBasedOnIntersection(String file){
        this.file = file
        this.testiResultForStatisticalAnalysisFile = "output${File.separator}testi_evaluation.csv"
        data = []
        def lines = CsvUtil.read(file)
        lines.remove(0)
        lines.each{ line ->
            data += [project: line[PROJECT_INDEX], left: line[LEFT_TASK_INDEX], right: line[RIGHT_TASK_INDEX],
                     conflict: line[CONFLICT_INDEX] as int, conflictOfInterest: line[CONFLIC_OF_INTEREST_INDEX] as int,
                     testiLeft: line[TESTI_LEFT].substring(1, line[TESTI_LEFT].size()-1).tokenize(",")*.trim(),
                     testiRight: line[TESTI_RIGHT].substring(1, line[TESTI_RIGHT].size()-1).tokenize(",")*.trim(),
                     intersection: line[INTERSECTION_INDEX]
                             .substring(1, line[INTERSECTION_INDEX].size()-1).tokenize(",")*.trim()]
        }
        this.intersectionResult = []
    }

    EvaluatingPrecisionRecallBasedOnIntersection(String file, String noEmptyTestiTasksFile){
        this.file = file
        data = []
        def lines = CsvUtil.read(file)
        lines.remove(0)
        lines.each{ line ->
            def project = fixProjectName(line[PROJECT_INDEX])
            data += [project: project, left: line[LEFT_TASK_INDEX], right: line[RIGHT_TASK_INDEX],
                     conflict: line[CONFLICT_INDEX] as int, conflictOfInterest: line[CONFLIC_OF_INTEREST_INDEX] as int,
                     testiLeft: line[TESTI_LEFT].substring(1, line[TESTI_LEFT].size()-1).tokenize(",")*.trim(),
                     testiRight: line[TESTI_RIGHT].substring(1, line[TESTI_RIGHT].size()-1).tokenize(",")*.trim(),
                     intersection: line[INTERSECTION_INDEX]
                             .substring(1, line[INTERSECTION_INDEX].size()-1).tokenize(",")*.trim()]
        }
        this.intersectionResult = []

        noEmptyTestiTasks = []
        def noEmptyTestiEntryTasks = CsvUtil.read(noEmptyTestiTasksFile)
        noEmptyTestiEntryTasks.remove(0)
        noEmptyTestiEntryTasks.each{ line ->
            def project = fixUrl(line[0])
            noEmptyTestiTasks += [project: project, id: line[1]]
        }
    }

    static void main(String[] args){
        def folder = "C:\\Users\\tatab\\Desktop\\conflict_prediction_given\\output\\"
        def file1 = "${folder}conflict_noemptytexti.csv"

        generateResultForStatisticalAnalysis(file1)

        def evaluator = new EvaluatingPrecisionRecallBasedOnIntersection(file1)
        def metrics1 = evaluator.evaluate(1, -1)
        def metrics2 = evaluator.evaluate(2, -1)
        def metrics3 = evaluator.evaluate(3, -1)
        def metrics4 = evaluator.evaluate(4, -1)
        def metrics5 = evaluator.evaluate(5, -1)
        def metrics6 = evaluator.evaluate(6, -1)
        def metrics7 = evaluator.evaluate(7, -1)
        def metrics8 = evaluator.evaluate(8, -1)
        def metrics9 = evaluator.evaluate(9, -1)
        def metrics10 = evaluator.evaluate(10, -1)

        println "conflict 1: ${metrics1}"
        println "conflict 2: ${metrics2}"
        println "conflict 3: ${metrics3}"
        println "conflict 4: ${metrics4}"
        println "conflict 5: ${metrics5}"
        println "conflict 6: ${metrics6}"
        println "conflict 7: ${metrics7}"
        println "conflict 8: ${metrics8}"
        println "conflict 9: ${metrics9}"
        println "conflict 10: ${metrics10}"
    }

    private exportConflictResultForTasksWithNoEmptyTexti(){
        def lines = CsvUtil.read(file)
        def conflictHeader = lines.get(0)
        lines.remove(0)

        List<String[]> content = []
        content += conflictHeader

        lines.each{ line ->
            def project = fixProjectName(line[PROJECT_INDEX])
            def left = noEmptyTestiTasks.find{ it.project== project && it.id==line[LEFT_TASK_INDEX] }
            def right = noEmptyTestiTasks.find{ it.project== project && it.id==line[RIGHT_TASK_INDEX] }
            if(left && right){
                content += line
            }
        }

        CsvUtil.write("output\\conflict-noemptyTexti.csv", content)
    }

    private static fixUrl(String url){
        def project = url.toLowerCase()
        project = project - "https://github.com/" - ".git"
        def index = project.lastIndexOf("/")
        if(index>-1) project.substring(index+1)
        else project
    }

    private static fixProjectName(String name){
        def project = name
        def index = name.lastIndexOf("_")
        if(index>-1) project = name.substring(index+1)

        switch (project){
            case "action": project = "action-center-platform"
                break
            case "e" : project = "e-petitions"
                break
            case "concord" : project = "rigse"
                break
            case "claim" : project = "claim-for-crown-court-defence"
                break
            case "one" : project = "one-click-orgs"
        }

        project
    }

    def generateIntersectionResultAccordingToTestiSize(double intersectionRate, double rateForSmallInterfaces){
        intersectionResult = []
        data?.each{ d ->
            def conflictPrediction = 0
            if(d.testiLeft.size()>0 && d.testiRight.size()>0 ){
                if(d.testiLeft.size()<=intersectionRate && d.testiRight.size()<=intersectionRate) {
                    if (d.intersection.size() >= intersectionRate) conflictPrediction = 1
                } else {
                    def smaller = d.testiLeft
                    if(d.testiRight.size() < d.testiLeft.size() ) smaller = d.testiRight
                    def truePositivesFromSmaller = smaller.findAll{ it in d.intersection }
                    def rate = 0
                    if(smaller.size()>0) rate = truePositivesFromSmaller.size()/smaller.size()
                    if(rate>=rateForSmallInterfaces) conflictPrediction = 1
                }
                intersectionResult += [project: d.project, left: d.left, right: d.right,
                                       intersection: d.intersection.size(),
                                       conflict: d.conflict,
                                       conflictOfInterest: d.conflictOfInterest,
                                       prediction: conflictPrediction]
            }
        }
    }

    def evaluate(double intersection, double rateForSmallInterfaces){
        def outputFile1 = "output${File.separator}precison_recall_conflict_intersection"
        if(file.endsWith("-controller.csv")) {
            outputFile1 += "-controller-${intersection}.csv"
            isController = true
        }
        else outputFile1 += "-${intersection}.csv"

        generateIntersectionResult(intersection, rateForSmallInterfaces)
        //generateIntersectionResultAccordingToTestiSize(intersection, rateForSmallInterfaces)

        def precision = calculatePrecision()
        def recall = calculateRecall()
        def f2 = calculateF2()
        def precisionOfInterest = calculatePrecisionForConflictsOfInterest()
        def recallOfInterest = calculateRecallForConflictsOfInterest()
        def f2OfInterest = calculateF2ForConflictsOfInterest()
        def metrics = [precision: precision, recall: recall, precisioni: precisionOfInterest, recalli: recallOfInterest,
                       f2:f2, f2i:f2OfInterest]

        exportResult(outputFile1, intersectionResult, metrics)

        metrics
    }

    def evaluateNoEmptyTexti(double intersection){
        def outputFile1 = "output${File.separator}precison_recall_conflict_intersection_noemptytexti"
        if(file.endsWith("-controller.csv")) {
            outputFile1 += "-controller-${intersection}.csv"
            isController = true
        }
        else outputFile1 += "-${intersection}.csv"

        generateIntersectionResultForNonEmptyTexti(intersection)

        def precision = calculatePrecision()
        def recall = calculateRecall()
        def f2 = calculateF2()
        def precisionOfInterest = calculatePrecisionForConflictsOfInterest()
        def recallOfInterest = calculateRecallForConflictsOfInterest()
        def f2OfInterest = calculateF2ForConflictsOfInterest()
        def metrics = [precision: precision, recall: recall, precisioni: precisionOfInterest, recalli: recallOfInterest,
                       f2:f2, f2i:f2OfInterest]

        exportResult(outputFile1, intersectionResult, metrics)

        metrics
    }

    //para considerar interseção segundo um limite máximo, mas tratando interfaces pequenas
    def generateIntersectionResult(double intersectionRate, double rateForSmallInterfaces){
        intersectionResult = []
        data?.each{ d ->
            def conflictPrediction = 0

            if(rateForSmallInterfaces == -1){ //não deve aplicar a lógica de tratar interfaces pequenas
                if(d.intersection.size()>=intersectionRate) conflictPrediction = 1
            } else {
                if(!isController || (d.testiLeft.size()>0 && d.testiRight.size()>0)){
                    boolean small = false
                    if(d.testiLeft.size()<=INTERFACE_SIZE_LIMIT || d.testiRight.size()<=INTERFACE_SIZE_LIMIT){
                        small = true
                    }

                    if(small){
                        def smaller = d.testiLeft
                        if(d.testiRight.size() < d.testiLeft.size() ) smaller = d.testiRight
                        def truePositivesFromSmaller = smaller.findAll{ it in d.intersection }
                        def rate = 0
                        if(smaller.size()>0) rate = truePositivesFromSmaller.size()/smaller.size()
                        if(rate>=rateForSmallInterfaces) conflictPrediction = 1

                    } else if(d.intersection.size()>=intersectionRate) conflictPrediction = 1
                }
            }

            if(d.testiLeft.size()>0 && d.testiRight.size()>0 ){
                intersectionResult += [project: d.project, left: d.left, right: d.right,
                                       intersection: d.intersection.size(),
                                       conflict: d.conflict,
                                       conflictOfInterest: d.conflictOfInterest,
                                       prediction: conflictPrediction]
            }
        }
    }

    //para considerar resultados apenas para tarefas cujo texti não é vazio
    def generateIntersectionResultForNonEmptyTexti(double intersectionRate){
        intersectionResult = []
        data?.each{ d ->
            def left = noEmptyTestiTasks.find{ it.project== d.project && it.id==d.left }
            def right = noEmptyTestiTasks.find{ it.project== d.project && it.id==d.right }
            if(left && right) {
                def conflictPrediction = 0
                if (d.intersection.size() >= intersectionRate) conflictPrediction = 1
                if (d.testiLeft.size() > 0 && d.testiRight.size() > 0) {
                    intersectionResult += [project           : d.project, left: d.left, right: d.right,
                                           intersection      : d.intersection.size(),
                                           conflict          : d.conflict,
                                           conflictOfInterest: d.conflictOfInterest,
                                           prediction        : conflictPrediction]
                }
            }
        }
    }

    def evaluate(double similarityRate){
        def outputFile1 = "output${File.separator}precison_recall_conflict_intersection"
        if(file.endsWith("-controller.csv")) {
            outputFile1 += "-controller-${similarityRate}.csv"
            isController = true
        }
        else outputFile1 += "-${similarityRate}.csv"

        generateIntersectionResult(similarityRate)

        def precision = calculatePrecision()
        def recall = calculateRecall()
        def f2 = calculateF2()
        def precisionOfInterest = calculatePrecisionForConflictsOfInterest()
        def recallOfInterest = calculateRecallForConflictsOfInterest()
        def f2OfInterest = calculateF2ForConflictsOfInterest()
        def metrics = [precision: precision, recall: recall, precisioni: precisionOfInterest, recalli: recallOfInterest,
                       f2:f2, f2i:f2OfInterest]

        exportResult(outputFile1, intersectionResult, metrics)

        metrics
    }

    //para considerar interseção segundo um limite máximo, sem tratar interfaces pequenas
    def generateIntersectionResult(double intersectionRate){
        generateIntersectionResult(intersectionRate, -1)
    }

    double calculatePrecision() {
        def conflicts = intersectionResult.findAll{ it.conflict == 1 }?.collect{ [it.project, it.left, it.right] }
        def predictedConflicts = intersectionResult.findAll{ it.prediction == 1 }?.collect{ [it.project, it.left, it.right] }
        double result = 0
        if (invalidInput(predictedConflicts, conflicts)) return result
        def truePositives = predictedConflicts.findAll{ it in conflicts }.size()
        if (truePositives > 0) result = (double) truePositives / predictedConflicts.size()
        result
    }

    double calculateRecall() {
        def conflicts = intersectionResult.findAll{ it.conflict == 1 }?.collect{ [it.project, it.left, it.right] }
        def predictedConflicts = intersectionResult.findAll{ it.prediction == 1 }?.collect{ [it.project, it.left, it.right] }
        double result = 0
        if (invalidInput(predictedConflicts, conflicts)) return result
        def truePositives = predictedConflicts.findAll{ it in conflicts }.size()
        if (truePositives > 0) result = (double) truePositives / conflicts.size()
        result
    }

    double calculateF2(){
        def beta = 2
        def precision = calculatePrecision()
        def recall = calculateRecall()
        (beta*beta + 1)*precision*recall / (beta*beta*precision + recall)
    }

    double calculatePrecisionForConflictsOfInterest() {
        def conflictsOfInterest = intersectionResult.findAll{ it.conflictOfInterest == 1 }?.collect{ [it.project, it.left, it.right] }
        def predictedConflicts = intersectionResult.findAll{ it.prediction == 1 }?.collect{ [it.project, it.left, it.right] }
        double result = 0
        if (invalidInput(predictedConflicts, conflictsOfInterest)) return result
        def truePositives = predictedConflicts.findAll{ it in conflictsOfInterest }.size()
        if (truePositives > 0) result = (double) truePositives / predictedConflicts.size()
        result
    }

    double calculateRecallForConflictsOfInterest() {
        def conflictsOfInterest = intersectionResult.findAll{ it.conflictOfInterest == 1 }?.collect{ [it.project, it.left, it.right] }
        def predictedConflicts = intersectionResult.findAll{ it.prediction == 1 }?.collect{ [it.project, it.left, it.right] }
        double result = 0
        if (invalidInput(predictedConflicts, conflictsOfInterest)) return result
        def truePositives = predictedConflicts.findAll{ it in conflictsOfInterest }.size()
        if (truePositives > 0) result = (double) truePositives / conflictsOfInterest.size()
        result
    }

    double calculateF2ForConflictsOfInterest(){
        def beta = 2
        def precision = calculatePrecisionForConflictsOfInterest()
        def recall = calculateRecallForConflictsOfInterest()
        (beta*beta + 1)*precision*recall / (beta*beta*precision + recall)
    }

    private static invalidInput(prediction, conflictResult) {
        if (!prediction || prediction.empty || !conflictResult || conflictResult.empty) true
        else false
    }

    static exportResult(String outputFile, List data, def metrics){
        List<String[]> content = []
        def header = ["Project", "left", "right", "intersection", "conflict", "conflict_of_interest", "prediction"] as String[]
        content += header
        data.each{
            content += [it.project, it.left, it.right, it.intersection, it.conflict, it.conflictOfInterest, it.prediction] as String[]
        }

        println "outputFile: ${outputFile}; content: ${content.size()-1}"

        content += ["precision", metrics.precision] as String []
        content += ["recall", metrics.recall] as String []
        content += ["precision of interest", metrics.precisioni] as String []
        content += ["recall of interest", metrics.recalli] as String []
        content += ["F2 interest", metrics.f2] as String []
        content += ["F2 of interest", metrics.f2i] as String []
        CsvUtil.write(outputFile, content)
    }

    static generateResultForStatisticalAnalysis(String conflictResult){
        println "Generating evaluation result for TestI"
        def evaluator1 = new EvaluatingPrecisionRecallBasedOnIntersection(conflictResult)
        evaluator1.generateIntersectionResult(1)

        def evaluator2 = new EvaluatingPrecisionRecallBasedOnIntersection(conflictResult)
        evaluator2.generateIntersectionResult(2)

        def evaluator3 = new EvaluatingPrecisionRecallBasedOnIntersection(conflictResult)
        evaluator3.generateIntersectionResult(3)

        def evaluator4 = new EvaluatingPrecisionRecallBasedOnIntersection(conflictResult)
        evaluator4.generateIntersectionResult(4)

        def evaluator5 = new EvaluatingPrecisionRecallBasedOnIntersection(conflictResult)
        evaluator5.generateIntersectionResult(5)

        List finalResult = []
        evaluator1.intersectionResult.eachWithIndex{ entry, int i ->
            def project = convertProjectToUrl(entry.project)
            def values = [project, entry.left, entry.right, entry.conflict, entry.conflictOfInterest, entry.prediction]
            def prediction2 = evaluator2.intersectionResult.get(i).prediction
            def prediction3 = evaluator3.intersectionResult.get(i).prediction
            def prediction4 = evaluator4.intersectionResult.get(i).prediction
            def prediction5 = evaluator5.intersectionResult.get(i).prediction
            def predictions = [prediction2, prediction3, prediction4, prediction5]
            finalResult.add((values+predictions).flatten() as String[])
        }
        exportSummarizedResult(finalResult, evaluator1.testiResultForStatisticalAnalysisFile)
    }

    static exportSummarizedResult(List data, String outputFile){
        List<String[]> content = []
        def header = ["PROJECT", "LEFT", "RIGHT", "HAS_CONFLICT", "HAS_CONFLICT_OF_INTEREST",
                      "INTERSECTION_1", "INTERSECTION_2", "INTERSECTION_3", "INTERSECTION_4", "INTERSECTION_5"] as String[]
        content += header
        content += data
        println "outputFile: ${outputFile}; content: ${content.size()-1}"
        CsvUtil.write(outputFile, content)
    }

    private static convertProjectToUrl(String project){
        def url = ""
        switch (project){
            case "allourideas_allourideas.org": url = "https://github.com/allourideas/allourideas.org"
                break
            case "alphagov_e": url = "https://github.com/alphagov/e-petitions"
                break
            case "alphagov_whitehall": url = "https://github.com/alphagov/whitehall"
                break
            case "bthuntercn_bsmi": url = "https://github.com/BTHUNTERCN/bsmi"
                break
            case "dchbx_enroll": url = "https://github.com/dchbx/enroll"
                break
            case "diaspora_diaspora": url = "https://github.com/diaspora/diaspora"
                break
            case "efforg_action": url = "https://github.com/EFForg/action-center-platform"
                break
            case "gitlabhq_gitlabhq": url = "https://github.com/gitlabhq/gitlabhq"
                break
            case "gleneivey_wontomedia": url = "https://github.com/gleneivey/wontomedia"
                break
            case "jekyll_jekyll": url = "https://github.com/jekyll/jekyll"
                break
            case "ministryofjustice_claim": url = "https://github.com/ministryofjustice/Claim-for-Crown-Court-Defence"
                break
            case "oneclickorgs_one": url = "https://github.com/oneclickorgs/one-click-orgs"
                break
            case "opengovernment_opengovernment": url = "https://github.com/opengovernment/opengovernment"
                break
            case "opf_openproject": url = "https://github.com/opf/openproject"
                break
            case "otwcode_otwarchive": url = "https://github.com/otwcode/otwarchive"
                break
            case "rapidftr_rapidftr": url = "https://github.com/rapidftr/RapidFTR"
                break
            case "sachac_quantified": url = "https://github.com/sachac/quantified"
                break
            case "sanger_sequencescape": url = "https://github.com/sanger/sequencescape"
                break
            case "sharetribe_sharetribe": url = "https://github.com/sharetribe/sharetribe"
        }
        url
    }

}
