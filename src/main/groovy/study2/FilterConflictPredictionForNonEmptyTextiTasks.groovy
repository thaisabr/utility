package study2

import br.ufpe.cin.tan.util.CsvUtil

/*
* Calculei TextI para todas as tarefas relevantes. Calculei risco de conflito para todas as tarefas relevantes também.
* Esse script filtra o resultado de risco de conflito a fim de só conter tarefas cujo TextI não é vazio.
* */
class FilterConflictPredictionForNonEmptyTextiTasks {

    static void main(String[] args){
        def noemptyTextiFile = "C:\\Users\\tatab\\Desktop\\texti_given_exclude_nov20\\result_organized\\final-result\\texti-noempty.csv"
        def conflictFile = "C:\\Users\\tatab\\Desktop\\conflict_prediction_given\\output\\conflict.csv"

        def noemptyTextiTasks = CsvUtil.read(noemptyTextiFile)
        noemptyTextiTasks.remove(0)
        def noemptyTextiResult = []
        noemptyTextiTasks.each{
            noemptyTextiResult += [project: it[0], id: it[1]]
        }
        println "noemptyTextiResult: ${noemptyTextiResult.size()}"
        println noemptyTextiResult.get(0)

        def conflictPredictions = CsvUtil.read(conflictFile)
        conflictPredictions.remove(0)
        def conflictPredictionsResult = []
        conflictPredictions.each{
            conflictPredictionsResult += [project: fixProjectName(it[0]), id1: it[1], id2:it[2]]
        }
        println "conflictPredictions: ${conflictPredictions.size()}"
        println conflictPredictions.get(0)

        def conflictPredictionsForNoemptyTexti = []
        noemptyTextiResult.each{ task ->
            println "task: ${task}"
            def candidates = conflictPredictionsResult.findAll{
                (it.project == task.project && (it.id1 == task.id || it.id2 == task.id))
            }
            println "candidates: ${candidates.size()}"
            def predictions = candidates.findAll{ c ->
                def t1 = [project: c.project, id: c.id1]
                def t2 = [project: c.project, id: c.id2]
                (t1 in noemptyTextiResult && t2 in noemptyTextiResult)
            }
            if(!predictions.empty) conflictPredictionsForNoemptyTexti += predictions
        }
        println "conflictPredictionsForNoemptyTexti: ${conflictPredictionsForNoemptyTexti.size()}"
        println conflictPredictionsForNoemptyTexti.get(0)

        def selectedPredictions = CsvUtil.read(conflictFile)
        String[] header = selectedPredictions.get(0)
        selectedPredictions.remove(0)
        List<String[]> data = []
        data += header
        conflictPredictionsForNoemptyTexti.each{ cp ->
            def found = selectedPredictions.find{ fixProjectName(it[0])==cp.project && it[1]==cp.id1 && it[2]==cp.id2 }
            if (found) data += found
            else println "not found: ${cp.project}, ${cp.id1}, ${cp.id2}"
        }
        data = data.unique{ [it[0], it[1], it[2]] }
        println "data: ${data.size()-1}"

        CsvUtil.write("output\\conflict_noemptytexti.csv", data)
    }

    private static fixProjectName(String name){
        def project = name
        def index = name.lastIndexOf("_")
        if(index>-1) project = name.substring(index+1)

        switch (project){
            case "action": project = "action-center-platform"
                break
            case "e" : project = "e-petitions"
                break
            case "concord" : project = "rigse"
                break
            case "claim" : project = "claim-for-crown-court-defence"
                break
            case "one" : project = "one-click-orgs"
        }

        project
    }

}
