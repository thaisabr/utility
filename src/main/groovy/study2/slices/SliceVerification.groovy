package study2.slices

import br.ufpe.cin.tan.test.ruby.routes.Inflector
import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

class SliceVerification {

    static final int PROJECT_INDEX = 0
    static final int LEFT_TASK_INDEX = 1
    static final int RIGHT_TASK_INDEX = 2
    static final int CONFLICT_INDEX = 3
    static final int CONFLIC_OF_INTEREST_INDEX = 5
    static final int TESTI_LEFT = 7
    static final int TESTI_RIGHT = 8
    static final int COMMON_CHANGED_PROD_FILES = 12
    static final int INTERSECTION_INDEX = 23

    static final String MODEL_SLICE = "app/models"
    static final String VIEW_SLICE = "app/views"
    static final String CONTROLLER_SLICE = "app/controllers"
    static final String SHARED_APP = "app/"

    List data
    Inflector inflector

    SliceVerification(String csv){
        data = []
        def lines = CsvUtil.read(csv)
        lines.remove(0)
        lines.each{ line ->
            data += [project: line[PROJECT_INDEX],
                     left: line[LEFT_TASK_INDEX],
                     right: line[RIGHT_TASK_INDEX],
                     conflict: line[CONFLICT_INDEX] as int,
                     conflictOfInterest: line[CONFLIC_OF_INTEREST_INDEX] as int,
                     testiLeft: line[TESTI_LEFT].substring(1, line[TESTI_LEFT].size()-1).tokenize(",")*.trim(),
                     testiRight: line[TESTI_RIGHT].substring(1, line[TESTI_RIGHT].size()-1).tokenize(",")*.trim(),
                     intersection: line[INTERSECTION_INDEX].substring(1, line[INTERSECTION_INDEX].size()-1).tokenize(",")*.trim(),
                     commonChangedProdFiles:  line[COMMON_CHANGED_PROD_FILES].substring(1, line[COMMON_CHANGED_PROD_FILES].size()-1).tokenize(",")*.trim()
            ]
        }
        inflector = new Inflector()
    }

    static void main(String[] args){
        def folder = "D:\\Dropbox\\Thaís\\phd_study2_testi_given\\conflict_prediction\\output\\"
        def file = "${folder}conflict_noemptytexti.csv"

        SliceVerification manager = new SliceVerification(file)
        manager.run()

        def output = CsvUtil.read("output\\slices.csv")
        output.remove(0)
        println "Total: ${output.size()}"
        def tarefasIntersecaoTestiPossuiController = output.findAll{ it[5] == "1" }
        def tarefasQueAlteramMesmoSlice = tarefasIntersecaoTestiPossuiController.findAll{ it[6] == "1" }
        println "Tarefas cujo TestI possui algum controller em comum: ${tarefasIntersecaoTestiPossuiController.size()}"
        println "Dentre elas, tarefas que alteram algum slice em comum: ${tarefasQueAlteramMesmoSlice.size()}"
        println "Proporção (%): ${tarefasQueAlteramMesmoSlice.size()/tarefasIntersecaoTestiPossuiController.size()*100}"
    }

    def run(){
        List<String[]> result = []
        result += ["Project", "Left", "right", "Conflict", "conflictOfInterest", "ControllerIntersectionTesti",
                   "ChangeAnyCommonSlice", "CommonChangedSlices"] as String[]
        CsvUtil.write("output${File.separator}slices.csv", result)
        data?.each{ line ->
            def controllersInTestiIntersection = line.intersection.findAll{ Util.isControllerFile(it) }
            def changeAnyCommonSlice = 0
            def commonChangedSlices = []
            if(!controllersInTestiIntersection.empty){
                def slicesTesti = identifySlices(controllersInTestiIntersection)
                def slices = identifySlices(line.commonChangedProdFiles)
                commonChangedSlices = slicesTesti.controllers.intersect(slices.slices)
                changeAnyCommonSlice = commonChangedSlices.empty ? 0 : 1
            }

            def existControllerIntersectionInTesti = controllersInTestiIntersection.empty ? 0 : 1
            List<String[]> r = []
            r += [line.project, line.left, line.right, line.conflict, line.conflictOfInterest,
                      existControllerIntersectionInTesti, changeAnyCommonSlice, commonChangedSlices] as String[]
            CsvUtil.append("output${File.separator}slices.csv", r)
        }
    }

    private identifySlices(def files){
        def countAppShared = 0
        def countConfig = 0

        def modelsSlices = []
        def viewsSlices = []
        def controllersSlices = []

        files.each { file ->
            if (file.startsWith(MODEL_SLICE)){
                modelsSlices += extractModelName(file)
            }
            else if (file.startsWith(VIEW_SLICE)){
                def tmp = extractViewName(file)
                if (!tmp.empty){
                    viewsSlices += tmp
                }
            }
            else if (file.startsWith(CONTROLLER_SLICE)){
                def tmp = extractControllerName(file)
                controllersSlices += tmp
            }
            else if (file.startsWith(SHARED_APP)) countAppShared++
            else countConfig++
        }

        modelsSlices = modelsSlices.unique()
        viewsSlices = viewsSlices.unique()
        controllersSlices = controllersSlices.unique()

        def slices = (modelsSlices + viewsSlices + controllersSlices).unique()
        def intersectionModelAndView = modelsSlices.intersect(viewsSlices)
        def intersectionModelAndController = modelsSlices.intersect(controllersSlices)

        [models: modelsSlices, views:viewsSlices, controllers:controllersSlices, slices:slices,
         interMV:intersectionModelAndView, interMC:intersectionModelAndController, countAppShared:countAppShared,
         countConfig:countConfig]
    }

    private static extractModelName(file){
        def nomeModelo = file.substring(MODEL_SLICE.size()+1)
        nomeModelo.substring(0, nomeModelo.indexOf("."))
    }

    private extractViewName(file){
        def nomeView = file.substring(VIEW_SLICE.size()+1)
        def tmp = ""
        def index = nomeView.indexOf("/")
        if (index > -1){
            tmp = nomeView.substring(0, index)
        }
        inflector.singularize(tmp)
    }

    private extractControllerName(file){
        def nomeController = file.substring(CONTROLLER_SLICE.size()+1)
        def tmp = nomeController.substring(0, nomeController.indexOf("."))
        if (tmp.contains("_")){
            tmp = tmp.substring(0, tmp.indexOf("_"))
        }
        def index = tmp.lastIndexOf("/")
        if(index>-1) tmp = tmp.substring(index+1)
        inflector.singularize(tmp)
    }

}
