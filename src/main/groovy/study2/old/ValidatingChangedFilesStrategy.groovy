package study2.old

import br.ufpe.cin.tan.util.CsvUtil

/*
* Eu calculava os arquivos alterados por uma tarefa fazendo o diff de cada commit em relação ao seu ancestral.
* Paulo disse para calcular fazendo o diff entre o primeiro e o último commit da tarefa.
* Esse script verifica se os resultados são equivalentes ou não.
* */

class ValidatingChangedFilesStrategy {

    static void main(String[] args){
        def conflictResultIndividualStrategy = CsvUtil.read("error\\diff_individual\\conflict.csv")
        conflictResultIndividualStrategy.remove(0)
        conflictResultIndividualStrategy = conflictResultIndividualStrategy.sort{
            [it[0]]}. sort{ [it[1] as double, it[2] as double] }

        def controllerConflictResultIndividualStrategy = CsvUtil.read("error\\diff_individual\\conflict-controller.csv")
        controllerConflictResultIndividualStrategy.remove(0)
        controllerConflictResultIndividualStrategy = controllerConflictResultIndividualStrategy.sort{
            [it[0]]}. sort{ [it[1] as double, it[2] as double] }

        def conflictResultIntervalStrategy = CsvUtil.read("error\\diff_interval\\conflict.csv")
        conflictResultIntervalStrategy.remove(0)
        conflictResultIntervalStrategy = conflictResultIntervalStrategy.sort{
            [it[0]]}. sort{ [it[1] as double, it[2] as double] }


        def controllerConflictResultIntervalStrategy = CsvUtil.read("error\\diff_interval\\conflict-controller.csv")
        controllerConflictResultIntervalStrategy.remove(0)
        controllerConflictResultIntervalStrategy = controllerConflictResultIntervalStrategy.sort{
            [it[0]]}. sort{ [it[1] as double, it[2] as double] }


        println "conflictResultIndividualStrategy: ${conflictResultIndividualStrategy.size()}"
        println "conflictResultIntervalStrategy: ${conflictResultIntervalStrategy.size()}"

        List<String[]> differentConflictResults = []
        for(int i=0; i<conflictResultIndividualStrategy.size(); i++){
            def oldValue = conflictResultIndividualStrategy.get(i)
            def newValue = conflictResultIntervalStrategy.get(i)
            if(oldValue != newValue) differentConflictResults.add([oldValue[0], oldValue[1], oldValue[2]] as String[])
        }
        println "differentConflictResults: ${differentConflictResults.size()}"
        CsvUtil.write("error\\difference_conflicts.csv", differentConflictResults)

        println "controllerConflictResultIndividualStrategy: ${controllerConflictResultIndividualStrategy.size()}"
        println "controllerConflictResultIntervalStrategy: ${controllerConflictResultIntervalStrategy.size()}"

        List<String[]> differentControllerConflictResults = []
        for(int i=0; i<controllerConflictResultIndividualStrategy.size(); i++){
            def oldValue = controllerConflictResultIndividualStrategy.get(i)
            def newValue = controllerConflictResultIntervalStrategy.get(i)
            if(oldValue != newValue) differentControllerConflictResults.add([oldValue[0], oldValue[1], oldValue[2]] as String[])
        }
        println "differentControllerConflictResults: ${differentControllerConflictResults.size()}"
        CsvUtil.write("error\\difference_controller_conflicts.csv", differentControllerConflictResults)

    }

}
