package study2.old

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

class RefactoringConflictResult {

    static void main(String[] args){
        def files = Util.findFilesFromDirectory("error")

        def header = ["PROJECT","LEFT_TASK","RIGHT_TASK","CONFLICT","CONFLICTING_FILES","CONFLICT_OF_INTEREST",
                      "CONFLICTING_FILES_OF_INTEREST",
                      "SIMJAC_TESTIL_TESTIR","SIMCOS_TESTIL_TESTIR",
                      "LEFT_TESTI", "RIGHT_TESTI",
                      "SIMJAC_TASKIL_IREAL-R","SIMCOS_TASKIL_TASKIR",
                      "SIMJAC_TESTIL_CONFLICTS", "SIMCOS_TESTIL_CONFLICTS",
                      "SIMJAC_TESTIR_CONFLICTS","SIMCOS_TESTIR_CONFLICTS",
                      "SIMJAC_TASKIL_CONFLICTS","SIMCOS_TASKIL_CONFLICTS",
                      "SIMJAC_TASKIR_CONFLICTS", "SIMCOS_TASKIR_CONFLICTS",
                      "#COMMON_CHANGED_FILES","COMMON_CHANGED_FILES",
                      "#COMMON_CHANGED_PROD_FILES","COMMON_CHANGED_PROD_FILES",
                      "#CONFLICTS",
                      "#DIFF_CONFLICTS_TESTIL","DIFF_CONFLICTS_TESTIL",
                      "#DIFF_CONFLICTS_TESTIR", "DIFF_CONFLICTS_TESTIR",
                      "#INTERSECTION_CONFLICTS_TESTIL","INTERSECTION_CONFLICTS_TESTIL",
                      "#INTERSECTION_CONFLICTS_TESTIR","INTERSECTION_CONFLICTS_TESTIR"] as String[]

        files.each{ file ->
            def lines = CsvUtil.read(file)
            lines[0] = header
            def name = file - ".csv" + "2.csv"
            CsvUtil.write(name, lines)
        }

    }


}
