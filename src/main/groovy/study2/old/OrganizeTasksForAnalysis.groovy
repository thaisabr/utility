package study2.old

import br.ufpe.cin.tan.util.Util

import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path

class OrganizeTasksForAnalysis {

    private static createFolder(String folder){
        File folderManager = new File(folder)
        if(folderManager.exists()) Util.deleteFolder(folder)
        else folderManager.mkdir()
    }

    static void main (String[] args){
        def outputFolder = "tasks\\study2-fixed\\paper_sample\\tasks_for_analysis"
        createFolder(outputFolder)

        def folder = "tasks\\study2-fixed\\paper_sample\\task_extraction"
        def subfolder = "\\3-tasks"
        def files = Util.findFilesFromDirectory(folder+subfolder).findAll{
            !it.contains("${File.separator}merges${File.separator}") &&
            it.endsWith("-independent-tasks.csv") &&
            !it.contains("-cucumber-independent-tasks.csv")
        }
        println "Files: ${files.size()}"
        files.each{ file ->
            println file
            Path source = FileSystems.getDefault().getPath(file)
            Path target = FileSystems.getDefault().getPath(outputFolder)
            Files.copy(source, target.resolve(source.getFileName()))
        }

    }

}
