package study2

import br.ufpe.cin.tan.util.CsvUtil

class TempCounter {

    static void main(String[] args){
        def file = "error\\conflict-controller.csv"
        def lines = CsvUtil.read(file)
        println "lines: ${lines.size()-1}"
    }
}
