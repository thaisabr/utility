package study2

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

class TaskExtraction {

    static void main (String[] args){
        def folder = "tasks\\study2-new\\task_extraction_fixed\\3-tasks"
        def file = "tasks\\study2-new\\task_extraction_fixed\\output\\execution.log"
        def lines = new File(file).readLines()
        def linesOfInterest = lines.collect{
            if(it.contains("Project: ") ||
                it.contains("All commits from project: ") ||
                it.contains("All merge commits: ") ||
                it.contains("Fast-fowarding merges: ") ||
                it.contains("Selected merges: ") ||
                it.contains("Found merge tasks: ") ||
                it.contains("Found unique tasks (from merge tasks): ") ||
                it.contains("Found P&T tasks (from unique tasks): ") ||
                it.contains("Found independent tasks (from P&T tasks): ") ||
                it.contains("Found cucumber tasks (from P&T tasks): ") ||
                it.contains("Found cucumber independent tasks: ")) it
            else null
        } - [null]

        println "linesOfInterest: ${linesOfInterest.size()}"
        linesOfInterest.each{ value ->
            def index = lines.indexOf(value) + 1
            println "$index: $value"
        }

        def mergeTasks = linesOfInterest.findAll{
            it.contains("Found merge tasks: ")}
            .collect{ it.split(" ").last().trim() as double }.sum()
        println "mergeTasks: ${mergeTasks}"

        def uniqueTasks = linesOfInterest.findAll{
            it.contains("Found unique tasks (from merge tasks): ")}
            .collect{ it.split(" ").last().trim() as double }.sum()
        println "uniqueTasks: ${uniqueTasks}"

        def ptTasks = linesOfInterest.findAll{
            it.contains("Found P&T tasks (from unique tasks): ")}
            .collect{ it.split(" ").last().trim() as double }.sum()
        println "ptTasks: ${ptTasks}"

        def independentTasks = linesOfInterest.findAll{
            it.contains("Found independent tasks (from P&T tasks): ")}
            .collect{ it.split(" ").last().trim() as double }.sum()
        println "independentTasks: ${independentTasks}"

        def cucumberTasks = linesOfInterest.findAll{
            it.contains("Found cucumber tasks (from P&T tasks): ")}
            .collect{ it.split(" ").last().trim() as double }.sum()
        println "cucumberTasks: ${cucumberTasks}"

        def cucumberIndependentTasks = linesOfInterest.findAll{
            it.contains("Found cucumber independent tasks: ")}
            .collect{ it.split(" ").last().trim() as double }.sum()
        println "cucumberIndependentTasks: ${cucumberIndependentTasks}"

        def files = Util.findFilesFromDirectory(folder).findAll{
            !it.contains("${File.separator}merges${File.separator}") && !it.endsWith("selected-projects.csv")
        }?.sort()

        files.each{ f ->
            List<String[]> content = CsvUtil.read(f)
            content.remove(0)
            def index = f.lastIndexOf(File.separator)
            def name = f.substring(index+1)
            println "File: ${name}; Entries: ${content.size()}"
        }
    }

}
