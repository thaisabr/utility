package study2

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

class TaskAnalysis {

    static void main(String[] args){
        def folder = "C:\\Users\\tatab\\Desktop\\testi_given_exclude_nov20\\result"
        def files = Util.findFilesFromDirectory(folder).findAll{
            it.contains("${File.separator}tasks${File.separator}") &&
                    !it.endsWith("_candidates.csv")
        }

        files.each{ f ->
            List<String[]> content = CsvUtil.read(f)
            if (f.endsWith("-detailed.csv")) content = content.subList(19, content.size())
            else if (f.endsWith("-relevant.csv")) content = content.subList(13, content.size())
            else content = content.subList(1, content.size())
            def index = f.lastIndexOf(File.separator)
            def name = f.substring(index+1)
            println "File: ${name}; Entries: ${content.size()}"
        }
    }

}
