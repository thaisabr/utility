package study2

import br.ufpe.cin.tan.util.CsvUtil


/*
* Script que verifica se a interseção de TestI prediz o arquivo com conflito.
* */
class VerifyConflicts {

    List data
    List result
    String file
    static final int PROJECT_INDEX = 0
    static final int LEFT_TASK_INDEX = 1
    static final int RIGHT_TASK_INDEX = 2
    static final int CONFLICT_INDEX = 3
    static final int CONFLICTING_FILES_INDEX = 4
    static final int CONFLIC_OF_INTEREST_INDEX = 5
    static final int CONFLICTING_FILES_OF_INTEREST_INDEX = 6
    static final int TESTI_LEFT = 7
    static final int TESTI_RIGHT = 8
    static final int INTERSECTION_INDEX = 23

    VerifyConflicts(String file){
        this.file = file
        data = []
        result = []
        def lines = CsvUtil.read(file)
        lines.remove(0)
        lines.each{ line ->
            data += [project: line[PROJECT_INDEX], left: line[LEFT_TASK_INDEX], right: line[RIGHT_TASK_INDEX],
                     conflict: line[CONFLICT_INDEX] as int, conflictOfInterest: line[CONFLIC_OF_INTEREST_INDEX] as int,
                     testiLeft: line[TESTI_LEFT].substring(1, line[TESTI_LEFT].size()-1).tokenize(",")*.trim(),
                     testiRight: line[TESTI_RIGHT].substring(1, line[TESTI_RIGHT].size()-1).tokenize(",")*.trim(),
                     intersectionTesti: line[INTERSECTION_INDEX]
                             .substring(1, line[INTERSECTION_INDEX].size()-1).tokenize(",")*.trim(),
                    conflictingFiles:line[CONFLICTING_FILES_INDEX]
                    .substring(1, line[CONFLICTING_FILES_INDEX].size()-1).tokenize(",")*.trim(),
                    conflictingFilesOfInterest: line[CONFLICTING_FILES_OF_INTEREST_INDEX]
                    .substring(1, line[CONFLICTING_FILES_OF_INTEREST_INDEX].size()-1).tokenize(",")*.trim()]
        }
    }

    def evaluateConflicts(){
        result = []

        data.each{ d ->
            def commonFilesBetweenConflictsAndTestiIntersection = d.conflictingFiles.intersect(d.intersectionTesti)
            def commonFilesBetweenConflictsOfInterestAndTestiIntersection = d.conflictingFilesOfInterest.intersect(d.intersectionTesti)
            def testiReachConflicts = commonFilesBetweenConflictsAndTestiIntersection.empty? 0 : 1
            def testiReachConflictsOfInterest = commonFilesBetweenConflictsOfInterestAndTestiIntersection.empty? 0 : 1

            result += [project: d.project, left: d.left, right: d.right,
                       intersectionTesti: d.intersectionTesti.size(),
                       conflict: d.conflict,
                       conflictingFiles: d.conflictingFiles.size(),
                       testiReachConflicts: testiReachConflicts,
                       reachedConflicts: commonFilesBetweenConflictsAndTestiIntersection,
                       conflictOI: d.conflictOfInterest,
                       conflictingFilesOI: d.conflictingFilesOfInterest.size(),
                       testiReachConflictsOI: testiReachConflictsOfInterest,
                       reachedConflictsOI: commonFilesBetweenConflictsOfInterestAndTestiIntersection]
        }

        def outputFile = "output${File.separator}intersection_conflicts_testi"
        if(file.endsWith("-controller.csv")) outputFile += "-controller.csv"
        else outputFile += ".csv"
        exportResult(outputFile)
    }

    def exportResult(String outputFile){
        List<String[]> content = []
        def header = ["Project", "left", "right", "intersection_testi", "conflict", "conflicting_files",
                      "testi_reach_conflicts", "reached _conflicts",
                      "conflict_of_interest", "conflicting_files_of_interest",
                      "testi_reach_conflicts_of_interest", "reached_conflicts_of_interest"] as String[]
        content += header
        result.each{
            content += [it.project, it.left, it.right, it.intersectionTesti, it.conflict, it.conflictingFiles,
                        it.testiReachConflicts, it.reachedConflicts, it.conflictOI, it.conflictingFilesOI,
                        it.testiReachConflictsOI, it.reachedConflictsOI] as String[]
        }

        println "outputFile: ${outputFile}; content: ${content.size()-1}"
        CsvUtil.write(outputFile, content)
    }

    static void main(String[] args) {
        def folder = "C:\\Users\\tatab\\Desktop\\conflict_prediction_given\\output\\"

        def verifier = new VerifyConflicts("${folder}conflict_noemptytexti.csv")
        verifier.evaluateConflicts()
    }

}
