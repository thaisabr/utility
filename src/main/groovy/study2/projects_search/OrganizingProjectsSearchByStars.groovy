package study2.projects_search

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

class OrganizingProjectsSearchByStars {

    static void main(String[] args){
        def folder = "D:\\Dropbox\\Thaís\\phd_study2\\project_search_and_filtering"
        def files = Util.findFilesFromDirectory(folder).findAll{
            !it.contains("project-search-cucumber-site") && !it.contains("project-search-last-update")
        }
        files = files - "${folder}\\candidate-projects.csv"
        files = files - "${folder}\\lembrete.txt"

        def otherFiles = Util.findFilesFromDirectory(folder).findAll{
            it.contains("project-search-cucumber-site") || it.contains("project-search-last-update")
        }

        def entryProjectsSet = []
        def entryProjectsFiles = files.findAll{
            it.endsWith("\\1-github\\projects.csv")
        }
        entryProjectsFiles.each{
            def entryProjects = CsvUtil.read(it)
            entryProjects.remove(0)
            entryProjectsSet += entryProjects
        }
        entryProjectsSet = entryProjectsSet.unique()
        println "entryProjectsSet by stars: ${entryProjectsSet.size()}"

        def railsSet = []
        def railsProjectsFiles = files.findAll{
            it.endsWith("\\2-filtered\\rails-projects.csv")
        }
        railsProjectsFiles.each{
            def entryProjects = CsvUtil.read(it)
            entryProjects.remove(0)
            railsSet += entryProjects
        }
        railsSet = railsSet.unique()
        println "rails by stars: ${railsSet.size()}"

        def cucumberset = []
        def cucumberProjectsFiles = files.findAll{
            it.endsWith("\\2-filtered\\cucumber-projects.csv")
        }
        cucumberProjectsFiles.each{
            def entryProjects = CsvUtil.read(it)
            entryProjects.remove(0)
            cucumberset += entryProjects
        }
        cucumberset = cucumberset.unique()
        println "cucumber by stars: ${cucumberset.size()}"


        def entryProjectsFileLastUpdate = otherFiles.find{
            it.endsWith("\\project-search-last-update\\1-github\\projects.csv")
        }
        def entryProjectsSetLastUpdate = CsvUtil.read(entryProjectsFileLastUpdate)
        entryProjectsSetLastUpdate.remove(0)

        def entryProjectsFileCucumberSite = otherFiles.find{
            it.endsWith("\\project-search-cucumber-site\\candidate-projects.csv")
        }
        def entryProjectsCucumberSite = CsvUtil.read(entryProjectsFileCucumberSite)
        entryProjectsCucumberSite.remove(0)

        def uniqueEntryProjectsSet = (entryProjectsSet + entryProjectsSetLastUpdate + entryProjectsCucumberSite).unique()
        uniqueEntryProjectsSet = uniqueEntryProjectsSet.collect{ it[0] }.unique()
        println "uniqueEntryProjectsSet: ${uniqueEntryProjectsSet.size()}"

        def railsProjectsFileLastUpdate = otherFiles.find{
            it.endsWith("\\project-search-last-update\\2-filtered\\rails-projects.csv")
        }
        def railsSetLastUpdate = CsvUtil.read(railsProjectsFileLastUpdate)
        railsSetLastUpdate.remove(0)

        def railsFileCucumberSite = otherFiles.find{
            it.endsWith("\\project-search-cucumber-site\\candidate-projects.csv")
        }
        def railsCucumberSite = CsvUtil.read(railsFileCucumberSite)
        railsCucumberSite.remove(0)

        def uniqueRailsSet = (railsSet + railsSetLastUpdate + railsCucumberSite).unique()
        uniqueRailsSet = uniqueRailsSet.collect{ it[0] }.unique()
        println "uniqueRailsSet: ${uniqueRailsSet.size()}"

        def cucumberFileLastUpdate = otherFiles.find{
            it.endsWith("\\project-search-last-update\\2-filtered\\cucumber-projects.csv")
        }
        def cucumberSetLastUpdate = CsvUtil.read(cucumberFileLastUpdate)
        cucumberSetLastUpdate.remove(0)

        def cucumberFileCucumberSite = otherFiles.find{
            it.endsWith("\\project-search-cucumber-site\\candidate-projects.csv")
        }
        def cucumberCucumberSetSite = CsvUtil.read(cucumberFileCucumberSite)
        cucumberCucumberSetSite.remove(0)

        def uniqueCucumberSet = (cucumberset + cucumberSetLastUpdate + cucumberCucumberSetSite).unique()
        uniqueCucumberSet = uniqueCucumberSet.collect{ it[0] }.unique()
        println "uniqueCucumberSet: ${uniqueCucumberSet.size()}"
    }
}
