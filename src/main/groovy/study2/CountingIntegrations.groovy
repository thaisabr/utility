package study2

import br.ufpe.cin.tan.util.CsvUtil

/*
* Script que resume a quantidade de integrações, conflitos, não conflitos, conflitos de interesse e tarefas integradas,
* por projeto.
* */
class CountingIntegrations {

    static printResult(String file){
        println "conflict task file: ${file}"
        def lines = CsvUtil.read(file)
        lines.remove(0)

        def resultPerProject = lines.groupBy { it[0] }
        resultPerProject.each{ rpp ->
            def left = rpp.value.collect{ it[1] }
            def right = rpp.value.collect{ it[2] }
            def tasks = (left+right).unique()

            def allConflicts = rpp.value.findAll{ it[3] == "1"}.size()
            def allNoConflicts = rpp.value.findAll{ it[3] == "0"}.size()
            def conflictsOfInterest = rpp.value.findAll{ it[5] == "1"}.size()

            //projeto; total de integrações; total de conflitos; total de não conflitos; conflitos de interesse
            println "project: ${rpp.key}; integrations: ${rpp.value.size()}; conflicts: ${allConflicts};" +
                    "not conflicts: ${allNoConflicts}; conflicts of interest: ${conflictsOfInterest}; " +
                    "tasks: ${tasks.size()}"
        }

    }

    static void main(String[] args){
        /*println "MERGE RESULT"
        def folder1 = "D:\\Dropbox\\Thaís\\phd_study2\\new_sample\\merge_conflict_analysis_refactored\\output\\"
        //def folder1 = "D:\\Dropbox\\Thaís\\phd_study2\\paper_sample\\merge_conflict_analysis_refactored\\output\\"

        def file1 = "${folder1}conflict.csv"
        printResult(file1)*/

        println "COMMON CHANGED FILES RESULT"
        def folder2 = "C:\\Users\\tatab\\Desktop\\conflict_prediction_given\\output\\"
        def file2 = "${folder2}conflict_noemptytexti.csv"
        printResult(file2)

    }

}
