package study2.texti

import br.ufpe.cin.tan.util.Util
import groovy.util.logging.Slf4j

/*
* Antes executar JoinCsv para organizar o resultado da análise (output org)
* */
@Slf4j
class Main {

    static void main(String[] args){
        def renameFiles = false
        def moveTextFiles = false
        def calculateTexti = false
        def organizeTextiResult = true

        def mainFolder = "D:\\Onedrive\\Documentos\\phd_study2_final_result\\texti-timestamp"
        def finalAnalysisResult = "${mainFolder}\\result_organized\\final-result"
        def mainTextFolder = "${mainFolder}\\result_organized\\text"

        //renomeando arquivos para retirar o termo "-independent-tasks"
        if(renameFiles) Main.renameFiles(finalAnalysisResult)

        //movendo os arquivos text para pasta de resultados organizados
        if(moveTextFiles) {
            def sourceResultFolder = "${mainFolder}\\result"
            def organizedFolder = "${mainFolder}\\result_organized"
            def organizer = new OrganizeTextFiles(sourceResultFolder, organizedFolder)
            organizer.run()
        }

        //calculando TextI
        if(calculateTexti) {
            def relevantFiles = Util.findFilesFromDirectory(finalAnalysisResult).findAll {
                it.endsWith("-relevant.csv")
            }
            relevantFiles.each { file ->
                log.info "file: $file"
                TextI iText = new TextI(file, mainTextFolder)
                iText.generate()
            }
        }

        if(organizeTextiResult) {
            TextInterfaceOrganizer evaluator = new TextInterfaceOrganizer(finalAnalysisResult)
            evaluator.summarize()
        }
    }

    static renameFiles(String inputFolder){
        def files = Util.findFilesFromDirectory(inputFolder)
        files.each{ file ->
            File obj = new File(file)
            def newname = file - "-independent-tasks"
            obj.renameTo(newname)
        }
    }

}
