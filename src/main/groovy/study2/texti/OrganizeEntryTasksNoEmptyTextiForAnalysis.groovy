package study2.texti

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

/* Organiza csvs de tarefas para serem analisadas tomando por base o resultado de Texti, a fim de gerar Testi apenas para
tarefas cujo Texti não seja vazio.
Devem ser fornecidos 2 csv de entrada: taskfile e Texti somente não vazia*/

class OrganizeEntryTasksNoEmptyTextiForAnalysis {

    List<String> entryTasksFiles
    List entryTasks
    List noemptyTextiTasks
    List<String[]> matches
    String[] header

    OrganizeEntryTasksNoEmptyTextiForAnalysis(String entryTasksFolder,String noemptyTextiFile){
        this.entryTasksFiles = Util.findFilesFromDirectory(entryTasksFolder)
        entryTasks = []
        extractNoemptyTextiTasks(noemptyTextiFile)
        matches = []
    }

    def extractNoemptyTextiTasks(String file){
        noemptyTextiTasks = []
        def lines = CsvUtil.read(file)
        lines.remove(0)
        noemptyTextiTasks += lines
    }

    def extractEntryTasks(){
        entryTasks = []
        header = null
        entryTasksFiles.each{ file ->
            def lines = CsvUtil.read(file)
            if(header==null) header = lines.get(0)
            lines.remove(0)
            entryTasks += lines
        }
        println "entryTasks: ${entryTasks.size()}"

        /*def urls = entryTasks.collect{ it[0] }.unique().sort()
        println "urls: ${urls.size()}"
        urls.each{ println it }

        def projects = urls.collect{ formatProjectName(it) }
        println "projects: ${projects.size()}"
        projects.each{ println it }*/
    }

    def findEntryTaskMatchingSelectedTask(){
        matches = []
        noemptyTextiTasks.each{ task ->
            def temp = entryTasks.find{
                formatProjectName(it[0])==task[0] && it[1]==task[1]
            }
            if(temp) matches += temp
        }
        println "matches: ${matches.size()}"
    }

    static formatProjectName(String project){
        String name = (project - ".git").toLowerCase()
        def index = name.lastIndexOf("/")
        name.substring(index+1)
    }

    def exportMatches(){
        List<String[]> content = []
        content += header
        content += matches
        CsvUtil.write("output\\tasks_noemptyTexti.csv", content)
    }

    def generateEntryTasksFileFilteredByMatches(){
        entryTasksFiles.each{ file ->
            def lines = CsvUtil.read(file)
            List<String[]> content = []
            List<String[]> matchesInFile = []

            matches.each{ m ->
                def found = lines.find{ it == m }
                if(found) matchesInFile += found
            }

            if(!matchesInFile.empty){
                content += header
                content += matchesInFile
                def index = file.lastIndexOf(File.separator)
                def filename = file.substring(index+1)
                println "$filename: ${matchesInFile.size()}"
                CsvUtil.write("output\\tasks-noemptyTexti\\${filename}", content)
            }
        }
    }

    def run(){
        extractEntryTasks()
        findEntryTaskMatchingSelectedTask()
        exportMatches()
        //generateEntryTasksFileFilteredByMatches()
    }

    static void main(String[] args){
        def entryTasksFolder = "D:\\Dropbox\\Thaís\\phd_study2_fixed_result\\conflict\\tasks\\taskfile"
        def noemptyTextiFile = "D:\\Dropbox\\Thaís\\phd_study2_fixed_result\\texti" +
                "\\testi_selected_changeset_fixed\\result_organized\\final-result\\texti-noempty.csv"
        def organizer = new OrganizeEntryTasksNoEmptyTextiForAnalysis(entryTasksFolder, noemptyTextiFile)
        organizer.run()
    }
}
