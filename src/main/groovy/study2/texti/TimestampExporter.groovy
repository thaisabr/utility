package study2.texti

import br.ufpe.cin.tan.util.CsvUtil
import groovy.time.TimeCategory
import groovy.time.TimeDuration
import groovy.util.logging.Slf4j

@Slf4j
class TimestampExporter {

    String filename
    String folder
    String[] csvHeader
    TextI texti
    List data
    String noemptyTextIResultFile

    TimestampExporter(String filename, TextI texti) {
        this.filename = filename
        this.texti = texti
        configureCsvHeader()
        def index = filename.lastIndexOf(File.separator)
        folder = filename.substring(0, index)
        this.data = []
        this.noemptyTextIResultFile = "D:\\Onedrive\\Documentos\\phd_study2_final_result\\texti-timestamp" +
                "\\result_organized\\final-result\\texti-noempty.csv"
    }

    def save(){
        /*println "timestampComputingTaskPairs"
        save(texti.timestampComputingTaskPairs, "${filename}-timestamp-taskPairs.csv")
        println "timestampComputingSimilarity"
        save(texti.timestampComputingSimilarity, "${filename}-timestamp-similarity.csv")
        println "timestampRankingSimilarity"
        save(texti.timestampRankingSimilarity, "${filename}-timestamp-ranking.csv")
        println "timestampComputingIntersection"
        save(texti.timestampComputingIntersection, "${filename}-timestamp-intersection.csv")*/
        organizeData()
        saveData("${filename}-timestamp.csv")
    }

    private organizeData(){
        log.info "texti.timestampComputingTaskPairs: ${texti.timestampComputingTaskPairs.size()}"
        texti.timestampComputingTaskPairs.each{ log.info it.toString()}
        log.info "texti.timestampComputingSimilarity: ${texti.timestampComputingSimilarity.size()}"
        texti.timestampComputingSimilarity.each{ log.info it.toString()}
        log.info "texti.timestampRankingSimilarity: ${texti.timestampRankingSimilarity.size()}"
        texti.timestampRankingSimilarity.each{ log.info it.toString()}
        log.info "texti.timestampComputingIntersection: ${texti.timestampComputingIntersection.size()}"
        texti.timestampComputingIntersection.each{ log.info it.toString()}

        if(texti.timestampComputingTaskPairs.empty || texti.timestampComputingSimilarity.empty ||
        texti.timestampRankingSimilarity.empty || texti.timestampComputingIntersection.empty) return

        def project = ""
        if(!texti.timestampComputingTaskPairs.empty) project = texti.timestampComputingTaskPairs.first().project
        else if(!texti.timestampComputingSimilarity.empty) project = texti.timestampComputingSimilarity.first().project
        else if(!texti.timestampRankingSimilarity.empty) project = texti.timestampRankingSimilarity.first().project
        else if (!texti.timestampComputingIntersection.empty) project = texti.timestampComputingIntersection.first().project

        if(project == null || project.empty) return

        def finalTaskSet = CsvUtil.read(noemptyTextIResultFile)
        finalTaskSet.remove(0)
        finalTaskSet = finalTaskSet.findAll{ it[0] == project }
        log.info "finalTaskSet: ${finalTaskSet.size()}"
        finalTaskSet.each{ entry->
            log.info "entry: ${entry[0]}, ${entry[1]}"
            def t1 = texti.timestampComputingTaskPairs.find{ it.project == entry[0] && it.task == entry[1] }
            def t2 = texti.timestampComputingSimilarity.find{ it.project == entry[0] && it.task == entry[1] }
            def t3 = texti.timestampRankingSimilarity.find{ it.project == entry[0] && it.task == entry[1] }
            def t4 = texti.timestampComputingIntersection.find{ it.project == entry[0] && it.task == entry[1] }

            t1 = t1==null? new TimeDuration(0, 0, 0, 0) : t1.timestamp
            t2 = t2==null? new TimeDuration(0, 0, 0, 0) : t2.timestamp
            t3 = t3==null? new TimeDuration(0, 0, 0, 0) : t3.timestamp
            t4 = t4==null? new TimeDuration(0, 0, 0, 0) : t4.timestamp

            TimeDuration totalTimestamp = null
            use(TimeCategory) {
                totalTimestamp = t1 + t2 + t3 + t4
            }
            data += [entry[0], entry[1], entry[2], entry[3], entry[4], entry[5], totalTimestamp] as String[]
        }
    }

    def save(list, filename) {
        if (!list || list.empty) return
        List<String[]> content = []
        content += csvHeader
        list?.each {
            println it
            content += [it.project, it.task, it.timestamp] as String[]
        }
        CsvUtil.write(filename, content)
    }

    def saveData(filename) {
        if (!data || data.empty) return
        List<String[]> content = []
        data?.each {
            println it
            content += it as String[]
        }
        CsvUtil.write(filename, content)
    }

    private configureCsvHeader(){
        csvHeader = ["Project", "Task", "Timestamp"] as String[]
    }

}
