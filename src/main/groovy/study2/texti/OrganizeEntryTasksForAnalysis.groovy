package study2.texti

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

/* Organiza csvs de tarefas para serem analisadas tomando por base o resultado anterior, em que as tarefas foram selecionadas
* e há múltiplas pastas de resultados.
* Devem ser fornecidos 2 csv de entrada: taskfile e pairs, os inputs para a análise de risco de conflito.*/

class OrganizeEntryTasksForAnalysis {

    List<String> entryTasksFiles
    List<String> selectedTasksFiles
    List entryTasks
    List selectedTasks
    List<String[]> matches
    String[] header

    OrganizeEntryTasksForAnalysis(String entryTasksFolder, String selectedTasksFolder){
        this.entryTasksFiles = Util.findFilesFromDirectory(entryTasksFolder)?.findAll{
            !it.endsWith("opf_openproject-ce-independent-tasks")
        }
        this.selectedTasksFiles = Util.findFilesFromDirectory(selectedTasksFolder)?.findAll{
            !it.endsWith("opf_openproject-ce-independent-tasks-pairs.csv")
        }
        entryTasks = []
        selectedTasks = []
        matches = []
    }

    def extractEntryTasks(){
        entryTasks = []
        header = null
        entryTasksFiles.each{ file ->
            def lines = CsvUtil.read(file)
            if(header==null) header = lines.get(0)
            lines.remove(0)
            entryTasks += lines
        }
        println "entryTasks: ${entryTasks.size()}"
        //entryTasks.each{ println "${it[0]}, ${it[1]}" }
    }

    def extractSelectedTasks(){
        selectedTasks = []
        selectedTasksFiles.each { file ->
            def lines = CsvUtil.read(file)
            lines.remove(0)

            def tasks = []
            def url = ""
            if(lines.size()>0){
                url = lines.first()[0]
                def tasks1 = lines.collect{ it[1] }
                def tasks2 = lines.collect{ it[2].substring(1, it[2].size()-1).tokenize(",")*.trim() }
                tasks = (tasks1 + tasks2).flatten().unique()
            }

            selectedTasks += tasks.collect{ [url, it] }
        }
        println "selectedTasks: ${selectedTasks.size()}"
        //selectedTasks.each{ println it }
    }

    def findEntryTaskMatchingSelectedTask(){
        matches = []
        selectedTasks.each{ task ->
            def temp = entryTasks.find{
                it[0]==task[0] && it[1]==task[1]
            }
            if(temp) matches += temp
        }
        println "matches: ${matches.size()}"
    }

    def exportMatches(){
        List<String[]> content = []
        content += header
        content += matches
        CsvUtil.write("output\\selected_tasks.csv", content)
    }

    def generateEntryTasksFileFilteredByMatches(){
        entryTasksFiles.each{ file ->
            def lines = CsvUtil.read(file)
            List<String[]> content = []
            List<String[]> matchesInFile = []

            matches.each{ m ->
                def found = lines.find{ it == m }
                if(found) matchesInFile += found
            }

            if(!matchesInFile.empty){
                content += header
                content += matchesInFile
                def index = file.lastIndexOf(File.separator)
                def filename = file.substring(index+1)
                println "$filename: ${matchesInFile.size()}"
                CsvUtil.write("output\\tasks\\${filename}", content)
            }
        }
    }

    def run(){
        extractEntryTasks()
        extractSelectedTasks()
        findEntryTaskMatchingSelectedTask()
        exportMatches()
        generateEntryTasksFileFilteredByMatches()
    }

    static void main(String[] args){
        def entryTasksFolder = "D:\\Dropbox\\Thaís\\phd_study2_fixed_result\\conflict\\tasks\\taskfile"
        def selectedTasksFolder = "D:\\Dropbox\\Thaís\\phd_study2_fixed_result\\conflict\\tasks\\pairs"
        def organizer = new OrganizeEntryTasksForAnalysis(entryTasksFolder, selectedTasksFolder)
        organizer.run()
    }
}
