package study2.texti

import br.ufpe.cin.tan.util.CsvUtil

class FormattingCsvForStatisticalEvaluation {

    static extractConflictsNumber(String inputfile, String outputfile){
        def lines = CsvUtil.read(inputfile)
        lines.remove(0)
        println "lines: ${lines.size()}"
        List<String[]> content = []
        content += ["CONFLICTS_NUMBER", "CONFLICTS_OF_INTEREST_NUMBER", "INTERSECTION_SIZE", "HAS_CONFLICT", "HAS_CONFLICT_OF_INTEREST",
        "INTERSECTION_1", "INTERSECTION_2", "INTERSECTION_3", "INTERSECTION_4", "INTERSECTION_5"] as String[]
        content += lines.collect{
            def conflictsNumber = it[4].substring(1, it[4].size()-1).tokenize(",")*.trim().size()
            def conflictsOfInterestNumber = it[6].substring(1, it[6].size()-1).tokenize(",")*.trim().size()
            def intersectionSize = it[22]
            def hasConflict = it[3]
            def hasConflictOfInterest = it[5]
            def intersectionLimit1 = (intersectionSize as int) >= 1? 1 : 0 as String
            def intersectionLimit2 = (intersectionSize as int) >= 2? 1 : 0 as String
            def intersectionLimit3 = (intersectionSize as int) >= 3? 1 : 0 as String
            def intersectionLimit4 = (intersectionSize as int) >= 4? 1 : 0 as String
            def intersectionLimit5 = (intersectionSize as int) >= 5? 1 : 0 as String
            [conflictsNumber, conflictsOfInterestNumber, intersectionSize, hasConflict, hasConflictOfInterest,
             intersectionLimit1, intersectionLimit2, intersectionLimit3, intersectionLimit4, intersectionLimit5] as String[]
        }
        println "content: ${content.size()-1}"
        CsvUtil.write(outputfile, content)
    }

    static void main (String[] args){
        def inputfile1 = "error\\conflict.csv"
        def outputfile1 = "error\\conflictNumber_intersectionSize.csv"
        extractConflictsNumber(inputfile1, outputfile1)

        def inputfile2 = "error\\conflict-controller.csv"
        def outputfile2 = "error\\conflictNumber_intersectionSize-controller.csv"
        extractConflictsNumber(inputfile2, outputfile2)
    }

}
