package study2.texti

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

/* Script para contar a quantidade de tarefas para as quais calculamos TextI, separando o caso de TextI ser vazia ou não.
* Na análise estatística, só admitimos TextI não vazia. */
class CoutingTasksWithTexti {


    static void main(String[] args){
        def sufix1 = "texti.csv"
        def sufix2 = "texti-noempty.csv"
        def folder = "C:\\Users\\tatab\\Desktop\\texti_given_exclude_nov20\\result_organized\\final-result"

        def texti = Util.findFilesFromDirectory(folder).find{ it.endsWith(sufix1) }
        println "File: $texti"
        def lines = CsvUtil.read(texti)
        lines.remove(0)
        println "texti: ${lines.size()}"

        def textiNoEmpty = Util.findFilesFromDirectory(folder).find{ it.endsWith(sufix2) }
        println "File: $textiNoEmpty"
        lines = CsvUtil.read(textiNoEmpty)
        lines.remove(0)
        println "texti no empty: ${lines.size()}"

        def resultPerProject = lines.groupBy { it[0] }
        resultPerProject.each{
            println "${it.key}: ${it.value.size()}"
        }


    }

}
