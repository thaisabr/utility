package study2.texti

import br.ufpe.cin.tan.util.CsvUtil

class EvaluatingPrecisionAndRecall {

    List conflictData
    List textiData
    List intersectionResult
    String conflictFile
    String textiFile
    String textiResultFile
    String textiTestiResultFile
    String textiResultForStatisticalAnalysisFile
    String hybridTextiResultForStatisticalAnalysisFile

    static final int PROJECT_INDEX = 0
    static final int LEFT_TASK_INDEX = 1
    static final int RIGHT_TASK_INDEX = 2
    static final int CONFLICT_INDEX = 3
    static final int CONFLIC_OF_INTEREST_INDEX = 5
    static final int TESTI_LEFT = 7
    static final int TESTI_RIGHT = 8
    static final int INTERSECTION_INDEX = 23

    EvaluatingPrecisionAndRecall(String conflictResult, String textiFile){
        this.conflictFile = conflictResult
        this.textiFile = textiFile
        this.textiResultFile = "output${File.separator}texti_precision_recall_"
        this.textiTestiResultFile = "output${File.separator}texti_testi_precision_recall_"
        this.textiResultForStatisticalAnalysisFile = "output${File.separator}texti_evaluation.csv"
        this.hybridTextiResultForStatisticalAnalysisFile = "output${File.separator}texti_hybrid_evaluation.csv"
        this.intersectionResult = []
        extractConflictData()
        extractTextiData()
    }

    static void main(String[] args){
        //def folder = "D:\\Dropbox\\Thaís\\phd_study2_final_result\\"
        def conflictResult = "C:\\Users\\tatab\\Desktop\\conflict_prediction_given\\output\\conflict_noemptytexti.csv"
        def textiFile = "C:\\Users\\tatab\\Desktop\\texti_given_exclude_nov20\\result_organized\\final-result\\texti-noempty.csv"

        generateResultForStatisticalAnalysis(conflictResult, textiFile)
        generateResultHybridInterfacesForStatisticalAnalysis(conflictResult, textiFile)

        def evaluator1 = new EvaluatingPrecisionAndRecall(conflictResult, textiFile)
        def metrics1 = evaluator1.evaluateTexti(1)
        def metrics2 = evaluator1.evaluateTexti(2)
        def metrics3 = evaluator1.evaluateTexti(3)
        def metrics4 = evaluator1.evaluateTexti(4)
        def metrics5 = evaluator1.evaluateTexti(5)
        println "conflict 1 (TextI): ${metrics1}"
        println "conflict 2 (TextI): ${metrics2}"
        println "conflict 3 (TextI): ${metrics3}"
        println "conflict 4 (TextI): ${metrics4}"
        println "conflict 5 (TextI): ${metrics5}"

        def evaluator2 = new EvaluatingPrecisionAndRecall(conflictResult, textiFile)
        metrics1 = evaluator2.evaluateTextiCombinedWithTesti(1)
        metrics2 = evaluator2.evaluateTextiCombinedWithTesti(2)
        metrics3 = evaluator2.evaluateTextiCombinedWithTesti(3)
        metrics4 = evaluator2.evaluateTextiCombinedWithTesti(4)
        metrics5 = evaluator2.evaluateTextiCombinedWithTesti(5)
        println "conflict 1 (Hybrid TextI): ${metrics1}"
        println "conflict 2 (Hybrid TextI): ${metrics2}"
        println "conflict 3 (Hybrid TextI): ${metrics3}"
        println "conflict 4 (Hybrid TextI): ${metrics4}"
        println "conflict 5 (Hybrid TextI): ${metrics5}"
    }

    static generateResultForStatisticalAnalysis(String conflictResult, String textiFile){
        println "Generating evaluation result for TextI"
        def evaluator1 = new EvaluatingPrecisionAndRecall(conflictResult, textiFile)
        evaluator1.generateIntersectionResultForTexti(1)

        def evaluator2 = new EvaluatingPrecisionAndRecall(conflictResult, textiFile)
        evaluator2.generateIntersectionResultForTexti(2)

        def evaluator3 = new EvaluatingPrecisionAndRecall(conflictResult, textiFile)
        evaluator3.generateIntersectionResultForTexti(3)

        def evaluator4 = new EvaluatingPrecisionAndRecall(conflictResult, textiFile)
        evaluator4.generateIntersectionResultForTexti(4)

        def evaluator5 = new EvaluatingPrecisionAndRecall(conflictResult, textiFile)
        evaluator5.generateIntersectionResultForTexti(5)

        List finalResult = []
        evaluator1.intersectionResult.eachWithIndex{ entry, int i ->
            def project = convertTextProjectToUrl(entry.project)
            def values = [project, entry.left, entry.right, entry.conflict, entry.conflictOfInterest, entry.prediction]
            def prediction2 = evaluator2.intersectionResult.get(i).prediction
            def prediction3 = evaluator3.intersectionResult.get(i).prediction
            def prediction4 = evaluator4.intersectionResult.get(i).prediction
            def prediction5 = evaluator5.intersectionResult.get(i).prediction
            def predictions = [prediction2, prediction3, prediction4, prediction5]
            finalResult.add((values+predictions).flatten() as String[])
        }
        exportResult(finalResult, evaluator1.textiResultForStatisticalAnalysisFile)
    }

    static generateResultHybridInterfacesForStatisticalAnalysis(String conflictResult, String textiFile){
        println "Generating evaluation result for hybrid TextI"
        def evaluator1 = new EvaluatingPrecisionAndRecall(conflictResult, textiFile)
        evaluator1.generateIntersectionResultForTextiCombinedWithTesti(1)

        def evaluator2 = new EvaluatingPrecisionAndRecall(conflictResult, textiFile)
        evaluator2.generateIntersectionResultForTextiCombinedWithTesti(2)

        def evaluator3 = new EvaluatingPrecisionAndRecall(conflictResult, textiFile)
        evaluator3.generateIntersectionResultForTextiCombinedWithTesti(3)

        def evaluator4 = new EvaluatingPrecisionAndRecall(conflictResult, textiFile)
        evaluator4.generateIntersectionResultForTextiCombinedWithTesti(4)

        def evaluator5 = new EvaluatingPrecisionAndRecall(conflictResult, textiFile)
        evaluator5.generateIntersectionResultForTextiCombinedWithTesti(5)

        List finalResult = []
        evaluator1.intersectionResult.eachWithIndex{ entry, int i ->
            def project = convertTextProjectToUrl(entry.project)
            def values = [project, entry.left, entry.right, entry.conflict, entry.conflictOfInterest, entry.prediction]
            def prediction2 = evaluator2.intersectionResult.get(i).prediction
            def prediction3 = evaluator3.intersectionResult.get(i).prediction
            def prediction4 = evaluator4.intersectionResult.get(i).prediction
            def prediction5 = evaluator5.intersectionResult.get(i).prediction
            def predictions = [prediction2, prediction3, prediction4, prediction5]
            finalResult.add((values+predictions).flatten() as String[])
        }
        exportResult(finalResult, evaluator1.hybridTextiResultForStatisticalAnalysisFile)
    }

    static exportResult(List data, String outputFile){
        List<String[]> content = []
        def header = ["PROJECT", "LEFT", "RIGHT", "HAS_CONFLICT", "HAS_CONFLICT_OF_INTEREST",
                      "INTERSECTION_1", "INTERSECTION_2", "INTERSECTION_3", "INTERSECTION_4", "INTERSECTION_5"] as String[]
        content += header
        content += data
        println "outputFile: ${outputFile}; content: ${content.size()-1}"
        CsvUtil.write(outputFile, content)
    }

    def evaluateTexti(double intersection){
        generateIntersectionResultForTexti(intersection)

        def precision = calculatePrecision()
        def recall = calculateRecall()
        def f2 = calculateF2()
        def precisionOfInterest = calculatePrecisionForConflictsOfInterest()
        def recallOfInterest = calculateRecallForConflictsOfInterest()
        def f2OfInterest = calculateF2ForConflictsOfInterest()
        def metrics = [precision: precision, recall: recall, precisioni: precisionOfInterest, recalli: recallOfInterest,
                       f2:f2, f2i:f2OfInterest]

        textiResultFile = textiResultFile + "${intersection}.csv"
        exportResult(textiResultFile, intersectionResult, metrics)
        textiResultFile = "output${File.separator}texti_precision_recall_"

        metrics
    }

    def evaluateTextiCombinedWithTesti(double intersection){
        generateIntersectionResultForTextiCombinedWithTesti(intersection)

        def precision = calculatePrecision()
        def recall = calculateRecall()
        def f2 = calculateF2()
        def precisionOfInterest = calculatePrecisionForConflictsOfInterest()
        def recallOfInterest = calculateRecallForConflictsOfInterest()
        def f2OfInterest = calculateF2ForConflictsOfInterest()
        def metrics = [precision: precision, recall: recall, precisioni: precisionOfInterest, recalli: recallOfInterest,
                       f2:f2, f2i:f2OfInterest]

        textiTestiResultFile = textiTestiResultFile + "${intersection}.csv"
        exportResult(textiTestiResultFile, intersectionResult, metrics)
        textiTestiResultFile = "output${File.separator}texti_testi_precision_recall_"

        metrics
    }

    def generateIntersectionResultForTexti(double intersectionRate){
        intersectionResult = []
        conflictData?.each{ d ->
            def textiLeft = textiData.find{ (it.project== d.project) && (it.task==d.left) }?.texti
            def textiRight = textiData.find{ (it.project== d.project) && (it.task==d.right) }?.texti
            if(textiLeft && textiRight){
                def intersection = textiLeft.intersect(textiRight)
                def conflictPrediction = 0
                if(intersection.size()>=intersectionRate) conflictPrediction = 1
                if(textiLeft.size()>0 && textiRight.size()>0 ){
                    intersectionResult += [project: d.project, left: d.left, right: d.right,
                                           intersection: d.intersection.size(),
                                           conflict: d.conflict,
                                           conflictOfInterest: d.conflictOfInterest,
                                           prediction: conflictPrediction]
                }
            }
        }
        println "intersectionResult: ${intersectionResult.size()}"
    }

    def generateIntersectionResultForTextiCombinedWithTesti(double intersectionRate){
        intersectionResult = []
        conflictData?.each{ d ->
            def textiLeft = textiData.find{ (it.project== d.project) && (it.task==d.left) }?.texti
            def textiRight = textiData.find{ (it.project== d.project) && (it.task==d.right) }?.texti
            if(textiLeft && textiRight){
                def interfaceLeft = textiLeft.intersect(d.testiLeft)
                def interfaceRight = textiRight.intersect(d.testiRight)
                def intersection = interfaceLeft.intersect(interfaceRight)
                def conflictPrediction = 0
                if(intersection.size()>=intersectionRate) conflictPrediction = 1
                if(textiLeft.size()>0 && textiRight.size()>0 ){
                    intersectionResult += [project: d.project, left: d.left, right: d.right,
                                           intersection: d.intersection.size(),
                                           conflict: d.conflict,
                                           conflictOfInterest: d.conflictOfInterest,
                                           prediction: conflictPrediction]
                }
            }
        }
        println "intersectionResult: ${intersectionResult.size()}"
    }

    double calculatePrecision() {
        def conflicts = intersectionResult.findAll{ it.conflict == 1 }?.collect{ [it.project, it.left, it.right] }
        def predictedConflicts = intersectionResult.findAll{ it.prediction == 1 }?.collect{ [it.project, it.left, it.right] }
        double result = 0
        if (invalidInput(predictedConflicts, conflicts)) return result
        def truePositives = predictedConflicts.findAll{ it in conflicts }.size()
        if (truePositives > 0) result = (double) truePositives / predictedConflicts.size()
        result
    }

    double calculateRecall() {
        def conflicts = intersectionResult.findAll{ it.conflict == 1 }?.collect{ [it.project, it.left, it.right] }
        def predictedConflicts = intersectionResult.findAll{ it.prediction == 1 }?.collect{ [it.project, it.left, it.right] }
        double result = 0
        if (invalidInput(predictedConflicts, conflicts)) return result
        def truePositives = predictedConflicts.findAll{ it in conflicts }.size()
        if (truePositives > 0) result = (double) truePositives / conflicts.size()
        result
    }

    double calculateF2(){
        def beta = 2
        def precision = calculatePrecision()
        def recall = calculateRecall()
        (beta*beta + 1)*precision*recall / (beta*beta*precision + recall)
    }

    double calculateF2ForConflictsOfInterest(){
        def beta = 2
        def precision = calculatePrecisionForConflictsOfInterest()
        def recall = calculateRecallForConflictsOfInterest()
        (beta*beta + 1)*precision*recall / (beta*beta*precision + recall)
    }

    double calculatePrecisionForConflictsOfInterest() {
        def conflictsOfInterest = intersectionResult.findAll{ it.conflictOfInterest == 1 }?.collect{ [it.project, it.left, it.right] }
        def predictedConflicts = intersectionResult.findAll{ it.prediction == 1 }?.collect{ [it.project, it.left, it.right] }
        double result = 0
        if (invalidInput(predictedConflicts, conflictsOfInterest)) return result
        def truePositives = predictedConflicts.findAll{ it in conflictsOfInterest }.size()
        if (truePositives > 0) result = (double) truePositives / predictedConflicts.size()
        result
    }

    double calculateRecallForConflictsOfInterest() {
        def conflictsOfInterest = intersectionResult.findAll{ it.conflictOfInterest == 1 }?.collect{ [it.project, it.left, it.right] }
        def predictedConflicts = intersectionResult.findAll{ it.prediction == 1 }?.collect{ [it.project, it.left, it.right] }
        double result = 0
        if (invalidInput(predictedConflicts, conflictsOfInterest)) return result
        def truePositives = predictedConflicts.findAll{ it in conflictsOfInterest }.size()
        if (truePositives > 0) result = (double) truePositives / conflictsOfInterest.size()
        result
    }

    private extractConflictData(){
        conflictData = []
        def lines = CsvUtil.read(conflictFile)
        lines.remove(0)
        lines.each{ line ->
            def project = fixProjectName(line[PROJECT_INDEX])
            conflictData += [project     : project, left: line[LEFT_TASK_INDEX], right: line[RIGHT_TASK_INDEX],
                             conflict    : line[CONFLICT_INDEX] as int, conflictOfInterest: line[CONFLIC_OF_INTEREST_INDEX] as int,
                             testiLeft   : line[TESTI_LEFT].substring(1, line[TESTI_LEFT].size()-1).tokenize(",")*.trim(),
                             testiRight  : line[TESTI_RIGHT].substring(1, line[TESTI_RIGHT].size()-1).tokenize(",")*.trim(),
                             intersection: line[INTERSECTION_INDEX]
                                     .substring(1, line[INTERSECTION_INDEX].size()-1).tokenize(",")*.trim()]
        }

        println "conflictData: ${conflictData.size()}"
    }

    private static fixProjectName(String name){
        def project = name
        def index = name.lastIndexOf("_")
        if(index>-1) project = name.substring(index+1)

        switch (project){
            case "action": project = "action-center-platform"
                break
            case "e" : project = "e-petitions"
                break
            case "concord" : project = "rigse"
                break
            case "claim" : project = "claim-for-crown-court-defence"
                break
            case "one" : project = "one-click-orgs"
        }

        project
    }

    private extractTextiData(){
        textiData = []
        def lines = CsvUtil.read(textiFile)
        lines.remove(0)
        lines.each{ line ->
            textiData += [project: line[0], task: line[1],
                          texti: line[3].substring(1, line[3].size()-1).tokenize(",")*.trim()]
        }
        println "textiData: ${textiData.size()}"
    }

    private static invalidInput(prediction, conflictResult) {
        if (!prediction || prediction.empty || !conflictResult || conflictResult.empty) true
        else false
    }

    static exportResult(String outputFile, List data, def metrics){
        List<String[]> content = []
        def header = ["Project", "left", "right", "intersection", "conflict", "conflict_of_interest", "prediction"] as String[]
        content += header
        data.each{
            content += [it.project, it.left, it.right, it.intersection, it.conflict, it.conflictOfInterest, it.prediction] as String[]
        }

        println "outputFile: ${outputFile}; content: ${content.size()-1}"

        content += ["precision", metrics.precision] as String []
        content += ["recall", metrics.recall] as String []
        content += ["precision of interest", metrics.precisioni] as String []
        content += ["recall of interest", metrics.recalli] as String []
        content += ["F2 interest", metrics.f2] as String []
        content += ["F2 of interest", metrics.f2i] as String []
        CsvUtil.write(outputFile, content)
    }

    private static convertTextProjectToUrl(String project){
        def url = ""
        switch (project){
            case "allourideas.org": url = "https://github.com/allourideas/allourideas.org"
                break
            case "e-petitions": url = "https://github.com/alphagov/e-petitions"
                break
            case "whitehall": url = "https://github.com/alphagov/whitehall"
                break
            case "bsmi": url = "https://github.com/BTHUNTERCN/bsmi"
                break
            case "enroll": url = "https://github.com/dchbx/enroll"
                break
            case "diaspora": url = "https://github.com/diaspora/diaspora"
                break
            case "action-center-platform": url = "https://github.com/EFForg/action-center-platform"
                break
            case "gitlabhq": url = "https://github.com/gitlabhq/gitlabhq"
                break
            case "wontomedia": url = "https://github.com/gleneivey/wontomedia"
                break
            case "jekyll": url = "https://github.com/jekyll/jekyll"
                break
            case "claim-for-crown-court-defence": url = "https://github.com/ministryofjustice/Claim-for-Crown-Court-Defence"
                break
            case "one-click-orgs": url = "https://github.com/oneclickorgs/one-click-orgs"
                break
            case "opengovernment": url = "https://github.com/opengovernment/opengovernment"
                break
            case "openproject": url = "https://github.com/opf/openproject"
                break
            case "otwarchive": url = "https://github.com/otwcode/otwarchive"
                break
            case "rapidftr": url = "https://github.com/rapidftr/RapidFTR"
                break
            case "quantified": url = "https://github.com/sachac/quantified"
                break
            case "sequencescape": url = "https://github.com/sanger/sequencescape"
                break
            case "sharetribe": url = "https://github.com/sharetribe/sharetribe"
        }
        url
    }

}
