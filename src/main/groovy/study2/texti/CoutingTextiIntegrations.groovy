package study2.texti

import br.ufpe.cin.tan.util.CsvUtil

class CoutingTextiIntegrations {

    static void main(String[] args){
        def file = "D:\\Dropbox\\Thaís\\phd_study2_fixed_result\\texti\\testi_selected_changeset_fixed" +
                "\\texti_evaluation.csv"

        def entries = CsvUtil.read(file)
        entries.remove(0)
        def groups = entries.groupBy { it[0] }

        groups.each{ group ->
            println "${group.key}: ${group.value.size()}"
        }

    }

}
