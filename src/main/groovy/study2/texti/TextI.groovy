package study2.texti

import br.ufpe.cin.tan.analysis.data.ExporterUtil
import br.ufpe.cin.tan.analysis.itask.TaskInterface
import br.ufpe.cin.tan.similarity.test.TestSimilarityAnalyser
import br.ufpe.cin.tan.similarity.text.TextualSimilarityAnalyser
import br.ufpe.cin.tan.util.CsvUtil
import groovy.time.TimeCategory
import groovy.time.TimeDuration
import groovy.util.logging.Slf4j
import study1.task_interfaces.text.TextControllerResult
import study1.task_interfaces.text.TextExporter
import study1.task_interfaces.text.TextResult

import java.text.SimpleDateFormat
import java.util.regex.Matcher

/*
* Gera interface de tarefa com base no IReal das tarefas mais similares textualmente.
* */

@Slf4j
class TextI {

    List tasks //analyzed tasks (TestI result)
    List pairs
    List similarityResult //t1, ireal1, t2, ireal2, similarity, testSimJaccard, realSimJaccard, testSimCosine, realSimCosine
    List ranking
    final int MAX
    String analysedTasksFile //csv that contains analysed tasks (itest and ireal data)
    String detailedAnalysedTasksFile //csv that contains date information
    String textiEvaluationFile
    String textiNoEmptyEvaluationFile
    String textualSimilarityTasksFile
    List<TextResult> results
    List<TextResult> noemptyResults
    List datePerTask
    String textFilesFolder
    List<String[]> similarityOutput
    String project

    String timestampFiles
    List timestampComputingTaskPairs
    List timestampComputingSimilarity
    List timestampRankingSimilarity
    List timestampComputingIntersection

    TextI(String analysedTasksFile, String mainTextFolder){
        this(analysedTasksFile, mainTextFolder, 3)
    }

    TextI(String analysedTasksFile, String mainTextFolder, int max){
        this.analysedTasksFile = analysedTasksFile
        this.detailedAnalysedTasksFile = analysedTasksFile - ".csv" + "-detailed.csv"
        this.textualSimilarityTasksFile = analysedTasksFile - "-relevant.csv" + "-text-similarity.csv"
        configureDatePerTask()
        configureOutputFile()
        this.textFilesFolder = mainTextFolder + File.separator + this.project
        MAX = max
        tasks = []
        pairs = []
        similarityResult = []
        similarityOutput = []
        ranking = []
        results = []
        noemptyResults = []
        timestampComputingTaskPairs = []
        timestampComputingSimilarity = []
        timestampRankingSimilarity = []
        timestampComputingIntersection = []
        extractTasks()
        log.info "TextI info"
        log.info "input file: ${this.analysedTasksFile}"
        log.info "date info file: $detailedAnalysedTasksFile"
        log.info "text files folder: ${textFilesFolder}"
        log.info "texti output file: $textiEvaluationFile"
        log.info "controller texti output file: $textiNoEmptyEvaluationFile"
        log.info "textual similarity file: $textualSimilarityTasksFile\n"
    }

    def generate(){
        computeTaskPairs()
        log.info "pairs: ${pairs.size()}"
        computeSimilarity()
        log.info "similarityResult: ${similarityResult.size()}"
        rankingSimilarity()
        computeIntersectionAmongSimilarTasks()
        exportResult()
    }

    private TimeDuration computeTimestamp(Date initTime) {
        TimeDuration timestamp = null
        def endTime = new Date()
        use(TimeCategory) {
            timestamp = endTime - initTime
        }
        timestamp
    }

    private configureDatePerTask(){
        datePerTask = []
        this.project = null
        if (!detailedAnalysedTasksFile || detailedAnalysedTasksFile.empty ||
                !(new File(detailedAnalysedTasksFile).exists())) return
        List<String[]> entries = CsvUtil.read(detailedAnalysedTasksFile)
        if (entries.size() <= 1) return

        def lines = entries.subList(19, entries.size())
        datePerTask = lines.collect{ l ->
            def datesString = l[2].substring(1, l[2].size()-1).split(", ")
            def lastDate = datesString?.last()
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy")
            def date = sdf.parse(lastDate)

            if(this.project == null){
                def project = formatProjectName(l[0])
                if(project ==~ /.+_\d+/ ){
                    def index = project.lastIndexOf("_")
                    project = project.substring(0, index)
                }
                this.project = project
            }
            [project:project, id:l[1], date:date]
        }
        datePerTask = datePerTask.sort{ [it.project, it.id] }
    }

    private static formatProjectName(String value){
        def project = value.toLowerCase()
        if(project ==~ /.+_\d+/ ){
            def index = project.lastIndexOf("_")
            project = project.substring(0, index)
        }
        project
    }

    private configureOutputFile(){
        def file = new File(analysedTasksFile)
        def index = file.path.lastIndexOf(File.separator)
        def path = file.path.substring(0, index+1)
        def name = ""
        if(analysedTasksFile.contains("-relevant.csv")) name = file.name - "-relevant.csv"
        else name = file.name - ".csv"
        textiEvaluationFile = "${path}${name}-text.csv"
        textiNoEmptyEvaluationFile = "${path}${name}-text-noempty.csv"
        timestampFiles = "${path}${name}"
    }

    private extractTasks(){
        if (!analysedTasksFile || analysedTasksFile.empty || !(new File(analysedTasksFile).exists())) return
        List<String[]> entries = CsvUtil.read(analysedTasksFile)
        if (entries.size() <= 1) return
        tasks = entries.subList(13, entries.size())
        def temp = []
        tasks.each{ task ->
            temp.add(([project] + task).flatten())
        }
        tasks = temp
    }

    private computeTaskPairs() {
        //datePerTask [project:, id:, date:[]]
        pairs = []
        if (!tasks || tasks.empty) return tasks
        if (tasks.size() == 1){
            pairs.add([task: tasks.first(), pairs: []])
        } else {
            def projects = datePerTask.collect{ it.project }.unique()
            projects.each{ project ->
                log.info "project: $project"
                def tasksFromProject = tasks.findAll{ it[0] == project }
                tasksFromProject.eachWithIndex { v, k ->
                    def initTime = new Date()

                    def next = tasksFromProject - v//tasksFromProject.drop(k+1)
                    def minDate = datePerTask.find{ it.project==v[0] && it.id == v[1] }.date
                    def othersProjectAndId = next.collect{ [it[0], it[1]] }
                    def others = datePerTask.findAll{ [it.project, it.id] in othersProjectAndId }
                    def previousTasks = others.findAll{ it.date.before(minDate) /*|| it.date==minDate*/ }*.id
                    def candidates = tasks.findAll{ it[0]==project && it[1] in previousTasks }.unique()

                    TimeDuration timestamp = computeTimestamp(initTime)
                    timestampComputingTaskPairs.add([project: v[0], task: v[1], timestamp: timestamp])

                    log.info "task: ${v[0]}, ${v[1]}; similar candidates (${candidates.size()}): ${candidates.collect{ [it[0], it[1]]}}"
                    pairs.add([project: project, task: v, pairs: candidates])
                }
            }
            pairs = pairs.unique()
        }
    }

    private extractTaskText(taskId) {
        def text = ""
        def filename = "${textFilesFolder}${File.separator}${taskId}.txt"
        File file = new File(filename)
        if (file.exists()) {
            file.withReader("utf-8") { reader ->
                text = reader.text
            }
        } else {
            log.warn "Text file '${filename}' not found!"
        }
        text
    }

    private computeSimilarity(){
        //pairs = [project: project, task: v, pairs: candidates]
        pairs?.each { item ->
            def initTime = new Date()
            def task = item.task
            def taskText = extractTaskText(task[1])
            def itest1 = task[ExporterUtil.TESTI_INDEX_SHORT_HEADER+1]?.replace(File.separator, Matcher.quoteReplacement(File.separator))
                    ?.substring(1, task[ExporterUtil.TESTI_INDEX_SHORT_HEADER+1].size() - 1)?.split(", ") as List
            def ireal1 = task[ExporterUtil.TASKI_INDEX_SHORT_HEADER+1]?.replace(File.separator, Matcher.quoteReplacement(File.separator))
                    ?.substring(1, task[ExporterUtil.TASKI_INDEX_SHORT_HEADER+1].size() - 1)?.split(", ") as List

            if(!taskText.empty) {
                item.pairs?.each { other ->
                    def otherText = extractTaskText(other[1])
                    if(!otherText.empty) {
                        def textualSimilarityAnalyser = new TextualSimilarityAnalyser()
                        def textSimilarity = textualSimilarityAnalyser.calculateSimilarity(taskText, otherText)
                        def itest2 = other[ExporterUtil.TESTI_INDEX_SHORT_HEADER + 1]
                                ?.replace(File.separator, Matcher.quoteReplacement(File.separator))
                                ?.substring(1, other[ExporterUtil.TESTI_INDEX_SHORT_HEADER + 1].size() - 1)?.split(", ") as List
                        def ireal2 = other[ExporterUtil.TASKI_INDEX_SHORT_HEADER + 1]
                                ?.replace(File.separator, Matcher.quoteReplacement(File.separator))
                                ?.substring(1, other[ExporterUtil.TASKI_INDEX_SHORT_HEADER + 1].size() - 1)?.split(", ") as List

                        def similarityAnalyser = new TestSimilarityAnalyser(itest1, itest2)
                        def testSimJaccard = similarityAnalyser.calculateSimilarityByJaccard()
                        def testSimCosine = similarityAnalyser.calculateSimilarityByCosine()

                        similarityAnalyser = new TestSimilarityAnalyser(ireal1, ireal2)
                        def realSimJaccard = similarityAnalyser.calculateSimilarityByJaccard()
                        def realSimCosine = similarityAnalyser.calculateSimilarityByCosine()

                        similarityResult += [project       : item.project, t1: task[1] as int, ireal1: ireal1, t2: other[1] as int, ireal2: ireal2,
                                             similarity    : textSimilarity, testSimJaccard: testSimJaccard,
                                             realSimJaccard: realSimJaccard,
                                             testSimCosine : testSimCosine, realSimCosine: realSimCosine]
                    }
                }
            }

            TimeDuration timestamp = computeTimestamp(initTime)
            timestampComputingSimilarity.add([project: item.project, task: task[1], timestamp: timestamp])
        }

        eliminateDuplicates()
    }

    private eliminateDuplicates(){
        def result = []
        similarityResult.each{ sr ->
            def others = similarityResult - sr
            def find = others.find{
                (it.project==sr.project && it.t1==sr.t1 && it.t2==sr.t2) ||
                        (it.project==sr.project && it.t1==sr.t2 && it.t2==sr.t1)
            }
            if(!find) result += sr
        }
        similarityResult = result
    }

    //gera o csv de similaridade da pasta tasks\\temp\\textual-similarity\\textual_similarity_all.csv
    private rankingSimilarity(){
        if(similarityOutput.empty) {
            similarityOutput += ["project", "task", "similar", "level"] as String[]
        }
        ranking = []
        def projects = tasks.collect{ it[0] }.unique()
        projects.each{ project ->
            def idList = tasks.findAll{ it[0] == project }.collect{ it[1] as int }.sort()
            idList.each{ id ->
                def initTime = new Date()
                def results = similarityResult.findAll{ it.project==project && it.t1==id }
                def resultsToFix = results.findAll{ it.project==project && it.t2 == id }
                def finalResult = results - resultsToFix
                resultsToFix.each{ r ->
                    finalResult += [project:project, t1: r.t2, ireal1: r.ireal2, t2: r.t1, ireal2: r.ireal1, similarity: r.similarity,
                                    testSimJaccard: r.testSimJaccard, realSimJaccard: r.realSimJaccard,
                                    testSimCosine: r.testSimCosine, realSimCosine: r.realSimCosine]
                }
                finalResult = finalResult.sort{ a, b -> b.similarity <=> a.similarity }

                def mostSimilarResults = []
                if(finalResult.size() >= MAX){
                    mostSimilarResults = finalResult.subList(0, MAX)
                } else if(!finalResult.empty){
                    mostSimilarResults = finalResult
                }
                if(!mostSimilarResults.empty){
                    mostSimilarResults.each{
                        similarityOutput += [project, id, it.t2, it.similarity] as String[]
                    }
                    ranking += [project: project, id: id, result: mostSimilarResults]
                }
                TimeDuration timestamp = computeTimestamp(initTime)
                timestampRankingSimilarity.add([project: project, task: id, timestamp: timestamp])
            }
        }
        log.info "similarityOutput: ${similarityOutput.size()-1}"
        CsvUtil.write(textualSimilarityTasksFile, similarityOutput)
    }

    private computeIntersectionAmongSimilarTasks(){
        results = []
        def projectList = pairs*.task*.getAt(0).flatten().unique()
        projectList.each{ project ->
            def tasksPerProject = pairs.findAll{ it.project == project }
            def idList = tasksPerProject*.task*.getAt(1).flatten() as int[]
            idList.each{ id ->
                def initTime = new Date()

                def value = ranking.find{ (it.project == project) && (it.id == id) }
                if(value){
                    def result = value.result
                    def ireal = result.get(0).ireal1
                    def othersIreal = result*.ireal2*.collect()
                    def intersection = othersIreal.get(0)
                    othersIreal.each {
                        intersection.retainAll(it)
                    }
                    results += new TextResult(id, project, intersection, ireal)
                } else {
                    results += new TextResult(id, project, [], [])
                }

                TimeDuration timestamp = computeTimestamp(initTime)
                timestampComputingIntersection.add([project: project, task: id, timestamp: timestamp])
            }
        }
        noemptyResults = results.findAll{ !it.files.empty }
    }

    private exportResult(){
        def iTextExporter = new TextExporter(textiEvaluationFile, results)
        iTextExporter.save()

        iTextExporter = new TextExporter(textiNoEmptyEvaluationFile, noemptyResults)
        iTextExporter.save()

        TimestampExporter timestampExporter = new TimestampExporter(timestampFiles, this)
        timestampExporter.save()
    }

}
