package study2.texti

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

class TextInterfaceOrganizer {

    String folder
    List<String> files
    List<String> noemptyFiles
    List<String> timestampFiles
    String outputFile
    String outputNoEmptyFile
    String outputNoEmptyTimestampFile

    static void main(String[] args){
        def finalAnalysisResult = "D:\\Onedrive\\Documentos\\phd_study2_final_result\\texti-timestamp" +
                "\\result_organized\\final-result"
        TextInterfaceOrganizer evaluator = new TextInterfaceOrganizer(finalAnalysisResult)
        evaluator.summarize()
    }

    TextInterfaceOrganizer(String folder){
        this.folder = folder
        outputFile = "$folder${File.separator}texti.csv"
        outputNoEmptyFile = "$folder${File.separator}texti-noempty.csv"
        outputNoEmptyTimestampFile = "$folder${File.separator}texti-noempty-timestamp.csv"
        findTextiFiles()
        findTextiNoEmptyFiles()
        findTimestampFiles()
    }

    def summarize(){
        summarizeResults()
        summarizeNoEmptyResults()
        summarizeResultsWithTimestamp()
    }

    def extractProjectName(String name){
        def i = name.indexOf("-text")
        def j = name.lastIndexOf(File.separator)
        name.substring(j+1, i)
    }

    private summarizeResults(){
        def content = summarizeData(files)
        CsvUtil.write(outputFile, content)
    }

    private summarizeNoEmptyResults(){
        def content = summarizeData(noemptyFiles)
        CsvUtil.write(outputNoEmptyFile, content)
    }

    private summarizeResultsWithTimestamp(){
        String[] firstLine = ["Project", "Task", "TextI_size", "TextI", "TaskI_size", "TaskI", "Timestamp"] as String[]
        List<String[]> content = []
        List<String[]> data = []
        content +=  firstLine
        timestampFiles.each{ file ->
            def lines = CsvUtil.read(file)
            data += lines.sort{ it[1] as double }
        }
        content +=  data
        CsvUtil.write(outputNoEmptyTimestampFile, content)
    }

    private summarizeData(resultFiles){
        String[] firstLine = ["Project", "Task", "TextI_size", "TextI", "TaskI_size", "TaskI"] as String[]
        List<String[]> content = []
        List<String[]> data = []
        content +=  firstLine
        resultFiles.each{ file ->
            def lines = CsvUtil.read(file)
            lines = lines.subList(1, lines.size()).collect{ [it[0], it[1], it[4], it[9], it[5], it[10]] as String[] }
            data += lines.sort{ it[1] as double }
        }
        content +=  data
        content
    }

    def findTextiFiles(){
        def aux = Util.findFilesFromDirectory(folder)
        files = aux.findAll{
            it.endsWith("-text.csv")
        }
        println "texti files: ${files.size()}"
        files.each{ println it }
    }

    def findTextiNoEmptyFiles(){
        def aux = Util.findFilesFromDirectory(folder)
        noemptyFiles = aux.findAll{
            it.endsWith("-text-noempty.csv")
        }
        println "texti no empty files: ${noemptyFiles.size()}"
        noemptyFiles.each{ println it }
    }

    def findTimestampFiles(){
        def aux = Util.findFilesFromDirectory(folder)
        timestampFiles = aux.findAll{
            it.endsWith("-timestamp.csv")
        }
        println "timestamp files: ${timestampFiles.size()}"
        timestampFiles.each{ println it }
    }

}
