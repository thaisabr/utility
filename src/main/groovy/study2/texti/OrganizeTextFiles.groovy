package study2.texti

import br.ufpe.cin.tan.util.Util

import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path

class OrganizeTextFiles {

    String mainFolder
    String outputFolder
    List<String> textFolders

    OrganizeTextFiles(String mainFolder, String outputFolder){
        this.mainFolder = mainFolder
        this.outputFolder = outputFolder + File.separator + "text"
        createFolder(this.outputFolder)
    }

    def run(){
        identifyTextFolders()
        copyFiles()
    }

    def identifyTextFolders(){
        textFolders = Util.findFoldersFromDirectory(mainFolder).findAll{
            it.contains("-independent-tasks") && it.endsWith("${File.separator}text")
        }
    }

    def copyFiles(){
        textFolders.each{ folder ->
            def name = extractFolderName(folder)
            def textFiles = Util.findFilesFromDirectory(folder)
            def targetFolder = outputFolder + File.separator + name
            createFolder(targetFolder)
            textFiles.each{ file ->
                Path source = FileSystems.getDefault().getPath(file)
                Path target = FileSystems.getDefault().getPath(targetFolder)
                Files.copy(source, target.resolve(source.getFileName()))
            }
        }
    }

    private static extractFolderName(String path){
        def name = path
        def index = name.lastIndexOf(File.separator)
        if(index>-1) name = name.substring(0, index)
        index = name.lastIndexOf(File.separator)
        if(index>-1) name = name.substring(index+1)
        index = name.indexOf("-independent-tasks")
        if(index>-1) name = name.substring(0, index)
        index = name.lastIndexOf("_")
        if(index>-1) name = name.substring(index+1)
        name.toLowerCase()
    }

    private static createFolder(String folder){
        File folderManager = new File(folder)
        folderManager.mkdir()
    }

}
