package study2

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

/*
* Script para verificar, das tarefas para as quais se calculou TestI, quantas descartadas tinham TaskI vazio.
* Importante saber que a tarefa pode ter sido descartada por outros motivos além desse.
* Precisamos disso para verificar a antiga amostra (13.792 integrações), pois o requisito de TaskI não vazio foi
* errôneamente empregado.
* */

class CountingAnalyzedTasksWithEmptyTaskI {

    static printFiles(String folder){
        println "folder: $folder"

        def relevantFiles = Util.findFilesFromDirectory(folder).findAll{
            it.endsWith("-independent-tasks-relevant.csv")
        }

        def affectedProjects = []
        def emptyTaskiCounter = 0
        println "relevant task files: ${relevantFiles.size()}"
        relevantFiles.each{ file ->
            def lines = CsvUtil.read(file)
            lines = lines.subList(13, lines.size())
            if(lines.size()>0){
                def tasksEmptyTaski = lines.findAll{
                    it[8] == "0"
                }
                emptyTaskiCounter += tasksEmptyTaski.size()
                if(tasksEmptyTaski.size()>0) affectedProjects += file
                println "${file}: ${tasksEmptyTaski.size()}"
                println "${tasksEmptyTaski.collect{it[0]}}"
            }
        }
        println "tasks with empty TaskI: ${emptyTaskiCounter}"
        println "projects with tasks with empty TaskI: ${affectedProjects.unique().size()}"

    }

    static void main(String[] args){
        def folder1 = "C:\\Users\\tatab\\Desktop\\testi_given_exclude_nov20\\result_organized\\final-result"
        printFiles(folder1)
    }


}
