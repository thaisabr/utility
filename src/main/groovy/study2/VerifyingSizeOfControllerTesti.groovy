package study2

import br.ufpe.cin.tan.util.CsvUtil
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

/* Código para verificar o tamanho das interfaces filtradas por controller. Isso é importante para delimitarmos o tamanho
* da interseção para sinalizar risco de conflito. */
class VerifyingSizeOfControllerTesti {

    List data //[project, left, right, conflict, conflictOfInterest, jaccard, cosine]
    String file
    static final int PROJECT_INDEX = 0
    static final int LEFT_TASK_INDEX = 1
    static final int RIGHT_TASK_INDEX = 2
    static final int CONFLICT_INDEX = 3
    static final int CONFLIC_OF_INTEREST_INDEX = 5
    static final int TESTI_LEFT = 7
    static final int TESTI_RIGHT = 8
    static final int INTERSECTION_INDEX = 23

    VerifyingSizeOfControllerTesti(String file){
        this.file = file
        data = []
        def lines = CsvUtil.read(file)
        lines.remove(0)
        lines.each{ line ->
            data += [project: line[PROJECT_INDEX], left: line[LEFT_TASK_INDEX], right: line[RIGHT_TASK_INDEX],
                     conflict: line[CONFLICT_INDEX] as int, conflictOfInterest: line[CONFLIC_OF_INTEREST_INDEX] as int,
                     testiLeft: line[TESTI_LEFT].substring(1, line[TESTI_LEFT].size()-1).tokenize(",")*.trim(),
                     testiRight: line[TESTI_RIGHT].substring(1, line[TESTI_RIGHT].size()-1).tokenize(",")*.trim(),
                     intersection: line[INTERSECTION_INDEX]
                             .substring(1, line[INTERSECTION_INDEX].size()-1).tokenize(",")*.trim()]
        }
    }

    def verifyTestiSize(){
        def leftTasks = data.collect{ [it.project, it.left, it.testiLeft] }.unique()
        def rightTasks = data.collect{ [it.project, it.right, it.testiRight] }.unique()
        def tasks = (leftTasks + rightTasks).sort{ it[0] }.sort{ it[1] }.unique()
        println "tasks: ${tasks.size()}"

        def statistics = new DescriptiveStatistics(tasks.collect{it[2].size() as double} as double[])
        println "Size overview"
        println "mean TestI size: ${statistics.mean}"
        println "median: ${statistics.getPercentile(50.0)}"
        println "standard deviation: ${statistics.standardDeviation}"

        println "Overview per project"
        def tasksPerProject = tasks.groupBy {it[0]}
        def projects = tasksPerProject.keySet()
        projects.each{ project ->
            def stats = new DescriptiveStatistics(tasksPerProject.get(project).collect{it[2].size() as double} as double[])
            println "Project: $project; TestI size: mean ${stats.mean}, median ${stats.getPercentile(50.0)}, sd ${stats.standardDeviation}"
        }

    }


    static void main(String[] args){
        def folder = "D:\\Dropbox\\Thaís\\phd_study2\\new_sample\\conflict_risk_by_changed_files_refactored_based_on_intersection\\"
        def file = "${folder}conflict-controller.csv"
        def evaluatorController = new VerifyingSizeOfControllerTesti(file)
        evaluatorController.verifyTestiSize()
    }
}
