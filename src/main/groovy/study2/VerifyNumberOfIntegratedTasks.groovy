package study2

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util


/* Do total de tarefas relevantes extraídas dos projetos, fazemos a integração daquelas desenvolvidas na mesma janela de tempo.
* Daí que pode acontecer de não ser possível integrar uma dada tarefa com nenhuma outra.
* Esse script verifica o total de tarefas que efetivamente foram integradas por projeto, dando também o resultado final para toda a amostra.
* Importante lembrar que tem 1 projeto na amostra que deve ser descartado porque ele é uma repetição de outro projeto que
* já está na amostra. Esse script não lida com essa repetição. */
class VerifyNumberOfIntegratedTasks {

    static void main(String[] args){
        def folder = "D:\\Dropbox\\Thaís\\phd_study2\\new_sample\\conflict_risk_by_changed_files_refactored\\tasks\\pairs"
        def files = Util.findFilesFromDirectory(folder)?.findAll{
            !it.endsWith("opf_openproject-ce-independent-tasks-pairs.csv")
        }
        def n = 0
        files.each{ file ->
            def lines = CsvUtil.read(file)
            lines.remove(0)

            def tasks = []
            if(lines.size()>0){
                def tasks1 = lines.collect{ it[1] }
                def tasks2 = lines.collect{ it[2].substring(1, it[2].size()-1).tokenize(",")*.trim() }
                tasks = (tasks1 + tasks2).flatten().unique()
                n += tasks.size()
            }

            println "File: ${file}"
            println "Tasks: ${tasks.size()}"
            println tasks
        }
        println "Total tasks: ${n}"
    }

}
