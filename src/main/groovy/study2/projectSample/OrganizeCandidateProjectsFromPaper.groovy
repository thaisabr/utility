package study2.projectSample

import br.ufpe.cin.tan.util.CsvUtil

class OrganizeCandidateProjectsFromPaper {

    static void main(String[] args){
        List<String[]> lines = CsvUtil.read("error\\projects_61_creationDate.csv")
        lines.remove(0)
        lines = lines.collect{ it[0] }
        List<String[]> content = []
        content += ["URL","MASTER_BRANCH","CREATED_AT","STARS","SIZE","DESCRIPTION","GEMS"] as String[]
        lines.each{
            content += [it, "", "", "", "", "", ""] as String[]
        }
        CsvUtil.write("error\\candidate-projects1.csv", content)
    }

}
