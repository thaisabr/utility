package study2.projectSample

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

class OrganizeCandidateProjects {

    static void main(String[] args){
        //reunindo em uma só planilha o resultado das múltiplas buscas realizadas por projetos Rails+Cucumber
        /*def folder = "error"
        def files = Util.findFilesFromDirectory(folder).findAll{ it.endsWith("candidate-projects.csv") }
        
        List<String[]> buffer = []
        buffer += ["URL","MASTER_BRANCH","CREATED_AT","STARS","SIZE","DESCRIPTION","GEMS"] as String[]
        files.each{ file ->
            List<String[]> lines = CsvUtil.read(file)
            lines.remove(0)
            buffer += lines
        }

        buffer = buffer.unique{ it[0] }
        println "Found projects: ${buffer.size()-1}"
        CsvUtil.write("${folder}${File.separator}candidate-projects.csv", buffer)*/

        //verify intersection among project samples
        List<String[]> linesFromSample1 = CsvUtil.read("tasks\\candidate-projects1.csv")
        linesFromSample1.remove(0)
        def projectsSample1 = linesFromSample1?.collect{ it[0] }?.unique()

        List<String[]> linesFromSample2 = CsvUtil.read("tasks\\candidate-projects2.csv")
        linesFromSample2.remove(0)
        def projectsSample2 = linesFromSample2?.collect{ it[0] }?.unique()

        println "sample1: ${projectsSample1.size()}"

        println "sample2: ${projectsSample2.size()}"

        def intersection = projectsSample1.intersect(projectsSample2)
        println "intersecao: ${intersection.size()}"
        intersection.each{ println it }

        def diff1 = projectsSample1 - projectsSample2
        println "está em sample1 e não está em sample2: ${diff1.size()}"
        diff1.each{ println it }

        def diff2 = projectsSample2 - projectsSample1
        println "está em sample2 e não está em sample1: ${diff2.size()}"
        diff2.each{ println it }

        def union = (projectsSample1 + projectsSample2).unique()
        println "união: ${union.size()}"
        union.each{ println it }
    }

}
