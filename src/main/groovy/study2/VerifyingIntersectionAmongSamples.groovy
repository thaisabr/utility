package study2

import br.ufpe.cin.tan.util.CsvUtil

/*
* Script para verificar a interseção entre as amostras de tarefas usadas nos artigos 1 e 2.
* */

class VerifyingIntersectionAmongSamples {

    static void main(String[] args){

        def smallerSamplePaper1 = "samples\\74_tasks.csv"
        def largerSamplePaper1 = "samples\\463_tasks.csv"
        def samplePaper2 = "samples\\990_tasks.csv"

        def lines1 = CsvUtil.read(smallerSamplePaper1)
        lines1.remove(0)
        println "sample 1: ${lines1.size()}"

        def lines2 = CsvUtil.read(largerSamplePaper1)
        lines2.remove(0)
        println "sample 2: ${lines2.size()}"

        def lines3 = CsvUtil.read(samplePaper2)
        lines3.remove(0)
        println "sample 3: ${lines3.size()}"

        def uniqueSamplePaper1 = (lines1 + lines2).unique()
        def matchesSmallerLarger = []
        lines1.each{ line ->
            def match = lines2.find{ it[0] == line[0] && it[3] == line[3] }
            if(match) matchesSmallerLarger += match
        }
        uniqueSamplePaper1 -= matchesSmallerLarger
        println "matchesSmallerLarger: ${matchesSmallerLarger.size()}"
        println "uniqueSamplePaper1: ${uniqueSamplePaper1.size()}"

        def intersectionPaper1AndPaper2 = []
        uniqueSamplePaper1.each{ line ->
            def match = lines3.find{ it[0] == line[0] && it[3] == line[3] }
            if(match) intersectionPaper1AndPaper2 += match
        }
        println "intersectionPaper1AndPaper2: ${intersectionPaper1AndPaper2.size()}"
        intersectionPaper1AndPaper2.each{ println it }

    }
}
