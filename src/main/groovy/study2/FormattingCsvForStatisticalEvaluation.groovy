package study2

import br.ufpe.cin.tan.util.CsvUtil

class FormattingCsvForStatisticalEvaluation {

    static void main (String[] args){
        def lines = CsvUtil.read("error\\conflict.csv")
        println "lines: ${lines.size()}"
        List<String[]> content = lines.collect{ [it[3], it[5], it[22], it[23]] as String[] }
        println "content: ${content.size()}"
        content.subList(1, content.size()).each{ c ->
            if((c[3] as double)>1) c[3] = "1.0"
        }
        def irregular = content.subList(1, content.size()).findAll{
            (it[2] as double)<0 || (it[2] as double)>1 || (it[3] as double)<0 || (it[3] as double)>1
        }
        println "irregular: ${irregular.size()}"
        irregular.each{ println it }
        CsvUtil.write("error\\conflict-summarized.csv", content)
    }

}
