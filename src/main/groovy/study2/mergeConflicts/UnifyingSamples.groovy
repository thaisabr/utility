package study2.mergeConflicts

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

/* Como na nossa amostra de tarefas identificamos apenas 41 merges, decidimos tb considerar merges da amostra do outro artigo.
* Da nova amostra identificamos 41 merges, da antiga identificamos 33, totalizando 74 merges.
* Acontece que existe interseção entre tais amostras.
* Nesse script identificamos a interseção e então geramos o resultado final dos merges analisados.
* Eles totalizam 55 merges.*/
class UnifyingSamples {

    def newSampleConflictFile
    def newSampleTasksFileFolder
    def newTasks
    def newConflictResults
    def newMergedTasks

    def olderSampleConflictFile
    def olderSampleTasksFileFolder
    def oldTasks
    def oldConflictResults
    def oldMergedTasks

    UnifyingSamples(def newConflictFile, def newFolder, def oldConflictFile, def oldFolder){
        newSampleConflictFile = newConflictFile
        newSampleTasksFileFolder = newFolder
        newConflictResults = extractConflictResults(newSampleConflictFile)
        println "new conflict results: ${newConflictResults.size()}"
        newTasks = extractTasksFromFolder(newSampleTasksFileFolder)
        println "new tasks: ${newTasks.size()}"
        newMergedTasks = extractTasksWithDetailedInfo(newTasks, newConflictResults)
        println "new merged tasks: ${newMergedTasks.size()}"

        olderSampleConflictFile = oldConflictFile
        olderSampleTasksFileFolder = oldFolder
        oldConflictResults = extractConflictResults(olderSampleConflictFile)
        println "old conflict results: ${oldConflictResults.size()}"
        oldTasks = extractTasksFromFolder(olderSampleTasksFileFolder)
        println "old tasks: ${oldTasks.size()}"
        oldMergedTasks = extractTasksWithDetailedInfo(oldTasks, oldConflictResults)
        println "old merged tasks: ${oldMergedTasks.size()}"
    }

    static void main(String[] args){
        def root = "D:\\Dropbox\\Thaís\\phd_study2\\"

        def newSampleConflictFile = "${root}new_sample\\merge_conflict_analysis\\output-org\\conflict.csv"
        def newSampleTasksFileFolder = "${root}new_sample\\merge_conflict_analysis\\tasks\\taskfile"

        def olderSampleConflictFile = "${root}paper_sample\\merge_conflict_analysis\\output-org\\conflict.csv"
        def olderSampleTasksFileFolder = "${root}paper_sample\\merge_conflict_analysis\\tasks\\taskfile"

        def obj = new UnifyingSamples(newSampleConflictFile, newSampleTasksFileFolder,
                olderSampleConflictFile, olderSampleTasksFileFolder)

        def intersection = obj.verifyIntersectionBetweenSamples()
        println "intersection between merged tasks: ${intersection.size()}"
        intersection.each{ println it }

        obj.derivateFinalTaskSample()
    }

    static configureProjectName(String tasksFile){
        def name = tasksFile.toLowerCase()
        def index = name.lastIndexOf(File.separator)
        name = name.substring(index+1)
        name - ".csv"
    }

    static extractTasksFromFile(String tasksFile){
        def projectName = configureProjectName(tasksFile)
        def tasks = CsvUtil.read(tasksFile)
        tasks.remove(0)
        tasks = tasks.collect{
            [project: projectName, id: it[1], hashes: it[3].substring(1, it[3].size()-1).tokenize(",")*.trim()]
        }
        tasks
    }

    static extractTasksFromFolder(String tasksFileFolder){
        def files = Util.findFilesFromDirectory(tasksFileFolder)
        def tasks =  []
        files.each{ file ->
            tasks += extractTasksFromFile(file)
        }
        tasks
    }

    static extractConflictResults(def conflictsFile){
        def conflictResults = CsvUtil.read(conflictsFile)
        conflictResults.remove(0)
        conflictResults = conflictResults.collect{
            [project: it[0], left: it[1], right: it[2], conflict: it[3] as int, conflictOfInterest: it[5] as int,
             testiLeft: it[9].substring(1, it[9].size()-1).tokenize(",")*.trim(),
             testiRight: it[10].substring(1, it[10].size()-1).tokenize(",")*.trim()]
        }
        conflictResults
    }

    static extractTasksWithDetailedInfo(List tasks, List conflictResults){
        def mergedTasksWithHashes = []
        conflictResults.each{ r ->
            def selectedTaskLeft = tasks.find{ (r.project== it.project) && (it.id == r.left) }
            def selectedTaskRight = tasks.find{ (r.project== it.project) && (it.id == r.right) }
            if(selectedTaskLeft) {
                mergedTasksWithHashes += [project: r.project, id: selectedTaskLeft.id, hashes: selectedTaskLeft.hashes,
                                          conflictOfInterest: r.conflictOfInterest]
            }
            if(selectedTaskRight){
                mergedTasksWithHashes += [project: r.project, id: selectedTaskRight.id, hashes: selectedTaskRight.hashes,
                                          conflictOfInterest: r.conflictOfInterest]
            }
        }

        mergedTasksWithHashes.unique()
    }

    def verifyIntersectionBetweenSamples(){
        def matches = []
        newMergedTasks.each{ task ->
            def match = oldMergedTasks.find{ (task.project == it.project) && (task.hashes == it.hashes) }
            if(match) {
                matches += [old: match, new: task]
            }
        }
        matches
    }

    def derivateFinalTaskSample(){
        List<String[]> data = []
        data += ["PROJECT","LEFT_TASK","RIGHT_TASK","CONFLICT", "CONFLICT_OF_INTEREST", "TESTIL", "TESTR", "TESTI_INTERSECT"] as String[]

        List<String[]> finalConflictResult = (newConflictResults + oldConflictResults) as List<String[]>
        finalConflictResult = finalConflictResult.unique{ [it.project, it.left, it.right] }
        println "finalConflictResult: ${finalConflictResult.size()}"
        finalConflictResult.each{
            println it
            def testiIntersection = it.testiLeft.intersect(it.testiRight).empty ? 0 : 1
            data += [it.project, it.left, it.right, it.conflict, it.conflictOfInterest, it.testiLeft, it.testiRight,
                     testiIntersection] as String[]
        }

        CsvUtil.write("output\\final-merge-conflict-result.csv", data)
    }




}
