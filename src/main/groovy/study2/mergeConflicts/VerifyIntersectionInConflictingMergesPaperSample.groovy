package study2.mergeConflicts

class VerifyIntersectionInConflictingMergesPaperSample {

    static void main(String[] args){

        def task338 = ["app/controllers/articles_controller.rb", "app/controllers/documents_controller.rb",
                       "app/controllers/events_controller.rb", "app/controllers/hangouts_controller.rb",
                       "app/controllers/projects_controller.rb", "app/controllers/registrations_controller.rb",
                       "app/controllers/static_pages_controller.rb", "app/controllers/users_controller.rb",
                       "app/controllers/visitors_controller.rb", "app/helpers/documents_helper.rb", "app/models/article.rb",
                       "app/models/document.rb", "app/models/event.rb", "app/models/project.rb", "app/models/user.rb",
                       "app/presenters/hangout_presenter.rb", "app/views/articles/_article.html.erb",
                       "app/views/articles/index.html.erb", "app/views/articles/show.html.erb",
                       "app/views/devise/registrations/edit.html.erb", "app/views/devise/registrations/new.html.erb",
                       "app/views/devise/sessions/new.html.erb", "app/views/disqus/_disqus.html.erb",
                       "app/views/documents/show.html.erb", "app/views/events/_hangouts_management.html.erb",
                       "app/views/events/edit.html.erb", "app/views/events/index.html.erb",
                       "app/views/events/new.html.erb", "app/views/events/show.html.erb",
                       "app/views/hangouts/_hangout_button.html.erb", "app/views/hangouts/_hangout_status.html.erb",
                       "app/views/hangouts/_hangouts.html.erb", "app/views/hangouts/_index_basic_info.html.erb",
                       "app/views/hangouts/_index_extra_info.html.erb", "app/views/hangouts/_index_header.html.erb",
                       "app/views/hangouts/index.html.erb", "app/views/layouts/_event_link.html.erb",
                       "app/views/layouts/_flash.html.erb", "app/views/layouts/_footer.html.erb",
                       "app/views/layouts/_head.html.erb", "app/views/layouts/_hire_me.html.erb",
                       "app/views/layouts/_meta_tags.html.erb", "app/views/layouts/_navbar.html.erb",
                       "app/views/layouts/_round_banners.html.erb", "app/views/projects/_activity.html.erb",
                       "app/views/projects/_listing.html.erb", "app/views/projects/_members_list.html.erb",
                       "app/views/projects/index.html.erb", "app/views/projects/show.html.erb",
                       "app/views/static_pages/show.html.erb", "app/views/users/_user_avatar.html.erb",
                       "app/views/users/index.html.erb", "app/views/users/profile/_detail.html.erb",
                       "app/views/users/profile/_summary.html.erb", "app/views/users/profile/_videos.html.erb",
                       "app/views/users/show.html.erb", "app/views/visitors/index.html.erb"]
        def task339 = ["app/controllers/articles_controller.rb", "app/controllers/documents_controller.rb",
                       "app/controllers/events_controller.rb", "app/controllers/hangouts_controller.rb",
                       "app/controllers/projects_controller.rb", "app/controllers/registrations_controller.rb",
                       "app/controllers/static_pages_controller.rb", "app/controllers/users_controller.rb",
                       "app/controllers/visitors_controller.rb", "app/models/article.rb, app/models/event.rb",
                       "app/models/user.rb", "app/presenters/hangout_presenter.rb", "app/views/articles/_article.html.erb",
                       "app/views/articles/index.html.erb", "app/views/articles/show.html.erb",
                       "app/views/devise/registrations/edit.html.erb", "app/views/devise/registrations/new.html.erb",
                       "app/views/devise/sessions/new.html.erb", "app/views/disqus/_disqus.html.erb",
                       "app/views/documents/show.html.erb", "app/views/events/_hangouts_management.html.erb",
                       "app/views/events/edit.html.erb", "app/views/events/index.html.erb", "app/views/events/new.html.erb",
                       "app/views/events/show.html.erb", "app/views/hangouts/_hangout_button.html.erb",
                       "app/views/hangouts/_hangout_status.html.erb", "app/views/hangouts/_hangouts.html.erb",
                       "app/views/hangouts/_index_basic_info.html.erb", "app/views/hangouts/_index_extra_info.html.erb",
                       "app/views/hangouts/_index_header.html.erb", "app/views/hangouts/index.html.erb",
                       "app/views/layouts/_event_link.html.erb", "app/views/layouts/_flash.html.erb",
                       "app/views/layouts/_footer.html.erb", "app/views/layouts/_head.html.erb",
                       "app/views/layouts/_hire_me.html.erb", "app/views/layouts/_meta_tags.html.erb",
                       "app/views/layouts/_navbar.html.erb", "app/views/layouts/_round_banners.html.erb",
                       "app/views/projects/_activity.html.erb", "app/views/projects/_listing.html.erb",
                       "app/views/projects/_members_list.html.erb", "app/views/projects/index.html.erb",
                       "app/views/projects/show.html.erb", "app/views/static_pages/show.html.erb",
                       "app/views/users/_user_avatar.html.erb", "app/views/users/index.html.erb",
                       "app/views/users/profile/_detail.html.erb", "app/views/users/profile/_summary.html.erb",
                       "app/views/users/profile/_videos.html.erb", "app/views/users/show.html.erb",
                       "app/views/visitors/index.html.erb"]

        def task843 = ["app/controllers/documents_controller.rb", "app/controllers/projects_controller.rb",
                       "app/controllers/visitors_controller.rb", "app/models/document.rb", "app/models/project.rb",
                       "app/models/user.rb", "app/views/devise/registrations/new.html.erb", "app/views/devise/sessions/new.html.erb",
                       "app/views/devise/shared/_links.erb", "app/views/documents/_form.html.erb", "app/views/documents/edit.html.erb",
                       "app/views/documents/index.html.erb", "app/views/documents/new.html.erb", "app/views/documents/show.html.erb",
                       "app/views/projects/_form.html.erb", "app/views/projects/edit.html.erb", "app/views/projects/index.html.erb",
                       "app/views/projects/new.html.erb", "app/views/projects/show.html.erb", "app/views/visitors/index.html.erb"]
        def task844 = ["app/controllers/visitors_controller.rb", "app/models/user.rb", "app/views/devise/sessions/new.html.erb",
                       "app/views/devise/shared/_links.erb", "app/views/visitors/index.html.erb"]

        def task12 = ["app/admin/category.rb", "app/admin/dish.rb", "app/admin/translation.rb", "app/controllers/home_controller.rb",
                      "app/models/category.rb", "app/models/dish.rb", "app/models/translation.rb", "app/views/home/index.html.erb"]
        def task13 = ["app/admin/category.rb", "app/controllers/home_controller.rb", "app/models/category.rb",
                      "app/views/home/index.html.erb"]

        def task85 = ["app/controllers/blogs_controller.rb", "app/controllers/home_controller.rb",
                      "app/controllers/pages_controller.rb", "app/controllers/postings_controller.rb", "app/models/blog.rb",
                      "app/models/page.rb", "app/models/user.rb", "app/views/attachments/_attachment_fields.haml",
                      "app/views/blogs/_blog.haml", "app/views/blogs/_blog_intro.haml",
                      "app/views/blogs/_cover_picture.html.erb", "app/views/blogs/_form.html.erb",
                      "app/views/blogs/_posting_header.html.erb", "app/views/blogs/edit.html.erb",
                      "app/views/blogs/index.haml", "app/views/blogs/new.html.erb", "app/views/blogs/show.haml",
                      "app/views/comments/_comment_fields.html.erb", "app/views/home/_action_buttons.haml",
                      "app/views/home/_interpreter_help.haml", "app/views/home/_load_more.haml", "app/views/home/index.haml",
                      "app/views/home/menu/_application.haml", "app/views/home/menu/_with_children.haml",
                      "app/views/page_components/_page_component_fields.haml", "app/views/pages/_attachments.haml",
                      "app/views/pages/_buttons.haml", "app/views/pages/_comments.html.erb",
                      "app/views/pages/_cover_picture.html.erb", "app/views/pages/_form.haml", "app/views/pages/_page.html.erb",
                      "app/views/pages/_page_intro.html.erb", "app/views/pages/edit.html.erb", "app/views/pages/index.html.erb",
                      "app/views/pages/show.html.erb", "app/views/postings/_cover_picture.html.erb",
                      "app/views/postings/_form.html.erb", "app/views/postings/_posting.html.erb",
                      "app/views/postings/edit.html.erb", "app/views/postings/new.html.erb",
                      "app/views/postings/show.html.erb", "lib/translator/translator.rb"]
        def task86 = ["app/controllers/blogs_controller.rb", "app/controllers/home_controller.rb",
                      "app/controllers/page_templates_controller.rb", "app/controllers/pages_controller.rb",
                      "app/controllers/postings_controller.rb", "app/models/blog.rb", "app/models/page.rb",
                      "app/models/page_component.rb", "app/models/posting.rb", "app/models/site_menu.rb",
                      "app/models/user.rb", "app/views/attachments/_attachment_fields.haml", "app/views/blogs/_blog.haml",
                      "app/views/blogs/_blog.html.erb", "app/views/blogs/_blog_intro.haml",
                      "app/views/blogs/_cover_picture.html.erb", "app/views/blogs/_form.html.erb",
                      "app/views/blogs/_posting_header.html.erb", "app/views/blogs/edit.html.erb",
                      "app/views/blogs/index.haml", "app/views/blogs/index.html.erb", "app/views/blogs/new.html.erb",
                      "app/views/blogs/show.haml", "app/views/blogs/show.html.erb", "app/views/comments/_comment_fields.html.erb",
                      "app/views/home/_action_buttons.haml", "app/views/home/_interpreter_help.haml",
                      "app/views/home/_load_more.haml", "app/views/home/index.haml", "app/views/home/menu/_application.haml",
                      "app/views/home/menu/_page_template_menu.haml", "app/views/home/menu/_with_children.haml",
                      "app/views/page_components/_page_component_fields.haml", "app/views/page_templates/_form.haml",
                      "app/views/page_templates/_page_template.haml", "app/views/page_templates/edit.haml",
                      "app/views/page_templates/index.haml", "app/views/page_templates/new.haml",
                      "app/views/pages/_attachments.haml", "app/views/pages/_buttons.haml",
                      "app/views/pages/_comments.html.erb", "app/views/pages/_cover_picture.html.erb",
                      "app/views/pages/_form.haml", "app/views/pages/_page.html.erb", "app/views/pages/_page_intro.html.erb",
                      "app/views/pages/edit.html.erb", "app/views/pages/index.html.erb", "app/views/pages/new_article.haml",
                      "app/views/pages/show.html.erb", "app/views/pages/templates.haml",
                      "app/views/postings/_cover_picture.html.erb", "app/views/postings/_form.html.erb",
                      "app/views/postings/_posting.html.erb", "app/views/postings/edit.html.erb",
                      "app/views/postings/new.html.erb", "app/views/postings/show.html.erb"]

        def intersec_338_339 = task338.intersect(task339)
        def intersec_843_844 = task843.intersect(task844)
        def intersec_12_13 = task12.intersect(task13)
        def intersec_85_86 = task85.intersect(task86)

        println "intersec_338_339: ${intersec_338_339.size()}; 338: ${task338.size()}; 339: ${task339.size()}"
        println "intersec_843_844: ${intersec_843_844.size()}; 843: ${task843.size()}; 844: ${task844.size()}"
        println "intersec_12_13: ${intersec_12_13.size()}; 12: ${task12.size()}; 13: ${task13.size()}"
        println "intersec_85_86: ${intersec_85_86.size()}; 85: ${task85.size()}; 86: ${task86.size()}"
    }

}
