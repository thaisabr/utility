package study2.mergeConflicts

class VerifyIntersectionInConflictingMerges {

    static void main(String[] args){

        def task175 = ["app/controllers/home_controller.rb", "app/controllers/questions_controller.rb",
                       "app/models/choice.rb", "app/models/earl.rb", "app/models/item.rb", "app/models/question.rb",
                       "app/views/abingo_dashboard/_experiment_row.html.haml", "app/views/abingo_dashboard/index.html.haml",
                       "app/views/home/about.html.haml", "app/views/home/index.html.haml", "app/views/home/privacy.html.haml",
                       "app/views/questions/_idea.html.haml", "app/views/questions/about.html.haml", "app/views/questions/admin.html.haml",
                       "app/views/questions/new.html.haml", "app/views/questions/results.html.haml",
                       "app/views/questions/voter_map.html.erb", "app/views/questions/word_cloud.html.erb",
                       "app/views/shared/_google_jsapi.html.haml", "app/views/shared/_header_vote.html.haml",
                       "app/views/shared/_highcharts_header.html.haml"]
        def task176 = ["app/models/choice.rb", "app/models/earl.rb", "app/models/question.rb",
                       "app/views/abingo_dashboard/_experiment_row.html.haml", "app/views/abingo_dashboard/index.html.haml"]
        def task2174 = ["app/controllers/admin/users_controller.rb", "app/models/user.rb", "app/views/admin/users/allocate.html.haml"]
        def task2175 = ["app/controllers/case_workers/claims_controller.rb", "app/models/user.rb",
                        "app/views/case_workers/claims/index.html.haml"]
        def task1057 = ["app/controllers/admin_controller.rb", "app/controllers/application_controller.rb",
                        "app/controllers/child_media_controller.rb", "app/controllers/children_controller.rb",
                        "app/controllers/form_section_controller.rb", "app/controllers/highlight_fields_controller.rb",
                        "app/controllers/histories_controller.rb", "app/controllers/users_controller.rb",
                        "app/models/form_section.rb", "app/models/searchable.rb, app/models/user.rb",
                        "app/views/admin/index.html.erb", "app/views/children/_audio_player.html.erb",
                        "app/views/children/_audio_upload_box.html.erb", "app/views/children/_check_boxes.html.erb",
                        "app/views/children/_date_field.html.erb", "app/views/children/_field_display_audio.html.erb",
                        "app/views/children/_field_display_basic.html.erb", "app/views/children/_field_display_photo.html.erb",
                        "app/views/children/_form_section.html.erb", "app/views/children/_form_section_info.html.erb",
                        "app/views/children/_mark_as.html.erb", "app/views/children/_numeric_field.html.erb",
                        "app/views/children/_photo_upload_box.html.erb", "app/views/children/_picture.html.erb",
                        "app/views/children/_radio_button.html.erb", "app/views/children/_repeatable_text_field.html.erb",
                        "app/views/children/_search_results.html.erb", "app/views/children/_select_box.html.erb",
                        "app/views/children/_show_child_toolbar.erb", "app/views/children/_show_form_section.html.erb",
                        "app/views/children/_summary_row.html.erb", "app/views/children/_tabs.html.erb",
                        "app/views/children/_text_field.html.erb", "app/views/children/_textarea.html.erb",
                        "app/views/children/edit.html.erb", "app/views/children/edit_photo.html.erb", "app/views/children/index.html.erb",
                        "app/views/children/new.html.erb", "app/views/children/search.html.erb", "app/views/children/show.html.erb",
                        "app/views/form_section/index.html.erb", "app/views/highlight_fields/index.html.erb",
                        "app/views/histories/_audio_history_change.html.erb", "app/views/histories/_flag_change.erb",
                        "app/views/histories/_history_change.html.erb", "app/views/histories/_photo_history_change.html.erb",
                        "app/views/histories/_reunited_change.erb", "app/views/histories/show.html.erb",
                        "app/views/shared/_form_fields.html.erb", "app/views/users/_devices.html.erb",
                        "app/views/users/_edittable_user.html.erb", "app/views/users/_mobile_login_history.html.erb",
                        "app/views/users/edit.html.erb", "app/views/users/index.html.erb", "app/views/users/new.html.erb"]
        def task1058 = ["app/controllers/child_media_controller.rb", "app/controllers/children_controller.rb",
                        "app/controllers/form_section_controller.rb", "app/controllers/histories_controller.rb",
                        "app/controllers/sessions_controller.rb", "app/controllers/users_controller.rb", "app/models/field.rb",
                        "app/models/form_section.rb", "app/views/children/_audio_player.html.erb",
                        "app/views/children/_audio_upload_box.html.erb", "app/views/children/_check_boxes.html.erb",
                        "app/views/children/_date_field.html.erb", "app/views/children/_field_display_audio.html.erb",
                        "app/views/children/_field_display_basic.html.erb", "app/views/children/_field_display_photo.html.erb",
                        "app/views/children/_form_section.html.erb", "app/views/children/_form_section_info.html.erb",
                        "app/views/children/_mark_as.html.erb", "app/views/children/_numeric_field.html.erb",
                        "app/views/children/_photo_upload_box.html.erb", "app/views/children/_picture.html.erb",
                        "app/views/children/_radio_button.html.erb", "app/views/children/_repeatable_text_field.html.erb",
                        "app/views/children/_search_results.html.erb", "app/views/children/_select_box.html.erb",
                        "app/views/children/_show_child_toolbar.erb", "app/views/children/_show_form_section.html.erb",
                        "app/views/children/_summary_row.html.erb", "app/views/children/_tabs.html.erb",
                        "app/views/children/_text_field.html.erb", "app/views/children/_textarea.html.erb",
                        "app/views/children/edit.html.erb", "app/views/children/edit_photo.html.erb",
                        "app/views/children/index.html.erb", "app/views/children/new.html.erb", "app/views/children/search.html.erb",
                        "app/views/children/show.html.erb", "app/views/histories/_audio_history_change.html.erb",
                        "app/views/histories/_flag_change.erb", "app/views/histories/_history_change.html.erb",
                        "app/views/histories/_photo_history_change.html.erb", "app/views/histories/_reunited_change.erb",
                        "app/views/histories/show.html.erb"]
        def task2188 = ["app/controllers/dashboard_controller.rb", "app/models/community.rb",
                        "app/models/community_membership.rb",
                        "app/models/contact_request.rb", "app/models/email.rb", "app/models/person.rb",
                        "app/views/dashboard/_contact_request_form.haml", "app/views/dashboard/index.haml",
                        "lib/np_guid/uuidtools.rb"]
        def task2189 = ["app/controllers/dashboard_controller.rb", "app/models/community.rb",
                        "app/models/community_membership.rb", "app/models/contact_request.rb",
                        "app/models/email.rb", "app/models/person.rb", "app/views/dashboard/_contact_request_form.haml",
                        "app/views/dashboard/index.haml"]


        def intersec_175_176 = task175.intersect(task176)
        def intersec_2174_2175 = task2174.intersect(task2175)
        def intersec_1057_1058 = task1057.intersect(task1058)
        def intersec_2188_2189 = task2188.intersect(task2189)

        println "intersec_175_176: ${intersec_175_176.size()}; 175: ${task175.size()}; 176: ${task176.size()}"
        println "intersec_2174_2175: ${intersec_2174_2175.size()}; 2174: ${task2174.size()}; 2175: ${task2175.size()}"
        println "intersec_1057_1058: ${intersec_1057_1058.size()}; 1057: ${task1057.size()}; 1058: ${task1058.size()}"
        println "intersec_2188_2189: ${intersec_2188_2189.size()}; 2188: ${task2188.size()}; 2189: ${task2189.size()}"
    }

}
