package study2.appendix

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

/*
* Gera um csv de merge tasks das tarefas da amostra final. 990 tarefas.
* */
class SelectedMergeTasks {

    static final int PROJECT = 0
    static final int ID = 1

    static void main(String[] args){
        def folder = "D:\\Dropbox\\Thaís\\phd_study2_final_result\\"
        def resultFile = "${folder}resultados_apendice\\tasks_data_990.csv"
        def outputFile = "${folder}resultados_apendice\\tasks_990.csv"

        def resultData = CsvUtil.read(resultFile)
        resultData.remove(0)
        println "resultData: ${resultData.size()}"
        println resultData.first()

        def mergeFilesFolder = "${folder}testi\\result_organized\\candidates"
        def mergeFiles = Util.findFilesFromDirectory(mergeFilesFolder).findAll{
            it.endsWith("-tasks_candidates.csv")
        }
        println "mergeFiles: ${mergeFiles.size()}"

        String[] header = null
        List<String[]> mergeTasks = []
        mergeFiles.each{ file ->
            def lines = CsvUtil.read(file)
            if(header == null) header = lines.get(0)
            lines.remove(0)
            mergeTasks += lines
        }
        println "mergeTasks: ${mergeTasks.size()}"
        println mergeTasks.first()

        List<String[]> data = []
        data += header
        resultData.each{ r ->
            def match = mergeTasks.find{ mt ->
                (mt[PROJECT] == (r[PROJECT]+".git")) && (mt[ID] == r[ID])
            }
            if(match!=null) data += match
        }
        println "data: ${data.size()-1}"
        CsvUtil.write(outputFile, data)
    }
}
