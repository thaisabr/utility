package study2.appendix

import br.ufpe.cin.tan.util.CsvUtil

/*
* Script que gera um csv único com TestI, o conjunto de arquivos alterados pelas tarefas e TextI, mas apenas para a amostra
* final de tarefas do estudo (tarefas cujo TestI e TextI não são vazios, e que são integráveis). Total de 990 tarefas.
* */
class CollapseResult {

    static final int PROJECT = 0
    static final int ID = 1
    static final int TESTI = 2
    static final int CHANGESET = 3
    static final int TEXTI = 2

    static void main(String[] args){
        def folder = "D:\\Dropbox\\Thaís\\phd_study2_final_result\\"
        def testiFile = "${folder}resultados_apendice\\testi_changeset.csv"
        def textiFile = "${folder}resultados_apendice\\texti.csv"
        def outputFile = "${folder}resultados_apendice\\tasks_data_990.csv"

        //lê o resultado de TestI previamente organizado
        def testiData = CsvUtil.read(testiFile)
        testiData.remove(0)
        println "testiData: ${testiData.size()}"

        //lê o resultado de TextI previamente organizado
        def textiData = CsvUtil.read(textiFile)
        textiData.remove(0)
        println "textiData: ${textiData.size()}"

        //faz o match e exporta
        List<String[]> data = []
        data += ["Project", "Task", "Changed files", "TestI", "TextI"] as String[]
        testiData.each{ testi ->
            def match = textiData.find{ texti ->
                (texti[PROJECT] == testi[PROJECT]) && (texti[ID] == testi[ID])
            }
            if(match!=null){
                data += [testi[PROJECT], testi[ID], testi[CHANGESET], testi[TESTI], match[TEXTI]] as String[]
            }
        }
        println "data: ${data.size()-1}"
        CsvUtil.write(outputFile, data)
    }

}
