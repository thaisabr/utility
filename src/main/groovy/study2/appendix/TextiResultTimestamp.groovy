package study2.appendix

import br.ufpe.cin.tan.util.CsvUtil

/*
* Script que gera um csv com o resultado de TextI, mas apenas para a amostra final de tarefas do estudo (tarefas cujo
* TestI e TextI não são vazios, e que são integráveis). Total de 990 tarefas.
* */

class TextiResultTimestamp {

    static final int PROJECT = 0
    static final int ID = 1
    static final int TEXTI = 3

    static void main(String[] args){
        def folder = "D:\\Onedrive\\Documentos\\phd_study2_final_result\\texti\\result_organized\\final-result"
        def nonEmptyTextiFile = "${folder}\\texti-noempty.csv"
        def timestampFile = "${folder}\\texti-timestamp.csv"

        //extrai resultado de TextI
        def result = []
        def lines = CsvUtil.read(nonEmptyTextiFile)
        lines.remove(0)
        println "texti: ${lines.size()}"

        //extrai timestamp
        List<String[]> finalResult = []
        finalResult += ["Project","Task","TextI_size","TextI","TaskI_size","TaskI","Timestamp"] as String[]
        def timestampLines = CsvUtil.read(timestampFile)
        timestampLines.remove(0)
        println "timestamp: ${timestampLines.size()}"
        lines.each { line ->
            def match = timestampLines.find{
                it[0]==line[0] && it[1]==line[1] && it[2]==line[2] && it[3]==line[3] && it[4]==line[4] && it[5]==line[5]
            }
            if(match){
                match[0] = convertTextProjectToUrl(match[0])
                finalResult += match as String[]
            }
        }

        println "finalResult: ${finalResult.size()}"

        //exporta resultado
        def outputFile = "D:\\Onedrive\\Documentos\\phd_study2_final_result\\resultados_apendice\\texti_timestamp.csv"
        CsvUtil.write(outputFile, finalResult)
    }

    private static convertTextProjectToUrl(String project){
        def url = ""
        switch (project){
            case "allourideas.org": url = "https://github.com/allourideas/allourideas.org"
                break
            case "e-petitions": url = "https://github.com/alphagov/e-petitions"
                break
            case "whitehall": url = "https://github.com/alphagov/whitehall"
                break
            case "bsmi": url = "https://github.com/BTHUNTERCN/bsmi"
                break
            case "enroll": url = "https://github.com/dchbx/enroll"
                break
            case "diaspora": url = "https://github.com/diaspora/diaspora"
                break
            case "action-center-platform": url = "https://github.com/EFForg/action-center-platform"
                break
            case "gitlabhq": url = "https://github.com/gitlabhq/gitlabhq"
                break
            case "wontomedia": url = "https://github.com/gleneivey/wontomedia"
                break
            case "jekyll": url = "https://github.com/jekyll/jekyll"
                break
            case "claim-for-crown-court-defence": url = "https://github.com/ministryofjustice/Claim-for-Crown-Court-Defence"
                break
            case "one-click-orgs": url = "https://github.com/oneclickorgs/one-click-orgs"
                break
            case "opengovernment": url = "https://github.com/opengovernment/opengovernment"
                break
            case "openproject": url = "https://github.com/opf/openproject"
                break
            case "otwarchive": url = "https://github.com/otwcode/otwarchive"
                break
            case "rapidftr": url = "https://github.com/rapidftr/RapidFTR"
                break
            case "quantified": url = "https://github.com/sachac/quantified"
                break
            case "sequencescape": url = "https://github.com/sanger/sequencescape"
                break
            case "sharetribe": url = "https://github.com/sharetribe/sharetribe"
        }
        url
    }

}
