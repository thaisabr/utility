package study2.appendix

import br.ufpe.cin.tan.util.CsvUtil
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

/*
* Script que gera um csv único com TestI, o conjunto de arquivos alterados pelas tarefas e TextI, mas apenas para a amostra
* final de tarefas do estudo (tarefas cujo TestI e TextI não são vazios, e que são integráveis). Total de 990 tarefas.
* O script adiciona o timestamp para calcular TestI e TextI, a fim de comparar tais valores.
* */

class CollapseResultTimestamp {

    static void main(String[] args){
        def folder = "D:\\Onedrive\\Documentos\\phd_study2_final_result\\"
        def summarizedResultFile = "${folder}resultados_apendice\\tasks_data_990.csv"
        def timestampTestiFile = "${folder}resultados_apendice\\testi_changeset_timestamp.csv"
        def timestampTextiFile= "${folder}resultados_apendice\\texti_timestamp.csv"
        def outputFileWithTimestamp = "${folder}resultados_apendice\\tasks_data_990_timestamp.csv"

        //lê o resultado de TestI com timestamp //Project,"ID","TestI","Changed files","Timestamp"
        def testiData = CsvUtil.read(timestampTestiFile)
        testiData.remove(0)
        println "testiData: ${testiData.size()}"

        //lê o resultado de TextI com timestamp //Project,"Task","TextI_size","TextI","TaskI_size","TaskI","Timestamp"
        def textiData = CsvUtil.read(timestampTextiFile)
        textiData.remove(0)
        println "textiData: ${textiData.size()}"

        //lê o resultado resumido com TestI e TextI para 990 tarefas
        def summarizedData = CsvUtil.read(summarizedResultFile)
        summarizedData.remove(0)
        println "summarizedData: ${summarizedData.size()}"

        List<String[]> data = []
        data += ["Project", "Task", "Changed files", "TestI", "TextI", "TestI_Timestamp", "TextI_Timestamp"] as String[]
        summarizedData.each{ sd ->
            def testiTimestamp = testiData.find{ it[0]==sd[0] && it[1]==sd[1]}
            if(testiTimestamp) testiTimestamp = testiTimestamp[4]
            def textiTimestamp = textiData.find{ it[0]==sd[0] && it[1]==sd[1]}
            if(textiTimestamp) textiTimestamp = textiTimestamp[6]
            if(testiTimestamp!=null && textiTimestamp!=null) {
                data += [sd[0], sd[1], sd[2], sd[3], sd[4], testiTimestamp, textiTimestamp] as String[]
            }
        }
        println "data: ${data.size()-1}"
        CsvUtil.write(outputFileWithTimestamp, data)

        //calcula média, mediana e desvio padrão do timestamp
        data.remove(0)
        def testiTimestampValues = data.collect{parseTimeDurationString(it[5])}

        def statistics = new DescriptiveStatistics(testiTimestampValues.allMillis as double[])
        println "TestI Timestamp overview"
        println "mean: ${statistics.mean}"
        println "median: ${statistics.getPercentile(50.0)}"
        println "standard deviation: ${statistics.standardDeviation}"

        def textiTimestampValues = data.collect{parseTimeDurationString(it[6])}
        statistics = new DescriptiveStatistics(textiTimestampValues.allMillis as double[])
        println "TextI Timestamp overview"
        println "mean: ${statistics.mean}"
        println "median: ${statistics.getPercentile(50.0)}"
        println "standard deviation: ${statistics.standardDeviation}"
    }

    private static parseTimeDurationString(String text){
        def minutes = ""
        def seconds = ""
        if(text.contains("minutes")){
            def index = text.indexOf(",")
            minutes = (text.substring(0, index) - " minutes").trim()
            text = text.substring(index+1).trim()
        }
        if(text.contains("seconds")){
            def index = text.indexOf("seconds")
            seconds = text.substring(0, index).trim()
        }

        double minutesValue = 0
        double secondsValue = 0
        if(!minutes.empty) minutesValue = Double.parseDouble(minutes)
        if(!seconds.empty) secondsValue = Double.parseDouble(seconds)

        double allMillis = (minutesValue*60*1000) + (secondsValue*1000)
        double allSeconds = (minutesValue*60) + secondsValue

        [seconds: secondsValue, minutes: minutesValue, allSeconds: allSeconds, allMillis:allMillis]
    }

}
