package study2.appendix

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util



class TestiResultGems {

    static final int ID = 1
    static final int TESTI = 27
    static final int TASKI = 28
    static final int GEMS = 34

    static void main(String[] args){
        def folder = "D:\\Onedrive\\Documentos\\phd_study2_final_result\\testi\\result_organized\\final-result"
        def files = Util.findFilesFromDirectory(folder).findAll{
            it.endsWith("-tasks-relevant-detailed.csv")
        }
        println "files: ${files.size()}"

        //extrai dados de interesse em arquivos de tarefas relevantes
        def result = []
        files.each{ file ->
            def lines = CsvUtil.read(file)
            def url = (lines[0][1]) - ".git"
            lines = lines.subList(19, lines.size())
            lines.each{ line ->
                def testi = line[TESTI].substring(1, line[TESTI].size()-1).tokenize(",")*.trim()
                def taski = line[TASKI].substring(1, line[TASKI].size()-1).tokenize(",")*.trim()
                def gems = line[GEMS].substring(1, line[GEMS].size()-1).tokenize(",")*.trim()
                result += [url:url, id: line[ID] as int, testi: testi, taski: taski, gems:gems]
            }
        }
        println "result: ${result.size()}"
        println result.first()

        //verifica interseção com resultado de tarefas cujo TextI não é vazio
        def conflictFile = "D:\\Onedrive\\Documentos\\phd_study2_final_result\\conflict_prediction\\output\\conflict_noemptytexti.csv"
        def conflictResult = CsvUtil.read(conflictFile)
        def selectedTasks = []
        conflictResult.remove(0)
        def resultPerProject = conflictResult.groupBy { it[0] }
        resultPerProject.each { rpp ->
            def left = rpp.value.collect { it[1] }
            def right = rpp.value.collect { it[2] }
            def tasks = (left + right).unique()
            def url = convertProjectToUrl(rpp.key)
            tasks.each{ t ->
                selectedTasks += [url: url, id: t as int]
            }
        }
        println "selectedTasks: ${selectedTasks.size()}"
        println selectedTasks.first()

        def finalResult = []
        result.each{ r ->
            def match = selectedTasks.find{ (r.url==it.url) && (r.id==it.id) }
            if(match){
                finalResult += r
            }
        }
        println "finalResult: ${finalResult.size()}"

        //exporta resultado de TestI e arquivos alterados para as 990 tarefas da amostra final
        List<String[]> data = []
        data += ["Project", "ID", "TestI", "Changed files", "Gems"] as String[]
        finalResult.each{ r ->
            data += [r.url, r.id, r.testi, r.taski, r.gems] as String[]
        }
        CsvUtil.write("D:\\Onedrive\\Documentos\\phd_study2_final_result\\resultados_apendice\\testi_changeset_gems.csv", data)
    }

    private static convertProjectToUrl(String project){
        def url = ""
        switch (project){
            case "allourideas_allourideas.org": url = "https://github.com/allourideas/allourideas.org"
                break
            case "alphagov_e": url = "https://github.com/alphagov/e-petitions"
                break
            case "alphagov_whitehall": url = "https://github.com/alphagov/whitehall"
                break
            case "bthuntercn_bsmi": url = "https://github.com/BTHUNTERCN/bsmi"
                break
            case "dchbx_enroll": url = "https://github.com/dchbx/enroll"
                break
            case "diaspora_diaspora": url = "https://github.com/diaspora/diaspora"
                break
            case "efforg_action": url = "https://github.com/EFForg/action-center-platform"
                break
            case "gitlabhq_gitlabhq": url = "https://github.com/gitlabhq/gitlabhq"
                break
            case "gleneivey_wontomedia": url = "https://github.com/gleneivey/wontomedia"
                break
            case "jekyll_jekyll": url = "https://github.com/jekyll/jekyll"
                break
            case "ministryofjustice_claim": url = "https://github.com/ministryofjustice/Claim-for-Crown-Court-Defence"
                break
            case "oneclickorgs_one": url = "https://github.com/oneclickorgs/one-click-orgs"
                break
            case "opengovernment_opengovernment": url = "https://github.com/opengovernment/opengovernment"
                break
            case "opf_openproject": url = "https://github.com/opf/openproject"
                break
            case "otwcode_otwarchive": url = "https://github.com/otwcode/otwarchive"
                break
            case "rapidftr_rapidftr": url = "https://github.com/rapidftr/RapidFTR"
                break
            case "sachac_quantified": url = "https://github.com/sachac/quantified"
                break
            case "sanger_sequencescape": url = "https://github.com/sanger/sequencescape"
                break
            case "sharetribe_sharetribe": url = "https://github.com/sharetribe/sharetribe"
        }
        url
    }



}
