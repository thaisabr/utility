package study2.appendix

import br.ufpe.cin.tan.util.CsvUtil

/*
* Script que gera um csv com o resultado de TextI, mas apenas para a amostra final de tarefas do estudo (tarefas cujo
* TestI e TextI não são vazios, e que são integráveis). Total de 990 tarefas.
* */
class TextiResult {

    static final int PROJECT = 0
    static final int ID = 1
    static final int TEXTI = 3

    static void main(String[] args){
        def folder = "D:\\Dropbox\\Thaís\\phd_study2_final_result\\texti\\result_organized\\final-result"
        def file = "${folder}\\texti-noempty.csv"

        //extrai resultado de TextI
        def result = []
        def lines = CsvUtil.read(file)
        lines.remove(0)
        lines.each{ line ->
            def url = convertTextProjectToUrl(line[PROJECT])
            def texti = line[TEXTI].substring(1, line[TEXTI].size()-1).tokenize(",")*.trim()
            result += [url: url, id: line[ID] as int, texti: texti]
        }
        println "result: ${result.size()}"
        println result.first()

        //verifica interseção com resultado de tarefas cujo TextI não é vazio
        def conflictFile = "D:\\Dropbox\\Thaís\\phd_study2_final_result\\conflict_prediction\\output\\conflict_noemptytexti.csv"
        def conflictResult = CsvUtil.read(conflictFile)
        def selectedTasks = []
        conflictResult.remove(0)
        def resultPerProject = conflictResult.groupBy { it[0] }
        resultPerProject.each { rpp ->
            def left = rpp.value.collect { it[1] }
            def right = rpp.value.collect { it[2] }
            def tasks = (left + right).unique()
            def url = convertConflictProjectToUrl(rpp.key)
            tasks.each{ t ->
                selectedTasks += [url: url, id: t as int]
            }
        }
        println "selectedTasks: ${selectedTasks.size()}"
        println selectedTasks.first()

        def finalResult = []
        result.each{ r ->
            def match = selectedTasks.find{ (r.url==it.url) && (r.id==it.id) }
            if(match){
                finalResult += r
            }
        }
        println "finalResult: ${finalResult.size()}"

        //exporta resultado de TestI e arquivos alterados para as 990 tarefas da amostra final
        List<String[]> data = []
        data += ["Project", "ID", "TextI"] as String[]
        finalResult.each{ r ->
            data += [r.url, r.id, r.texti] as String[]
        }
        CsvUtil.write("D:\\Dropbox\\Thaís\\phd_study2_final_result\\resultados_apendice\\texti.csv", data)
    }

    private static convertConflictProjectToUrl(String project){
        def url = ""
        switch (project){
            case "allourideas_allourideas.org": url = "https://github.com/allourideas/allourideas.org"
                break
            case "alphagov_e": url = "https://github.com/alphagov/e-petitions"
                break
            case "alphagov_whitehall": url = "https://github.com/alphagov/whitehall"
                break
            case "bthuntercn_bsmi": url = "https://github.com/BTHUNTERCN/bsmi"
                break
            case "dchbx_enroll": url = "https://github.com/dchbx/enroll"
                break
            case "diaspora_diaspora": url = "https://github.com/diaspora/diaspora"
                break
            case "efforg_action": url = "https://github.com/EFForg/action-center-platform"
                break
            case "gitlabhq_gitlabhq": url = "https://github.com/gitlabhq/gitlabhq"
                break
            case "gleneivey_wontomedia": url = "https://github.com/gleneivey/wontomedia"
                break
            case "jekyll_jekyll": url = "https://github.com/jekyll/jekyll"
                break
            case "ministryofjustice_claim": url = "https://github.com/ministryofjustice/Claim-for-Crown-Court-Defence"
                break
            case "oneclickorgs_one": url = "https://github.com/oneclickorgs/one-click-orgs"
                break
            case "opengovernment_opengovernment": url = "https://github.com/opengovernment/opengovernment"
                break
            case "opf_openproject": url = "https://github.com/opf/openproject"
                break
            case "otwcode_otwarchive": url = "https://github.com/otwcode/otwarchive"
                break
            case "rapidftr_rapidftr": url = "https://github.com/rapidftr/RapidFTR"
                break
            case "sachac_quantified": url = "https://github.com/sachac/quantified"
                break
            case "sanger_sequencescape": url = "https://github.com/sanger/sequencescape"
                break
            case "sharetribe_sharetribe": url = "https://github.com/sharetribe/sharetribe"
        }
        url
    }

    /*privileges
    rigse
    cufcq
    atlrug500
    folioapp
    moumentei
    elektra*/
    private static convertTextProjectToUrl(String project){
        def url = ""
        switch (project){
            case "allourideas.org": url = "https://github.com/allourideas/allourideas.org"
                break
            case "e-petitions": url = "https://github.com/alphagov/e-petitions"
                break
            case "whitehall": url = "https://github.com/alphagov/whitehall"
                break
            case "bsmi": url = "https://github.com/BTHUNTERCN/bsmi"
                break
            case "enroll": url = "https://github.com/dchbx/enroll"
                break
            case "diaspora": url = "https://github.com/diaspora/diaspora"
                break
            case "action-center-platform": url = "https://github.com/EFForg/action-center-platform"
                break
            case "gitlabhq": url = "https://github.com/gitlabhq/gitlabhq"
                break
            case "wontomedia": url = "https://github.com/gleneivey/wontomedia"
                break
            case "jekyll": url = "https://github.com/jekyll/jekyll"
                break
            case "claim-for-crown-court-defence": url = "https://github.com/ministryofjustice/Claim-for-Crown-Court-Defence"
                break
            case "one-click-orgs": url = "https://github.com/oneclickorgs/one-click-orgs"
                break
            case "opengovernment": url = "https://github.com/opengovernment/opengovernment"
                break
            case "openproject": url = "https://github.com/opf/openproject"
                break
            case "otwarchive": url = "https://github.com/otwcode/otwarchive"
                break
            case "rapidftr": url = "https://github.com/rapidftr/RapidFTR"
                break
            case "quantified": url = "https://github.com/sachac/quantified"
                break
            case "sequencescape": url = "https://github.com/sanger/sequencescape"
                break
            case "sharetribe": url = "https://github.com/sharetribe/sharetribe"
        }
        url
    }

}
