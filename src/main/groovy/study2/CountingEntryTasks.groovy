package study2

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util

/*
*  Faz o balanço sobre as tarefas extraídas dos projetos a partir de merges e para as quais podemos calcular TestI.
*  A ideia é saber quantas tarefas foram extraídas por projeto, quantas alteram produção e testes e quantas são independentes
*  de outras.
* */
class CountingEntryTasks {

    static printFiles(String folder){
        println "folder: $folder"

        def mergeFiles = Util.findFilesFromDirectory(folder).findAll{
            it.contains("${File.separator}merges${File.separator}") &&
                    !it.endsWith("_fastforward_merges.csv") &&
                    !it.endsWith("_problematic_merges.csv") &&
                    it.endsWith("_merges.csv")
        }

        println "merge files: ${mergeFiles.size()}"
        mergeFiles.each{ file ->
            def lines = CsvUtil.read(file)
            if(lines.size()>2){
                def url = lines[0][0]
                lines.remove(0)
                lines.remove(0)
                println "$url; ${lines.size()}"
            } else {
                println "$file; empty"
            }
        }

        def mergeTasksFiles = Util.findFilesFromDirectory(folder).findAll{
            !it.contains("${File.separator}merges${File.separator}") &&
                    it.endsWith("-merge-tasks.csv")
        }

        println "merge task files: ${mergeTasksFiles.size()}"
        mergeTasksFiles.each{ file ->
            def lines = CsvUtil.read(file)
            lines.remove(0)
            if(lines.size()>0){
                def url = lines[0][0]
                println "$url; ${lines.size()}"
            } else {
                println "$file; empty"
            }
        }

        def ptTasksFiles = Util.findFilesFromDirectory(folder).findAll{
            !it.contains("${File.separator}merges${File.separator}") &&
                    it.endsWith("-pt-tasks.csv")
        }

        println "pt task files: ${ptTasksFiles.size()}"
        ptTasksFiles.each{ file ->
            def lines = CsvUtil.read(file)
            lines.remove(0)
            if(lines.size()>0){
                def url = lines[0][0]
                println "$url; ${lines.size()}"
            } else {
                println "$file; empty"
            }
        }

        //esse código omite o arquivo do projeto https://github.com/RailsApps/rails3-devise-rspec-cucumber
        def independentTasksFiles = Util.findFilesFromDirectory(folder).findAll{
            !it.contains("${File.separator}merges${File.separator}") &&
                    it.endsWith("-independent-tasks.csv") &&
                    !it.endsWith("-cucumber-independent-tasks.csv") &&
                    !it.endsWith("-cucumber-cucumber-independent-tasks.csv")
        }

        println "independent task files: ${independentTasksFiles.size()}"
        independentTasksFiles.each{ file ->
            def lines = CsvUtil.read(file)
            lines.remove(0)
            if(lines.size()>0){
                def url = lines[0][0]
                println "$url; ${lines.size()}"
            } else {
                println "$file; empty"
            }
        }
    }

    static void main(String[] args){
        def folder1 = "D:\\Dropbox\\Thaís\\phd_study2\\new_sample\\task_extraction\\3-tasks"
        printFiles(folder1)

        //def folder2 = "D:\\Dropbox\\Thaís\\phd_study2\\paper_sample\\task_extraction\\3-tasks"
        //printFiles(folder2)
    }

}
