package study2

import br.ufpe.cin.tan.util.CsvUtil
import br.ufpe.cin.tan.util.Util
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics

class CountingAnalyzedTasks {

    def testiTasks
    def finalTestiTasks
    def testiGivenTasks

    static importTasks(String folder){
        def relevantTasks = Util.findFilesFromDirectory(folder).findAll{
            it.endsWith("-tasks-relevant.csv")
        }
        println "relevant task files: ${relevantTasks.size()}"

        def output = []
        relevantTasks.each{ file ->
            def lines = CsvUtil.read(file)
            if(lines.size()>0){
                def url = lines[0][1]
                lines = lines.subList(13, lines.size())
                lines.each{ line ->
                    output += [url: url, id: line[0], size:line[7] as double]
                }
            }
        }
        output
    }

    def importTestiTasks(){
        def folder = "D:\\Onedrive\\Documentos\\phd_study2_final_result\\testi\\result_organized\\final-result"
        println "importing TestI Tasks"
        testiTasks = importTasks(folder)
    }

    def importTestiGivenTasks(){
        def folder = "C:\\Users\\tatab\\Desktop\\testi_given_exclude_nov20\\result_organized\\final-result"
        println "importing TestI-Given Tasks"
        testiGivenTasks = importTasks(folder)
    }

    def configureFinalTestiTasks(){
        /*finalTestiTasks = []
        testiTasks.each { t ->
            def match = testiGivenTasks.find{
                it.url==t.url && it.id==t.id
            }
            if(match!=null) finalTestiTasks += t
        }*/
        finalTestiTasks = testiTasks
    }

    def print(){
        importTestiTasks()
        importTestiGivenTasks()
        configureFinalTestiTasks()

        def testiSizeBuffer = finalTestiTasks*.size.flatten()
        //println "testiSizeBuffer: ${testiSizeBuffer}"
        double[] testiSize = testiSizeBuffer as double[]
        println "testi tasks: ${finalTestiTasks.size()}"
        println "data about testi size: ${testiSizeBuffer.size()}"
        def statistics = new DescriptiveStatistics(testiSize)
        println "testi size: [mean: ${statistics.mean}, median: ${statistics.getPercentile(50.0)}, " +
                "sd: ${statistics.standardDeviation}]"

        def testiGivenSizeBuffer = testiGivenTasks*.size.flatten()
        println "original data about testi-given size: ${testiGivenSizeBuffer.size()}"
        def difference = testiSizeBuffer.size() - testiGivenSizeBuffer.size()
        def aux = []
        for(n in 1..difference){
            aux += 0
        }
        testiGivenSizeBuffer = testiGivenSizeBuffer + aux
        testiSize = testiGivenSizeBuffer as double[]
        println "testi-given tasks: ${testiGivenTasks.size()}"
        println "data about testi-given size: ${testiGivenSizeBuffer.size()}"
        statistics = new DescriptiveStatistics(testiSize)
        println "testi-given size: [mean: ${statistics.mean}, median: ${statistics.getPercentile(50.0)}, " +
                "sd: ${statistics.standardDeviation}]"
    }

    static void main(String[] args){
        def obj = new CountingAnalyzedTasks()
        obj.print()
    }

}
