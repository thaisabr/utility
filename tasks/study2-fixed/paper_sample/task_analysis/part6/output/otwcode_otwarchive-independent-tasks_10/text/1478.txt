Feature: creating and editing skins
Scenario: New skin form should have the correct skin type pre-selected
Given I am logged in as "skinner"
When I am on the skins page
And I follow "Create Skin"
Then "Site Skin" should be selected within "skin_type"
When I am on the skins page
And I follow "Work Skins"
And I follow "Create Skin"
Then "Work Skin" should be selected within "skin_type"
Scenario: Skin type should persist and remain selectable if you encounter errors during creation
Given I am logged in as "skinner"
When I am on the skins page
And I follow "Work Skins"
And I follow "Create Skin"
And I fill in "Title" with "invalid skin"
And I fill in "CSS" with "this is invalid css"
And I submit
Then I should see errors
And "Work Skin" should be selected within "skin_type"
When I select "Site Skin" from "skin_type"
And I fill in "CSS" with "still invalid css"
And I submit
Then I should see errors
And "Site Skin" should be selected within "skin_type"
