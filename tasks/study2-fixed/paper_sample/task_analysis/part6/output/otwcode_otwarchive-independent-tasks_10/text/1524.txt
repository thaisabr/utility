Feature: Edit chapters
In order to have an work full of chapters
As a humble user
I want to add and remove chapters
Scenario: Create a work and add a draft chapter, edit the draft chapter, and save changes to the draft chapter without previewing or posting
Given basic tags
And I am logged in as "moose" with password "muffin"
When I go to the new work page
Then I should see "Post New Work"
And I select "General Audiences" from "Rating"
And I check "No Archive Warnings Apply"
And I fill in "Fandoms" with "If You Give an X a Y"
And I fill in "Work Title" with "If You Give Users a Draft Feature"
And I fill in "content" with "They will expect it to work."
And I press "Post Without Preview"
When I should see "Work was successfully posted."
And I should see "They will expect it to work."
When I follow "Add Chapter"
And I fill in "content" with "And then they will request more features for it."
And I press "Preview"
Then I should see "This is a draft showing what this chapter will look like when it's posted to the Archive."
And I should see "And then they will request more features for it."
When I press "Edit"
And I fill in "content" with "And then they will request more features for it. Like the ability to save easily."
And I press "Save Without Posting"
Then I should see "Chapter was successfully updated."
And I should see "This chapter is a draft and hasn't been posted yet!"
And I should see "Like the ability to save easily."
Feature: Work Drafts
Scenario: Creating a work draft, editing it, and saving the changes without posting or previewing and then double check that it is saved and I didn't get the success message erroneously
Given basic tags
And I am logged in as "persnickety" with password "editingisfun"
When I go to the new work page
Then I should see "Post New Work"
And I select "General Audiences" from "Rating"
And I check "No Archive Warnings Apply"
And I fill in "Fandoms" with "MASH (TV)"
And I fill in "Work Title" with "Draft Dodging"
And I fill in "content" with "Klinger lay under his porch."
And I press "Preview"
Then I should see "Draft was successfully created."
When I press "Edit"
Then I should see "Edit Work"
And I fill in "content" with "Klinger, in Uncle Gus's Aunt Gussie dress, lay under his porch."
And I press "Save Without Posting"
Then I should see "This work is a draft and has not been posted."
And I should see "Klinger, in Uncle Gus's Aunt Gussie dress, lay under his porch."
When I am on persnickety's works page
Then I should not see "Draft Dodging"
And I should see "Drafts (1)"
When I follow "Drafts (1)"
Then I should see "Draft Dodging"
When I follow "Draft Dodging"
Then I should see "Klinger, in Uncle Gus's Aunt Gussie dress, lay under his porch."
Scenario: Saving changes to an existing draft without posting and then double check that it is saved and I didn't get the success message erroneously
Given I am logged in as "drafty" with password "breezeinhere"
And the draft "Windbag"
When I am on drafty's works page
Then I should see "Drafts (1)"
When I follow "Drafts (1)"
Then I should see "Windbag"
And I should see "Edit" within "#main .own.work.blurb .navigation"
When I follow "Edit"
Then I should see "Edit Work"
When I fill in "content" with "My draft has changed!"
And I press "Save Without Posting"
Then I should see "This work is a draft and has not been posted"
And I should see "My draft has changed!"
When I am on drafty's works page
Then I should see "Drafts (1)"
When I follow "Drafts (1)"
Then I should see "Windbag"
When I follow "Windbag"
Then I should see "My draft has changed!"
