Feature: creating and editing skins
Scenario: Admins should be able to edit but not delete public approved skins
Given the approved public skin "public skin" with css "#title { text-decoration: blink;}"
And I am logged in as an admin
When I go to "public skin" skin page
Then I should see "Edit"
But I should not see "Delete"
When I follow "Edit"
And I fill in "CSS" with "#greeting.logged-in { text-decoration: blink;}"
And I fill in "Description" with "Blinky love (admin modified)"
And I submit
Then I should see an update confirmation message
And I should see "(admin modified)"
And I should see "#greeting.logged-in"
And I should not see "#title"
Then the cache of the skin on "public skin" should expire after I save the skin
Scenario: Users should be able to create and use a work skin
Given I am logged in as "skinner"
And the default ratings exist
When I am on skin's new page
And I select "Work Skin" from "skin_type"
And I fill in "Title" with "Awesome Work Skin"
And I fill in "Description" with "Great work skin"
And I fill in "CSS" with "p {color: purple;}"
And I submit
Then I should see "Skin was successfully created"
And I should see "#workskin p"
When I go to the new work page
Then I should see "Awesome Work Skin"
When I set up the draft "Story With Awesome Skin"
And I select "Awesome Work Skin" from "work_work_skin_id"
And I press "Preview"
Then I should see "Preview"
And I should see "color: purple" within "style"
When I press "Post"
Then I should see "Story With Awesome Skin"
And I should see "color: purple" within "style"
And I should see "Hide Creator's Style"
When I follow "Hide Creator's Style"
Then I should see "Story With Awesome Skin"
And I should not see "color: purple"
And I should not see "Hide Creator's Style"
And I should see "Show Creator's Style"
Then the cache of the skin on "Awesome Work Skin" should expire after I save the skin
Scenario: Vendor-prefixed properties should be allowed
Given basic skins
And I am logged in as "skinner"
When I am on skin's new page
And I fill in "Title" with "skin with prefixed property"
And I fill in "CSS" with ".myclass { -moz-box-sizing: border-box; -webkit-transition: opacity 2s; }"
And I submit
Then I should see "Skin was successfully created"
Then the cache of the skin on "skin with prefixed property" should expire after I save the skin
Scenario: The cache should be flushed with a parent and not when unrelated
Given I have loaded site skins
And I am logged in as "skinner"
And I set up the skin "Complex"
And I select "replace archive skin entirely" from "skin_role"
And I check "add_site_parents"
And I submit
Then I should see a create confirmation message
When I am on skin's new page
And I fill in "Title" with "my blinking skin"
And I fill in "CSS" with "#title { text-decoration: blink;}"
And I submit
Then I should see "Skin was successfully created"
Then the cache of the skin on "my blinking skin" should not expire after I save "Complex"
Then the cache of the skin on "Complex" should expire after I save a parent skin
