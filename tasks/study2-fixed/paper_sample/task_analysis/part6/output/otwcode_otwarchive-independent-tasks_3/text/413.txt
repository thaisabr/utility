Feature: Create bookmarks
In order to have an archive full of bookmarks
As a humble user
I want to bookmark some works
Scenario: Adding bookmark to non-existent collection (AO3-4338)
Given I am logged in as "moderator" with password "password"
And I post the work "Programmed for Murder"
And I view the work "Programmed for Murder"
And I follow "Bookmark"
And I press "Create"
And I should see "Bookmark was successfully created"
Then I follow "Edit"
And I fill in "bookmark_collection_names" with "some_nonsense_collection"
And I press "Update"
And I should see "does not exist."
