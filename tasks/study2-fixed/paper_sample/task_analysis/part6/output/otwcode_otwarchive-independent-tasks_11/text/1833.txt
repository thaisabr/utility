Feature: Gift Exchange Challenge
In order to have more fics for my fandom
As a humble user
I want to run a gift exchange
Scenario: Gift exchange also appears in list of open gift exchange challenges
Given I am logged in as "mod1"
And I have created the gift exchange "My Gift Exchange"
And I am on "My Gift Exchange" gift exchange edit page
When I check "Signup open?"
And I submit
When I view open challenges
And I follow "Gift Exchange Challenges"
Then I should see "My Gift Exchange"
Feature: Prompt Meme Challenge
In order to have an archive full of works
As a humble user
I want to create a prompt meme and post to it
Scenario: Prompt meme is also in list of open prompt meme challenges
Given I have Battle 12 prompt meme fully set up
And I am logged in as a random user
When I view open challenges
And I follow "Prompt Meme Challenges"
Then I should see "Battle 12"
