Given(/^#{capture_model} exists?(?: with #{capture_fields})?$/) do |name, fields|
  create_model(name, fields)
end
Given /^I am logged in as "([^\"]*)" with password "([^\"]*)"$/ do |login, password|
  Given "I am logged out"
  user = User.find_by_login(login)
  if user.blank?
    user = Factory.create(:user, {:login => login, :password => password})
    user.activate
  else
    user.password = password
    user.password_confirmation = password
    user.save
  end
  visit login_path
  fill_in "User name", :with => login
  fill_in "Password", :with => password
  check "Remember me"
  click_button "Log in"
  assert UserSession.find
end
When /^I view the work "([^\"]*)"$/ do |work|
  work = Work.find_by_title!(work)
  visit work_url(work)
end
When /^I post the work "([^\"]*)" with fandom "([^\"]*)"$/ do |title, fandom|
  work = Work.find_by_title(title)
  if work.blank?
    Given "the draft \"#{title}\" with fandom \"#{fandom}\""
    work = Work.find_by_title(title)
  end
  visit preview_work_url(work)
  click_button("Post")
  Then "I should see \"Work was successfully posted.\""
end
When /^I post the work "([^\"]*)" with fandom "([^\"]*)" with freeform "([^\"]*)"$/ do |title, fandom, freeform|
  work = Work.find_by_title(title)
  if work.blank?
    Given "the draft \"#{title}\" with fandom \"#{fandom}\" with freeform \"#{freeform}\""
    work = Work.find_by_title(title)
  end
  visit preview_work_url(work)
  click_button("Post")
  Then "I should see \"Work was successfully posted.\""
end
When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    click_button(button)
  end
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
When /^(?:|I )fill in "([^"]*)" with "([^"]*)"(?: within "([^"]*)")?$/ do |field, value, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
When /^(?:|I )check "([^"]*)"(?: within "([^"]*)")?$/ do |field, selector|
  with_scope(selector) do
    check(field)
  end
end
When /^(?:|I )uncheck "([^"]*)"(?: within "([^"]*)")?$/ do |field, selector|
  with_scope(selector) do
    uncheck(field)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_content(text)
    else
      assert page.has_content?(text)
    end
  end
end
Then /^(?:|I )should not see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_no_content(text)
    else
      assert page.has_no_content?(text)
    end
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)
    case page_name

    when /the home\s?page/
      '/'

    # the following are examples using path_to_pickle

    when /^#{capture_model}(?:'s)? page$/                           # eg. the forum's page
      path_to_pickle $1

    when /^#{capture_model}(?:'s)? #{capture_model}(?:'s)? page$/   # eg. the forum's post's page
      path_to_pickle $1, $2

    when /^#{capture_model}(?:'s)? #{capture_model}'s (.+?) page$/  # eg. the forum's post's comments page
      path_to_pickle $1, $2, :extra => $3                           #  or the forum's post's edit page

    when /^#{capture_model}(?:'s)? (.+?) page$/                     # eg. the forum's posts page
      path_to_pickle $1, :extra => $2                               #  or the forum's edit page

    # Add more mappings here.

    when /^the login page$/i
      new_user_session_path
    when /^(.*)'s user page$/i
      user_path(:id => $1)
    when /^(.*)'s user url$/i
      user_url(:id => $1).sub("http://www.example.com", ArchiveConfig.APP_URL)
    when /^(.*)'s works page$/i
      user_works_path(:user_id => $1)
    when /^the "(.*)" work page/
      work_path(Work.find_by_title($1))
    when /^the work page with title (.*)/
      work_path(Work.find_by_title($1))
    when /^(.*)'s bookmarks page$/i
      user_bookmarks_path(:user_id => $1)
    when /^(.*)'s pseuds page$/i
      user_pseuds_path(:user_id => $1)
    when /^(.*)'s reading page$/i
      user_readings_path(:user_id => $1)
    when /^(.*)'s series page$/i
      user_series_index_path(:user_id => $1)
    when /^(.*)'s preferences page$/i
      user_preferences_path(:user_id => $1)
    when /^the subscriptions page for "(.*)"$/i
      user_subscriptions_path(:user_id => $1)
    when /my user page/
      user_path(User.current_user)
    when /my preferences page/
      user_preferences_path(User.current_user)
    when /my bookmarks page/
      user_bookmarks_path(User.current_user)
    when /my subscriptions page/
      user_subscriptions_path(User.current_user)      
    when /the import page/
      new_work_path(:import => 'true')
    when /the work-skins page/
      skins_path(:work_skins => true)
    when /^(.*)'s skin page/
      skins_path(:user_id => $1)
    when /^"(.*)" skin page/
      skin_path(Skin.find_by_title($1))
    when /^"(.*)" edit skin page/
      edit_skin_path(Skin.find_by_title($1))
    when /^"(.*)" collection's page$/i                      # e.g. when I go to "Collection name" collection's page
      collection_path(Collection.find_by_title($1))
    when /^"(.*)" collection's url$/i                      # e.g. when I go to "Collection name" collection's url
      collection_url(Collection.find_by_title($1)).sub("http://www.example.com", ArchiveConfig.APP_URL)
    when /^"(.*)" gift exchange edit page$/i
      edit_collection_gift_exchange_path(Collection.find_by_title($1))
    when /^"(.*)" collection's static page$/i
      static_collection_path(Collection.find_by_title($1))
    when /^the works tagged "(.*)"$/i
      tag_works_path(Tag.find_by_name($1))
    when /^the url for works tagged "(.*)"$/i
      tag_works_url(Tag.find_by_name($1)).sub("http://www.example.com", ArchiveConfig.APP_URL)
    when /^the works tagged "(.*)" in collection "(.*)"$/i
      collection_tag_works_path(Collection.find_by_title($2), Tag.find_by_name($1))
    when /^the url for works tagged "(.*)" in collection "(.*)"$/i
      collection_tag_works_url(Collection.find_by_title($2), Tag.find_by_name($1)).sub("http://www.example.com", ArchiveConfig.APP_URL)
    when /^the admin-posts page$/i
      admin_posts_path
    when /^the FAQ reorder page$/i
      manage_archive_faqs_path
      
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
When /^"([^\"]*)" is fixed$/ do |what|
  puts "\nDEFERRED (#{what})"
end
