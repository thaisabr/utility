Given /^I view the chaptered work(?: with ([\d]+) comments?)? "([^"]*)"(?: in (full|chapter-by-chapter) mode)?$/ do |n_comments, title, mode|
  Given %{I am logged in as a random user}
  And %{I post the chaptered work "#{title}"}
  work = Work.find_by_title!(title)
  visit work_url(work)
  n_comments ||= 0
  n_comments.to_i.times do |i|
    Given %{I post the comment "Bla bla" on the work "#{title}"}
  end
  And %{I am logged out}
  visit work_url(work)
  And %{I follow "View Entire Work"} if mode == "full"
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_content(text)
    else
      assert page.has_content?(text)
    end
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
Given /^I am logged in as a random user$/ do
  Given "I am logged out"
  name = "testuser#{User.count + 1}"
  user = Factory.create(:user, :login => name, :password => DEFAULT_PASSWORD)
  user.activate
  visit login_path
  fill_in "User name", :with => name
  fill_in "Password", :with => DEFAULT_PASSWORD
  check "Remember me"
  click_button "Log in"
  assert UserSession.find
end
Given /^I set my preferences to View Full Work mode by default$/ do
  user = User.current_user
  user.preference.view_full_works = true
  user.preference.save
end
When /^I post the comment "([^"]*)" on the work "([^"]*)"$/ do |comment_text, work|
  Given "I set up the comment \"#{comment_text}\" on the work \"#{work}\""
  click_button("Add Comment")
end
