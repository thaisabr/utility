Given /^I have created the gift exchange "([^\"]*)"$/ do |challengename|
  Given %{I have set up the gift exchange "#{challengename}"}
  When "I fill in gift exchange challenge options"
    click_button("Submit")
end
Given /^I have opened signup for the gift exchange "([^\"]*)"$/ do |challengename|
  Given %{I am on "#{challengename}" gift exchange edit page}
  check "Signup open?"
  click_button "Submit"
end  
Given /^I have Battle 12 prompt meme fully set up$/ do
  Given %{I am logged in as "mod1"}
    And "I have standard challenge tags setup"
  When "I set up Battle 12 promptmeme collection"
  When "I fill in Battle 12 challenge options"
  When %{I follow "Log out"}
end
Given /^everyone has signed up for Battle 12$/ do
  When %{I am logged in as "myname1"}
  # no anon
  When %{I sign up for Battle 12 with combination A}
  When %{I am logged in as "myname2"}
  # both anon
  When %{I sign up for Battle 12 with combination B}
  When %{I am logged in as "myname3"}
  # one anon
  When %{I sign up for Battle 12}
  When %{I am logged in as "myname4"}
  When %{I sign up for Battle 12 with combination C}
end
Given /^everyone has signed up for the gift exchange "([^\"]*)"$/ do |challengename|
  When %{I am logged in as "myname1"}
  When %{I sign up for "#{challengename}" with combination A}
  When %{I am logged in as "myname2"}
  When %{I sign up for "#{challengename}" with combination B}
  When %{I am logged in as "myname3"}
  When %{I sign up for "#{challengename}" with combination C}
  When %{I am logged in as "myname4"}
  When %{I sign up for "#{challengename}" with combination D}
end
Given /^I have generated matches for "([^\"]*)"$/ do |challengename|
  When %{I close signups for "#{challengename}"}
  When %{I follow "Matching"}
  When %{I follow "Generate Potential Matches"}
  Given %{the system processes jobs}
    And %{I wait 3 seconds}
  When %{I reload the page}
  When %{all emails have been delivered}
end
Given /^I have sent assignments for "([^\"]*)"$/ do |challengename|
  When %{I follow "Send Assignments"}
  Given %{the system processes jobs}
    And %{I wait 3 seconds}
  When %{I reload the page}
  Then %{I should not see "Assignments are now being sent out"}
end
When /^I claim a prompt from "([^\"]*)"$/ do |title|
  visit collection_path(Collection.find_by_title(title))
    And %{I follow "Prompts ("}
  Then %{I should see "Claim" within "th"}
    And %{I should not see "Sign in to claim prompts"}
  When %{I press "Claim"}
end
When /^I start to fulfill my claim$/ do
  When %{I am on my user page}
  When %{I follow "My Claims ("}
  When %{I follow "Post To Fulfill"}
    And %{I fill in "Work Title" with "Fulfilled Story"}
    And %{I select "Not Rated" from "Rating"}
    And %{I check "No Archive Warnings Apply"}
    And %{I fill in "content" with "This is an exciting story about Atlantis"}
end
Given /^I am logged in as "([^\"]*)"$/ do |login|
  Given "I am logged in as \"#{login}\" with password \"#{DEFAULT_PASSWORD}\""
end
Then(/^#{capture_model} should be #{capture_model}$/) do |a, b|
  model!(a).should == model!(b)
end
Then(/^#{capture_model} should not (?:be|have) (?:an? )?#{capture_predicate}$/) do |name, predicate|
  if model!(name).respond_to?("has_#{predicate.gsub(' ', '_')}")
    model!(name).should_not send("have_#{predicate.gsub(' ', '_')}")
  else
    model!(name).should_not send("be_#{predicate.gsub(' ', '_')}")
  end
end
Then /^the "([^"]*)" checkbox(?: within "([^"]*)")? should be disabled$/ do |label, selector|
  with_scope(selector) do
    field_disabled = find_field(label)['disabled']
    if field_disabled.respond_to? :should
      field_disabled.should be_true
    else
      assert field_disabled
    end
  end
end
Then /^the "([^"]*)" checkbox(?: within "([^"]*)")? should not be disabled$/ do |label, selector|
  with_scope(selector) do
    field_disabled = find_field(label)['disabled']
    if field_disabled.respond_to? :should
      field_disabled.should be_false
    else
      assert !field_disabled
    end
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
