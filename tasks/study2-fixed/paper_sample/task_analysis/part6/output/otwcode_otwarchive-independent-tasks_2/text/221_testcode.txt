Given /^I have no users$/ do
  User.delete_all
end
Given /^I am logged in as "([^\"]*)"$/ do |login|
  step(%{I am logged in as "#{login}" with password "#{DEFAULT_PASSWORD}"})
end
Given /^I have an AdminSetting$/ do
  unless AdminSetting.first
    settings = AdminSetting.new(default_settings)
    settings.save(validate: false)
  end
end
Given /the following admins? exists?/ do |table|
  table.hashes.each do |hash|
    admin = FactoryGirl.create(:admin, hash)
  end
end
Given /^I am logged in as an admin$/ do
  step("I am logged out")
  admin = Admin.find_by_login("testadmin")
  if admin.blank?
    admin = FactoryGirl.create(:admin, login: "testadmin", password: "testadmin", email: "testadmin@example.org")
  end
  visit admin_login_path
  fill_in "Admin user name", with: "testadmin"
  fill_in "Admin password", with: "testadmin"
  click_button "Log in as admin"
  step("I should see \"Successfully logged in\"")
end
Given /^basic languages$/ do
  Language.default
  german = Language.find_or_create_by_short_and_name("DE", "Deutsch")
  de = Locale.new
  de.iso = 'de'
  de.name = 'Deutsch'
  de.language_id = german.id
  de.save!
end
Given /^I have posted an admin post with tags$/ do
  step("I am logged in as an admin")
  visit new_admin_post_path
  fill_in("admin_post_title", with: "Default Admin Post")
  fill_in("content", with: "Content of the admin post.")
  fill_in("admin_post_tag_list", with: "quotes, futurama")
  click_button("Post")
end
When /^I make an admin post$/ do
  visit new_admin_post_path
  fill_in("admin_post_title", with: "Default Admin Post")
  fill_in("content", with: "Content of the admin post.")
  click_button("Post")
end
When /^I make a(?: (\d+)(?:st|nd|rd|th)?)? FAQ post$/ do |n|
  n ||= 1
  visit new_archive_faq_path
  fill_in("Question*", with: "Number #{n} Question.")
  fill_in("Answer*", with: "Number #{n} posted FAQ, this is.")
  fill_in("Category name*", with: "Number #{n} FAQ")
  fill_in("Anchor name*", with: "Number#{n}anchor")
  click_button("Post")
end
When /^I make a(?: (\d+)(?:st|nd|rd|th)?)? Admin Post$/ do |n|
  n ||= 1
  visit new_admin_post_path
  fill_in("admin_post_title", with: "Amazing News #{n}")
  fill_in("content", with: "This is the content for the #{n} Admin Post")
  click_button("Post")
end
When (/^I make a translation of an admin post$/) do
  visit new_admin_post_path
  fill_in("admin_post_title", with: "Deutsch Ankuendigung")
  fill_in("content", with: "Deutsch Woerter")
  step(%{I select "Deutsch" from "Choose a language"})
  fill_in("admin_post_translated_post_id", with: AdminPost.find_by_title("Default Admin Post").id)
  click_button("Post")
end
Then (/^I should see a translated admin post$/) do
  step(%{I go to the admin-posts page})
  step(%{I should see "Default Admin Post"})
  step(%{I should see "Translations: Deutsch"})
  step(%{I follow "Default Admin Post"})
  step(%{I should see "Deutsch" within "dd.translations"})
  step(%{I follow "Deutsch"})
  step(%{I should see "Deutsch Woerter"})
end
Then (/^I should see a translated admin post with tags$/) do
  step(%{I go to the admin-posts page})
  step(%{I should see "Default Admin Post"})
  step(%{I should see "Tags: quotes futurama"})
  step(%{I should see "Translations: Deutsch"})
  step(%{I follow "Default Admin Post"})
  step(%{I should see "Deutsch" within "dd.translations"})
  step(%{I should see "futurama" within "dd.tags"})
end
Then (/^I should not see a translated admin post$/) do
  step(%{I go to the admin-posts page})
  step(%{I should see "Default Admin Post"})
  step(%{I should see "Deutsch Ankuendigung"})
  step(%{I follow "Default Admin Post"})
  step(%{I should not see "Translations: Deutsch"})
end
When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    click_button(button)
  end
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
When /^(?:|I )fill in "([^"]*)" with "([^"]*)"(?: within "([^"]*)")?$/ do |field, value, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    page.should have_content(text)
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)
    case page_name

    when /the home\s?page/
      '/'
    when /^the search bookmarks page$/i
      Bookmark.tire.index.refresh
      search_bookmarks_path
    when /^the search tags page$/i
      Tag.tire.index.refresh
      search_tags_path
    when /^the search works page$/i
      Work.tire.index.refresh
      search_works_path      
    when /^the search people page$/i
      Pseud.tire.index.refresh
      search_people_path
    when /^the bookmarks page$/i
      bookmarks_path

    # the following are examples using path_to_pickle

    when /^#{capture_model}(?:'s)? page$/                           # eg. the forum's page
      path_to_pickle $1

    when /^#{capture_model}(?:'s)? #{capture_model}(?:'s)? page$/   # eg. the forum's post's page
      path_to_pickle $1, $2

    when /^#{capture_model}(?:'s)? #{capture_model}'s (.+?) page$/  # eg. the forum's post's comments page
      path_to_pickle $1, $2, :extra => $3                           #  or the forum's post's edit page

    when /^#{capture_model}(?:'s)? (.+?) page$/                     # eg. the forum's posts page
      path_to_pickle $1, :extra => $2                               #  or the forum's edit page

    # Add more mappings here.

    when /^the tagsets page$/i
      tag_sets_path
    when /^the login page$/i
      new_user_session_path
    when /^account creation page$/i
      new_user_path
    when /^invite requests page$/i
      invite_requests_path
    when /my pseuds page/
      user_pseuds_path(User.current_user)
    when /my user page/
      user_path(User.current_user)
    when /my preferences page/
      user_preferences_path(User.current_user)
    when /my bookmarks page/
      Bookmark.tire.index.refresh
      user_bookmarks_path(User.current_user)
    when /my works page/
      Work.tire.index.refresh
      user_works_path(User.current_user)
    when /my edit multiple works page/
      show_multiple_user_works_path(User.current_user)
    when /my subscriptions page/
      user_subscriptions_path(User.current_user)   
    when /my stats page/
      user_stats_path(User.current_user)   
    when /my profile page/
      user_profile_path(User.current_user)
    when /my claims page/
      user_claims_path(User.current_user)
    when /my signups page/
      user_signups_path(User.current_user)
    when /my related works page/
      user_related_works_path(User.current_user)
    when /my inbox page/
      user_inbox_path(User.current_user)
    when /my invitations page/
      user_invitations_path(User.current_user)
    when /my gifts page/
      user_gifts_path(User.current_user)
    when /^(.*)'s gifts page/
      user_gifts_path(user_id: $1)
    when /the import page/
      new_work_path(:import => 'true')
    when /the work-skins page/
      skins_path(:skin_type => "WorkSkin")
    when /^(.*?)(?:'s)? user page$/i
      user_path(id: $1)
    when /^(.*?)(?:'s)? user url$/i
      user_url(id: $1).sub("http://www.example.com", "http://#{ArchiveConfig.APP_HOST}")
    when /^(.*?)(?:'s)? works page$/i
      Work.tire.index.refresh
      user_works_path(user_id: $1)
    when /^the "(.*)" work page/
      work_path(Work.find_by_title($1))
    when /^the work page with title (.*)/
      work_path(Work.find_by_title($1))
    when /^(.*?)(?:'s)? bookmarks page$/i
      Bookmark.tire.index.refresh
      user_bookmarks_path(user_id: $1)
    when /^(.*?)(?:'s)? pseuds page$/i
      user_pseuds_path(user_id: $1)
    when /^(.*?)(?:'s)? invitations page$/i
      user_invitations_path(user_id: $1)
    when /^(.*?)(?:'s)? reading page$/i
      user_readings_path(user_id: $1)
    when /^(.*?)(?:'s)? series page$/i
      user_series_index_path(user_id: $1)
    when /^(.*?)(?:'s)? stats page$/i
      user_stats_path(user_id: $1)
    when /^(.*?)(?:'s)? preferences page$/i
      user_preferences_path(user_id: $1)
    when /^(.*?)(?:'s)? related works page$/i
      user_related_works_path(user_id: $1)
    when /^the subscriptions page for "(.*)"$/i
      user_subscriptions_path(user_id: $1)
    when /^(.*?)(?:'s)? profile page$/i
      user_profile_path(user_id: $1)
    when /^(.*)'s skins page/
      skins_path(user_id: $1)
    when /^"(.*)" skin page/
      skin_path(Skin.find_by_title($1))
    when /^the new wizard skin page/
      new_skin_path(wizard: true)
    when /^"(.*)" edit skin page/
      edit_skin_path(Skin.find_by_title($1))
    when /^"(.*)" edit wizard skin page/
      edit_skin_path(Skin.find_by_title($1), wizard: true)
    when /^"(.*)" collection's page$/i                         # e.g. when I go to "Collection name" collection's page
      collection_path(Collection.find_by_title($1))
    when /^the "(.*)" signups page$/i                          # e.g. when I go to the "Collection name" signup page
      collection_signups_path(Collection.find_by_title($1))
    when /^the "(.*)" requests page$/i                         # e.g. when I go to the "Collection name" signup page
      collection_requests_path(Collection.find_by_title($1))
    when /^the "(.*)" assignments page$/i                      # e.g. when I go to the "Collection name" assignments page
      collection_assignments_path(Collection.find_by_title($1))
    when /^"(.*)" collection's url$/i                          # e.g. when I go to "Collection name" collection's url
      collection_url(Collection.find_by_title($1)).sub("http://www.example.com", "http://#{ArchiveConfig.APP_HOST}")
    when /^"(.*)" gift exchange edit page$/i
      edit_collection_gift_exchange_path(Collection.find_by_title($1))
    when /^"(.*)" gift exchange matching page$/i
      collection_potential_matches_path(Collection.find_by_title($1))
    when /^the works tagged "(.*)"$/i
      Work.tire.index.refresh
      tag_works_path(Tag.find_by_name($1))
    when /^the bookmarks tagged "(.*)"$/i
      Bookmark.tire.index.refresh
      tag_bookmarks_path(Tag.find_by_name($1))
    when /^the url for works tagged "(.*)"$/i
      Work.tire.index.refresh
      tag_works_url(Tag.find_by_name($1)).sub("http://www.example.com", "http://#{ArchiveConfig.APP_HOST}")
    when /^the bookmarks in collection "(.*)"$/i
      Bookmark.tire.index.refresh
      collection_bookmarks_path(Collection.find_by_title($1))
    when /^the works tagged "(.*)" in collection "(.*)"$/i
      Work.tire.index.refresh
      collection_tag_works_path(Collection.find_by_title($2), Tag.find_by_name($1))
    when /^the url for works tagged "(.*)" in collection "(.*)"$/i
      Work.tire.index.refresh
      collection_tag_works_url(Collection.find_by_title($2), Tag.find_by_name($1)).sub("http://www.example.com", "http://#{ArchiveConfig.APP_HOST}")
    when /^the tag comments? page for "(.*)"$/i
      tag_comments_path(Tag.find_by_name($1))
    when /^the admin-posts page$/i
      admin_posts_path
    when /^the admin-settings page$/i
      admin_settings_path      
    when /^the admin-notices page$/i
      notify_admin_users_path
    when /^the FAQ reorder page$/i
      manage_archive_faqs_path
    when /^the Wrangling Guidelines reorder page$/i
      manage_wrangling_guidelines_path
    when /^the tos page$/i
      tos_path
    when /^the faq page$/i
      archive_faqs_path
    when /^the wrangling guidelines page$/i
      wrangling_guidelines_path
    when /^the support page$/i
      new_feedback_report_path
    when /^the new tag ?set page$/i
      new_tag_set_path
    when /^the "(.*)" tag ?set edit page$/i
      edit_tag_set_path(OwnedTagSet.find_by_title($1))    
    when /^the "(.*)" tag ?set page$/i
      tag_set_path(OwnedTagSet.find_by_title($1))
    when /^the manage users page$/
      admin_users_path
    when /^the abuse administration page for "(.*)"$/i
      admin_user_path(User.find_by_login($1))
    when /^the Open Doors tools page$/i
      opendoors_tools_path
      
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Given /^I am logged in as "([^\"]*)" with password "([^\"]*)"$/ do |login, password|
  step("I am logged out")
  user = User.find_by_login(login)
  if user.blank?
    user = FactoryGirl.create(:user, {:login => login, :password => password})
    user.activate
  else
    user.password = password
    user.password_confirmation = password
    user.save
  end
  visit login_path
  fill_in "User name", :with => login
  fill_in "Password", :with => password
  check "Remember Me"
  click_button "Log In"
  assert UserSession.find
end
Given /^I am logged out$/ do
  visit logout_path
  assert !UserSession.find
  visit admin_logout_path
  assert !AdminSession.find
end
When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
When /^(?:|I )select "([^"]*)" from "([^"]*)"(?: within "([^"]*)")?$/ do |value, field, selector|
  with_scope(selector) do
    select(value, :from => field)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    page.should have_content(text)
  end
end
Then /^(?:|I )should not see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_no_content(text)
    else
      assert page.has_no_content?(text)
    end
  end
end
