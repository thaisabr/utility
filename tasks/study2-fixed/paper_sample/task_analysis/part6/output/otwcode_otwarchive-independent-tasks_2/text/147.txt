Feature: Admin Actions for Works and Bookmarks
As an admin
I should be able to perform special actions on works
Scenario: Admin can edit language on works when posting without previewing
Given basic tags
And basic languages
And I am logged in as "regular_user"
And I post the work "Wrong Language"
When I am logged in as an admin
And I view the work "Wrong Language"
And I follow "Edit Tags and Language"
Then I should see "Edit Work Tags and Language for "
When I select "Deutsch" from "Choose a language"
And I press "Post Without Preview"
Then I should see "Deutsch"
And I should not see "English"
Scenario: Admin can edit language on works when previewing first
Given basic tags
And basic languages
And I am logged in as "regular_user"
And I post the work "Wrong Language"
When I am logged in as an admin
And I view the work "Wrong Language"
And I follow "Edit Tags and Language"
When I select "Deutsch" from "Choose a language"
And I press "Preview"
Then I should see "Preview Tags and Language"
When I press "Update"
Then I should see "Deutsch"
And I should not see "English"
