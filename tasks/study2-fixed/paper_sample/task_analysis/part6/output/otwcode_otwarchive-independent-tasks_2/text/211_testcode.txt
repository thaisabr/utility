Given /^I am logged in as an admin$/ do
  step("I am logged out")
  admin = Admin.find_by_login("testadmin")
  if admin.blank?
    admin = FactoryGirl.create(:admin, login: "testadmin", password: "testadmin", email: "testadmin@example.org")
  end
  visit admin_login_path
  fill_in "Admin user name", with: "testadmin"
  fill_in "Admin password", with: "testadmin"
  click_button "Log in as admin"
  step("I should see \"Successfully logged in\"")
end
Given(/^I have blacklisted the address "([^"]*)"$/) do |email|
  visit admin_blacklisted_emails_url
  fill_in("Email", with: email)
  click_button("Add To Blacklist")
end
Given(/^I have blacklisted the address for user "([^"]*)"$/) do |user|
  visit admin_blacklisted_emails_url
  u = User.find_by_login(user)
  fill_in("admin_blacklisted_email_email", with: u.email)
  click_button("Add To Blacklist")
end
Then(/^the address "([^"]*)" should be in the blacklist$/) do |email|
  visit admin_blacklisted_emails_url
  fill_in("Email to find", with: email)
  click_button("Search Blacklist")
  assert page.should have_content(email)
end
Then(/^the address "([^"]*)" should not be in the blacklist$/) do |email|
  visit admin_blacklisted_emails_url
  fill_in("Email to find", with: email)
  click_button("Search Blacklist")
  step %{I should see "0 emails found"}
end
Then(/^I should not be able to comment with the address "([^"]*)"$/) do |email|
  step %{the work "New Work"}
  step %{I post the comment "I loved this" on the work "New Work" as a guest with email "#{email}"}
  step %{I should see "has been blocked at the owner's request"}
  step %{I should not see "Comment created!"}
end
Then(/^I should be able to comment with the address "([^"]*)"$/) do |email|
  step %{the work "New Work"}
  step %{I post the comment "I loved this" on the work "New Work" as a guest with email "#{email}"}
  step %{I should not see "has been blocked at the owner's request"}
  step %{I should see "Comment created!"}
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    click_button(button)
  end
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
When /^(?:|I )fill in "([^"]*)" with "([^"]*)"(?: within "([^"]*)")?$/ do |field, value, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    page.should have_content(text)
  end
end
Then /^(?:|I )should not see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_no_content(text)
    else
      assert page.has_no_content?(text)
    end
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
Given /^the user "([^\"]*)" exists and is activated$/ do |login|
  user = User.find_by_login(login)
  if user.blank?
    user = FactoryGirl.create(:user, {:login => login, :password => "#{DEFAULT_PASSWORD}"})
    user.activate
  end
end
Given /^I am logged in as "([^\"]*)"$/ do |login|
  step(%{I am logged in as "#{login}" with password "#{DEFAULT_PASSWORD}"})
end
Given /^I am logged out$/ do
  visit logout_path
  assert !UserSession.find
  visit admin_logout_path
  assert !AdminSession.find
end
When /^I post (?:a|the) work "([^\"]*)"(?: with fandom "([^\"]*)")?(?: with freeform "([^\"]*)")?(?: with category "([^\"]*)")?(?: (?:in|to) (?:the )?collection "([^\"]*)")?(?: as a gift (?:for|to) "([^\"]*)")?$/ do |title, fandom, freeform, category, collection, recipient|  
  # If the work is already a draft then visit the preview page and post it
  work = Work.find_by_title(title)
  if work
    visit preview_work_url(work)
    click_button("Post")
  else
    # Note: this will match the above regexp and work just fine even if all the options are blank!
    step %{I set up the draft "#{title}" with fandom "#{fandom}" with freeform "#{freeform}" with category "#{category}" in collection "#{collection}" as a gift to "#{recipient}"}
    click_button("Post Without Preview")
  end
  Work.tire.index.refresh
end
Given /^the work "([^\"]*)"$/ do |work|
  unless Work.where(title: work).exists?
    step %{I have a work "#{work}"}
    step %{I am logged out}
  end
end
When /^I set up the comment "([^"]*)" on the work "([^"]*)"$/ do |comment_text, work|
  work = Work.find_by_title!(work)
  visit work_url(work)
  fill_in("comment[content]", with: comment_text)
end
When /^I post the comment "([^"]*)" on the work "([^"]*)" as a guest(?: with email "([^"]*)")?$/ do |comment_text, work, email|
  step "I am logged out"
  step "I set up the comment \"#{comment_text}\" on the work \"#{work}\""
  fill_in("Name", with: "guest")
  fill_in("Email", with: (email || "guest@foo.com"))
  click_button "Comment"
end
When /^I edit a comment$/ do
  step %{I follow "Edit"}
  fill_in("comment[content]", with: "Edited comment")
  click_button "Update"
end
When /^I post a comment "([^"]*)"$/ do |comment_text|
  fill_in("comment[content]", with: comment_text)
  click_button("Comment")
end
When /^I reply to a comment with "([^"]*)"$/ do |comment_text|
  step %{I follow "Reply"}
  step %{I should see the reply to comment form}
  with_scope(".odd") do
    fill_in("comment[content]", with: comment_text)
    click_button("Comment")
  end
end
When /^I compose an invalid comment(?: within "([^"]*)")?$/ do |selector|
  with_scope(selector) do
    fill_in("Comment", with: %/Sed mollis sapien ac massa pulvinar facilisis. Nulla rhoncus neque nisi. Integer sit amet nulla vel orci hendrerit aliquam. Proin vehicula bibendum vulputate. Nullam porttitor, arcu eu mollis accumsan, turpis justo ornare tellus, ac congue lectus purus ut risus. Phasellus feugiat, orci id tempor elementum, sapien nulla dignissim sapien, dictum eleifend nisl erat vitae urna. Cras imperdiet bibendum porttitor. Suspendisse vitae tellus nibh, vel facilisis magna. Quisque nec massa augue. Pellentesque in ipsum lacus. Aenean mauris leo, viverra sit amet fringilla sit amet, volutpat eu risus. Etiam scelerisque, nibh a condimentum eleifend, augue ipsum blandit tortor, lacinia pharetra ante felis eget lorem. Proin tristique dictum placerat. Aenean commodo imperdiet massa et auctor. Phasellus eleifend posuere varius.
Sed bibendum nisl vel ligula rhoncus at laoreet lorem lacinia. Vivamus est est, euismod vel pretium in, aliquam ac turpis. Integer ac leo sem, vel egestas lacus. Duis id nibh magna, vel adipiscing erat. Aliquam arcu velit, laoreet eget laoreet eget, semper id augue. Nullam volutpat pretium turpis vitae molestie. Ut id nisi eget nibh blandit blandit malesuada et sem. Fusce at accumsan erat. Sed sed adipiscing tortor. Proin vitae eros eget neque dignissim ullamcorper. Vestibulum eleifend nisl sed erat molestie suscipit. Fusce rutrum dignissim diam vel ultricies. Proin nec consequat velit. Aliquam eu nulla urna. Morbi ac orci nisl.
Vivamus vitae felis erat, a hendrerit nisi. Nullam et nunc sed est laoreet tempus non at nibh. Pellentesque tincidunt, diam eu vestibulum pretium, diam metus volutpat risus, ut mollis augue dolor quis ligula. Fusce in placerat leo. Nullam quis orci dui. Donec ultrices quam ut metus blandit cursus. Quisque lobortis elit sit amet libero mollis quis egestas ipsum faucibus. Curabitur sit amet sollicitudin metus. Vivamus sit amet justo eget felis dictum scelerisque in eu mauris. Vestibulum in diam ligula, et convallis ante. Praesent risus magna, adipiscing in vehicula eu, interdum eu arcu. Duis in nisl libero, nec posuere massa. Vestibulum pretium fermentum dui et dignissim. Mauris at diam sed purus faucibus tristique. Maecenas non orci et augue dignissim tempor. Sed vestibulum condimentum faucibus.
Morbi nec ullamcorper dolor. In luctus vulputate arcu et egestas. Nullam at pretium enim. Nulla congue tincidunt dignissim. Fusce malesuada odio nec turpis sagittis et accumsan tellus iaculis. Mauris eu libero non diam pretium feugiat quis in mauris. Vestibulum ut facilisis massa. Cras est metus, pulvinar eget ullamcorper in, eleifend id est. Ut ac bibendum elit. Vestibulum quis eros sem. Duis elementum congue lorem, nec semper justo adipiscing vitae. Nam eget velit est, nec varius leo. Quisque aliquet aliquet elit, eu elementum enim lacinia aliquam. Suspendisse laoreet convallis interdum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In quis velit massa. Nullam lectus risus, condimentum ac fringilla eu, pretium sed metus.
Nunc eget dolor ut nisi laoreet scelerisque. Vestibulum condimentum dignissim leo ut luctus. Aliquam sed sem velit. Nulla justo nulla, molestie cursus mollis eget, ullamcorper aliquet mi. Duis et sem elit, quis pretium diam. Nam consectetur ullamcorper velit, varius vulputate dui ultrices sodales. Sed aliquet laoreet tortor, vitae varius enim ornare vel. Nam ornare dapibus aliquam. Proin faucibus tellus eget nibh lacinia in dignissim odio ultricies. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla aliquet pulvinar turpis vitae malesuada. Mauris porttitor erat in urna bibendum luctus. Vestibulum nec mi eros, nec rutrum ligula. Nunc ac nisl eros, ut adipiscing diam. Integer feugiat justo a purus fermentum sollicitudin. Mauris lacinia venenatis commodo. Nam urna libero, viverra in rhoncus vel, ultricies vitae augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Morbi vitae lacus vitae magna volutpat pharetra rhoncus eget nisi. Proin vehicula, felis nec tempor eleifend, dolor ipsum volutpat dolor, et eleifend nibh libero ac turpis. Donec odio est, sodales nec consectetur vehicula, adipiscing sit amet magna. Suspendisse dapibus tincidunt velit sit amet mollis. Curabitur eget blandit li./)
  end
end
Given /^I am logged in as "([^\"]*)" with password "([^\"]*)"$/ do |login, password|
  step("I am logged out")
  user = User.find_by_login(login)
  if user.blank?
    user = FactoryGirl.create(:user, {:login => login, :password => password})
    user.activate
  else
    user.password = password
    user.password_confirmation = password
    user.save
  end
  visit login_path
  fill_in "User name", :with => login
  fill_in "Password", :with => password
  check "Remember Me"
  click_button "Log In"
  assert UserSession.find
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    page.should have_content(text)
  end
end
Then /^(?:|I )should not see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_no_content(text)
    else
      assert page.has_no_content?(text)
    end
  end
end
When /^I set up (?:a|the) draft "([^\"]*)"(?: with fandom "([^\"]*)")?(?: with freeform "([^\"]*)")?(?: with category "([^\"]*)")?(?: (?:in|to|with) (?:the )?collection "([^\"]*)")?(?: as a gift (?:for|to) "([^\"]*)")?$/ do |title, fandom, freeform, category, collection, recipient|
  step %{basic tags}
  visit new_work_path
  step %{I fill in the basic work information for "#{title}"}
  check(category.blank? ? DEFAULT_CATEGORY : category)
  fill_in("Fandoms", with: (fandom.blank? ? DEFAULT_FANDOM : fandom))
  fill_in("Additional Tags", with: (freeform.blank? ? DEFAULT_FREEFORM : freeform))
  unless collection.blank?
    c = Collection.find_by_title(collection)
    fill_in("Collections", with: c.name)
  end
  fill_in("work_recipients", with: "#{recipient}") unless recipient.blank?
end
Given /^I have a work "([^\"]*)"$/ do |work|
  step %{I am logged in as a random user}
  step %{I post the work "#{work}"}
end
Given /^the work "([^\"]*)"$/ do |work|
  unless Work.where(title: work).exists?
    step %{I have a work "#{work}"}
    step %{I am logged out}
  end
end
Then /^I should see the reply to comment form$/ do
  step %{I should see "Comment as" within ".odd"}
end
When /^I post the comment "([^"]*)" on the work "([^"]*)" as a guest(?: with email "([^"]*)")?$/ do |comment_text, work, email|
  step "I am logged out"
  step "I set up the comment \"#{comment_text}\" on the work \"#{work}\""
  fill_in("Name", with: "guest")
  fill_in("Email", with: (email || "guest@foo.com"))
  click_button "Comment"
end
Given /^I am logged in as a random user$/ do
  step("I am logged out")
  name = "testuser#{User.count + 1}"
  user = FactoryGirl.create(:user, :login => name, :password => DEFAULT_PASSWORD)
  user.activate
  visit login_path
  fill_in "User name", :with => name
  fill_in "Password", :with => DEFAULT_PASSWORD
  check "Remember me"
  click_button "Log In"
  assert UserSession.find
end
Given /^I am logged out$/ do
  visit logout_path
  assert !UserSession.find
  visit admin_logout_path
  assert !AdminSession.find
end
Given /^basic tags$/ do
  ratings = [ArchiveConfig.RATING_DEFAULT_TAG_NAME,
             ArchiveConfig.RATING_GENERAL_TAG_NAME,
             ArchiveConfig.RATING_TEEN_TAG_NAME,
             ArchiveConfig.RATING_MATURE_TAG_NAME,
             ArchiveConfig.RATING_EXPLICIT_TAG_NAME]
  ratings.each do |rating|
    Rating.find_or_create_by_name_and_canonical(rating, true)
  end
  Warning.find_or_create_by_name_and_canonical("No Archive Warnings Apply", true)
  Warning.find_or_create_by_name_and_canonical("Choose Not To Use Archive Warnings", true)
  Fandom.find_or_create_by_name_and_canonical("No Fandom", true)
  Category.find_or_create_by_name_and_canonical("Other", true)
  Category.find_or_create_by_name_and_canonical("F/F", true)
  Category.find_or_create_by_name_and_canonical("Multi", true)
  Category.find_or_create_by_name_and_canonical("M/F", true)
  Category.find_or_create_by_name_and_canonical("M/M", true)
end
When /^I fill in the basic work information for "([^\"]*)"$/ do |title|
  step %{I fill in basic work tags}
  check(DEFAULT_WARNING)
  fill_in("Work Title", with: title)
  fill_in("content", with: DEFAULT_CONTENT)
end
When /^I post (?:a|the) work "([^\"]*)"(?: with fandom "([^\"]*)")?(?: with freeform "([^\"]*)")?(?: with category "([^\"]*)")?(?: (?:in|to) (?:the )?collection "([^\"]*)")?(?: as a gift (?:for|to) "([^\"]*)")?$/ do |title, fandom, freeform, category, collection, recipient|  
  # If the work is already a draft then visit the preview page and post it
  work = Work.find_by_title(title)
  if work
    visit preview_work_url(work)
    click_button("Post")
  else
    # Note: this will match the above regexp and work just fine even if all the options are blank!
    step %{I set up the draft "#{title}" with fandom "#{fandom}" with freeform "#{freeform}" with category "#{category}" in collection "#{collection}" as a gift to "#{recipient}"}
    click_button("Post Without Preview")
  end
  Work.tire.index.refresh
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    page.should have_content(text)
  end
end
When /^I set up the comment "([^"]*)" on the work "([^"]*)"$/ do |comment_text, work|
  work = Work.find_by_title!(work)
  visit work_url(work)
  fill_in("comment[content]", with: comment_text)
end
When /^I set up (?:a|the) draft "([^\"]*)"(?: with fandom "([^\"]*)")?(?: with freeform "([^\"]*)")?(?: with category "([^\"]*)")?(?: (?:in|to|with) (?:the )?collection "([^\"]*)")?(?: as a gift (?:for|to) "([^\"]*)")?$/ do |title, fandom, freeform, category, collection, recipient|
  step %{basic tags}
  visit new_work_path
  step %{I fill in the basic work information for "#{title}"}
  check(category.blank? ? DEFAULT_CATEGORY : category)
  fill_in("Fandoms", with: (fandom.blank? ? DEFAULT_FANDOM : fandom))
  fill_in("Additional Tags", with: (freeform.blank? ? DEFAULT_FREEFORM : freeform))
  unless collection.blank?
    c = Collection.find_by_title(collection)
    fill_in("Collections", with: c.name)
  end
  fill_in("work_recipients", with: "#{recipient}") unless recipient.blank?
end
When /^I fill in basic work tags$/ do
  select(DEFAULT_RATING, :from => "Rating")
  fill_in("Fandoms", :with => DEFAULT_FANDOM)
  fill_in("Additional Tags", :with => DEFAULT_FREEFORM)
end
When /^I fill in the basic work information for "([^\"]*)"$/ do |title|
  step %{I fill in basic work tags}
  check(DEFAULT_WARNING)
  fill_in("Work Title", with: title)
  fill_in("content", with: DEFAULT_CONTENT)
end
When /^I fill in basic work tags$/ do
  select(DEFAULT_RATING, :from => "Rating")
  fill_in("Fandoms", :with => DEFAULT_FANDOM)
  fill_in("Additional Tags", :with => DEFAULT_FREEFORM)
end
