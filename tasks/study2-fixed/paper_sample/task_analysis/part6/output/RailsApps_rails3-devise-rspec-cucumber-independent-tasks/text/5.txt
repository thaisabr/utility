Feature: Sign in
In order to get access to protected sections of the site
A user
Should be able to sign in
Scenario: User is not signed up
Given I do not exist as a user
When I sign in with valid credentials
Then I see an invalid login message
And I should be signed out
Scenario: User enters wrong password
Given I exist as a user
And I am not logged in
When I sign in with a wrong password
Then I see an invalid login message
And I should be signed out
Scenario: User signs in successfully with email
Given I exist as a user
And I am not logged in
When I sign in with valid credentials
Then I see a successful sign in message
When I return to the site
Then I should be signed in
Feature: Sign out
To protect my account from unauthorized access
A signed in user
Should be able to sign out
Scenario: User signs out
Given I am logged in
When I sign out
Then I should see a signed out message
When I return to the site
Then I should be signed out
Feature: Sign up
In order to get access to protected sections of the site
As a user
I want to be able to sign up
Background:
Given I am not logged in
Scenario: User signs up with valid data
When I sign up with valid user data
Then I should see a succesfull sign up message
Scenario: User signs up with invalid email
When I sign up with an invalid email
Then I should see an invalid email message
Scenario: User signs up without password
When I sign up without a password
Then I should see a missing password message
Scenario: User signs up without password confirmation
When I sign up without a confirmed password
Then I should see a missing password confirmation message
Scenario: User signs up with mismatched password and confirmation
When I sign up with a mismatched password confirmation
Then I should see a mismatched password message
Feature: Edit User
As a registered user of the website
I want to edit my user profile
so I can change my username
Scenario: I sign in and edit my account
Given I am logged in
When I edit my account details
Then I should see an account edited message
Feature: Show Users
As a visitor to the website
I want to see registered users listed on the homepage
so I can know if the site has users
Scenario: Viewing users
Given I exist as a user
When I look at the list of users
Then I should see my name
