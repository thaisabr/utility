Feature: User Authentication
Scenario: invalid user
Given I have loaded the fixtures
When I am on the home page
And I follow "Forgot password?"
When I fill in "reset_password_for" with "testuser"
And I press "Reset Password"
Then I should see "Check your email"
And 1 email should be delivered
When I fill in "User name" with "testuser"
And I fill in "testuser"'s temporary password
And I press "Log In"
Then I should see "Hi, testuser"
And I should see "Change My Password"
When I fill in "New Password" with "newpas"
And I fill in "Confirm New Password" with "newpas"
And I press "Change Password"
Then I should see "Your password has been changed"
When I am logged out
When I am on the homepage
And I fill in "User name" with "testuser"
And I fill in "Password" with "newpas"
And I press "Log In"
Then I should see "Hi, testuser"
Scenario: Wrong username
Given I have no users
And the following activated user exists
| login    | password |
| sam      | secret   |
And all emails have been delivered
When I am on the home page
And I fill in "User name" with "sammy"
And I fill in "Password" with "test"
And I press "Log In"
Then I should see "The password or user name you entered doesn't match our records."
Scenario: Wrong username
Given I have no users
And the following activated user exists
| login    | password |
| sam      | secret   |
And all emails have been delivered
When I am on the home page
And I fill in "User name" with "sam"
And I fill in "Password" with "tester"
And I press "Log In"
Then I should see "The password or user name you entered doesn't match our records. Please try again or follow the 'Forgot password?' link below."
