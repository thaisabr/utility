Classes: 5
[name:Collection, file:jpatel531_folioapp/app/models/collection.rb, step:Then ]
[name:Rails, file:null, step:When ]
[name:User, file:jpatel531_folioapp/app/models/user.rb, step:Given ]
[name:User, file:jpatel531_folioapp/app/models/user.rb, step:Then ]
[name:User, file:jpatel531_folioapp/app/models/user.rb, step:null]

Methods: 32
[name:attach_file, type:Object, file:null, step:When ]
[name:caption, type:Object, file:null, step:null]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:Given ]
[name:click_link, type:Object, file:null, step:When ]
[name:create, type:User, file:jpatel531_folioapp/app/models/user.rb, step:Given ]
[name:current_path, type:Object, file:null, step:Given ]
[name:current_path, type:Object, file:null, step:Then ]
[name:eq, type:Object, file:null, step:Given ]
[name:eq, type:Object, file:null, step:Then ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find, type:Object, file:null, step:Then ]
[name:genre_names, type:Work, file:jpatel531_folioapp/app/models/work.rb, step:null]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_css, type:Object, file:null, step:Then ]
[name:have_text, type:Object, file:null, step:Then ]
[name:image, type:Object, file:null, step:null]
[name:index, type:CollectionsController, file:jpatel531_folioapp/app/controllers/collections_controller.rb, step:null]
[name:join, type:Object, file:null, step:When ]
[name:last, type:Object, file:null, step:Then ]
[name:login_as, type:Object, file:null, step:Given ]
[name:medium_names, type:Work, file:jpatel531_folioapp/app/models/work.rb, step:null]
[name:new, type:WorksController, file:jpatel531_folioapp/app/controllers/works_controller.rb, step:null]
[name:not_to, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:Then ]
[name:publish, type:Object, file:null, step:null]
[name:show, type:UsersController, file:jpatel531_folioapp/app/controllers/users_controller.rb, step:null]
[name:title, type:Object, file:null, step:null]
[name:to, type:Object, file:null, step:Given ]
[name:to, type:Object, file:null, step:Then ]
[name:within, type:Object, file:null, step:Given ]

Referenced pages: 6
jpatel531_folioapp/app/views/collections/index.html.erb
jpatel531_folioapp/app/views/users/_information.html.erb
jpatel531_folioapp/app/views/users/show.html.erb
jpatel531_folioapp/app/views/works/_art.html.erb
jpatel531_folioapp/app/views/works/_writing.html.erb
jpatel531_folioapp/app/views/works/new.html.erb

