Classes: 10
[name:OpportunitiesController, file:jpatel531_folioapp/app/controllers/opportunities_controller.rb, step:null]
[name:Opportunity, file:jpatel531_folioapp/app/models/opportunity.rb, step:null]
[name:Organisation, file:jpatel531_folioapp/app/models/organisation.rb, step:Given ]
[name:Organisation, file:jpatel531_folioapp/app/models/organisation.rb, step:null]
[name:OrganisationsController, file:jpatel531_folioapp/app/controllers/organisations_controller.rb, step:null]
[name:Rails, file:null, step:Given ]
[name:Rails, file:null, step:When ]
[name:User, file:jpatel531_folioapp/app/models/user.rb, step:Given ]
[name:User, file:jpatel531_folioapp/app/models/user.rb, step:null]
[name:User, file:jpatel531_folioapp/app/models/user.rb, step:Then ]

Methods: 27
[name:attach_file, type:Object, file:null, step:Given ]
[name:attach_file, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:Given ]
[name:create, type:Object, file:null, step:Given ]
[name:create, type:User, file:jpatel531_folioapp/app/models/user.rb, step:Given ]
[name:creator_role, type:Organisation, file:jpatel531_folioapp/app/models/organisation.rb, step:null]
[name:current_user, type:Object, file:null, step:null]
[name:fill_in, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_css, type:Object, file:null, step:Then ]
[name:have_text, type:Object, file:null, step:Then ]
[name:index, type:HomeController, file:jpatel531_folioapp/app/controllers/home_controller.rb, step:null]
[name:join, type:Object, file:null, step:Given ]
[name:join, type:Object, file:null, step:When ]
[name:login_as, type:Object, file:null, step:Given ]
[name:new, type:Organisation, file:jpatel531_folioapp/app/models/organisation.rb, step:Given ]
[name:new, type:OrganisationsController, file:jpatel531_folioapp/app/controllers/organisations_controller.rb, step:null]
[name:new, type:OpportunitiesController, file:jpatel531_folioapp/app/controllers/opportunities_controller.rb, step:null]
[name:page, type:Object, file:null, step:Then ]
[name:select, type:Object, file:null, step:When ]
[name:show, type:UsersController, file:jpatel531_folioapp/app/controllers/users_controller.rb, step:null]
[name:show, type:OrganisationsController, file:jpatel531_folioapp/app/controllers/organisations_controller.rb, step:null]
[name:to, type:Object, file:null, step:Then ]

Referenced pages: 11
jpatel531_folioapp/app/views/home/index.html.erb
jpatel531_folioapp/app/views/opportunities/new.html.erb
jpatel531_folioapp/app/views/organisations/_information.html.erb
jpatel531_folioapp/app/views/organisations/_new_profile_pic.html.erb
jpatel531_folioapp/app/views/organisations/_profile_picture.html.erb
jpatel531_folioapp/app/views/organisations/new.html.erb
jpatel531_folioapp/app/views/organisations/show.html.erb
jpatel531_folioapp/app/views/users/_information.html.erb
jpatel531_folioapp/app/views/users/_new_profile_pic.html.erb
jpatel531_folioapp/app/views/users/_profile_picture.html.erb
jpatel531_folioapp/app/views/users/show.html.erb

