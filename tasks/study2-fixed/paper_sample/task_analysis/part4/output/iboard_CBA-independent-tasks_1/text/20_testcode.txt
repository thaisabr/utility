Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )fill in "([^"]*)" with "([^"]*)"(?: within "([^"]*)")?$/ do |field, value, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_content(text)
    else
      assert page.has_content?(text)
    end
  end
end
Given /^the following default pages?$/ do |table|
  Page.unscoped.delete_all
  t = PageTemplate.find_or_create_by(name: 'default')
  t.save!
  table.hashes.each do |hash|
    Factory('page', hash.merge( :page_template_id => t.id ))
  end
end
Given /the default user set/ do
  User.unscoped.delete_all
  [
    #ROLES = [:guest, :confirmed_user, :author, :moderator, :maintainer, :admin]
    #see user.rb model
    #
    #  ATTENTION cba makes the first user an admin!
    #  -> The first user of the following hash must be the admin!
    {
      :email => 'admin@iboard.cc',
      :name  => 'admin',
      :roles_mask => 5,
      :password => 'thisisnotsecret', :password_confirmation => 'thisisnotsecret',
      :confirmed_at => "2010-01-01 00:00:00"
    },
    # Define NON-ADMINS BELOW
    {
      :email => 'user@iboard.cc',
      :name  => 'testmax',
      :roles_mask => 1,
      :password => 'thisisnotsecret', :password_confirmation => 'thisisnotsecret',
      :confirmed_at => "2010-01-01 00:00:00"
    },
    {
      :email => 'author@iboard.cc',
      :name  => 'Author',
      :roles_mask => 2,
      :password => 'thisisnotsecret', :password_confirmation => 'thisisnotsecret',
      :confirmed_at => "2010-01-01 00:00:00"
    },
    {
      :email => 'moderator@iboard.cc',
      :name  => 'Moderator',
      :roles_mask => 3,
      :password => 'thisisnotsecret', :password_confirmation => 'thisisnotsecret',
      :confirmed_at => "2010-01-01 00:00:00"
    },
    {
      :email => 'maintainer@iboard.cc',
      :name  => 'maintainer',
      :roles_mask => 4,
      :password => 'thisisnotsecret', :password_confirmation => 'thisisnotsecret',
      :confirmed_at => "2010-01-01 00:00:00"
    },
    {
      :email => 'staff@iboard.cc',
      :name  => 'staff',
      :roles_mask => 4,
      :password => 'thisisnotsecret', :password_confirmation => 'thisisnotsecret',
      :confirmed_at => "2010-01-01 00:00:00"
    }
  ].each do |hash|
    Factory('user', hash)
  end
end
Given /^I am logged in as user "([^"]*)" with password "([^"]*)"$/ do |email, password|
  visit path_to('sign_out')
  visit path_to('sign_in')
  fill_in('user_email', :with => email)
  fill_in('user_password', :with => password)
  click_button('Sign in')
  page.should have_content("Signed in successfully.")
end
Given /^I click on "([^"]*)"$/ do |button|
  click_button(button.to_s)
end
Given /^I click on link "([^"]*)" within "([^"]*)"$/ do |arg1, arg2|
  within( :css, "#{arg2}" ) do
    click_link(arg1.to_s)
  end
end
Given /^the following comment records for page "([^"]*)"$/ do |commentable, table|
  page = Page.where(:title => commentable).first
  page.comments.unscoped.delete_all
  table.hashes.each do |hash|
    page.comments << Factory('comment', hash)
  end
  page.save
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)

    case page_name
    when /the home page/
      '/'
    when /users page/
      '/registrations'
    when /registrations page/
      '/registrations'
    when /sign_in/
      '/users/sign_in'
    when /sign_out/
      '/users/sign_out'
    when /the edit user page/
      '/users/edit'
    when /the profile page of user "([^"]*)/
      username = $1
      user = User.where(name: username).first
      "/profile/#{user.id}"
    when /(the )?edit page for "([^"]*)"/
      title = $1 == 'the ' ? $2 : $1
      page = Page.where(:title => title).first
      "/pages/#{page.id.to_s}/edit"
    when /(the )?edit page template for "([^"]*)"/
      title = $1 == 'the ' ? $2 : $1
      page = Page.unscoped.where(:title => title).first
      "/pages/#{page.id.to_s}/edit"
    when /(the )?page path of "([^"]*)"/
      title = $1 == 'the ' ? $2 : $1
      page = Page.unscoped.where(:title => title).first
      if page
        "/pages/#{page.id.to_s}"
      else
        "/pages"
      end
    when /permalink_path of "([^"]*)"/
      title = $1
      page = Page.where(:title => title).first
      "/p/#{page.link_to_title}"
    when /edit role page for "([^"]*)"/
      begin
        user = User.where(:name => $1).first
        "/users/#{user.id}/edit_role"
      rescue Object => e
        raise "Can't find user #{user.name} / #{e.inspect}"
      end
    when /blogs page/
      '/blogs'
    when /blog path of "([^"]*)"/
      title = $1
      blog = Blog.where(:title => title).first
      "/blogs/#{blog._id}"
    when /the read posting "([^"]*)" page of blog "([^"]*)"/
      posting_title = $1
      blog_title = $2
      blog = Blog.where(:title => blog_title).first
      posting = blog.postings.where(:title => posting_title).first
      "/blogs/#{blog._id}/postings/#{posting._id}"
    when /the posting page of "([^"]*)"/
      posting=Posting.where(:title=> $1).first
      "/postings/#{posting.id}"
    when /feed/
      "/feed"
    when /edit site_menu page for menu "([^"]*)"/
      menu_name = $1
      site_menu = SiteMenu.where(:name => menu_name).first
      "/site_menus/#{site_menu.id.to_s}/edit"
    when /the templates page/
      "/pages/templates"
    when /the new_article page/
      "/pages/new_article"
    when /the edit first component path for "([^"]*)"/
      page_title = $1
      page = Page.where(:title => page_title).first
      component = page.page_components.first
      "/pages/#{page.id.to_s}/page_components/#{component.id.to_s}/edit"
    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "With the following path_components \"#{path_components.inspect}\"\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
