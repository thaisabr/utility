Feature: photos
In order to enlighten humanity for the good of society
As a rock star
I want to post pictures of a button
Background:
Given a user with username "bob"
When I sign in as "bob@bob.bob"
And I am on the home page
Scenario: deleting a photo will delete a photo-only post if the photo was the last image
Given I expand the publisher
And I attach the file "spec/fixtures/button.png" to hidden element "file" within "#file-upload"
And I wait for the ajax to finish
And I press "Share"
And I wait for the ajax to finish
When I go to the photo page for "bob@bob.bob"'s latest post
And I follow "edit_photo_toggle"
And I preemptively confirm the alert
And I press "Delete Photo"
And I go to the home page
Then I should see 0 posts
Feature: invitation acceptance
Scenario: accept invitation from admin
Given I have been invited by an admin
And I am on my acceptance form page
And I fill in the following:
| user_username              | ohai           |
| user_email                 | woot@sweet.com |
| user_password         | secret         |
| user_password_confirmation | secret         |
And I press "Create my account"
Then I should be on the getting started page
And I should see "Well, hello there!"
And I fill in the following:
| profile_first_name | O             |
When I follow "awesome_button"
Then I should be on the multi page
Scenario: accept invitation from user
Given I have been invited by a user
And I am on my acceptance form page
And I fill in the following:
| user_username              | ohai           |
| user_email                 | woot@sweet.com |
| user_password         | secret         |
| user_password_confirmation | secret         |
And I press "Create my account"
Then I should be on the getting started page
And I should see "Well, hello there!"
And I fill in the following:
| profile_first_name | O             |
When I follow "awesome_button"
Then I should be on the multi page
Feature: User manages contacts
In order to share with a limited group
As a User
I want to create new aspects
Scenario: deleting an aspect from contacts index
Given I am signed in
And I have an aspect called "People"
When I am on the contacts page
And I follow "People"
And I follow "add contacts to People"
And I wait for the ajax to finish
And I preemptively confirm the alert
And I press "Delete" in the modal window
Then I should be on the contacts page
And I should not see "People" within "#aspect_nav"
Scenario: Editing the aspect memberships of a contact from the aspect edit facebox
Given I am signed in
And I have 2 contacts
And I have an aspect called "Cat People"
When I am on the contacts page
And I follow "Cat People"
And I follow "add contacts to Cat People"
And I wait for the ajax to finish
And I press the first ".contact_list .button"
And I wait for the ajax to finish
Then I should have 1 contact in "Cat People"
When I press the first ".contact_list .button"
And I wait for the ajax to finish
Then I should have 0 contacts in "Cat People"
Feature: new user registration
Background:
When I go to the new user registration page
And I fill in "user_username" with "ohai"
And I fill in "user_email" with "ohai@example.com"
And I fill in "user_password" with "secret"
And I fill in "user_password_confirmation" with "secret"
And I press "Create my account"
Then I should be on the getting started page
And I should see "Well, hello there!"
And I should see "Who are you?"
And I should see "What are you into?"
Scenario: new user goes through the setup wizard
And I fill in the following:
| profile_first_name | O             |
And I follow "awesome_button"
Then I should be on the multi page
And I should not see "awesome_button"
Scenario: new user skips the setup wizard
When I follow "awesome_button"
Then I should be on the multi page
Feature: Unfollowing
In order to stop seeing updates from self-important rockstars
As a user
I want to be able to stop following people
Background:
Given a user with email "bob@bob.bob"
And a user with email "alice@alice.alice"
When I sign in as "bob@bob.bob"
And I am on "alice@alice.alice"'s page
And I add the person to my "Besties" aspect
Scenario: stop following someone while on the aspect edit page
When I go to the home page
And I click on my name in the header
And I follow "Contacts"
And I follow "Besties"
And I follow "add contacts to Besties"
And I wait for the ajax to finish
And I preemptively confirm the alert
And I press the first ".added" within "#facebox .contact_list ul > li:first-child"
And I wait for the ajax to finish
When I follow "My Contacts"
Then I should have 0 contacts in "Besties"
When I go to the destroy user session page
And I sign in as "alice@alice.alice"
And I am on "bob@bob.bob"'s page
Then I should not see "is sharing with you."
