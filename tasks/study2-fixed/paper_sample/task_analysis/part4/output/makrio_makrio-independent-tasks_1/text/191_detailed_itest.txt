Classes: 2
[name:User, file:makrio_makrio/app/models/user.rb, step:When ]
[name:User, file:makrio_makrio/app/models/user.rb, step:Given ]

Methods: 34
[name:aspects, type:StreamsController, file:makrio_makrio/app/controllers/streams_controller.rb, step:Given ]
[name:aspects, type:Aspect, file:makrio_makrio/lib/stream/aspect.rb, step:Given ]
[name:aspects, type:Base, file:makrio_makrio/lib/stream/base.rb, step:Given ]
[name:click_link, type:Object, file:null, step:When ]
[name:create_user, type:Object, file:null, step:Given ]
[name:delete_all_visible_cookies, type:Object, file:null, step:Given ]
[name:delete_cookie, type:Object, file:null, step:Given ]
[name:find_by_email, type:User, file:makrio_makrio/app/models/user.rb, step:When ]
[name:find_by_email, type:User, file:makrio_makrio/app/models/user.rb, step:Given ]
[name:first, type:Object, file:null, step:When ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:Then ]
[name:post, type:Object, file:null, step:Given ]
[name:posts, type:EvilQuery, file:makrio_makrio/lib/evil_query.rb, step:When ]
[name:posts, type:Activity, file:makrio_makrio/lib/stream/activity.rb, step:When ]
[name:posts, type:Aspect, file:makrio_makrio/lib/stream/aspect.rb, step:When ]
[name:posts, type:Base, file:makrio_makrio/lib/stream/base.rb, step:When ]
[name:posts, type:Comments, file:makrio_makrio/lib/stream/comments.rb, step:When ]
[name:posts, type:FollowedTag, file:makrio_makrio/lib/stream/followed_tag.rb, step:When ]
[name:posts, type:Likes, file:makrio_makrio/lib/stream/likes.rb, step:When ]
[name:posts, type:Mention, file:makrio_makrio/lib/stream/mention.rb, step:When ]
[name:posts, type:Multi, file:makrio_makrio/lib/stream/multi.rb, step:When ]
[name:posts, type:Person, file:makrio_makrio/lib/stream/person.rb, step:When ]
[name:posts, type:Public, file:makrio_makrio/lib/stream/public.rb, step:When ]
[name:posts, type:Tag, file:makrio_makrio/lib/stream/tag.rb, step:When ]
[name:profile, type:Object, file:null, step:Given ]
[name:respond_to?, type:Object, file:null, step:Then ]
[name:show, type:PostsController, file:makrio_makrio/app/controllers/posts_controller.rb, step:null]
[name:split, type:Object, file:null, step:Given ]
[name:update_attributes!, type:Object, file:null, step:Given ]
[name:with_scope, type:WebSteps, file:makrio_makrio/features/step_definitions/web_steps.rb, step:When ]
[name:with_scope, type:WebSteps, file:makrio_makrio/features/step_definitions/web_steps.rb, step:Then ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 2
makrio_makrio/app/views/posts/show.html.haml
makrio_makrio/app/views/posts/show.mobile.haml

