Feature: Interacting with tags
Background:
Given there is a user "Samuel Beckett" who's tagged "#rockstar"
And I am signed in
And I am on the homepage
Scenario: Searching for a tag keeps the search term in the search field
When I search for "#rockstar"
Then I should be on the tag page for "rockstar"
And the "q" field within "#global_search" should contain "#rockstar"
Feature: Change email
Scenario: Change my email
Given I am signed in
And I click on my name in the header
And I follow "Settings"
Then I should be on my account settings page
When I fill in "user_email" with "new_email@newplac.es"
And I press "Change email"
Then I should see "Email changed"
And I follow the "confirm_email" link from the last sent email
Then I should see "activated"
And my "email" should be "new_email@newplac.es"
Feature: Change password
Scenario: Change my password
Given I am signed in
And I click on my name in the header
And I follow "Settings"
Then I should be on my account settings page
When I put in my password in "user_current_password"
And I fill in "user_password" with "newsecret"
And I fill in "user_password_confirmation" with "newsecret"
And I press "Change password"
Then I should see "Password changed"
Then I should be on the new user session page
When I sign in with password "newsecret"
Then I should be on the aspects page
Feature: Close Account
In order to close an existing account
As a user
I want to sign in, close my account and try to log in again
Scenario: user closes account
Given I am signed in
When I click on my name in the header
And I follow "Settings"
And I preemptively confirm the alert
And I follow "Close Account"
Then I should be on the home page
When I go to the new user session page
And I try to sign in manually
Then I should be on the new user session page
When I wait for the ajax to finish
Then I should see "Invalid email or password."
Scenario: post display should not throw error when mention is removed for the user whose account is closed
Given a user named "Bob Jones" with email "bob@bob.bob"
And a user named "Alice Smith" with email "alice@alice.alice"
And a user with email "bob@bob.bob" is connected with "alice@alice.alice"
When I sign in as "alice@alice.alice"
And I am on the home page
And I expand the publisher
And I fill in "status_message_fake_text" with "Hi, @{Bob Jones; bob_jones@example.org} long time no see"
And I press "Share"
And I log out
Then I sign in as "bob@bob.bob"
When I click on my name in the header
And I follow "Settings"
And I preemptively confirm the alert
And I follow "Close Account"
Then I sign in as "alice@alice.alice"
And I am on the home page
Then I should see "Hi, Bob Jones long time no see"
Feature: user authentication
@javascript
Scenario: user logs out
Given I am signed in
And I click on my name in the header
And I follow "Log out"
Then I should be on the logged out page
And I should see "Now go mobile."
Feature: User manages contacts
In order to share with a limited group
As a User
I want to create new aspects
Scenario: clicking on the contacts link in the header with zero contacts directs a user to the featured users page
Given I am signed in
And I have 0 contacts
And I am on the home page
And I click on my name in the header
When I follow "Contacts"
Then I should see "Featured Users" within ".span-18"
Scenario: clicking on the contacts link in the header with contacts does not send a user to the featured users page
Given I am signed in
And I have 2 contacts
And I am on the home page
And I click on my name in the header
When I follow "Contacts"
Then I should not see "Featured Users" within "#section_header"
Feature: Unfollowing
In order to stop seeing updates from self-important rockstars
As a user
I want to be able to stop following people
Background:
Given a user with email "bob@bob.bob"
And a user with email "alice@alice.alice"
When I sign in as "bob@bob.bob"
And I am on "alice@alice.alice"'s page
And I add the person to my "Besties" aspect
Scenario: stop following someone while on the aspect edit page
When I go to the home page
And I click on my name in the header
And I follow "Contacts"
And I follow "Besties"
And I follow "Add contacts to Besties"
And I wait for the ajax to finish
And I preemptively confirm the alert
And I press the first ".added" within "#facebox .contact_list ul > li:first-child"
And I wait for the ajax to finish
When I follow "My Contacts"
Then I should have 0 contacts in "Besties"
When I go to the destroy user session page
And I sign in as "alice@alice.alice"
And I am on "bob@bob.bob"'s page
Then I should not see "is sharing with you."
Feature: oembed
In order to make videos easy accessible
As a user
I want the links in my posts be replaced by their oEmbed representation
Background:
Given a user named "Alice Smith" with email "alice@alice.alice"
And I have several oEmbed data in cache
When I sign in as "alice@alice.alice"
And I am on the home page
Scenario: Post a secure video link
Given I expand the publisher
When I fill in "status_message_fake_text" with "http://youtube.com/watch?v=M3r2XDceM6A&format=json"
And I press "Share"
And I follow "Your Aspects"
Then I should see a video player
Scenario: Post an unsecure video link
Given I expand the publisher
When I fill in "status_message_fake_text" with "http://mytube.com/watch?v=M3r2XDceM6A&format=json"
And I press "Share"
And I follow "Your Aspects"
Then I should not see a video player
And I should see "http://mytube.com/watch?v=M3r2XDceM6A&format=json"
Scenario: Post an unsecure rich-typed link
Given I expand the publisher
When I fill in "status_message_fake_text" with "http://myrichtube.com/watch?v=M3r2XDceM6A&format=json"
And I press "Share"
And I follow "Your Aspects"
Then I should not see a video player
And I should see "http://myrichtube.com/watch?v=M3r2XDceM6A&format=json"
Scenario: Post a photo link
Given I expand the publisher
When I fill in "status_message_fake_text" with "http://farm4.static.flickr.com/3123/2341623661_7c99f48bbf_m.jpg"
And I press "Share"
And I follow "Your Aspects"
Then I should see a "img" within ".stream_element"
Scenario: Post an unsupported text link
Given I expand the publisher
When I fill in "status_message_fake_text" with "http://www.we-do-not-support-oembed.com/index.html"
And I press "Share"
And I follow "Your Aspects"
Then I should see "http://www.we-do-not-support-oembed.com/index.html" within ".stream_element"
