Feature: Change email
Scenario: Change my email preferences
Given I am signed in
When I go to the users edit page
And I uncheck "user_email_preferences_mentioned"
And I press "change_email_preferences"
Then I should see "Email notifications changed"
And the "user_email_preferences_mentioned" checkbox should not be checked
Feature: Change password
Scenario: Attempt to change my password with invalid input
Given I am signed in
When I go to the edit user page
And I fill out change password section with my password and "too" and "short"
And I press "Change password"
Then I should see "Password change failed"
And I should see "Password is too short"
And I should see "Password confirmation doesn't match"
Scenario: Attempt to reset password with invalid password
Given a user named "Georges Abitbol" with email "forgetful@users.net"
Given I am on forgot password page
When I fill out forgot password form with "forgetful@users.net"
And I submit forgot password form
When I follow the "Change my password" link from the last sent email
When I fill out reset password form with "too" and "short"
And I press "Change my password"
Then I should be on the user password page
And I should see "Password is too short"
And I should see "Password confirmation doesn't match"
