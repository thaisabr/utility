When /^I (?:sign|log) in as "([^"]*)"$/ do |email|
  @me = User.find_by_email(email)
  @me.password ||= 'password'
  automatic_login
  confirm_login
end
When /^I (?:log|sign) out$/ do
  logout
end
  def automatic_login
    @me ||= FactoryGirl.create(:user_with_aspect, :getting_started => false)
    visit(new_integration_sessions_path(:user_id => @me.id))
    click_button "Login"
  end
  def confirm_login
    page.has_content?("#{@me.first_name} #{@me.last_name}")
  end
  def logout
    $browser.delete_cookie('_session', 'path=/') if $browser
    $browser.delete_all_visible_cookies if $browser
  end
Given /^(?:|I )am on (.+)$/ do |page_name|
  navigate_to(page_name)
end
When /^(?:|I )go to (.+)$/ do |page_name|
  navigate_to(page_name)
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    click_button(button)
  end
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
When /^(?:|I )fill in the following(?: within "([^"]*)")?:$/ do |selector, fields|
  with_scope(selector) do
    fields.rows_hash.each do |name, value|
      step %{I fill in "#{name}" with "#{value}"}
    end
  end
end
Then /^(?:|I )should see (\".+?\"[\s]*)(?:[\s]+within[\s]* "([^"]*)")?$/ do |vars, selector|
  vars.scan(/"([^"]+?)"/).flatten.each do |text|
    with_scope(selector) do
      current_scope.should have_content(text)
    end
  end
end
Then /^(?:|I )should not see (\".+?\"[\s]*)(?:[\s]+within[\s]* "([^"]*)")?$/ do |vars,selector|
  vars.scan(/"([^"]+?)"/).flatten.each do |text|
    with_scope(selector) do
      page.should have_no_content(text)
    end
  end
end
Then /^(?:|I )should be on (.+)$/ do |page_name|
  confirm_on_page(page_name)
end
  def navigate_to(page_name)
    path = path_to(page_name)
    unless path.is_a?(Hash)
      visit(path)
    else
      visit(path[:path])
      await_elem = path[:special_elem]
      find(await_elem.delete(:selector), await_elem)
    end
  end
  def confirm_on_page(page_name)
    current_path = URI.parse(current_url).path
    expect(current_path).to eq(path_to(page_name))
  end
  def with_scope(locator)
    if locator
      within(locator, match: :first) do
        yield
      end
    else
      yield
    end
  end
  def path_to(page_name)
    case page_name
      when /^person_photos page$/
         person_photos_path(@me.person)
      when /^the home(?: )?page$/
        stream_path
      when /^step (\d)$/
        if $1.to_i == 1
          getting_started_path
        else
          getting_started_path(:step => $1)
        end
      when /^the tag page for "([^\"]*)"$/
        tag_path($1)
      when /^its ([\w ]+) page$/
        send("#{$1.gsub(/\W+/, '_')}_path", @it)
      when /^the ([\w ]+) page$/
        send("#{$1.gsub(/\W+/, '_')}_path")
      when /^my edit profile page$/
        edit_profile_path
      when /^my profile page$/
        person_path(@me.person)
      when /^my acceptance form page$/
        invite_code_path(InvitationCode.first)
      when /^the requestors profile$/
        person_path(Request.where(:recipient_id => @me.person.id).first.sender)
      when /^"([^\"]*)"'s page$/
        p = User.find_by_email($1).person
        { path: person_path(p),
          # '#diaspora_handle' on desktop, '.description' on mobile
          special_elem: { selector: '#diaspora_handle, .description', text: p.diaspora_handle }
        }
      when /^"([^\"]*)"'s photos page$/
        p = User.find_by_email($1).person
        person_photos_path p
      when /^my account settings page$/
        edit_user_path
      when /^forgot password page$/
          new_user_password_path
      when /^"(\/.*)"/
        $1
      else
        raise "Can't find mapping from \"#{page_name}\" to a path."
    end
  end
And /^I expand the publisher$/ do
 click_publisher
end
And /^I close the publisher$/ do
 find("#publisher #hide_publisher").click
end
Then /^the publisher should be expanded$/ do
  find("#publisher")["class"].should_not include("closed")
end
And /^I want to mention (?:him|her) from the profile$/ do
  find('#mention_button').click
  within('#mentionModal') do
    click_publisher
  end
end
When /^I click to delete the first uploaded photo$/ do
  page.execute_script("$('#photodropzone .x').css('display', 'block');")
  find("#photodropzone .x", match: :first).click
end
And /^I confirm the alert$/ do
  page.driver.browser.switch_to.alert.accept
end
When /^(.*) in the mention modal$/ do |action|
  within('#mentionModal') do
    step action
  end
end
When /^I press the first "([^"]*)"(?: within "([^"]*)")?$/ do |link_selector, within_selector|
  with_scope(within_selector) do
    current_scope.find(link_selector, match: :first).click
  end
end
Then /^(?:|I )should see a "([^\"]*)"(?: within "([^\"]*)")?$/ do |selector, scope_selector|
  with_scope(scope_selector) do
    current_scope.should have_css selector
  end
end
When /^I have turned off jQuery effects$/ do
  page.execute_script("$.fx.off = true")
end
When /^I search for "([^\"]*)"$/ do |search_term|
  fill_in "q", :with => search_term
  find_field("q").native.send_key(:enter)
  find("#tags_show .span3")
end
Then /^I should see a flash message indicating success$/ do
  flash_message_success?.should be true
end
  def click_publisher
    page.execute_script('
     $("#publisher").removeClass("closed");
     $("#publisher").find("#status_message_fake_text").focus();
    ')
  end
  def flash_message_success?
    flash_message(selector: "notice").visible?
  end
  def flash_message(opts={})
    selector = opts.delete(:selector)
    selector &&= "#flash_#{selector}"
    find(selector || '.message', {match: :first}.merge(opts))
  end
Given /^a user with username "([^\"]*)"$/ do |username|
  create_user(:email => username + "@" + username + '.' + username, :username => username)
end
Given /^(?:|[tT]hat )?following user[s]?(?: exist[s]?)?:$/ do |table|
  table.hashes.each do |hash|
    if hash.has_key? "username" and hash.has_key? "email"
      step %{a user named "#{hash['username']}" with email "#{hash['email']}"}
    elsif hash.has_key? "username"
      step %{a user with username "#{hash['username']}"}
    elsif hash.has_key? "email"
      step %{a user with email "#{hash['email']}"}
    end
  end
end
Given /^I have following aspect[s]?:$/ do |fields|
  fields.raw.each do |field|
    step %{I have an aspect called "#{field[0]}"}
  end
end
When /^I have user with username "([^"]*)" in an aspect called "([^"]*)"$/ do |username, aspect|
  user = User.find_by_username(username)
  contact = @me.reload.contact_for(user.person)
  contact.aspects << @me.aspects.find_by_name(aspect)
end
Given /^a user with email "([^"]*)" is connected with "([^"]*)"$/ do |arg1, arg2|
  user1 = User.where(:email => arg1).first
  user2 = User.where(:email => arg2).first
  connect_users(user1, user1.aspects.where(:name => "Besties").first, user2, user2.aspects.where(:name => "Besties").first)
end
Given /^a user with username "([^"]*)" is connected with "([^"]*)"$/ do |arg1, arg2|
  user1 = User.where(:username => arg1).first
  user2 = User.where(:username => arg2).first
  connect_users(user1, user1.aspects.where(:name => "Besties").first, user2, user2.aspects.where(:name => "Besties").first)
end
Given /^there is a user "([^\"]*)" who's tagged "([^\"]*)"$/ do |full_name, tag|
  username = full_name.gsub(/\W/, "").underscore
  step "a user named \"#{full_name}\" with email \"#{username}@example.com\""
  user = User.find_by_username(username)
  user.profile.tag_string = tag
  user.profile.build_tags
  user.profile.save!
end
Given /^many posts from alice for bob$/ do
  alice = FactoryGirl.create(:user_with_aspect, :username => 'alice', :email => 'alice@alice.alice', :password => 'password', :getting_started => false)
  bob = FactoryGirl.create(:user_with_aspect, :username => 'bob', :email => 'bob@bob.bob', :password => 'password', :getting_started => false)
  connect_users_with_aspects(alice, bob)
  time_fulcrum = Time.now - 40000
  time_interval = 1000
  (1..30).each do |n|
    post = alice.post :status_message, :text => "#{alice.username} - #{n} - #seeded", :to => alice.aspects.where(:name => "generic").first.id
    post.created_at = time_fulcrum - time_interval
    post.updated_at = time_fulcrum + time_interval
    post.save
    time_interval += 1000
  end
end
When /^I (?:add|remove) the person (?:to|from) my "([^\"]*)" aspect$/ do |aspect_name|
  toggle_aspect_via_ui(aspect_name)
end
  def save
    begin
      @run_server = Capybara.run_server
      @driver = Capybara.current_driver
      @host = Capybara.app_host
    rescue => e
      puts "Saving exception: " + e.inspect
    end
  end
  def toggle_aspect_via_ui(aspect_name)
    aspects_dropdown = find(".aspect_membership_dropdown .dropdown-toggle", match: :first)
    aspects_dropdown.click
    aspect = find(".aspect_membership_dropdown.open .dropdown-menu li", text: aspect_name)
    aspect.click
    aspect.parent.should have_no_css(".loading")

    # close dropdown
    page.should have_no_css('#profile.loading')
    aspects_dropdown.click if aspects_dropdown.has_xpath?("..[contains(@class, 'active')]", wait: 3)
    aspects_dropdown.should have_no_xpath("..[contains(@class, 'active')]")
  end
When /^I press the "([^\"]*)" key somewhere$/ do |key|
  within("#main_stream") do
    find("div.stream_element", match: :first).native.send_keys(key)
  end
end
When /^I press the "([^\"]*)" key in the publisher$/ do |key|
  find("#status_message_fake_text").native.send_keys(key)
end
Then /^post (\d+) should be highlighted$/ do |position|
  find(".shortcut_selected .post-content").text.should == stream_element_numbers_content(position).text
end
And /^I should have navigated to the highlighted post$/ do
  find(".shortcut_selected")["offsetTop"].to_i.should == page.evaluate_script("window.pageYOffset + 50").to_i
end
  def stream_element_numbers_content(position)
    find(".stream_element:nth-child(#{position}) .post-content")
  end
And /^I mention Alice in the publisher$/ do
  alice = User.find_by_email 'alice@alice.alice'
  write_in_publisher("@{Alice Smith ; #{alice.person.diaspora_handle}}")
end
And /^I click on the first user in the mentions dropdown list$/ do
  find('.mentions-autocomplete-list li', match: :first).click
end
  def write_in_publisher(txt)
    fill_in 'status_message_fake_text', with: txt
  end
Then /^I should see an uploaded image within the photo drop zone$/ do
  find("#photodropzone img")["src"].should include("uploads/images")
end
Then /^I should not see an uploaded image within the photo drop zone$/ do
  page.should_not have_css "#photodropzone img"
end
Then /^I should not see any posts in my stream$/ do
  page.assert_selector(".stream_element", count: 0)
end
When /^I write the status message "([^"]*)"$/ do |text|
  write_in_publisher(text)
end
When /^I append "([^"]*)" to the publisher$/ do |text|
  append_to_publisher(text)
end
When /^I attach "([^"]*)" to the publisher$/ do |path|
  upload_file_with_publisher(path)
end
  def write_in_publisher(txt)
    fill_in 'status_message_fake_text', with: txt
  end
  def append_to_publisher(txt, input_selector='#status_message_fake_text')
    elem = find(input_selector, visible: false)
    elem.native.send_keys(' ' + txt)

    # make sure the other text field got the new contents
    expect(find('#status_message_text', visible: false).value).to include(txt)
  end
  def upload_file_with_publisher(path)
    page.execute_script(%q{$("input[name='file']").css("opacity", '1');})
    with_scope("#publisher_textarea_wrapper") do
      attach_file("file", Rails.root.join(path).to_s)
      # wait for the image to be ready
      page.assert_selector(".publisher_photo.loading", count: 0)
    end
  end
When /^I select only "([^"]*)" aspect$/ do |aspect_name|
  click_link 'My Aspects'
  within('#aspects_list') do
    click_link 'Select all' if has_link? 'Select all'
    click_link 'Deselect all'
    current_scope.should have_no_css '.selected'
  end
  step %Q(I select "#{aspect_name}" aspect as well)
end
Then /^"([^"]*)" should be post (\d+)$/ do |post_text, position|
  stream_element_numbers_content(position).should have_content(post_text)
end
Then /^the first post should be a preview$/ do
  find(".post_preview .post-content").text.should == first_post_text
end
  def first_post_text
    find(".stream_element .post-content", match: :first).text
  end
When /^I fill in the following for the options:$/ do |table|
  i = 0
  table.raw.flatten.each do |value|
    all(".poll-answer input")[i].set(value)
    i+=1
  end
end
When(/^I press the element "(.*?)"$/) do |selector|
  page.should have_css(selector)
  find(selector).click
end
Then /I should see the mention modal/ do
  step %{I should see a "#mentionModal.in"}
end
When /^(?:|I )fill in "([^"]*)" with "([^"]*)"(?: within "([^"]*)")?$/ do |field, value, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
Given /^a user with email "([^\"]*)"$/ do |email|
  create_user(:email => email)
end
Given /^a user with username "([^\"]*)"$/ do |username|
  create_user(:email => username + "@" + username + '.' + username, :username => username)
end
Given /^a user named "([^\"]*)" with email "([^\"]*)"$/ do |name, email|
  first, last = name.split
  user = create_user(:email => email, :username => "#{first}_#{last}")
  user.profile.update_attributes!(:first_name => first, :last_name => last) if first
end
Given /^I have an aspect called "([^\"]*)"$/ do |aspect_name|
  @me.aspects.create!(:name => aspect_name)
  @me.reload
end
  def create_user(overrides={})
    default_attrs = {
        :password => 'password',
        :password_confirmation => 'password',
        :getting_started => false
    }

    user = FactoryGirl.create(:user, default_attrs.merge(overrides))
    add_standard_aspects(user)
    user
  end
  def add_standard_aspects(user)
    user.aspects.create(:name => "Besties")
    user.aspects.create(:name => "Unicorns")
  end
When /^I select "([^"]*)" aspect as well$/ do |aspect_name|
  within('#aspects_list') do
    click_link aspect_name
  end
  step %Q(I should see "#{aspect_name}" aspect selected)
end
Then /^(?:|I )should see a "([^\"]*)"(?: within "([^\"]*)")?$/ do |selector, scope_selector|
  with_scope(scope_selector) do
    current_scope.should have_css selector
  end
end
Then /^I should see "([^"]*)" aspect selected$/ do |aspect_name|
  aspect = @me.aspects.where(:name => aspect_name).first
  within("#aspects_list") do
    page.should have_css "li[data-aspect_id='#{aspect.id}'] .selected"
  end
end
