Classes: 15
[name:Array, file:null, step:Then ]
[name:Block, file:diaspora_diaspora/app/models/block.rb, step:When ]
[name:BlocksController, file:diaspora_diaspora/app/controllers/blocks_controller.rb, step:When ]
[name:FactoryGirl, file:null, step:Given ]
[name:Hash, file:null, step:When ]
[name:InvitationCode, file:diaspora_diaspora/app/models/invitation_code.rb, step:When ]
[name:Post, file:diaspora_diaspora/app/models/post.rb, step:Given ]
[name:Rails, file:null, step:When ]
[name:Request, file:diaspora_diaspora/lib/diaspora/federated/request.rb, step:When ]
[name:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:User, file:diaspora_diaspora/app/models/user.rb, step:When ]
[name:User, file:diaspora_diaspora/app/models/user.rb, step:Then ]
[name:UsersController, file:diaspora_diaspora/app/controllers/admin/users_controller.rb, step:When ]
[name:UsersController, file:diaspora_diaspora/app/controllers/users_controller.rb, step:When ]
[name:aspects_controller, file:diaspora_diaspora/app/controllers/aspects_controller.rb, step:When ]

Methods: 180
[name:accept, type:Object, file:null, step:When ]
[name:activity, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:When ]
[name:add_standard_aspects, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:alert, type:Object, file:null, step:When ]
[name:append_to_publisher, type:PublishingCukeHelpers, file:diaspora_diaspora/features/support/publishing_cuke_helpers.rb, step:Given ]
[name:aspect_ids, type:Aspect, file:diaspora_diaspora/lib/stream/aspect.rb, step:Given ]
[name:aspect_ids, type:Base, file:diaspora_diaspora/lib/stream/base.rb, step:Given ]
[name:aspects, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:Given ]
[name:aspects, type:UserPresenter, file:diaspora_diaspora/app/presenters/user_presenter.rb, step:Given ]
[name:aspects, type:Aspect, file:diaspora_diaspora/lib/stream/aspect.rb, step:Given ]
[name:aspects, type:Base, file:diaspora_diaspora/lib/stream/base.rb, step:Given ]
[name:aspects, type:Object, file:null, step:Given ]
[name:aspects, type:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:attach_file, type:Object, file:null, step:When ]
[name:automatic_login, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:automatic_login, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:When ]
[name:automatic_login, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Then ]
[name:be, type:Object, file:null, step:Then ]
[name:bookmarklet, type:StatusMessagesController, file:diaspora_diaspora/app/controllers/status_messages_controller.rb, step:When ]
[name:browser, type:Object, file:null, step:When ]
[name:btn, type:Object, file:null, step:When ]
[name:btn btn-block, type:Object, file:null, step:When ]
[name:btn btn-danger, type:Object, file:null, step:When ]
[name:button, type:Object, file:null, step:When ]
[name:changelog_url, type:ApplicationHelper, file:diaspora_diaspora/app/helpers/application_helper.rb, step:When ]
[name:click, type:Object, file:null, step:When ]
[name:click, type:Object, file:null, step:Then ]
[name:click, type:Object, file:null, step:Given ]
[name:click, type:Object, file:null, step:And ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:Then ]
[name:confirm_login, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:confirm_login_mobile, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:confirm_login_mobile, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:When ]
[name:confirm_login_mobile, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Then ]
[name:confirm_on_page, type:Paths, file:diaspora_diaspora/features/support/paths.rb, step:Then ]
[name:connect_users, type:Object, file:null, step:Given ]
[name:contact_for, type:Querying, file:diaspora_diaspora/app/models/user/querying.rb, step:Given ]
[name:create, type:Object, file:null, step:Given ]
[name:create!, type:Generator, file:diaspora_diaspora/lib/federated/generator.rb, step:Given ]
[name:create_user, type:Object, file:null, step:Given ]
[name:create_user, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:current_scope, type:Object, file:null, step:Then ]
[name:current_scope, type:Object, file:null, step:When ]
[name:delete, type:Object, file:null, step:When ]
[name:delete, type:Object, file:null, step:Then ]
[name:delete_all_visible_cookies, type:Object, file:null, step:Given ]
[name:delete_cookie, type:Object, file:null, step:Given ]
[name:diaspora_handle, type:Object, file:null, step:When ]
[name:diaspora_handle, type:Comment, file:diaspora_diaspora/app/models/comment.rb, step:Given ]
[name:diaspora_handle, type:Conversation, file:diaspora_diaspora/app/models/conversation.rb, step:Given ]
[name:diaspora_handle, type:Message, file:diaspora_diaspora/app/models/message.rb, step:Given ]
[name:diaspora_handle, type:PollParticipation, file:diaspora_diaspora/app/models/poll_participation.rb, step:Given ]
[name:diaspora_handle, type:Profile, file:diaspora_diaspora/app/models/profile.rb, step:Given ]
[name:diaspora_handle, type:RelayableRetraction, file:diaspora_diaspora/lib/diaspora/federated/relayable_retraction.rb, step:Given ]
[name:diaspora_handle, type:Request, file:diaspora_diaspora/lib/diaspora/federated/request.rb, step:Given ]
[name:diaspora_handle, type:Shareable, file:diaspora_diaspora/lib/diaspora/federated/shareable.rb, step:Given ]
[name:diaspora_handle, type:SignedRetraction, file:diaspora_diaspora/lib/diaspora/federated/signed_retraction.rb, step:Given ]
[name:diaspora_handle, type:Relayable, file:diaspora_diaspora/lib/federated/relayable.rb, step:Given ]
[name:driver, type:Object, file:null, step:When ]
[name:each, type:Object, file:null, step:When ]
[name:each, type:Object, file:null, step:Then ]
[name:each, type:Object, file:null, step:Given ]
[name:edit, type:ProfilesController, file:diaspora_diaspora/app/controllers/profiles_controller.rb, step:When ]
[name:edit, type:UsersController, file:diaspora_diaspora/app/controllers/admin/users_controller.rb, step:When ]
[name:edit, type:UsersController, file:diaspora_diaspora/app/controllers/users_controller.rb, step:When ]
[name:execute_script, type:Object, file:null, step:When ]
[name:export_photos, type:UsersController, file:diaspora_diaspora/app/controllers/admin/users_controller.rb, step:When ]
[name:export_photos, type:UsersController, file:diaspora_diaspora/app/controllers/users_controller.rb, step:When ]
[name:export_profile, type:UsersController, file:diaspora_diaspora/app/controllers/admin/users_controller.rb, step:When ]
[name:export_profile, type:UsersController, file:diaspora_diaspora/app/controllers/users_controller.rb, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:find, type:Object, file:null, step:When ]
[name:find, type:Object, file:null, step:Then ]
[name:find, type:Object, file:null, step:Given ]
[name:find, type:Object, file:null, step:And ]
[name:find_by_email, type:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:find_by_email, type:User, file:diaspora_diaspora/app/models/user.rb, step:When ]
[name:find_by_email, type:User, file:diaspora_diaspora/app/models/user.rb, step:Then ]
[name:find_by_name, type:Object, file:null, step:Given ]
[name:find_by_text, type:Post, file:diaspora_diaspora/app/models/post.rb, step:Given ]
[name:find_by_username, type:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:find_field, type:Object, file:null, step:Then ]
[name:find_field, type:Object, file:null, step:Given ]
[name:first, type:InvitationCode, file:diaspora_diaspora/app/models/invitation_code.rb, step:When ]
[name:first, type:Object, file:null, step:When ]
[name:first, type:Object, file:null, step:Then ]
[name:first, type:Object, file:null, step:Given ]
[name:first_name, type:Object, file:null, step:Given ]
[name:flash_message, type:ApplicationCukeHelpers, file:diaspora_diaspora/features/support/application_cuke_helpers.rb, step:Then ]
[name:flash_message_alert?, type:ApplicationCukeHelpers, file:diaspora_diaspora/features/support/application_cuke_helpers.rb, step:Then ]
[name:flatten, type:Object, file:null, step:Then ]
[name:getting_started, type:User, file:diaspora_diaspora/app/models/user.rb, step:When ]
[name:guid, type:SignedRetraction, file:diaspora_diaspora/lib/diaspora/federated/signed_retraction.rb, step:Given ]
[name:has_content?, type:Object, file:null, step:Given ]
[name:has_css?, type:Object, file:null, step:Given ]
[name:has_key?, type:Object, file:null, step:Given ]
[name:hashes, type:Object, file:null, step:Given ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:Given ]
[name:have_css, type:Object, file:null, step:Then ]
[name:have_no_content, type:Object, file:null, step:Then ]
[name:have_value, type:Object, file:null, step:Given ]
[name:hover, type:Object, file:null, step:When ]
[name:id, type:Object, file:null, step:Given ]
[name:id, type:Object, file:null, step:When ]
[name:include, type:Object, file:null, step:Then ]
[name:index, type:PhotosController, file:diaspora_diaspora/app/controllers/photos_controller.rb, step:When ]
[name:index, type:ContactsController, file:diaspora_diaspora/app/controllers/contacts_controller.rb, step:When ]
[name:index, type:ServicesController, file:diaspora_diaspora/app/controllers/services_controller.rb, step:When ]
[name:index, type:TermsController, file:diaspora_diaspora/app/controllers/terms_controller.rb, step:When ]
[name:is_a?, type:Object, file:null, step:When ]
[name:is_a?, type:Object, file:null, step:Then ]
[name:join, type:Object, file:null, step:When ]
[name:last_name, type:Object, file:null, step:Given ]
[name:liked, type:User, file:diaspora_diaspora/app/models/user.rb, step:When ]
[name:local_or_remote_person_path, type:PeopleHelper, file:diaspora_diaspora/app/helpers/people_helper.rb, step:When ]
[name:login_as, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:When ]
[name:login_page, type:Paths, file:diaspora_diaspora/features/support/paths.rb, step:When ]
[name:logout, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:manual_login, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:When ]
[name:mentioned, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:When ]
[name:mentioned, type:User, file:diaspora_diaspora/app/models/user.rb, step:When ]
[name:merge, type:Object, file:null, step:Given ]
[name:multi, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:When ]
[name:native, type:Object, file:null, step:Given ]
[name:navigate_to, type:Paths, file:diaspora_diaspora/features/support/paths.rb, step:When ]
[name:navigate_to, type:Paths, file:diaspora_diaspora/features/support/paths.rb, step:Given ]
[name:page, type:Object, file:null, step:Given ]
[name:page, type:Object, file:null, step:When ]
[name:page, type:Object, file:null, step:Then ]
[name:password, type:Object, file:null, step:When ]
[name:path_to, type:Paths, file:diaspora_diaspora/features/support/paths.rb, step:When ]
[name:path_to, type:Paths, file:diaspora_diaspora/features/support/paths.rb, step:Then ]
[name:person, type:Object, file:null, step:When ]
[name:person, type:PostPresenter, file:diaspora_diaspora/app/presenters/post_presenter.rb, step:When ]
[name:person, type:PostPresenter, file:diaspora_diaspora/app/presenters/post_presenter.rb, step:Given ]
[name:post, type:Object, file:null, step:Given ]
[name:profile, type:Object, file:null, step:Given ]
[name:raise, type:Object, file:null, step:When ]
[name:raw, type:MessageRenderer, file:diaspora_diaspora/lib/diaspora/message_renderer.rb, step:Given ]
[name:reload, type:Object, file:null, step:Given ]
[name:rows_hash, type:Object, file:null, step:When ]
[name:scan, type:Object, file:null, step:Then ]
[name:select, type:Object, file:null, step:When ]
[name:select, type:Object, file:null, step:Given ]
[name:send, type:Object, file:null, step:When ]
[name:send_key, type:Object, file:null, step:Given ]
[name:send_keys, type:Object, file:null, step:Given ]
[name:sender, type:Object, file:null, step:When ]
[name:show, type:TagsController, file:diaspora_diaspora/app/controllers/tags_controller.rb, step:When ]
[name:show, type:InvitationCodesController, file:diaspora_diaspora/app/controllers/invitation_codes_controller.rb, step:When ]
[name:show, type:PhotosController, file:diaspora_diaspora/app/controllers/photos_controller.rb, step:When ]
[name:split, type:Object, file:null, step:Given ]
[name:spotlight, type:ContactsController, file:diaspora_diaspora/app/controllers/contacts_controller.rb, step:When ]
[name:strip_exif, type:Block, file:diaspora_diaspora/app/models/block.rb, step:When ]
[name:switch_to, type:Object, file:null, step:When ]
[name:tag_name, type:Object, file:null, step:Then ]
[name:text, type:Photo, file:diaspora_diaspora/app/models/photo.rb, step:When ]
[name:text, type:Publisher, file:diaspora_diaspora/lib/publisher.rb, step:Then ]
[name:to, type:Object, file:null, step:Then ]
[name:to, type:Object, file:null, step:Given ]
[name:to_s, type:Aspect, file:diaspora_diaspora/app/models/aspect.rb, step:When ]
[name:to_s, type:MessageRenderer, file:diaspora_diaspora/lib/diaspora/message_renderer.rb, step:When ]
[name:toggle_mobile, type:HomeController, file:diaspora_diaspora/app/controllers/home_controller.rb, step:When ]
[name:toggle_mobile, type:HomeController, file:diaspora_diaspora/app/controllers/home_controller.rb, step:null]
[name:update_attributes!, type:Object, file:null, step:Given ]
[name:username, type:Object, file:null, step:When ]
[name:value, type:Object, file:null, step:Then ]
[name:visible?, type:Object, file:null, step:Then ]
[name:where, type:Request, file:diaspora_diaspora/lib/diaspora/federated/request.rb, step:When ]
[name:where, type:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:where, type:Object, file:null, step:Given ]
[name:with_scope, type:WebSteps, file:diaspora_diaspora/features/step_definitions/web_steps.rb, step:When ]
[name:with_scope, type:WebSteps, file:diaspora_diaspora/features/step_definitions/web_steps.rb, step:Given ]
[name:with_scope, type:WebSteps, file:diaspora_diaspora/features/step_definitions/web_steps.rb, step:Then ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 51
diaspora_diaspora/app/views/aspects/_aspect_listings.haml
diaspora_diaspora/app/views/aspects/_aspect_stream.haml
diaspora_diaspora/app/views/aspects/_no_contacts_message.haml
diaspora_diaspora/app/views/aspects/_no_posts_message.haml
diaspora_diaspora/app/views/contacts/_aspect_listings.haml
diaspora_diaspora/app/views/contacts/_header.html.haml
diaspora_diaspora/app/views/contacts/_sidebar.html.haml
diaspora_diaspora/app/views/contacts/index.html.haml
diaspora_diaspora/app/views/contacts/index.mobile.haml
diaspora_diaspora/app/views/contacts/spotlight.haml
diaspora_diaspora/app/views/devise/passwords/new.haml
diaspora_diaspora/app/views/devise/passwords/new.mobile.haml
diaspora_diaspora/app/views/home/default.haml
diaspora_diaspora/app/views/home/show.haml
diaspora_diaspora/app/views/people/_index.html.haml
diaspora_diaspora/app/views/people/_person.html.haml
diaspora_diaspora/app/views/photos/_index.html.haml
diaspora_diaspora/app/views/photos/_new_profile_photo.haml
diaspora_diaspora/app/views/photos/_new_profile_photo.mobile.haml
diaspora_diaspora/app/views/photos/_photo.haml
diaspora_diaspora/app/views/photos/edit.html.haml
diaspora_diaspora/app/views/photos/show.mobile.haml
diaspora_diaspora/app/views/profiles/_edit.haml
diaspora_diaspora/app/views/profiles/_edit_public.haml
diaspora_diaspora/app/views/profiles/edit.haml
diaspora_diaspora/app/views/profiles/edit.mobile.haml
diaspora_diaspora/app/views/publisher/_aspect_dropdown.html.haml
diaspora_diaspora/app/views/publisher/_publisher.html.haml
diaspora_diaspora/app/views/services/_add_remove_services.haml
diaspora_diaspora/app/views/services/index.html.haml
diaspora_diaspora/app/views/shared/_donatepod.html.haml
diaspora_diaspora/app/views/shared/_invitations.haml
diaspora_diaspora/app/views/shared/_links.haml
diaspora_diaspora/app/views/shared/_public_explain.haml
diaspora_diaspora/app/views/shared/_right_sections.html.haml
diaspora_diaspora/app/views/shared/_settings_nav.haml
diaspora_diaspora/app/views/shared/_stream.haml
diaspora_diaspora/app/views/status_messages/bookmarklet.html.haml
diaspora_diaspora/app/views/status_messages/bookmarklet.mobile.haml
diaspora_diaspora/app/views/streams/main_stream.html.haml
diaspora_diaspora/app/views/streams/main_stream.mobile.haml
diaspora_diaspora/app/views/tags/_followed_tags_listings.haml
diaspora_diaspora/app/views/tags/show.haml
diaspora_diaspora/app/views/tags/show.mobile.haml
diaspora_diaspora/app/views/terms/default.haml
diaspora_diaspora/app/views/users/_close_account_modal.haml
diaspora_diaspora/app/views/users/edit.html.haml
diaspora_diaspora/app/views/users/edit.mobile.haml
diaspora_diaspora/app/views/users/getting_started.haml
diaspora_diaspora/app/views/users/getting_started.mobile.haml
diaspora_diaspora/app/views/users/privacy_settings.html.haml

