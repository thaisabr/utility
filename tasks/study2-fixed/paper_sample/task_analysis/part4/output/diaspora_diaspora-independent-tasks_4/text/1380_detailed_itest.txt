Classes: 12
[name:Aspect, file:diaspora_diaspora/app/models/aspect.rb, step:When ]
[name:Aspect, file:diaspora_diaspora/lib/stream/aspect.rb, step:When ]
[name:FactoryGirl, file:null, step:When ]
[name:FactoryGirl, file:null, step:Given ]
[name:Hash, file:null, step:When ]
[name:InvitationCode, file:diaspora_diaspora/app/models/invitation_code.rb, step:When ]
[name:Rails, file:null, step:Given ]
[name:Request, file:diaspora_diaspora/lib/diaspora/federated/request.rb, step:When ]
[name:URI, file:null, step:Then ]
[name:User, file:diaspora_diaspora/app/models/user.rb, step:When ]
[name:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:aspects_controller, file:diaspora_diaspora/app/controllers/aspects_controller.rb, step:When ]

Methods: 144
[name:accept, type:Object, file:null, step:When ]
[name:activity, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:When ]
[name:add_standard_aspects, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:alert, type:Object, file:null, step:When ]
[name:append_to_publisher, type:PublishingCukeHelpers, file:diaspora_diaspora/features/support/publishing_cuke_helpers.rb, step:Given ]
[name:aspects, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:When ]
[name:aspects, type:UserPresenter, file:diaspora_diaspora/app/presenters/user_presenter.rb, step:When ]
[name:aspects, type:Aspect, file:diaspora_diaspora/lib/stream/aspect.rb, step:When ]
[name:aspects, type:Base, file:diaspora_diaspora/lib/stream/base.rb, step:When ]
[name:aspects, type:Object, file:null, step:When ]
[name:aspects, type:Object, file:null, step:Then ]
[name:aspects, type:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:attach_file, type:Object, file:null, step:Given ]
[name:automatic_login, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:When ]
[name:automatic_login, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:be, type:Object, file:null, step:Then ]
[name:browser, type:Object, file:null, step:When ]
[name:btn, type:Object, file:null, step:When ]
[name:btn btn-danger, type:Object, file:null, step:When ]
[name:button, type:Object, file:null, step:When ]
[name:changelog_url, type:ApplicationHelper, file:diaspora_diaspora/app/helpers/application_helper.rb, step:When ]
[name:click, type:Object, file:null, step:Given ]
[name:click, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:click_publisher, type:PublishingCukeHelpers, file:diaspora_diaspora/features/support/publishing_cuke_helpers.rb, step:When ]
[name:click_publisher, type:PublishingCukeHelpers, file:diaspora_diaspora/features/support/publishing_cuke_helpers.rb, step:Given ]
[name:close_account_confirm, type:Object, file:null, step:When ]
[name:confirm_login, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:When ]
[name:confirm_login, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:confirm_on_page, type:Paths, file:diaspora_diaspora/features/support/paths.rb, step:Then ]
[name:connect_users, type:Object, file:null, step:When ]
[name:contact_for, type:Querying, file:diaspora_diaspora/app/models/user/querying.rb, step:When ]
[name:contacts, type:PeopleController, file:diaspora_diaspora/app/controllers/people_controller.rb, step:Then ]
[name:contacts, type:Contact, file:diaspora_diaspora/app/models/contact.rb, step:Then ]
[name:count, type:Object, file:null, step:Then ]
[name:create, type:Object, file:null, step:Given ]
[name:create!, type:Generator, file:diaspora_diaspora/lib/federated/generator.rb, step:When ]
[name:create_user, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:create_user, type:Object, file:null, step:Given ]
[name:current_scope, type:Object, file:null, step:Then ]
[name:current_scope, type:Object, file:null, step:When ]
[name:current_url, type:Object, file:null, step:Then ]
[name:delete, type:Object, file:null, step:When ]
[name:delete_all_visible_cookies, type:Object, file:null, step:Then ]
[name:delete_cookie, type:Object, file:null, step:Then ]
[name:diaspora_handle, type:Object, file:null, step:When ]
[name:driver, type:Object, file:null, step:When ]
[name:each, type:Object, file:null, step:When ]
[name:each, type:Object, file:null, step:Then ]
[name:each, type:Object, file:null, step:Given ]
[name:edit, type:ProfilesController, file:diaspora_diaspora/app/controllers/profiles_controller.rb, step:When ]
[name:edit, type:UsersController, file:diaspora_diaspora/app/controllers/admin/users_controller.rb, step:When ]
[name:edit, type:UsersController, file:diaspora_diaspora/app/controllers/users_controller.rb, step:When ]
[name:eq, type:Object, file:null, step:Then ]
[name:execute_script, type:Object, file:null, step:Given ]
[name:execute_script, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find, type:Object, file:null, step:When ]
[name:find, type:Object, file:null, step:Given ]
[name:find_by_email, type:User, file:diaspora_diaspora/app/models/user.rb, step:When ]
[name:find_by_email, type:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:find_by_name, type:Object, file:null, step:When ]
[name:find_by_username, type:User, file:diaspora_diaspora/app/models/user.rb, step:When ]
[name:first, type:InvitationCode, file:diaspora_diaspora/app/models/invitation_code.rb, step:When ]
[name:first, type:Object, file:null, step:When ]
[name:first, type:Object, file:null, step:Then ]
[name:first_name, type:Object, file:null, step:When ]
[name:flatten, type:Object, file:null, step:Then ]
[name:has_content?, type:Object, file:null, step:When ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:has_key?, type:Object, file:null, step:Given ]
[name:has_link?, type:Object, file:null, step:When ]
[name:has_xpath?, type:Object, file:null, step:When ]
[name:hashes, type:Object, file:null, step:Given ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_css, type:Object, file:null, step:Then ]
[name:have_css, type:Object, file:null, step:When ]
[name:have_no_content, type:Object, file:null, step:Then ]
[name:have_no_css, type:Object, file:null, step:When ]
[name:have_no_xpath, type:Object, file:null, step:When ]
[name:hover, type:Object, file:null, step:When ]
[name:id, type:Object, file:null, step:When ]
[name:include, type:Object, file:null, step:Given ]
[name:index, type:PhotosController, file:diaspora_diaspora/app/controllers/photos_controller.rb, step:When ]
[name:index, type:ServicesController, file:diaspora_diaspora/app/controllers/services_controller.rb, step:When ]
[name:index, type:ContactsController, file:diaspora_diaspora/app/controllers/contacts_controller.rb, step:When ]
[name:index, type:TermsController, file:diaspora_diaspora/app/controllers/terms_controller.rb, step:When ]
[name:is_a?, type:Object, file:null, step:When ]
[name:join, type:Object, file:null, step:Given ]
[name:last_name, type:Object, file:null, step:When ]
[name:local_or_remote_person_path, type:PeopleHelper, file:diaspora_diaspora/app/helpers/people_helper.rb, step:When ]
[name:logout, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Then ]
[name:magic_bookmarklet_link, type:ApplicationHelper, file:diaspora_diaspora/app/helpers/application_helper.rb, step:When ]
[name:mentioned, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:When ]
[name:merge, type:Object, file:null, step:Given ]
[name:multi, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:When ]
[name:native, type:Object, file:null, step:Given ]
[name:navigate_to, type:Paths, file:diaspora_diaspora/features/support/paths.rb, step:When ]
[name:new, type:InvitationsController, file:diaspora_diaspora/app/controllers/invitations_controller.rb, step:When ]
[name:page, type:Object, file:null, step:When ]
[name:page, type:Object, file:null, step:Given ]
[name:page, type:Object, file:null, step:Then ]
[name:parent, type:Aspect, file:diaspora_diaspora/app/models/aspect.rb, step:When ]
[name:parent, type:Aspect, file:diaspora_diaspora/lib/stream/aspect.rb, step:When ]
[name:path, type:Object, file:null, step:Then ]
[name:path_to, type:Paths, file:diaspora_diaspora/features/support/paths.rb, step:When ]
[name:path_to, type:Paths, file:diaspora_diaspora/features/support/paths.rb, step:Then ]
[name:person, type:Object, file:null, step:When ]
[name:person, type:PostPresenter, file:diaspora_diaspora/app/presenters/post_presenter.rb, step:When ]
[name:profile, type:Object, file:null, step:Given ]
[name:raise, type:Object, file:null, step:When ]
[name:raw, type:MessageRenderer, file:diaspora_diaspora/lib/diaspora/message_renderer.rb, step:When ]
[name:reload, type:Object, file:null, step:When ]
[name:rows_hash, type:Object, file:null, step:When ]
[name:scan, type:Object, file:null, step:Then ]
[name:send, type:Object, file:null, step:When ]
[name:send_keys, type:Object, file:null, step:Given ]
[name:sender, type:Object, file:null, step:When ]
[name:show, type:TagsController, file:diaspora_diaspora/app/controllers/tags_controller.rb, step:When ]
[name:show, type:InvitationCodesController, file:diaspora_diaspora/app/controllers/invitation_codes_controller.rb, step:When ]
[name:show, type:PhotosController, file:diaspora_diaspora/app/controllers/photos_controller.rb, step:When ]
[name:split, type:Object, file:null, step:Given ]
[name:spotlight, type:ContactsController, file:diaspora_diaspora/app/controllers/contacts_controller.rb, step:When ]
[name:switch_to, type:Object, file:null, step:When ]
[name:text, type:Photo, file:diaspora_diaspora/app/models/photo.rb, step:When ]
[name:to, type:Object, file:null, step:Then ]
[name:to, type:Object, file:null, step:Given ]
[name:to_i, type:Object, file:null, step:Then ]
[name:to_s, type:Aspect, file:diaspora_diaspora/app/models/aspect.rb, step:Given ]
[name:to_s, type:MessageRenderer, file:diaspora_diaspora/lib/diaspora/message_renderer.rb, step:Given ]
[name:toggle_aspect_via_ui, type:AspectsSteps, file:diaspora_diaspora/features/step_definitions/aspects_steps.rb, step:When ]
[name:toggle_mobile, type:HomeController, file:diaspora_diaspora/app/controllers/home_controller.rb, step:When ]
[name:update_attributes!, type:Object, file:null, step:Given ]
[name:value, type:Object, file:null, step:Given ]
[name:where, type:Request, file:diaspora_diaspora/lib/diaspora/federated/request.rb, step:When ]
[name:where, type:User, file:diaspora_diaspora/app/models/user.rb, step:When ]
[name:where, type:Object, file:null, step:When ]
[name:where, type:Object, file:null, step:Then ]
[name:with_scope, type:WebSteps, file:diaspora_diaspora/features/step_definitions/web_steps.rb, step:When ]
[name:with_scope, type:WebSteps, file:diaspora_diaspora/features/step_definitions/web_steps.rb, step:Given ]
[name:with_scope, type:WebSteps, file:diaspora_diaspora/features/step_definitions/web_steps.rb, step:Then ]
[name:within, type:Object, file:null, step:When ]
[name:within, type:Object, file:null, step:Given ]

Referenced pages: 47
diaspora_diaspora/app/views/aspects/_aspect_listings.haml
diaspora_diaspora/app/views/aspects/_aspect_stream.haml
diaspora_diaspora/app/views/aspects/_no_contacts_message.haml
diaspora_diaspora/app/views/aspects/_no_posts_message.haml
diaspora_diaspora/app/views/contacts/_aspect_listings.haml
diaspora_diaspora/app/views/contacts/_contact.html.haml
diaspora_diaspora/app/views/contacts/_header.html.haml
diaspora_diaspora/app/views/contacts/_sidebar.html.haml
diaspora_diaspora/app/views/contacts/index.html.haml
diaspora_diaspora/app/views/contacts/index.mobile.haml
diaspora_diaspora/app/views/contacts/spotlight.haml
diaspora_diaspora/app/views/home/show.html.haml
diaspora_diaspora/app/views/home/show.mobile.haml
diaspora_diaspora/app/views/invitations/new.html.haml
diaspora_diaspora/app/views/invitations/new.mobile.haml
diaspora_diaspora/app/views/invitations/new_blueprint.html.haml
diaspora_diaspora/app/views/people/_index.html.haml
diaspora_diaspora/app/views/people/_person.html.haml
diaspora_diaspora/app/views/photos/_index.html.haml
diaspora_diaspora/app/views/photos/_new_profile_photo.haml
diaspora_diaspora/app/views/photos/_new_profile_photo.mobile.haml
diaspora_diaspora/app/views/photos/_photo.haml
diaspora_diaspora/app/views/photos/edit.html.haml
diaspora_diaspora/app/views/photos/show.mobile.haml
diaspora_diaspora/app/views/profiles/edit.haml
diaspora_diaspora/app/views/profiles/edit.mobile.haml
diaspora_diaspora/app/views/publisher/_aspect_dropdown.html.haml
diaspora_diaspora/app/views/publisher/_publisher.html.haml
diaspora_diaspora/app/views/publisher/_publisher_blueprint.html.haml
diaspora_diaspora/app/views/publisher/_publisher_bootstrap.html.haml
diaspora_diaspora/app/views/services/index.html.haml
diaspora_diaspora/app/views/shared/_add_remove_services.haml
diaspora_diaspora/app/views/shared/_donatepod.html.haml
diaspora_diaspora/app/views/shared/_invitations_blueprint.haml
diaspora_diaspora/app/views/shared/_links.haml
diaspora_diaspora/app/views/shared/_public_explain.haml
diaspora_diaspora/app/views/shared/_right_sections.html.haml
diaspora_diaspora/app/views/shared/_settings_nav.haml
diaspora_diaspora/app/views/shared/_stream.haml
diaspora_diaspora/app/views/streams/main_stream.html.haml
diaspora_diaspora/app/views/streams/main_stream.mobile.haml
diaspora_diaspora/app/views/tags/_followed_tags_listings.haml
diaspora_diaspora/app/views/tags/show.haml
diaspora_diaspora/app/views/tags/show.mobile.haml
diaspora_diaspora/app/views/terms/default.haml
diaspora_diaspora/app/views/users/edit.html.haml
diaspora_diaspora/app/views/users/edit.mobile.haml

