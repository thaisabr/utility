Feature: Change email
Scenario: Change my email
Given I am signed in
And I click on my name in the header
And I follow "settings"
Then I should be on my account settings page
When I fill in "user_email" with "new_email@newplac.es"
And I press "Change email"
Then I should see "Email changed"
And I follow the "confirm_email" link from the last sent email
Then I should see "activated"
And my "email" should be "new_email@newplac.es"
Feature: Change password
Scenario: Change my password
Given I am signed in
And I click on my name in the header
And I follow "settings"
Then I should be on my account settings page
When I put in my password in "user_current_password"
And I fill in "user_password" with "newsecret"
And I fill in "user_password_confirmation" with "newsecret"
And I press "Change password"
Then I should see "Password changed"
Then I should be on the new user session page
When I sign in with password "newsecret"
Then I should be on the aspects page
Feature: public repost
In order to make Diaspora more viral
As a User
I want to reshare my friends post
Background:
Given a user named "Bob Jones" with email "bob@bob.bob"
And a user named "Alice Smith" with email "alice@alice.alice"
And a user with email "bob@bob.bob" is connected with "alice@alice.alice"
Scenario: Keeps track of the number of reshares
And "bob@bob.bob" has a public post with text "reshare this!"
And I sign in as "alice@alice.alice"
And I preemptively confirm the alert
And I follow "Reshare"
And I wait for the ajax to finish
And I go to the home page
Then I should see a ".reshare"
And I follow "Your Aspects"
Then I should see "reshare this!"
Then I should see a ".reshare"
And I should see "Bob"
And I go to the home page
And I should see "1 reshare"
