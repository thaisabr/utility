Classes: 2
[name:User, file:diaspora_diaspora/app/models/user.rb, step:When ]
[name:User, file:diaspora_diaspora/lib/diaspora/user.rb, step:When ]

Methods: 21
[name:Factory, type:Object, file:null, step:Given ]
[name:aspects, type:Object, file:null, step:Given ]
[name:aspects, type:Object, file:null, step:When ]
[name:be_nil, type:Object, file:null, step:Then ]
[name:connect_users, type:Object, file:null, step:When ]
[name:create, type:Conversation, file:diaspora_diaspora/app/models/conversation.rb, step:Given ]
[name:find, type:Object, file:null, step:Then ]
[name:find_by_email, type:User, file:diaspora_diaspora/app/models/user.rb, step:When ]
[name:find_by_email, type:User, file:diaspora_diaspora/lib/diaspora/user.rb, step:When ]
[name:first, type:Object, file:null, step:When ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:Then ]
[name:profile, type:Object, file:null, step:Given ]
[name:respond_to?, type:Object, file:null, step:Then ]
[name:split, type:Object, file:null, step:Given ]
[name:update_attributes, type:Object, file:null, step:Given ]
[name:where, type:User, file:diaspora_diaspora/app/models/user.rb, step:When ]
[name:where, type:User, file:diaspora_diaspora/lib/diaspora/user.rb, step:When ]
[name:with_scope, type:WebSteps, file:diaspora_diaspora/features/step_definitions/web_steps.rb, step:Then ]
[name:within, type:Object, file:null, step:Then ]

Referenced pages: 0

