Feature: commenting
In order to tell Alice how great the picture of her dog is
As Alice's friend
I want to comment on her post
Background:
Given a user named "Bob Jones" with email "bob@bob.bob"
And a user named "Alice Smith" with email "alice@alice.alice"
And a user with email "bob@bob.bob" is connected with "alice@alice.alice"
When I sign in as "alice@alice.alice"
And I am on the home page
And I expand the publisher
And I attach the file "spec/fixtures/button.png" to hidden element "file" within "#file-upload"
And I fill in "status_message_fake_text" with "Look at this dog"
And I press "Share"
And I wait for the ajax to finish
And I follow "All Aspects"
Then I should see "Look at this dog" within ".stream_element"
And I should see a "img" within ".stream_element div.photo_attachments"
Then I log out
Scenario: comment on a status show page
When I sign in as "bob@bob.bob"
And I am on "alice@alice.alice"'s page
Then I should see "Look at this dog"
When I follow "less than a minute ago"
Then I should see "Look at this dog"
Then I follow "Comment"
And I fill in "text" with "I think thats a cat"
And I press "Comment"
And I wait for the ajax to finish
When I am on "alice@alice.alice"'s page
Then I should see "I think thats a cat"
