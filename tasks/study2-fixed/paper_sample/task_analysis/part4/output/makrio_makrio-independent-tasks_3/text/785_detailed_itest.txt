Classes: 7
[name:Photo, file:makrio_makrio/app/models/photo.rb, step:When ]
[name:Rails, file:null, step:When ]
[name:StatusMessagesController, file:makrio_makrio/app/controllers/status_messages_controller.rb, step:When ]
[name:User, file:makrio_makrio/app/models/user.rb, step:When ]
[name:User, file:makrio_makrio/lib/diaspora/user.rb, step:When ]
[name:User, file:makrio_makrio/app/models/user.rb, step:Given ]
[name:User, file:makrio_makrio/lib/diaspora/user.rb, step:Given ]

Methods: 75
[name:Factory, type:Object, file:null, step:Given ]
[name:add_to_streams, type:User, file:makrio_makrio/app/models/user.rb, step:Given ]
[name:are_you_sure, type:Object, file:null, step:When ]
[name:aspects, type:Object, file:null, step:Given ]
[name:aspects, type:Object, file:null, step:Then ]
[name:aspects, type:Object, file:null, step:When ]
[name:be_true, type:Object, file:null, step:Then ]
[name:build_post, type:User, file:makrio_makrio/app/models/user.rb, step:Given ]
[name:button, type:Object, file:null, step:When ]
[name:click, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:connect_users, type:Object, file:null, step:Given ]
[name:create, type:Conversation, file:makrio_makrio/app/models/conversation.rb, step:Given ]
[name:create!, type:Object, file:null, step:Given ]
[name:drag_to, type:Object, file:null, step:When ]
[name:edit, type:ProfilesController, file:makrio_makrio/app/controllers/profiles_controller.rb, step:When ]
[name:edit, type:UsersController, file:makrio_makrio/app/controllers/users_controller.rb, step:When ]
[name:evaluate_script, type:Object, file:null, step:Then ]
[name:evaluate_script, type:Object, file:null, step:When ]
[name:execute_script, type:Object, file:null, step:When ]
[name:export, type:UsersController, file:makrio_makrio/app/controllers/users_controller.rb, step:When ]
[name:export_photos, type:UsersController, file:makrio_makrio/app/controllers/users_controller.rb, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find, type:Object, file:null, step:When ]
[name:find_by_email, type:User, file:makrio_makrio/app/models/user.rb, step:When ]
[name:find_by_email, type:User, file:makrio_makrio/lib/diaspora/user.rb, step:When ]
[name:find_by_name, type:Object, file:null, step:Then ]
[name:find_by_name, type:Object, file:null, step:When ]
[name:finder, type:ServicesController, file:makrio_makrio/app/controllers/services_controller.rb, step:When ]
[name:first, type:Object, file:null, step:Given ]
[name:getting_started, type:UsersController, file:makrio_makrio/app/controllers/users_controller.rb, step:When ]
[name:getting_started_completed, type:UsersController, file:makrio_makrio/app/controllers/users_controller.rb, step:When ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:Then ]
[name:id, type:Fake, file:makrio_makrio/lib/fake.rb, step:Then ]
[name:id, type:Fake, file:makrio_makrio/lib/fake.rb, step:When ]
[name:index, type:ConversationsController, file:makrio_makrio/app/controllers/conversations_controller.rb, step:When ]
[name:index, type:PhotosController, file:makrio_makrio/app/controllers/photos_controller.rb, step:When ]
[name:index, type:ServicesController, file:makrio_makrio/app/controllers/services_controller.rb, step:When ]
[name:inspect, type:Object, file:null, step:Given ]
[name:inviter, type:ServicesController, file:makrio_makrio/app/controllers/services_controller.rb, step:When ]
[name:join, type:Object, file:null, step:When ]
[name:manage, type:AspectsController, file:makrio_makrio/app/controllers/aspects_controller.rb, step:When ]
[name:new, type:ConversationsController, file:makrio_makrio/app/controllers/conversations_controller.rb, step:When ]
[name:object_path, type:ApplicationHelper, file:makrio_makrio/app/helpers/application_helper.rb, step:When ]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:When ]
[name:profile, type:Object, file:null, step:Given ]
[name:raise, type:Object, file:null, step:Given ]
[name:reload, type:Object, file:null, step:Given ]
[name:reload, type:Object, file:null, step:Then ]
[name:reload, type:Object, file:null, step:When ]
[name:respond_to?, type:Object, file:null, step:Then ]
[name:save!, type:Object, file:null, step:Given ]
[name:services.index.really_disconnect, type:Object, file:null, step:When ]
[name:show, type:PeopleController, file:makrio_makrio/app/controllers/people_controller.rb, step:When ]
[name:show, type:HomeController, file:makrio_makrio/app/controllers/home_controller.rb, step:When ]
[name:show, type:PhotosController, file:makrio_makrio/app/controllers/photos_controller.rb, step:When ]
[name:show, type:StatusMessagesController, file:makrio_makrio/app/controllers/status_messages_controller.rb, step:When ]
[name:silence_warnings, type:Object, file:null, step:When ]
[name:sleep, type:Object, file:null, step:When ]
[name:split, type:Object, file:null, step:Given ]
[name:status_message, type:Object, file:null, step:When ]
[name:to_i, type:Object, file:null, step:Then ]
[name:to_s, type:Aspect, file:makrio_makrio/app/models/aspect.rb, step:When ]
[name:to_s, type:Fake, file:makrio_makrio/lib/fake.rb, step:When ]
[name:update_attributes, type:Object, file:null, step:Given ]
[name:visible_posts, type:Querying, file:makrio_makrio/lib/diaspora/user/querying.rb, step:Given ]
[name:where, type:User, file:makrio_makrio/app/models/user.rb, step:Given ]
[name:where, type:User, file:makrio_makrio/lib/diaspora/user.rb, step:Given ]
[name:with_scope, type:WebSteps, file:makrio_makrio/features/step_definitions/web_steps.rb, step:When ]
[name:with_scope, type:WebSteps, file:makrio_makrio/features/step_definitions/web_steps.rb, step:Then ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 44
makrio_makrio/app/views/aspects/_no_posts_message.haml
makrio_makrio/app/views/aspects/manage.html.haml
makrio_makrio/app/views/comments/_comment.html.haml
makrio_makrio/app/views/comments/_comments.haml
makrio_makrio/app/views/conversations/_conversation.haml
makrio_makrio/app/views/conversations/_show.haml
makrio_makrio/app/views/conversations/index.haml
makrio_makrio/app/views/conversations/new.haml
makrio_makrio/app/views/home/show.html.haml
makrio_makrio/app/views/home/show.mobile.haml
makrio_makrio/app/views/messages/_message.haml
makrio_makrio/app/views/people/_person.html.haml
makrio_makrio/app/views/people/_profile_sidebar.html.haml
makrio_makrio/app/views/people/show.html.haml
makrio_makrio/app/views/people/show.mobile.haml
makrio_makrio/app/views/photos/_index.html.haml
makrio_makrio/app/views/photos/_new_photo.haml
makrio_makrio/app/views/photos/_new_profile_photo.haml
makrio_makrio/app/views/photos/_photo.haml
makrio_makrio/app/views/photos/edit.html.haml
makrio_makrio/app/views/photos/show.html.haml
makrio_makrio/app/views/photos/show.mobile.haml
makrio_makrio/app/views/profiles/edit.html.haml
makrio_makrio/app/views/requests/_manage_aspect_contacts.haml
makrio_makrio/app/views/services/_finder.html.haml
makrio_makrio/app/views/services/_remote_friend.html.haml
makrio_makrio/app/views/services/finder.html.haml
makrio_makrio/app/views/services/index.html.haml
makrio_makrio/app/views/shared/_add_contact.html.haml
makrio_makrio/app/views/shared/_add_remove_services.haml
makrio_makrio/app/views/shared/_author_info.html.haml
makrio_makrio/app/views/shared/_contact_list.html.haml
makrio_makrio/app/views/shared/_invitations.haml
makrio_makrio/app/views/shared/_reshare.haml
makrio_makrio/app/views/shared/_stream.haml
makrio_makrio/app/views/shared/_stream_element.html.haml
makrio_makrio/app/views/status_messages/_status_message.haml
makrio_makrio/app/views/status_messages/show.html.haml
makrio_makrio/app/views/status_messages/show.mobile.haml
makrio_makrio/app/views/users/edit.html.haml
makrio_makrio/app/views/users/getting_started.html.haml
makrio_makrio/app/views/users/getting_started/_step_1.html.haml
makrio_makrio/app/views/users/getting_started/_step_2.html.haml
makrio_makrio/app/views/users/getting_started/_step_3.html.haml

