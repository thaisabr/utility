Feature: Not safe for work
Scenario: Setting not safe for work
Given following users exist:
| username    | email             |
| pr0n king   | tommy@pr0n.xxx    |
And I sign in as "tommy@pr0n.xxx"
When I go to the edit profile page
And I mark myself as not safe for work
And I submit the form
Then I should be on the edit profile page
And the "profile[nsfw]" checkbox should be checked
When I go to the edit profile page
And I mark myself as safe for work
And I submit the form
Then I should be on the edit profile page
And the "profile[nsfw]" checkbox should not be checked
