Classes: 4
[name:FactoryGirl, file:null, step:Given ]
[name:Rails, file:null, step:When ]
[name:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:aspects_controller, file:diaspora_diaspora/app/controllers/aspects_controller.rb, step:When ]

Methods: 74
[name:accept, type:Object, file:null, step:When ]
[name:activity, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:When ]
[name:add_standard_aspects, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:alert, type:Object, file:null, step:When ]
[name:aspects, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:Given ]
[name:aspects, type:UserPresenter, file:diaspora_diaspora/app/presenters/user_presenter.rb, step:Given ]
[name:aspects, type:Aspect, file:diaspora_diaspora/lib/stream/aspect.rb, step:Given ]
[name:aspects, type:Base, file:diaspora_diaspora/lib/stream/base.rb, step:Given ]
[name:aspects, type:Object, file:null, step:Given ]
[name:aspects, type:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:attach_file, type:Object, file:null, step:When ]
[name:automatic_login, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:be_false, type:Object, file:null, step:Then ]
[name:browser, type:Object, file:null, step:When ]
[name:button creation, type:Object, file:null, step:When ]
[name:changelog_url, type:ApplicationHelper, file:diaspora_diaspora/app/helpers/application_helper.rb, step:When ]
[name:click, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_publisher, type:PublishingCukeHelpers, file:diaspora_diaspora/features/support/publishing_cuke_helpers.rb, step:Given ]
[name:confirm_login, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:connect_users, type:Object, file:null, step:Given ]
[name:contact_for, type:Querying, file:diaspora_diaspora/app/models/user/querying.rb, step:Given ]
[name:create, type:Object, file:null, step:Given ]
[name:create!, type:Generator, file:diaspora_diaspora/lib/federated/generator.rb, step:Given ]
[name:create_user, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:create_user, type:Object, file:null, step:Given ]
[name:driver, type:Object, file:null, step:When ]
[name:each, type:Object, file:null, step:Given ]
[name:edit, type:AspectsController, file:diaspora_diaspora/app/controllers/aspects_controller.rb, step:When ]
[name:execute_script, type:Object, file:null, step:When ]
[name:execute_script, type:Object, file:null, step:Given ]
[name:find, type:Object, file:null, step:When ]
[name:find, type:Object, file:null, step:Then ]
[name:find_by_email, type:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:find_by_name, type:Object, file:null, step:Given ]
[name:find_by_username, type:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:first, type:Object, file:null, step:Given ]
[name:first_name, type:Object, file:null, step:Given ]
[name:has_content?, type:Object, file:null, step:Given ]
[name:has_key?, type:Object, file:null, step:Given ]
[name:hashes, type:Object, file:null, step:Given ]
[name:have_css, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:Given ]
[name:index, type:ContactsController, file:diaspora_diaspora/app/controllers/contacts_controller.rb, step:When ]
[name:index, type:ServicesController, file:diaspora_diaspora/app/controllers/services_controller.rb, step:When ]
[name:join, type:Object, file:null, step:When ]
[name:last_name, type:Object, file:null, step:Given ]
[name:local_or_remote_person_path, type:PeopleHelper, file:diaspora_diaspora/app/helpers/people_helper.rb, step:When ]
[name:magic_bookmarklet_link, type:ApplicationHelper, file:diaspora_diaspora/app/helpers/application_helper.rb, step:When ]
[name:mentioned, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:When ]
[name:merge, type:Object, file:null, step:Given ]
[name:multi, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:When ]
[name:new, type:InvitationsController, file:diaspora_diaspora/app/controllers/invitations_controller.rb, step:When ]
[name:new, type:AspectsController, file:diaspora_diaspora/app/controllers/aspects_controller.rb, step:When ]
[name:page, type:Object, file:null, step:When ]
[name:page, type:Object, file:null, step:Given ]
[name:page, type:Object, file:null, step:Then ]
[name:person, type:PostPresenter, file:diaspora_diaspora/app/presenters/post_presenter.rb, step:Given ]
[name:profile, type:Object, file:null, step:Given ]
[name:publisher_submittable?, type:PublishingCukeHelpers, file:diaspora_diaspora/features/support/publishing_cuke_helpers.rb, step:Then ]
[name:raw, type:Object, file:null, step:Given ]
[name:reload, type:Object, file:null, step:Given ]
[name:show, type:TagsController, file:diaspora_diaspora/app/controllers/tags_controller.rb, step:When ]
[name:split, type:Object, file:null, step:Given ]
[name:spotlight, type:ContactsController, file:diaspora_diaspora/app/controllers/contacts_controller.rb, step:When ]
[name:switch_to, type:Object, file:null, step:When ]
[name:to, type:Object, file:null, step:Then ]
[name:to_s, type:Aspect, file:diaspora_diaspora/app/models/aspect.rb, step:When ]
[name:toggle_mobile, type:HomeController, file:diaspora_diaspora/app/controllers/home_controller.rb, step:When ]
[name:update_attributes!, type:Object, file:null, step:Given ]
[name:where, type:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:where, type:Object, file:null, step:Given ]
[name:with_scope, type:WebSteps, file:diaspora_diaspora/features/step_definitions/web_steps.rb, step:When ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 35
diaspora_diaspora/app/views/aspects/_aspect_listings.haml
diaspora_diaspora/app/views/aspects/_aspect_stream.haml
diaspora_diaspora/app/views/aspects/_no_contacts_message.haml
diaspora_diaspora/app/views/aspects/_no_posts_message.haml
diaspora_diaspora/app/views/aspects/edit.html.haml
diaspora_diaspora/app/views/aspects/new.haml
diaspora_diaspora/app/views/community_spotlight/_user.html.haml
diaspora_diaspora/app/views/contacts/_aspect_listings.haml
diaspora_diaspora/app/views/contacts/_contact.html.haml
diaspora_diaspora/app/views/contacts/index.html.haml
diaspora_diaspora/app/views/contacts/index.mobile.haml
diaspora_diaspora/app/views/contacts/spotlight.haml
diaspora_diaspora/app/views/home/show.html.haml
diaspora_diaspora/app/views/home/show.mobile.haml
diaspora_diaspora/app/views/invitations/new.html.haml
diaspora_diaspora/app/views/invitations/new.mobile.haml
diaspora_diaspora/app/views/people/_index.html.haml
diaspora_diaspora/app/views/people/_person.html.haml
diaspora_diaspora/app/views/services/index.html.haml
diaspora_diaspora/app/views/shared/_add_remove_services.haml
diaspora_diaspora/app/views/shared/_contact_list.html.haml
diaspora_diaspora/app/views/shared/_contact_sidebar.html.haml
diaspora_diaspora/app/views/shared/_donatepod.html.haml
diaspora_diaspora/app/views/shared/_invitations.haml
diaspora_diaspora/app/views/shared/_links.haml
diaspora_diaspora/app/views/shared/_public_explain.haml
diaspora_diaspora/app/views/shared/_publisher.html.haml
diaspora_diaspora/app/views/shared/_right_sections.html.haml
diaspora_diaspora/app/views/shared/_settings_nav.haml
diaspora_diaspora/app/views/shared/_stream.haml
diaspora_diaspora/app/views/streams/main_stream.html.haml
diaspora_diaspora/app/views/streams/main_stream.mobile.haml
diaspora_diaspora/app/views/tags/_followed_tags_listings.haml
diaspora_diaspora/app/views/tags/show.haml
diaspora_diaspora/app/views/tags/show.mobile.haml

