Feature: following and being followed
Background:
Given following users exist:
| email             |
| bob@bob.bob       |
| alice@alice.alice |
When I sign in as "bob@bob.bob"
And I am on "alice@alice.alice"'s page
And I add the person to my "Besties" aspect
When I am on the home page
And I expand the publisher
And I fill in the following:
| status_message_fake_text    | I am following you    |
Scenario: interacting with the profile page of someone you follow who is not following you
When I sign in as "bob@bob.bob"
And I am on "alice@alice.alice"'s page
Then I should see "Besties"
Then  I should see a "#mention_button" within "#profile"
Then I should not see a "#message_button" within "#profile"
Scenario: interacting with the profile page of someone who follows you but who you do not follow
Given I sign in as "alice@alice.alice"
And I am on "bob@bob.bob"'s page
Then I should see "Add contact"
Then I should not see a "#mention_button" within "#profile"
Then I should not see a "#message_button" within "#profile"
Scenario: interacting with the profile page of someone you follow who also follows you
Given I sign in as "alice@alice.alice"
And I am on "bob@bob.bob"'s page
When I add the person to my "Besties" aspect
And I add the person to my "Unicorns" aspect
When I go to "bob@bob.bob"'s page
Then I should see "All Aspects"
Then I should see a "#mention_button" within "#profile"
Then I should see a "#message_button" within "#profile"
