Given /^a user with email "([^\"]*)"$/ do |email|
  create_user(:email => email)
end
And /^I follow the "([^\"]*)" link from the last sent email$/ do |link_text|
  email_text = Devise.mailer.deliveries.first.body.to_s
  email_text = Devise.mailer.deliveries.first.html_part.body.raw_source if email_text.blank?
  doc = Nokogiri("<div>" + email_text + "</div>")

  links = doc.css('a')
  link = links.detect{ |link| link.text == link_text }
  link = links.detect{ |link| link.attributes["href"].value.include?(link_text)} unless link
  path = link.attributes["href"].value
  visit URI::parse(path).request_uri
end
  def create_user(overrides={})
    default_attrs = {
        :password => 'password',
        :password_confirmation => 'password',
        :getting_started => false
    }

    user = FactoryGirl.create(:user, default_attrs.merge(overrides))
    add_standard_aspects(user)
    user
  end
  def add_standard_aspects(user)
    user.aspects.create(:name => "Besties")
    user.aspects.create(:name => "Unicorns")
  end
Given /^(?:|I )am on (.+)$/ do |page_name|
  navigate_to(page_name)
end
Then /^(?:|I )should see (\".+?\"[\s]*)(?:[\s]+within[\s]* "([^"]*)")?$/ do |vars, selector|
  vars.scan(/"([^"]+?)"/).flatten.each do |text|
    with_scope(selector) do
      current_scope.should have_content(text)
    end
  end
end
Then /^(?:|I )should be on (.+)$/ do |page_name|
  current_path = URI.parse(current_url).path
  current_path.should == path_to(page_name)
end
  def path_to(page_name)
    case page_name
      when /^person_photos page$/
         person_photos_path(@me.person)
      when /^the home(?: )?page$/
        stream_path
      when /^step (\d)$/
        if $1.to_i == 1
          getting_started_path
        else
          getting_started_path(:step => $1)
        end
      when /^the tag page for "([^\"]*)"$/
        tag_path($1)
      when /^its ([\w ]+) page$/
        send("#{$1.gsub(/\W+/, '_')}_path", @it)
      when /^the ([\w ]+) page$/
        send("#{$1.gsub(/\W+/, '_')}_path")
      when /^my edit profile page$/
        edit_profile_path
      when /^my profile page$/
        person_path(@me.person)
      when /^my acceptance form page$/
        invite_code_path(InvitationCode.first)
      when /^the requestors profile$/
        person_path(Request.where(:recipient_id => @me.person.id).first.sender)
      when /^"([^\"]*)"'s page$/
        p = User.find_by_email($1).person
        { path: person_path(p),
          # '.diaspora_handle' on desktop, '.description' on mobile
          special_elem: { selector: '.diaspora_handle, .description', text: p.diaspora_handle }
        }
      when /^my account settings page$/
        edit_user_path
      when /^my new profile page$/
        person_path(@me.person,  :ex => true)
      when /^the new stream$/
        stream_path(:ex => true)
      when /^forgot password page$/
          new_user_password_path
      when /^"(\/.*)"/
        $1
      else
        raise "Can't find mapping from \"#{page_name}\" to a path."
    end
  end
  def navigate_to(page_name)
    path = path_to(page_name)
    unless path.is_a?(Hash)
      visit(path)
    else
      visit(path[:path])
      await_elem = path[:special_elem]
      find(await_elem.delete(:selector), await_elem)
    end
  end
  def with_scope(locator)
    if locator
      within(locator, match: :first) do
        yield
      end
    else
      yield
    end
  end
  def path_to(page_name)
    case page_name
      when /^person_photos page$/
         person_photos_path(@me.person)
      when /^the home(?: )?page$/
        stream_path
      when /^step (\d)$/
        if $1.to_i == 1
          getting_started_path
        else
          getting_started_path(:step => $1)
        end
      when /^the tag page for "([^\"]*)"$/
        tag_path($1)
      when /^its ([\w ]+) page$/
        send("#{$1.gsub(/\W+/, '_')}_path", @it)
      when /^the ([\w ]+) page$/
        send("#{$1.gsub(/\W+/, '_')}_path")
      when /^my edit profile page$/
        edit_profile_path
      when /^my profile page$/
        person_path(@me.person)
      when /^my acceptance form page$/
        invite_code_path(InvitationCode.first)
      when /^the requestors profile$/
        person_path(Request.where(:recipient_id => @me.person.id).first.sender)
      when /^"([^\"]*)"'s page$/
        p = User.find_by_email($1).person
        { path: person_path(p),
          # '.diaspora_handle' on desktop, '.description' on mobile
          special_elem: { selector: '.diaspora_handle, .description', text: p.diaspora_handle }
        }
      when /^my account settings page$/
        edit_user_path
      when /^my new profile page$/
        person_path(@me.person,  :ex => true)
      when /^the new stream$/
        stream_path(:ex => true)
      when /^forgot password page$/
          new_user_password_path
      when /^"(\/.*)"/
        $1
      else
        raise "Can't find mapping from \"#{page_name}\" to a path."
    end
  end
When /^I fill out forgot password form with "([^"]*)"$/ do |email|
  fill_forgot_password_form(email)
end
When /^I submit forgot password form$/ do
  submit_forgot_password_form
end
When /^I fill out reset password form with "([^"]*)" and "([^"]*)"$/ do |new_pass,confirm_pass|
  fill_reset_password_form(new_pass, confirm_pass)
end
When /^I submit reset password form$/ do
  submit_reset_password_form
end
  def fill_forgot_password_form(email)
    fill_in 'user_email', :with => email
  end
  def submit_forgot_password_form
    find("#new_user input.button").click
  end
  def fill_reset_password_form(new_pass, confirm_pass)
    fill_in 'user_password', :with => new_pass
    fill_in 'user_password_confirmation', :with => confirm_pass
  end
  def submit_reset_password_form
    find(".button").click
  end
