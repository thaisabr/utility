Classes: 5
[name:Capybara, file:null, step:Given ]
[name:FactoryGirl, file:null, step:Given ]
[name:Time, file:null, step:Given ]
[name:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:aspects_controller, file:diaspora_diaspora/app/controllers/aspects_controller.rb, step:null]

Methods: 59
[name:activity, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:null]
[name:aspects, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:Given ]
[name:aspects, type:UserPresenter, file:diaspora_diaspora/app/presenters/user_presenter.rb, step:Given ]
[name:aspects, type:Aspect, file:diaspora_diaspora/lib/stream/aspect.rb, step:Given ]
[name:aspects, type:Base, file:diaspora_diaspora/lib/stream/base.rb, step:Given ]
[name:automatic_login, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:be_visible, type:Object, file:null, step:Then ]
[name:browser, type:Object, file:null, step:When ]
[name:button creation, type:Object, file:null, step:null]
[name:changelog_url, type:ApplicationHelper, file:diaspora_diaspora/app/helpers/application_helper.rb, step:null]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_publisher, type:PublishingCukeHelpers, file:diaspora_diaspora/features/support/publishing_cuke_helpers.rb, step:Given ]
[name:confirm_login, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:connect_users_with_aspects, type:Object, file:null, step:Given ]
[name:driver, type:Object, file:null, step:When ]
[name:driver, type:Object, file:null, step:Given ]
[name:edit, type:AspectsController, file:diaspora_diaspora/app/controllers/aspects_controller.rb, step:null]
[name:evaluate_script, type:Object, file:null, step:When ]
[name:evaluate_script, type:Object, file:null, step:Then ]
[name:execute_script, type:Object, file:null, step:Given ]
[name:execute_script, type:Object, file:null, step:When ]
[name:find, type:Object, file:null, step:When ]
[name:find, type:Object, file:null, step:Then ]
[name:find_by_email, type:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:first, type:Object, file:null, step:Given ]
[name:first_name, type:Object, file:null, step:Given ]
[name:followed_tags, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:null]
[name:has_content?, type:Object, file:null, step:Given ]
[name:id, type:Object, file:null, step:Given ]
[name:index, type:ContactsController, file:diaspora_diaspora/app/controllers/contacts_controller.rb, step:null]
[name:index, type:ServicesController, file:diaspora_diaspora/app/controllers/services_controller.rb, step:null]
[name:inspect, type:Object, file:null, step:Given ]
[name:last_name, type:Object, file:null, step:Given ]
[name:local_or_remote_person_path, type:PeopleHelper, file:diaspora_diaspora/app/helpers/people_helper.rb, step:null]
[name:magic_bookmarklet_link, type:ApplicationHelper, file:diaspora_diaspora/app/helpers/application_helper.rb, step:null]
[name:mentioned, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:null]
[name:multi, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:null]
[name:native, type:Object, file:null, step:When ]
[name:new, type:InvitationsController, file:diaspora_diaspora/app/controllers/invitations_controller.rb, step:null]
[name:new, type:AspectsController, file:diaspora_diaspora/app/controllers/aspects_controller.rb, step:null]
[name:page, type:Object, file:null, step:Given ]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:When ]
[name:post, type:Object, file:null, step:Given ]
[name:save, type:Server, file:diaspora_diaspora/features/support/server.rb, step:Given ]
[name:send_keys, type:Object, file:null, step:When ]
[name:show, type:TagsController, file:diaspora_diaspora/app/controllers/tags_controller.rb, step:null]
[name:spotlight, type:ContactsController, file:diaspora_diaspora/app/controllers/contacts_controller.rb, step:null]
[name:stream_element_numbers_content, type:PublishingCukeHelpers, file:diaspora_diaspora/features/support/publishing_cuke_helpers.rb, step:Then ]
[name:text, type:StatusMessage, file:diaspora_diaspora/app/models/status_message.rb, step:Then ]
[name:text, type:Publisher, file:diaspora_diaspora/lib/publisher.rb, step:Then ]
[name:to_i, type:Object, file:null, step:Then ]
[name:toggle_mobile, type:HomeController, file:diaspora_diaspora/app/controllers/home_controller.rb, step:null]
[name:username, type:Object, file:null, step:Given ]
[name:visit, type:Object, file:null, step:Given ]
[name:wait_for_ajax_to_finish, type:PublishingCukeHelpers, file:diaspora_diaspora/features/support/publishing_cuke_helpers.rb, step:When ]
[name:wait_until, type:Object, file:null, step:When ]
[name:wait_until, type:Object, file:null, step:Given ]
[name:where, type:Object, file:null, step:Given ]

Referenced pages: 37
diaspora_diaspora/app/views/aspects/_aspect_listings.haml
diaspora_diaspora/app/views/aspects/_aspect_stream.haml
diaspora_diaspora/app/views/aspects/_no_contacts_message.haml
diaspora_diaspora/app/views/aspects/_no_posts_message.haml
diaspora_diaspora/app/views/aspects/edit.html.haml
diaspora_diaspora/app/views/aspects/new.haml
diaspora_diaspora/app/views/community_spotlight/_user.html.haml
diaspora_diaspora/app/views/contacts/_aspect_listings.haml
diaspora_diaspora/app/views/contacts/_contact.html.haml
diaspora_diaspora/app/views/contacts/index.html.haml
diaspora_diaspora/app/views/contacts/index.mobile.haml
diaspora_diaspora/app/views/contacts/spotlight.haml
diaspora_diaspora/app/views/home/show.html.haml
diaspora_diaspora/app/views/home/show.mobile.haml
diaspora_diaspora/app/views/invitations/new.html.haml
diaspora_diaspora/app/views/invitations/new.mobile.haml
diaspora_diaspora/app/views/people/_index.html.haml
diaspora_diaspora/app/views/people/_person.html.haml
diaspora_diaspora/app/views/photos/_new_photo.haml
diaspora_diaspora/app/views/services/index.html.haml
diaspora_diaspora/app/views/shared/_add_remove_services.haml
diaspora_diaspora/app/views/shared/_contact_list.html.haml
diaspora_diaspora/app/views/shared/_contact_sidebar.html.haml
diaspora_diaspora/app/views/shared/_donatediaspora.html.haml
diaspora_diaspora/app/views/shared/_donatepod.html.haml
diaspora_diaspora/app/views/shared/_invitations.haml
diaspora_diaspora/app/views/shared/_links.haml
diaspora_diaspora/app/views/shared/_public_explain.haml
diaspora_diaspora/app/views/shared/_publisher.html.haml
diaspora_diaspora/app/views/shared/_right_sections.html.haml
diaspora_diaspora/app/views/shared/_settings_nav.haml
diaspora_diaspora/app/views/shared/_stream.haml
diaspora_diaspora/app/views/streams/main_stream.html.haml
diaspora_diaspora/app/views/streams/main_stream.mobile.haml
diaspora_diaspora/app/views/tags/_followed_tags_listings.haml
diaspora_diaspora/app/views/tags/show.haml
diaspora_diaspora/app/views/tags/show.mobile.haml

