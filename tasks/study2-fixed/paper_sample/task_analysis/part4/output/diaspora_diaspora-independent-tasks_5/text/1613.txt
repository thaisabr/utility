Feature: posting from the main page
In order to enlighten humanity for the good of society
As a rock star
I want to tell the world I am eating a yogurt
Background:
Given following users exist:
| username   |
| bob        |
| alice      |
And I sign in as "bob@bob.bob"
And a user with username "bob" is connected with "alice"
Given I have following aspects:
| PostingTo            |
| NotPostingThingsHere |
Scenario: back out of posting a photo-only post
Given I expand the publisher
And I have turned off jQuery effects
When I attach the file "spec/fixtures/bad_urls.txt" to "file" within "#file-upload"
And I confirm the alert
Then I should not see an uploaded image within the photo drop zone
When I attach the file "spec/fixtures/button.png" to hidden "file" within "#file-upload"
And I click to delete the first uploaded photo
Then I should not see an uploaded image within the photo drop zone
And I should not be able to submit the publisher
