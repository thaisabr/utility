Feature: Comments
In order to maintain comments
As an admin
I want a list of all comments and a delete-button for each comment
Background:
Given the following user records
| email            | name      | roles_mask | password         | password_confirmation | confirmed_at         |
| admin@iboard.cc  | admin     | 5          | thisisnotsecret  | thisisnotsecret       | 2010-01-01 00:00:00  |
| user@iboard.cc   | testmax   | 1          | thisisnotsecret  | thisisnotsecret       | 2010-01-01 00:00:00  |
| guest@iboard.cc  | guest     | 0          | thisisnotsecret  | thisisnotsecret       | 2010-01-01 00:00:00  |
| staff@iboard.cc  | staff     | 4          | thisisnotsecret  | thisisnotsecret       | 2010-01-01 00:00:00  |
And the following page records
| title  | body                 | show_in_menu |
| Page 1 | Lorem ipsum          | true         |
| Page 2 | Lirum Opsim          | false        |
And the following comment records for page "Page 1"
| name | email    | comment          |
| Andi | aa@bb.cc | My first Comment |
Scenario: Only moderators should see a the comments-menu-item
Given I am logged out
And I am on the home page
Then I should not see "Comments" within ".hmenu"
Scenario: Only moderators should see a list of all comments
Given I am logged out
And I am on the comments page
Then I should be on the home page
And I should see "not authorized"
