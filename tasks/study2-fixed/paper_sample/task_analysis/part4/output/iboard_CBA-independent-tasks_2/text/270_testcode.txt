Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_content(text)
    else
      assert page.has_content?(text)
    end
  end
end
Then /^(?:|I )should not see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_no_content(text)
    else
      assert page.has_no_content?(text)
    end
  end
end
Then /^(?:|I )should be on (.+)$/ do |page_name|
  current_path = URI.parse(current_url).path
  if current_path.respond_to? :should
    current_path.should == path_to(page_name)
  else
    assert_equal path_to(page_name), current_path
  end
end
Given /^the following (.+) records?$/ do |factory, table|
  eval "#{factory.camelize}.delete_all"
  table.hashes.each do |hash|  
    Factory(factory, hash)  
  end  
end
Given /^I am logged in as user "([^"]*)" with password "([^"]*)"$/ do |email, password|
  visit path_to('sign_in')
  fill_in('user_email', :with => email)
  fill_in('user_password', :with => password)
  click_button('Sign in')
  page.should have_content("Signed in successfully.")
end
Given /^I click on link "([^"]*)"$/ do |link|
  click_link(link)
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)
    
    case page_name
    when /the home page/
      '/'
    when /users page/
      '/registrations'
    when /registrations page/
      '/registrations'
    when /sign_in/
      '/users/sign_in'
    when /page path of "([^"]*)"/
      title = $1
      page = Page.where(:title => title).first
      "/pages/#{page._id}"
    when /permalink_path of "([^"]*)"/
      title = $1
      page = Page.where(:title => title).first
      "/p/#{page.link_to_title}"
    when /edit roles page for "([^"]*)"/
      begin
        user = User.where(:name => $1).first
        "/users/#{user.id}/edit_roles"
      rescue Object => e
        raise "Can't find user #{user.name} / #{e.inspect}"
      end
    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "With the following path_components \"#{path_components.inspect}\"\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
