Classes: 5
[name:BlogsController, file:iboard_CBA/app/controllers/blogs_controller.rb, step:Then ]
[name:PagesController, file:iboard_CBA/app/controllers/pages_controller.rb, step:Then ]
[name:User, file:iboard_CBA/app/models/user.rb, step:Given ]
[name:UserNotification, file:iboard_CBA/app/models/user_notification.rb, step:Given ]
[name:UserNotification, file:iboard_CBA/test/factories/user_notification.rb, step:Given ]

Methods: 38
[name:Factory, type:Object, file:null, step:Given ]
[name:are_you_sure, type:Object, file:null, step:Then ]
[name:blog, type:Object, file:null, step:Then ]
[name:camelize, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:Given ]
[name:delete_cover_picture, type:PostingsController, file:iboard_CBA/app/controllers/postings_controller.rb, step:Then ]
[name:delete_cover_picture, type:PagesController, file:iboard_CBA/app/controllers/pages_controller.rb, step:Then ]
[name:delete_cover_picture, type:BlogsController, file:iboard_CBA/app/controllers/blogs_controller.rb, step:Then ]
[name:each, type:Object, file:null, step:Given ]
[name:edit, type:PagesController, file:iboard_CBA/app/controllers/pages_controller.rb, step:Then ]
[name:edit, type:PostingsController, file:iboard_CBA/app/controllers/postings_controller.rb, step:Then ]
[name:edit, type:BlogsController, file:iboard_CBA/app/controllers/blogs_controller.rb, step:Then ]
[name:eval, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:first, type:Object, file:null, step:Given ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:has_no_content?, type:Object, file:null, step:Then ]
[name:hashes, type:Object, file:null, step:Given ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:Given ]
[name:have_no_content, type:Object, file:null, step:Then ]
[name:index, type:HomeController, file:iboard_CBA/app/controllers/home_controller.rb, step:Then ]
[name:index, type:PagesController, file:iboard_CBA/app/controllers/pages_controller.rb, step:Then ]
[name:index, type:BlogsController, file:iboard_CBA/app/controllers/blogs_controller.rb, step:Then ]
[name:new, type:UserNotification, file:iboard_CBA/app/models/user_notification.rb, step:Given ]
[name:new, type:UserNotification, file:iboard_CBA/test/factories/user_notification.rb, step:Given ]
[name:new, type:PostingsController, file:iboard_CBA/app/controllers/postings_controller.rb, step:Then ]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:Given ]
[name:respond_to?, type:Object, file:null, step:Then ]
[name:save!, type:Object, file:null, step:Given ]
[name:show, type:BlogsController, file:iboard_CBA/app/controllers/blogs_controller.rb, step:Then ]
[name:show, type:PagesController, file:iboard_CBA/app/controllers/pages_controller.rb, step:Then ]
[name:show, type:PostingsController, file:iboard_CBA/app/controllers/postings_controller.rb, step:Then ]
[name:user_notifications, type:Object, file:null, step:Given ]
[name:where, type:User, file:iboard_CBA/app/models/user.rb, step:Given ]
[name:with_scope, type:WebSteps, file:iboard_CBA/features/step_definitions/web_steps.rb, step:Then ]
[name:within, type:Object, file:null, step:Then ]

Referenced pages: 28
iboard_CBA/app/views/attachments/_attachment_fields.html.erb
iboard_CBA/app/views/blogs/_blog.html.erb
iboard_CBA/app/views/blogs/_blog_intro.html.erb
iboard_CBA/app/views/blogs/_cover_picture.html.erb
iboard_CBA/app/views/blogs/_form.html.erb
iboard_CBA/app/views/blogs/_posting_header.html.erb
iboard_CBA/app/views/blogs/edit.html.erb
iboard_CBA/app/views/blogs/index.html.erb
iboard_CBA/app/views/blogs/new.html.erb
iboard_CBA/app/views/blogs/show.html.erb
iboard_CBA/app/views/comments/_comment_fields.html.erb
iboard_CBA/app/views/home/_interpreter_help.html.erb
iboard_CBA/app/views/home/_load_more.html.erb
iboard_CBA/app/views/home/index.html.erb
iboard_CBA/app/views/pages/_cover_picture.html.erb
iboard_CBA/app/views/pages/_form.html.erb
iboard_CBA/app/views/pages/_page.html.erb
iboard_CBA/app/views/pages/_page_intro.html.erb
iboard_CBA/app/views/pages/edit.html.erb
iboard_CBA/app/views/pages/index.html.erb
iboard_CBA/app/views/pages/new.html.erb
iboard_CBA/app/views/pages/show.html.erb
iboard_CBA/app/views/postings/_cover_picture.html.erb
iboard_CBA/app/views/postings/_form.html.erb
iboard_CBA/app/views/postings/_posting.html.erb
iboard_CBA/app/views/postings/edit.html.erb
iboard_CBA/app/views/postings/new.html.erb
iboard_CBA/app/views/postings/show.html.erb

