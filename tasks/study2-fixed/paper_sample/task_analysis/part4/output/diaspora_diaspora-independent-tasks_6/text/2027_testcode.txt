Given /^a user with email "([^\"]*)"$/ do |email|
  create_user(:email => email)
end
Given /^a nsfw user with email "([^\"]*)"$/ do |email|
  user = create_user(:email => email)
  user.profile.update_attributes(:nsfw => true)
end
Given /^a user with email "([^"]*)" is connected with "([^"]*)"$/ do |arg1, arg2|
  user1 = User.where(:email => arg1).first
  user2 = User.where(:email => arg2).first
  connect_users(user1, user1.aspects.where(:name => "Besties").first, user2, user2.aspects.where(:name => "Besties").first)
end
  def create_user(overrides={})
    default_attrs = {
        :password => 'password',
        :password_confirmation => 'password',
        :getting_started => false
    }

    user = Factory(:user, default_attrs.merge(overrides))
    add_standard_aspects(user)
    user
  end
  def add_standard_aspects(user)
    user.aspects.create(:name => "Besties")
    user.aspects.create(:name => "Unicorns")
  end
Given /^"([^"]*)" has a public post with text "([^"]*)"$/ do |email, text|
  user = User.find_by_email(email)
  user.post(:status_message, :text => text, :public => true, :to => user.aspects)
end
When /^I (?:sign|log) in as "([^"]*)"$/ do |email|
  @me = User.find_by_email(email)
  @me.password ||= 'password'
  step 'I am signed in'
end
When /^I toggle nsfw posts$/ do
  find(".toggle_nsfw_state").click
end
Then /^I should have (\d+) nsfw posts$/ do |num_posts|
  all(".nsfw-shield").size.should == num_posts.to_i
end
And /^I preemptively confirm the alert$/ do
  page.evaluate_script("window.confirm = function() { return true; }")
end
When /^I wait for the ajax to finish$/ do
  wait_for_ajax_to_finish
end
  def wait_for_ajax_to_finish(wait_time=30)
    wait_until(wait_time) do
      evaluate_script("$.active") == 0
    end
  end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
Then /^(?:|I )should not see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_no_content(text)
    else
      assert page.has_no_content?(text)
    end
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
Given /^(?:I am signed in|I sign in)$/ do
  step %(I try to sign in)
  wait_until { page.has_content?("#{@me.first_name} #{@me.last_name}") }
end
When /^I try to sign in$/ do
  @me ||= Factory(:user_with_aspect, :getting_started => false)
  page.driver.visit(new_integration_sessions_path(:user_id => @me.id))
  step %(I press "Login")
  # To save time as compared to:
  #step %(I go to the new user session page)
  #step %(I fill in "Username" with "#{@me.username}")
  #step %(I fill in "Password" with "#{@me.password}")
  #step %(I press "Sign in")
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    silence_warnings { 
      sleep 1 if button == "Share"
      click_button(button) } # ruby 1.9 produces a warning about UTF8 from rack-util
  end
end
