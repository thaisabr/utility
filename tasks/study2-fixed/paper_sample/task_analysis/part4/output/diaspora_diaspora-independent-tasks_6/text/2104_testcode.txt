Given /^a user with email "([^\"]*)"$/ do |email|
  create_user(:email => email)
end
Given /^a user with username "([^\"]*)"$/ do |username|
  create_user(:email => username + "@" + username + '.' + username, :username => username)
end
Given /^a user named "([^\"]*)" with email "([^\"]*)"$/ do |name, email|
  first, last = name.split
  user = create_user(:email => email, :username => "#{first}_#{last}")
  user.profile.update_attributes!(:first_name => first, :last_name => last) if first
end
Given /^a user with email "([^"]*)" is connected with "([^"]*)"$/ do |arg1, arg2|
  user1 = User.where(:email => arg1).first
  user2 = User.where(:email => arg2).first
  connect_users(user1, user1.aspects.where(:name => "Besties").first, user2, user2.aspects.where(:name => "Besties").first)
end
Then /^I should have (\d+) email delivery$/ do |n|
  ActionMailer::Base.deliveries.length.should == n.to_i
end
  def create_user(overrides={})
    default_attrs = {
        :password => 'password',
        :password_confirmation => 'password',
        :getting_started => false
    }

    user = Factory(:user, default_attrs.merge!(overrides))
    add_standard_aspects(user)
    user
  end
  def add_standard_aspects(user)
    user.aspects.create(:name => "Besties")
    user.aspects.create(:name => "Unicorns")
  end
Given /^"([^"]*)" has a public post with text "([^"]*)"$/ do |email, text|
  user = User.find_by_email(email)
  user.post(:status_message, :text => text, :public => true, :to => user.aspects)
end
Then /^I should see "([^"]*)" as the first post in my stream$/ do |text|
  first_post_text.should include(text)
end
  def first_post_text
    find('.stream_element:first .post-content').text()
  end
When /^I (?:sign|log) in as "([^"]*)"$/ do |email|
  @me = User.find_by_email(email)
  @me.password ||= 'password'
  step 'I am signed in'
end
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    silence_warnings { 
      sleep 1 if button == "Share"
      click_button(button) } # ruby 1.9 produces a warning about UTF8 from rack-util
  end
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
When /^(?:|I )fill in "([^"]*)" with "([^"]*)"(?: within "([^"]*)")?$/ do |field, value, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_content(text)
    else
      assert page.has_content?(text)
    end
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)
    case page_name
      when /^the home(?: )?page$/
        stream_path
      when /^step (\d)$/
        if $1.to_i == 1
          getting_started_path
        else
          getting_started_path(:step => $1)
        end
      when /^the tag page for "([^\"]*)"$/
        tag_path($1)
      when /^its ([\w ]+) page$/
        send("#{$1.gsub(/\W+/, '_')}_path", @it)
      when /^the ([\w ]+) page$/
        send("#{$1.gsub(/\W+/, '_')}_path")
      when /^my edit profile page$/
        edit_profile_path
      when /^my profile page$/
        person_path(@me.person)
      when /^my acceptance form page$/
        invite_code_path(InvitationCode.first)
      when /^the requestors profile$/
        person_path(Request.where(:recipient_id => @me.person.id).first.sender)
      when /^"([^\"]*)"'s page$/
        person_path(User.find_by_email($1).person)
      when /^my account settings page$/
        edit_user_path
      when /^"(\/.*)"/
        $1
      else
        raise "Can't find mapping from \"#{page_name}\" to a path."
    end
  end
When /^I focus the comment field$/ do
  focus_comment_box
end
  def focus_comment_box
    find("a.focus_comment_textarea").click
  end
When /^(.*) in the header$/ do |action|
  within('header') do
    step action
  end
end
Then /^the notification dropdown should be visible$/ do
  find(:css, "#notification_dropdown").should be_visible
end
When /^I trumpet$/ do
  visit new_post_path
end
When /^I write "([^"]*)"$/ do |text|
  fill_in 'text', :with => text
end
Then /I mention "([^"]*)"$/ do |text|
  fill_in_autocomplete('textarea.text', '@a')
  sleep(5)
  find("li.active").click
end
When /^I select "([^"]*)" in my aspects dropdown$/ do |title|
  within ".aspect_selector" do
    select_from_dropdown(title, aspects_dropdown)
  end
end
Then /^"([^"]*)" should be a (limited|public) post in my stream$/ do |post_text, scope|
  find_post_by_text(post_text).find(".post_scope").text.should =~ /#{scope}/i
end
When /^I upload a fixture picture with filename "([^"]*)"$/ do |file_name|
  upload_photo(file_name)
end
Then /^"([^"]*)" should have the "([^"]*)" picture$/ do |post_text, file_name|
  within find_post_by_text(post_text) do
    find_image_by_filename(file_name).should be_present
  end
end
When /^I go through the default composer$/ do
  go_to_framer
  finalize_frame
end
When /^I start the framing process$/ do
  go_to_framer
end
When /^I finalize my frame$/ do
  finalize_frame
end
Then /^"([^"]*)" should have (\d+) pictures$/ do |post_text, number_of_pictures|
  find_post_by_text(post_text).all(".photo_attachments img").size.should == number_of_pictures.to_i
end
Then /^I should see "([^"]*)" in the framer preview$/ do |post_text|
  within(find(".post")) { page.should have_content(post_text) }
end
When /^I select the mood "([^"]*)"$/ do |template_name|
  select template_name, :from => 'template'
end
Then /^the post's mood should (?:still |)be "([^"]*)"$/ do |template_name|
  assert_post_renders_with(template_name)
end
When /^"([^"]*)" should be in the post's picture viewer$/ do |file_name|
  within(".photo_viewer") do
    find_image_by_filename(file_name).should be_present
  end
end
def fill_in_autocomplete(selector, value)
  pending #make me work if yr board, investigate send_keys
  page.execute_script %Q{$('#{selector}').val('#{value}').keyup()}
end
def aspects_dropdown
  find(".dropdown-toggle")
end
def select_from_dropdown(option_text, dropdown)
  dropdown.click
  within ".dropdown-menu" do
    link = find("a:contains('#{option_text}')")
    link.should be_visible
    link.click
  end
  #assert dropdown text is link
end
def go_to_framer
  click_button "Next"
end
def finalize_frame
  click_button "done"
end
def assert_post_renders_with(template_name)
  find(".post")["data-template"].should == template_name.downcase
end
def find_image_by_filename(filename)
  find("img[src='#{@image_sources[filename]}']")
end
def upload_photo(file_name)
  orig_photo_count = all(".photos img").size

  within ".new_photo" do
    attach_file "photo[user_file]", Rails.root.join("spec", "fixtures", file_name)
    wait_until { all(".photos img").size == orig_photo_count + 1 }
  end

  store_image_filename(file_name)
end
  def find_post_by_text(text)
    find(".stream_element:contains('#{text}')")
  end
def store_image_filename(file_name)
  @image_sources ||= {}
  @image_sources[file_name] = all(".photos img").last["src"]
  @image_sources[file_name].should be_present
end
Then /^"([^"]*)" should be post (\d+)$/ do |post_text, position|
  find(".stream_element:nth-child(#{position}) .post-content").text.should == post_text
end
When /^I click the show page link for "([^"]*)"$/ do |post_text|
  within(find_post_by_text(post_text)) do
    find("time").click
  end
end
Given /^(?:I am signed in|I sign in)$/ do
  step %(I try to sign in)
  wait_until { page.has_content?("#{@me.first_name} #{@me.last_name}") }
end
When /^I try to sign in$/ do
  @me ||= Factory(:user_with_aspect, :getting_started => false)
  page.driver.visit(new_integration_sessions_path(:user_id => @me.id))
  step %(I press "Login")
  # To save time as compared to:
  #step %(I go to the new user session page)
  #step %(I fill in "Username" with "#{@me.username}")
  #step %(I fill in "Password" with "#{@me.password}")
  #step %(I press "Sign in")
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    silence_warnings { 
      sleep 1 if button == "Share"
      click_button(button) } # ruby 1.9 produces a warning about UTF8 from rack-util
  end
end
