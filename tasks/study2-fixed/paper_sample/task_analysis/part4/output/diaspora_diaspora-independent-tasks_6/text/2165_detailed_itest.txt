Classes: 2
[name:User, file:diaspora_diaspora/app/models/user.rb, step:When ]
[name:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]

Methods: 34
[name:aspects, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:Given ]
[name:aspects, type:Aspect, file:diaspora_diaspora/lib/stream/aspect.rb, step:Given ]
[name:aspects, type:Base, file:diaspora_diaspora/lib/stream/base.rb, step:Given ]
[name:click_link, type:Object, file:null, step:When ]
[name:create_user, type:Object, file:null, step:Given ]
[name:delete_all_visible_cookies, type:Object, file:null, step:Given ]
[name:delete_cookie, type:Object, file:null, step:Given ]
[name:find_by_email, type:User, file:diaspora_diaspora/app/models/user.rb, step:When ]
[name:find_by_email, type:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:first, type:Object, file:null, step:When ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:Then ]
[name:post, type:Object, file:null, step:Given ]
[name:posts, type:EvilQuery, file:diaspora_diaspora/lib/evil_query.rb, step:When ]
[name:posts, type:Activity, file:diaspora_diaspora/lib/stream/activity.rb, step:When ]
[name:posts, type:Aspect, file:diaspora_diaspora/lib/stream/aspect.rb, step:When ]
[name:posts, type:Base, file:diaspora_diaspora/lib/stream/base.rb, step:When ]
[name:posts, type:Comments, file:diaspora_diaspora/lib/stream/comments.rb, step:When ]
[name:posts, type:FollowedTag, file:diaspora_diaspora/lib/stream/followed_tag.rb, step:When ]
[name:posts, type:Likes, file:diaspora_diaspora/lib/stream/likes.rb, step:When ]
[name:posts, type:Mention, file:diaspora_diaspora/lib/stream/mention.rb, step:When ]
[name:posts, type:Multi, file:diaspora_diaspora/lib/stream/multi.rb, step:When ]
[name:posts, type:Person, file:diaspora_diaspora/lib/stream/person.rb, step:When ]
[name:posts, type:Public, file:diaspora_diaspora/lib/stream/public.rb, step:When ]
[name:posts, type:Tag, file:diaspora_diaspora/lib/stream/tag.rb, step:When ]
[name:profile, type:Object, file:null, step:Given ]
[name:respond_to?, type:Object, file:null, step:Then ]
[name:show, type:PostsController, file:diaspora_diaspora/app/controllers/posts_controller.rb, step:null]
[name:split, type:Object, file:null, step:Given ]
[name:update_attributes!, type:Object, file:null, step:Given ]
[name:with_scope, type:WebSteps, file:diaspora_diaspora/features/step_definitions/web_steps.rb, step:When ]
[name:with_scope, type:WebSteps, file:diaspora_diaspora/features/step_definitions/web_steps.rb, step:Then ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 2
diaspora_diaspora/app/views/posts/show.html.haml
diaspora_diaspora/app/views/posts/show.mobile.haml

