And /^I click on selector "([^"]*)"$/ do |selector|
  page.execute_script("$('#{selector}').click();")
end
Then /^(?:|I )should see a "([^\"]*)"(?: within "([^\"]*)")?$/ do |selector, scope_selector|
  with_scope(scope_selector) do
    page.has_css?(selector).should be_true
  end
end
Then /^(?:|I )should not see a "([^\"]*)"(?: within "([^\"]*)")?$/ do |selector, scope_selector|
  with_scope(scope_selector) do
    page.has_css?(selector, :visible => true).should be_false
  end
end
When /^I wait for the ajax to finish$/ do
  wait_for_ajax_to_finish
end
When /^I attach the file "([^\"]*)" to hidden element "([^\"]*)"(?: within "([^\"]*)")?$/ do |path, field, selector|
  page.execute_script <<-JS
    $("#{selector || 'body'}").find("input[name=#{field}]").css({opacity: 1});
  JS

  if selector
    step "I attach the file \"#{Rails.root.join(path).to_s}\" to \"#{field}\" within \"#{selector}\""
  else
    step "I attach the file \"#{Rails.root.join(path).to_s}\" to \"#{field}\""
  end

  page.execute_script <<-JS
    $("#{selector || 'body'}").find("input[name=#{field}]").css({opacity: 0});
  JS
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def wait_for_ajax_to_finish(wait_time=30)
    wait_until(wait_time) do
      evaluate_script("$.active") == 0
    end
  end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    silence_warnings { 
      sleep 1 if button == "Share"
      click_button(button) } # ruby 1.9 produces a warning about UTF8 from rack-util
  end
end
Then /^(?:|I )should see (\".+?\"[\s]*)(?:[\s]+within[\s]* "([^"]*)")?$/ do |vars,selector|
  vars.scan(/"([^"]+?)"/).flatten.each do |text|
    with_scope(selector) do
      if page.respond_to? :should
        page.should have_content(text)
      else
        assert page.has_content?(text)
      end
    end
  end
end
Given /^a user with username "([^\"]*)"$/ do |username|
  create_user(:email => username + "@" + username + '.' + username, :username => username)
end
When /^I (?:sign|log) in as "([^"]*)"$/ do |email|
  @me = User.find_by_email(email)
  @me.password ||= 'password'
  automatic_login
  confirm_login
end
  def automatic_login
    @me ||= FactoryGirl.create(:user_with_aspect, :getting_started => false)
    page.driver.visit(new_integration_sessions_path(:user_id => @me.id))
    click_button "Login"
  end
  def confirm_login
    wait_until { page.has_content?("#{@me.first_name} #{@me.last_name}") }
  end
When /^I toggle the mobile view$/ do
  visit('/mobile/toggle')
end
When /^(?:|I )attach the file "([^"]*)" to "([^"]*)"(?: within "([^"]*)")?$/ do |path, field, selector|
  with_scope(selector) do
    attach_file(field, path)
  end
end
