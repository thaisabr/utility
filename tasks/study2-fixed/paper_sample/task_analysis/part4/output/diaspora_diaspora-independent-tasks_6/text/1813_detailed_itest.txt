Classes: 10
[name:FactoryGirl, file:null, step:Given ]
[name:InvitationCode, file:diaspora_diaspora/app/models/invitation_code.rb, step:When ]
[name:InvitationCode, file:diaspora_diaspora/app/models/invitation_code.rb, step:Then ]
[name:Request, file:diaspora_diaspora/app/models/request.rb, step:When ]
[name:Request, file:diaspora_diaspora/app/models/request.rb, step:Then ]
[name:URI, file:null, step:Then ]
[name:User, file:diaspora_diaspora/app/models/user.rb, step:When ]
[name:User, file:diaspora_diaspora/app/models/user.rb, step:Then ]
[name:aspects_controller, file:diaspora_diaspora/app/controllers/aspects_controller.rb, step:When ]
[name:aspects_controller, file:diaspora_diaspora/app/controllers/aspects_controller.rb, step:Then ]

Methods: 87
[name:assert_equal, type:Object, file:null, step:Then ]
[name:automatic_login, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:btn, type:Object, file:null, step:When ]
[name:btn, type:Object, file:null, step:Then ]
[name:button, type:Object, file:null, step:When ]
[name:button, type:Object, file:null, step:Then ]
[name:button creation, type:Object, file:null, step:When ]
[name:button creation, type:Object, file:null, step:Then ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:confirm_login, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:current_path, type:Object, file:null, step:Then ]
[name:current_url, type:Object, file:null, step:Then ]
[name:driver, type:Object, file:null, step:Given ]
[name:edit, type:ProfilesController, file:diaspora_diaspora/app/controllers/profiles_controller.rb, step:When ]
[name:edit, type:UsersController, file:diaspora_diaspora/app/controllers/users_controller.rb, step:When ]
[name:edit, type:AspectsController, file:diaspora_diaspora/app/controllers/aspects_controller.rb, step:When ]
[name:edit, type:ProfilesController, file:diaspora_diaspora/app/controllers/profiles_controller.rb, step:Then ]
[name:edit, type:UsersController, file:diaspora_diaspora/app/controllers/users_controller.rb, step:Then ]
[name:edit, type:AspectsController, file:diaspora_diaspora/app/controllers/aspects_controller.rb, step:Then ]
[name:evaluate_script, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find, type:Object, file:null, step:Then ]
[name:find_by_email, type:User, file:diaspora_diaspora/app/models/user.rb, step:When ]
[name:find_by_email, type:User, file:diaspora_diaspora/app/models/user.rb, step:Then ]
[name:first, type:InvitationCode, file:diaspora_diaspora/app/models/invitation_code.rb, step:When ]
[name:first, type:Object, file:null, step:When ]
[name:first, type:InvitationCode, file:diaspora_diaspora/app/models/invitation_code.rb, step:Then ]
[name:first, type:Object, file:null, step:Then ]
[name:first_name, type:Object, file:null, step:Given ]
[name:flash_message, type:ApplicationCukeHelpers, file:diaspora_diaspora/features/support/application_cuke_helpers.rb, step:Then ]
[name:flash_message_containing?, type:ApplicationCukeHelpers, file:diaspora_diaspora/features/support/application_cuke_helpers.rb, step:Then ]
[name:followed_tags, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:When ]
[name:followed_tags, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:Then ]
[name:has_content?, type:Object, file:null, step:Given ]
[name:have_content, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:Given ]
[name:id, type:Object, file:null, step:When ]
[name:id, type:Object, file:null, step:Then ]
[name:index, type:ContactsController, file:diaspora_diaspora/app/controllers/contacts_controller.rb, step:When ]
[name:index, type:ServicesController, file:diaspora_diaspora/app/controllers/services_controller.rb, step:When ]
[name:index, type:ContactsController, file:diaspora_diaspora/app/controllers/contacts_controller.rb, step:Then ]
[name:index, type:ServicesController, file:diaspora_diaspora/app/controllers/services_controller.rb, step:Then ]
[name:last_name, type:Object, file:null, step:Given ]
[name:local_or_remote_person_path, type:PeopleHelper, file:diaspora_diaspora/app/helpers/people_helper.rb, step:When ]
[name:local_or_remote_person_path, type:PeopleHelper, file:diaspora_diaspora/app/helpers/people_helper.rb, step:Then ]
[name:login_as, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:When ]
[name:login_page, type:Paths, file:diaspora_diaspora/features/support/paths.rb, step:When ]
[name:manual_login, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:When ]
[name:multi, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:When ]
[name:multi, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:Then ]
[name:new, type:AspectsController, file:diaspora_diaspora/app/controllers/aspects_controller.rb, step:When ]
[name:new, type:AspectsController, file:diaspora_diaspora/app/controllers/aspects_controller.rb, step:Then ]
[name:page, type:Object, file:null, step:Given ]
[name:page, type:Object, file:null, step:When ]
[name:password, type:Object, file:null, step:When ]
[name:path, type:Object, file:null, step:Then ]
[name:path_to, type:Paths, file:diaspora_diaspora/features/support/paths.rb, step:When ]
[name:path_to, type:Paths, file:diaspora_diaspora/features/support/paths.rb, step:Then ]
[name:person, type:Object, file:null, step:When ]
[name:person, type:PostPresenter, file:diaspora_diaspora/app/presenters/post_presenter.rb, step:When ]
[name:person, type:Object, file:null, step:Then ]
[name:person, type:PostPresenter, file:diaspora_diaspora/app/presenters/post_presenter.rb, step:Then ]
[name:raise, type:Object, file:null, step:When ]
[name:raise, type:Object, file:null, step:Then ]
[name:respond_to?, type:Object, file:null, step:Then ]
[name:send, type:Object, file:null, step:When ]
[name:send, type:Object, file:null, step:Then ]
[name:sender, type:Object, file:null, step:When ]
[name:sender, type:Object, file:null, step:Then ]
[name:show, type:TagsController, file:diaspora_diaspora/app/controllers/tags_controller.rb, step:When ]
[name:show, type:InvitationCodesController, file:diaspora_diaspora/app/controllers/invitation_codes_controller.rb, step:When ]
[name:show, type:TagsController, file:diaspora_diaspora/app/controllers/tags_controller.rb, step:Then ]
[name:show, type:InvitationCodesController, file:diaspora_diaspora/app/controllers/invitation_codes_controller.rb, step:Then ]
[name:spotlight, type:ContactsController, file:diaspora_diaspora/app/controllers/contacts_controller.rb, step:When ]
[name:spotlight, type:ContactsController, file:diaspora_diaspora/app/controllers/contacts_controller.rb, step:Then ]
[name:toggle_mobile, type:HomeController, file:diaspora_diaspora/app/controllers/home_controller.rb, step:When ]
[name:toggle_mobile, type:HomeController, file:diaspora_diaspora/app/controllers/home_controller.rb, step:Then ]
[name:username, type:Object, file:null, step:When ]
[name:visit, type:Object, file:null, step:Given ]
[name:wait_until, type:Object, file:null, step:Given ]
[name:wait_until, type:Object, file:null, step:Then ]
[name:where, type:Request, file:diaspora_diaspora/app/models/request.rb, step:When ]
[name:where, type:Request, file:diaspora_diaspora/app/models/request.rb, step:Then ]
[name:with_scope, type:WebSteps, file:diaspora_diaspora/features/step_definitions/web_steps.rb, step:When ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 31
diaspora_diaspora/app/views/aspects/edit.html.haml
diaspora_diaspora/app/views/aspects/new.haml
diaspora_diaspora/app/views/community_spotlight/_user.html.haml
diaspora_diaspora/app/views/contacts/_aspect_listings.haml
diaspora_diaspora/app/views/contacts/_contact.html.haml
diaspora_diaspora/app/views/contacts/index.html.haml
diaspora_diaspora/app/views/contacts/index.mobile.haml
diaspora_diaspora/app/views/contacts/spotlight.haml
diaspora_diaspora/app/views/devise/passwords/new.haml
diaspora_diaspora/app/views/devise/passwords/new.mobile.haml
diaspora_diaspora/app/views/devise/shared/_links.haml
diaspora_diaspora/app/views/home/show.html.haml
diaspora_diaspora/app/views/home/show.mobile.haml
diaspora_diaspora/app/views/people/_index.html.haml
diaspora_diaspora/app/views/people/_person.html.haml
diaspora_diaspora/app/views/photos/_new_photo.haml
diaspora_diaspora/app/views/profiles/edit.haml
diaspora_diaspora/app/views/profiles/edit.mobile.haml
diaspora_diaspora/app/views/services/index.html.haml
diaspora_diaspora/app/views/shared/_add_remove_services.haml
diaspora_diaspora/app/views/shared/_contact_list.html.haml
diaspora_diaspora/app/views/shared/_contact_sidebar.html.haml
diaspora_diaspora/app/views/shared/_public_explain.haml
diaspora_diaspora/app/views/shared/_publisher.html.haml
diaspora_diaspora/app/views/shared/_settings_nav.haml
diaspora_diaspora/app/views/shared/_stream.haml
diaspora_diaspora/app/views/tags/_followed_tags_listings.haml
diaspora_diaspora/app/views/tags/show.haml
diaspora_diaspora/app/views/tags/show.mobile.haml
diaspora_diaspora/app/views/users/edit.html.haml
diaspora_diaspora/app/views/users/edit.mobile.haml

