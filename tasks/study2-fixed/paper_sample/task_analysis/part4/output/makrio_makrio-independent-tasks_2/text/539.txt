Feature: posting
In order to enlighten humanity for the good of society
As a rock star
I want to tell the world I am eating a yogurt
Background:
Given a user with username "bob"
And a user with username "alice"
When I sign in as "bob@bob.bob"
And a user with username "bob" is connected with "alice"
And I have an aspect called "PostTo"
And I have an aspect called "DidntPostTo"
And I have user with username "alice" in an aspect called "PostTo"
And I have user with username "alice" in an aspect called "DidntPostTo"
And I am on the home page
Scenario Outline: posting to one aspect from the profile page
Given I am on "alice@alice.alice"'s page
And I have turned off jQuery effects
And I click "Mention" button
And I expand the publisher in the modal window
And I append "I am eating a yogurt" to the publisher
And I press the aspect dropdown in the modal window
And I toggle the aspect "DidntPostTo" in the modal window
And I press "Share" in the modal window
And I am on the aspects page
And I follow "<aspect>" within "#aspect_nav"
Then I should <see> "I am eating a yogurt"
Examples:
| aspect      | see     |
| PostTo      | see     |
| DidntPostTo | not see |
