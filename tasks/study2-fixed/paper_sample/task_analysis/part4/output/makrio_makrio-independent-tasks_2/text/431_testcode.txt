When /^(.*) in the header$/ do |action|
  within('header') do
    When action
  end
end
And /^I expand the publisher$/ do
  page.execute_script('
    $("#publisher").removeClass("closed");
    $("#publisher").find("textarea").focus();
    ')
end
And /^I preemptively confirm the alert$/ do
  page.evaluate_script("window.confirm = function() { return true; }")
end
When /^I press the first "([^"]*)"(?: within "([^"]*)")?$/ do |link_selector, within_selector|
  with_scope(within_selector) do
    find(:css, link_selector).click
  end
end
Then /^(?:|I )should see a "([^\"]*)"(?: within "([^\"]*)")?$/ do |selector, scope_selector|
  with_scope(scope_selector) do
    page.has_css?(selector).should be_true
  end
end
When /^I wait for the ajax to finish$/ do
  wait_until(30) { evaluate_script("$.active") == 0 }
end
When /^I search for "([^\"]*)"$/ do |search_term|
  When "I fill in \"q\" with \"#{search_term}\""
  page.execute_script <<-JS
    var e = jQuery.Event("keypress");
    e.keyCode = 13;
    $("#q").trigger(e);
  JS
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    silence_warnings { 
      sleep 1 if button == "Share"
      click_button(button) } # ruby 1.9 produces a warning about UTF8 from rack-util
  end
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
When /^(?:|I )fill in "([^"]*)" with "([^"]*)"(?: within "([^"]*)")?$/ do |field, value, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_content(text)
    else
      assert page.has_content?(text)
    end
  end
end
Then /^(?:|I )should not see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_no_content(text)
    else
      assert page.has_no_content?(text)
    end
  end
end
Then /^the "([^"]*)" field(?: within "([^"]*)")? should contain "([^"]*)"$/ do |field, selector, value|
  with_scope(selector) do
    field = find_field(field)
    field_value = (field.tag_name == 'textarea') ? field.text : field.value
    if field_value.respond_to? :should
      field_value.should =~ /#{value}/
    else
      assert_match(/#{value}/, field_value)
    end
  end
end
Then /^(?:|I )should be on (.+)$/ do |page_name|
  current_path = URI.parse(current_url).path
  if current_path.respond_to? :should
    current_path.should == path_to(page_name)
  else
    assert_equal path_to(page_name), current_path
  end
end
  def path_to(page_name)
    case page_name
      when /^the home(?: )?page$/
        root_path
      when /^step (\d)$/
        if $1.to_i == 1
          getting_started_path
        else
          getting_started_path(:step => $1)
        end
      when /^the tag page for "([^\"]*)"$/
        tag_path($1)
      when /^its ([\w ]+) page$/
        send("#{$1.gsub(/\W+/, '_')}_path", @it)
      when /^the ([\w ]+) page$/
        send("#{$1.gsub(/\W+/, '_')}_path")
      when /^my edit profile page$/
        edit_profile_path
      when /^my profile page$/
        person_path(@me.person)
      when /^my acceptance form page$/
        accept_user_invitation_path(:invitation_token => @me.invitation_token)
      when /^the requestors profile$/
        person_path(Request.where(:recipient_id => @me.person.id).first.sender)
      when /^"([^\"]*)"'s page$/
        person_path(User.find_by_email($1).person)
      when /^my account settings page$/
        edit_user_path
      when /^the photo page for "([^\"]*)"'s latest post$/
        photo_path(User.find_by_email($1).posts.where(:type => "Photo").last)
      when /^the photo page for "([^\"]*)"'s post "([^\"]*)"$/
        photo_path(User.find_by_email($1).posts.find_by_text($2))
      when /^"(\/.*)"/
        $1
      else
        raise "Can't find mapping from \"#{page_name}\" to a path."
    end
  end
Given /^a user with email "([^\"]*)"$/ do |email|
  user = Factory(:user, :email => email, :password => 'password',
                 :password_confirmation => 'password', :getting_started => false)
  user.aspects.create(:name => "Besties")
  user.aspects.create(:name => "Unicorns")
end
Given /^a user named "([^\"]*)" with email "([^\"]*)"$/ do |name, email|
  first, last = name.split
  username = "#{first}_#{last}" if first
  user = Factory.create(:user, :email => email, :password => 'password', :username => "#{first}_#{last}",
                 :password_confirmation => 'password', :getting_started => false)

  user.profile.update_attributes!(:first_name => first, :last_name => last) if first
  user.aspects.create!(:name => "Besties")
  user.aspects.create!(:name => "Unicorns")
end
Given /^a user with email "([^"]*)" is connected with "([^"]*)"$/ do |arg1, arg2|
  user1 = User.where(:email => arg1).first
  user2 = User.where(:email => arg2).first
  connect_users(user1, user1.aspects.where(:name => "Besties").first, user2, user2.aspects.where(:name => "Besties").first)
end
Given /^there is a user "([^\"]*)" who's tagged "([^\"]*)"$/ do |full_name, tag|
  username = full_name.gsub(/\W/, "").underscore
  Given "a user named \"#{full_name}\" with email \"#{username}@example.com\""
  user = User.find_by_username(username)
  user.profile.tag_string = tag
  user.profile.build_tags
  user.profile.save!
end
Then /^I should have (\d) contacts? in "([^"]*)"$/ do |n_contacts, aspect_name|
  @me.aspects.where(:name => aspect_name).first.contacts.count.should == n_contacts.to_i
end
When /^I (?:add|remove) the person (?:to|from) my "([^\"]*)" aspect$/ do |aspect_name|
  steps %Q{
    And I press the first ".toggle.button"
    And I click on selector ".dropdown.active .dropdown_list li[data-aspect_id=#{@me.aspects.where(:name => aspect_name).first.id}]"
    And I press the first ".toggle.button"
  }
end
And /^I follow the "([^\"]*)" link from the last sent email$/ do |link_text|
  email_text = Devise.mailer.deliveries.first.body.to_s
  email_text = Devise.mailer.deliveries.first.html_part.body.raw_source if email_text.blank?
  doc = Nokogiri(email_text)
  links = doc.css('a')
  link = links.detect{ |link| link.text == link_text }
  link = links.detect{ |link| link.attributes["href"].value.include?(link_text)} unless link
  path = link.attributes["href"].value
  visit URI::parse(path).request_uri
end
Then /^my "([^\"]*)" should be "([^\"]*)"$/ do |field, value|
  @me.reload.send(field).should == value
end
Given /^I have (\d+) contacts$/ do |n|
  count = n.to_i - @me.contacts.count

  people = []
  contacts = []
  aspect_memberships = []

  count.times do
    person = Factory.create(:person)
    people << person
  end

  people.each do |person|
    contacts << Contact.new(:person_id => person.id, :user_id => @me.id, :sharing => true, :receiving => true)
  end
  Contact.import(contacts)
  contacts = @me.contacts.limit(n.to_i)

  aspect_id = @me.aspects.length == 1 ? @me.aspects.first.id : @me.aspects.where(:name => "Besties").first.id
  contacts.each do |contact|
    aspect_memberships << AspectMembership.new(:contact_id => contact.id, :aspect_id => aspect_id)
  end
  AspectMembership.import(aspect_memberships)
end
Given /^(?:I am signed in|I sign in)$/ do
  When %(I try to sign in)
  wait_until { page.has_content?("#{@me.first_name} #{@me.last_name}") }
end
When /^I try to sign in manually$/ do
  When %(I go to the new user session page)
  When %(I fill in "Username" with "#{@me.username}")
  When %(I fill in "Password" with "#{@me.password}")
  When %(I press "Sign in")
end
When /^I sign in as "([^"]*)"$/ do |email|
  @me = User.find_by_email(email)
  @me.password ||= 'password'
  Given 'I am signed in'
  And 'I wait for the ajax to finish'
end
When /^I sign in with password "([^"]*)"$/ do |password|
  @me.password = password
  Given 'I am signed in'
end
When /^I put in my password in "([^"]*)"$/ do |field|
 When %(I fill in "#{field}" with "#{@me.password}")
end
When /^I (?:log|sign) out$/ do
  When 'I click on my name in the header'
  And 'I follow "Log out"'
end
Given /^I have several oEmbed data in cache$/ do
  scenarios = {
    "photo" => {
      "oembed_data" => {
        "trusted_endpoint_url" => "__!SPOOFED!__",
        "version" => "1.0",
        "type" => "photo",
        "title" => "ZB8T0193",
        "width" => "240",
        "height" => "160",
        "url" => "http://farm4.static.flickr.com/3123/2341623661_7c99f48bbf_m.jpg"
      },
      "link_url" => 'http://www.flickr.com/photos/bees/2341623661',
      "oembed_get_request" => "http://www.flickr.com/services/oembed/?format=json&frame=1&iframe=1&maxheight=420&maxwidth=420&url=http://www.flickr.com/photos/bees/2341623661",
    },

    "unsupported" => {
      "oembed_data" => {},
      "oembed_get_request" => 'http://www.we-do-not-support-oembed.com/index.html',
      "link_url" => 'http://www.we-do-not-support-oembed.com/index.html',
      "discovery_data" => 'no LINK tag!',
    },

    "secure_video" => {
      "oembed_data" => {
        "version" => "1.0",
        "type" => "video",
        "width" => 425,
        "height" => 344,
        "title" => "Amazing Nintendo Facts",
        "html" => "<object width=\"425\" height=\"344\">
            <param name=\"movie\" value=\"http://www.youtube.com/v/M3r2XDceM6A&fs=1\"></param>
            <param name=\"allowFullScreen\" value=\"true\"></param>
            <param name=\"allowscriptaccess\" value=\"always\"></param>
            <embed src=\"http://www.youtube.com/v/M3r2XDceM6A&fs=1\"
	            type=\"application/x-shockwave-flash\" width=\"425\" height=\"344\"
	            allowscriptaccess=\"always\" allowfullscreen=\"true\"></embed>
          </object>",
      },
      "link_url" => "http://youtube.com/watch?v=M3r2XDceM6A&format=json",
      "oembed_get_request" => "http://www.youtube.com/oembed?format=json&frame=1&iframe=1&maxheight=420&maxwidth=420&url=http://youtube.com/watch?v=M3r2XDceM6A",
    },

    "unsecure_video" => {
      "oembed_data" => {
        "version" => "1.0",
        "type" => "video",
        "title" => "This is a video from an unsecure source",
        "html" => "<object width=\"425\" height=\"344\">
            <param name=\"movie\" value=\"http://www.youtube.com/v/M3r2XDceM6A&fs=1\"></param>
            <param name=\"allowFullScreen\" value=\"true\"></param>
            <param name=\"allowscriptaccess\" value=\"always\"></param>
            <embed src=\"http://www.youtube.com/v/M3r2XDceM6A&fs=1\"
	            type=\"application/x-shockwave-flash\" width=\"425\" height=\"344\"
	            allowscriptaccess=\"always\" allowfullscreen=\"true\"></embed>
          </object>",
      },
      "link_url" => "http://myrichtube.com/watch?v=M3r2XDceM6A&format=json",
      "discovery_data" => '<link rel="alternate" type="application/json+oembed" href="http://www.mytube.com/oembed?format=json&frame=1&iframe=1&maxheight=420&maxwidth=420&url=http://mytube.com/watch?v=M3r2XDceM6A" />',
      "oembed_get_request" => "http://www.mytube.com/oembed?format=json&frame=1&iframe=1&maxheight=420&maxwidth=420&url=http://mytube.com/watch?v=M3r2XDceM6A",
    },

    "secure_rich" => {
      "oembed_data" => {
        "version" => "1.0",
        "type" => "rich",
        "width" => 425,
        "height" => 344,
        "title" => "Amazing Nintendo Facts",
        "html" => "<object width=\"425\" height=\"344\">
            <param name=\"movie\" value=\"http://www.youtube.com/v/M3r2XDceM6A&fs=1\"></param>
            <param name=\"allowFullScreen\" value=\"true\"></param>
            <param name=\"allowscriptaccess\" value=\"always\"></param>
            <embed src=\"http://www.youtube.com/v/M3r2XDceM6A&fs=1\"
	            type=\"application/x-shockwave-flash\" width=\"425\" height=\"344\"
	            allowscriptaccess=\"always\" allowfullscreen=\"true\"></embed>
          </object>",
      },
      "link_url" => "http://yourichtube.com/watch?v=M3r2XDceM6A&format=json",
      "oembed_get_request" => "http://www.youtube.com/oembed?format=json&frame=1&iframe=1&maxheight=420&maxwidth=420&url=http://youtube.com/watch?v=M3r2XDceM6A",
    },

    "unsecure_rich" => {
      "oembed_data" => {
        "version" => "1.0",
        "type" => "rich",
        "title" => "This is a video from an unsecure source",
        "html" => "<object width=\"425\" height=\"344\">
            <param name=\"movie\" value=\"http://www.youtube.com/v/M3r2XDceM6A&fs=1\"></param>
            <param name=\"allowFullScreen\" value=\"true\"></param>
            <param name=\"allowscriptaccess\" value=\"always\"></param>
            <embed src=\"http://www.youtube.com/v/M3r2XDceM6A&fs=1\"
	            type=\"application/x-shockwave-flash\" width=\"425\" height=\"344\"
	            allowscriptaccess=\"always\" allowfullscreen=\"true\"></embed>
          </object>",
      },
      "link_url" => "http://mytube.com/watch?v=M3r2XDceM6A&format=json",
      "discovery_data" => '<link rel="alternate" type="application/json+oembed" href="http://www.mytube.com/oembed?format=json&frame=1&iframe=1&maxheight=420&maxwidth=420&url=http://mytube.com/watch?v=M3r2XDceM6A" />',
      "oembed_get_request" => "http://www.mytube.com/oembed?format=json&frame=1&iframe=1&maxheight=420&maxwidth=420&url=http://mytube.com/watch?v=M3r2XDceM6A",
    },
  }
  scenarios.each do |type, data|
    unless type=='unsupported'
      url = data['oembed_get_request'].split('?')[0]
      store_data = data['oembed_data'].merge('trusted_endpoint_url' => url)
      OEmbedCache.new(:url => data['link_url'], :data => store_data.to_json);
    end
  end
end
Then /^I should see a video player$/ do
  page.has_css?('object')
end
Then /^I should not see a video player$/ do
  page.has_no_css?('object')
end
And /^I click on selector "([^"]*)"$/ do |selector|
  page.execute_script("$('#{selector}').click();")
end
When /^I press the first "([^"]*)"(?: within "([^"]*)")?$/ do |link_selector, within_selector|
  with_scope(within_selector) do
    find(:css, link_selector).click
  end
end
