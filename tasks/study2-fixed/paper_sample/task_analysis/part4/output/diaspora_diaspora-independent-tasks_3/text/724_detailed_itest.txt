Classes: 8
[name:FactoryGirl, file:null, step:Given ]
[name:Hash, file:null, step:When ]
[name:InvitationCode, file:diaspora_diaspora/app/models/invitation_code.rb, step:When ]
[name:Regexp, file:null, step:When ]
[name:Request, file:diaspora_diaspora/lib/diaspora/federated/request.rb, step:When ]
[name:User, file:diaspora_diaspora/app/models/user.rb, step:When ]
[name:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:aspects_controller, file:diaspora_diaspora/app/controllers/aspects_controller.rb, step:When ]

Methods: 97
[name:accept, type:Object, file:null, step:When ]
[name:activity, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:When ]
[name:add_standard_aspects, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:alert, type:Object, file:null, step:When ]
[name:aspect_ids, type:Aspect, file:diaspora_diaspora/lib/stream/aspect.rb, step:Given ]
[name:aspect_ids, type:Base, file:diaspora_diaspora/lib/stream/base.rb, step:Given ]
[name:aspects, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:Given ]
[name:aspects, type:UserPresenter, file:diaspora_diaspora/app/presenters/user_presenter.rb, step:Given ]
[name:aspects, type:Aspect, file:diaspora_diaspora/lib/stream/aspect.rb, step:Given ]
[name:aspects, type:Base, file:diaspora_diaspora/lib/stream/base.rb, step:Given ]
[name:aspects, type:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:assert_selector, type:Object, file:null, step:Then ]
[name:automatic_login, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:bookmarklet_code, type:ApplicationHelper, file:diaspora_diaspora/app/helpers/application_helper.rb, step:When ]
[name:browser, type:Object, file:null, step:When ]
[name:btn btn-block btn-primary, type:Object, file:null, step:When ]
[name:btn btn-primary, type:Object, file:null, step:When ]
[name:changelog_url, type:ApplicationHelper, file:diaspora_diaspora/app/helpers/application_helper.rb, step:When ]
[name:click, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:Given ]
[name:confirm_login, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:connect_users, type:Object, file:null, step:Given ]
[name:create, type:Object, file:null, step:Given ]
[name:create_user, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:create_user, type:Object, file:null, step:Given ]
[name:delete, type:Object, file:null, step:When ]
[name:diaspora_handle, type:Object, file:null, step:When ]
[name:diaspora_handle, type:Comment, file:diaspora_diaspora/app/models/comment.rb, step:Given ]
[name:diaspora_handle, type:Conversation, file:diaspora_diaspora/app/models/conversation.rb, step:Given ]
[name:diaspora_handle, type:Message, file:diaspora_diaspora/app/models/message.rb, step:Given ]
[name:diaspora_handle, type:PollParticipation, file:diaspora_diaspora/app/models/poll_participation.rb, step:Given ]
[name:diaspora_handle, type:Profile, file:diaspora_diaspora/app/models/profile.rb, step:Given ]
[name:diaspora_handle, type:RelayableRetraction, file:diaspora_diaspora/lib/diaspora/federated/relayable_retraction.rb, step:Given ]
[name:diaspora_handle, type:Request, file:diaspora_diaspora/lib/diaspora/federated/request.rb, step:Given ]
[name:diaspora_handle, type:Shareable, file:diaspora_diaspora/lib/diaspora/federated/shareable.rb, step:Given ]
[name:diaspora_handle, type:SignedRetraction, file:diaspora_diaspora/lib/diaspora/federated/signed_retraction.rb, step:Given ]
[name:diaspora_handle, type:Relayable, file:diaspora_diaspora/lib/federated/relayable.rb, step:Given ]
[name:driver, type:Object, file:null, step:When ]
[name:each, type:Object, file:null, step:Given ]
[name:edit, type:ProfilesController, file:diaspora_diaspora/app/controllers/profiles_controller.rb, step:When ]
[name:edit, type:UsersController, file:diaspora_diaspora/app/controllers/admin/users_controller.rb, step:When ]
[name:edit, type:UsersController, file:diaspora_diaspora/app/controllers/users_controller.rb, step:When ]
[name:find, type:Object, file:null, step:When ]
[name:find, type:Object, file:null, step:Then ]
[name:find_by_email, type:User, file:diaspora_diaspora/app/models/user.rb, step:When ]
[name:find_by_email, type:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:first, type:InvitationCode, file:diaspora_diaspora/app/models/invitation_code.rb, step:When ]
[name:first, type:Object, file:null, step:When ]
[name:first, type:Object, file:null, step:Given ]
[name:first_name, type:Object, file:null, step:Given ]
[name:force_mobile, type:HomeController, file:diaspora_diaspora/app/controllers/home_controller.rb, step:When ]
[name:gsub, type:Object, file:null, step:When ]
[name:has_content?, type:Object, file:null, step:Given ]
[name:has_key?, type:Object, file:null, step:Given ]
[name:hashes, type:Object, file:null, step:Given ]
[name:have_content, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:When ]
[name:id, type:Object, file:null, step:Given ]
[name:index, type:PhotosController, file:diaspora_diaspora/app/controllers/photos_controller.rb, step:When ]
[name:index, type:ContactsController, file:diaspora_diaspora/app/controllers/contacts_controller.rb, step:When ]
[name:index, type:ServicesController, file:diaspora_diaspora/app/controllers/services_controller.rb, step:When ]
[name:index, type:TermsController, file:diaspora_diaspora/app/controllers/terms_controller.rb, step:When ]
[name:is_a?, type:Object, file:null, step:When ]
[name:last_name, type:Object, file:null, step:Given ]
[name:local_or_remote_person_path, type:PeopleHelper, file:diaspora_diaspora/app/helpers/people_helper.rb, step:When ]
[name:mentioned, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:When ]
[name:merge, type:Object, file:null, step:Given ]
[name:multi, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:When ]
[name:navigate_to, type:Paths, file:diaspora_diaspora/features/support/paths.rb, step:When ]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:When ]
[name:page, type:Object, file:null, step:Given ]
[name:path_to, type:Paths, file:diaspora_diaspora/features/support/paths.rb, step:When ]
[name:person, type:Object, file:null, step:When ]
[name:person, type:PostPresenter, file:diaspora_diaspora/app/presenters/post_presenter.rb, step:When ]
[name:person, type:PostPresenter, file:diaspora_diaspora/app/presenters/post_presenter.rb, step:Given ]
[name:post, type:Object, file:null, step:Given ]
[name:profile, type:Object, file:null, step:Given ]
[name:public, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:When ]
[name:public_send, type:Object, file:null, step:When ]
[name:raise, type:Object, file:null, step:When ]
[name:send, type:Object, file:null, step:When ]
[name:sender, type:Object, file:null, step:When ]
[name:show, type:TagsController, file:diaspora_diaspora/app/controllers/tags_controller.rb, step:When ]
[name:show, type:InvitationCodesController, file:diaspora_diaspora/app/controllers/invitation_codes_controller.rb, step:When ]
[name:show, type:PhotosController, file:diaspora_diaspora/app/controllers/photos_controller.rb, step:When ]
[name:split, type:Object, file:null, step:Given ]
[name:spotlight, type:ContactsController, file:diaspora_diaspora/app/controllers/contacts_controller.rb, step:When ]
[name:statistics, type:NodeInfoController, file:diaspora_diaspora/app/controllers/node_info_controller.rb, step:When ]
[name:stream_element_numbers_content, type:PublishingCukeHelpers, file:diaspora_diaspora/features/support/publishing_cuke_helpers.rb, step:Then ]
[name:switch_to, type:Object, file:null, step:When ]
[name:text, type:Photo, file:diaspora_diaspora/app/models/photo.rb, step:When ]
[name:toggle_mobile, type:HomeController, file:diaspora_diaspora/app/controllers/home_controller.rb, step:When ]
[name:update_attributes!, type:Object, file:null, step:Given ]
[name:where, type:Request, file:diaspora_diaspora/lib/diaspora/federated/request.rb, step:When ]
[name:where, type:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:where, type:Object, file:null, step:Given ]

Referenced pages: 47
diaspora_diaspora/app/views/aspects/_aspect_listings.haml
diaspora_diaspora/app/views/aspects/_aspect_stream.haml
diaspora_diaspora/app/views/aspects/_no_contacts_message.haml
diaspora_diaspora/app/views/aspects/_no_posts_message.haml
diaspora_diaspora/app/views/contacts/_aspect_listings.haml
diaspora_diaspora/app/views/contacts/_header.html.haml
diaspora_diaspora/app/views/contacts/_sidebar.html.haml
diaspora_diaspora/app/views/contacts/index.html.haml
diaspora_diaspora/app/views/contacts/index.mobile.haml
diaspora_diaspora/app/views/contacts/spotlight.haml
diaspora_diaspora/app/views/devise/passwords/new.haml
diaspora_diaspora/app/views/devise/passwords/new.mobile.haml
diaspora_diaspora/app/views/home/default.haml
diaspora_diaspora/app/views/home/show.haml
diaspora_diaspora/app/views/node_info/statistics.html.haml
diaspora_diaspora/app/views/node_info/statistics.mobile.haml
diaspora_diaspora/app/views/people/_index.html.haml
diaspora_diaspora/app/views/people/_person.html.haml
diaspora_diaspora/app/views/photos/_index.mobile.haml
diaspora_diaspora/app/views/photos/_new_profile_photo.haml
diaspora_diaspora/app/views/photos/edit.html.haml
diaspora_diaspora/app/views/photos/show.mobile.haml
diaspora_diaspora/app/views/profiles/_edit_private.haml
diaspora_diaspora/app/views/profiles/_edit_public.haml
diaspora_diaspora/app/views/profiles/edit.haml
diaspora_diaspora/app/views/profiles/edit.mobile.haml
diaspora_diaspora/app/views/publisher/_aspect_dropdown.html.haml
diaspora_diaspora/app/views/publisher/_publisher.html.haml
diaspora_diaspora/app/views/services/_add_remove_services.haml
diaspora_diaspora/app/views/services/index.html.haml
diaspora_diaspora/app/views/services/index.mobile.haml
diaspora_diaspora/app/views/shared/_donatepod.html.haml
diaspora_diaspora/app/views/shared/_invitations.haml
diaspora_diaspora/app/views/shared/_links.haml
diaspora_diaspora/app/views/shared/_public_explain.haml
diaspora_diaspora/app/views/shared/_right_sections.html.haml
diaspora_diaspora/app/views/shared/_settings_nav.haml
diaspora_diaspora/app/views/shared/_stream.haml
diaspora_diaspora/app/views/streams/main_stream.html.haml
diaspora_diaspora/app/views/streams/main_stream.mobile.haml
diaspora_diaspora/app/views/tags/_followed_tags_listings.haml
diaspora_diaspora/app/views/tags/show.haml
diaspora_diaspora/app/views/tags/show.mobile.haml
diaspora_diaspora/app/views/terms/default.haml
diaspora_diaspora/app/views/users/_edit.haml
diaspora_diaspora/app/views/users/edit.html.haml
diaspora_diaspora/app/views/users/edit.mobile.haml

