Given /^a submitted (publication|policy|news article|consultation|speech|international priority) "([^"]*)" exists$/ do |document_type, title|
  create("submitted_#{document_class(document_type).name.underscore}".to_sym, title: title)
end
When /^I publish (#{THE_DOCUMENT})$/ do |edition|
  visit_document_preview edition.title
  publish
end
Then /^I should see (#{THE_DOCUMENT})$/ do |edition|
  assert has_css?(record_css_selector(edition))
end
Then /^(#{THE_DOCUMENT}) should be visible to the public$/ do |edition|
  visit homepage
  case edition
  when Publication
    click_link "Publications"
  when NewsArticle, Speech
    click_link "News"
  when Consultation
    click_link "Consultations"
  when Policy
    visit policies_path
  when InternationalPriority
    visit international_priorities_path
  else
    raise "Don't know what to click on for #{edition.class.name}s"
  end
  assert page.has_css?(record_css_selector(edition), text: edition.title)
end
Then /^my attempt to publish "([^"]*)" should succeed$/ do |title|
  edition = Edition.latest_edition.find_by_title!(title)
  assert edition.published?
end
  def document_class(type)
    type = 'edition' if type == 'document'
    type.gsub(" ", "_").classify.constantize
  end
  def visit_document_preview(title, scope = :scoped)
    document = Edition.send(scope).find_by_title(title)
    visit admin_document_path(document)
  end
  def publish(options = {})
    if publishing_requires_change_note?
      if options[:minor_change]
        check "Minor change?"
      elsif !options[:without_change_note]
        fill_in "Change note", with: "Fixed some grammatical errors."
      end
    end
    click_button options[:force] ? "Force Publish" : "Publish"
    unless options[:ignore_errors]
      refute_flash_alerts_exist
    end
  end
  def homepage
    home_path
  end
  def publishing_requires_change_note?
    has_css?("textarea[name='document[change_note]']")
  end
  def refute_flash_alerts_exist
    refute has_css?(".flash.alert")
  end
  def admin_document_path(edition, *args)
    if edition.is_a?(Speech)
      admin_speech_path(edition, *args)
    else
      polymorphic_path([:admin, edition], *args)
    end
  end
Given /^a submitted policy titled "([^"]*)"$/ do |policy_title|
  create(:submitted_policy, title: policy_title)
end
When /^I reject the policy titled "([^"]*)"$/ do |policy_title|
  policy = Policy.find_by_title(policy_title)
  visit admin_policy_path(policy)
  click_button "Reject"
  fill_in "Remark", with: "reason-for-rejection"
  click_button "Submit remark"
end
Then /^the writers who worked on the policy titled "([^"]*)" should be emailed about the rejection$/ do |policy_title|
  policy = Policy.find_by_title(policy_title)
  policy.authors.each do |writer|
    assert_equal 1, unread_emails_for(writer.email).size
    assert_match /The policy #{policy_title} was rejected by/, unread_emails_for(writer.email).first.subject
  end
end
Then /^the writers who worked on the policy titled "([^"]*)" should be emailed about the publication$/ do |policy_title|
  policy = Policy.find_by_title(policy_title)
  policy.authors.each do |writer|
    assert_equal 1, unread_emails_for(writer.email).size
    assert_match /The policy #{policy_title} has been published/, unread_emails_for(writer.email).first.subject
  end
end
  def unread_emails_for(address)
    emails_for(address) - read_emails_for(address)
  end
  def emails_for(address)
    ActionMailer::Base.deliveries.select do |d|
      d.to.include?(address)
    end
  end
  def read_emails_for(address)
    read_emails[address] ||= []
  end
  def read_emails
    @read_emails ||= {}
  end
Given /^I am (?:a|an) (writer|editor|admin)(?: called "([^"]*)")?$/ do |role, name|
  user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Editor"))
  when "admin"
    create(:user)
  end
  login_as user
end
  def login_as(user)
    GDS::SSO.test_user = user
    PaperTrail.whodunnit = user
  end
