When /^I draft a new policy "([^"]*)"$/ do |title|
  begin_drafting_policy title: title
  click_button "Save"
end
  def begin_drafting_policy(options)
    begin_drafting_document(options.merge(type: "policy"))
    fill_in "Summary", with: options[:summary] || "Policy summary"
  end
  def begin_drafting_document(options)
    visit admin_editions_path
    click_link "Create #{options[:type].titleize}"
    fill_in "Title", with: options[:title]
    fill_in "Body", with: options[:body] || "Any old iron"
  end
When /^I force publish (#{THE_DOCUMENT})$/ do |edition|
  visit_document_preview edition.title, :draft
  click_link "Edit"
  fill_in_change_note_if_required
  click_button "Save"
  publish(force: true)
end
  def visit_document_preview(title, scope = :scoped)
    document = Edition.send(scope).find_by_title(title)
    visit admin_edition_path(document)
  end
  def fill_in_change_note_if_required
    if has_css?("textarea[name='edition[change_note]']")
      fill_in "Change note", with: "changes"
    end
  end
  def publish(options = {})
    click_button options[:force] ? "Force Publish" : "Publish"
    unless options[:ignore_errors]
      refute_flash_alerts_exist
    end
  end
  def refute_flash_alerts_exist
    refute has_css?(".flash.alert")
  end
  def admin_edition_path(edition, *args)
    polymorphic_path([:admin, edition], *args)
  end
When /^another editor retrospectively approves the "([^"]*)" policy$/ do |policy_title|
  user = create(:departmental_editor, name: "Other editor")
  login_as user
  visit admin_editions_path(state: :published)
  click_link policy_title
  click_button "Looks good"
end
Then /^the "([^"]*)" policy should not be flagged as force\-published any more$/ do |policy_title|
  visit admin_editions_path(state: :published)
  policy = Policy.find_by_title(policy_title)
  assert page.has_css? record_css_selector(policy)
  assert page.has_no_css?(record_css_selector(policy) + ".force_published")
end
  def login_as(user)
    GDS::SSO.test_user = user
    PaperTrail.whodunnit = user
  end
Given /^I am (?:a|an) (writer|editor|admin)(?: called "([^"]*)")?$/ do |role, name|
  user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Editor"))
  when "admin"
    create(:user)
  end
  login_as user
end
