Classes: 2
[name:Rails, file:null, step:When ]
[name:THE_DOCUMENT, file:null, step:Then ]

Methods: 14
[name:attach_file, type:Object, file:null, step:When ]
[name:begin_drafting_document, type:Object, file:null, step:When ]
[name:classify, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:constantize, type:Object, file:null, step:When ]
[name:create, type:Object, file:null, step:Given ]
[name:document_class, type:DocumentHelper, file:alphagov_whitehall/features/support/document_helper.rb, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find_by_title!, type:Object, file:null, step:When ]
[name:gsub, type:Object, file:null, step:When ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:join, type:Object, file:null, step:When ]
[name:record_css_selector, type:Object, file:null, step:Then ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 0

