Given /^a published policy called "([^"]*)" with feedback "([^"]*)" exists$/ do |title, comments|
  policy = create(:published_policy, title: title)
  fact_check_request = create(:fact_check_request,
                              edition: policy,
                              email_address: "user@example.com",
                              comments: comments)
end
Given /^I am (?:a|an) (writer|editor|admin)(?: called "([^"]*)")?$/ do |role, name|
  user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Editor"))
  when "admin"
    create(:user)
  end
  login_as user
end
  def login_as(user)
    GDS::SSO.test_user = user
    PaperTrail.whodunnit = user
  end
Given /^I visit the list of draft policies$/ do
  visit admin_editions_path(state: :draft)
end
Given /^I click on the policy "([^"]*)"$/ do |policy_title|
  click_link policy_title
end
When /^I create a new edition of the published policy "([^"]*)"$/ do |policy_title|
  visit admin_editions_path(state: :published)
  click_link policy_title
  click_button 'Create new edition'
end
Then /^I should see (#{THE_DOCUMENT})$/ do |edition|
  assert has_css?(record_css_selector(edition))
end
