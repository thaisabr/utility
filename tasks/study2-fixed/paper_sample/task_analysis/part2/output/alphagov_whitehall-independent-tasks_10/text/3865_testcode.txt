Given /^a draft publication "([^"]*)" with a legacy url "([^"]*)"$/ do |title, old_url|
  publication = create(:draft_publication, title: title)
  publication.document.document_sources.create(url: old_url)
end
Given /^a draft publication "([^"]*)" with legacy urls "([^"]*)" and "([^"]*)"$/ do |title, old_url_1, old_url_2|
  publication = create(:draft_publication, title: title)
  publication.document.document_sources.create(url: old_url_1)
  publication.document.document_sources.create(url: old_url_2)
end
Then /^I should see the legacy url "([^"]*)"$/ do |old_url|
  within "#document-sources" do
    assert has_content?(old_url), "should have legacy url of #{old_url}"
  end
end
When /^I add "([^"]*)" as a legacy url to the "([^"]*)" publication$/ do |old_url, title|
  publication = Publication.find_by_title!(title)
  visit admin_edition_path(publication)
  fill_in "document_sources", with: old_url
  click_button 'Update'
end
When /^I change the legacy url "([^"]*)" to "([^"]*)" on the "([^"]*)" publication$/ do |old_old_url, new_old_url, title|
  publication = Publication.find_by_title!(title)
  visit admin_edition_path(publication)
  assert has_field?('document_sources', with: old_old_url)
  fill_in "document_sources", with: new_old_url
  click_button 'Update'
end
When /^I remove the legacy url "([^"]*)" on the "([^"]*)" publication$/ do |old_url, title|
  publication = Publication.find_by_title!(title)
  visit admin_edition_path(publication)
  fill_in "document_sources", with: ''
  click_button 'Update'
end
Then /^I should see that it has no legacy urls$/ do
  within "#document-sources" do
    assert has_field?('document_sources', with: '')
  end
end
  def admin_edition_path(edition, *args)
    polymorphic_path([:admin, edition], *args)
  end
Given /^a draft (document|publication|policy|news article|consultation|speech) "([^"]*)" exists$/ do |document_type, title|
  document_type = 'policy' if document_type == 'document'
  create("draft_#{document_class(document_type).name.underscore}".to_sym, title: title)
end
When /^I view the (publication|policy|news article|consultation|speech|document) "([^"]*)"$/ do |document_type, title|
  click_link title
end
When /^I visit the list of draft documents$/ do
  visit admin_editions_path(state: :draft)
end
  def document_class(type)
    type = 'edition' if type == 'document'
    type.gsub(" ", "_").classify.constantize
  end
Given /^I am (?:a|an) (writer|editor|admin|GDS editor|importer)(?: called "([^"]*)")?$/ do |role, name|
  @user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Editor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  when 'importer'
    create(:importer)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    PaperTrail.whodunnit = user
    super(user) # warden
  end
