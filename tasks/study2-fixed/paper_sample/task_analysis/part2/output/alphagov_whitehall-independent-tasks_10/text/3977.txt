Feature: Viewing published policies
In order to obtain useful information about government
A member of the public
Should be able to view policies
@javascript
Scenario: Viewing a policy with a video link
Given a published policy "Policy" with a link "http://www.youtube.com/watch?v=OXHPWmnycno" in the body
When I visit the policy "Policy"
