Given /^I am (?:a|an) (writer|editor|admin|GDS editor)(?: called "([^"]*)")?$/ do |role, name|
  user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Editor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  end
  login_as user
end
  def login_as(user)
    GDS::SSO.test_user = user
    PaperTrail.whodunnit = user
    super(user) # warden
  end
When /^I draft a new news article "([^"]*)"$/ do |title|
  begin_drafting_document type: "news_article", title: title
  fill_in "Summary", with: "here's a simple summary"
  within ".images" do
    attach_file "File", Rails.root.join("test/fixtures/minister-of-funk.960x640.jpg")
    fill_in "Alt text", with: 'An alternative description'
  end
  click_button "Save"
end
When /^I attempt to add the article image into the markdown$/ do
  fill_in "Body", with: "body copy\n!!1\nmore body"
end
Then /^I should be informed I shouldn't use this image in the markdown$/ do
  click_on "Edit"
  assert has_no_css?("fieldset#image_fields .image input[value='!!1']")
end
Then /^I should see the first uploaded image used as the lead image$/ do
  article = NewsArticle.last
  click_on "Force Publish"
  visit document_path(article)
  assert page.has_css?("aside.sidebar img[src*='#{article.images.first.url(:s300)}']")
end
Then /^if no image is uploaded a default image is shown$/ do
  article = NewsArticle.last
  article.images.first.destroy
  visit document_path(article)
  assert page.has_css?("aside.sidebar img[src*='placeholder']")
end
  def document_path(edition, options={})
    polymorphic_path(model_name(edition), options.merge(id: edition.document))
  end
Given /^a published (publication|policy|news article|consultation|speech) "([^"]*)" exists$/ do |document_type, title|
  create("published_#{document_class(document_type).name.underscore}".to_sym, title: title)
end
Then /^I should see (#{THE_DOCUMENT})$/ do |edition|
  assert has_css?(record_css_selector(edition))
end
Then /^my attempt to save it should fail with error "([^"]*)"/ do |error_message|
  click_button "Save"
  assert page.has_css?(".errors li", text: error_message)
end
  def document_class(type)
    type = 'edition' if type == 'document'
    type.gsub(" ", "_").classify.constantize
  end
When /^I create a new edition of the published policy "([^"]*)"$/ do |policy_title|
  visit admin_editions_path(state: :published)
  click_link policy_title
  click_button 'Create new edition'
end
