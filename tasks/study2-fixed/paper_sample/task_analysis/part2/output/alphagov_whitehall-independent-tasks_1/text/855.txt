Feature: Speed tagging editions
I want to be able to tag new editions (especially but not exclusively imported editions) using a slimmed down version of the edit screen.
Specifically, the page:
- should only present policies which are associated with the org of the doc being imported
- and only present them once, even if they are associated with multiple orgs associated with the doc
- should only present ministers which are associated with the org of the doc being imported
- should present mandatory data elements for that document type. (i.e. speech type, publication subtype)
- should include first published at fields for news articles and statistical data sets
- should include opening and closing dates for consultations
- should include delivered on date for speeches
- should include publication date for publications
Background:
Given I am a writer
Scenario: Speed tagging shows world locations when relevant
Given a world location "Uganda" exists
When I go to speed tag a newly imported news article "News article about Uganda"
Then I should be able to select the world location "Uganda"
Scenario: Speed tagging a consultation shows the required fields
When I go to speed tag a newly imported consultation "Review of the Ministry of Beards 2012"
Then I should be able to set the consultation dates
