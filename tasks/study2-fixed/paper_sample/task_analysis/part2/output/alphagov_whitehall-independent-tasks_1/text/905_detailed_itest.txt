Classes: 4
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Given ]
[name:GDS, file:null, step:Given ]
[name:Options, file:alphagov_whitehall/lib/whitehall/document_filter/options.rb, step:Given ]
[name:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]

Methods: 16
[name:begin_drafting_document, type:Object, file:null, step:Given ]
[name:begin_drafting_publication, type:DocumentHelper, file:alphagov_whitehall/features/support/document_helper.rb, step:Given ]
[name:check, type:Object, file:null, step:When ]
[name:click, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_on, type:Object, file:null, step:When ]
[name:create, type:Object, file:null, step:Given ]
[name:create, type:SpeechType, file:alphagov_whitehall/app/models/speech_type.rb, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:fill_in_publication_fields, type:Object, file:null, step:Given ]
[name:first, type:Object, file:null, step:When ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:has_link?, type:Object, file:null, step:Then ]
[name:login_as, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:Given ]
[name:page, type:StatisticsAnnouncementsFilter, file:alphagov_whitehall/app/models/frontend/statistics_announcements_filter.rb, step:Then ]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:Then ]

Referenced pages: 0

