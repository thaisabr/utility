Given /^I start drafting a new specialist guide$/ do
  begin_drafting_document type: 'specialist_guide', title: "Specialist Guide"
end
When /^I select an image for the specialist guide$/ do
  within ".images" do
    attach_file "File", Rails.root.join("features/fixtures/minister-of-soul.jpg")
  end
end
Then /^I should be able to select another image for the specialist guide$/ do
  assert_equal 2, page.all(".images input[type=file]").length
end
When /^I select an attachment for the specialist guide$/ do
  within ".attachments" do
    attach_file "File", Rails.root.join("features/fixtures/attachment.pdf")
  end
end
Then /^I should be able to select another attachment for the specialist guide$/ do
  assert_equal 2, page.all(".attachments input[type=file]").length
end
Given /^I am (?:a|an) (writer|editor|admin)(?: called "([^"]*)")?$/ do |role, name|
  user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Editor"))
  when "admin"
    create(:user)
  end
  login_as user
end
  def login_as(user)
    GDS::SSO.test_user = user
    PaperTrail.whodunnit = user
  end
