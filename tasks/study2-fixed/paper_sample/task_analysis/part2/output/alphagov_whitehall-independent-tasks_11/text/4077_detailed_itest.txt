Classes: 7
[name:File, file:null, step:Given ]
[name:GDS, file:null, step:Given ]
[name:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Given ]
[name:PaperTrail, file:alphagov_whitehall/config/initializers/paper_trail.rb, step:Given ]
[name:Rails, file:null, step:Given ]
[name:THE_DOCUMENT, file:null, step:Then ]
[name:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]

Methods: 16
[name:classify, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:Given ]
[name:constantize, type:Object, file:null, step:When ]
[name:create, type:Object, file:null, step:Given ]
[name:document_class, type:DocumentHelper, file:alphagov_whitehall/features/support/document_helper.rb, step:When ]
[name:edit, type:Admin/organisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:Given ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:find_by_name!, type:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Given ]
[name:find_by_title!, type:Object, file:null, step:When ]
[name:gsub, type:Object, file:null, step:When ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:join, type:Object, file:null, step:Given ]
[name:login_as, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:Given ]
[name:page, type:Object, file:null, step:Then ]
[name:pdf_attachment, type:DocumentHelper, file:alphagov_whitehall/features/support/document_helper.rb, step:Given ]
[name:record_css_selector, type:Object, file:null, step:Then ]

Referenced pages: 4
alphagov_whitehall/app/views/admin/organisations/_contacts_form.html.erb
alphagov_whitehall/app/views/admin/organisations/_form.html.erb
alphagov_whitehall/app/views/admin/organisations/_sidebar.html.erb
alphagov_whitehall/app/views/admin/organisations/edit.html.erb

