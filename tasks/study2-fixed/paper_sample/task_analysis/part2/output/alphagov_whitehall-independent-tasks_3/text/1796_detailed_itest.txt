Classes: 8
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Given ]
[name:Edition, file:alphagov_whitehall/app/presenters/publishing_api_presenters/edition.rb, step:Given ]
[name:GDS, file:null, step:Given ]
[name:Options, file:alphagov_whitehall/lib/whitehall/document_filter/options.rb, step:When ]
[name:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:When ]
[name:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Given ]
[name:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Then ]
[name:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]

Methods: 114
[name:ago, type:Object, file:null, step:Given ]
[name:all, type:Object, file:null, step:When ]
[name:all, type:Object, file:null, step:Then ]
[name:announcements_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:When ]
[name:assert_equal, type:Object, file:null, step:Then ]
[name:build, type:Object, file:null, step:Given ]
[name:check, type:Object, file:null, step:When ]
[name:choose, type:Object, file:null, step:When ]
[name:click_on, type:Object, file:null, step:When ]
[name:click_on, type:Object, file:null, step:Then ]
[name:create, type:Object, file:null, step:When ]
[name:create, type:Object, file:null, step:Given ]
[name:create, type:SpeechType, file:alphagov_whitehall/app/models/speech_type.rb, step:Given ]
[name:css_classes, type:PromotionalFeature, file:alphagov_whitehall/app/models/promotional_feature.rb, step:When ]
[name:delete, type:Options, file:alphagov_whitehall/lib/whitehall/document_filter/options.rb, step:When ]
[name:document, type:FeaturePresenter, file:alphagov_whitehall/app/presenters/feature_presenter.rb, step:Given ]
[name:document, type:TagChangesProcessor, file:alphagov_whitehall/lib/data_hygiene/tag_changes_processor.rb, step:Given ]
[name:each, type:Object, file:null, step:Then ]
[name:edit, type:Admin/organisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:When ]
[name:email_signup_path, type:EmailSignupHelper, file:alphagov_whitehall/app/helpers/email_signup_helper.rb, step:When ]
[name:evaluate_script, type:Object, file:null, step:When ]
[name:execute_script, type:Object, file:null, step:When ]
[name:featured_items, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find, type:OrganisationFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/organisation_finder.rb, step:When ]
[name:find, type:MinisterialRolesFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/ministerial_roles_finder.rb, step:When ]
[name:find, type:NewsArticleTypeFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/news_article_type_finder.rb, step:When ]
[name:find, type:OperationalFieldFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/operational_field_finder.rb, step:When ]
[name:find, type:PublicationTypeFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/publication_type_finder.rb, step:When ]
[name:find, type:RoleAppointmentsFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/role_appointments_finder.rb, step:When ]
[name:find, type:SpeechTypeFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/speech_type_finder.rb, step:When ]
[name:find_by, type:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:When ]
[name:find_by, type:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Given ]
[name:find_by!, type:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Then ]
[name:find_field, type:Object, file:null, step:When ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:has_no_css?, type:Object, file:null, step:Then ]
[name:id, type:AttachmentVisibility, file:alphagov_whitehall/app/models/attachment_visibility.rb, step:When ]
[name:id, type:FeaturePresenter, file:alphagov_whitehall/app/presenters/feature_presenter.rb, step:When ]
[name:index, type:CorporateInformationPagesController, file:alphagov_whitehall/app/controllers/admin/corporate_information_pages_controller.rb, step:When ]
[name:index, type:CorporateInformationPagesController, file:alphagov_whitehall/app/controllers/corporate_information_pages_controller.rb, step:When ]
[name:items, type:HomePageList, file:alphagov_whitehall/app/models/home_page_list.rb, step:When ]
[name:items, type:PromotionalFeature, file:alphagov_whitehall/app/models/promotional_feature.rb, step:When ]
[name:login_as, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:Given ]
[name:mainstream_category_path, type:MainstreamCategoryRoutesHelper, file:alphagov_whitehall/app/helpers/mainstream_category_routes_helper.rb, step:When ]
[name:map, type:Object, file:null, step:Then ]
[name:name, type:Object, file:null, step:When ]
[name:name, type:Person, file:alphagov_whitehall/app/models/person.rb, step:When ]
[name:name, type:SitewideSetting, file:alphagov_whitehall/app/models/sitewide_setting.rb, step:When ]
[name:name, type:RolePresenter, file:alphagov_whitehall/app/presenters/role_presenter.rb, step:When ]
[name:name, type:DataMigration, file:alphagov_whitehall/lib/whitehall/data_migration.rb, step:When ]
[name:name, type:Object, file:null, step:Then ]
[name:name, type:Person, file:alphagov_whitehall/app/models/person.rb, step:Then ]
[name:name, type:SitewideSetting, file:alphagov_whitehall/app/models/sitewide_setting.rb, step:Then ]
[name:name, type:RolePresenter, file:alphagov_whitehall/app/presenters/role_presenter.rb, step:Then ]
[name:name, type:DataMigration, file:alphagov_whitehall/lib/whitehall/data_migration.rb, step:Then ]
[name:organisation, type:EmailSignupInformationController, file:alphagov_whitehall/app/controllers/email_signup_information_controller.rb, step:When ]
[name:organisation, type:EditionFilter, file:alphagov_whitehall/app/models/admin/edition_filter.rb, step:When ]
[name:organisation, type:StatisticsAnnouncementFilter, file:alphagov_whitehall/app/models/admin/statistics_announcement_filter.rb, step:When ]
[name:organisation, type:ImportRowWorker, file:alphagov_whitehall/app/workers/import_row_worker.rb, step:When ]
[name:organisation, type:BrokenLinkReporter, file:alphagov_whitehall/lib/whitehall/broken_link_reporter.rb, step:When ]
[name:organisation, type:FatalityNoticeRow, file:alphagov_whitehall/lib/whitehall/uploader/fatality_notice_row.rb, step:When ]
[name:organisation, type:Row, file:alphagov_whitehall/lib/whitehall/uploader/row.rb, step:When ]
[name:page, type:StatisticsAnnouncementsFilter, file:alphagov_whitehall/app/models/frontend/statistics_announcements_filter.rb, step:When ]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:When ]
[name:page, type:StatisticsAnnouncementsFilter, file:alphagov_whitehall/app/models/frontend/statistics_announcements_filter.rb, step:Then ]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:Then ]
[name:person, type:Appointment, file:alphagov_whitehall/app/models/edition/appointment.rb, step:When ]
[name:person, type:RoleAppointmentPresenter, file:alphagov_whitehall/app/presenters/role_appointment_presenter.rb, step:When ]
[name:policies_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:When ]
[name:promotional_feature_items, type:PromotionalFeature, file:alphagov_whitehall/app/models/promotional_feature.rb, step:When ]
[name:public_document_path, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:When ]
[name:public_document_url, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:When ]
[name:public_document_url, type:AuthorNotifier, file:alphagov_whitehall/app/services/service_listeners/author_notifier.rb, step:When ]
[name:publications_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:When ]
[name:respond_to?, type:EditionCollectionPresenter, file:alphagov_whitehall/app/presenters/edition_collection_presenter.rb, step:Then ]
[name:see_all_link, type:Object, file:null, step:When ]
[name:select, type:Javascript, file:alphagov_whitehall/features/support/javascript.rb, step:When ]
[name:select_from_chosen, type:Javascript, file:alphagov_whitehall/features/support/javascript.rb, step:When ]
[name:select_option, type:Object, file:null, step:When ]
[name:show, type:Admin/organisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:When ]
[name:show, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:When ]
[name:show, type:OrganisationsController, file:alphagov_whitehall/app/controllers/api/organisations_controller.rb, step:When ]
[name:show, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:When ]
[name:show, type:CorporateInformationPagesController, file:alphagov_whitehall/app/controllers/admin/corporate_information_pages_controller.rb, step:When ]
[name:show, type:CorporateInformationPagesController, file:alphagov_whitehall/app/controllers/corporate_information_pages_controller.rb, step:When ]
[name:show, type:ServicesAndInformationController, file:alphagov_whitehall/app/controllers/services_and_information_controller.rb, step:When ]
[name:text, type:Object, file:null, step:When ]
[name:title, type:EditionFilter, file:alphagov_whitehall/app/models/admin/edition_filter.rb, step:When ]
[name:title, type:StatisticsAnnouncementFilter, file:alphagov_whitehall/app/models/admin/statistics_announcement_filter.rb, step:When ]
[name:title, type:ClassificationFeaturing, file:alphagov_whitehall/app/models/classification_featuring.rb, step:When ]
[name:title, type:CorporateInformationPage, file:alphagov_whitehall/app/models/corporate_information_page.rb, step:When ]
[name:title, type:RegisterableEdition, file:alphagov_whitehall/app/models/registerable_edition.rb, step:When ]
[name:title, type:EmailSignupInformationPresenter, file:alphagov_whitehall/app/presenters/email_signup_information_presenter.rb, step:When ]
[name:title, type:FeaturePresenter, file:alphagov_whitehall/app/presenters/feature_presenter.rb, step:When ]
[name:title, type:PromotionalFeatureItemPresenter, file:alphagov_whitehall/app/presenters/promotional_feature_item_presenter.rb, step:When ]
[name:title, type:Row, file:alphagov_whitehall/lib/whitehall/uploader/row.rb, step:When ]
[name:title, type:EditionFilter, file:alphagov_whitehall/app/models/admin/edition_filter.rb, step:Then ]
[name:title, type:StatisticsAnnouncementFilter, file:alphagov_whitehall/app/models/admin/statistics_announcement_filter.rb, step:Then ]
[name:title, type:ClassificationFeaturing, file:alphagov_whitehall/app/models/classification_featuring.rb, step:Then ]
[name:title, type:CorporateInformationPage, file:alphagov_whitehall/app/models/corporate_information_page.rb, step:Then ]
[name:title, type:RegisterableEdition, file:alphagov_whitehall/app/models/registerable_edition.rb, step:Then ]
[name:title, type:EmailSignupInformationPresenter, file:alphagov_whitehall/app/presenters/email_signup_information_presenter.rb, step:Then ]
[name:title, type:FeaturePresenter, file:alphagov_whitehall/app/presenters/feature_presenter.rb, step:Then ]
[name:title, type:PromotionalFeatureItemPresenter, file:alphagov_whitehall/app/presenters/promotional_feature_item_presenter.rb, step:Then ]
[name:title, type:Row, file:alphagov_whitehall/lib/whitehall/uploader/row.rb, step:Then ]
[name:title, type:PromotionalFeature, file:alphagov_whitehall/app/models/promotional_feature.rb, step:When ]
[name:to_json, type:Object, file:null, step:When ]
[name:visible?, type:Object, file:null, step:When ]
[name:visit_organisation, type:Paths, file:alphagov_whitehall/features/support/paths.rb, step:Then ]
[name:with_index, type:Object, file:null, step:Then ]
[name:within, type:Object, file:null, step:When ]
[name:within, type:Object, file:null, step:Then ]

Referenced pages: 33
alphagov_whitehall/app/views/admin/organisations/_form.html.erb
alphagov_whitehall/app/views/admin/organisations/_sidebar.html.erb
alphagov_whitehall/app/views/admin/organisations/edit.html.erb
alphagov_whitehall/app/views/admin/organisations/show.html.erb
alphagov_whitehall/app/views/admin/shared/_default_news_image_fields.html.erb
alphagov_whitehall/app/views/admin/shared/_featured_link_fields.html.erb
alphagov_whitehall/app/views/contacts/_contact.html.erb
alphagov_whitehall/app/views/corporate_information_pages/index.html.erb
alphagov_whitehall/app/views/corporate_information_pages/show.html.erb
alphagov_whitehall/app/views/corporate_information_pages/show_worldwide_organisation.html.erb
alphagov_whitehall/app/views/mainstream_categories/_list_description.html.erb
alphagov_whitehall/app/views/organisations/_corporate_information.html.erb
alphagov_whitehall/app/views/organisations/_documents.html.erb
alphagov_whitehall/app/views/organisations/_featured_items.html.erb
alphagov_whitehall/app/views/organisations/_header.html.erb
alphagov_whitehall/app/views/organisations/_promotional_feature_item.html.erb
alphagov_whitehall/app/views/organisations/show-promotional.html.erb
alphagov_whitehall/app/views/organisations/show.html.erb
alphagov_whitehall/app/views/people/_person.html.erb
alphagov_whitehall/app/views/policies/_list_description.html.erb
alphagov_whitehall/app/views/services_and_information/_collection_group.html.erb
alphagov_whitehall/app/views/services_and_information/show.html.erb
alphagov_whitehall/app/views/shared/_available_languages.html.erb
alphagov_whitehall/app/views/shared/_featured.html.erb
alphagov_whitehall/app/views/shared/_featured_links.html.erb
alphagov_whitehall/app/views/shared/_feeds.html.erb
alphagov_whitehall/app/views/shared/_govspeak_header_contents.html.erb
alphagov_whitehall/app/views/shared/_heading.html.erb
alphagov_whitehall/app/views/shared/_list_description.html.erb
alphagov_whitehall/app/views/shared/_recently_updated.html.erb
alphagov_whitehall/app/views/shared/_recently_updated_documents.html.erb
alphagov_whitehall/app/views/shared/_social_media_accounts.html.erb
alphagov_whitehall/app/views/worldwide_organisations/_header.html.erb

