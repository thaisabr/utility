Given /^I have imported a file that succeeded$/ do
  organisation = create(:organisation)
  topic = create(:topic)
  data = %Q{
old_url,title,summary,body,organisation,policy_1,publication_type,document_collection_1,publication_date,order_url,price,isbn,urn,command_paper_number,ignore_1,html_title,html_body,attachment_1_url,attachment_1_title,country_1,topic_1
http://example.com/1,title-1,a-summary,a-body,,,,,14-Dec-2011,,,,,,,html-title-A,html-body-A,,,,#{topic.slug}
http://example.com/2,title-2,a-summary,a-body,,,,,14-Dec-2011,,,,,,,html-title-B,html-body-B,,,,#{topic.slug}
    }.strip
  @force_publish_import = import_data_as_document_type_for_organisation(data, 'Publication', organisation)
end
When /^I speed tag all of the documents and make them draft$/ do
  speed_tag_publication('title-1')
  convert_to_draft('title-1')
  speed_tag_publication('title-2')
  convert_to_draft('title-2')
end
Then /^I cannot force publish the import(?: again)?$/ do
  visit admin_imports_path

  within("tr#import_#{@force_publish_import.id}") do
    assert page.has_no_button?('Force publish')
  end
end
Then /^I can see the log output of the force publish for my import$/ do
  visit admin_imports_path
  within("tr#import_#{@force_publish_import.id}") do
    click_on 'View log for most recent force publication attempt'
  end

  within '.log' do
    # has_content? would be nicer, but I think the fact that it's a
    # <pre> tag and thus whitespace is important is messing with things
    # better to be explicit
    assert page.has_xpath?("./pre[normalize-space(.) = normalize-space('#{@force_publish_import.most_recent_force_publication_attempt.log}')]", %Q{should have log!})
  end
end
Then /^my imported documents are published$/ do
  visit public_document_path(Edition.find_by_title('title-1'))
  assert page.has_css?('h1', 'title-1')

  visit public_document_path(Edition.find_by_title('title-2'))
  assert page.has_css?('h1', 'title-2')
end
  def import_data_as_document_type_for_organisation(data, document_type, organisation)
    make_sure_data_importer_user_exists
    Import.use_separate_connection

    with_import_csv_file(data) do |path|
      visit new_admin_import_path
      select document_type, from: 'Type'
      attach_file 'CSV File', path
      select organisation.name, from: 'Default organisation'
      click_button 'Save'
      click_button 'Run'

      visit current_path
    end
    Import.find(current_path.match(/admin\/imports\/(\d+)\Z/)[1])
  end
  def speed_tag_publication(title)
    edition = Edition.find_by_title(title)
    visit admin_edition_path(edition)

    assert page.has_css?('.speed-tag')
    within '.speed-tag' do
      select 'Research and analysis', from: 'Publication type'
      click_on 'Save'
      assert page.has_no_css?('.speed-tag .alert')
    end
  end
  def convert_to_draft(title)
    edition = Edition.find_by_title(title)
    visit admin_edition_path(edition)

    click_on 'Convert to draft'
    assert page.has_no_css?('.speed-tag')
  end
  def with_import_csv_file(data)
    tf = Tempfile.new('csv_import')
    tf << data
    tf.close
    yield tf.path
    tf.unlink
  end
  def make_sure_data_importer_user_exists
    unless User.find_by_name('Automatic Data Importer')
      create(:importer, name: 'Automatic Data Importer')
    end
  end
  def select(value, options={})
    if options.has_key?(:from)
      element = find(:select, options[:from], visible: :all).find(:option, value, visible: :all)
      if element.visible?
        from = options.delete(:from)
        find(:select, from, options).find(:option, value, options).select_option
      else
        select_from_chosen(value, options)
      end
    else
      find(:option, value, options).select_option
    end
  end
  def select_from_chosen(value, options={})
    field = find_field(options[:from], visible: false, match: :first)
    option_value = page.evaluate_script("$(\"##{field[:id]} option:contains('#{value}')\").val()")

    if field[:multiple]
      page.execute_script("value = ['#{option_value}']\; if ($('##{field[:id]}').val()) {$.merge(value, $('##{field[:id]}').val())}")
      option_value = page.evaluate_script("value")
    end

    page.execute_script("$('##{field[:id]}').val(#{option_value.to_json})")
    page.execute_script("$('##{field[:id]}').trigger('liszt:updated').trigger('change')")
  end
  def public_document_path(edition, options = {})
    document_path(edition, options)
  end
  def admin_edition_path(edition, *args)
    polymorphic_path([:admin, edition], *args)
  end
When /^I force publish (#{THE_DOCUMENT})$/ do |edition|
  visit_edition_admin edition.title, :draft
  click_link "Edit draft"
  fill_in_change_note_if_required
  click_button "Save"
  publish(force: true)
end
  def visit_edition_admin(title, scope = :scoped)
    document = Edition.send(scope).find_by_title(title)
    visit admin_edition_path(document)
  end
  def fill_in_change_note_if_required
    if has_css?("textarea[name='edition[change_note]']")
      fill_in "edition_change_note", with: "changes"
    end
  end
  def publish(options = {})
    if options[:force]
      click_link "Force publish"
      page.has_css?("#forcePublishModal", visible: true)
      within '#forcePublishModal' do
        fill_in 'reason', with: "because"
        click_button 'Force publish'
      end
      unless options[:ignore_errors]
        refute_flash_alerts_exist
      end
    else
      click_button "Publish"
      unless options[:ignore_errors]
        refute_flash_alerts_exist
      end
    end
  end
  def refute_flash_alerts_exist
    assert has_no_css?(".flash.alert")
  end
Given /^I am (?:a|an) (writer|editor|admin|GDS editor|importer|managing editor)(?: called "([^"]*)")?$/ do |role, name|
  @user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Depteditor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  when 'importer'
    create(:importer)
  when 'managing editor'
    create(:managing_editor)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    Edition::AuditTrail.whodunnit = user
    super(user) # warden
  end
