Given /^I am (?:an?) (writer|editor) in the organisation "([^"]*)"$/ do |role, organisation_name|
  organisation = Organisation.find_by_name(organisation_name) || create(:organisation, name: organisation_name)
  @user = case role
  when "writer"
    create(:policy_writer, name: "Wally Writer", organisation: organisation)
  when "editor"
    create(:departmental_editor, name: "Eddie Depteditor", organisation: organisation)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    Edition::AuditTrail.whodunnit = user
    super(user) # warden
  end
When /^I draft a new detailed guide "([^"]*)"$/ do |title|
  category = create(:mainstream_category)
  begin_drafting_document type: 'detailed_guide', title: title, primary_mainstream_category: category
  click_button "Save"
end
Then /^I should see (#{THE_DOCUMENT})$/ do |edition|
  assert has_css?(record_css_selector(edition))
end
