Given /^I am (?:a|an) (writer|editor|admin|GDS editor|importer|managing editor)(?: called "([^"]*)")?$/ do |role, name|
  @user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Depteditor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  when 'importer'
    create(:importer)
  when 'managing editor'
    create(:managing_editor)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    Edition::AuditTrail.whodunnit = user
    super(user) # warden
  end
Given /^the organisation "([^"]*)" exists$/ do |name|
  create(:ministerial_department, name: name)
end
When /^I add some featured services and guidance to the organisation "([^"]*)" via the admin$/ do |organisation_name|
  organisation = Organisation.find_by_name!(organisation_name)
  visit admin_organisation_path(organisation)
  click_link "Edit"
  within ".featured-services-and-guidance" do
    fill_in "Url", with: "https://www.gov.uk/example/service"
    fill_in "Title", with: "Example Service"
  end
  click_button "Save"
end
Then /^the featured services and guidance for the organisation "([^"]*)" should be visible on the public site$/ do |organisation_name|
  visit_organisation organisation_name
  within ".featured-services-and-guidance" do
    assert page.has_css?("a[href='https://www.gov.uk/example/service']", "Example Service")
  end
end
  def visit_organisation(name)
    organisation = Organisation.find_by_name!(name)
    visit organisation_path(organisation)
  end
