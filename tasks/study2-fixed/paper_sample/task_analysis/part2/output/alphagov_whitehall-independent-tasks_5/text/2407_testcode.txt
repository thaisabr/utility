Given(/^There is a statistics announcement$/) do
  @organisation = create :ministerial_department
  @topic = create :topic
  @announcement = create :statistics_announcement,
                         organisation: @organisation,
                         topic: @topic,
                         current_release_date: build(:statistics_announcement_date),
                         statistics_announcement_dates: [ build(:statistics_announcement_date_change, change_note: "A change note") ]
  @announcement.reload # Factorygirl doesn't get current_release_date / statistics_announcement_dates quite right - needs reload here.
end
When(/^I visit the statistics announcements page$/) do
  visit statistics_announcements_path
end
When(/^I click on the first statistics announcement$/) do
  within(".filter-results") { click_on @announcement.title }
end
Then(/^I should be on a page showing the title, release date, organisation, topic, summary and date change information of the release announcement$/) do
  assert_equal statistics_announcement_path(@announcement), current_path

  assert page.has_content? @announcement.title
  assert page.has_content? @announcement.current_release_date.display_date
  assert page.has_content? @announcement.summary
  assert page.has_link? @organisation.name, href: organisation_path(@organisation)
  assert page.has_link? @topic.name, href: topic_path(@topic)
  assert page.has_content? @announcement.last_change_note
  assert page.has_content? @announcement.previous_display_date
end
