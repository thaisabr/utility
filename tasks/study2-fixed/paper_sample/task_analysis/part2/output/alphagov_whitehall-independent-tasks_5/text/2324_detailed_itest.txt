Classes: 2
[name:SitewideSetting, file:alphagov_whitehall/app/models/sitewide_setting.rb, step:Given ]
[name:THE_DOCUMENT, file:null, step:Then ]

Methods: 17
[name:has_content?, type:Object, file:null, step:Then ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:has_no_css?, type:Object, file:null, step:Then ]
[name:index, type:MinisterialRolesController, file:alphagov_whitehall/app/controllers/ministerial_roles_controller.rb, step:When ]
[name:ministers_page, type:Paths, file:alphagov_whitehall/features/support/paths.rb, step:When ]
[name:name, type:Person, file:alphagov_whitehall/app/models/person.rb, step:When ]
[name:name, type:SitewideSetting, file:alphagov_whitehall/app/models/sitewide_setting.rb, step:When ]
[name:name, type:RolePresenter, file:alphagov_whitehall/app/presenters/role_presenter.rb, step:When ]
[name:name, type:DataMigration, file:alphagov_whitehall/lib/whitehall/data_migration.rb, step:When ]
[name:new, type:SitewideSetting, file:alphagov_whitehall/app/models/sitewide_setting.rb, step:Given ]
[name:page, type:StatisticsAnnouncementsFilter, file:alphagov_whitehall/app/models/frontend/statistics_announcements_filter.rb, step:Then ]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:Then ]
[name:person, type:Appointment, file:alphagov_whitehall/app/models/edition/appointment.rb, step:When ]
[name:person, type:RoleAppointmentPresenter, file:alphagov_whitehall/app/presenters/role_appointment_presenter.rb, step:When ]
[name:record_css_selector, type:Object, file:null, step:Then ]
[name:refute, type:Object, file:null, step:Then ]
[name:save!, type:Object, file:null, step:Given ]

Referenced pages: 3
alphagov_whitehall/app/views/ministerial_roles/index.html.erb
alphagov_whitehall/app/views/people/_person.html.erb
alphagov_whitehall/app/views/shared/_heading.html.erb

