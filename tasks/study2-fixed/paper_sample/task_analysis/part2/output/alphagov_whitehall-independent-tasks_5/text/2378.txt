Feature: Administering Organisations
Background:
Given I am an admin called "Jane"
Scenario: Adding featured services and guidance
Given I am a GDS editor
And the organisation "Ministry of Pop" exists
When I add some featured services and guidance to the organisation "Ministry of Pop" via the admin
Then the featured services and guidance for the organisation "Ministry of Pop" should be visible on the public site
