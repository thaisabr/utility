Given /^I am (?:a|an) (writer|editor|admin|GDS editor|importer)(?: called "([^"]*)")?$/ do |role, name|
  @user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Depteditor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  when 'importer'
    create(:importer)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    Edition::AuditTrail.whodunnit = user
    super(user) # warden
  end
When /^I draft a new authored article "([^"]*)"$/ do |title|
  begin_drafting_speech title: title
  select 'Authored article', from: "Type"
end
When /^I preview the authored article$/ do
  click_link "Preview"
end
Then /^it should be shown as an authored article in the admin screen$/ do
  click_button "Save"
  assert page.has_content?("Draft Authored Article")
end
  def begin_drafting_speech(options)
    organisation = create(:ministerial_department)
    person = create_person("Colonel Mustard")
    role = create(:ministerial_role, name: "Attorney General", organisations: [organisation])
    role_appointment = create(:role_appointment, person: person, role: role, started_at: Date.parse('2010-01-01'))
    speech_type = SpeechType::Transcript
    begin_drafting_document options.merge(type: 'speech', summary: "Some summary of the content")
    select speech_type.name, from: "Type"
    select "Colonel Mustard, Attorney General", from: "Speaker"
    select_date "Delivered on", with: 1.day.ago.to_s
    fill_in "Location", with: "The Drawing Room"
  end
  def create_person(name, attributes = {})
    create(:person, split_person_name(name).merge(attributes))
  end
  def begin_drafting_document(options)
    if Organisation.count == 0
      create(:organisation)
    end
    visit admin_editions_path
    click_link "Create #{options[:type].titleize}"
    fill_in "Title", with: options[:title]
    fill_in "Body", with: options[:body] || "Any old iron"
    fill_in "Summary", with: options[:summary] || 'one plus one euals two!'
    fill_in_change_note_if_required
    set_lead_organisation_on_document(Organisation.first)
    if options[:alternative_format_provider]
      select options[:alternative_format_provider].name, from: "edition_alternative_format_provider_id"
    end
    if options[:primary_mainstream_category]
      select options[:primary_mainstream_category].title, from: "Primary detailed guidance category"
    end
  end
  def split_person_name(name)
    if match = /^(\w+)\s*(.*?)$/.match(name)
      forename, surname = match.captures
      { title: nil, forename: forename, surname: surname, letters: nil }
    else
      raise "couldn't split \"#{name}\""
    end
  end
  def set_lead_organisation_on_document(organisation, order = 1)
    if has_css?("select#edition_lead_organisation_ids_#{order}")
      select organisation.name, from: "edition_lead_organisation_ids_#{order}"
    end
  end
  def fill_in_change_note_if_required
    if has_css?("textarea[name='edition[change_note]']")
      fill_in "edition_change_note", with: "changes"
    end
  end
Then /^I should see (#{THE_DOCUMENT})$/ do |edition|
  assert has_css?(record_css_selector(edition))
end
