Classes: 14
[name:Contact, file:alphagov_whitehall/app/models/contact.rb, step:Then ]
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Given ]
[name:GDS, file:null, step:Given ]
[name:Locale, file:alphagov_whitehall/lib/locale.rb, step:When ]
[name:Locale, file:alphagov_whitehall/lib/locale.rb, step:Then ]
[name:LocalisedModel, file:alphagov_whitehall/lib/localised_model.rb, step:When ]
[name:NewsArticle, file:alphagov_whitehall/app/models/news_article.rb, step:When ]
[name:Rails, file:null, step:When ]
[name:THE_DOCUMENT, file:null, step:Then ]
[name:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]
[name:WorldLocation, file:alphagov_whitehall/app/models/world_location.rb, step:When ]
[name:WorldLocation, file:alphagov_whitehall/app/models/world_location.rb, step:Given ]
[name:WorldLocation, file:alphagov_whitehall/app/models/world_location.rb, step:Then ]
[name:WorldwideOrganisation, file:alphagov_whitehall/app/models/worldwide_organisation.rb, step:Then ]

Methods: 61
[name:add_translation_to_world_location, type:Object, file:null, step:Given ]
[name:announcements_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:Then ]
[name:attach_file, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:Then ]
[name:click_on, type:Object, file:null, step:When ]
[name:contactable, type:Object, file:null, step:Then ]
[name:create, type:Object, file:null, step:Given ]
[name:current_person, type:Role, file:alphagov_whitehall/app/models/role.rb, step:null]
[name:current_person, type:RolePresenter, file:alphagov_whitehall/app/presenters/role_presenter.rb, step:null]
[name:each_with_index, type:Object, file:null, step:When ]
[name:feature_news_article_in_world_location, type:WorldLocationSteps, file:alphagov_whitehall/features/step_definitions/world_location_steps.rb, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find, type:DocumentSeriesFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/document_series_finder.rb, step:When ]
[name:find, type:PoliciesFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/policies_finder.rb, step:When ]
[name:find, type:WorldLocationsFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/world_locations_finder.rb, step:When ]
[name:find_by_language_name, type:Locale, file:alphagov_whitehall/lib/locale.rb, step:When ]
[name:find_by_name!, type:WorldLocation, file:alphagov_whitehall/app/models/world_location.rb, step:When ]
[name:find_by_name!, type:WorldLocation, file:alphagov_whitehall/app/models/world_location.rb, step:Then ]
[name:find_by_title, type:CorporateInformationPageType, file:alphagov_whitehall/app/models/corporate_information_page_type.rb, step:When ]
[name:first, type:AttachmentsPresenter, file:alphagov_whitehall/app/presenters/attachments_presenter.rb, step:Then ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:has_no_css?, type:Object, file:null, step:Then ]
[name:join, type:Object, file:null, step:When ]
[name:last, type:WorldLocation, file:alphagov_whitehall/app/models/world_location.rb, step:Given ]
[name:last, type:WorldwideOrganisation, file:alphagov_whitehall/app/models/worldwide_organisation.rb, step:Then ]
[name:login_as, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:Given ]
[name:new, type:LocalisedModel, file:alphagov_whitehall/lib/localised_model.rb, step:When ]
[name:organisation, type:EditionsController, file:alphagov_whitehall/app/controllers/admin/editions_controller.rb, step:null]
[name:organisation, type:FatalityNoticeRow, file:alphagov_whitehall/lib/whitehall/uploader/fatality_notice_row.rb, step:null]
[name:organisation, type:Row, file:alphagov_whitehall/lib/whitehall/uploader/row.rb, step:null]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:When ]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:Then ]
[name:person, type:Appointment, file:alphagov_whitehall/app/models/edition/appointment.rb, step:null]
[name:person, type:RoleAppointmentPresenter, file:alphagov_whitehall/app/presenters/role_appointment_presenter.rb, step:null]
[name:policies_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:Then ]
[name:public_document_path, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:Then ]
[name:public_document_url, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:Then ]
[name:publications_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:Then ]
[name:record_css_selector, type:Object, file:null, step:When ]
[name:record_css_selector, type:Object, file:null, step:Then ]
[name:rows, type:Import, file:alphagov_whitehall/app/models/import.rb, step:When ]
[name:rows_hash, type:Object, file:null, step:When ]
[name:rows_hash, type:Object, file:null, step:Then ]
[name:set, type:Object, file:null, step:When ]
[name:show, type:Admin/worldLocationsController, file:alphagov_whitehall/app/controllers/admin/world_locations_controller.rb, step:Then ]
[name:show, type:WorldLocationsController, file:alphagov_whitehall/app/controllers/admin/world_locations_controller.rb, step:Then ]
[name:show, type:WorldLocationsController, file:alphagov_whitehall/app/controllers/api/world_locations_controller.rb, step:Then ]
[name:show, type:WorldLocationsController, file:alphagov_whitehall/app/controllers/world_locations_controller.rb, step:Then ]
[name:show, type:WorldwideOrganisationsController, file:alphagov_whitehall/app/controllers/admin/worldwide_organisations_controller.rb, step:null]
[name:show, type:WorldwideOrganisationsController, file:alphagov_whitehall/app/controllers/api/worldwide_organisations_controller.rb, step:null]
[name:show, type:WorldwideOrganisationsController, file:alphagov_whitehall/app/controllers/worldwide_organisations_controller.rb, step:null]
[name:title, type:WorldwidePriority, file:alphagov_whitehall/app/models/worldwide_priority.rb, step:Then ]
[name:url, type:WorldwidePriority, file:alphagov_whitehall/app/models/worldwide_priority.rb, step:Then ]
[name:view_world_location_in_locale, type:WorldLocationSteps, file:alphagov_whitehall/features/step_definitions/world_location_steps.rb, step:Then ]
[name:where, type:Contact, file:alphagov_whitehall/app/models/contact.rb, step:Then ]
[name:within, type:Object, file:null, step:When ]
[name:within, type:Object, file:null, step:Then ]
[name:worldwide_organisation, type:WorldwideOfficesController, file:alphagov_whitehall/app/controllers/admin/worldwide_offices_controller.rb, step:Then ]

Referenced pages: 17
alphagov_whitehall/app/views/admin/world_locations/show.html.erb
alphagov_whitehall/app/views/announcements/_list_description.html.erb
alphagov_whitehall/app/views/contacts/_contact.html.erb
alphagov_whitehall/app/views/people/_person.html.erb
alphagov_whitehall/app/views/policies/_list_description.html.erb
alphagov_whitehall/app/views/publications/_list_description.html.erb
alphagov_whitehall/app/views/shared/_available_languages.html.erb
alphagov_whitehall/app/views/shared/_featured_news.html.erb
alphagov_whitehall/app/views/shared/_feeds.html.erb
alphagov_whitehall/app/views/shared/_recently_updated.html.erb
alphagov_whitehall/app/views/shared/_recently_updated_documents.html.erb
alphagov_whitehall/app/views/shared/_social_media_accounts.html.erb
alphagov_whitehall/app/views/world_locations/show.html.erb
alphagov_whitehall/app/views/worldwide_organisations/_corporate_information.html.erb
alphagov_whitehall/app/views/worldwide_organisations/_header.html.erb
alphagov_whitehall/app/views/worldwide_organisations/_worldwide_organisation.html.erb
alphagov_whitehall/app/views/worldwide_organisations/show.html.erb

