Given /^I am (?:a|an) (writer|editor|admin|GDS editor|importer)(?: called "([^"]*)")?$/ do |role, name|
  @user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Depteditor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  when 'importer'
    create(:importer)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    Edition::AuditTrail.whodunnit = user
    super(user) # warden
  end
Given /^there are some ministers for the "([^"]*)"$/ do |organisation_name|
  aaron = create_role_appointment('Aaron A. Aadrvark', "Minister of The Start Of The Alphabet", organisation_name, 2.years.ago)
  marion = create_role_appointment('Marion M. Myddleton', "Minister of The Middle Of The Alphabet", organisation_name, 2.years.ago)
  zeke = create_role_appointment('Zeke Z. Zaltzman', "Minister of The End Of The Alphabet", organisation_name, 2.years.ago)
  onezero = create_role_appointment('10101010', "Minister of Numbers", organisation_name, 2.years.ago)
  @the_ministers = [aaron, marion, zeke, onezero]
  @the_ministerial_organisation = Organisation.find_by_name(organisation_name)
end
When /^I specify an order for those ministers$/ do
  visit people_admin_organisation_path(@the_ministerial_organisation)
  # .shuffle on it's own isn't enough to guarantee a new ordering, so we
  # swap the first and last, and shuffle the middle
  @the_ordered_ministers = [@the_ministers[-1], *(@the_ministers[1..-2].shuffle), @the_ministers[0]]
  @the_ordered_ministers.each_with_index do |role_appointment, index|
    fill_in "#{role_appointment.role.name}#{role_appointment.person.name}", with: index
  end
  click_on 'Save'
end
Then /^I should see that ordering displayed on the organisation page$/ do
  visit organisation_path(@the_ministerial_organisation)
  within '#ministers' do
    @the_ordered_ministers.each.with_index do |role_appointment, idx|
      assert page.has_css?("li:nth-child(#{idx + 1}) h3", text: role_appointment.person.name)
    end
  end
end
Then /^I should see that ordering displayed on the section for the organisation on the ministers page$/ do
  visit ministers_page
  within "#organisation_#{@the_ministerial_organisation.id} .minister-list" do
    @the_ordered_ministers.each.with_index do |role_appointment, idx|
      assert page.has_css?("li:nth-child(#{idx + 1}) h4", text: role_appointment.person.name)
    end
  end
end
  def create_role_appointment(person_name, role_name, organisation_name, timespan, options = {})
    person = find_or_create_person(person_name)
    organisation = Organisation.find_by_name(organisation_name) || create(:ministerial_department, name: organisation_name)
    role = MinisterialRole.create!({name: role_name}.merge(options[:role_options] || {}))
    organisation.ministerial_roles << role

    if timespan.is_a?(Hash)
      started_at = timespan.keys.first
      ended_at = timespan.values.first
    else
      started_at = timespan
      ended_at = nil
    end

    create(:role_appointment, role: role, person: person, started_at: started_at, ended_at: ended_at)
  end
  def ministers_page
    ministerial_roles_path
  end
  def find_or_create_person(name)
    find_person(name) || create_person(name)
  end
  def create_person(name, attributes = {})
    create(:person, split_person_name(name).merge(attributes))
  end
  def find_person(name)
    Person.where(split_person_name(name)).first
  end
  def split_person_name(name)
    if match = /^(\w+)\s*(.*?)$/.match(name)
      forename, surname = match.captures
      { title: nil, forename: forename, surname: surname, letters: nil }
    else
      raise "couldn't split \"#{name}\""
    end
  end
Then /^I should see (#{THE_DOCUMENT})$/ do |edition|
  assert has_css?(record_css_selector(edition))
end
