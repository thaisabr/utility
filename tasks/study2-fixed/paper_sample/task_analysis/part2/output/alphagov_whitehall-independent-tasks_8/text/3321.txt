Feature: Grouping documents into series
As an organisation,
I want to present regularly published documents as series
So that my users can more easily find earlier publications of the same type
Background:
Given I am a writer in the organisation "Department of Beards"
And I create a series called "Monthly Facial Topiary Update" in the "Department of Beards" organisation
Scenario: Series list all their documents
Given I create a document called "May 2012 Update" in the "Monthly Facial Topiary Update" series
And I create a document called "June 2012 Update" in the "Monthly Facial Topiary Update" series
And someone publishes the document "May 2012 Update"
And someone publishes the document "June 2012 Update"
When I view the "Monthly Facial Topiary Update" series
Then I should see a summary of the series clearly displayed
Then I should see links to all the documents in the "Monthly Facial Topiary Update" series
