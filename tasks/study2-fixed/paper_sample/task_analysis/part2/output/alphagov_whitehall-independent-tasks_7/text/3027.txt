Feature: Filtering documents by author in admin
Scenario: Viewing only publications written by me
Given I am a writer
And there is a user called "Janice"
And "Janice" drafts a new publication "Janice's Publication"
And I draft a new policy "My Policy"
And I visit the list of draft documents
When I filter by author "Janice"
And I select the "Publication" edition filter
Then I should see the publication "Janice's Publication"
And I should not see the policy "My Policy"
