Classes: 8
[name:Consultation, file:alphagov_whitehall/app/models/consultation.rb, step:When ]
[name:Consultation, file:alphagov_whitehall/app/models/consultation.rb, step:Then ]
[name:ConsultationOutcome, file:alphagov_whitehall/app/models/consultation_outcome.rb, step:Then ]
[name:ConsultationPublicFeedback, file:alphagov_whitehall/app/models/consultation_public_feedback.rb, step:Then ]
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Given ]
[name:GDS, file:null, step:Given ]
[name:Rails, file:null, step:When ]
[name:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]

Methods: 34
[name:attach_file, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:Then ]
[name:create, type:Object, file:null, step:Given ]
[name:create, type:SpeechType, file:alphagov_whitehall/app/models/speech_type.rb, step:Given ]
[name:edit, type:Admin/consultationsController, file:alphagov_whitehall/app/controllers/admin/consultations_controller.rb, step:When ]
[name:ensure_path, type:PathHelper, file:alphagov_whitehall/features/support/path_helper.rb, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:fill_in_change_note_if_required, type:DocumentHelper, file:alphagov_whitehall/features/support/document_helper.rb, step:When ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:has_css?, type:Object, file:null, step:When ]
[name:join, type:Object, file:null, step:When ]
[name:last, type:Consultation, file:alphagov_whitehall/app/models/consultation.rb, step:When ]
[name:last, type:ConsultationOutcome, file:alphagov_whitehall/app/models/consultation_outcome.rb, step:Then ]
[name:last, type:ConsultationPublicFeedback, file:alphagov_whitehall/app/models/consultation_public_feedback.rb, step:Then ]
[name:last, type:Consultation, file:alphagov_whitehall/app/models/consultation.rb, step:Then ]
[name:login_as, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:Given ]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:When ]
[name:publish, type:DocumentHelper, file:alphagov_whitehall/features/support/document_helper.rb, step:When ]
[name:record_css_selector, type:Object, file:null, step:Then ]
[name:refute, type:Object, file:null, step:When ]
[name:refute_flash_alerts_exist, type:DocumentHelper, file:alphagov_whitehall/features/support/document_helper.rb, step:When ]
[name:select_most_recent_consultation_from_list, type:ConsultationHelper, file:alphagov_whitehall/features/support/consultation_helper.rb, step:Then ]
[name:title, type:CorporateInformationPage, file:alphagov_whitehall/app/models/corporate_information_page.rb, step:Then ]
[name:title, type:TitleExtractor, file:alphagov_whitehall/app/models/email_signup/title_extractor.rb, step:Then ]
[name:title, type:AttachmentsPresenter, file:alphagov_whitehall/app/presenters/attachments_presenter.rb, step:Then ]
[name:title, type:FeaturePresenter, file:alphagov_whitehall/app/presenters/feature_presenter.rb, step:Then ]
[name:title, type:PromotionalFeatureItemPresenter, file:alphagov_whitehall/app/presenters/promotional_feature_item_presenter.rb, step:Then ]
[name:title, type:Row, file:alphagov_whitehall/lib/whitehall/uploader/row.rb, step:Then ]
[name:title, type:StatisticalDataSetRow, file:alphagov_whitehall/lib/whitehall/uploader/statistical_data_set_row.rb, step:Then ]
[name:view_visible_consultation_on_website, type:ConsultationHelper, file:alphagov_whitehall/features/support/consultation_helper.rb, step:Then ]
[name:within, type:Object, file:null, step:Then ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 7
alphagov_whitehall/app/views/admin/consultations/_additional_significant_fields.html.erb
alphagov_whitehall/app/views/admin/consultations/_date_fields.html.erb
alphagov_whitehall/app/views/admin/consultations/_edition.html.erb
alphagov_whitehall/app/views/admin/consultations/_form.html.erb
alphagov_whitehall/app/views/admin/consultations/_required_fields_for_imported_editions.html.erb
alphagov_whitehall/app/views/documents/_attachment.html.erb
alphagov_whitehall/app/views/documents/_attachments.html.erb

