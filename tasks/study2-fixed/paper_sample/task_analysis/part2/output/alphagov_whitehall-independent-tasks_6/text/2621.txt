Feature: Corporate Information Pages
Scenario:
Given I am a writer
And my organisation has a "Terms of reference" corporate information page
Then I should be able to add attachments to the "Terms of reference" corporate information page
