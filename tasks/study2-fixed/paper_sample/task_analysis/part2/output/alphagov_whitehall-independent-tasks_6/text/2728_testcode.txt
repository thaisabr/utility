Given /^I am (?:a|an) (writer|editor|admin|GDS editor|importer|managing editor)(?: called "([^"]*)")?$/ do |role, name|
  @user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Depteditor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  when 'importer'
    create(:importer)
  when 'managing editor'
    create(:managing_editor)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    Edition::AuditTrail.whodunnit = user
    super(user) # warden
  end
Given /^a published (publication|policy|news article|consultation|speech|detailed guide) "([^"]*)" exists$/ do |document_type, title|
  create("published_#{document_class(document_type).name.underscore}".to_sym, title: title)
end
  def document_class(type)
    type = 'edition' if type == 'document'
    type.gsub(" ", "_").classify.constantize
  end
When(/^I unpublish the duplicate, marking it as consolidated into the other page$/) do
  visit admin_edition_path(@duplicate_edition)
  click_on 'Archive or unpublish'
  choose 'Unpublish: consolidated into another GOV.UK page'

  within '#js-consolidated-form' do
    fill_in 'consolidated_alternative_url', with: Whitehall.url_maker.policy_url(@existing_edition.document)
    click_button 'Unpublish'
  end
end
When(/^I archive the policy because it is no longer government policy$/) do
  @policy = Policy.last
  visit admin_edition_path(@policy)
  click_on 'Archive or unpublish'
  choose 'Archive: no longer current government policy/activity'
  fill_in 'Public explanation (this is shown on the live site) *', with: 'We no longer believe people should shave'
  click_button 'Archive'

  assert_equal :archived, @policy.reload.current_state
end
Then(/^the policy should be marked as archived on the public site$/) do
  visit public_document_path(@policy)
  assert page.has_content?(@policy.title)
  assert page.has_content?('This policy was archived')
  assert page.has_content?('We no longer believe people should shave')
end
Then(/^I should be redirected to the other page when I view the document on the public site$/) do
  visit public_document_path(@duplicate_edition)
  assert_equal policy_path(@existing_edition.document), page.current_path
end
Then(/^there should be an editorial remark recording the fact that the document was archived$/) do
  edition = Edition.last
  assert_equal 'Archived', edition.editorial_remarks.last.body
end
Then /^I should see that the document was published in error on the public site$/ do
  edition = Edition.last
  visit public_document_path(edition)
  assert page.has_no_content?(edition.title)
  assert page.has_content?('The information on this page has been removed because it was published in error')
  assert page.has_content?('This page should never have existed')
end
Then /^I should see that the document was published in error at the original url$/ do
  visit policy_path(@original_slug)
  assert page.has_no_content?(@document.title)
  assert page.has_content?('The information on this page has been removed because it was published in error')
  assert page.has_content?('This page should never have existed')
end
When /^I unpublish the document and ask for a redirect$/ do
  unpublish_edition(Edition.last) do
    fill_in 'published_in_error_alternative_url', with: Whitehall.url_maker.how_government_works_url
    check 'Redirect to URL automatically?'
  end
end
  def unpublish_edition(edition)
    visit admin_edition_path(edition)
    click_on 'Archive or unpublish'
    choose 'Unpublish: published in error'
    within '#js-published-in-error-form' do
      fill_in 'Public explanation (this is shown on the live site)', with: 'This page should never have existed'
      yield if block_given?
      click_button 'Unpublish'
    end
  end
  def admin_edition_path(edition, *args)
    polymorphic_path([:admin, edition], *args)
  end
  def public_document_path(edition, options = {})
    document_path(edition, options)
  end
When(/^I add the document "(.*?)" to the document collection$/) do |document_title|
  doc_edition = Edition.find_by_title!(document_title)
  refute @document_collection.nil?, "No document collection to act on."

  visit admin_document_collection_path(@document_collection)
  click_on "Edit draft"
  click_on "Collection documents"

  fill_in 'title', with: document_title
  click_on 'Find'
  find('li.ui-menu-item').click
  click_on 'Add'

  within ('section.group') do
    assert page.has_content? doc_edition.title
  end
end
Then(/^I (?:can )?preview the document collection$/) do
  refute @document_collection.nil?, "No document collection to act on."

  visit preview_document_path(@document_collection)

  assert page.has_selector?("h1", text: @document_collection.title)
  assert page.has_content? @document_collection.summary
  assert page.has_content? @document_collection.body
end
Then(/^I can see in the preview that "(.*?)" is part of the document collection$/) do |document_title|
  visit preview_document_path(@document_collection)
  assert_document_is_part_of_document_collection(document_title)
end
  def assert_document_is_part_of_document_collection(document_title)
    within '#document_collection' do
      assert page.has_content? document_title
    end
  end
  def preview_document_path(edition, options = {})
    query = { preview: edition.latest_edition.id, cachebust: Time.zone.now.getutc.to_i }
    document_path(edition, options.merge(query))
  end
