When /^I visit the "([^"]*)" page$/ do |path|
  visit history_path(path.parameterize)
end
Then /^I should see (#{THE_DOCUMENT})$/ do |edition|
  assert has_css?(record_css_selector(edition))
end
