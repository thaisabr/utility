Classes: 18
[name:Consultation, file:alphagov_whitehall/app/models/consultation.rb, step:Then ]
[name:DetailedGuide, file:alphagov_whitehall/app/models/detailed_guide.rb, step:Then ]
[name:DetailedGuide, file:alphagov_whitehall/app/models/detailed_guide.rb, step:When ]
[name:Document, file:alphagov_whitehall/app/models/document.rb, step:When ]
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Given ]
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Then ]
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:When ]
[name:GDS, file:null, step:Given ]
[name:NewsArticle, file:alphagov_whitehall/app/models/news_article.rb, step:Then ]
[name:Options, file:alphagov_whitehall/lib/whitehall/document_filter/options.rb, step:When ]
[name:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Given ]
[name:Policy, file:alphagov_whitehall/app/models/policy.rb, step:Then ]
[name:Publication, file:alphagov_whitehall/app/models/publication.rb, step:Then ]
[name:Speech, file:alphagov_whitehall/app/models/speech.rb, step:Then ]
[name:THE_DOCUMENT, file:null, step:When ]
[name:THE_DOCUMENT, file:null, step:Then ]
[name:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]
[name:WorldwidePriority, file:alphagov_whitehall/app/models/worldwide_priority.rb, step:Then ]

Methods: 73
[name:classify, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:constantize, type:Object, file:null, step:Given ]
[name:create, type:Object, file:null, step:Given ]
[name:create, type:SpeechType, file:alphagov_whitehall/app/models/speech_type.rb, step:Given ]
[name:detailed_guide_url, type:DetailedGuidePresenter, file:alphagov_whitehall/app/presenters/api/detailed_guide_presenter.rb, step:When ]
[name:document, type:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Then ]
[name:document_class, type:DocumentHelper, file:alphagov_whitehall/features/support/document_helper.rb, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find_by_name, type:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Given ]
[name:find_by_title, type:Object, file:null, step:When ]
[name:find_by_title!, type:Object, file:null, step:Then ]
[name:find_by_title!, type:Object, file:null, step:When ]
[name:gsub, type:Object, file:null, step:Given ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:has_css?, type:Object, file:null, step:When ]
[name:has_no_css?, type:Object, file:null, step:When ]
[name:index, type:PublicationsController, file:alphagov_whitehall/app/controllers/admin/publications_controller.rb, step:When ]
[name:index, type:PublicationsController, file:alphagov_whitehall/app/controllers/publications_controller.rb, step:When ]
[name:index, type:AnnouncementsController, file:alphagov_whitehall/app/controllers/announcements_controller.rb, step:When ]
[name:index, type:ConsultationsController, file:alphagov_whitehall/app/controllers/admin/consultations_controller.rb, step:When ]
[name:index, type:ConsultationsController, file:alphagov_whitehall/app/controllers/consultations_controller.rb, step:When ]
[name:index, type:PoliciesController, file:alphagov_whitehall/app/controllers/admin/policies_controller.rb, step:When ]
[name:index, type:PoliciesController, file:alphagov_whitehall/app/controllers/policies_controller.rb, step:When ]
[name:index, type:WorldwidePrioritiesController, file:alphagov_whitehall/app/controllers/admin/worldwide_priorities_controller.rb, step:When ]
[name:index, type:WorldwidePrioritiesController, file:alphagov_whitehall/app/controllers/worldwide_priorities_controller.rb, step:When ]
[name:latest_edition, type:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Then ]
[name:latest_edition, type:DetailedGuide, file:alphagov_whitehall/app/models/detailed_guide.rb, step:When ]
[name:login_as, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:Given ]
[name:name, type:Person, file:alphagov_whitehall/app/models/person.rb, step:Given ]
[name:name, type:SitewideSetting, file:alphagov_whitehall/app/models/sitewide_setting.rb, step:Given ]
[name:name, type:RolePresenter, file:alphagov_whitehall/app/presenters/role_presenter.rb, step:Given ]
[name:name, type:DataMigration, file:alphagov_whitehall/lib/whitehall/data_migration.rb, step:Given ]
[name:name, type:Person, file:alphagov_whitehall/app/models/person.rb, step:Then ]
[name:name, type:SitewideSetting, file:alphagov_whitehall/app/models/sitewide_setting.rb, step:Then ]
[name:name, type:RolePresenter, file:alphagov_whitehall/app/presenters/role_presenter.rb, step:Then ]
[name:name, type:DataMigration, file:alphagov_whitehall/lib/whitehall/data_migration.rb, step:Then ]
[name:new, type:EmailSignupsController, file:alphagov_whitehall/app/controllers/email_signups_controller.rb, step:When ]
[name:page, type:StatisticsAnnouncementsFilter, file:alphagov_whitehall/app/models/frontend/statistics_announcements_filter.rb, step:Then ]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:Then ]
[name:page, type:StatisticsAnnouncementsFilter, file:alphagov_whitehall/app/models/frontend/statistics_announcements_filter.rb, step:When ]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:When ]
[name:preview_path_for_attachment, type:AttachmentsHelper, file:alphagov_whitehall/app/helpers/attachments_helper.rb, step:When ]
[name:public_document_path, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:When ]
[name:public_document_url, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:When ]
[name:public_document_url, type:AuthorNotifier, file:alphagov_whitehall/app/services/service_listeners/author_notifier.rb, step:When ]
[name:publish, type:DocumentHelper, file:alphagov_whitehall/features/support/document_helper.rb, step:When ]
[name:publish, type:EditionWorkflowController, file:alphagov_whitehall/app/controllers/admin/edition_workflow_controller.rb, step:When ]
[name:published?, type:Document, file:alphagov_whitehall/app/models/document.rb, step:Then ]
[name:published?, type:RegisterableEdition, file:alphagov_whitehall/app/models/registerable_edition.rb, step:Then ]
[name:raise, type:Object, file:null, step:Then ]
[name:record_css_selector, type:Object, file:null, step:Then ]
[name:refute_flash_alerts_exist, type:DocumentHelper, file:alphagov_whitehall/features/support/document_helper.rb, step:When ]
[name:send, type:Edition, file:alphagov_whitehall/app/models/edition.rb, step:When ]
[name:title, type:EditionFilter, file:alphagov_whitehall/app/models/admin/edition_filter.rb, step:When ]
[name:title, type:ClassificationFeaturing, file:alphagov_whitehall/app/models/classification_featuring.rb, step:When ]
[name:title, type:CorporateInformationPage, file:alphagov_whitehall/app/models/corporate_information_page.rb, step:When ]
[name:title, type:RegisterableEdition, file:alphagov_whitehall/app/models/registerable_edition.rb, step:When ]
[name:title, type:FeaturePresenter, file:alphagov_whitehall/app/presenters/feature_presenter.rb, step:When ]
[name:title, type:PromotionalFeatureItemPresenter, file:alphagov_whitehall/app/presenters/promotional_feature_item_presenter.rb, step:When ]
[name:title, type:Row, file:alphagov_whitehall/lib/whitehall/uploader/row.rb, step:When ]
[name:title, type:EditionFilter, file:alphagov_whitehall/app/models/admin/edition_filter.rb, step:Then ]
[name:title, type:ClassificationFeaturing, file:alphagov_whitehall/app/models/classification_featuring.rb, step:Then ]
[name:title, type:CorporateInformationPage, file:alphagov_whitehall/app/models/corporate_information_page.rb, step:Then ]
[name:title, type:RegisterableEdition, file:alphagov_whitehall/app/models/registerable_edition.rb, step:Then ]
[name:title, type:FeaturePresenter, file:alphagov_whitehall/app/presenters/feature_presenter.rb, step:Then ]
[name:title, type:PromotionalFeatureItemPresenter, file:alphagov_whitehall/app/presenters/promotional_feature_item_presenter.rb, step:Then ]
[name:title, type:Row, file:alphagov_whitehall/lib/whitehall/uploader/row.rb, step:Then ]
[name:underscore, type:Object, file:null, step:Given ]
[name:visit_edition_admin, type:DocumentHelper, file:alphagov_whitehall/features/support/document_helper.rb, step:When ]
[name:visit_public_index_for, type:Paths, file:alphagov_whitehall/features/support/paths.rb, step:Then ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 22
alphagov_whitehall/app/views/announcements/index.html.erb
alphagov_whitehall/app/views/consultations/_document_summary.html.erb
alphagov_whitehall/app/views/consultations/show.html.erb
alphagov_whitehall/app/views/documents/_archive_notice.html.erb
alphagov_whitehall/app/views/documents/_attachment.html.erb
alphagov_whitehall/app/views/documents/_attachment_full_width.html.erb
alphagov_whitehall/app/views/documents/_change_notes.html.erb
alphagov_whitehall/app/views/documents/_document_extra_metadata.html.erb
alphagov_whitehall/app/views/documents/_document_footer_meta.html.erb
alphagov_whitehall/app/views/documents/_filter_form.html.erb
alphagov_whitehall/app/views/documents/_filter_results.html.erb
alphagov_whitehall/app/views/documents/_header.html.erb
alphagov_whitehall/app/views/documents/_metadata.html.erb
alphagov_whitehall/app/views/documents/_share_links.html.erb
alphagov_whitehall/app/views/email_signups/_form.html.erb
alphagov_whitehall/app/views/email_signups/new.html.erb
alphagov_whitehall/app/views/policies/index.html.erb
alphagov_whitehall/app/views/publications/index.html.erb
alphagov_whitehall/app/views/shared/_available_languages.html.erb
alphagov_whitehall/app/views/shared/_feeds.html.erb
alphagov_whitehall/app/views/shared/_heading.html.erb
alphagov_whitehall/app/views/worldwide_priorities/index.html.erb

