Classes: 8
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Given ]
[name:GDS, file:null, step:Given ]
[name:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Given ]
[name:StatisticsAnnouncement, file:alphagov_whitehall/app/models/frontend/statistics_announcement.rb, step:Given ]
[name:StatisticsAnnouncement, file:alphagov_whitehall/app/models/statistics_announcement.rb, step:Given ]
[name:StatisticsAnnouncement, file:alphagov_whitehall/app/models/frontend/statistics_announcement.rb, step:null]
[name:StatisticsAnnouncement, file:alphagov_whitehall/app/models/statistics_announcement.rb, step:null]
[name:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]

Methods: 16
[name:click_on, type:Object, file:null, step:When ]
[name:create, type:Object, file:null, step:Given ]
[name:display_date, type:StatisticsAnnouncement, file:alphagov_whitehall/app/models/frontend/statistics_announcement.rb, step:null]
[name:display_date, type:StatisticsAnnouncement, file:alphagov_whitehall/app/models/statistics_announcement.rb, step:null]
[name:fill_in, type:Object, file:null, step:When ]
[name:find_by_name, type:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Given ]
[name:has_css?, type:Object, file:null, step:When ]
[name:index, type:Admin/statisticsAnnouncementsController, file:alphagov_whitehall/app/controllers/admin/statistics_announcements_controller.rb, step:null]
[name:login_as, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:Given ]
[name:new, type:Admin/statisticsAnnouncementsController, file:alphagov_whitehall/app/controllers/admin/statistics_announcements_controller.rb, step:null]
[name:organisation, type:StatisticsAnnouncement, file:alphagov_whitehall/app/models/frontend/statistics_announcement.rb, step:null]
[name:organisation, type:StatisticsAnnouncement, file:alphagov_whitehall/app/models/statistics_announcement.rb, step:null]
[name:page, type:StatisticsAnnouncementsFilter, file:alphagov_whitehall/app/models/frontend/statistics_announcements_filter.rb, step:When ]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:When ]
[name:title, type:StatisticsAnnouncement, file:alphagov_whitehall/app/models/frontend/statistics_announcement.rb, step:null]
[name:title, type:StatisticsAnnouncement, file:alphagov_whitehall/app/models/statistics_announcement.rb, step:null]

Referenced pages: 6
alphagov_whitehall/app/views/admin/statistics_announcements/_filter_options.html.erb
alphagov_whitehall/app/views/admin/statistics_announcements/_form.html.erb
alphagov_whitehall/app/views/admin/statistics_announcements/_search_results.html.erb
alphagov_whitehall/app/views/admin/statistics_announcements/_warning.html.erb
alphagov_whitehall/app/views/admin/statistics_announcements/index.html.erb
alphagov_whitehall/app/views/admin/statistics_announcements/new.html.erb

