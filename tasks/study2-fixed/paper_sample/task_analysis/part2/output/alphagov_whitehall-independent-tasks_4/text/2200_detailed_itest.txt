Classes: 10
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Given ]
[name:GDS, file:null, step:Given ]
[name:Options, file:alphagov_whitehall/lib/whitehall/document_filter/options.rb, step:When ]
[name:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Given ]
[name:Publication, file:alphagov_whitehall/app/models/publication.rb, step:Then ]
[name:StatisticsAnnouncement, file:alphagov_whitehall/app/models/frontend/statistics_announcement.rb, step:Then ]
[name:StatisticsAnnouncement, file:alphagov_whitehall/app/models/statistics_announcement.rb, step:Then ]
[name:StatisticsAnnouncement, file:alphagov_whitehall/app/models/frontend/statistics_announcement.rb, step:When ]
[name:StatisticsAnnouncement, file:alphagov_whitehall/app/models/statistics_announcement.rb, step:When ]
[name:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]

Methods: 55
[name:click_on, type:Object, file:null, step:When ]
[name:create, type:Object, file:null, step:Given ]
[name:create, type:SpeechType, file:alphagov_whitehall/app/models/speech_type.rb, step:Given ]
[name:delete, type:Options, file:alphagov_whitehall/lib/whitehall/document_filter/options.rb, step:When ]
[name:display_date, type:StatisticsAnnouncement, file:alphagov_whitehall/app/models/frontend/statistics_announcement.rb, step:When ]
[name:display_date, type:StatisticsAnnouncement, file:alphagov_whitehall/app/models/statistics_announcement.rb, step:When ]
[name:evaluate_script, type:Object, file:null, step:When ]
[name:execute_script, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find, type:OrganisationFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/organisation_finder.rb, step:When ]
[name:find, type:MinisterialRolesFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/ministerial_roles_finder.rb, step:When ]
[name:find, type:NewsArticleTypeFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/news_article_type_finder.rb, step:When ]
[name:find, type:OperationalFieldFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/operational_field_finder.rb, step:When ]
[name:find, type:PublicationTypeFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/publication_type_finder.rb, step:When ]
[name:find, type:RoleAppointmentsFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/role_appointments_finder.rb, step:When ]
[name:find, type:SpeechTypeFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/speech_type_finder.rb, step:When ]
[name:find_by_name, type:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Given ]
[name:find_field, type:Object, file:null, step:When ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:has_link?, type:Object, file:null, step:Then ]
[name:index, type:Admin/statisticsAnnouncementsController, file:alphagov_whitehall/app/controllers/admin/statistics_announcements_controller.rb, step:When ]
[name:last, type:Publication, file:alphagov_whitehall/app/models/publication.rb, step:Then ]
[name:login_as, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:Given ]
[name:name, type:Person, file:alphagov_whitehall/app/models/person.rb, step:When ]
[name:name, type:SitewideSetting, file:alphagov_whitehall/app/models/sitewide_setting.rb, step:When ]
[name:name, type:RolePresenter, file:alphagov_whitehall/app/presenters/role_presenter.rb, step:When ]
[name:name, type:DataMigration, file:alphagov_whitehall/lib/whitehall/data_migration.rb, step:When ]
[name:new, type:Admin/statisticsAnnouncementsController, file:alphagov_whitehall/app/controllers/admin/statistics_announcements_controller.rb, step:When ]
[name:organisation, type:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]
[name:organisation, type:Object, file:null, step:When ]
[name:organisation, type:StatisticsAnnouncement, file:alphagov_whitehall/app/models/frontend/statistics_announcement.rb, step:When ]
[name:organisation, type:StatisticsAnnouncement, file:alphagov_whitehall/app/models/statistics_announcement.rb, step:When ]
[name:page, type:StatisticsAnnouncementsFilter, file:alphagov_whitehall/app/models/frontend/statistics_announcements_filter.rb, step:Then ]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:Then ]
[name:page, type:StatisticsAnnouncementsFilter, file:alphagov_whitehall/app/models/frontend/statistics_announcements_filter.rb, step:When ]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:When ]
[name:record_css_selector, type:Object, file:null, step:Then ]
[name:refute, type:Object, file:null, step:Then ]
[name:select, type:Javascript, file:alphagov_whitehall/features/support/javascript.rb, step:When ]
[name:select_from_chosen, type:Javascript, file:alphagov_whitehall/features/support/javascript.rb, step:When ]
[name:select_option, type:Object, file:null, step:When ]
[name:show, type:Admin/publicationsController, file:alphagov_whitehall/app/controllers/admin/publications_controller.rb, step:When ]
[name:title, type:Object, file:null, step:Then ]
[name:title, type:EditionFilter, file:alphagov_whitehall/app/models/admin/edition_filter.rb, step:Then ]
[name:title, type:ClassificationFeaturing, file:alphagov_whitehall/app/models/classification_featuring.rb, step:Then ]
[name:title, type:CorporateInformationPage, file:alphagov_whitehall/app/models/corporate_information_page.rb, step:Then ]
[name:title, type:RegisterableEdition, file:alphagov_whitehall/app/models/registerable_edition.rb, step:Then ]
[name:title, type:FeaturePresenter, file:alphagov_whitehall/app/presenters/feature_presenter.rb, step:Then ]
[name:title, type:PromotionalFeatureItemPresenter, file:alphagov_whitehall/app/presenters/promotional_feature_item_presenter.rb, step:Then ]
[name:title, type:Row, file:alphagov_whitehall/lib/whitehall/uploader/row.rb, step:Then ]
[name:title, type:StatisticsAnnouncement, file:alphagov_whitehall/app/models/frontend/statistics_announcement.rb, step:When ]
[name:title, type:StatisticsAnnouncement, file:alphagov_whitehall/app/models/statistics_announcement.rb, step:When ]
[name:to_json, type:Object, file:null, step:When ]
[name:visible?, type:Object, file:null, step:When ]
[name:within, type:Object, file:null, step:Then ]

Referenced pages: 9
alphagov_whitehall/app/views/admin/publications/_form.html.erb
alphagov_whitehall/app/views/admin/publications/_required_fields_for_imported_editions.html.erb
alphagov_whitehall/app/views/admin/publications/_subtype_fields.html.erb
alphagov_whitehall/app/views/admin/statistics_announcements/_filter_options.html.erb
alphagov_whitehall/app/views/admin/statistics_announcements/_form.html.erb
alphagov_whitehall/app/views/admin/statistics_announcements/_search_results.html.erb
alphagov_whitehall/app/views/admin/statistics_announcements/_warning.html.erb
alphagov_whitehall/app/views/admin/statistics_announcements/index.html.erb
alphagov_whitehall/app/views/admin/statistics_announcements/new.html.erb

