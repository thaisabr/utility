Given /^I am (?:an?) (writer|editor|GDS editor) in the organisation "([^"]*)"$/ do |role, organisation_name|
  organisation = Organisation.find_by_name(organisation_name) || create(:organisation, name: organisation_name)
  @user = case role
  when "writer"
    create(:policy_writer, name: "Wally Writer", organisation: organisation)
  when "editor"
    create(:departmental_editor, name: "Eddie Depteditor", organisation: organisation)
  when "GDS editor"
    create(:gds_editor, organisation: organisation)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    Edition::AuditTrail.whodunnit = user
    super(user) # warden
  end
Given(/^a statistics announcement called "(.*?)" exists$/) do |announcement_title|
  @statistics_announcement = create(:statistics_announcement, title: announcement_title)
end
When(/^I search for announcements containing "(.*?)"$/) do |keyword|
  visit admin_statistics_announcements_path
  fill_in 'Title or slug', with: keyword
  click_on 'Search'
end
Then(/^I should (see|only see) a statistics announcement called "(.*?)"$/) do |single_or_multiple, title|
  assert page.has_css?("tr.statistics_announcement", count: 1) if single_or_multiple == 'only see'
  assert page.has_css?("tr.statistics_announcement", text: title)
end
