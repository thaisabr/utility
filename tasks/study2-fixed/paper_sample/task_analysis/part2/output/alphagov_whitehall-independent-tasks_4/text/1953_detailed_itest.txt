Classes: 2
[name:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:When ]
[name:THE_DOCUMENT, file:null, step:Then ]

Methods: 53
[name:announcements_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:When ]
[name:click_on, type:Object, file:null, step:When ]
[name:create, type:Object, file:null, step:Given ]
[name:create, type:SpeechType, file:alphagov_whitehall/app/models/speech_type.rb, step:Given ]
[name:css_classes, type:PromotionalFeature, file:alphagov_whitehall/app/models/promotional_feature.rb, step:When ]
[name:document, type:FeaturePresenter, file:alphagov_whitehall/app/presenters/feature_presenter.rb, step:Given ]
[name:document, type:TagChangesProcessor, file:alphagov_whitehall/lib/data_hygiene/tag_changes_processor.rb, step:Given ]
[name:email_signup_path, type:EmailSignupHelper, file:alphagov_whitehall/app/helpers/email_signup_helper.rb, step:When ]
[name:find_by_name!, type:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:When ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:id, type:AttachmentVisibility, file:alphagov_whitehall/app/models/attachment_visibility.rb, step:When ]
[name:id, type:FeaturePresenter, file:alphagov_whitehall/app/presenters/feature_presenter.rb, step:When ]
[name:index, type:CorporateInformationPagesController, file:alphagov_whitehall/app/controllers/admin/corporate_information_pages_controller.rb, step:When ]
[name:index, type:CorporateInformationPagesController, file:alphagov_whitehall/app/controllers/corporate_information_pages_controller.rb, step:When ]
[name:items, type:HomePageList, file:alphagov_whitehall/app/models/home_page_list.rb, step:When ]
[name:items, type:PromotionalFeature, file:alphagov_whitehall/app/models/promotional_feature.rb, step:When ]
[name:mainstream_category_path, type:MainstreamCategoryRoutesHelper, file:alphagov_whitehall/app/helpers/mainstream_category_routes_helper.rb, step:When ]
[name:organisation, type:EmailSignupInformationController, file:alphagov_whitehall/app/controllers/email_signup_information_controller.rb, step:When ]
[name:organisation, type:EditionFilter, file:alphagov_whitehall/app/models/admin/edition_filter.rb, step:When ]
[name:organisation, type:StatisticsAnnouncementFilter, file:alphagov_whitehall/app/models/admin/statistics_announcement_filter.rb, step:When ]
[name:organisation, type:ImportRowWorker, file:alphagov_whitehall/app/workers/import_row_worker.rb, step:When ]
[name:organisation, type:BrokenLinkReporter, file:alphagov_whitehall/lib/whitehall/broken_link_reporter.rb, step:When ]
[name:organisation, type:FatalityNoticeRow, file:alphagov_whitehall/lib/whitehall/uploader/fatality_notice_row.rb, step:When ]
[name:organisation, type:Row, file:alphagov_whitehall/lib/whitehall/uploader/row.rb, step:When ]
[name:person, type:Appointment, file:alphagov_whitehall/app/models/edition/appointment.rb, step:When ]
[name:person, type:RoleAppointmentPresenter, file:alphagov_whitehall/app/presenters/role_appointment_presenter.rb, step:When ]
[name:policies_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:When ]
[name:promotional_feature_items, type:PromotionalFeature, file:alphagov_whitehall/app/models/promotional_feature.rb, step:When ]
[name:public_document_path, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:When ]
[name:public_document_url, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:When ]
[name:public_document_url, type:AuthorNotifier, file:alphagov_whitehall/app/services/service_listeners/author_notifier.rb, step:When ]
[name:publications_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:When ]
[name:record_css_selector, type:Object, file:null, step:Then ]
[name:see_all_link, type:Object, file:null, step:When ]
[name:show, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:When ]
[name:show, type:OrganisationsController, file:alphagov_whitehall/app/controllers/api/organisations_controller.rb, step:When ]
[name:show, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:When ]
[name:show, type:CorporateInformationPagesController, file:alphagov_whitehall/app/controllers/admin/corporate_information_pages_controller.rb, step:When ]
[name:show, type:CorporateInformationPagesController, file:alphagov_whitehall/app/controllers/corporate_information_pages_controller.rb, step:When ]
[name:show, type:ServicesAndInformationController, file:alphagov_whitehall/app/controllers/services_and_information_controller.rb, step:When ]
[name:text, type:Object, file:null, step:When ]
[name:title, type:PromotionalFeature, file:alphagov_whitehall/app/models/promotional_feature.rb, step:When ]
[name:title, type:EditionFilter, file:alphagov_whitehall/app/models/admin/edition_filter.rb, step:When ]
[name:title, type:StatisticsAnnouncementFilter, file:alphagov_whitehall/app/models/admin/statistics_announcement_filter.rb, step:When ]
[name:title, type:ClassificationFeaturing, file:alphagov_whitehall/app/models/classification_featuring.rb, step:When ]
[name:title, type:CorporateInformationPage, file:alphagov_whitehall/app/models/corporate_information_page.rb, step:When ]
[name:title, type:RegisterableEdition, file:alphagov_whitehall/app/models/registerable_edition.rb, step:When ]
[name:title, type:EmailSignupInformationPresenter, file:alphagov_whitehall/app/presenters/email_signup_information_presenter.rb, step:When ]
[name:title, type:FeaturePresenter, file:alphagov_whitehall/app/presenters/feature_presenter.rb, step:When ]
[name:title, type:PromotionalFeatureItemPresenter, file:alphagov_whitehall/app/presenters/promotional_feature_item_presenter.rb, step:When ]
[name:title, type:Row, file:alphagov_whitehall/lib/whitehall/uploader/row.rb, step:When ]
[name:visit_organisation, type:Paths, file:alphagov_whitehall/features/support/paths.rb, step:When ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 27
alphagov_whitehall/app/views/contacts/_contact.html.erb
alphagov_whitehall/app/views/corporate_information_pages/index.html.erb
alphagov_whitehall/app/views/corporate_information_pages/show.html.erb
alphagov_whitehall/app/views/corporate_information_pages/show_worldwide_organisation.html.erb
alphagov_whitehall/app/views/mainstream_categories/_list_description.html.erb
alphagov_whitehall/app/views/organisations/_corporate_information.html.erb
alphagov_whitehall/app/views/organisations/_documents.html.erb
alphagov_whitehall/app/views/organisations/_featured_items.html.erb
alphagov_whitehall/app/views/organisations/_header.html.erb
alphagov_whitehall/app/views/organisations/_promotional_feature_item.html.erb
alphagov_whitehall/app/views/organisations/show-promotional.html.erb
alphagov_whitehall/app/views/organisations/show.html.erb
alphagov_whitehall/app/views/people/_person.html.erb
alphagov_whitehall/app/views/policies/_list_description.html.erb
alphagov_whitehall/app/views/services_and_information/_collection_group.html.erb
alphagov_whitehall/app/views/services_and_information/show.html.erb
alphagov_whitehall/app/views/shared/_available_languages.html.erb
alphagov_whitehall/app/views/shared/_featured.html.erb
alphagov_whitehall/app/views/shared/_featured_links.html.erb
alphagov_whitehall/app/views/shared/_feeds.html.erb
alphagov_whitehall/app/views/shared/_govspeak_header_contents.html.erb
alphagov_whitehall/app/views/shared/_heading.html.erb
alphagov_whitehall/app/views/shared/_list_description.html.erb
alphagov_whitehall/app/views/shared/_recently_updated.html.erb
alphagov_whitehall/app/views/shared/_recently_updated_documents.html.erb
alphagov_whitehall/app/views/shared/_social_media_accounts.html.erb
alphagov_whitehall/app/views/worldwide_organisations/_header.html.erb

