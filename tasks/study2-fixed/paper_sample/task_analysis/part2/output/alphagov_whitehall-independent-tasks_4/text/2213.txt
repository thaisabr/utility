Feature: Announcing a upcoming statistics release
As an publisher of government statistics
I want to be able to announce upcoming statistics publications
So that citizens can see which statistics publications are coming soon and when they will be published.
Scenario: searching for a statistics announcement
Given I am a GDS editor in the organisation "Department for Beards"
And a statistics announcement called "MQ5 statistics" exists
And a statistics announcement called "PQ3 statistics" exists
When I search for announcements containing "MQ5"
And I should only see a statistics announcement called "MQ5 statistics"
