Classes: 4
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Given ]
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:When ]
[name:GDS, file:null, step:Given ]
[name:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]

Methods: 15
[name:click_on, type:Object, file:null, step:When ]
[name:create, type:Object, file:null, step:Given ]
[name:create, type:SpeechType, file:alphagov_whitehall/app/models/speech_type.rb, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:govspeak_with_links, type:Object, file:null, step:Given ]
[name:govspeak_with_links, type:Object, file:null, step:When ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:has_link?, type:Object, file:null, step:Then ]
[name:login_as, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:Given ]
[name:page, type:StatisticsAnnouncementsFilter, file:alphagov_whitehall/app/models/frontend/statistics_announcements_filter.rb, step:Then ]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:Then ]
[name:stub_request, type:Object, file:null, step:Given ]
[name:stub_request, type:Object, file:null, step:When ]
[name:to_return, type:Object, file:null, step:Given ]
[name:to_return, type:Object, file:null, step:When ]

Referenced pages: 0

