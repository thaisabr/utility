Classes: 4
[name:Document, file:alphagov_whitehall/app/models/document.rb, step:Given ]
[name:Document, file:alphagov_whitehall/app/models/document.rb, step:When ]
[name:RegisterableEdition, file:alphagov_whitehall/app/models/registerable_edition.rb, step:Given ]
[name:THE_DOCUMENT, file:null, step:Then ]

Methods: 23
[name:artefact_for_slug_with_a_child_tags, type:Object, file:null, step:Given ]
[name:content_api_has_an_artefact, type:Object, file:null, step:Given ]
[name:content_api_has_draft_and_live_tags, type:Object, file:null, step:Given ]
[name:create, type:Object, file:null, step:Given ]
[name:create_document_tagged_to_a_specialist_sector, type:SpecialistSectorHelper, file:alphagov_whitehall/features/support/specialist_sector_helper.rb, step:Given ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:has_no_css?, type:Object, file:null, step:Then ]
[name:map, type:Object, file:null, step:Given ]
[name:new, type:RegisterableEdition, file:alphagov_whitehall/app/models/registerable_edition.rb, step:Given ]
[name:record_css_selector, type:Object, file:null, step:Then ]
[name:slug, type:DocumentCollectionGroup, file:alphagov_whitehall/app/models/document_collection_group.rb, step:Given ]
[name:slug, type:NewsArticleType, file:alphagov_whitehall/app/models/news_article_type.rb, step:Given ]
[name:slug, type:PublicationType, file:alphagov_whitehall/app/models/publication_type.rb, step:Given ]
[name:slug, type:RegisterableEdition, file:alphagov_whitehall/app/models/registerable_edition.rb, step:Given ]
[name:slug, type:SpeechType, file:alphagov_whitehall/app/models/speech_type.rb, step:Given ]
[name:slug, type:WorldLocationType, file:alphagov_whitehall/app/models/world_location_type.rb, step:Given ]
[name:slug, type:WorldwideOfficeType, file:alphagov_whitehall/app/models/worldwide_office_type.rb, step:Given ]
[name:slug, type:WorldwideServiceType, file:alphagov_whitehall/app/models/worldwide_service_type.rb, step:Given ]
[name:slug, type:FilterOption, file:alphagov_whitehall/lib/whitehall/filter_option.rb, step:Given ]
[name:slug, type:WhipOrganisation, file:alphagov_whitehall/lib/whitehall/whip_organisation.rb, step:Given ]
[name:specialist_sectors, type:Document, file:alphagov_whitehall/app/models/document.rb, step:Given ]
[name:stub_content_api_tags, type:SpecialistSectorHelper, file:alphagov_whitehall/features/support/specialist_sector_helper.rb, step:Given ]
[name:stub_specialist_sectors, type:SpecialistSectorHelper, file:alphagov_whitehall/features/support/specialist_sector_helper.rb, step:Given ]

Referenced pages: 0

