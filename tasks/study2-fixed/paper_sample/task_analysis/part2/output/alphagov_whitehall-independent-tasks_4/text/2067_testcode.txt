Given /^I am (?:a|an) (writer|editor|admin|GDS editor|importer|managing editor)(?: called "([^"]*)")?$/ do |role, name|
  @user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Depteditor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  when 'importer'
    create(:importer)
  when 'managing editor'
    create(:managing_editor)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    Edition::AuditTrail.whodunnit = user
    super(user) # warden
  end
When /^I start drafting a new publication "([^"]*)"$/ do |title|
  begin_drafting_publication(title)
  click_button "Save"
end
When /^I draft a new publication "([^"]*)"$/ do |title|
  begin_drafting_publication(title)
  click_button "Save"
  add_external_attachment
end
Given /^"([^"]*)" drafts a new publication "([^"]*)"$/ do |user_name, title|
  user = User.find_by_name(user_name)
  as_user(user) do
    begin_drafting_publication(title)
    click_button "Save"
    add_external_attachment
  end
end
When /^I draft a new publication "([^"]*)" that does not apply to the nations:$/ do |title, nations|
  begin_drafting_publication(title)
  nations.raw.flatten.each do |nation_name|
    within record_css_selector(Nation.find_by_name!(nation_name)) do
      check nation_name
      fill_in "Alternative url", with: "http://www.#{nation_name}.com/"
    end
  end
  click_button "Save"
  add_external_attachment
end
When /^I draft a new publication "([^"]*)" referencing the data set "([^"]*)"$/ do |title, data_set_name|
  begin_drafting_publication(title)
  select data_set_name, from: "Related statistical data sets"
  click_button "Save"
  add_external_attachment
end
When /^I publish a new publication called "([^"]*)"$/ do |title|
  begin_drafting_publication(title, first_published: Date.today.to_s)
  click_button "Save"
  add_external_attachment
  publish(force: true)
end
When /^I publish a new publication of the type "([^"]*)" called "([^"]*)"$/ do |publication_type, title|
  begin_drafting_publication(title, first_published: Date.today.to_s, publication_type: publication_type)
  click_button "Save"
  add_external_attachment
  publish(force: true)
end
  def begin_drafting_publication(title, options = {})
    policy = create(:policy)
    begin_drafting_document type: 'publication', title: title, summary: "Some summary of the content", alternative_format_provider: create(:alternative_format_provider)
    fill_in_publication_fields(options)
    select policy.title, from: "Related policies"
  end
  def publish(options = {})
    if options[:force]
      click_link "Force publish"
      page.has_css?("#forcePublishModal", visible: true)
      within '#forcePublishModal' do
        fill_in 'reason', with: "because"
        click_button 'Force publish'
      end
      unless options[:ignore_errors]
        refute_flash_alerts_exist
      end
    else
      click_button "Publish"
      unless options[:ignore_errors]
        refute_flash_alerts_exist
      end
    end
  end
  def add_external_attachment
    location = current_url
    click_link "Modify attachments"
    create_external_attachment('http://www.example.com/example', 'Example doc')
    visit location
  end
  def as_user(user)
    original_user = GDS::SSO.test_user
    login_as(user)
    yield
    login_as(original_user)
  end
  def select(value, options = {})
    if options.has_key?(:from)
      element = find(:select, options[:from], visible: :all).find(:option, value, visible: :all)
      if element.visible?
        from = options.delete(:from)
        find(:select, from, options).find(:option, value, options).select_option
      else
        select_from_chosen(value, options)
      end
    else
      find(:option, value, options).select_option
    end
  end
  def select(value, options = {})
    if options.has_key?(:from)
      element = find(:select, options[:from], visible: :all).find(:option, value, visible: :all)
      if element.visible?
        from = options.delete(:from)
        find(:select, from, options).find(:option, value, options).select_option
      else
        select_from_chosen(value, options)
      end
    else
      find(:option, value, options).select_option
    end
  end
  def select_from_chosen(value, options = {})
    field = find_field(options[:from], visible: false, match: :first)
    option_value = page.evaluate_script("$(\"##{field[:id]} option:contains('#{value}')\").val()")

    if field[:multiple]
      page.execute_script("value = ['#{option_value}']\; if ($('##{field[:id]}').val()) {$.merge(value, $('##{field[:id]}').val())}")
      option_value = page.evaluate_script("value")
    end

    page.execute_script("$('##{field[:id]}').val(#{option_value.to_json})")
    page.execute_script("$('##{field[:id]}').trigger('liszt:updated').trigger('change')")
  end
  def refute_flash_alerts_exist
    assert has_no_css?(".flash.alert")
  end
  def create_external_attachment(url, attachment_title)
    click_on 'Add new external attachment'
    fill_in 'Title', with: attachment_title
    fill_in 'External url', with: url
    click_on 'Save'
    Attachment.find_by_title(attachment_title)
  end
  def select_from_chosen(value, options = {})
    field = find_field(options[:from], visible: false, match: :first)
    option_value = page.evaluate_script("$(\"##{field[:id]} option:contains('#{value}')\").val()")

    if field[:multiple]
      page.execute_script("value = ['#{option_value}']\; if ($('##{field[:id]}').val()) {$.merge(value, $('##{field[:id]}').val())}")
      option_value = page.evaluate_script("value")
    end

    page.execute_script("$('##{field[:id]}').val(#{option_value.to_json})")
    page.execute_script("$('##{field[:id]}').trigger('liszt:updated').trigger('change')")
  end
When(/^I start editing the attachments from the .*? page$/) do
  click_on 'Modify attachments'
end
When(/^I upload a file attachment with the title "(.*?)" and the file "(.*?)"$/) do |title, fixture_file_name|
  click_on 'Upload new file attachment'
  fill_in 'Title', with: title
  attach_file 'File', Rails.root + "test/fixtures/#{fixture_file_name}"
  click_on 'Save'
end
When(/^I upload an html attachment with the title "(.*?)" and the body "(.*?)"$/) do |title, body|
  click_on 'Add new HTML attachment'
  fill_in 'Title', with: title
  fill_in 'Body', with: body
  check 'Manually numbered headings'
  click_on 'Save'
end
When(/^I add an external attachment with the title "(.*?)" and the URL "(.*?)"$/) do |title, url|
  create_external_attachment(url, title)
end
Then(/^the .* "(.*?)" should have (\d+) attachments$/) do |title, expected_number_of_attachments|
  assert_equal expected_number_of_attachments.to_i, Edition.find_by_title(title).attachments.count
end
When(/^I set the order of attachments to:$/) do |attachment_order|
  attachment_order.hashes.each do |attachment_info|
    attachment = Attachment.find_by_title(attachment_info[:title])
    fill_in "ordering[#{attachment.id}]", with: attachment_info[:order]
  end
  click_on 'Save attachment order'
end
Then(/^the attachments should be in the following order:$/) do |attachment_list|

  attachment_ids = page.all('.existing-attachments > li').map {|element| element[:id] }

  attachment_list.hashes.each_with_index do |attachment_info, index|
    attachment = Attachment.find_by_title(attachment_info[:title])

    assert_equal "attachment_#{attachment.id}", attachment_ids[index]
  end
end
When /^I draft a new publication "([^"]*)" relating it to topical event "([^"]*)"$/ do |publication_title, topical_event_name|
  begin_drafting_publication publication_title
  select topical_event_name, from: "Topical events"
  click_button "Save"
  add_external_attachment
end
  def begin_drafting_publication(title, options = {})
    policy = create(:policy)
    begin_drafting_document type: 'publication', title: title, summary: "Some summary of the content", alternative_format_provider: create(:alternative_format_provider)
    fill_in_publication_fields(options)
    select policy.title, from: "Related policies"
  end
