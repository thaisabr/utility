Given /^a published policy "([^"]*)" relevant to local government$/ do |title|
  @policy = create(:published_policy, title: title, relevant_to_local_government: true)
end
When(/^I filter the announcements list by "(.*?)"$/) do |announcement_type|
  visit announcements_path
  select announcement_type, from: "Announcement type"
  click_on "Refresh results"
end
  def select(value, options = {})
    if options.has_key?(:from)
      element = find(:select, options[:from], visible: :all).find(:option, value, visible: :all)
      if element.visible?
        from = options.delete(:from)
        find(:select, from, options).find(:option, value, options).select_option
      else
        select_from_chosen(value, options)
      end
    else
      find(:option, value, options).select_option
    end
  end
  def select_from_chosen(value, options = {})
    field = find_field(options[:from], visible: false, match: :first)
    option_value = page.evaluate_script("$(\"##{field[:id]} option:contains('#{value}')\").val()")

    if field[:multiple]
      page.execute_script("value = ['#{option_value}']\; if ($('##{field[:id]}').val()) {$.merge(value, $('##{field[:id]}').val())}")
      option_value = page.evaluate_script("value")
    end

    page.execute_script("$('##{field[:id]}').val(#{option_value.to_json})")
    page.execute_script("$('##{field[:id]}').trigger('liszt:updated').trigger('change')")
  end
Given(/^govuk delivery exists$/) do
  mock_govuk_delivery_client
end
When(/^I sign up for emails, checking the relevant to local government box$/) do
  within '.feeds' do
    click_on 'email'
  end

  check 'Only include results relevant to local government'
  click_on 'Create subscription'
end
Then(/^I should be signed up for the local government news stories mailing list$/) do
  assert_signed_up_to_mailing_list("/government/announcements.atom?announcement_filter_option=news-stories&relevant_to_local_government=1", "news stories which are relevant to local government")
end
Then(/^a govuk_delivery notification should have been sent to the mailing list I signed up for$/) do
  mock_govuk_delivery_client.assert_method_called(:notify, with: ->(feed_urls, _subject, _body) {
    feed_urls.include?(@feed_signed_up_to)
  })
end
def mock_govuk_delivery_client
  @mock_client ||= RetrospectiveStub.new.tap { |mock_client|
    mock_client.stub :topic
    mock_client.stub :signup_url, returns: public_url("/email_signup_url")
    mock_client.stub :notify
    Whitehall.stubs(govuk_delivery_client: mock_client)
  }
end
def assert_signed_up_to_mailing_list(feed_path, description)
  @feed_signed_up_to = public_url(feed_path)
  mock_govuk_delivery_client.assert_method_called(:topic, with: [@feed_signed_up_to, description])
  mock_govuk_delivery_client.assert_method_called(:signup_url, with: [@feed_signed_up_to])
end
  def assert_method_called(method, opts = {})
    raise UnsatisfiedAssertion.new("Expected :#{method} to have been called, but wasn't\n\nCalls: \n#{inspect_calls}") unless @calls.any? { | call |
      call[:method] == method
    }

    if opts[:with].present?
      raise UnsatisfiedAssertion.new("Expected :#{method} to have been called #{inspect_args opts[:with]}, but wasn't\n\nCalls: \n#{inspect_calls}") unless @calls.any? { | call |
        call[:method] == method && (
          opts[:with].is_a?(Proc) ? opts[:with].call(*call[:args]) : opts[:with] == call[:args]
        )
      }
    end
  end
  def stub(method, opts = {})
    stubs << {
      method: method,
      with: opts[:with],
      returns: opts[:returns]
    }
  end
  def inspect_calls
    calls.map { |call|
      ":#{call[:method]}, Arguments: #{call[:args]}"
    }.join "\n"
  end
  def inspect_args(args)
    if args.is_a? Proc
      return "matching block: #{args.source}"
    else
      "with: #{args.inspect}"
    end
  end
  def public_url(path)
    (Plek.new.website_uri + path).to_s
  end
When /^I publish (#{THE_DOCUMENT})$/ do |edition|
  visit_edition_admin edition.title
  publish
end
  def visit_edition_admin(title, scope = :scoped)
    document = Edition.send(scope).find_by_title(title)
    visit admin_edition_path(document)
  end
  def publish(options = {})
    if options[:force]
      click_link "Force publish"
      page.has_css?("#forcePublishModal", visible: true)
      within '#forcePublishModal' do
        fill_in 'reason', with: "because"
        click_button 'Force publish'
      end
      unless options[:ignore_errors]
        refute_flash_alerts_exist
      end
    else
      click_button "Publish"
      unless options[:ignore_errors]
        refute_flash_alerts_exist
      end
    end
  end
  def refute_flash_alerts_exist
    assert has_no_css?(".flash.alert")
  end
  def admin_edition_path(edition, *args)
    polymorphic_path([:admin, edition], *args)
  end
Given /^I am (?:a|an) (writer|editor|admin|GDS editor|importer|managing editor)(?: called "([^"]*)")?$/ do |role, name|
  @user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Depteditor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  when 'importer'
    create(:importer)
  when 'managing editor'
    create(:managing_editor)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    Edition::AuditTrail.whodunnit = user
    super(user) # warden
  end
