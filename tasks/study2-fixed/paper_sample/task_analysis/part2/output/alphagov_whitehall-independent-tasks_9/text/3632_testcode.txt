Given /^a document series "([^"]*)"$/ do |name|
  create(:document_series, name: name)
end
When /^I go to speed tag a newly imported (publication|speech|news article)(?: "(.*?)")?(?: for "(.*?)"(?: and "(.*?)")?)?$/ do |edition_type, title, organisation1_name, organisation2_name|
  organisations = []
  organisations << find_or_create_organisation(organisation1_name) if organisation1_name
  organisations << find_or_create_organisation(organisation2_name) if organisation2_name
  @edition = create("imported_#{edition_type.sub(' ', '_')}", organisations: organisations, title: title || "default title")
  visit admin_edition_path(@edition)
end
Then /^I should be able to select the document series "([^"]*)"$/ do |name|
  select name, from: 'Document series'
end
  def find_or_create_organisation(name)
    Organisation.find_by_name(name) || create(:organisation, name: name)
  end
  def admin_edition_path(edition, *args)
    polymorphic_path([:admin, edition], *args)
  end
Given /^I am (?:a|an) (writer|editor|admin|GDS editor|importer)(?: called "([^"]*)")?$/ do |role, name|
  @user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Editor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  when 'importer'
    create(:importer)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    Edition::AuditTrail.whodunnit = user
    super(user) # warden
  end
