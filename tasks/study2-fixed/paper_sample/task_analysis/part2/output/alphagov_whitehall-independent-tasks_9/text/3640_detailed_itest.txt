Classes: 12
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Given ]
[name:GDS, file:null, step:Given ]
[name:Locale, file:alphagov_whitehall/lib/locale.rb, step:Given ]
[name:Locale, file:alphagov_whitehall/lib/locale.rb, step:When ]
[name:THE_DOCUMENT, file:null, step:Then ]
[name:Translation, file:alphagov_whitehall/lib/whitehall/translation.rb, step:When ]
[name:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]
[name:WorldLocation, file:alphagov_whitehall/app/models/world_location.rb, step:When ]
[name:WorldLocation, file:alphagov_whitehall/app/models/world_location.rb, step:Then ]
[name:WorldLocationType, file:alphagov_whitehall/app/models/world_location_type.rb, step:Given ]
[name:WorldwideOrganisation, file:alphagov_whitehall/app/models/worldwide_organisation.rb, step:When ]
[name:WorldwideOrganisation, file:alphagov_whitehall/app/models/worldwide_organisation.rb, step:Then ]

Methods: 77
[name:add_translation_to_world_location, type:Object, file:null, step:Given ]
[name:add_translation_to_world_location, type:WorldLocationSteps, file:alphagov_whitehall/features/step_definitions/world_location_steps.rb, step:When ]
[name:add_translation_to_worldwide_organisation, type:WorldwideOrganisationSteps, file:alphagov_whitehall/features/step_definitions/worldwide_organisation_steps.rb, step:When ]
[name:announcements_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:null]
[name:announcements_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:When ]
[name:click_link, type:Object, file:null, step:Given ]
[name:click_link, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:Then ]
[name:click_on, type:Object, file:null, step:When ]
[name:code, type:Object, file:null, step:When ]
[name:code, type:Object, file:null, step:Given ]
[name:create, type:Object, file:null, step:Given ]
[name:current_person, type:Role, file:alphagov_whitehall/app/models/role.rb, step:When ]
[name:current_person, type:RolePresenter, file:alphagov_whitehall/app/presenters/role_presenter.rb, step:When ]
[name:edit, type:Admin/worldLocationsController, file:alphagov_whitehall/app/controllers/admin/world_locations_controller.rb, step:When ]
[name:edit, type:Admin/worldLocationTranslationsController, file:alphagov_whitehall/app/controllers/admin/world_location_translations_controller.rb, step:When ]
[name:edit, type:Admin/worldwideOrganisationsController, file:alphagov_whitehall/app/controllers/admin/worldwide_organisations_controller.rb, step:When ]
[name:edit, type:Admin/worldwideOrganisationsTranslationsController, file:alphagov_whitehall/app/controllers/admin/worldwide_organisations_translations_controller.rb, step:When ]
[name:edit_translation_for_worldwide_organisation, type:WorldwideOrganisationSteps, file:alphagov_whitehall/features/step_definitions/worldwide_organisation_steps.rb, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find_by_language_name, type:Locale, file:alphagov_whitehall/lib/locale.rb, step:Given ]
[name:find_by_name!, type:WorldLocation, file:alphagov_whitehall/app/models/world_location.rb, step:When ]
[name:find_by_name!, type:WorldLocation, file:alphagov_whitehall/app/models/world_location.rb, step:Then ]
[name:find_by_name!, type:WorldwideOrganisation, file:alphagov_whitehall/app/models/worldwide_organisation.rb, step:When ]
[name:find_by_name!, type:WorldwideOrganisation, file:alphagov_whitehall/app/models/worldwide_organisation.rb, step:Then ]
[name:first, type:Object, file:null, step:Then ]
[name:gsub, type:Object, file:null, step:Given ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:index, type:Admin/worldLocationsController, file:alphagov_whitehall/app/controllers/admin/world_locations_controller.rb, step:When ]
[name:index, type:Admin/worldLocationTranslationsController, file:alphagov_whitehall/app/controllers/admin/world_location_translations_controller.rb, step:When ]
[name:index, type:Admin/worldwideOrganisationsController, file:alphagov_whitehall/app/controllers/admin/worldwide_organisations_controller.rb, step:When ]
[name:index, type:Admin/worldwideOrganisationsTranslationsController, file:alphagov_whitehall/app/controllers/admin/worldwide_organisations_translations_controller.rb, step:When ]
[name:login_as, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:Given ]
[name:name, type:WorldLocation, file:alphagov_whitehall/app/models/world_location.rb, step:When ]
[name:name, type:WorldwideOrganisation, file:alphagov_whitehall/app/models/worldwide_organisation.rb, step:When ]
[name:native_language_name, type:Locale, file:alphagov_whitehall/lib/locale.rb, step:When ]
[name:new, type:Admin/worldwideOrganisationsController, file:alphagov_whitehall/app/controllers/admin/worldwide_organisations_controller.rb, step:When ]
[name:page, type:DetailedGuidePresenter, file:alphagov_whitehall/app/presenters/api/detailed_guide_presenter.rb, step:Then ]
[name:person, type:Appointment, file:alphagov_whitehall/app/models/edition/appointment.rb, step:When ]
[name:person, type:RoleAppointmentPresenter, file:alphagov_whitehall/app/presenters/role_appointment_presenter.rb, step:When ]
[name:policies_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:null]
[name:policies_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:When ]
[name:public_document_path, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:null]
[name:public_document_path, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:When ]
[name:public_document_url, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:null]
[name:public_document_url, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:When ]
[name:publications_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:null]
[name:publications_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:When ]
[name:record_css_selector, type:Object, file:null, step:Then ]
[name:record_css_selector, type:Object, file:null, step:When ]
[name:rows_hash, type:Object, file:null, step:When ]
[name:rows_hash, type:Object, file:null, step:Then ]
[name:select, type:Object, file:null, step:When ]
[name:show, type:WorldLocationsController, file:alphagov_whitehall/app/controllers/admin/world_locations_controller.rb, step:null]
[name:show, type:WorldLocationsController, file:alphagov_whitehall/app/controllers/world_locations_controller.rb, step:null]
[name:show, type:WorldLocationsController, file:alphagov_whitehall/app/controllers/admin/world_locations_controller.rb, step:When ]
[name:show, type:WorldLocationsController, file:alphagov_whitehall/app/controllers/world_locations_controller.rb, step:When ]
[name:show, type:Admin/worldwideOrganisationsController, file:alphagov_whitehall/app/controllers/admin/worldwide_organisations_controller.rb, step:When ]
[name:show, type:WorldwideOrganisationsController, file:alphagov_whitehall/app/controllers/admin/worldwide_organisations_controller.rb, step:When ]
[name:show, type:WorldwideOrganisationsController, file:alphagov_whitehall/app/controllers/worldwide_organisations_controller.rb, step:When ]
[name:summary, type:WorldwideOrganisation, file:alphagov_whitehall/app/models/worldwide_organisation.rb, step:When ]
[name:title, type:WorldwidePriority, file:alphagov_whitehall/app/models/worldwide_priority.rb, step:null]
[name:title, type:WorldwidePriority, file:alphagov_whitehall/app/models/worldwide_priority.rb, step:When ]
[name:to_sym, type:Object, file:null, step:Given ]
[name:url, type:WorldwidePriority, file:alphagov_whitehall/app/models/worldwide_priority.rb, step:null]
[name:url, type:WorldwidePriority, file:alphagov_whitehall/app/models/worldwide_priority.rb, step:When ]
[name:url_for, type:Object, file:null, step:When ]
[name:within, type:Object, file:null, step:When ]
[name:within, type:Object, file:null, step:Then ]
[name:world_location_type, type:WorldLocation, file:alphagov_whitehall/app/models/world_location.rb, step:When ]
[name:world_locations, type:NewsArticleRow, file:alphagov_whitehall/lib/whitehall/uploader/news_article_row.rb, step:Then ]
[name:world_locations, type:PublicationRow, file:alphagov_whitehall/lib/whitehall/uploader/publication_row.rb, step:Then ]
[name:world_locations, type:SpeechRow, file:alphagov_whitehall/lib/whitehall/uploader/speech_row.rb, step:Then ]
[name:worldwide_organisation, type:WorldwideOfficesController, file:alphagov_whitehall/app/controllers/admin/worldwide_offices_controller.rb, step:null]
[name:worldwide_organisation, type:WorldwideOrganisationsTranslationsController, file:alphagov_whitehall/app/controllers/admin/worldwide_organisations_translations_controller.rb, step:null]
[name:worldwide_organisation, type:WorldwideOfficesController, file:alphagov_whitehall/app/controllers/admin/worldwide_offices_controller.rb, step:When ]
[name:worldwide_organisation, type:WorldwideOrganisationsTranslationsController, file:alphagov_whitehall/app/controllers/admin/worldwide_organisations_translations_controller.rb, step:When ]

Referenced pages: 28
alphagov_whitehall/app/views/admin/editions/_govspeak_help.html.erb
alphagov_whitehall/app/views/admin/shared/_mainstream_link_fields.html.erb
alphagov_whitehall/app/views/admin/world_location_translations/edit.html.erb
alphagov_whitehall/app/views/admin/world_location_translations/index.html.erb
alphagov_whitehall/app/views/admin/world_locations/edit.html.erb
alphagov_whitehall/app/views/admin/world_locations/index.html.erb
alphagov_whitehall/app/views/admin/worldwide_organisations/_form.html.erb
alphagov_whitehall/app/views/admin/worldwide_organisations/_sidebar.html.erb
alphagov_whitehall/app/views/admin/worldwide_organisations/edit.html.erb
alphagov_whitehall/app/views/admin/worldwide_organisations/index.html.erb
alphagov_whitehall/app/views/admin/worldwide_organisations/new.html.erb
alphagov_whitehall/app/views/admin/worldwide_organisations/show.html.erb
alphagov_whitehall/app/views/admin/worldwide_organisations_translations/edit.html.erb
alphagov_whitehall/app/views/admin/worldwide_organisations_translations/index.html.erb
alphagov_whitehall/app/views/announcements/_list_description.html.erb
alphagov_whitehall/app/views/contacts/_contact.html.erb
alphagov_whitehall/app/views/people/_person.html.erb
alphagov_whitehall/app/views/policies/_list_description.html.erb
alphagov_whitehall/app/views/publications/_list_description.html.erb
alphagov_whitehall/app/views/shared/_available_languages.html.erb
alphagov_whitehall/app/views/shared/_featured_news.html.erb
alphagov_whitehall/app/views/shared/_feeds.html.erb
alphagov_whitehall/app/views/shared/_recently_updated.html.erb
alphagov_whitehall/app/views/shared/_recently_updated_documents.html.erb
alphagov_whitehall/app/views/shared/_social_media_accounts.html.erb
alphagov_whitehall/app/views/world_locations/show.html.erb
alphagov_whitehall/app/views/worldwide_organisations/_worldwide_organisation.html.erb
alphagov_whitehall/app/views/worldwide_organisations/show.html.erb

