Classes: 8
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Then ]
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Given ]
[name:GDS, file:null, step:Given ]
[name:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:When ]
[name:THE_DOCUMENT, file:null, step:Then ]
[name:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]
[name:WorldwidePriority, file:alphagov_whitehall/app/models/worldwide_priority.rb, step:When ]
[name:WorldwidePriority, file:alphagov_whitehall/app/models/worldwide_priority.rb, step:Given ]

Methods: 61
[name:activity, type:WorldwidePrioritiesController, file:alphagov_whitehall/app/controllers/admin/worldwide_priorities_controller.rb, step:Given ]
[name:activity, type:WorldwidePrioritiesController, file:alphagov_whitehall/app/controllers/worldwide_priorities_controller.rb, step:Given ]
[name:admin_edition_path, type:EditionRoutesHelper, file:alphagov_whitehall/app/helpers/admin/edition_routes_helper.rb, step:When ]
[name:admin_edition_url, type:EditionRoutesHelper, file:alphagov_whitehall/app/helpers/admin/edition_routes_helper.rb, step:When ]
[name:admin_supporting_page_path, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:When ]
[name:admin_supporting_page_url, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:When ]
[name:begin_drafting_document, type:DocumentHelper, file:alphagov_whitehall/features/support/document_helper.rb, step:When ]
[name:begin_drafting_policy, type:DocumentHelper, file:alphagov_whitehall/features/support/document_helper.rb, step:When ]
[name:classify, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:constantize, type:Object, file:null, step:Given ]
[name:count, type:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:When ]
[name:create, type:Object, file:null, step:Given ]
[name:create, type:Object, file:null, step:When ]
[name:document, type:Object, file:null, step:When ]
[name:document_class, type:DocumentHelper, file:alphagov_whitehall/features/support/document_helper.rb, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:fill_in_change_note_if_required, type:DocumentHelper, file:alphagov_whitehall/features/support/document_helper.rb, step:When ]
[name:find_by_title!, type:WorldwidePriority, file:alphagov_whitehall/app/models/worldwide_priority.rb, step:When ]
[name:find_by_title!, type:WorldwidePriority, file:alphagov_whitehall/app/models/worldwide_priority.rb, step:Given ]
[name:find_by_title!, type:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Then ]
[name:first, type:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:When ]
[name:gsub, type:Object, file:null, step:Given ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:has_css?, type:Object, file:null, step:When ]
[name:index, type:Admin/editionsController, file:alphagov_whitehall/app/controllers/admin/editions_controller.rb, step:When ]
[name:login_as, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:Given ]
[name:merge, type:Object, file:null, step:When ]
[name:name, type:Person, file:alphagov_whitehall/app/models/person.rb, step:Given ]
[name:name, type:RolePresenter, file:alphagov_whitehall/app/presenters/role_presenter.rb, step:Given ]
[name:name, type:DataMigration, file:alphagov_whitehall/lib/whitehall/data_migration.rb, step:Given ]
[name:name, type:Person, file:alphagov_whitehall/app/models/person.rb, step:When ]
[name:name, type:RolePresenter, file:alphagov_whitehall/app/presenters/role_presenter.rb, step:When ]
[name:name, type:DataMigration, file:alphagov_whitehall/lib/whitehall/data_migration.rb, step:When ]
[name:name, type:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:When ]
[name:new, type:Admin/policiesController, file:alphagov_whitehall/app/controllers/admin/policies_controller.rb, step:When ]
[name:new, type:Admin/publicationsController, file:alphagov_whitehall/app/controllers/admin/publications_controller.rb, step:When ]
[name:new, type:Admin/newsArticlesController, file:alphagov_whitehall/app/controllers/admin/news_articles_controller.rb, step:When ]
[name:new, type:Admin/fatalityNoticesController, file:alphagov_whitehall/app/controllers/admin/fatality_notices_controller.rb, step:When ]
[name:new, type:Admin/consultationsController, file:alphagov_whitehall/app/controllers/admin/consultations_controller.rb, step:When ]
[name:new, type:Admin/speechesController, file:alphagov_whitehall/app/controllers/admin/speeches_controller.rb, step:When ]
[name:new, type:Admin/detailedGuidesController, file:alphagov_whitehall/app/controllers/admin/detailed_guides_controller.rb, step:When ]
[name:new, type:Admin/worldwidePrioritiesController, file:alphagov_whitehall/app/controllers/admin/worldwide_priorities_controller.rb, step:When ]
[name:new, type:Admin/caseStudiesController, file:alphagov_whitehall/app/controllers/admin/case_studies_controller.rb, step:When ]
[name:new, type:Admin/statisticalDataSetsController, file:alphagov_whitehall/app/controllers/admin/statistical_data_sets_controller.rb, step:When ]
[name:new, type:Admin/worldLocationNewsArticlesController, file:alphagov_whitehall/app/controllers/admin/world_location_news_articles_controller.rb, step:When ]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:Then ]
[name:record_css_selector, type:Object, file:null, step:Then ]
[name:select, type:Object, file:null, step:When ]
[name:set_lead_organisation_on_document, type:DocumentHelper, file:alphagov_whitehall/features/support/document_helper.rb, step:When ]
[name:title, type:CorporateInformationPage, file:alphagov_whitehall/app/models/corporate_information_page.rb, step:Then ]
[name:title, type:AttachmentsPresenter, file:alphagov_whitehall/app/presenters/attachments_presenter.rb, step:Then ]
[name:title, type:Row, file:alphagov_whitehall/lib/whitehall/uploader/row.rb, step:Then ]
[name:title, type:StatisticalDataSetRow, file:alphagov_whitehall/lib/whitehall/uploader/statistical_data_set_row.rb, step:Then ]
[name:title, type:CorporateInformationPage, file:alphagov_whitehall/app/models/corporate_information_page.rb, step:When ]
[name:title, type:AttachmentsPresenter, file:alphagov_whitehall/app/presenters/attachments_presenter.rb, step:When ]
[name:title, type:Row, file:alphagov_whitehall/lib/whitehall/uploader/row.rb, step:When ]
[name:title, type:StatisticalDataSetRow, file:alphagov_whitehall/lib/whitehall/uploader/statistical_data_set_row.rb, step:When ]
[name:titleize, type:Object, file:null, step:When ]
[name:underscore, type:Object, file:null, step:Given ]

Referenced pages: 44
alphagov_whitehall/app/views/admin/case_studies/_form.html.erb
alphagov_whitehall/app/views/admin/case_studies/_required_fields_for_imported_editions.html.erb
alphagov_whitehall/app/views/admin/consultations/_additional_significant_fields.html.erb
alphagov_whitehall/app/views/admin/consultations/_date_fields.html.erb
alphagov_whitehall/app/views/admin/consultations/_edition.html.erb
alphagov_whitehall/app/views/admin/consultations/_form.html.erb
alphagov_whitehall/app/views/admin/consultations/_required_fields_for_imported_editions.html.erb
alphagov_whitehall/app/views/admin/detailed_guides/_edition.html.erb
alphagov_whitehall/app/views/admin/detailed_guides/_form.html.erb
alphagov_whitehall/app/views/admin/editions/index.html.erb
alphagov_whitehall/app/views/admin/fatality_notices/_form.html.erb
alphagov_whitehall/app/views/admin/news_articles/_additional_significant_fields.html.erb
alphagov_whitehall/app/views/admin/news_articles/_form.html.erb
alphagov_whitehall/app/views/admin/news_articles/_news_article_type_fields.html.erb
alphagov_whitehall/app/views/admin/news_articles/_required_fields_for_imported_editions.html.erb
alphagov_whitehall/app/views/admin/policies/_form.html.erb
alphagov_whitehall/app/views/admin/publications/_additional_significant_fields.html.erb
alphagov_whitehall/app/views/admin/publications/_edition.html.erb
alphagov_whitehall/app/views/admin/publications/_form.html.erb
alphagov_whitehall/app/views/admin/publications/_html_version_fields.html.erb
alphagov_whitehall/app/views/admin/publications/_publication_date_fields.html.erb
alphagov_whitehall/app/views/admin/publications/_required_fields_for_imported_editions.html.erb
alphagov_whitehall/app/views/admin/shared/_govdelivery_fields.html.erb
alphagov_whitehall/app/views/admin/speeches/_additional_significant_fields.html.erb
alphagov_whitehall/app/views/admin/speeches/_delivered_by_fields.html.erb
alphagov_whitehall/app/views/admin/speeches/_delivered_on_fields.html.erb
alphagov_whitehall/app/views/admin/speeches/_form.html.erb
alphagov_whitehall/app/views/admin/speeches/_required_fields_for_imported_editions.html.erb
alphagov_whitehall/app/views/admin/speeches/_speech_details.html.erb
alphagov_whitehall/app/views/admin/speeches/_speech_type_fields.html.erb
alphagov_whitehall/app/views/admin/statistical_data_sets/_form.html.erb
alphagov_whitehall/app/views/admin/statistical_data_sets/_list.html.erb
alphagov_whitehall/app/views/admin/statistical_data_sets/_required_fields_for_imported_editions.html.erb
alphagov_whitehall/app/views/admin/world_location_news_articles/_form.html.erb
alphagov_whitehall/app/views/admin/worldwide_priorities/_form.html.erb
alphagov_whitehall/app/views/documents/_attachment.html.erb
alphagov_whitehall/app/views/documents/_attachments.html.erb
alphagov_whitehall/app/views/documents/_change_notes.html.erb
alphagov_whitehall/app/views/documents/_document_extra_metadata.html.erb
alphagov_whitehall/app/views/documents/_header.html.erb
alphagov_whitehall/app/views/organisations/_organisations_logo_list.html.erb
alphagov_whitehall/app/views/shared/_available_languages.html.erb
alphagov_whitehall/app/views/worldwide_priorities/_navigation.html.erb
alphagov_whitehall/app/views/worldwide_priorities/activity.html.erb

