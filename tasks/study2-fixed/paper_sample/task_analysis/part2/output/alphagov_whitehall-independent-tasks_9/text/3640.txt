Feature: Providing translated content from gov.uk/government
As someone interested in the foreign activities of the UK government who is not a native english speaker
I want to be able to read information about the UK government in my own language
So that I can better understand it's relationship to the locales that I am interested in
Scenario: Maintaining locale between pages
Given I am viewing a world location that is translated
When I visit a world organisation associated with that locale that is also translated
Then I should see the translation of that world organisation
Feature: Administering world location information
Background:
Given I am an admin
Scenario: Adding a new translation
Given a country "Afrolasia" exists with the mission statement "The UK has a long-standing relationship with Afrolasia"
When I add a new translation to the country "Afrolasia" with:
| locale            | Français                                                    |
| name              | Afrolasie                                                   |
| title             | UK en Afrolasia                                             |
| mission_statement | Le Royaume-Uni a une relation de longue date avec Afrolasie |
Then when viewing the country "Afrolasia" with the locale "Français" I should see:
| name              | Afrolasie                                                   |
| title             | UK en Afrolasia                                             |
| mission_statement | Le Royaume-Uni a une relation de longue date avec Afrolasie |
Scenario: Editing an existing translation
Given a country "Afrolasia" exists with a translation for the locale "Français"
When I edit the "Français" translation for "Afrolasia" setting:
| locale            | Français                                                    |
| name              | Afrolandie                                                  |
| title             | UK en Afrolandie                                            |
| mission_statement | Enseigner aux gens comment infuser le thé                   |
Then when viewing the country "Afrolasia" with the locale "Français" I should see:
| name              | Afrolandie                                                  |
| title             | UK en Afrolandie                                            |
| mission_statement | Enseigner aux gens comment infuser le thé                   |
Feature: Administering worldwide organisation
As a citizen interested in UK gov activity around the world, I want there to be profiles of the world organisation (eg embassies, DFID offices, UKTI branches) in each worldwide location, so I can see which organisation are active in each location and read more about them.
Acceptance criteria:
* Each world organisation has:
* a unique name e.g. "British Embassy in Madrid" and a URL "/world/offices/british-embassy-in-madrid" which is generated from the name
* a text short summary and markdown long description.
* multiple social media links (like orgs)
* multiple sets of office information (like orgs)
* with the addition of a list of services (chosen from a set) that the office provides
* a logo formatted name (always using the standard HMG crest for now)
* Each world organisation can be associated with 1+ world locations, and shows on the world locations page to which they are associated (see mock up on the [ticket](https://www.pivotaltracker.com/story/show/41026113))
* Each can have corporate information pages (like orgs)
Background:
Given I am a GDS editor
Scenario: Adding a new translation
Given a worldwide organisation "Department of Beards in France" exists for the country "France" with translations into "fr"
When I add a new translation to the worldwide organisation "Department of Beards in France" with:
| locale      | Français                                          |
| name        | Département des barbes en France                  |
| summary     | Nous nous occupons de la pilosité faciale du pays |
| description | Barbes, moustaches, même rouflaquettes            |
| services    | Montante, pommades, humide rase                   |
Then when viewing the worldwide organisation "Department of Beards in France" with the locale "Français" I should see:
| name        | Département des barbes en France                  |
| summary     | Nous nous occupons de la pilosité faciale du pays |
| description | Barbes, moustaches, même rouflaquettes            |
| services    | Montante, pommades, humide rase                   |
Scenario: Editing an existing translation
Given a worldwide organisation "Department of Beards in France" exists with a translation for the locale "Français"
When I edit the "Français" translation for the worldwide organisation "Department of Beards in France" setting:
| name        | Le super département des barbes en France         |
| summary     | Nous nous occupons de la pilosité faciale du pays |
| description | Barbes, moustaches, même rouflaquettes            |
| services    | Montante, pommades, humide rase                   |
Then when viewing the worldwide organisation "Department of Beards in France" with the locale "Français" I should see:
| name        | Le super département des barbes en France         |
| summary     | Nous nous occupons de la pilosité faciale du pays |
| description | Barbes, moustaches, même rouflaquettes            |
| services    | Montante, pommades, humide rase                   |
