Classes: 7
[name:Document, file:alphagov_whitehall/app/models/document.rb, step:Then ]
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Given ]
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:When ]
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Then ]
[name:GDS, file:null, step:Given ]
[name:URI, file:null, step:Then ]
[name:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]

Methods: 59
[name:about, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:Then ]
[name:about, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:Then ]
[name:alternative_url, type:Object, file:null, step:Then ]
[name:announcements_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:Then ]
[name:assert_current_url, type:Assertions, file:alphagov_whitehall/features/support/assertions.rb, step:Then ]
[name:assert_equal, type:Object, file:null, step:Then ]
[name:block_given?, type:Object, file:null, step:When ]
[name:check, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:create, type:Object, file:null, step:Given ]
[name:current_url, type:Object, file:null, step:Then ]
[name:fill_in, type:Object, file:null, step:When ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:Then ]
[name:index, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:Then ]
[name:index, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:Then ]
[name:last, type:Edition, file:alphagov_whitehall/app/models/edition.rb, step:When ]
[name:last, type:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Then ]
[name:login_as, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:Given ]
[name:name, type:Person, file:alphagov_whitehall/app/models/person.rb, step:Then ]
[name:name, type:RolePresenter, file:alphagov_whitehall/app/presenters/role_presenter.rb, step:Then ]
[name:name, type:DataMigration, file:alphagov_whitehall/lib/whitehall/data_migration.rb, step:Then ]
[name:organisation, type:EditionsController, file:alphagov_whitehall/app/controllers/admin/editions_controller.rb, step:Then ]
[name:organisation, type:FatalityNoticeRow, file:alphagov_whitehall/lib/whitehall/uploader/fatality_notice_row.rb, step:Then ]
[name:organisation, type:Row, file:alphagov_whitehall/lib/whitehall/uploader/row.rb, step:Then ]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:Then ]
[name:person, type:Appointment, file:alphagov_whitehall/app/models/edition/appointment.rb, step:Then ]
[name:person, type:RoleAppointmentPresenter, file:alphagov_whitehall/app/presenters/role_appointment_presenter.rb, step:Then ]
[name:policies_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:Then ]
[name:public_document_path, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:Then ]
[name:public_document_url, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:Then ]
[name:publications_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:Then ]
[name:refute, type:Object, file:null, step:Then ]
[name:select, type:Object, file:null, step:When ]
[name:show, type:PoliciesController, file:alphagov_whitehall/app/controllers/admin/policies_controller.rb, step:Then ]
[name:show, type:PoliciesController, file:alphagov_whitehall/app/controllers/policies_controller.rb, step:Then ]
[name:show, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:Then ]
[name:show, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:Then ]
[name:show, type:TopicsController, file:alphagov_whitehall/app/controllers/admin/topics_controller.rb, step:Then ]
[name:show, type:TopicsController, file:alphagov_whitehall/app/controllers/topics_controller.rb, step:Then ]
[name:show, type:CorporateInformationPagesController, file:alphagov_whitehall/app/controllers/admin/corporate_information_pages_controller.rb, step:Then ]
[name:show, type:CorporateInformationPagesController, file:alphagov_whitehall/app/controllers/corporate_information_pages_controller.rb, step:Then ]
[name:slug, type:NewsArticleType, file:alphagov_whitehall/app/models/news_article_type.rb, step:Then ]
[name:slug, type:PublicationType, file:alphagov_whitehall/app/models/publication_type.rb, step:Then ]
[name:slug, type:SpeechType, file:alphagov_whitehall/app/models/speech_type.rb, step:Then ]
[name:slug, type:WorldLocationType, file:alphagov_whitehall/app/models/world_location_type.rb, step:Then ]
[name:slug, type:WorldwideServiceType, file:alphagov_whitehall/app/models/worldwide_service_type.rb, step:Then ]
[name:slug, type:AnnouncementFilterOption, file:alphagov_whitehall/lib/whitehall/announcement_filter_option.rb, step:Then ]
[name:slug, type:PublicationFilterOption, file:alphagov_whitehall/lib/whitehall/publication_filter_option.rb, step:Then ]
[name:slug, type:WhipOrganisation, file:alphagov_whitehall/lib/whitehall/whip_organisation.rb, step:Then ]
[name:text, type:Object, file:null, step:Then ]
[name:title, type:CorporateInformationPage, file:alphagov_whitehall/app/models/corporate_information_page.rb, step:Then ]
[name:title, type:Row, file:alphagov_whitehall/lib/whitehall/uploader/row.rb, step:Then ]
[name:title, type:StatisticalDataSetRow, file:alphagov_whitehall/lib/whitehall/uploader/statistical_data_set_row.rb, step:Then ]
[name:title, type:Document, file:alphagov_whitehall/app/models/document.rb, step:Then ]
[name:title, type:DetailedGuide, file:alphagov_whitehall/app/models/detailed_guide.rb, step:Then ]
[name:unpublish_edition, type:UnpublishingPublishedDocumentsSteps, file:alphagov_whitehall/features/step_definitions/unpublishing_published_documents_steps.rb, step:When ]
[name:unpublishing, type:Object, file:null, step:Then ]

Referenced pages: 31
alphagov_whitehall/app/views/announcements/_list_description.html.erb
alphagov_whitehall/app/views/classifications/_list_description.html.erb
alphagov_whitehall/app/views/contacts/_contact.html.erb
alphagov_whitehall/app/views/corporate_information_pages/show.html.erb
alphagov_whitehall/app/views/corporate_information_pages/show_worldwide_organisation.html.erb
alphagov_whitehall/app/views/documents/_change_notes.html.erb
alphagov_whitehall/app/views/documents/_document_extra_metadata.html.erb
alphagov_whitehall/app/views/documents/_header.html.erb
alphagov_whitehall/app/views/organisations/_corporate_information.html.erb
alphagov_whitehall/app/views/organisations/_header.html.erb
alphagov_whitehall/app/views/organisations/_organisations_logo_list.html.erb
alphagov_whitehall/app/views/organisations/_works_with.html.erb
alphagov_whitehall/app/views/organisations/about.html.erb
alphagov_whitehall/app/views/organisations/index.html.erb
alphagov_whitehall/app/views/organisations/show.html.erb
alphagov_whitehall/app/views/people/_person.html.erb
alphagov_whitehall/app/views/policies/_case_studies.html.erb
alphagov_whitehall/app/views/policies/_document_content.html.erb
alphagov_whitehall/app/views/policies/_document_contextual.html.erb
alphagov_whitehall/app/views/policies/_list_description.html.erb
alphagov_whitehall/app/views/policies/_policy_navigation.html.erb
alphagov_whitehall/app/views/policies/show.html.erb
alphagov_whitehall/app/views/publications/_list_description.html.erb
alphagov_whitehall/app/views/shared/_available_languages.html.erb
alphagov_whitehall/app/views/shared/_featured_news.html.erb
alphagov_whitehall/app/views/shared/_feeds.html.erb
alphagov_whitehall/app/views/shared/_recently_updated.html.erb
alphagov_whitehall/app/views/shared/_recently_updated_documents.html.erb
alphagov_whitehall/app/views/shared/_social_media_accounts.html.erb
alphagov_whitehall/app/views/topics/show.html.erb
alphagov_whitehall/app/views/worldwide_organisations/_header.html.erb

