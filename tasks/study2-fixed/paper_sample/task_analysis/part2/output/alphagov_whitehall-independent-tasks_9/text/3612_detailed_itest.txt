Classes: 7
[name:CorporateInformationPageType, file:alphagov_whitehall/app/models/corporate_information_page_type.rb, step:When ]
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Given ]
[name:GDS, file:null, step:Given ]
[name:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]
[name:WorldwideOrganisation, file:alphagov_whitehall/app/models/worldwide_organisation.rb, step:Given ]
[name:WorldwideOrganisation, file:alphagov_whitehall/app/models/worldwide_organisation.rb, step:When ]
[name:WorldwideOrganisation, file:alphagov_whitehall/app/models/worldwide_organisation.rb, step:Then ]

Methods: 34
[name:by_type, type:Edition, file:alphagov_whitehall/app/models/edition.rb, step:When ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_link, type:Object, file:null, step:Given ]
[name:click_link, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:Then ]
[name:click_on, type:Object, file:null, step:When ]
[name:corporate_information_pages, type:Object, file:null, step:When ]
[name:create, type:Object, file:null, step:Given ]
[name:current_person, type:Role, file:alphagov_whitehall/app/models/role.rb, step:null]
[name:current_person, type:RolePresenter, file:alphagov_whitehall/app/presenters/role_presenter.rb, step:null]
[name:edit, type:Admin/worldwideOrganisationsController, file:alphagov_whitehall/app/controllers/admin/worldwide_organisations_controller.rb, step:null]
[name:fill_in, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find_by_name, type:WorldwideOrganisation, file:alphagov_whitehall/app/models/worldwide_organisation.rb, step:When ]
[name:find_by_name, type:WorldwideOrganisation, file:alphagov_whitehall/app/models/worldwide_organisation.rb, step:Then ]
[name:find_by_title, type:CorporateInformationPageType, file:alphagov_whitehall/app/models/corporate_information_page_type.rb, step:When ]
[name:first, type:Object, file:null, step:When ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:last, type:WorldwideOrganisation, file:alphagov_whitehall/app/models/worldwide_organisation.rb, step:Given ]
[name:login_as, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:Given ]
[name:organisation, type:EditionsController, file:alphagov_whitehall/app/controllers/admin/editions_controller.rb, step:null]
[name:organisation, type:ConsultationRow, file:alphagov_whitehall/lib/whitehall/uploader/consultation_row.rb, step:null]
[name:organisation, type:NewsArticleRow, file:alphagov_whitehall/lib/whitehall/uploader/news_article_row.rb, step:null]
[name:page, type:DetailedGuidePresenter, file:alphagov_whitehall/app/presenters/api/detailed_guide_presenter.rb, step:Then ]
[name:person, type:Appointment, file:alphagov_whitehall/app/models/edition/appointment.rb, step:null]
[name:person, type:RoleAppointmentPresenter, file:alphagov_whitehall/app/presenters/role_appointment_presenter.rb, step:null]
[name:record_css_selector, type:Object, file:null, step:When ]
[name:select, type:Object, file:null, step:Given ]
[name:select, type:Object, file:null, step:When ]
[name:show, type:Admin/worldwideOrganisationsController, file:alphagov_whitehall/app/controllers/admin/worldwide_organisations_controller.rb, step:null]
[name:show, type:WorldwideOrganisationsController, file:alphagov_whitehall/app/controllers/admin/worldwide_organisations_controller.rb, step:null]
[name:show, type:WorldwideOrganisationsController, file:alphagov_whitehall/app/controllers/worldwide_organisations_controller.rb, step:null]
[name:url_for, type:Object, file:null, step:null]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 11
alphagov_whitehall/app/views/admin/worldwide_organisations/_form.html.erb
alphagov_whitehall/app/views/admin/worldwide_organisations/_sidebar.html.erb
alphagov_whitehall/app/views/admin/worldwide_organisations/edit.html.erb
alphagov_whitehall/app/views/admin/worldwide_organisations/show.html.erb
alphagov_whitehall/app/views/contacts/_contact.html.erb
alphagov_whitehall/app/views/people/_person.html.erb
alphagov_whitehall/app/views/shared/_available_languages.html.erb
alphagov_whitehall/app/views/shared/_social_media_accounts.html.erb
alphagov_whitehall/app/views/worldwide_organisations/_corporate_information.html.erb
alphagov_whitehall/app/views/worldwide_organisations/_header.html.erb
alphagov_whitehall/app/views/worldwide_organisations/show.html.erb

