Given /^the worldwide office "([^"]*)" exists$/ do |office_name|
  create(:worldwide_office, name: office_name, logo_formatted_name: office_name)
end
Given /^a person called "([^"]*)" is assigned as its ambassador "([^"]*)"$/ do |person_name, role_name|
  person = create_person(person_name)
  role = create(:ambassador_role, name: role_name, worldwide_offices: [WorldwideOffice.last])
  role_appointment = create(:ambassador_role_appointment, role: role, person: person)
end
  def create_person(name)
    create(:person, split_person_name(name))
  end
  def split_person_name(name)
    if match = /^(\w+)\s*(.*?)$/.match(name)
      forename, surname = match.captures
      { title: nil, forename: forename, surname: surname, letters: nil }
    else
      raise "couldn't split \"#{name}\""
    end
  end
Then /^I should see (#{THE_DOCUMENT})$/ do |edition|
  assert has_css?(record_css_selector(edition))
end
Then /^I should see the worldwide office listed on his public page$/ do
  person = Person.last
  office = WorldwideOffice.last
  visit person_url(person)

  within record_css_selector(person) do
    assert page.has_content?(person.name)
    assert page.has_css?("#current-roles a", text: office.name)
  end
end
