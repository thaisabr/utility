When /^I import the following data as CSV as "([^"]*)" for "([^"]*)":$/ do |document_type, organisation_name, data|
  organisation = Organisation.find_by_name(organisation_name) || create(:organisation, name: organisation_name)
  import_data_as_document_type_for_organisation(data, document_type, organisation)
end
Then /^the import succeeds, creating (\d+) imported news articles? for "([^"]*)" with "([^"]*)" news article type$/ do |edition_count, organisation_name, news_article_type_slug|
  organisation = Organisation.find_by_name(organisation_name)
  news_article_type  = NewsArticleType.find_by_slug(news_article_type_slug)
  assert_equal edition_count.to_i, Edition.imported.count

  edition = Edition.imported.first
  assert_kind_of NewsArticle, edition
  assert_equal organisation, edition.organisations.first
  assert_equal news_article_type, edition.news_article_type
end
Then /^the imported news article has (?:a|an) "([^"]*)" locale translation$/ do |locale|
  news_article = Import.last.documents.where(document_type: NewsArticle.name).first.latest_edition
  assert news_article.available_in_locale?(locale), "News article should have been translated into #{locale}"
end
  def import_data_as_document_type_for_organisation(data, document_type, organisation)
    make_sure_data_importer_user_exists
    Import.use_separate_connection

    with_import_csv_file(data) do |path|
      visit new_admin_import_path
      select document_type, from: 'Type'
      attach_file 'CSV File', path
      select organisation.name, from: 'Default organisation'
      click_button 'Save'
      click_button 'Run'

      run_last_import

      visit current_path
    end
    Import.find(current_path.match(/admin\/imports\/(\d+)\Z/)[1])
  end
  def run_last_import
    Import.last.perform
  end
  def with_import_csv_file(data)
    tf = Tempfile.new('csv_import')
    tf << data
    tf.close
    yield tf.path
    tf.unlink
  end
  def make_sure_data_importer_user_exists
    unless User.find_by_name('Automatic Data Importer')
      create(:importer, name: 'Automatic Data Importer')
    end
  end
Given /^I am (?:a|an) (writer|editor|admin|GDS editor|importer)(?: called "([^"]*)")?$/ do |role, name|
  @user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Depteditor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  when 'importer'
    create(:importer)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    Edition::AuditTrail.whodunnit = user
    super(user) # warden
  end
