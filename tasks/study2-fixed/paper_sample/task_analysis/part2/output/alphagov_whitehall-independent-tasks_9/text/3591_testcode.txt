Given /^"([^"]*)" is the "([^"]*)" for the "([^"]*)"$/ do |person_name, ministerial_role, organisation_name|
  create_role_appointment(person_name, ministerial_role, organisation_name, 2.years.ago)
end
When /^I visit the ministers page$/ do
  visit ministers_page
end
Then /^I should see that "([^"]*)" is a minister in the "([^"]*)" with role "([^"]*)"$/ do |minister_name, organisation_name, role|
  organisation = Organisation.find_by_name!(organisation_name)
  within record_css_selector(organisation) do
    assert page.has_css?('.current-appointee', text: minister_name)
    assert page.has_css?('.role', text: role)
  end
end
  def create_role_appointment(person_name, role_name, organisation_name, timespan, options = {})
    person = find_or_create_person(person_name)
    organisation = Organisation.find_by_name(organisation_name) || create(:ministerial_department, name: organisation_name)
    role = MinisterialRole.create!({name: role_name}.merge(options[:role_options] || {}))
    organisation.ministerial_roles << role

    if timespan.is_a?(Hash)
      started_at = timespan.keys.first
      ended_at = timespan.values.first
    else
      started_at = timespan
      ended_at = nil
    end

    create(:role_appointment, role: role, person: person, started_at: started_at, ended_at: ended_at)
  end
  def ministers_page
    ministerial_roles_path
  end
  def find_or_create_person(name)
    find_person(name) || create_person(name)
  end
  def create_person(name, attributes = {})
    create(:person, split_person_name(name).merge(attributes))
  end
  def find_person(name)
    Person.where(split_person_name(name)).first
  end
  def split_person_name(name)
    if match = /^(\w+)\s*(.*?)$/.match(name)
      forename, surname = match.captures
      { title: nil, forename: forename, surname: surname, letters: nil }
    else
      raise "couldn't split \"#{name}\""
    end
  end
Then /^I should see (#{THE_DOCUMENT})$/ do |edition|
  assert has_css?(record_css_selector(edition))
end
