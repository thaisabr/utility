Feature: Creating and publishing topical events
As an editor
I want to be able to create and publish topical events
So that I can communicate about them
Background:
Given I am an editor
Scenario: Archiving a new topical event
When I create a new topical event "An Event" with description "A topical event" and it ends today
Then I should see the topical event "An Event" in the admin interface
And I should see the topical event "An Event" on the frontend is archived
