Given(/^I am on the Your Details step$/) do
  your_details_page.load(language_code: language_code)
end
When(/^I choose to make minimum contributions$/) do
  your_details_page.minimum_contribution_button.set(true)
end
When(/^I enter my details$/) do
  step %{I enter my age as "35"}
  step %{I select my gender as "Female"}
end
When(/^I enter a "([^"]*)" below the minimum threshold$/) do |salary|
  step %{I enter my salary as "#{salary}"}
end
When(/^I select a valid "([^"]*)"$/) do |salary_frequency|
  step %{I select my salary frequency as "#{salary_frequency}"}
end
When(/^I submit my details$/) do
  your_details_page.next_button.click
end
Then(/^I should see that the full contribution option should( not| NOT)? be selected$/) do |should_not|
  if should_not
    expect(your_details_page.full_contribution_button).not_to be_checked
  else
    expect(your_details_page.full_contribution_button).to be_checked
  end
end
Then(/^I should see the salary below threshold "([^"]*)"$/) do |callout_message|
  expect(your_details_page.salary_below_threshold_callout).to have_content(callout_message)
end
    def language_code
      @language
    end
Given(/^I enter my age as "([^"]*)"$/) do |age|
  your_details_page.age.set(age)
end
Given(/^I select my gender as "([^"]*)"$/) do |gender|
  your_details_page.genders.select(gender.capitalize)
end
Given(/^I enter my salary as "([^"]*)"$/) do |salary|
  your_details_page.salary.set(salary)
end
Given(/^I select my salary frequency as "([^"]*)"$/) do |salary_frequency|
  your_details_page.salary_frequencies.select(salary_frequency)
end
