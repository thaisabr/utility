Classes: 4
[name:Capybara, file:null, step:Given ]
[name:I18n, file:null, step:Given ]
[name:Language, file:moneyadviceservice_wpcc/features/support/world/language.rb, step:Given ]
[name:Timeout, file:null, step:Given ]

Methods: 38
[name:age, type:Object, file:null, step:Given ]
[name:capitalize, type:Object, file:null, step:Given ]
[name:click, type:Object, file:null, step:And ]
[name:downcase, type:Object, file:null, step:And ]
[name:eq, type:Object, file:null, step:Then ]
[name:finished_all_js_requests?, type:WaitForPageLoad, file:moneyadviceservice_wpcc/features/support/world/wait_for_page_load.rb, step:Given ]
[name:flatten, type:Object, file:null, step:Then ]
[name:genders, type:Object, file:null, step:Given ]
[name:have_content, type:Object, file:null, step:Then ]
[name:html, type:Object, file:null, step:Given ]
[name:include?, type:Object, file:null, step:Given ]
[name:language_code, type:Language, file:moneyadviceservice_wpcc/features/support/world/language.rb, step:Given ]
[name:load, type:ConfigLoader, file:moneyadviceservice_wpcc/app/models/wpcc/config_loader.rb, step:Given ]
[name:loop, type:Object, file:null, step:Given ]
[name:map, type:Object, file:null, step:Then ]
[name:minimum_contribution_button, type:Object, file:null, step:Given ]
[name:next_button, type:Object, file:null, step:And ]
[name:page, type:Object, file:null, step:Given ]
[name:percent_table_headings, type:Object, file:null, step:Then ]
[name:raw, type:Object, file:null, step:Then ]
[name:salary, type:Object, file:null, step:And ]
[name:salary, type:Object, file:null, step:Given ]
[name:salary_frequencies, type:Object, file:null, step:And ]
[name:select, type:Object, file:null, step:Given ]
[name:select, type:Object, file:null, step:And ]
[name:send, type:Object, file:null, step:And ]
[name:set, type:Object, file:null, step:Given ]
[name:set, type:Object, file:null, step:And ]
[name:table_cells, type:Object, file:null, step:Then ]
[name:text, type:Object, file:null, step:Then ]
[name:to, type:Object, file:null, step:Then ]
[name:to_not, type:Object, file:null, step:Then ]
[name:wait_for_page_load, type:WaitForPageLoad, file:moneyadviceservice_wpcc/features/support/world/wait_for_page_load.rb, step:Given ]
[name:wait_for_page_load, type:WaitForPageLoad, file:moneyadviceservice_wpcc/features/support/world/wait_for_page_load.rb, step:And ]
[name:your_contributions_page, type:Object, file:null, step:Then ]
[name:your_details_page, type:Object, file:null, step:Given ]
[name:your_details_page, type:Object, file:null, step:And ]
[name:your_results_page, type:Object, file:null, step:Then ]

Referenced pages: 0

