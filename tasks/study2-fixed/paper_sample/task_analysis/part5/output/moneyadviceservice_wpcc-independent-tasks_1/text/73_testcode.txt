Given(/^I enter my age as "([^"]*)"$/) do |age|
  your_details_page.age.set(age)
end
Given(/^I select my gender as "([^"]*)"$/) do |gender|
  your_details_page.genders.select(gender.capitalize)
end
Given(/^I enter my salary as "([^"]*)"$/) do |salary|
  your_details_page.salary.set(salary)
end
Given(/^I select my salary frequency as "([^"]*)"$/) do |salary_frequency|
  your_details_page.salary_frequencies.select(salary_frequency)
end
Given(/^I choose my contribution preference as "([^"]*)"$/) do |contribution_preference|
  your_details_page.send("#{contribution_preference.downcase}_contribution_button").set(true)
end
And(/^I click the Next button$/) do
  your_details_page.next_button.click
end
Given(/^my employee contribution is "([^"]*)"$/) do |employee_percent|
  your_contributions_page.employee_percent.set(employee_percent)
end
Given(/^my employer contribution is "([^"]*)"$/) do |employer_percent|
  your_contributions_page.employer_percent.set(employer_percent)
end
When(/^I move on to the results page$/) do
  click_button
end
Then(/^I should see my employee contributions for current period as "([^"]*)"$/) do |employee_contribution|
  expect(your_results_page.current_period.employee_contribution.text).to eq(employee_contribution)
end
Then(/^I should see my tax relief for current period as "([^"]*)"$/) do |tax_relief|
  expect(your_results_page.current_period.tax_relief.text).to eq("(includes tax relief of #{tax_relief})")
end
Then(/^I should see my employer contributions for current period as "([^"]*)"$/) do |employer_contribution|
  expect(your_results_page.current_period.employer_contribution.text).to eq(employer_contribution)
end
Then(/^I should see my total contributions for current period as "([^"]*)"$/) do |total_contributions|
  expect(your_results_page.current_period.total_contributions.text).to eq(total_contributions)
end
Then(/^I should see my employee contributions for second period as "([^"]*)"$/) do |employee_contribution|
  next if your_results_page.has_no_second_period?

  expect(your_results_page.second_period.employee_contribution.text).to eq(employee_contribution)
end
Then(/^I should see my employer contributions for second period as "([^"]*)"$/) do |employer_contribution|
  next if your_results_page.has_no_second_period?

  expect(your_results_page.second_period.employer_contribution.text).to eq(employer_contribution)
end
Then(/^I should see my tax relief for second period as "([^"]*)"$/) do |tax_relief|
  next if your_results_page.has_no_second_period?

  expect(your_results_page.second_period.tax_relief.text).to eq("(includes tax relief of #{tax_relief})")
end
Then(/^I should see my total contributions for second period as "([^"]*)"$/) do |total_contributions|
  next if your_results_page.has_no_second_period?

  expect(your_results_page.second_period.total_contributions.text).to eq(total_contributions)
end
Then(/^I should see my employee contributions for third period as "([^"]*)"$/) do |employee_contribution|
  expect(your_results_page.third_period.employee_contribution.text).to eq(employee_contribution)
end
Then(/^I should see my employer contributions for third period as "([^"]*)"$/) do |employer_contribution|
  expect(your_results_page.third_period.employer_contribution.text).to eq(employer_contribution)
end
Then(/^I should see my tax relief for third period as "([^"]*)"$/) do |tax_relief|
  expect(your_results_page.third_period.tax_relief.text).to eq("(includes tax relief of #{tax_relief})")
end
Then(/^I should see my total contributions for third period as "([^"]*)"$/) do |total_contributions|
  expect(your_results_page.third_period.total_contributions.text).to eq(total_contributions)
end
Given(/^I am on the YourDetailsPage$/) do
  step 'I am on step 1 of the WPCC homepage'
end
Given(/^I am on step 1 of the WPCC homepage$/) do
  your_details_page.load
end
