When(/^(?:|I )go to (.+)$/) do |page_name|
  visit path_to(page_name)
end
When(/^(?:|I )press "([^"]*)"$/) do |button|
  click_button(button)
end
When(/^(?:|I )follow "([^"]*)"$/) do |link|
  click_link(link)
end
Then(/^(?:|I )should be on (.+)$/) do |page_name|
  current_path = URI.parse(current_url).path
  if current_path.respond_to? :should
    current_path.should == path_to(page_name)
  else
    assert_equal path_to(page_name), current_path
  end
end
  def path_to(page_name)
    case page_name

    when /^the home\s?page$/
      '/'
    when /^the setup page$/
      '/setup'
    when /^the new association page$/
      '/associations/new'
    when /^the new co-op page$/
      '/coops/new'
    when /^the welcome page$/
      '/welcome'
    when /^the voting and proposals page$/
      '/'
    when /^my member page$/
      member_path(@user)
    when /^my account page$/
      edit_member_path(@user)
    when /^the proposal page$/
      @proposal ||= Proposal.last
      proposal_path(@proposal)
    when /^the member page for "(.*)"$/
      @member = @organisation.members.find_by_email($1)
      member_path(@member)
    when /^a member's page$/
      @member = @organisation.members.active.last
      member_path(@member)
    when /^the member's profile page$/
      @member ||= @organisation.members.active.last
      member_path(@member)
    when /^the amendments page$/
      edit_constitution_path
    when /^the members page$/
      members_path
    when /^the new company page$/
      new_company_path
    when /^the Votes & Minutes page$/
      '/'
    when /^the page for the minutes$/
      @meeting ||= @organisation.meeting.last
      meeting_path(@meeting)
    when /^the Directors page$/
      case @organisation
      when Company
        members_path
      when Coop
        directors_path
      end
    when /^the (D|d)ashboard( page)?$/
      '/'
    when /^the Resolutions page$/
      proposals_path
    when /^the "Convene a General Meeting" page$/
      new_general_meeting_path
    when /^the Meetings page$/
      meetings_path
    when /^the Members page$/
      members_path
    when /^another member's profile page$/
      @member = (@organisation.members - [@user]).first
      member_path(@member)
    when /^convene a General Meeting$/
      new_general_meeting_path
    when /^the Rules page$/
      constitution_path
    when /^convene an AGM$/
      new_general_meeting_path
    when /^the (D|d)ashboard(| page) for the (new|draft) co-op$/
      root_path
    when /^the Amendments page$/
      edit_constitution_path
    when /^the co-op review page$/
      admin_coops_path
    when /^the Shares page$/
      '/shares'
    when /^the Checklist page$/
      '/checklist'
    when /^the Proposals page$/
      '/proposals'
    when /^the Board page$/
      '/board'

    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /^the (.*) page$/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue NoMethodError, ArgumentError
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Then(/^I should see the custom fields in the Rules filled in appropriately$/) do
  constitution = @organisation.constitution

  # TODO Check the custom fields which result in entire sections of the
  # Rules being displayed or not; not just the custom fields which are
  # displayed literally in the Rules.

  # TODO Do a more precise check of the appearance of fields like
  # 'max_user_directors' which are just a number. Such a number probably
  # will occur somewhere else in the Rules, and hence trigger a false positive
  # in this test.

  [
    :name,
    :registered_office_address,
    :objectives,
    :meeting_notice_period,
    :quorum_number,
    :quorum_percentage,
    :max_user_directors,
    :max_employee_directors,
    :max_supporter_directors,
    :max_producer_directors,
    :max_consumer_directors
  ].each do |field|
    page.should have_content(constitution.send(field))
  end
end
Given(/^there is a co\-op$/) do
  set_up_application_if_necessary

  @coop = @organisation = Coop.make!

  secretary = @coop.members.make!(:secretary)
  secretary_office = @coop.offices.make!(:title => "Secretary")
  secretary_office.officership = Officership.make!(:officer => secretary)

  @coop.members.make!(:director)

  set_subdomain_to_organisation
end
  def set_up_application_if_necessary
    unless Setting[:base_domain] && Setting[:signup_domain]
      set_up_application
    end
  end
def set_subdomain_to_organisation
  set_subdomain(@organisation.subdomain)
end
  def set_up_application
    # *.ocolocalhost.com resolves to 127.0.0.1. This lets us test subdomain
    # look-up using domains like 'company.ocolocalhost.com' and
    # 'association.ocolocalhost.com', without having to set up local wildcard
    # entries on each developer's machine and on the CI server.
    #
    # N.B. This means some Cucumber scenarios will fail if your machine
    # isn't connected to the internet. We shoud probably fix this.
    #
    # Port needs to be saved for Selenium tests (because our app code considers
    # the port as well as the hostname when figuring out how to handle a
    # request, and the Capybara app server doesn't run on port 80).
    #
    # When using the rack-test driver, don't use port numbers at all,
    # since it makes our test-session-resetting code (see features/support/capybara_domains.rb)
    # more complicated.
    port_segment = Capybara.current_driver == :selenium ? ":#{Capybara.server_port}" : ''

    Setting[:base_domain] = "ocolocalhost.com#{port_segment}"
    Setting[:signup_domain] = "create.ocolocalhost.com#{port_segment}"
  end
def set_subdomain(subdomain)
  domain = "#{subdomain}.#{Setting[:base_domain].sub(/:\d+$/, '')}"
  set_domain(domain)
end
def set_domain(domain)
  if Capybara.current_driver == :selenium
    Capybara.default_host = "http://#{domain}:#{Capybara.server_port}"
    Capybara.app_host = "http://#{domain}:#{Capybara.server_port}"
  else
    Capybara.default_host = "http://#{domain}:#{Capybara.server_port}"
    Capybara.app_host = "http://#{domain}"
  end
end
Given(/^I am the (?:S|s)ecretary of the co\-op$/) do
  @organisation ||= Coop.last

  if @organisation.secretary
    @user = @organisation.secretary

    # We need to be able to read the user's password in order
    # to log in as them. A Member record fetched from the
    # database does not have the raw password stored, so we
    # set a new one here.
    @user.password = @user.password_confirmation = "password"
    @user.save!
  else
    @user = @organisation.members.make!(:secretary)
  end
  user_logs_in
end
Given(/^I am a (?:M|m)ember of the co\-op$/) do
  @organisation ||= Coop.last
  @user = @organisation.members.make!(:member)
  user_logs_in
end
def user_logs_in
  visit '/'
  fill_in('Email', :with => @user.email)
  fill_in('Password', :with => @user.password)
  click_button("Login")
end
When(/^I open the "(.*?)" tab$/) do |tab_name|
  within('.ui-tabs-nav') do
    click_link(tab_name)
  end
end
Then(/^I should get a "([^"]*)" download with the name of the organisation$/) do |extension|
  @organisation ||= Organisation.last
  page.response_headers['Content-Disposition'].should =~ Regexp.new("filename=\"#{Regexp.escape(@organisation.name)}.*#{Regexp.escape(extension)}\"")
end
Given(/^there has been a past board meeting$/) do
  @meeting = @board_meeting ||= @organisation.board_meetings.make!(:past)
end
Given(/^no minutes for the past board meeting have been entered yet$/) do
  @board_meeting ||= @organisation.board_meetings.past.last
  @board_meeting.update_attribute(:minutes, nil)
end
When(/^I enter minutes for the board meeting$/) do
  enter_minutes
end
When(/^I follow "(.*?)" for the past board meeting$/) do |link|
  @board_meeting ||= @organisation.board_meetings.past.last

  within("#board_meeting_#{@board_meeting.id}") do
    click_link(link)
  end
end
When(/^I choose the Directors who were in attendance$/) do
  directors = @organisation.directors
  directors.each do |director|
    check(director.name)
  end
end
When(/^I follow "(.*?)" for the board meeting$/) do |link|
  @board_meeting ||= @organisation.board_meetings.last

  within("#board_meeting_#{@board_meeting.id}") do
    click_link(link)
  end
end
Then(/^I should see "(.*?)" for the past board meeting$/) do |text|
  @board_meeting ||= @organisation.board_meetings.past.last

  within("#board_meeting_#{@board_meeting.id}") do
    page.should have_content(text)
  end
end
def enter_minutes
  if page.has_field?('Apologies for Absence')
    fill_in("Apologies for Absence", :with => "Bob Smith")
    fill_in("Minutes of Previous Meeting", :with => "The minutes of the previous meeting were accepted.")
    fill_in("Any Other Business", :with => "Jenny Jenkins thanked Geoff Newell for providing the refreshments.")
    fill_in("Time and date of next meeting", :with => "31 October at 6pm")
    @minute_type = :agenda_items
  elsif page.has_field?('minute[minutes]')
    fill_in('minute[minutes]', :with => "We discussed things.")
    @minute_type = :minutes
  elsif page.has_field?('board_meeting[minutes]')
    fill_in('board_meeting[minutes]', :with => "We discussed things.")
    @minute_type = :minutes
  else
    raise "Could not find minutes field."
  end
end
Given(/^there has been a past meeting$/) do
  @general_meeting = @organisation.general_meetings.make!(:past)
end
When(/^I choose the Members who were in attendance$/) do
  members = @organisation.members.limit(2)
  within('div.participants') do
    members.each do |member|
      fill_in("Add member", :with => member.name)
      sleep(1)
      page.execute_script("$('.ui-menu-item a:contains(\"#{escape_javascript(member.name)}\")').trigger('mouseenter').click();")
    end
  end
end
When(/^I follow "(.*?)" for the meeting$/) do |link|
  @general_meeting ||= @organisation.general_meetings.last

  within("#general_meeting_#{@general_meeting.id}") do
    click_link(link)
  end
end
Then(/^I should see the minutes I entered$/) do
  if @minute_type && @minute_type == :agenda_items
    page.should have_content("Apologies for Absence")
    page.should have_content("Bob Smith")
    page.should have_content("Minutes of Previous Meeting")
    page.should have_content("The minutes of the previous meeting were accepted.")
    page.should have_content("Any Other Business")
    page.should have_content("Jenny Jenkins thanked Geoff Newell for providing the refreshments.")
    page.should have_content("Time and date of next meeting")
    page.should have_content("31 October at 6pm")
  else
    page.should have_content("We discussed things.")
  end
end
Then(/^I should see the participants I chose$/) do
  members = @organisation.members.limit(2)

  within('.participants') do
    members.each do |member|
      page.should have_content(member.name)
    end
  end
end
Given(/^minutes have been entered for the meeting$/) do
  @meeting ||= @organisation.meetings.last
  @meeting.agenda_items.each do |agenda_item|
    agenda_item.minutes = Faker::Lorem.paragraph
    agenda_item.save!
  end
  @meeting.minutes = Faker::Lorem.paragraph
  @meeting.save!
end
When(/^I enter the date of the meeting$/) do
  select('2011', :from => 'minute[happened_on(1i)]')
  select('May', :from => 'minute[happened_on(2i)]')
  select('1', :from => 'minute[happened_on(3i)]')
end
When(/^I choose "(.*?)" from the list of meeting types$/) do |meeting_type|
  select(meeting_type, :from => 'minute[meeting_class]')
end
When(/^I enter minutes for the meeting$/) do
  enter_minutes
end
When(/^I enter that all the resolutions were passed$/) do
  @meeting ||= @organisation.meetings.last
  @resolutions ||= @meeting.resolutions

  @resolutions.each do |resolution|
    within('#' + ActionController::RecordIdentifier.dom_id(resolution)) do
      check("Resolution was passed")
    end
  end
end
When(/^I enter other minutes for the meeting$/) do
  enter_minutes
end
When(/^I edit the minutes$/) do
  if page.has_field?('board_meeting[minutes]')
    fill_in('board_meeting[minutes]', :with => "Edited minutes")
  else
    fill_in('general_meeting[minutes]', :with => "Edited minutes")
  end
end
Then(/^I should see a list of the resolutions attached to the meeting$/) do
  @meeting ||= @organisation.meetings.last
  @resolutions ||= @meeting.resolutions

  @resolutions.each do |resolution|
    page.should have_content(resolution.title)
  end
end
Then(/^I should see the resolutions marked as passed$/) do
  @meeting ||= @organisation.meetings.last
  @resolutions ||= @meeting.resolutions

  @resolutions.each do |resolution|
    page.should have_content("#{resolution.title} was accepted")
  end
end
Then(/^I should see a field for each of the standard agenda items$/) do
  page.should have_field("Apologies for Absence")
  page.should have_field("Minutes of Previous Meeting")
  page.should have_field("Any Other Business")
  page.should have_field("Time and date of next meeting")
end
Then(/^I should see the edited minutes$/) do
  page.should have_content("Edited minutes")
end
Given(/^there has been a past AGM$/) do
  @annual_general_meeting = @organisation.annual_general_meetings.make!(:past)
end
Given(/^no minutes for the past AGM have been entered yet$/) do
  @annual_general_meeting ||= @organisation.annual_general_meetings.past.last

  @annual_general_meeting.update_attribute(:minutes, nil)
end
Given(/^the AGM has no minutes yet$/) do
  @annual_general_meeting ||= @organisation.annual_general_meetings.past.last

  @annual_general_meeting.update_attribute(:minutes, nil)
end
Given(/^there were resolutions attached to the AGM$/) do
  @resolutions = @organisation.resolutions.make!(2)

  @annual_general_meeting ||= @organisation.annual_general_meetings.last

  @annual_general_meeting.resolutions << @resolutions
end
When(/^I follow "(.*?)" for the past AGM$/) do |link|
  @annual_general_meeting ||= @organisation.annual_general_meetings.past.last

  within("#annual_general_meeting_#{@annual_general_meeting.id}") do
    click_link(link)
  end
end
When(/^I enter minutes for the AGM$/) do
  enter_minutes
end
When(/^I follow "(.*?)" for the AGM$/) do |link|
  @annual_general_meeting ||= @organisation.annual_general_meetings.last

  within("#annual_general_meeting_#{@annual_general_meeting.id}") do
    click_link(link)
  end
end
Then(/^I should see "(.*?)" for the past AGM$/) do |content|
  @annual_general_meeting ||= @organisation.annual_general_meetings.past.last

  within("#annual_general_meeting_#{@annual_general_meeting.id}") do
    page.should have_content(content)
  end
end
Then(/^I should see the AGM in the list of Past Meetings$/) do
  @annual_general_meeting ||= @organisation.annual_general_meetings.last
  within('.past_meetings') do
    page.should have_css('#' + ActionController::RecordIdentifier.dom_id(@annual_general_meeting))
  end
end
When(/^I choose a closing date for nominations$/) do
  date = 2.weeks.from_now
  page.execute_script(%Q{$('#annual_general_meeting_nominations_closing_date_datepicker').datepicker('setDate', new Date(#{date.year}, #{date.month}, #{date.day}));})
end
When(/^I choose a closing date for voting$/) do
  date = 4.weeks.from_now
  page.execute_script(%Q{$('#annual_general_meeting_voting_closing_date_datepicker').datepicker('setDate', new Date(#{date.year}, #{date.month}, #{date.day}));})
end
