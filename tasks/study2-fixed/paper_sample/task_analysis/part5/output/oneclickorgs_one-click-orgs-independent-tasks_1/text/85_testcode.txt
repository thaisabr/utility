Given(/^there is a co\-op$/) do
  set_up_application_if_necessary

  @coop = @organisation = Coop.make!

  secretary = @coop.members.make!(:secretary)
  secretary_office = @coop.offices.make!(:title => "Secretary")
  secretary_office.officership = Officership.make!(:officer => secretary)

  @coop.members.make!(:director)

  set_subdomain_to_organisation
end
  def set_up_application_if_necessary
    unless Setting[:base_domain] && Setting[:signup_domain]
      set_up_application
    end
  end
def set_subdomain_to_organisation
  set_subdomain(@organisation.subdomain)
end
  def set_up_application
    # *.ocolocalhost.com resolves to 127.0.0.1. This lets us test subdomain
    # look-up using domains like 'company.ocolocalhost.com' and
    # 'association.ocolocalhost.com', without having to set up local wildcard
    # entries on each developer's machine and on the CI server.
    #
    # N.B. This means some Cucumber scenarios will fail if your machine
    # isn't connected to the internet. We shoud probably fix this.
    #
    # Port needs to be saved for Selenium tests (because our app code considers
    # the port as well as the hostname when figuring out how to handle a
    # request, and the Capybara app server doesn't run on port 80).
    #
    # When using the rack-test driver, don't use port numbers at all,
    # since it makes our test-session-resetting code (see features/support/capybara_domains.rb)
    # more complicated.
    port_segment = Capybara.current_driver == :selenium ? ":#{Capybara.server_port}" : ''

    Setting[:base_domain] = "ocolocalhost.com#{port_segment}"
    Setting[:signup_domain] = "create.ocolocalhost.com#{port_segment}"
  end
def set_subdomain(subdomain)
  domain = "#{subdomain}.#{Setting[:base_domain].sub(/:\d+$/, '')}"
  set_domain(domain)
end
def set_domain(domain)
  if Capybara.current_driver == :selenium
    Capybara.default_host = "http://#{domain}:#{Capybara.server_port}"
    Capybara.app_host = "http://#{domain}:#{Capybara.server_port}"
  else
    Capybara.default_host = "http://#{domain}:#{Capybara.server_port}"
    Capybara.app_host = "http://#{domain}"
  end
end
Given(/^I am the (?:S|s)ecretary of the co\-op$/) do
  @organisation ||= Coop.last

  if @organisation.secretary
    @user = @organisation.secretary

    # We need to be able to read the user's password in order
    # to log in as them. A Member record fetched from the
    # database does not have the raw password stored, so we
    # set a new one here.
    @user.password = @user.password_confirmation = "password"
    @user.save!
  else
    @user = @organisation.members.make!(:secretary)
  end
  user_logs_in
end
Given(/^I am a (?:M|m)ember of the co\-op$/) do
  @organisation ||= Coop.last
  @user = @organisation.members.make!(:member)
  user_logs_in
end
def user_logs_in
  visit '/'
  fill_in('Email', :with => @user.email)
  fill_in('Password', :with => @user.password)
  click_button("Login")
end
Given(/^I have a current task$/) do
  # Create a share application task
  share_application = ShareApplication.new(:member => @user, :amount => 1)
  share_application.save!
  @task = @user.tasks.last
  @task.subject.should be_a_kind_of ShareTransaction
end
When(/^I dismiss the task$/) do
  within("#task_#{@task.id}") do
    click_link("Dismiss")
  end
end
Then(/^I should see a notification of the new membership application$/) do
  @member ||= @organisation.members.pending.last
  within('.tasks') do
    page.should have_css("a[href='/members/#{@member.to_param}']")
  end
end
Then(/^I should no longer see a notification of the new membership application$/) do
  @member ||= @organisation.members.last
  within('.tasks') do
    page.should have_no_css("a[href='/members/#{@member.to_param}']")
  end
end
Then /^I should not see a task telling me to vote in the resolution$/ do
  @resolution ||= @organisation.resolutions.last
  if page.has_css?('.tasks')
    within('.tasks') do
      page.should have_no_content(@resolution.title)
      page.should have_no_css("a[href='/resolutions/#{@resolution.to_param}']")
    end
  else
    page.should have_no_css('.tasks')
  end
end
Then(/^I should see the task$/) do
  @task ||= @user.tasks.last
  page.should have_css("#task_#{@task.id}")
end
Then(/^I should not see the task$/) do
  @task ||= @user.tasks.last
  within('.section.tasks') do
    page.should have_no_css("#task_#{@task.id}")
  end
end
When(/^(?:|I )go to (.+)$/) do |page_name|
  visit path_to(page_name)
end
When(/^(?:|I )press "([^"]*)"$/) do |button|
  click_button(button)
end
Then(/^(?:|I )should be on (.+)$/) do |page_name|
  current_path = URI.parse(current_url).path
  if current_path.respond_to? :should
    current_path.should == path_to(page_name)
  else
    assert_equal path_to(page_name), current_path
  end
end
  def path_to(page_name)
    case page_name

    when /^the home\s?page$/
      '/'
    when /^the setup page$/
      '/setup'
    when /^the new association page$/
      '/associations/new'
    when /^the new co-op page$/
      '/coops/new'
    when /^the welcome page$/
      '/welcome'
    when /^the voting and proposals page$/
      '/'
    when /^my member page$/
      member_path(@user)
    when /^my account page$/
      edit_member_path(@user)
    when /^the proposal page$/
      @proposal ||= Proposal.last
      proposal_path(@proposal)
    when /^the member page for "(.*)"$/
      @member = @organisation.members.find_by_email($1)
      member_path(@member)
    when /^a member's page$/
      @member = @organisation.members.active.last
      member_path(@member)
    when /^the member's profile page$/
      @member ||= @organisation.members.active.last
      member_path(@member)
    when /^the amendments page$/
      edit_constitution_path
    when /^the members page$/
      members_path
    when /^the new company page$/
      new_company_path
    when /^the Votes & Minutes page$/
      '/'
    when /^the page for the minutes$/
      @meeting ||= @organisation.meeting.last
      meeting_path(@meeting)
    when /^the Directors page$/
      case @organisation
      when Company
        members_path
      when Coop
        directors_path
      end
    when /^the (D|d)ashboard( page)?$/
      '/'
    when /^the Resolutions page$/
      proposals_path
    when /^the "Convene a General Meeting" page$/
      new_general_meeting_path
    when /^the Meetings page$/
      meetings_path
    when /^the Members page$/
      members_path
    when /^another member's profile page$/
      @member = (@organisation.members - [@user]).first
      member_path(@member)
    when /^convene a General Meeting$/
      new_general_meeting_path
    when /^the Rules page$/
      constitution_path
    when /^convene an AGM$/
      new_general_meeting_path
    when /^the (D|d)ashboard(| page) for the (new|draft) co-op$/
      root_path
    when /^the Amendments page$/
      edit_constitution_path
    when /^the co-op review page$/
      admin_coops_path
    when /^the Shares page$/
      '/shares'
    when /^the Checklist page$/
      '/checklist'
    when /^the Proposals page$/
      '/proposals'
    when /^the Board page$/
      '/board'

    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /^the (.*) page$/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue NoMethodError, ArgumentError
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Given(/^there is a suggested resolution$/) do
  @resolution_proposal = @organisation.resolution_proposals.make!
end
Given(/^there is a resolution open for electronic voting$/) do
  @resolution = @organisation.resolutions.make!
end
Given(/^I have voted to support the resolution$/) do
  @resolution ||= @organisation.resolutions.last
  @user.cast_vote(:for, @resolution)
end
When(/^I press "(.*?)" for the suggested resolution$/) do |button|
  @resolution_proposal ||= @organisation.resolution_proposals.last

  within('#' + ActionController::RecordIdentifier.dom_id(@resolution_proposal)) do
    click_button(button)
  end
end
Then(/^I should see the (?:|new )resolution in the list of currently\-open resolutions$/) do
  @resolution ||= @organisation.resolutions.last
  within('.proposals') do
    page.should have_content(@resolution.description)
  end
end
Then(/^I should not see the suggested resolution in the list of suggested resolutions$/) do
  @resolution_proposal ||= @organisation.resolution_proposals.last
  if page.has_css?('.resolution_proposals')
    within('.resolution_proposals') do
      page.should have_no_content(@resolution_proposal.title)
      page.should have_no_content(@resolution_proposal.description)
    end
  else
    page.should have_no_css('.resolution_proposals')
  end
end
Then(/^I should not see a notification to process the suggested resolution$/) do
  @resolution_proposal ||= @organisation.resolution_proposals.last
  page.should have_no_css("a[href='/resolution_proposals/#{@resolution_proposal.to_param}']")
end
Given(/^a membership application has been received$/) do
  @member = @organisation.members.make!(:pending)
end
When(/^I follow the link to process the new membership application$/) do
  click_link("Process application.")
end
Then(/^I should see the new member in the list of members$/) do
  @member ||= @organisation.members.last
  within('.members') do
    page.should have_content(@member.name)
  end
end
Then(/^the new member should receive an invitation email$/) do
  @member ||= @organisation.members.last
  @email = last_email_to(@member.email)

  @email.should be_present
  @email.subject.should include('application')
  @email.body.should include('/i/') # Invitation link
end
def last_email_to(email_address)
  ActionMailer::Base.deliveries.select{|e| e.to.include?(email_address)}.last
end
Given(/^members of the co\-op can hold more than one share$/) do
  @organisation.single_shareholding = false
  @organisation.save!
end
Given(/^a member has made an application for more shares$/) do
  @member = @organisation.members.make!

  @original_balance = @member.find_or_build_share_account.balance

  @share_application = ShareApplication.new(:amount => 3)
  @share_application.member = @member
  @share_application.save!
end
Then(/^I should see the share application$/) do
  page.should have_content("applied for 3 shares")
end
Then(/^I should see that the member's new shares have been allotted$/) do
  expected_share_balance = @original_balance + 3
  within("#member_#{@member.id}") do
    page.should have_content(@member.name)
    page.should have_content(expected_share_balance)
  end
end
Then(/^I should no longer see a notification about the member's share application$/) do
  page.should have_no_content("#{@member.name} applied")
end
