Classes: 16
[name:ArgumentError, file:null, step:Then ]
[name:Capybara, file:null, step:Given ]
[name:Comment, file:oneclickorgs_one-click-orgs/app/models/comment.rb, step:Then ]
[name:CommentsController, file:oneclickorgs_one-click-orgs/app/controllers/comments_controller.rb, step:Then ]
[name:Director, file:oneclickorgs_one-click-orgs/app/models/director.rb, step:Then ]
[name:DirectorsController, file:oneclickorgs_one-click-orgs/app/controllers/directors_controller.rb, step:Then ]
[name:Meeting, file:oneclickorgs_one-click-orgs/app/models/meeting.rb, step:Then ]
[name:Member, file:oneclickorgs_one-click-orgs/app/models/member.rb, step:Then ]
[name:NoMethodError, file:null, step:Then ]
[name:Organisation, file:oneclickorgs_one-click-orgs/app/models/organisation.rb, step:Then ]
[name:Proposal, file:oneclickorgs_one-click-orgs/app/models/proposal.rb, step:Then ]
[name:Setting, file:oneclickorgs_one-click-orgs/app/models/setting.rb, step:Given ]
[name:Setting, file:oneclickorgs_one-click-orgs/app/models/setting.rb, step:Then ]
[name:URI, file:null, step:Then ]
[name:create_domains, file:null, step:Then ]
[name:set_single_organisation_mode, file:null, step:Then ]

Methods: 40
[name:active, type:Object, file:null, step:Then ]
[name:add this director, type:Object, file:null, step:Then ]
[name:assert_equal, type:Object, file:null, step:Then ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:create proposal to eject this member, type:Object, file:null, step:Then ]
[name:current_url, type:Object, file:null, step:Then ]
[name:dashboard, type:OneClickController, file:oneclickorgs_one-click-orgs/app/controllers/one_click_controller.rb, step:Then ]
[name:description, type:VotingSystems, file:oneclickorgs_one-click-orgs/app/models/voting_systems.rb, step:Then ]
[name:edit, type:MembersController, file:oneclickorgs_one-click-orgs/app/controllers/members_controller.rb, step:Then ]
[name:edit, type:ConstitutionsController, file:oneclickorgs_one-click-orgs/app/controllers/constitutions_controller.rb, step:Then ]
[name:email, type:Member, file:oneclickorgs_one-click-orgs/app/models/member.rb, step:Then ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find_by_email, type:Object, file:null, step:Then ]
[name:first_name, type:Member, file:oneclickorgs_one-click-orgs/app/models/member.rb, step:Then ]
[name:have_css, type:Object, file:null, step:Then ]
[name:host, type:Organisation, file:oneclickorgs_one-click-orgs/app/models/organisation.rb, step:Then ]
[name:index, type:MembersController, file:oneclickorgs_one-click-orgs/app/controllers/members_controller.rb, step:Then ]
[name:join, type:Object, file:null, step:Then ]
[name:last, type:Proposal, file:oneclickorgs_one-click-orgs/app/models/proposal.rb, step:Then ]
[name:last, type:Object, file:null, step:Then ]
[name:last_name, type:Member, file:oneclickorgs_one-click-orgs/app/models/member.rb, step:Then ]
[name:new, type:CoopsController, file:oneclickorgs_one-click-orgs/app/controllers/coops_controller.rb, step:Then ]
[name:new, type:CompaniesController, file:oneclickorgs_one-click-orgs/app/controllers/companies_controller.rb, step:Then ]
[name:page, type:Object, file:null, step:Then ]
[name:path, type:Object, file:null, step:Then ]
[name:path_to, type:Paths, file:oneclickorgs_one-click-orgs/features/support/paths.rb, step:Then ]
[name:push, type:Object, file:null, step:Then ]
[name:raise, type:Object, file:null, step:Then ]
[name:respond_to?, type:Object, file:null, step:Then ]
[name:send, type:Object, file:null, step:Then ]
[name:send invitation, type:Object, file:null, step:Then ]
[name:show, type:MembersController, file:oneclickorgs_one-click-orgs/app/controllers/members_controller.rb, step:Then ]
[name:show, type:ProposalsController, file:oneclickorgs_one-click-orgs/app/controllers/proposals_controller.rb, step:Then ]
[name:show, type:MeetingsController, file:oneclickorgs_one-click-orgs/app/controllers/meetings_controller.rb, step:Then ]
[name:sub, type:Object, file:null, step:Given ]
[name:sub, type:Object, file:null, step:Then ]
[name:submit, type:Object, file:null, step:Then ]
[name:title, type:Object, file:null, step:Then ]
[name:to_sym, type:Object, file:null, step:Then ]

Referenced pages: 35
oneclickorgs_one-click-orgs/app/views/add_member_proposals/_form.html.haml
oneclickorgs_one-click-orgs/app/views/companies/new.html.haml
oneclickorgs_one-click-orgs/app/views/constitutions/edit.html.haml
oneclickorgs_one-click-orgs/app/views/coops/new.html.haml
oneclickorgs_one-click-orgs/app/views/directors/_form.html.haml
oneclickorgs_one-click-orgs/app/views/eject_member_proposals/_form.html.haml
oneclickorgs_one-click-orgs/app/views/founding_members/_form.html.haml
oneclickorgs_one-click-orgs/app/views/meetings/_form.html.haml
oneclickorgs_one-click-orgs/app/views/meetings/show.html.haml
oneclickorgs_one-click-orgs/app/views/members/association/index.html.haml
oneclickorgs_one-click-orgs/app/views/members/association/show.html.haml
oneclickorgs_one-click-orgs/app/views/members/company/index.html.haml
oneclickorgs_one-click-orgs/app/views/members/confirm_resign.html.haml
oneclickorgs_one-click-orgs/app/views/members/coop/index.html.haml
oneclickorgs_one-click-orgs/app/views/members/edit.html.haml
oneclickorgs_one-click-orgs/app/views/members/new.html.haml
oneclickorgs_one-click-orgs/app/views/members/resigned.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/_decision.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/_failed_proposal.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/_meeting.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/_new_member.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/_open_proposals.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/_proposal.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/_resignation.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/_timeline.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/_vote.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/association/dashboard.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/company/dashboard.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/coop/dashboard.html.haml
oneclickorgs_one-click-orgs/app/views/proposals/_description.html.haml
oneclickorgs_one-click-orgs/app/views/proposals/_vote.html.haml
oneclickorgs_one-click-orgs/app/views/proposals/_vote_count.html.haml
oneclickorgs_one-click-orgs/app/views/proposals/show.html.haml
oneclickorgs_one-click-orgs/app/views/setup/index.html.haml
oneclickorgs_one-click-orgs/app/views/shared/_propose_freeform_form.html.haml

