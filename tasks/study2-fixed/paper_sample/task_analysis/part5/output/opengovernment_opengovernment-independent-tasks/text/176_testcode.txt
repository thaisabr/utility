When /^I visit subdomain "(.+)"$/ do |sub|
  Capybara.default_host = "#{sub}.www.example.com" #for Rack::Test
  Capybara.app_host = "http://#{sub}.www.example.com:9887" if Capybara.current_driver == :culerity

  # To test with non-Rack drivers you may have to add the
  # {sub}.www.example.com domains to your /etc/hosts file.
end
When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    click_button(button)
  end
end
When /^(?:|I )fill in "([^"]*)" with "([^"]*)"(?: within "([^"]*)")?$/ do |field, value, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_content(text)
    else
      assert page.has_content?(text)
    end
  end
end
Then /^(?:|I )should be on (.+)$/ do |page_name|
  current_path = URI.parse(current_url).path
  if current_path.respond_to? :should
    current_path.should == path_to(page_name)
  else
    assert_equal path_to(page_name), current_path
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)
    case page_name

    when /the home\s?page/
      '/'
    when /the sign up page/i
      sign_up_path
    when /the sign in page/i
      new_user_session_path
    when /the password reset request page/i
      new_password_path
    when /the admin list of states/i
      admin_states_path
    when /the person page for "(.*)"/i
      person_path(
        Person.find_by_first_name_and_last_name(*parse_person_name($1)))

    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
def parse_person_name(name)
  if name =~ /,/
    name.split(/,/, 2).reverse
  else
    name =~ /^(.*) ([^ ]*)$/
    [$1, $2]
  end
end
