Feature:User Activation
@javascript
Scenario: An admin could activate the pending registration request
Given I open the registration modal
And I fill in "user_login" with "heidi"
And I fill in "user_password" with "test123=321test"
And I fill in "user_password_confirmation" with "test123=321test"
And I fill in "user_firstname" with "Heidi"
And I fill in "user_lastname" with "Swiss"
And I fill in "user_mail" with "heidi@heidiland.com"
And I click on "Create" within ".registration-modal"
Then I should see "Your account was created and is now pending administrator approval"
And I am admin
And I am on the admin page of pending users
Then I should see "heidi" within ".generic-table"
@javascript
Scenario: An admin activates the pending registration request
Given I open the registration modal
And I fill in "user_login" with "heidi"
And I fill in "user_password" with "test123=321test"
And I fill in "user_password_confirmation" with "test123=321test"
And I fill in "user_firstname" with "Heidi"
And I fill in "user_lastname" with "Swiss"
And I fill in "user_mail" with "heidi@heidiland.com"
And I click on "Create" within ".registration-modal"
Then I should see "Your account was created and is now pending administrator approval"
And I am already admin
And I am on the admin page of pending users
When I follow "Activate" within ".generic-table"
Then I should see "Successful update"
Feature: User Registration
@javascript
Scenario: A user can register successfully after filling in the registration form
Given I open the registration modal
And I fill in "user_login" with "heidi"
And I fill in "user_password" with "test123456T?"
And I fill in "user_password_confirmation" with "test123456T?"
And I fill in "user_firstname" with "Heidi"
And I fill in "user_lastname" with "Swiss"
And I fill in "user_mail" with "heidi@test.com"
And I click on "Create" within ".registration-modal"
Then I should see "Your account was created and is now pending administrator approval."
@javascript
Scenario: A user is unable to register if one of the constraints left blank
Given I open the registration modal
And I fill in "user_login" with "heidi"
And I fill in "user_password" with "test123456T?"
And I fill in "user_password_confirmation" with "test123456T?"
And I fill in "user_firstname" with "Heidi"
And I fill in "user_lastname" with "Swiss"
And I click on "Create" within ".registration-modal"
Then the element "#user_mail" should be invalid
@javascript
Scenario: A user is unable to register if the password does not match the confirmation
Given I open the registration modal
And I fill in "user_login" with "heidi"
And I fill in "user_password" with "test123456T?"
And I fill in "user_password_confirmation" with "test1"
And I fill in "user_firstname" with "Heidi"
And I fill in "user_lastname" with "Swiss"
And I fill in "user_mail" with "heidi@test.com"
And I click on "Create" within ".registration-modal"
Then I should see "Confirmation doesn't match Password"
