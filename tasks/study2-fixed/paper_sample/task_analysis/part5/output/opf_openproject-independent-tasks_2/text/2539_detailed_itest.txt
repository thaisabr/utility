Classes: 5
[name:I18n, file:opf_openproject/lib/redmine/i18n.rb, step:Then ]
[name:I18n, file:opf_openproject/lib/redmine/i18n.rb, step:Given ]
[name:Options, file:opf_openproject/lib/plugins/acts_as_journalized/lib/redmine/acts/journalized/options.rb, step:Given ]
[name:Regexp, file:null, step:Then ]
[name:User, file:opf_openproject/app/models/user.rb, step:Then ]

Methods: 38
[name:all, type:Object, file:null, step:Given ]
[name:click, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:Then ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:Then ]
[name:find, type:InstanceFinder, file:opf_openproject/lib/instance_finder.rb, step:Given ]
[name:find_by, type:User, file:opf_openproject/app/models/user.rb, step:Then ]
[name:first, type:User, file:opf_openproject/app/models/user.rb, step:Given ]
[name:first, type:TimelinesCollectionProxy, file:opf_openproject/lib/timelines_collection_proxy.rb, step:Given ]
[name:gsub, type:Object, file:null, step:Then ]
[name:gsub, type:Object, file:null, step:Given ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_selector, type:Object, file:null, step:Then ]
[name:id, type:Timeline, file:opf_openproject/app/models/timeline.rb, step:Then ]
[name:index, type:HomescreenController, file:opf_openproject/app/controllers/homescreen_controller.rb, step:Given ]
[name:last, type:TimelinesCollectionProxy, file:opf_openproject/lib/timelines_collection_proxy.rb, step:Then ]
[name:last, type:TimelinesCollectionProxy, file:opf_openproject/lib/timelines_collection_proxy.rb, step:Given ]
[name:login, type:RowCell, file:opf_openproject/app/cells/users/row_cell.rb, step:Then ]
[name:login, type:AccountController, file:opf_openproject/app/controllers/account_controller.rb, step:Then ]
[name:login, type:LoginSteps, file:opf_openproject/features/support/login_steps.rb, step:Then ]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:Given ]
[name:present?, type:TimelinesCollectionProxy, file:opf_openproject/lib/timelines_collection_proxy.rb, step:Given ]
[name:raise, type:Object, file:null, step:Given ]
[name:selector_for, type:Selectors, file:opf_openproject/features/support/selectors.rb, step:Given ]
[name:set_rack_session, type:Object, file:null, step:Then ]
[name:split, type:Object, file:null, step:Then ]
[name:split, type:Object, file:null, step:Given ]
[name:t, type:I18n, file:opf_openproject/lib/redmine/i18n.rb, step:Then ]
[name:t, type:I18n, file:opf_openproject/lib/redmine/i18n.rb, step:Given ]
[name:to, type:Object, file:null, step:Then ]
[name:translate, type:CukeI18n, file:opf_openproject/features/support/cuke_i18n.rb, step:Then ]
[name:translate, type:CukeI18n, file:opf_openproject/features/support/cuke_i18n.rb, step:Given ]
[name:with_scope, type:WebSteps, file:opf_openproject/features/step_definitions/web_steps.rb, step:Given ]
[name:with_scope, type:WebSteps, file:opf_openproject/features/step_definitions/web_steps.rb, step:Then ]
[name:with_scope, type:WebSteps, file:opf_openproject/features/step_definitions/web_steps.rb, step:When ]
[name:within, type:Object, file:null, step:Given ]
[name:within, type:Object, file:null, step:Then ]

Referenced pages: 11
opf_openproject/app/views/account/_auth_providers.html.erb
opf_openproject/app/views/account/_footer.html.erb
opf_openproject/app/views/account/_login.html.erb
opf_openproject/app/views/account/_password_login_form.html.erb
opf_openproject/app/views/account/_register.html.erb
opf_openproject/app/views/account/login.html.erb
opf_openproject/app/views/announcements/_show.html.erb
opf_openproject/app/views/customizable/_field.html.erb
opf_openproject/app/views/homescreen/index.html.erb
opf_openproject/app/views/hooks/login/_auth_provider.html.erb
opf_openproject/app/views/onboarding/_starting_video_modal.html.erb

