Feature: Databases List
In order to get information about a featured databases
As a user
I want to see the list of the digital collections
@databases
Scenario: View a list of databases
Given I literally go to databases
Then I should see the label 'Search top databases'
@mla
@mla @databases
Scenario: Make sure list contains known database
Given I literally go to databases
Then I should see the label 'General Interest and Reference Biographies'
@mla
@mla @databases
Scenario: Make sure list contains known collection
Given I literally go to databases
And I fill in the search box with 'MLA'
And I press 'search'
Then I should see the label 'MLA'
Feature: Digital Collection View
In order to get information about a Digital Collection
As a user
I want to see the list of the digital collections
@digitalcollections
Scenario: View a list of digital collections
Given I literally go to digitalcollections
Then I should see the label 'Collections digitized and curated by Cornell University Library.'
@conzo
@conzo @digitalcollections
Scenario: Make sure list contains known collection
Given I literally go to digitalcollections
Then I should see the label 'Conzo'
@hiphop
@hiphop @digitalcollections
Scenario: Make sure list contains known collection
Given I literally go to digitalcollections
And I fill in the search box with 'hip hop'
And I press 'search'
Then I should see the label 'Conzo'
Feature: Item view
In order to get information about a specific item
As a user
I want to see details from the item's catalog record, holdings, and availability.
@allow-rescue
@allow-rescue @e404
Scenario: goto an invalid page
When I literally go to abcdefg
Then I should see an error
Then it should have link "mlink" with value "mailto:cul-dafeedback-l@cornell.edu"
@availability
Scenario: View an items holdings
Given I request the item view for 4759
Then I should see the label 'Request'
@aeon
@aeon @rmcnoitems
Scenario: View an items holdings
Given I request the item view for 8753977
Then I should see the label 'Request'
@aeon
Scenario: View an items holdings, and request from aeon
Given I request the item view for 2083253
And click on link "Request"
Then I should see the label 'Upton, G. B. (George Burr), 1882-1942'
@aeon
Scenario: View an items holdings, and request from aeon
Given I request the item view for 2083253
And click on link "Request"
Then I should see the label '16-5-268 This rare item may be delivered only to the RMC Reading Room.'
@aeon
Scenario: View an items holdings, and request from aeon
Given I request the item view for 2083253
Then it should have link "Request" with value "/aeon/2083253"
@aeon
Scenario: View an items holdings, and request from aeon
Given I request the item view for 2083253
And click on link "Request"
Then I should see the label 'Upton, G. B. (George Burr), 1882-1942'
@aeon
Scenario: View an items holdings, and request from aeon
Given I request the item view for 2083253
And click on link "Request"
Then I should see the label '16-5-268 This rare item may be delivered only to the RMC Reading Room.'
@aeon
Scenario: View an items holdings, and request from aeon
Given I request the item view for 2083253
Then it should have link "Request" with value "/aeon/2083253"
@availability
Scenario: As a user I can see the availability for an item
Given I request the item view for 30000
Then I should see the label 'Library Annex'
@availability @due
Scenario: As a user I can see the availability for an item
Given I request the item view for 2269649
Then I should see the label 'Checked out, due'
@availability
Scenario: As a user I can see the availability for an item
Given I request the item view for 1535861
Then I should see the labels 'Library Annex,Olin Library'
@availability
Scenario: As a user I can see the availability for an item at an overriden location
Given I request the item view for 561536
Then I should not see the label 'Olin Library Media Center'
@availability
Scenario: As a user I can see the availability for an item at an overriden location
Given I request the item view for 115628
Then I should see the label 'Temporarily shelved'
@availability
Scenario: As a user I can see the availability for an item at an overriden location but
when all items have an overriden location that location takes over for the main location
Given I request the item view for 2378252
Then I should not see the label 'Shelved in'
@availability
Scenario: As a user I can see the availability for an item at an overriden location but
when all items have an overriden location that location takes over for the main location
Given I request the item view for 2378252
Then I should see the label 'Law Library (Myron Taylor Hall) Rare Books'
@DISCOVERYACCESS-988
@DISCOVERYACCESS-988 @availability
Scenario: As a user I can see the availability for an item at an overriden location but
when all items have an overriden location that location takes over for the main location
Given I request the item view for 2378252
Then I should see the "fa-on-site" class 3 times
@availability
@availability @DISCOVERYACCESS-988
Scenario: As a user I can see the availability for an item at a temporary location that overrides the permanent location.
Given I request the item view for 44112
Then I should not see the label 'Temporarily shelved'
@availability
@DISCOVERYACCESS-988
@availability @DISCOVERYACCESS-988 @nomusic
Scenario: As a user I can see the availability for an item at a temporary location that overrides the permanent location.
Given I request the item view for 2269649
Then I should not see the label 'Music Library Reserve'
@availability
@DISCOVERYACCESS-988
@availability @DISCOVERYACCESS-988 @templocation
Scenario: As a user I can see the availability for an item at a temporary location that overrides the permanent location.
Given I request the item view for 8635196
Then I should see the label 'ILR Library Reserve'
@availability @intransit @DISCOVERYACCESS-1483
Scenario: As a user I can see the availability for an In transit item
Given I request the item view for 114103
Then I should see the labels 'In transit'
@availability @intransit
Scenario: As a user I can see the availability for an In transit item
Given I request the item view for 2000195
Then I should see the labels 'In transit'
@availability @intransit
Scenario: As a user I can see the availability for an In transit item, but no bogus LOC
Given I request the item view for 8272732
Then I should not see the label '%LOC'
@availability @intransit
Scenario: As a user I can see the availability for an In transit item, but no bogus LOC
Given I request the item view for 106223
Then I should not see the label '%LOC'
@availability @holdings @DISCOVERYACCESS-1430 @DISCOVERYACCESS-1483
Scenario: As a user I can see the exactly what copy is available
Given I request the item view for 1535861
Then I should see the label '1 copy'
@availability
@holdings
@DISCOVERYACCESS-1430
@availability @holdings @DISCOVERYACCESS-1430 @DISCOVERYACCESS-1483
Scenario: As a user I can see the exactly what copy is available
Given I request the item view for 7728655
Then I should see the label 'HG4026 .R677 2013 Text Available 3 copies'
