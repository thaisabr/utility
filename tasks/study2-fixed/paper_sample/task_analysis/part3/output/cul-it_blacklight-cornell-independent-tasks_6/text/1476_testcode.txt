Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press '([^"]*)'$/ do |button|
  click_button button
end
Then /^(?:|I )should be on (.+)$/ do |page_name|
  current_path = URI.parse(current_url).path
  if current_path.respond_to? :should
    current_path.should == path_to(page_name)
  else
    assert_equal path_to(page_name), current_path
  end
end
  def path_to(page_name)
    case page_name

    when /the home\s?page/
      root_path

      
    when /the catalog page/
      #catalog_index_path
      root_path
      
    when /the folder page/
      folder_index_path
         
    when /the document page for id (.+)/ 
      catalog_path($1)
      
    when /the facet page for "([^\"]*)"/
      catalog_facet_path($1)

    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Then /^I should see a "([^\"]*)" button$/ do |label|
  page.should have_selector('button#search')
end
Then /^I should get results$/ do
  page.should have_selector("div.document")
end
Then /^I should see an RSS discovery link/ do
  page.should have_selector("link[rel=alternate][type='application/rss+xml']")
end
Then /^I should see an Atom discovery link/ do
  page.should have_selector("link[rel=alternate][type='application/atom+xml']")
end
Then /^I should see OpenSearch response metadata tags/ do
  page.should have_selector("meta[name=totalResults]")
  page.should have_selector("meta[name=startIndex]")
  page.should have_selector("meta[name=itemsPerPage]")
end
Then /^the page title should be "(.*?)"$/ do |title|
  # Capybara 2 ignores invisible text
  # https://github.com/jnicklas/capybara/issues/863
  first('title').native.text == title
end
Then /^the '(.*?)' drop\-down should have an option for '(.*?)'$/ do |menu, option|
  page.has_select?(menu, :with_options => [option]).should == true
end
Then /^I should see the text '(.*?)'$/i do |text|
  page.should have_content(text)
end
When /^I fill in the search box with '(.*?)'$/ do |query|
  query.gsub!(/\\"/, '"')
  fill_in('q', :with => query)
end
Then /^there should be at least (\d+) search results?$/ do |count|
 # print page.html
  page.find("meta[name=totalResults]")['content'].to_i.should >= count.to_i
end
Then /^there should be (\d+) search results$/ do |count|
 # print page.html
  page.find("meta[name=totalResults]")['content'].to_i.should == count.to_i
end
Then /^I should see 'Displaying all (\d+) items' or I should see 'Displaying items (\d+) \- (\d+) of (\d+)'$/ do |arg1, arg2, arg3, arg4|
 # print page.html
  (page.should have_content("Displaying all #{arg1} items")) || (page.should have_content("Displaying items #{arg2} - #{arg3} of #{arg4}"))

end
Then /^I should see the per_page select list$/ do
  page.should have_selector('#per_page-dropdown')
end
Then /^the '(.*?)' select list should default to '(.*?)'$/ do |list, option|
  page.find('#' + list + '-dropdown ul.css-dropdown li.btn > a').text.should == option
end
Then /^the '(.*?)' select list should have an option for '(.*?)'$/ do |list, option|
  page.all('#' + list + '-dropdown ul.css-dropdown li.btn li', :text => option)
end
Then /^I should see each item format$/ do
  within('#documents') do
  	page.should have_css('.blacklight-title_display')
  	page.should have_css('.blacklight-author_display')
  	page.should have_css('div.blacklight-fpl')
  end
end
Then /^results should have a select checkbox$/ do
  within('#documents') do
  	page.should have_selector('form.bookmark_toggle')
  end
end
Then /^results should have a title field$/ do
  within('#documents') do
    page.should have_css('.blacklight-title_display')
  end
end
Then /^it should contain filter "(.*?)" with value "(.*?)"/ do |filter, value|
  page.should have_selector('span.filter-label', :text => filter)
  page.should have_selector('span.filter-value', :text => value)
end
Then /^I should see a facet called '(.*?)'$/ do |facet|
  within('div.facets') do
	page.should have_content(facet)
  end
end
Then /^the '(.*?)' facet (should|should not) be open$/ do |facet, yesno|
  #TODO: Not sure how to test this
  facet.downcase!
  facet = facet_to(facet)
  within('div.facets') do

  	if (facet == 'blacklight-pub_date')
  	  if (yesno == 'should')
	   	page.should have_css("div.#{facet} div.limit_content", :visible => true)
	  else
	 	page.should have_css("div.#{facet} div.limit_content", :visible => false)
  	  end

  	else
  	  if (yesno == 'should')
	   	page.should have_css("div.#{facet} ul", :visible => true)
	  else
	 	page.should have_css("div.#{facet} ul", :visible => false)
	  end
	end
  end
  # within('div.facets') do

  # end
end
  def facet_to(facet) 
    case facet

    when /^format$/
       'blacklight-format'
    when /^topic$/
       'blacklight-subject_topic_facet'
    when /^language$/
       'blacklight-language_facet'
    when /^call number$/
       'blacklight-lc_1letter_facet'
    when /^region$/
       'blacklight-subject_geo_facet'
    when /^era$/
       'blacklight-subject_era_facet'
    when /^location$/
       'blacklight-location_facet'
    when /^publication year$/
       'blacklight-pub_date'
    end
  end
