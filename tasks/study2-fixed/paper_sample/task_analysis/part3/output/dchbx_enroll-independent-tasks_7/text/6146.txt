Feature: Create Employer
In order to offer health and dental insurance benefits to my employees, employers must create and manage an account on the HBX for their organization.  Such organizations are referred to as an Employer
An Employer Representative
Should be able to create an Employer account
Scenario: Employer Representative has previously signed up on HBX
Given I have signed up previously through consumer, broker agency or previous visit to the Employer portal with email "example@example.com"
When I visit the Employer portal to sign in
And I sign in with valid user data with email example@example.com and password "12345678"
Then I should see a welcome page with successful sign in message
Then I should see fields to search for person and employer
And I should see an initial fieldset to enter my name, ssn and dob
And My user data from existing the fieldset values are prefilled using data from my existing account
And I should see a second form with a fieldset for Employer information, including: legal name, DBA, fein, entity_kind, broker agency, URL, address, and phone
And I should see a successful creation message
