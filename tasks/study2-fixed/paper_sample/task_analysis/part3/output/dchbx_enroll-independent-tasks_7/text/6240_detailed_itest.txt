Classes: 4
[name:User, file:dchbx_enroll/app/models/user.rb, step:Given ]
[name:User, file:dchbx_enroll/app/models/user.rb, step:When ]
[name:Watir, file:null, step:When ]
[name:Watir, file:null, step:Then ]

Methods: 63
[name:a, type:Object, file:null, step:When ]
[name:be_truthy, type:Object, file:null, step:Then ]
[name:be_truthy, type:Object, file:null, step:When ]
[name:button, type:Object, file:null, step:When ]
[name:checkboxes, type:Object, file:null, step:When ]
[name:click, type:Object, file:null, step:When ]
[name:count, type:Object, file:null, step:Then ]
[name:create, type:EmployerProfilesController, file:dchbx_enroll/app/controllers/employers/employer_profiles_controller.rb, step:Given ]
[name:create, type:FamilyController, file:dchbx_enroll/app/controllers/employers/family_controller.rb, step:Given ]
[name:create, type:PeopleController, file:dchbx_enroll/app/controllers/people_controller.rb, step:Given ]
[name:create_visitor, type:UserSteps, file:dchbx_enroll/features/step_definitions/user_steps.rb, step:Given ]
[name:delete_all, type:User, file:dchbx_enroll/app/models/user.rb, step:When ]
[name:delete_user, type:UserSteps, file:dchbx_enroll/features/step_definitions/user_steps.rb, step:Given ]
[name:destroy, type:User, file:dchbx_enroll/app/models/user.rb, step:Given ]
[name:div, type:Object, file:null, step:Then ]
[name:divs, type:Object, file:null, step:Then ]
[name:each, type:Object, file:null, step:Then ]
[name:element, type:Object, file:null, step:Then ]
[name:eq, type:Object, file:null, step:Then ]
[name:first, type:BrokerAgencyProfile, file:dchbx_enroll/app/models/broker_agency_profile.rb, step:Given ]
[name:first, type:BrokerRole, file:dchbx_enroll/app/models/broker_role.rb, step:Given ]
[name:first, type:CarrierProfile, file:dchbx_enroll/app/models/carrier_profile.rb, step:Given ]
[name:first, type:EmployeeRole, file:dchbx_enroll/app/models/employee_role.rb, step:Given ]
[name:first, type:EmployerProfile, file:dchbx_enroll/app/models/employer_profile.rb, step:Given ]
[name:first, type:BrokerAgencyProfile, file:dchbx_enroll/app/models/broker_agency_profile.rb, step:When ]
[name:first, type:BrokerRole, file:dchbx_enroll/app/models/broker_role.rb, step:When ]
[name:first, type:CarrierProfile, file:dchbx_enroll/app/models/carrier_profile.rb, step:When ]
[name:first, type:EmployeeRole, file:dchbx_enroll/app/models/employee_role.rb, step:When ]
[name:first, type:EmployerProfile, file:dchbx_enroll/app/models/employer_profile.rb, step:When ]
[name:goto, type:Object, file:null, step:When ]
[name:have_content, type:Object, file:null, step:Then ]
[name:include?, type:Object, file:null, step:Then ]
[name:index, type:WelcomeController, file:dchbx_enroll/app/controllers/employers/welcome_controller.rb, step:Given ]
[name:index, type:WelcomeController, file:dchbx_enroll/app/controllers/welcome_controller.rb, step:Given ]
[name:input, type:Object, file:null, step:When ]
[name:last, type:BrokerAgencyProfile, file:dchbx_enroll/app/models/broker_agency_profile.rb, step:When ]
[name:last, type:BrokerRole, file:dchbx_enroll/app/models/broker_role.rb, step:When ]
[name:last, type:CarrierProfile, file:dchbx_enroll/app/models/carrier_profile.rb, step:When ]
[name:last, type:EmployerProfile, file:dchbx_enroll/app/models/employer_profile.rb, step:When ]
[name:new, type:Consumer/employeeRolesController, file:dchbx_enroll/app/controllers/consumer/employee_roles_controller.rb, step:Given ]
[name:new, type:Employers/employerProfilesController, file:dchbx_enroll/app/controllers/employers/employer_profiles_controller.rb, step:Given ]
[name:p, type:Object, file:null, step:When ]
[name:p, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:Then ]
[name:present?, type:Object, file:null, step:When ]
[name:present?, type:Object, file:null, step:Then ]
[name:rand, type:Object, file:null, step:When ]
[name:search, type:EmployeeRolesController, file:dchbx_enroll/app/controllers/consumer/employee_roles_controller.rb, step:Given ]
[name:select, type:Object, file:null, step:Then ]
[name:set, type:Object, file:null, step:When ]
[name:sign in, type:Object, file:null, step:Given ]
[name:sleep, type:Object, file:null, step:When ]
[name:span, type:Object, file:null, step:Then ]
[name:strong, type:Object, file:null, step:Then ]
[name:strongs, type:Object, file:null, step:Then ]
[name:text, type:Object, file:null, step:Then ]
[name:text_field, type:Object, file:null, step:When ]
[name:text_field, type:Object, file:null, step:Then ]
[name:to, type:Object, file:null, step:Then ]
[name:to, type:Object, file:null, step:When ]
[name:visible?, type:Object, file:null, step:Then ]
[name:visible?, type:Object, file:null, step:When ]
[name:where, type:User, file:dchbx_enroll/app/models/user.rb, step:Given ]

Referenced pages: 16
dchbx_enroll/app/views/consumer/employee_roles/_remote_search_form.html.haml
dchbx_enroll/app/views/consumer/employee_roles/_search_fields.html.erb
dchbx_enroll/app/views/consumer/employee_roles/_welcome_msg.html.erb
dchbx_enroll/app/views/consumer/employee_roles/match.html.erb
dchbx_enroll/app/views/consumer/employee_roles/no_match.html.haml
dchbx_enroll/app/views/consumer/employee_roles/search.html.haml
dchbx_enroll/app/views/consumer/employee_roles/welcome.html.haml
dchbx_enroll/app/views/devise/sessions/new.html.erb
dchbx_enroll/app/views/devise/shared/_links.html.erb
dchbx_enroll/app/views/employers/employer_profiles/new.html.erb
dchbx_enroll/app/views/shared/_benefit_group_fields.html.erb
dchbx_enroll/app/views/shared/_employer_profile_fields.html.erb
dchbx_enroll/app/views/shared/_error_messages.html.erb
dchbx_enroll/app/views/shared/_plan_year_fields.html.erb
dchbx_enroll/app/views/shared/_relationship_benefit_fields.html.erb
dchbx_enroll/app/views/welcome/index.html.erb

