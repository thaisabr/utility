Feature: Item view
In order to get information about a specific item
As a user
I want to see details from the item's catalog record, holdings, and availability.
@all_item_view
@all_item_view @availability
Scenario: View an items holdings
Given I request the item view for 4759
Then I should see the label 'Request'
@all_item_view
@aeon
@all_item_view @aeon @rmcnoitems
Scenario: View an items holdings
Given I request the item view for 8753977
Then I should see the label 'Request'
@all_item_view
@aeon
@all_item_view @aeon @all_item_view @aeon @all_item_view @DISCOVERYACCESS-136
Scenario: As a user, the author's name in an item record is clickable and produces a query resulting in a list of works by that author.
Given I request the item view for 6041
And click on link "Catholic Church. Pope (1939-1958 : Pius XII)"
Then results should have a "author" that looks sort of like "Catholic Church. Pope (1939"
@all_item_view
@aeon @all_item_view @aeon @all_item_view @DISCOVERYACCESS-136 @all_item_view
Scenario: As a user, the author's name in an item record is clickable and produces a query resulting in a list of works by that author.
Given I request the item view for 6041
And click on link "Catholic Church. Pope (1939-1958 : Pius XII)"
Then results should have a "author" that looks sort of like "Catholic Church"
@availability @all_item_view
Scenario: As a user I can see the availability for an item
Given I request the item view for 30000
Then I should see the label 'Library Annex'
@all_item_view
@all_item_view @availability @due
Scenario: As a user I can see the availability for an item
Given I request the item view for 8916645
Then I should see the label 'Checked out, due'
@all_item_view
@availability
@javascript
@bibid1902405
@DISCOVERYACCESS-1659
@all_item_view @availability @javascript @bibid1902405 @DISCOVERYACCESS-1659 @request
Scenario: As a user I can see the availability for an item
Given I request the item view for 1902405
Then I should see the label 'Request'
@all_item_view
@availability
@DISCOVERYACCESS-988
@all_item_view @availability @intransit @DISCOVERYACCESS-1483
Scenario: As a user I can see the availability for an In transit item
Given I request the item view for 5729532
Then I should see the labels 'Missing'
@all_item_view
@all_item_view @availability @intransit
Scenario: As a user I can see the availability for an In transit item
Given I request the item view for 52325
Then I should see the labels 'In transit'
@all_item_view
@all_item_view @availability @intransit
Scenario: As a user I can see the availability for an In transit item, but no bogus LOC
Given I request the item view for 8272732
Then I should not see the label '%LOC'
@all_item_view
@all_item_view @availability @intransit
Scenario: As a user I can see the availability for an In transit item, but no bogus LOC
Given I request the item view for 106223
Then I should not see the label '%LOC'
@all_item_view
@all_item_view @boundwith @DISCOVERYACCESS-1903 @DISCOVERYACCESS-1328
Scenario: Show the record properly an item is bound with another item, and there are several volumes in separate items in other volumes
Given I request the item view for 28297
Then I should see the label 'The v.75B item is bound with'
@all_item_view
@boundwith
@DISCOVERYACCESS-1903
@hours-page @all_item_view @availability @javascript @bibid9264410 @DISCOVERYACCESS-2855
Scenario: As a user I can see the availability for an item
Given I request the item view for 9264410
Then I should see the label 'On-site use'
And I should not see the label 'Request item'
And it should have link "Hours/Map" with value "http://spif.astro.cornell.edu/"
@all_item_view
@availability
@javascript
@bibid/9203210
@all_item_view @availability @javascript @bibid/9203210 @DISCOVERYACCESS-3413
Scenario: As a user I can see the availability for an item
Given I request the item view for 9203210
Then I should see the label 'On-site use'
And I should see the label 'Request item'
And it should have link "Hours/Map" with value "http://spif.astro.cornell.edu/"
@all_item_view
@availability
@javascript
@bibid1545844
@all_item_view @availability @javascript @bibid1545844 @DISCOVERYACCESS-1380
Scenario: As a user I can see the availability for an item
Given I request the item view for 1545844
Then I should see the label 'On-site use'
Then I should see the label 'Request item'
@hours-page
@on-site-use
