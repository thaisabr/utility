When /^(?:|I )press '([^"]*)'$/ do |button|

  if button == 'search'
    page.find(:css, 'button#search-btn').click
  else
    click_button button
  end
end
When /^(?:|I )literally go to (.+)$/ do |page_name|
  visit page_name
end
When /^I fill in the authorities search box with '(.*?)'$/ do |query|
  query.gsub!(/\\"/, '"')
  fill_in('authq', :with => query)
end
Given /^I click a link with text '(.*?)' within '(.*?)'$/ do |text, id|
  find(:xpath, "//*[@id='#{id}']//a[text()='#{text}']", visible:false).click
end
Given /^I click '(.*?)' in the first page navigator$/ do |text|
  within(:xpath, "(//div[contains(@class, 'results-count')])[1]") do
    click_link(text)
  end
end
Given /^I click '(.*?)' in the last page navigator$/ do |text|
  within(:xpath, "(//div[contains(@class, 'results-count')])[last()]") do
    click_link(text)
  end
end
Given /^I select ["'](.*?)["'] from the ["'](.*?)["'] drop\-down$/ do |option, menu|
  #select(option, :from => menu)
  find('#' + menu).find(:option,"#{option}").select_option
end
Then /^I should get results$/ do
  page.should have_selector(".document")
end
Then /^click on link "(.*?)"$/ do |link|
  click_link link
end
Then /^click on first link "(.*?)"$/ do |link|
  l = page.first('a', :text => link)
  l.click 
end
Then /^I (should|should not) see the label '(.*?)'$/ do |yesno, label|
  if yesno == "should not"
	page.should_not have_content(label)
  else
  	page.should have_content(label)
  end
end
