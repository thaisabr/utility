Feature: Item view
In order to get information about a specific item
As a user
I want to see details from the item's catalog record, holdings, and availability.
@all_item_view
@allow-rescue
@all_item_view @availability
Scenario: View an items holdings
Given I request the item view for 4759
Then I should see the label 'Request'
@all_item_view
@aeon
@all_item_view @aeon @rmcnoitems
Scenario: View an items holdings
Given I request the item view for 8753977
Then I should see the label 'Request'
@all_item_view
@all_item_view @aeon @all_item_view @aeon @all_item_view @DISCOVERYACCESS-136
Scenario: As a user, the author's name in an item record is clickable and produces a query resulting in a list of works by that author.
Given I request the item view for 6041
And click on link "Catholic Church. Pope (1939-1958 : Pius XII)"
Then results should have a "author" that looks sort of like "Catholic Church. Pope (1939"
@all_item_view
@aeon @all_item_view @aeon @all_item_view @DISCOVERYACCESS-136 @all_item_view
Scenario: As a user, the author's name in an item record is clickable and produces a query resulting in a list of works by that author.
Given I request the item view for 6041
And click on link "Catholic Church. Pope (1939-1958 : Pius XII)"
Then results should have a "author" that looks sort of like "Catholic Church"
@availability @all_item_view
Scenario: As a user I can see the availability for an item
Given I request the item view for 30000
Then I should see the label 'Library Annex'
@all_item_view
@all_item_view @availability @due
Scenario: As a user I can see the availability for an item
Given I request the item view for 8916645
Then I should see the label 'Checked out, due'
@all_item_view
@availability
@javascript
@bibid1902405
@DISCOVERYACCESS-1659
@all_item_view @availability @javascript @bibid1902405 @DISCOVERYACCESS-1659 @request
Scenario: As a user I can see the availability for an item
Given I request the item view for 1902405
Then I should see the label 'Request'
@all_item_view
@availability
@DISCOVERYACCESS-988
@all_item_view @availability @intransit @DISCOVERYACCESS-1483
Scenario: As a user I can see the availability for an In transit item
Given I request the item view for 5729532
Then I should see the labels 'In transit'
@all_item_view
@all_item_view @availability @intransit
Scenario: As a user I can see the availability for an In transit item
Given I request the item view for 52325
Then I should see the labels 'In transit'
@all_item_view
@all_item_view @availability @intransit
Scenario: As a user I can see the availability for an In transit item, but no bogus LOC
Given I request the item view for 8272732
Then I should not see the label '%LOC'
@all_item_view
@all_item_view @availability @intransit
Scenario: As a user I can see the availability for an In transit item, but no bogus LOC
Given I request the item view for 106223
Then I should not see the label '%LOC'
@all_item_view
@hours-page @all_item_view @availability @javascript @bibid9264410 @DISCOVERYACCESS-2855
Scenario: As a user I can see the availability for an item
Given I request the item view for 9264410
Then I should see the label 'On-site use'
And I should not see the label 'Request item'
And it should have link "Hours/Map" with value "http://spif.astro.cornell.edu/"
@all_item_view
@availability
@javascript
@bibid/9203210
@all_item_view @availability @javascript @bibid/9203210 @DISCOVERYACCESS-3413
Scenario: As a user I can see the availability for an item
Given I request the item view for 9203210
Then I should see the label 'On-site use'
And I should see the label 'Request item'
And it should have link "Hours/Map" with value "http://spif.astro.cornell.edu/"
@all_item_view
@availability
@javascript
@bibid1545844
@all_item_view @availability @javascript @bibid1545844 @DISCOVERYACCESS-1380
Scenario: As a user I can see the availability for an item
Given I request the item view for 1545844
Then I should see the label 'On-site use'
Then I should see the label 'Request item'
@hours-page
@on-site-use
Feature: Select and export items from the result set
In order to save results for later
As a researcher
I want to select items from a results list and export them in different ways.
@citations @rdf_zotero @all_select_and_export
Scenario: User needs to send a book record to rdf zotero format (might go to zotero)
Given I request the item view for 3261564
Given I request the item view for 3261564.rdf_zotero
Then I should see the xml text '<z:itemType>audioRecording</z:itemType>'
Then I should see the xml text '<dc:title>Debabrata Biśvāsa</dc:title>'
Then I should see the xml path 'z','//z:composers','http://www.zotero.org/namespaces/export#','Cakrabarttī'
@all_select_and_export
Scenario: User needs to send a book record to rdf zotero format (might go to zotero)
Given I request the item view for 10055679
Given I request the item view for 10055679.rdf_zotero
Then I should see the xml path 'dcterms','//dcterms:LCC','http://purl.org/dc/terms/','Mann Library  SF98.A5 M35 2017'
@all_select_and_export @DISCOVERYANDACCESS-3603 @DISCOVERYANDACCESS-3603_acquired_dt_returned
Scenario: User needs to see the date acquired in a JSON feed
When I literally go to /catalog.json?advanced_query=yes&boolean_row[1]=AND&counter=1&op_row[]=AND&op_row[]=AND&q=author%2Fcreator+%3D+Charlier&q_row[]=Zombies&q_row[]=Charlier&search_field=advanced&search_field_row[]=title&search_field_row[]=author%2Fcreator&sort=score+desc%2C+pub_date_sort+desc%2C+title_sort+asc&total=1
Then I should see the text 'Zombies : an anthropological investigation of the living dead'
And I should see the text 'acquired_dt'
And I should see the text '2017-10-23T00:00:00Z'
And I should get a response with content-type "application/json; charset=utf-8"
@all_select_and_export @DISCOVERYANDACCESS-3603 @DISCOVERYANDACCESS-3603_rss
Scenario: User needs to see zombies as an rss feed
When I literally go to /catalog.rss?advanced_query=yes&boolean_row[1]=AND&counter=1&op_row[]=AND&op_row[]=AND&q=author%2Fcreator+%3D+Charlier&q_row[]=Zombies&q_row[]=Charlier&search_field=advanced&search_field_row[]=title&search_field_row[]=author%2Fcreator&sort=score+desc%2C+pub_date_sort+desc%2C+title_sort+asc&total=1
Then I should see the xml text '<title>Zombies : an anthropological investigation of the living dead</title>'
And I should see the text 'Gainesville : University Press of Florida, [2017]'
And I should see the text 'GR581 .C4313 2017 -- Olin Library'
And I should get a response with content-type "application/rss+xml; charset=utf-8"
