Feature: Search
In order to find documents in an advanced search way
As a user
I want to enter terms, select terms for fields fields, and select number of results per page
@all_search
@adv_search
@searchpage
@adv_search @all_search @searchpage @javascript
Scenario: Advanced search with title NOT publisher
When I literally go to advanced
And the page title should be "Advanced Search - Cornell University Library Catalog"
And I should see a stylesheet
And I fill in "q_row1" with 'combinatorial algorithms'
And I select 'Title' from the 'search_field_advanced' drop-down
Then I should select radio "NOT"
And I fill in "q_row2" with 'springer'
And I select 'Publisher' from the 'search_field_advanced2' drop-down
And I press 'advanced_search'
And I sleep 4 seconds
Then I should get results
And I should see the label '1 - 20 of'
And I should see the label 'NOT Publisher: springer'
@adv_search
@all_search
@searchpage
@adv_search @all_search @searchpage @javascript
Scenario: Advanced search with title NOT publisher
When I literally go to advanced
And the page title should be "Advanced Search - Cornell University Library Catalog"
And I should see a stylesheet
And I fill in "q_row1" with 'combinatorial algorithms'
And I select 'Title' from the 'search_field_advanced' drop-down
Then I should select radio "NOT"
And I fill in "q_row2" with 'springer'
And I select 'All Fields' from the 'search_field_advanced2' drop-down
And I press 'advanced_search'
Then I should get results
And I should see the label '1 - 20 of 43'
@adv_search
@all_search
@callnumber
@adv_search @all_search @notes @javascript
Scenario: Perform an advanced search by notes
When I literally go to advanced
And I sleep 4 seconds
And I fill in "q_row1" with 'English, German, Italian, Latin, or Portugese'
And I select 'phrase' from the 'op_row' drop-down
And I select 'Notes' from the 'search_field_advanced' drop-down
And I press 'advanced_search'
Then I should get results
And I should see the label '1 result'
@adv_search
@all_search
@issnisbn
@adv_search @all_search @adv_notes @javascript
Scenario: Perform an advanced search by notes
When I literally go to advanced
And I sleep 4 seconds
And I fill in "q_row1" with 'English, German, Italian, Latin, or Portugese'
And I select 'all' from the 'op_row' drop-down
And I select 'Notes' from the 'search_field_advanced' drop-down
And I fill in "q_row2" with 'Bibliotheca Instituti Historici'
And I select 'phrase' from the 'op_row2' drop-down
And I press 'advanced_search'
Then I should get results
And I should see the label '1 result'
@adv_search
@all_search
@adv_donor
@adv_search @all_search @adv_donor @javascript
Scenario: Perform an advanced search by donor
When I literally go to advanced
And I fill in "q_row1" with 'Jan Olsen'
And I select 'all' from the 'op_row' drop-down
And I select 'Donor Name' from the 'search_field_advanced' drop-down
And I press 'advanced_search'
Then I should get results
And I should see the label '1 - 19 of 19'
@adv_search
@all_search
@adv_donor
@adv_search @all_search @adv_donor @javascript
Scenario: Perform an advanced search by donor
When I literally go to advanced
And I fill in "q_row1" with 'Jan'
And I select 'all' from the 'op_row' drop-down
And I select 'Donor Name' from the 'search_field_advanced' drop-down
And I fill in "q_row2" with 'Olsen'
And I select 'all' from the 'op_row2' drop-down
And I select 'Donor Name' from the 'search_field_advanced2' drop-down
And I press 'advanced_search'
Then I should get results
And I should see the label '1 - 19 of 19'
And I should see the label 'Modify advanced'
@adv_search
@all_search
@adv_subject
@adv_search @all_search @adv_subject @javascript
Scenario: Perform an advanced search by subject
When I literally go to advanced
And I fill in "q_row1" with 'Molecular Biology'
And I select 'all' from the 'op_row' drop-down
And I select 'Subject' from the 'search_field_advanced' drop-down
And I fill in "q_row2" with 'Recombinant Dna'
And I select 'all' from the 'op_row2' drop-down
And I select 'Subject' from the 'search_field_advanced2' drop-down
And I press 'advanced_search'
Then I should get results
And I should see the label '1 - '
@adv_search
@all_search
@adv_subject
@adv_search @all_search @adv_subject @javascript
Scenario: Perform an advanced search by subject
When I literally go to advanced
And I fill in "q_row1" with 'Molecular Biology'
And I select 'phrase' from the 'op_row' drop-down
And I select 'Subject' from the 'search_field_advanced' drop-down
And I fill in "q_row2" with 'Recombinant Dna'
And I select 'phrase' from the 'op_row2' drop-down
And I select 'Subject' from the 'search_field_advanced2' drop-down
And I press 'advanced_search'
Then I should get results
And I should see the label '1 - 9 of 9'
@adv_search
@all_search
@adv_subject
@adv_search @all_search @adv_subject @javascript
Scenario: Perform an advanced search by subject
When I literally go to advanced
And I fill in "q_row1" with 'Molecular Biology'
And I select 'phrase' from the 'op_row' drop-down
And I select 'Subject' from the 'search_field_advanced' drop-down
And I fill in "q_row2" with 'Recombinant Dna'
And I select 'phrase' from the 'op_row2' drop-down
And I select 'Subject' from the 'search_field_advanced2' drop-down
And click on link "add-row"
And I sleep 4 seconds
And I fill in "q_row3" with 'yeast'
And I select 'phrase' from the 'op_row3' drop-down
And I select 'Title' from the 'search_field_advanced3' drop-down
And I press 'advanced_search'
Then I should get results
And I should see the label '1 result'
And I should see the label 'Yeast molecular biology--recombinant DNA'
@adv_search
@all_search
@adv_subject
@begins_with @adv_search @all_search @adv_place @javascript
Scenario: Perform a 1 row  advanced search by begins with Title
When I literally go to advanced
And I fill in "q_row1" with 'we were feminists'
And I select 'begins' from the 'op_row' drop-down
And I select 'Title' from the 'search_field_advanced' drop-down
And I press 'advanced_search'
Then I should get results
And I should see the label 'Modify advanced'
And I should see the label 'We were Feminists'
And I should see the label '1 result'
@wip
@begins_with
@adv_search
@all_search
@adv_place
@javascript
@all_search @adv_place @javascript @DISCOVERYACCESS-3350
Scenario: Perform a 2 row  advanced search with a blank in one field.
When I literally go to advanced
And I fill in "q_row1" with ' '
And I fill in "q_row2" with 'we were once'
And click on link "add-row"
And I sleep 4 seconds
And I fill in "q_row3" with ' '
And I press 'advanced_search'
Then I should not see the label 'searched'
@javascript
@javascript @DISCOVERYACCESS-3350
Scenario: Perform a 2 row  advanced search with a blank in one field.
When I literally go to advanced
And I fill in "q_row1" with ' '
And I fill in "q_row2" with 'we were once'
And I press 'advanced_search'
Then I should not see the label 'searched'
@adv_search
@all_search
@adv_title_percent
@adv_search @all_search @adv_title_percent @javascript
Scenario: Perform a 2 row  advanced search with Title, with percent that must be url encoded.
When I literally go to advanced
And I fill in "q_row1" with 'beef'
And I select 'Title' from the 'search_field_advanced' drop-down
And I fill in "q_row2" with '100%'
And I select 'Title' from the 'search_field_advanced2' drop-down
And I press 'advanced_search'
Then I should get results
Then it should have link "Title: beef" with value 'catalog?&q_row[]=100%25&boolean_row[1]=AND&op_row[]=AND&search_field_row[]=title&search_field=advanced&action=index&commit=Search'
Then click on first link "Title: beef"
@adv_search
@all_search
@adv_title_percent
@adv_search @all_search @adv_title_percent @javascript
Scenario: Perform a 2 row  advanced search with Title, with percent that must be url encoded.
When I literally go to advanced
And I fill in "q_row1" with 'manual of the trees of north america (exclusive of mexico)'
And I fill in "q_row2" with 'sargent, charles sprague'
And I select 'Title' from the 'search_field_advanced2' drop-down
And I press 'advanced_search'
Then I should get results
@adv_search
@all_search
@adv_place
@javascript
@adv_search @all_search @adv_place @javascript @allow_rescue
Scenario: Perform a 1 row  advanced search by begins with Title
When I literally go to advanced
And I fill in "q_row1" with quoted 'varieties of capitalism'
And I select 'begins' from the 'op_row' drop-down
And I press 'advanced_search'
Then I should get results
And I should see the label 'Modify advanced'
And I should see the label 'Varieties of capitalism and business history'
@begins_with
@adv_search
@all_search
@adv_place
@javascript
@begins_with @adv_search @all_search @adv_place @javascript @allow_rescue
Scenario: Perform a 1 row  advanced search by begins with Title
When I literally go to advanced
And I fill in "q_row1" with 'game design'
And I press 'advanced_search'
Then I should get results
And I should see the label 'Modify advanced search'
Then click on first link "Book"
Then click on first link "Modify advanced search"
And I should see the label 'Add a row'
Feature: Results list
In order to find items that I search for
As a user
I want to view a list of search results with various options.
@all_results_list @search_with_view_all_books
Scenario: Search with view all books link
Given I literally go to search
When I fill in "q" with 'nature'
And I press 'Search'
When I follow "link_top_book"
And I should see the text "Nature : the double helix"
@all_results_list @search_with_view_all_digital_collections
Scenario: Search with view all books link
Given I literally go to search
When I fill in "q" with 'game design'
And I press 'Search'
Then I should get bento results
And I should see the text "Def Jam"
@all_results_list @search_with_view_advanced_link
Scenario: Search with view all books link
Given I literally go to search
When I fill in "q" with 'game design'
And I press 'Search'
Then I should get bento results
And I should see the text "or use advanced search"
@all_results_list @search_with_view_all_libguides
Scenario: Search with view all books link
Given I literally go to search
When I fill in "q" with 'African American Historical Newspapers Online'
And I press 'Search'
Then I should get bento results
And I should see the text "research guide"
@all_results_list @search_with_view_all_music_match_box
Scenario: Search with view all music link
Given I literally go to search
When I fill in "q" with 'nature morte'
And I press 'Search'
Then box "link_top_musical_recording" should match "0" th "from Catalog" in "page_entries"
@all_results_list @search_with_view_all_manuscript_archive
Scenario: Search with view all music link
Given I literally go to search
When I fill in "q" with 'george burr upton'
And I press 'Search'
Then I should get bento results
Then box "link_top_manuscript_archive" should match "0" th "from Catalog" in "page_entries"
@all_results_list @search_with_view_all_journals_match_box_with_percent
Scenario: Search with view all journals link
Given I literally go to search
When I fill in "q" with 'beef'
And I press 'Search'
Then I should get bento results
Then box "link_top_journal_periodical" should match "0" th "from Catalog" in "page_entries"
@all_results_list @search_with_view_all_books_match_box_with_percent
Scenario: Search with view all books link
Given I literally go to search
When I fill in "q" with 'beef'
And I press 'Search'
Then I should get bento results
Then box "link_top_book" should match "2" nd "from Catalog" in "page_entries"
@all_results_list @search_with_view_all_computer_file_match_box_with_ampersand
Scenario: Search with view all journals link
Given I literally go to search
When I fill in "q" with '100 Vietnamese painters & sculptors'
And I press 'Search'
Then I should get bento results
Then box "link_top_computer_file" should match "0" th "from Catalog" in "page_entries"
@all_results_list @search_with_view_all_article_match_box
Scenario: Search with view all article link should match bento box total
Given I literally go to search
When I fill in "q" with 'stress testing cardio horse insights'
And I press 'Search'
Then I should get bento results
@all_results_list @search_with_view_all_article_match_box
Scenario: Search with view all article link should match bento box total
Given I literally go to search
When I fill in "q" with 'photoplethysmography methodological studies arterial stiffness'
And I press 'Search'
Then I should get bento results
