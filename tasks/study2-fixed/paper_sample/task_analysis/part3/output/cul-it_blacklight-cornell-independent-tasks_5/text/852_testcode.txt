When /^(?:|I )fill in "([^"]*)" with ["']([^"]*)["']$/ do |field, value|
  fill_in(field, :with => value)
end
When /^(?:|I )press '([^"]*)'$/ do |button|

  if button == 'search'
    page.find(:css, 'button#search-btn').click
  else
    click_button button
  end
end
When /^(?:|I )literally go to (.+)$/ do |page_name|
  visit page_name
end
Then /^(?:|I )should be on (.+)$/ do |page_name|
  current_path = URI.parse(current_url).path
  if current_path.respond_to? :should
    current_path.should == path_to(page_name)
  else
    assert_equal path_to(page_name), current_path
  end
end
  def path_to(page_name)
    case page_name

    when /the home\s?page/
      root_path

      
    when /the catalog page/
      catalog_index_path
      
    when /the folder page/
      folder_index_path
         
    when /the document page for id (.+)/ 
      catalog_path($1)
      
    when /the facet page for "([^\"]*)"/
      catalog_facet_path($1)
      
    when /the search page/
      single_search_path

    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Then /^I should get results$/ do
  page.should have_selector(".document")
end
Then /^I should not get results$/ do
  page.should_not have_selector("div.document")
end
Then /^I should see the text "(.*?)"$/i do |text|
  page.should have_content(text)
end
Then /^I should not see the text "(.*?)"$/i do |text|
  page.should_not have_content(text)
end
When /^I follow "([^\"]*)"$/ do |link|
   click_link(link)
end
Then(/^facet "(.*?)" should match "(.*?)" (nth|th|rd|st|nd) "(.*?)" in "(.*?)"$/) do |label,nth,nstr,type,divtag|
     total = all('.view-all')[nth.to_i]
     total.should have_content(type) 
     #print "total on bento box page is #{total.text}\n"
     num = total.text.match(/([0-9,]+)/)[0]
     #print "#{__FILE__} #{__LINE__} num is #{num}\n"
     l2 = find('#' + label)
     href =  l2[:href]
     if type.match("from Catalog")
       cmd =  "wget -O -  '#{href}' 2>/dev/null"
       page2 = `#{cmd}`
       pagedom = Nokogiri::HTML(page2)   
       pagedom.css('.'+divtag)[0].should_not be_nil 
       numx = pagedom.css('.'+divtag)[0].text 
       #print "total2 on view all page is #{numx}\n"
       numx.should_not be_nil
       numx.match(/of\s+(\d+)/).should_not be_nil
       numx.match(/of\s+(\d+)/)[1].should_not be_nil
       num2 = numx.match(/of\s+(\d+)/)[1]
     else
       visit(href)
       #print "HREF is #{href}\n"
       total2 = find('.'+divtag,match: :first)
       total2.should_not be_nil
       #print "total2 on view all page is #{total2.text}\n"
       num2 = total2.text.match(/of ([0-9,]+) /)[1]
     end
     num.should_not be_nil
     num2.should_not be_nil
     num2.gsub!(',','')
     num.gsub!(',','')
     diff = (num2.to_i - num.to_i).abs
     diff.should <=(20)
end
Then(/^box "(.*?)" should match "(.*?)" (nth|th|rd|st|nd) "(.*?)" in "(.*?)"$/) do |label,nth,nstr,type,divtag|
     total = all('.view-all')[nth.to_i]
     total.should have_content(type) 
     #print "total on bento box page is #{total.text}\n"
     num = total.text.match(/([0-9,]+)/)[0]
     #print "#{__FILE__} #{__LINE__} num is #{num}\n"
     #print page.html
     l2 = find('#' + label)
     href =  l2[:href]
     case
     when type.match("from Articles")
       cmd =  "wget -O -  '#{href}' 2>/dev/null"
       page2 = `#{cmd}`
       pagedom = Nokogiri::HTML(page2)   
       pagedom.css('#'+divtag)[0].should_not be_nil 
       numx = pagedom.css('#'+divtag)[0].text 
       #print "total2 on view all page is #{numx}\n"
       numx.should_not be_nil
       numx.match(/returned\s+(\d+)/).should_not be_nil
       numx.match(/returned\s+(\d+)/)[1].should_not be_nil
       num2 = numx.match(/returned\s+(\d+)/)[1]
     when type.match("from Catalog")
       cmd =  "wget -O -  '#{href}' 2>/dev/null"
       #print "****cmd is #{cmd}"
       page2 = `#{cmd}`
       #print "++++ #{page2}"
       pagedom = Nokogiri::HTML(page2)   
       pagedom.css('.'+divtag)[0].should_not be_nil 
       numx = pagedom.css('.'+divtag)[0].text 
       #print "total2 on view (line #{__LINE__}  all page is '#{numx}'\n"
       numx.should_not be_nil
       if numx.match(/of\s+(\d+)/)
         numx.match(/of\s+(\d+)/).should_not be_nil
         numx.match(/of\s+(\d+)/)[1].should_not be_nil
         #print "Inspect:" +  numx.match(/of\s+([0-9,]) /).inspect
         num2 = numx.match(/of\s+([0-9,]+)/)[1]
       else
         numx.match(/(\d+)\s+result/).should_not be_nil
         numx.match(/(\d+)\s+result/)[1].should_not be_nil
         #print "Inspect:" +  numx.match(/(\d+)\s+result/).inspect
         num2 = numx.match(/([0-9,]+)\s+result/)[1]
       end
       #print "num2 on view (line #{__LINE__} all page is #{num2}\n"
     else
       visit(href)
       #print "HREF is #{href}\n"
       total2 = find('.'+divtag,match: :first)
       total2.should_not be_nil
       #print "total2 on view all page is #{total2.text}\n"
       num2 = total2.text.match(/of ([0-9,]+) /)[1]
     end
     num.should_not be_nil
     num2.should_not be_nil
     num2.gsub!(',','')
     num.gsub!(',','')
     #print "box total of items = #{num}, and page number of items = #{num2}\n"
     diff = (num2.to_i - num.to_i).abs
     diff.should <=(20)
end
