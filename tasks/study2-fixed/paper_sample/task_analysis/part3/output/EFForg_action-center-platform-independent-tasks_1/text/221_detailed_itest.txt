Classes: 10
[name:ActionPage, file:EFForg_action-center-platform/app/models/action_page.rb, step:When ]
[name:ActionPage, file:EFForg_action-center-platform/spec/factories/action_page.rb, step:When ]
[name:ActionPage, file:EFForg_action-center-platform/app/models/action_page.rb, step:Given ]
[name:ActionPage, file:EFForg_action-center-platform/spec/factories/action_page.rb, step:Given ]
[name:FactoryGirl, file:null, step:Given ]
[name:Petition, file:EFForg_action-center-platform/app/models/petition.rb, step:Given ]
[name:RSpec, file:null, step:When ]
[name:SmartyStreets, file:EFForg_action-center-platform/lib/smarty_streets.rb, step:When ]
[name:User, file:EFForg_action-center-platform/app/models/user.rb, step:When ]
[name:User, file:EFForg_action-center-platform/app/models/user.rb, step:Given ]

Methods: 32
[name:allow, type:Object, file:null, step:When ]
[name:and_return, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:Given ]
[name:create_an_action_page_petition_needing_one_more_signature, type:UserSteps, file:EFForg_action-center-platform/features/step_definitions/user_steps.rb, step:Given ]
[name:create_community_member_user, type:UserSteps, file:EFForg_action-center-platform/features/step_definitions/user_steps.rb, step:Given ]
[name:create_visitor, type:UserSteps, file:EFForg_action-center-platform/features/step_definitions/user_steps.rb, step:Given ]
[name:delete_user, type:UserSteps, file:EFForg_action-center-platform/features/step_definitions/user_steps.rb, step:Given ]
[name:destroy, type:User, file:EFForg_action-center-platform/app/models/user.rb, step:Given ]
[name:downcase, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:find_by_email, type:User, file:EFForg_action-center-platform/app/models/user.rb, step:Given ]
[name:first, type:Object, file:null, step:When ]
[name:get_city_state, type:SmartyStreets, file:EFForg_action-center-platform/lib/smarty_streets.rb, step:When ]
[name:gsub, type:Object, file:null, step:When ]
[name:has_content?, type:Object, file:null, step:When ]
[name:have_content, type:Object, file:null, step:Then ]
[name:index, type:Admin/actionPagesController, file:EFForg_action-center-platform/app/controllers/admin/action_pages_controller.rb, step:Given ]
[name:last, type:Object, file:null, step:When ]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:When ]
[name:receive, type:Object, file:null, step:When ]
[name:show, type:ActionPagesController, file:EFForg_action-center-platform/app/controllers/admin/action_pages_controller.rb, step:Given ]
[name:sign_in, type:UserSteps, file:EFForg_action-center-platform/features/step_definitions/user_steps.rb, step:Given ]
[name:sign_in, type:UserSteps, file:EFForg_action-center-platform/features/step_definitions/user_steps.rb, step:When ]
[name:sleep, type:Object, file:null, step:When ]
[name:split, type:Object, file:null, step:When ]
[name:stub_smarty_streets, type:Env, file:EFForg_action-center-platform/features/support/env.rb, step:When ]
[name:to, type:Object, file:null, step:Then ]
[name:to, type:Object, file:null, step:When ]
[name:with, type:Object, file:null, step:When ]

Referenced pages: 5
EFForg_action-center-platform/app/views/admin/_header.html.erb
EFForg_action-center-platform/app/views/admin/_nav_tabs.html.erb
EFForg_action-center-platform/app/views/admin/action_pages/_date_range_form.html.erb
EFForg_action-center-platform/app/views/admin/action_pages/_date_vars.html.erb
EFForg_action-center-platform/app/views/admin/action_pages/index.html.erb

