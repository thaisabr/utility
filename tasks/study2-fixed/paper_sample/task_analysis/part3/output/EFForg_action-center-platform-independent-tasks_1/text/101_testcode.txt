When (/^I go to "(.*)"$/) do |path|
  visit path
end
When /^I press "([^\"]*)"$/ do |button|
  click_button(button)
end
When /^I click "([^\"]*)"$/ do |link|
  click_link(link)
end
When /^I fill in "([^\"]*)" with "([^\"]*)"$/ do |field, value|
  fill_in(field.gsub(' ', '_'), :with => value)
end
When /^I select "([^\"]*)" from "([^\"]*)"$/ do |value, field|
  select(value, :from => field)
end
When /^I check "([^\"]*)"$/ do |field|
  check(field)
end
Then(/^"(.*?)" should be selected from "(.*?)"$/) do |value, field|
  selected = false
  find_field(field).all('option[selected]').each do |el|
    if el.text == value
      selected = true
      break
    end
  end
  selected
end
Given(/^I exist as an activist$/) do
  create_activist_user
end
Given(/^I am logged in$/) do
  sign_in
end
def create_activist_user
  create_visitor
  delete_user
  @user = FactoryGirl.create(:activist_user, email: @visitor[:email], password: @visitor[:password])
end
def sign_in
  visit '/login'
  fill_in "Email", :with => @visitor[:email]
  fill_in "Password", :with => @visitor[:password]

  click_button "Sign in"
end
def create_visitor
  @visitor ||= { name: "Test User",
    email: "me@example.com",
    zip_code: "94117",
    password: "strong passwords defeat lobsters covering wealth",
    password_confirmation: "strong passwords defeat lobsters covering wealth" }
end
def delete_user
  @user ||= User.find_by_email(@visitor[:email])
  @user.destroy unless @user.nil?
end
Given(/^a partner named "(.*?)" exists$/) do |name|
  @action_page = FactoryGirl.create(:partner, name: name)
end
