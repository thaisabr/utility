Classes: 4
[name:ActionPage, file:EFForg_action-center-platform/app/models/action_page.rb, step:Given ]
[name:ActionPage, file:EFForg_action-center-platform/spec/factories/action_page.rb, step:Given ]
[name:FactoryGirl, file:null, step:Given ]
[name:User, file:EFForg_action-center-platform/app/models/user.rb, step:Given ]

Methods: 19
[name:all, type:Object, file:null, step:Then ]
[name:check, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_link, type:Object, file:null, step:When ]
[name:create_activist_user, type:UserSteps, file:EFForg_action-center-platform/features/step_definitions/user_steps.rb, step:Given ]
[name:create_visitor, type:UserSteps, file:EFForg_action-center-platform/features/step_definitions/user_steps.rb, step:Given ]
[name:delete_user, type:UserSteps, file:EFForg_action-center-platform/features/step_definitions/user_steps.rb, step:Given ]
[name:destroy, type:User, file:EFForg_action-center-platform/app/models/user.rb, step:Given ]
[name:each, type:Object, file:null, step:Then ]
[name:fill_in, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:find_by_email, type:User, file:EFForg_action-center-platform/app/models/user.rb, step:Given ]
[name:find_field, type:Object, file:null, step:Then ]
[name:gsub, type:Object, file:null, step:When ]
[name:new, type:Admin/actionPagesController, file:EFForg_action-center-platform/app/controllers/admin/action_pages_controller.rb, step:null]
[name:select, type:Object, file:null, step:When ]
[name:sign_in, type:UserSteps, file:EFForg_action-center-platform/features/step_definitions/user_steps.rb, step:Given ]
[name:text, type:Object, file:null, step:Then ]

Referenced pages: 1
EFForg_action-center-platform/app/views/admin/action_pages/new.html.erb

