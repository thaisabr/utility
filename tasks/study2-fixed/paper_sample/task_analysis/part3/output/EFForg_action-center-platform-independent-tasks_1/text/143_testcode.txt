When(/^I browse to the action page$/) do
  visit "/action/#{@action_page.title.downcase.gsub(" ", "-")}"
end
When(/^I fill in my phone number, address, and zip code and click call$/) do
  find("input[name=inputPhone]").set("415-555-0100")
  find("input[name=inputStreetAddress]").set("100 Abc St.")
  find("input[name=inputZip]").set("94109")
  find("button.call-tool-submit").click
end
Given(/^a call campaign exists$/) do
  create_a_call_campaign
end
Given(/^an email campaign exists$/) do
  create_an_email_campaign
end
When(/^I enter my email address and opt for mailings$/) do
  find("input[name=subscription\\[email\\]]").set("abcdef@example.com")
  find("label[for=do-subscribe]").click
end
Then(/^I should see an option to sign up for mailings$/) do
  expect(page).to have_css(".email-signup input[type=radio][name=subscribe][value='0']:checked", visible: false)
  expect(page).to have_css(".email-signup input[type=radio][name=subscribe][value='1']", visible: false)
end
Then(/^I should have signed up for mailings$/) do
  expect(page).to have_css("form[data-signed-up-for-mailings=true]", visible: false)
end
Then(/^I should not have signed up for mailings$/) do
  expect(page).not_to have_css("form[data-signed-up-for-mailings=true]", visible: false)
end
When(/^I click open using gmail$/) do
  click_on "Gmail"
end
Then(/^I should see a sign up form for mailings$/) do
  expect(page).to have_css("form.newsletter-subscription")
end
When(/^I enter my email address for mailings and click Sign Up$/) do
  find("input[name=subscription\\[email\\]]").set("abcdef@example.com")
  click_on "Sign up"
end
def create_a_call_campaign
  @call_campaign = FactoryGirl.create(:call_campaign, call_campaign_id: senate_call_campaign_id)
  @action_page = @call_campaign.action_page
end
def create_an_email_campaign
  @email_campaign = FactoryGirl.create(:email_campaign_with_custom_target)
  @action_page = @email_campaign.action_page
end
def senate_call_campaign_id; 1; end
