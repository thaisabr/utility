Given (/^I am on "(.*)"$/) do |path|
  visit path
end
When /^I press "([^\"]*)"$/ do |button|
  click_button(button)
end
When /^I fill in "([^\"]*)" with "([^\"]*)"$/ do |field, value|
  fill_in(field.gsub(' ', '_'), :with => value)
end
Then(/^the user "(.*?)" should( not)? be a collaborator$/) do |email, should_not|
  user = User.find_by(email: email)
  expect(user).to be_present
  if should_not
    expect(user).not_to be_collaborator
  else
    expect(user).to be_collaborator
  end
end
Given(/^a user with the email "(.*?)"$/) do |email|
  FactoryGirl.create(:user, email: email)
end
Given(/^I exist as an activist$/) do
  create_activist_user
end
Given(/^I am logged in$/) do
  sign_in
end
def create_activist_user
  create_visitor
  delete_user
  @user = FactoryGirl.create(:activist_user, email: @visitor[:email], password: @visitor[:password])
end
def sign_in
  visit '/login'
  fill_in "Email", :with => @visitor[:email]
  fill_in "Password", :with => @visitor[:password]

  click_button "Sign in"
end
def create_visitor
  @visitor ||= { name: "Test User",
    email: "me@example.com",
    password: "strong passwords defeat lobsters covering wealth",
    password_confirmation: "strong passwords defeat lobsters covering wealth" }
end
def delete_user
  @user ||= User.find_by_email(@visitor[:email])
  @user.destroy unless @user.nil?
end
