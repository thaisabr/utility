When /^(?:|I )fill in "([^"]*)" with ['"]([^'"]*)['"]$/ do |field, value|
  fill_in(field, :with => value)
end
When /^(?:|I )fill in "([^"]*)" with quoted ['"]([^'"]*)['"]$/ do |field, value|
  fill_in(field, :with => '"' + value + '"')
end
When /^(?:|I )press '([^"]*)'$/ do |button|

  if button == 'search'
    page.find(:css, 'button#search-btn').click
  else
    click_button button
  end
end
When /^(?:|I )literally go to (.+)$/ do |page_name|
  visit page_name
end
Then(/^I sleep (\d+) seconds$/) do |wait_seconds|
  sleep wait_seconds.to_i 
end
Given /^PENDING/ do
  pending
end
Then /^I should see a stylesheet/ do
  page.should have_selector("link[rel=stylesheet]", :visible => false)
end
Then /^the page title should be "(.*?)"$/ do |title|
  # Capybara 2 ignores invisible text
  # https://github.com/jnicklas/capybara/issues/863
  #first('title').native.text == title
  #first('title').text == title
  first('title') == title
end
Then /I should select radio "(.*)"$/i do |target|
  case target 
    when 'OR'
      page.all(:xpath, "//input[@value='OR']").first.click
    when 'AND'
      page.all(:xpath, "//input[@value='AND']").first.click
    when 'NOT'
      page.all(:xpath, "//input[@value='NOT']").first.click
  end
end
Given /^I select ["'](.*?)["'] from the ["'](.*?)["'] drop\-down$/ do |option, menu|
  #select(option, :from => menu)
  find('#' + menu).find(:option,"#{option}").select_option
end
Then /^I should get results$/ do
  page.should have_selector(".document")
end
Then /^click on link "(.*?)"$/ do |link|
  click_link link
end
Then /^click on first link "(.*?)"$/ do |link|
  l = page.first('a', :text => link)
  l.click 
end
Then /^it should have link ["'](.*?)["'] with value ["'](.*?)["']$/ do |txt, alink|
  #print page.html
  expect(page).to have_link(txt, :href =>alink) 
  #res.should == true 
end
Then /^I (should|should not) see the label '(.*?)'$/ do |yesno, label|
  if yesno == "should not"
	page.should_not have_content(label)
  else
  	page.should have_content(label)
  end
end
