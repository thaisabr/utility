Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
  def path_to(page_name)
    case page_name

    when /the home\s?page/
      root_path

    
    when /the catalog page/
      search_catalog_path

    when /the folder page/
      folder_index_path

    when /the document page for id (.+)/
      facet_catalog_path($1)

    when /the facet page for "([^\"]*)"/
      facet_catalog_path($1)

    when /the search page/
      search_index_path

    when /the single search results page/
      search_index_path

    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Then /^I should see a facet called '(.*?)'$/ do |facet|
    within('div.facets') do
	page.should have_content(facet)
    end
end
Then /^I should not see a facet called '(.*?)'$/ do |facet|
  within('div.facets') do
    page.should_not have_content(facet)
  end
end
Then /^the '(.*?)' facet (should|should not) be open$/ do |facet, yesno|
  #TODO: Not sure how to test this
  facet.downcase!
  facet = facet_to(facet)
  within('div.facets') do

  	if (facet == 'blacklight-pub_date_facet')
  	  if (yesno == 'should')
	   	page.should have_css("div.#{facet} div.limit_content", :visible => true)
	  else
	 	page.should have_css("div.#{facet} div.limit_content", :visible => false)
  	  end

  	else
  	  if (yesno == 'should')
	   	page.should have_css("div.#{facet} ul", :visible => true)
	  else
	 	page.should have_css("div.#{facet} ul", :visible => false)
	  end
	end
  end
  # within('div.facets') do

  # end
end
  def facet_to(facet)
    case facet

    when /^access$/
      'blacklight-online'
    when /^author.*$/
      'blacklight-author_facet'
    when /^fiction\/non-fiction$/
      'blacklight-subject_content_facet'
    when /^format$/
       'blacklight-format'
    when /.*genre$/
       'blacklight-fast_genre_facet'
    when /^language$/
       'blacklight-language_facet'
    when /^library location$/
      'blacklight-location_facet'
    when /^call number$/
       'blacklight-lc_callnum_facet'
    when /.*region$/
       'blacklight-fast_geo_facet'
    when /.*era$/
       'blacklight-fast_era_facet'
    when /^location$/
       'blacklight-location_facet'
    when /^publication year$/
       'blacklight-pub_date_facet'
    end
  end
Then /^I (should|should not) see the label '(.*?)'$/ do |yesno, label|
  if yesno == "should not"
	page.should_not have_content(label)
  else
  	page.should have_content(label)
  end
end
