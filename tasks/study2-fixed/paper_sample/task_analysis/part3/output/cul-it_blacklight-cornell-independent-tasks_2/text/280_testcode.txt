Given /^I request the item view for (.*?)$/ do |bibid|
  visit "/catalog/#{bibid}"
end
Then /^click on link "(.*?)"$/ do |link|
  click_link link
end
Then /^click on first link "(.*?)"$/ do |link|
  l = page.first('a', :text => link)
  l.click 
end
Then /^I (should|should not) see the label '(.*?)'$/ do |yesno, label|
  if yesno == "should not"
	page.should_not have_content(label)
  else
  	page.should have_content(label)
  end
end
Then(/^in modal ['"](.*?)['"] I should see label ['"](.*?)['"]$/) do |modal,label|
  within(modal) do
    page.should have_content(label) # async
  end

end
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press '([^"]*)'$/ do |button|

  if button == 'search'
    page.find(:css, 'button#search-btn').click
  else
    click_button button
  end
end
When /^(?:|I )literally go to (.+)$/ do |page_name|
  visit page_name
end
Then(/^I sleep (\d+) seconds$/) do |wait_seconds|
  sleep wait_seconds.to_i 
end
  def path_to(page_name)
    case page_name

    when /the home\s?page/
      root_path

    
    when /the catalog page/
      search_catalog_path

    when /the folder page/
      folder_index_path

    when /the document page for id (.+)/
      facet_catalog_path($1)

    when /the facet page for "([^\"]*)"/
      facet_catalog_path($1)

    when /the search page/
      search_index_path

    when /the single search results page/
      search_index_path

    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Then /^I should see the xml path '(.*?)','(.*?)','(.*?)','(.*?)'$/i do |ns,xp,nsdef,str|
  xml_doc  = Nokogiri::XML(page.body)
  contents = xml_doc.xpath(xp,ns => nsdef).first.text
  print "str = #{str.inspect}\n"
  if !contents.nil? && contents.include?(str)
    page.should have_content('e')
  else
    page.should have_content(xp)
  end
end
Then /^I should see the xml text '(.*?)'$/i do |text|
  if  page.body.include?(text)
    page.should have_content('e')
  else
    page.should have_content(text)
  end
end
Then /^I should see the text '(.*?)'$/i do |text|
  page.should have_content(text)
end
When /^I fill in the search box with '(.*?)'$/ do |query|
  query.gsub!(/\\"/, '"')
  fill_in('q', :with => query)
end
Then /^the '(.*?)' select list should have an option for '(.*?)'$/ do |list, option|
  page.all('#' + list + '-dropdown ul.css-dropdown li.btn li', :text => option)
end
Then /^I should see each item format$/ do
  within('#documents') do
  	page.should have_css('.blacklight-title_display')
  	page.should have_css('.blacklight-author_display')
  end
end
Then /^results should have a select checkbox$/ do
  within('#documents') do
  	page.should have_selector('.bookmark_add')
  end
end
Then /^results should have a title field$/ do
  within('#documents') do
    page.should have_css('.blacklight-title_display')
  end
end
Given /^I select ["'](.*?)["'] from the ["'](.*?)["'] drop\-down$/ do |option, menu|
  #select(option, :from => menu)
  find('#' + menu).find(:option,"#{option}").select_option
end
Then /^I should get results$/ do
  page.should have_selector(".document")
end
