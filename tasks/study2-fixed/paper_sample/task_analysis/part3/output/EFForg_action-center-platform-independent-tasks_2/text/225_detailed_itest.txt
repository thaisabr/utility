Classes: 3
[name:ActionPage, file:EFForg_action-center-platform/app/models/action_page.rb, step:Given ]
[name:FactoryGirl, file:null, step:Given ]
[name:User, file:EFForg_action-center-platform/app/models/user.rb, step:Given ]

Methods: 35
[name:change, type:Object, file:null, step:Given ]
[name:click, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:create_activist_user, type:UserSteps, file:EFForg_action-center-platform/features/step_definitions/user_steps.rb, step:Given ]
[name:create_community_member_user, type:UserSteps, file:EFForg_action-center-platform/features/step_definitions/user_steps.rb, step:Given ]
[name:create_visitor, type:UserSteps, file:EFForg_action-center-platform/features/step_definitions/user_steps.rb, step:Given ]
[name:delete_user, type:UserSteps, file:EFForg_action-center-platform/features/step_definitions/user_steps.rb, step:Given ]
[name:destroy, type:User, file:EFForg_action-center-platform/app/models/user.rb, step:Given ]
[name:edit, type:UsersController, file:EFForg_action-center-platform/app/controllers/users_controller.rb, step:Given ]
[name:evaluate_script, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find, type:Object, file:null, step:When ]
[name:find_by_email, type:User, file:EFForg_action-center-platform/app/models/user.rb, step:Given ]
[name:first, type:Object, file:null, step:When ]
[name:get email alerts, type:Object, file:null, step:Given ]
[name:have_content, type:Object, file:null, step:Then ]
[name:html, type:Object, file:null, step:Then ]
[name:include, type:Object, file:null, step:Then ]
[name:index, type:WelcomeController, file:EFForg_action-center-platform/app/controllers/welcome_controller.rb, step:Given ]
[name:index, type:Admin/actionPagesController, file:EFForg_action-center-platform/app/controllers/admin/action_pages_controller.rb, step:Given ]
[name:index, type:ActionPagesController, file:EFForg_action-center-platform/app/controllers/admin/action_pages_controller.rb, step:Given ]
[name:not_to, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:When ]
[name:set, type:Object, file:null, step:When ]
[name:setup_call_action, type:UserSteps, file:EFForg_action-center-platform/features/step_definitions/user_steps.rb, step:When ]
[name:show, type:UsersController, file:EFForg_action-center-platform/app/controllers/users_controller.rb, step:Given ]
[name:sign_in, type:UserSteps, file:EFForg_action-center-platform/features/step_definitions/user_steps.rb, step:When ]
[name:sign_in, type:UserSteps, file:EFForg_action-center-platform/features/step_definitions/user_steps.rb, step:Given ]
[name:to, type:Object, file:null, step:Then ]
[name:update, type:ActionPagesController, file:EFForg_action-center-platform/app/controllers/admin/action_pages_controller.rb, step:Given ]
[name:update, type:TopicCategoriesController, file:EFForg_action-center-platform/app/controllers/admin/topic_categories_controller.rb, step:Given ]
[name:update, type:TopicSetsController, file:EFForg_action-center-platform/app/controllers/admin/topic_sets_controller.rb, step:Given ]
[name:update, type:PartnersController, file:EFForg_action-center-platform/app/controllers/partners_controller.rb, step:Given ]
[name:update, type:UsersController, file:EFForg_action-center-platform/app/controllers/users_controller.rb, step:Given ]

Referenced pages: 12
EFForg_action-center-platform/app/views/admin/_header.html.erb
EFForg_action-center-platform/app/views/admin/_nav_tabs.html.erb
EFForg_action-center-platform/app/views/admin/action_pages/_date_range_form.html.erb
EFForg_action-center-platform/app/views/admin/action_pages/_date_vars.html.erb
EFForg_action-center-platform/app/views/admin/action_pages/index.html.erb
EFForg_action-center-platform/app/views/devise/registrations/edit.html.erb
EFForg_action-center-platform/app/views/subscriptions/_form.html.erb
EFForg_action-center-platform/app/views/tools/_loading.html.erb
EFForg_action-center-platform/app/views/users/edit.html.erb
EFForg_action-center-platform/app/views/users/show.html.erb
EFForg_action-center-platform/app/views/welcome/_meta_tags.html.erb
EFForg_action-center-platform/app/views/welcome/index.html.erb

