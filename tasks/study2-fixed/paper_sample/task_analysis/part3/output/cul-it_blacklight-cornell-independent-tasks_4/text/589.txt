Feature: Select and export items from the result set
In order to save results for later
As a researcher
I want to select items from a results list and export them in different ways.
@all_select_and_export @citations @two264s @DISCOVERYACCESS-1677 @javascript
Scenario: User needs to cite a record
Given I request the item view for 8392067
And click on link "Cite"
And I sleep 2 seconds
Then I should see the label 'MLA Shannon, Timothy J. The Seven Years' War In North America : a Brief History with Documents. Boston: Bedford/St. Martin's, 2014.'
@all_select_and_export
@citations
@all_select_and_export @citations @DISCOVERYACCESS-1677 @javascript
Scenario: User needs to cite a record
Given I request the item view for 8125253
And click on link "Cite"
And I sleep 2 seconds
Then I should see the label 'MLA Wake, William. Three Tracts Against Popery. Written In the Year Mdclxxxvi. By William Wake, M.a. Student of Christ Church, Oxon; Chaplain to the Right Honourable the Lord Preston, and Preacher At S. Ann's Church, Westminster. London: printed for Richard Chiswell, at the Rose and Crown in S. Paul's Church-Yard, 1687.'
@all_select_and_export
@citations
@all_select_and_export @citations @DISCOVERYACCESS-1677 @javascript
Scenario: User needs to cite a record
Given I request the item view for 8867518
And click on link "Cite"
And I sleep 2 seconds
Then I should see the label 'MLA Fitch, G. Michael. The Impact of Hand-held and Hands-free Cell Phone Use On Driving Performance and Safety-critical Event Risk : Final Report. [Washington, DC]: U.S. Department of Transportation, National Highway Traffic Safety Administration, 2013.'
@all_select_and_export
@citations
@all_select_and_export @citations @DISCOVERYACCESS-1677 @javascript
Scenario: User needs to cite a record
Given I request the item view for 8069112
And click on link "Cite"
And I sleep 2 seconds
Then I should see the label 'APA Cohen, A. I. (2013). Social media : legal risk and corporate policy. New York: Wolters Kluwer Law & Business.'
@all_select_and_export
@citations
@all_select_and_export @citations @DISCOVERYACCESS-1677 @javascript
Scenario: User needs to cite a record
Given PENDING
Given I request the item view for 8696757
And click on link "Cite"
And I sleep 2 seconds
Then I should see the label 'Chicago Funk, Tom. Advanced Social Media Marketing: How to Lead, Launch, and Manage a Successful Social Media Program. Berkeley, CA: Apress, 2013.'
@all_select_and_export
@citations
@all_select_and_export @citations @DISCOVERYACCESS-1677 @javascript
Scenario: User needs to cite a record
Given I request the item view for 5558811
And click on link "Cite"
And I sleep 2 seconds
Then I should see the label 'MLA Company for Propagation of the Gospel in New England and the Parts Adjacent in America.. Mamusse Wunneetupanatamwe Up-biblum God Naneeswe Nukkone Testament Kah Wonk Wusku Testament. Cambridge [Mass.].: Printeuoop nashpe Samuel Green., 1685'
@all_select_and_export
@DISCOVERYACCESS-1633
