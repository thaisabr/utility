Feature: Item view
In order to get information about a specific item
As a user
I want to see details from the item's catalog record, holdings, and availability.
@all_item_view
@allow-rescue
@all_item_view @availability
Scenario: View an items holdings
Given I request the item view for 4759
Then I should see the label 'Request'
@all_item_view
@aeon
@all_item_view @aeon @rmcnoitems
Scenario: View an items holdings
Given I request the item view for 8753977
Then I should see the label 'Request'
@all_item_view
@all_item_view @aeon
Scenario: View an items holdings, and request from aeon
Given I request the item view for 2083253
And click on link "Request"
Then I should see the label 'Upton, G. B. (George Burr), 1882-1942'
@all_item_view
@all_item_view @aeon
Scenario: View an items holdings, and request from aeon
Given I request the item view for 2083253
And click on link "Request"
Then I should see the label '16-5-268 This rare item may be delivered only to the RMC Reading Room.'
@all_item_view
@all_item_view @aeon
Scenario: View an items holdings, and request from aeon
Given I request the item view for 2083253
Then it should have link "Request" with value "/aeon/2083253"
@all_item_view
@aeon @all_item_view
Scenario: View an items holdings, and request from aeon
Given I request the item view for 2083253
And click on link "Request"
Then I should see the label 'Upton, G. B. (George Burr), 1882-1942'
@aeon
@aeon @all_item_view
Scenario: View an items holdings, and request from aeon
Given I request the item view for 2083253
And click on link "Request"
Then I should see the label '16-5-268 This rare item may be delivered only to the RMC Reading Room.'
@aeon
@aeon @all_item_view
Scenario: View an items holdings, and request from aeon
Given I request the item view for 2083253
Then it should have link "Request" with value "/aeon/2083253"
@DISCOVERYACCESS-136
@availability @all_item_view
Scenario: As a user I can see the availability for an item
Given I request the item view for 30000
Then I should see the label 'Library Annex'
@all_item_view
@all_item_view @availability @due
Scenario: As a user I can see the availability for an item
Given I request the item view for 2269649
Then I should see the label 'Checked out, due'
@all_item_view
@availability
@javascript
@bibid1902405
@all_item_view @availability @javascript @bibid1902405 @DISCOVERYACCESS-1659
Scenario: As a user I can see the availability for an item
Given I request the item view for 1902405
Then I should see the label '1950 c. 1 Checked out, due 2016-01-09'
Then I should see the label 'Request'
@all_item_view
@availability
@all_item_view @availability @DISCOVERYACCESS-988
Scenario: As a user I can see the availability for an item at a temporary location that overrides the permanent location.
Given I request the item view for 44112
Then I should not see the label 'Temporarily shelved'
@all_item_view
@availability
@DISCOVERYACCESS-988
@all_item_view @availability @DISCOVERYACCESS-988 @nomusic
Scenario: As a user I can see the availability for an item at a temporary location that overrides the permanent location.
Given I request the item view for 2269649
Then I should not see the label 'Music Library Reserve'
@all_item_view
@all_item_view @availability @DISCOVERYACCESS-1386
Scenario: As a user I can see the information about an ONLINE item, but not the call number
Given I request the item view for 5380314
Then I should see the label 'Available online'
@availability
@all_item_view
@all_item_view @availability @intransit @DISCOVERYACCESS-1483
Scenario: As a user I can see the availability for an In transit item
Given I request the item view for 424344
Then I should see the labels 'In transit'
@all_item_view
@all_item_view @availability @intransit
Scenario: As a user I can see the availability for an In transit item
Given I request the item view for 52325
Then I should see the labels 'In transit'
@all_item_view
@all_item_view @availability @intransit
Scenario: As a user I can see the availability for an In transit item, but no bogus LOC
Given I request the item view for 8272732
Then I should not see the label '%LOC'
@all_item_view
@all_item_view @availability @intransit
Scenario: As a user I can see the availability for an In transit item, but no bogus LOC
Given I request the item view for 106223
Then I should not see the label '%LOC'
@all_item_view
Feature: Results list
In order to find items that I search for
As a user
I want to view a list of search results with various options.
@all_results_list @search_with_view_all_websites_multi_word
Scenario: Search with view all websites multi word link
Given I literally go to search
When I fill in "q" with 'levitated nanosphere'
And I press 'Search'
Then I should get bento results
When I follow "link_top_web"
And I should see the text "levitated nanosphere"
@all_results_list @search_with_view_all_websites_multi_word_with_percent
Scenario: Search with view all websites multi word link
Given I literally go to search
When I fill in "q" with '100% beef'
And I press 'Search'
Then I should get bento results
When I follow "link_top_web"
And I should see the text "National Beef Packing Co"
@all_results_list @search_with_view_all_books
Scenario: Search with view all books link
Given I literally go to search
When I fill in "q" with 'nature'
And I press 'Search'
When I follow "link_top_book"
And I should see the text "Nature / 2012"
@all_results_list @search_with_view_all_webs_match_box
Scenario: Search with view all webs link
Given I literally go to search
Given PENDING
When I fill in "q" with 'gettysburg address'
And I press 'Search'
Then I should get bento results
Then box "link_top_web" should match "4" th "Library Websites" in "web-count"
@all_results_list @search_with_view_all_music_match_box
Scenario: Search with view all music link
Given I literally go to search
When I fill in "q" with 'nature morte'
And I press 'Search'
Then box "link_top_musical_recording" should match "0" th "from Catalog" in "page_entries"
@all_results_list @search_with_view_all_manuscript_archive
Scenario: Search with view all music link
Given I literally go to search
When I fill in "q" with 'george burr upton'
And I press 'Search'
Then I should get bento results
Then box "link_top_manuscript_archive" should match "0" th "from Catalog" in "page_entries"
@all_results_list @search_with_view_all_webs_match_box_with_percent
Scenario: Search with view all webs link
Given I literally go to search
When I fill in "q" with '100% beef packing'
And I press 'Search'
Then I should get bento results
Then box "link_top_web" should match "2" nd "Library Websites" in "web-count"
@all_results_list @search_with_view_all_journals_match_box_with_percent
Scenario: Search with view all journals link
Given I literally go to search
When I fill in "q" with 'beef'
And I press 'Search'
Then I should get bento results
Given PENDING
Then box "link_top_journal_periodical" should match "0" th "from Catalog" in "page_entries"
@all_results_list @search_with_view_all_books_match_box_with_percent
Scenario: Search with view all books link
Given I literally go to search
When I fill in "q" with 'beef'
And I press 'Search'
Then I should get bento results
Given PENDING
Then box "link_top_book" should match "2" nd "from Catalog" in "page_entries"
@all_results_list @search_with_view_all_computer_file_match_box_with_ampersand
Scenario: Search with view all journals link
Given I literally go to search
When I fill in "q" with '100 Vietnamese painters & sculptors'
And I press 'Search'
Then I should get bento results
Then box "link_top_computer_file" should match "0" th "from Catalog" in "page_entries"
@all_results_list @search_with_view_all_article_match_box
Scenario: Search with view all article link should match bento box total
Given I literally go to search
When I fill in "q" with 'stress testing cardio horse insights'
And I press 'Search'
Given PENDING
Then I should get bento results
Then box "link_top_summon_bento" should match "0" th "from Articles & Full Text" in "summary"
@all_results_list @search_with_view_all_article_match_box
Scenario: Search with view all article link should match bento box total
Given I literally go to search
When I fill in "q" with 'photoplethysmography methodological studies arterial stiffness'
Given PENDING
And I press 'Search'
Then I should get bento results
Then box "link_top_summon_bento" should match "0" th "from Articles & Full Text" in "summary"
