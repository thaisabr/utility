Feature: Facets
In order to refine my search results
As a user
I want to use facets for different search parameters.
@homepage @javascript
Scenario: Viewing the home page
Given I am on the home page
Then I should see a facet called 'Access'
And I should see a facet called 'Format'
And I should see a facet called 'Author, etc.'
And I should see a facet called 'Publication Year'
And I should see a facet called 'Language'
And I should see a facet called 'Subject: Region'
And I should see a facet called 'Subject: Era'
And I should see a facet called 'Fiction/Non-Fiction'
And I should see a facet called 'Library Location'
And I should not see a facet called 'Call Number'
And the 'Access' facet should not be open
And the 'Format' facet should be open
And the 'Author, etc.' facet should not be open
And the 'Publication Year' facet should not be open
And the 'Language' facet should not be open
And the 'Subject/Genre' facet should not be open
And the 'Subject: Region' facet should not be open
And the 'Subject: Era' facet should not be open
And the 'Fiction/Non-Fiction' facet should not be open
And the 'Library Location' facet should not be open
@homepage
@nocallnumber
@homepage @nocallnumber @javascript
Scenario: Viewing the home page
Given I am on the home page
And I should not see a facet called 'Call Number'
Feature: Item view
In order to get information about a specific item
As a user
I want to see details from the item's catalog record, holdings, and availability.
@all_item_view
@allow-rescue
@all_item_view @availability
Scenario: View an items holdings
Given I request the item view for 4759
Then I should see the label 'Request'
@all_item_view
@aeon
@all_item_view @aeon @rmcnoitems
Scenario: View an items holdings
Given I request the item view for 8753977
Then I should see the label 'Request'
@all_item_view
@all_item_view @aeon
Scenario: View an items holdings, and request from aeon
Given I request the item view for 2083253
And click on link "Request"
Then I should see the label 'Upton, G. B. (George Burr), 1882-1942'
@all_item_view
@all_item_view @aeon
Scenario: View an items holdings, and request from aeon
Given I request the item view for 2083253
And click on link "Request"
Then I should see the label '16-5-268 This rare item may be delivered only to the RMC Reading Room.'
@all_item_view
@all_item_view @aeon
Scenario: View an items holdings, and request from aeon
Given I request the item view for 2083253
Then it should have link "Request" with value "/aeon/2083253"
@all_item_view
@aeon @all_item_view
Scenario: View an items holdings, and request from aeon
Given I request the item view for 2083253
And click on link "Request"
Then I should see the label 'Upton, G. B. (George Burr), 1882-1942'
@aeon
@aeon @all_item_view
Scenario: View an items holdings, and request from aeon
Given I request the item view for 2083253
And click on link "Request"
Then I should see the label '16-5-268 This rare item may be delivered only to the RMC Reading Room.'
@aeon
@aeon @all_item_view
Scenario: View an items holdings, and request from aeon
Given I request the item view for 2083253
Then it should have link "Request" with value "/aeon/2083253"
@DISCOVERYACCESS-136
@availability @all_item_view
Scenario: As a user I can see the availability for an item
Given I request the item view for 30000
Then I should see the label 'Library Annex'
@all_item_view
@all_item_view @availability @due
Scenario: As a user I can see the availability for an item
Given I request the item view for 2269649
Then I should see the label 'Checked out, due'
@all_item_view
@availability
@javascript
@bibid1902405
@all_item_view @availability @javascript @bibid1902405 @DISCOVERYACCESS-1659
Scenario: As a user I can see the availability for an item
Given I request the item view for 1902405
Then I should see the label '1950 c. 1 Checked out, due 2016-01-09'
Then I should see the label 'Request'
@all_item_view
@availability
@all_item_view @availability @DISCOVERYACCESS-988
Scenario: As a user I can see the availability for an item at a temporary location that overrides the permanent location.
Given I request the item view for 44112
Then I should not see the label 'Temporarily shelved'
@all_item_view
@availability
@DISCOVERYACCESS-988
@all_item_view @availability @DISCOVERYACCESS-988 @nomusic
Scenario: As a user I can see the availability for an item at a temporary location that overrides the permanent location.
Given I request the item view for 2269649
Then I should not see the label 'Music Library Reserve'
@all_item_view
@all_item_view @availability @intransit @DISCOVERYACCESS-1483
Scenario: As a user I can see the availability for an In transit item
Given I request the item view for 424344
Then I should see the labels 'In transit'
@all_item_view
@all_item_view @availability @intransit
Scenario: As a user I can see the availability for an In transit item
Given I request the item view for 52325
Then I should see the labels 'In transit'
@all_item_view
@all_item_view @availability @intransit
Scenario: As a user I can see the availability for an In transit item, but no bogus LOC
Given I request the item view for 8272732
Then I should not see the label '%LOC'
@all_item_view
@all_item_view @availability @intransit
Scenario: As a user I can see the availability for an In transit item, but no bogus LOC
Given I request the item view for 106223
Then I should not see the label '%LOC'
@all_item_view
