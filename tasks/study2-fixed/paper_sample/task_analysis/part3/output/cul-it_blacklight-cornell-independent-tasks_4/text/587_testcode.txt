Given /^I request the item view for (.*?)$/ do |bibid|
  visit "/catalog/#{bibid}"
end
Then /^click on link "(.*?)"$/ do |link|
  click_link link
end
Then /^click on first link "(.*?)"$/ do |link|
  l = page.first('a', :text => link)
  l.click 
end
Then /^it should contain "(.*?)" with value "(.*?)"$/ do |field, author|
  page.should have_selector(field_to(field), :text => author,:exact =>false )
end
Then /^I (should|should not) see the label '(.*?)'$/ do |yesno, label|
  if yesno == "should not"
	page.should_not have_content(label)
  else
  	page.should have_content(label)
  end
end
Then /^I (should|should not) see the labels '(.*?)'$/ do |yesno, llist|
  labels = llist.split(',') 
  if yesno == "should not"
        labels.each do |label|
	  page.should_not have_content(label)
        end
  else
        labels.each do |label|
	  page.should have_content(label)
        end
  end
end
  def field_to(field_name)
    case field_name

    when /^author$/
      '.blacklight-author_display'
    when /^author_addl_display$/
      '.blacklight-author_addl_display'
    when /^edition$/
      '.blacklight-edition_display'
    when /^notes$/
      '.blacklight-notes'
    when /^pub_info$/
      '.blacklight-pub_info_display'
    when /^subject$/
      '.blacklight-subject_display'
    when /^title$/
      '.blacklight-title_display'
    else
      "#{field_name} did not match a field name in field_to"
    end
  end
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
Then(/^I sleep (\d+) seconds$/) do |wait_seconds|
  sleep wait_seconds.to_i 
end
  def path_to(page_name)
    case page_name

    when /the home\s?page/
      root_path


    when /the catalog page/
      search_catalog_path

    when /the folder page/
      folder_index_path

    when /the document page for id (.+)/
      facet_catalog_path($1)

    when /the facet page for "([^\"]*)"/
      facet_catalog_path($1)

    when /the search page/
      search_index_path

    when /the single search results page/
      search_index_path

    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Given /^PENDING/ do
  pending
end
Then /^I should see the text '(.*?)'$/i do |text|
  page.should have_content(text)
end
