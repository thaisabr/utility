Feature: Search
In order to find documents in an advanced search way
As a user
I want to enter terms, select terms for fields fields, and select number of results per page
@all_search
@adv_search
@searchpage
@adv_search @all_search @searchpage @javascript
Scenario: Search Page
When I literally go to advanced
And the page title should be "Advanced Search - Cornell University Library Catalog"
And I should see a stylesheet
And I fill in "q_row1" with 'combinatorial algorithms'
And I select 'Title' from the 'search_field_advanced' drop-down
And I fill in "q_row2" with 'algorithmics'
And I select 'Publisher' from the 'search_field_advanced2' drop-down
And I press 'advanced_search'
Then I should get results
And I should see the label '1 result'
And I should see the label 'AND Publisher: algorithmics'
@adv_search
@all_search
@searchpage
@adv_search @all_search @searchpage @javascript
Scenario: Search Page
When I literally go to advanced
And the page title should be "Advanced Search - Cornell University Library Catalog"
And I should see a stylesheet
And I fill in "q_row1" with 'combinatorial algorithms'
And I select 'Title' from the 'search_field_advanced' drop-down
Then I should select radio "OR"
And I fill in "q_row2" with 'algorithmics'
And I select 'Publisher' from the 'search_field_advanced2' drop-down
And I press 'advanced_search'
Then I should get results
And I should see the label '1 - 20 of'
And I should see the label 'OR Publisher: algorithmics'
@adv_search
@all_search
@callnumber
@adv_search @all_search @notes @javascript
Scenario: Perform an advanced search by notes
When I literally go to advanced
And I sleep 4 seconds
And I fill in "q_row1" with 'English, German, Italian, Latin, or Portugese'
And I select 'phrase' from the 'op_row' drop-down
And I select 'Notes' from the 'search_field_advanced' drop-down
And I press 'advanced_search'
Then I should get results
And I should see the label '1 result'
@adv_search
@all_search
@issnisbn
@adv_search @all_search @adv_notes @javascript
Scenario: Perform an advanced search by notes
When I literally go to advanced
And I sleep 4 seconds
And I fill in "q_row1" with 'English, German, Italian, Latin, or Portugese'
And I select 'all' from the 'op_row' drop-down
And I select 'Notes' from the 'search_field_advanced' drop-down
And I fill in "q_row2" with 'Bibliotheca Instituti Historici'
And I select 'phrase' from the 'op_row2' drop-down
And I press 'advanced_search'
Then I should get results
And I should see the label '1 - 8 of 8'
@adv_search
@all_search
@adv_donor
@adv_search @all_search @adv_donor @javascript
Scenario: Perform an advanced search by donor
When I literally go to advanced
And I fill in "q_row1" with 'Jan Olsen'
And I select 'all' from the 'op_row' drop-down
And I select 'Donor Name' from the 'search_field_advanced' drop-down
And I press 'advanced_search'
Then I should get results
And I should see the label '1 - 19 of 19'
And I should not see the label 'Modify advanced'
@adv_search
@all_search
@adv_donor
@adv_search @all_search @adv_donor @javascript
Scenario: Perform an advanced search by donor
When I literally go to advanced
And I fill in "q_row1" with 'Jan'
And I select 'all' from the 'op_row' drop-down
And I select 'Donor Name' from the 'search_field_advanced' drop-down
And I fill in "q_row2" with 'Olsen'
And I select 'all' from the 'op_row2' drop-down
And I select 'Donor Name' from the 'search_field_advanced2' drop-down
And I press 'advanced_search'
Then I should get results
And I should see the label '1 - 19 of 19'
And I should see the label 'Modify advanced'
@adv_search
@all_search
@adv_subject
@adv_search @all_search @adv_subject @javascript
Scenario: Perform an advanced search by subject
When I literally go to advanced
And I fill in "q_row1" with 'Molecular Biology'
And I select 'all' from the 'op_row' drop-down
And I select 'Subject' from the 'search_field_advanced' drop-down
And I fill in "q_row2" with 'Recombinant Dna'
And I select 'all' from the 'op_row2' drop-down
And I select 'Subject' from the 'search_field_advanced2' drop-down
And I press 'advanced_search'
Then I should get results
And I should see the label '1 - 12 of 12'
@adv_search
@all_search
@adv_subject
@adv_search @all_search @adv_subject @javascript
Scenario: Perform an advanced search by subject
When I literally go to advanced
And I fill in "q_row1" with 'Molecular Biology'
And I select 'phrase' from the 'op_row' drop-down
And I select 'Subject' from the 'search_field_advanced' drop-down
And I fill in "q_row2" with 'Recombinant Dna'
And I select 'phrase' from the 'op_row2' drop-down
And I select 'Subject' from the 'search_field_advanced2' drop-down
And I press 'advanced_search'
Then I should get results
And I should see the label '1 - 7 of 7'
@adv_search
@all_search
@adv_subject
@adv_search @all_search @adv_subject @javascript
Scenario: Perform an advanced search by subject
When I literally go to advanced
And I fill in "q_row1" with 'Molecular Biology'
And I select 'phrase' from the 'op_row' drop-down
And I select 'Subject' from the 'search_field_advanced' drop-down
And I fill in "q_row2" with 'Recombinant Dna'
And I select 'phrase' from the 'op_row2' drop-down
And I select 'Subject' from the 'search_field_advanced2' drop-down
And click on link "add-row"
And I sleep 4 seconds
And I fill in "q_row3" with 'yeast'
And I select 'phrase' from the 'op_row3' drop-down
And I select 'Title' from the 'search_field_advanced3' drop-down
And I press 'advanced_search'
Then I should get results
And I should see the label '1 result'
And I should see the label 'Yeast molecular biology--recombinant DNA'
@adv_search
@all_search
@adv_place
@begins_with @adv_search @all_search @adv_place @javascript
Scenario: Perform a 2 row  advanced search by begins with Title
When I literally go to advanced
And I fill in "q_row1" with 'we were feminists'
And I select 'begins' from the 'op_row' drop-down
And I select 'Title' from the 'search_field_advanced' drop-down
And I fill in "q_row2" with 'we were feminists'
And I select 'begins' from the 'op_row2' drop-down
And I select 'Title' from the 'search_field_advanced2' drop-down
And I press 'advanced_search'
Then I should get results
And I should see the label 'Modify advanced'
And I should see the label 'We were Feminists Once'
And I should see the label '1 - '
@begins_with
@adv_search
@all_search
@adv_place
@begins_with @adv_search @all_search @adv_place @javascript
Scenario: Perform a 1 row  advanced search by begins with Title
When I literally go to advanced
And I fill in "q_row1" with 'we were feminists'
And I select 'begins' from the 'op_row' drop-down
And I select 'Title' from the 'search_field_advanced' drop-down
And I press 'advanced_search'
Then I should get results
And I should not see the label 'Modify advanced'
And I should see the label 'We were Feminists'
And I should see the label '1 - '
@wip
@begins_with
@adv_search
@all_search
@adv_place
@javascript
