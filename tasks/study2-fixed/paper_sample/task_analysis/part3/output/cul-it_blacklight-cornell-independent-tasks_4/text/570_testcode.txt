Given /^I request the item view for (.*?)$/ do |bibid|
  visit "/catalog/#{bibid}"
end
Then /^click on link "(.*?)"$/ do |link|
  click_link link
end
Then /^I (should|should not) see the label '(.*?)'$/ do |yesno, label|
  if yesno == "should not"
	page.should_not have_content(label)
  else
  	page.should have_content(label)
  end
end
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )fill in "([^"]*)" with ["']([^"]*)["']$/ do |field, value|
  fill_in(field, :with => value)
end
When /^(?:|I )press '([^"]*)'$/ do |button|

  if button == 'search'
    page.find(:css, 'button#search-btn').click
  else
    click_button button
  end
end
When /^(?:|I )press "([^"]*)"$/ do |button|

  if button == 'search'
    page.find(:css, 'button#search-btn').click
  else
    click_button button
  end
end
Then(/^I sleep (\d+) seconds$/) do |wait_seconds|
  sleep wait_seconds.to_i 
end
  def path_to(page_name)
    case page_name

    when /the home\s?page/
      root_path


    when /the catalog page/
      search_catalog_path

    when /the folder page/
      folder_index_path

    when /the document page for id (.+)/
      facet_catalog_path($1)

    when /the facet page for "([^\"]*)"/
      facet_catalog_path($1)

    when /the search page/
      search_index_path

    when /the single search results page/
      search_index_path

    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Given /^PENDING/ do
  pending
end
Then /I should select checkbox "(.*)"$/i do |target|
  find(:css, "\##{target}").set(true)
end
When /^I fill in the search box with '(.*?)'$/ do |query|
  query.gsub!(/\\"/, '"')
  fill_in('q', :with => query)
end
Then /^it should contain filter "(.*?)" with value "(.*?)"/ do |filter, value|
  page.should have_selector('span.filter-label', :text => filter)
  page.should have_selector('span.filter-value', :text => value)
end
Given /^I select ["'](.*?)["'] from the ["'](.*?)["'] drop\-down$/ do |option, menu|
  #select(option, :from => menu)
  find('#' + menu).find(:option,"#{option}").select_option
end
Then(/^I should see the "([^\"]*)" class$/) do |id|
   page.should have_selector(".#{id}")
end
Then /^I should get results$/ do
  page.should have_selector(".document")
end
Then /^"([^"]*)" receives an email with "([^"]*)" in the content$/ do |email_address, content|
  open_email(email_address)
  expect(current_email.body).to include(content) 
end
Then /^I should see "([^"]*)" in the email body$/ do |content|
  expect(current_email.body).to include(content) 
end
