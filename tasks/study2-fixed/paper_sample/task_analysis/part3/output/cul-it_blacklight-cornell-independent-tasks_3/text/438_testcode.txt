When /^(?:|I )fill in "([^"]*)" with ['"]([^'"]*)['"]$/ do |field, value|
  fill_in(field, :with => value)
end
When /^(?:|I )fill in "([^"]*)" with quoted ['"]([^'"]*)['"]$/ do |field, value|
  fill_in(field, :with => '"' + value + '"')
end
When /^(?:|I )press '([^"]*)'$/ do |button|

  if button == 'search'
    page.find(:css, 'button#search-btn').click
  else
    click_button button
  end
end
When /^(?:|I )literally go to (.+)$/ do |page_name|
  visit page_name
end
Then(/^I sleep (\d+) seconds$/) do |wait_seconds|
  sleep wait_seconds.to_i 
end
Given /^PENDING/ do
  pending
end
Then /^I should see a stylesheet/ do
  page.should have_selector("link[rel=stylesheet]", :visible => false)
end
Then /^the page title should be "(.*?)"$/ do |title|
  # Capybara 2 ignores invisible text
  # https://github.com/jnicklas/capybara/issues/863
  #first('title').native.text == title
  #first('title').text == title
  first('title') == title
end
Then /^I should see the text "(.*?)"$/i do |text|
  page.should have_content(text)
end
Then /I should select radio "(.*)"$/i do |target|
  case target 
    when 'OR'
      page.all(:xpath, "//input[@value='OR']").first.click
    when 'AND'
      page.all(:xpath, "//input[@value='AND']").first.click
    when 'NOT'
      page.all(:xpath, "//input[@value='NOT']").first.click
  end
end
Given /^I select ["'](.*?)["'] from the ["'](.*?)["'] drop\-down$/ do |option, menu|
  #select(option, :from => menu)
  find('#' + menu).find(:option,"#{option}").select_option
end
Then /^I should get results$/ do
  page.should have_selector(".document")
end
Then /^click on link "(.*?)"$/ do |link|
  click_link link
end
Then /^I (should|should not) see the label '(.*?)'$/ do |yesno, label|
  if yesno == "should not"
	page.should_not have_content(label)
  else
  	page.should have_content(label)
  end
end
Then /^I should get bento results$/ do
  page.should  have_selector('.bento1')
  bento1 = all('.bento1')[0].text
  bento1.should match(/[a-zA-Z]+/) 
end
Then(/^box "(.*?)" should match "(.*?)" (nth|th|rd|st|nd) "(.*?)" in "(.*?)"$/) do |label,nth,nstr,type,divtag|
  
  total = all('.view-all')[nth.to_i]
  total.should have_content(type) 
  #print "total on bento box page is #{total.text}\n"
  num = total.text.match(/([0-9,]+)/)[0]
  #print "#{__FILE__} #{__LINE__} num is #{num}\n"
  #print "#{__FILE__} #{__LINE__} type is #{type}\n"
  #print "#{__FILE__} #{__LINE__} label is #{label}\n"
  l2 = find('#' + label)
  href =  l2[:href]
  case
    when type.match("from Articles")
      cmd =  "wget -O -  '#{href}' 2>/dev/null"
      page2 = `#{cmd}`
      pagedom = Nokogiri::HTML(page2)   
      #print "pagedom: #{pagedom}"
      pagedom.css("##{divtag}")[0].should_not be_nil 
      numx = pagedom.css("##{divtag}")[0].text 
      #print "total2 on view all page is #{numx}\n"
      #page.find(".#{divtag}").should_not be_nil
      #numx = page.find(".#{divtag}").text
      numx.should_not be_nil
      numx.match(/returned\s+(\d+)/).should_not be_nil
      numx.match(/returned\s+(\d+)/)[1].should_not be_nil
      num2 = numx.match(/returned\s+(\d+)/)[1]
    when type.match("from Catalog")
      click_link("#{label}")
      #cmd =  "wget -O -  '#{href}' 2>/dev/null"
      #print "****cmd is #{cmd}"
      #page2 = `#{cmd}`
      #page2 = page 
      #print "++++ #{page2}"
      #pagedom = Nokogiri::HTML(page2)   
      #pagedom.css('.'+divtag)[0].should_not be_nil 
      #numx = pagedom.css('.'+divtag)[0].text 
      page.find(".#{divtag}").should_not be_nil 
      numx = page.find(".#{divtag}").text
      #print "total2 on view (line #{__LINE__}  all page is '#{numx}'\n"
      numx.should_not be_nil
      if numx.match(/of\s+(\d+)/)
        numx.match(/of\s+(\d+)/).should_not be_nil
        numx.match(/of\s+(\d+)/)[1].should_not be_nil
        #print "Inspect:" +  numx.match(/of\s+([0-9,]) /).inspect
        num2 = numx.match(/of\s+([0-9,]+)/)[1]
      else
        numx.match(/(\d+)\s+result/).should_not be_nil
        numx.match(/(\d+)\s+result/)[1].should_not be_nil
        #print "Inspect:" +  numx.match(/(\d+)\s+result/).inspect
        num2 = numx.match(/([0-9,]+)\s+result/)[1]
      end
      #print "num2 on view (line #{__LINE__} all page is #{num2}\n"
    else
      visit(href)
      sleep 3
      #print "HREF is #{href}\n"
      total2 = find('.'+divtag,match: :first)
      total2.should_not be_nil
      #print "total2 on view all page is #{total2.text}\n"
      num2 = total2.text.match(/of ([0-9,]+) /)[1]
    end
    num.should_not be_nil
    num2.should_not be_nil
    num2.gsub!(',','')
    num.gsub!(',','')
    #print "box total of items = #{num}, and page number of items = #{num2}\n"
    diff = (num2.to_i - num.to_i).abs
    diff.should <=(20)
end
When /^I follow "([^\"]*)"$/ do |link|
   click_link(link)
end
