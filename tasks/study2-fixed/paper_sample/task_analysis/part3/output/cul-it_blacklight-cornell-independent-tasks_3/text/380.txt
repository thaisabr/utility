Feature: Results list
In order to find items that I search for
As a user
I want to view a list of search results with various options.
@all_results_list @next
Scenario: Search with results
Given I am on the home page
When I fill in the search box with 'biology'
And I press 'search'
Then I should get results
Then click on first link "Next »"
Then I should get results
@all_results_list
@all_results_list @next_facet
Scenario: Search with results
Given I am on the home page
When I fill in the search box with 'biology'
And I press 'search'
Then I should get results
Then click on first link "Next »"
@all_results_list
@all_results_list @getresults
Scenario: Search with results
Given I am on the home page
When I fill in the search box with 'biology'
And I press 'search'
Then I should get results
@all_results_list
Scenario: Search with results
Given I am on the home page
When I fill in the search box with 'biology'
And I press 'search'
Then I should get results
And I should see each item format
And the 'sort' select list should have an option for 'relevance'
And the 'sort' select list should have an option for 'year ascending'
And the 'sort' select list should have an option for 'year descending'
And the 'sort' select list should have an option for 'title A-Z'
And the 'sort' select list should have an option for 'title Z-A'
And the 'sort' select list should have an option for 'author A-Z'
And the 'sort' select list should have an option for 'author Z-A'
And the 'sort' select list should have an option for 'call number'
And results should have a select checkbox
And results should have a title field
@all_results_list
@all_results_list @next_facet @javascript
Scenario: Search with results,
Given I am on the home page
When I fill in the search box with 'We were feminists'
And I press 'search'
Then I should get results
Then I should see the text 'Click : '
Then click on first link "Click : when we knew we were feminists"
And I sleep 10 seconds
Then I should see the text 'edited by Courtney E. Martin and J. Courtney Sullivan.'
Then click on first link "Next »"
Then I should see the text 'Involvement and Ideologies of Black Mothers'
@all_results_list
@all_results_list @next_facet @javascript
Scenario: Search with results,
Given I am on the home page
When I fill in the search box with 'cigarette prices'
And I select 'Title' from the 'search_field' drop-down
And I press 'search'
And I sleep 10 seconds
Then I should get results
And I sleep 4 seconds
Then I should see the text 'Lighting Up and'
Then click on first link "Lighting Up"
Then click on first link "Next »"
Then I should see the text 'Heterogeneity of the Cigarette Price Effect on Body Mass Index'
Then click on first link "Previous"
Then I should see the text 'Lighting Up and'
@all_results_list
Feature: Search
In order to find documents
As a user
I want to enter terms, select fields, and select number of results per page
@all_search
@all_search @peabody
Scenario: Perform a search by author, as author see results
Given I am on the home page
And I fill in the search box with 'Peabody, William Bourn Oliver, 1799-1847'
And I press 'search'
Then I should get results
And I should see the label 'of 1'
@all_search
@all_search @peabody
Scenario: Perform a search by author, as author see results
Given I am on the home page
And I select 'Author, etc.' from the 'search_field' drop-down
And I fill in the search box with 'Peabody, William Bourn Oliver, 1799-1847'
And I press 'search'
Then I should get results
And I should see the label 'of 1'
@all_search
@search_availability_annotated_hobbit
@availability
@clock
@javascript
@all_search @search_availability_title_professional_manager_multiple @multiple @availability @javascript
Scenario: Perform a title search and see avail icon, avail at  multiple locations
Given I am on the home page
And I select 'Title' from the 'search_field' drop-down
And I fill in the search box with '"The Professional Manager"'
And I press 'search'
Then I should get results
And I sleep 15 seconds
And I should see the "fa-check" class
And I should see the label 'Multiple locations'
@all_search
@search_availability_title_mission_etrangeres_missing
@multiple
@availability
@all_search @search_availability_title_mission_etrangeres_missing @multiple @availability @javascript
Scenario: Perform a title search and see avail icon, avail at  multiple locations
Given I am on the home page
And I select 'Title' from the 'search_field' drop-down
And I fill in the search box with 'Atlas des missions de la '
And I press 'search'
Then I should get results
And I should see the "fa-check" class
And I should see the label 'Olin Library Maps'
@all_search
@search_availability_title_tolkien_critical
@multiple
@availability
@all_search @search_availability_title_tolkien_critical @multiple @availability @javascript
Scenario: Perform a title search and see avail icon, avail at  multiple locations
Given I am on the home page
And I select 'Title' from the 'search_field' drop-down
And I fill in the search box with 'Tolkien, new critical perspectives'
And I press 'search'
And I sleep 1 seconds
Then I should get results
And I should see the "fa-clock-o" class
And I should see the label 'Olin Library'
Feature: Databases List
In order to get information about a featured databases
As a user
I want to see the list of the digital collections
@databases
Scenario: View a list of databases
Given I literally go to databases
Then I should see the label 'Search for top databases'
@mla
@mla @databases
Scenario: Make sure list contains known collection
Given I literally go to databases
And I fill in the search box with 'MLA'
And I press 'search'
Then I should see the label 'MLA'
@DISCOVERYACCESS-2325
@DISCOVERYACCESS-2325 @databases
Scenario: Make sure list contains known collection
Given I literally go to databases/subject/Images
Then I should see the label 'ARTstor'
@DISCOVERYACCESS-2325
@DISCOVERYACCESS-2325 @databases
Scenario: Make sure list contains known collection
Given I literally go to databases/subject/Images
Then it should have link "ARTstor" with value "http://resolver.library.cornell.edu/misc/5346517"
