Given /^PENDING/ do
  pending
end
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press '([^"]*)'$/ do |button|

  if button == 'search'
    page.find(:css, 'button#search-btn').click
  else
    click_button button
  end
end
  def path_to(page_name)
    case page_name

    when /the home\s?page/
      root_path


    when /the catalog page/
      search_catalog_path

    when /the folder page/
      folder_index_path

    when /the document page for id (.+)/
      facet_catalog_path($1)

    when /the facet page for "([^\"]*)"/
      facet_catalog_path($1)

    when /the search page/
      search_index_path

    when /the single search results page/
      search_index_path

    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
When /^I fill in the search box with '(.*?)'$/ do |query|
  query.gsub!(/\\"/, '"')
  fill_in('q', :with => query)
end
Then /^the '(.*?)' select list should have an option for '(.*?)'$/ do |list, option|
  page.all('#' + list + '-dropdown ul.css-dropdown li.btn li', :text => option)
end
Then /^I should see each item format$/ do
  within('#documents') do
  	page.should have_css('.blacklight-title_display')
  	page.should have_css('.blacklight-author_display')
  end
end
Then /^results should have a select checkbox$/ do
  within('#documents') do
  	page.should have_selector('.bookmark_add')
  end
end
Then /^results should have a title field$/ do
  within('#documents') do
    page.should have_css('.blacklight-title_display')
  end
end
Then /^I should get results$/ do
  page.should have_selector(".document")
end
Then /^I should not get results$/ do
  page.should_not have_selector("div.document")
end
Then /^click on first link "(.*?)"$/ do |link|
  l = page.first('a', :text => link)
  l.click 
end
