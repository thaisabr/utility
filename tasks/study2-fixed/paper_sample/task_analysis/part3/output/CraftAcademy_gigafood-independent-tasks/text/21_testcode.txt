Given(/^the following orders exist:$/) do |table|
  table.hashes.each do |order|
    FactoryGirl.create(:order, order)
  end
end
When(/^I press "([^"]*)" for order "([^"]*)"$/) do |link, order_name|
  order = Order.find_by billing_name: order_name
  within("#order_#{order.id}") do
    click_link_or_button link
  end
end
Given(/^"([^"]*)"'s order contains no items$/) do |billing_name|
  @order = Order.find_by(billing_name: billing_name)
  @order.clear if @order.shopping_cart_items
end
When(/^I click on "([^"]*)"$/) do |link_or_button|
  click_link_or_button link_or_button
end
When /^(?:I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
def path_to(page_name)
  case page_name
  when 'the dashboard'
    admin_root_path
  when 'the landing page'
    root_path
  when 'order page'
    order_path
  else
    error
  end
end
When /^(?:I )press "([^"]*)"$/ do |button|
  click_link_or_button(button)
end
Then /^(?:I )should( not)? see( the element)? "([^"]*)"$/ do |negate, is_css, text|
  should = negate ? :not_to : :to
  have = is_css ? have_css(text) : have_content(text)
  expect(page).send should, have
end
Given(/^an admin exists with email "([^"]*)" and password "([^"]*)"$/) do |email, password|
  @admin = FactoryGirl.create(:admin_user, email: email, password: password)
end
And(/^I'm loged in as admin user "([^"]*)"$/) do |email|
  login_as(@admin, scope: :admin_user)
end
Given(/^the following categories exist:$/) do |table|
  table.hashes.each do | category |
    Category.create(category)
  end
end
Given(/^the following dishes exist:$/) do |table|
  table.hashes.each do |dish|
    if dish[:category]
      category = Category.find_by(name: dish[:category])
      hash = dish.except!(dish[:category]).merge(category: category)
      FactoryGirl.create(:dish, hash)
    else
      FactoryGirl.create(:dish, dish)
    end
  end
end
