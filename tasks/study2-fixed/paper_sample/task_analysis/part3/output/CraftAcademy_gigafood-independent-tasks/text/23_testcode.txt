Given(/^the following orders exist:$/) do |table|
  table.hashes.each do |order|
    FactoryGirl.create(:order, order)
  end
end
When(/^I press "([^"]*)" for order "([^"]*)"$/) do |link, order_name|
  order = Order.find_by billing_name: order_name
  within("#order_#{order.id}") do
    click_link_or_button link
  end
end
Given(/^"([^"]*)"'s order contains:$/) do |billing_name, table|
  @order = Order.find_by(billing_name: billing_name)
  table.hashes.each do |item|
    dish = Dish.find_by(name: item[:dish_name])
    @order.add(dish, dish.price, item[:quantity].to_i)
  end
end
When(/^I click on "([^"]*)"$/) do |link_or_button|
  click_link_or_button link_or_button
end
When /^(?:I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
def path_to(page_name)
  case page_name
  when 'the dashboard'
    admin_root_path
  when 'the landing page'
    root_path
  when 'order page'
    order_path
  else
    error
  end
end
When /^(?:I )press "([^"]*)"$/ do |button|
  click_link_or_button(button)
end
Then /^(?:I )should( not)? see( the element)? "([^"]*)"$/ do |negate, is_css, text|
  should = negate ? :not_to : :to
  have = is_css ? have_css(text) : have_content(text)
  expect(page).send should, have
end
Given(/^an admin exists with email "([^"]*)" and password "([^"]*)"$/) do |email, password|
  @admin = FactoryGirl.create(:admin_user, email: email, password: password)
end
And(/^I'm loged in as admin user "([^"]*)"$/) do |email|
  login_as(@admin, scope: :admin_user)
end
Then(/^an invoice for the order should be created$/) do
  expect(@order.attachments.first.file_type).to eq 'invoice'
end
And(/^the invoice should contain "([^"]*)"$/) do |content|

  remote_pdf = open(Rails.root.join('spec', 'fixtures', 'tmp', 'tmp.pdf'), 'wb') do |file|
    file << open(@order.attachments.first.file.url).read
  end
  pdf = PDF::Inspector::Text.analyze_file(remote_pdf)
  expect(pdf.strings).to include content
end
Given(/^an Invoice has been generated for "([^"]*)"'s order$/) do |billing_email|
  steps %Q{
      Given "#{billing_email}"'s order contains:
        | dish_name | quantity |
        | Dish 1    | 10       |
        | Dish 2    | 20       |
      And I click on "Orders"
      And I press "View" for order "Bob Schmob"
      And I press "Generate Invoice"
      }
end
Then(/^I should see the invoice in a new window$/) do
  switch_to_window windows.last
  expect(page.response_headers['Content-Type']).to eq 'application/pdf'
end
Given(/^the following categories exist:$/) do |table|
  table.hashes.each do | category |
    Category.create(category)
  end
end
Given(/^the following dishes exist:$/) do |table|
  table.hashes.each do |dish|
    if dish[:category]
      category = Category.find_by(name: dish[:category])
      hash = dish.except!(dish[:category]).merge(category: category)
      FactoryGirl.create(:dish, hash)
    else
      FactoryGirl.create(:dish, dish)
    end
  end
end
Given(/^"([^"]*)"'s order contains:$/) do |billing_name, table|
  @order = Order.find_by(billing_name: billing_name)
  table.hashes.each do |item|
    dish = Dish.find_by(name: item[:dish_name])
    @order.add(dish, dish.price, item[:quantity].to_i)
  end
end
