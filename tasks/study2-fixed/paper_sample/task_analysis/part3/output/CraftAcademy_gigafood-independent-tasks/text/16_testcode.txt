When(/^I click on "([^"]*)"$/) do |link_or_button|
  click_link_or_button link_or_button
end
When /^(?:I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
def path_to(page_name)
  case page_name
  when 'the dashboard'
    admin_root_path
  when 'the landing page'
    root_path
  when 'order page'
    order_path
  else
    error
  end
end
Given(/^the following dishes exist:$/) do |table|
  table.hashes.each do |dish|
    if dish[:category]
      category = Category.find_by(name: dish[:category])
      hash = dish.except!(dish[:category]).merge(category: category)
      FactoryGirl.create(:dish, hash)
    else
      FactoryGirl.create(:dish, dish)
    end
  end
end
When(/^I click on \+ for "([^"]*)"$/) do |dish_name|
  dish_id = Dish.find_by(name: dish_name).id

  within "#dish_item_#{dish_id}" do
    click_link_or_button '+'
  end
end
And(/^I fill in Delivery Date with "([^"]*)"$/) do |date|
  fill_in('order_delivery_date', with: date)
end
Then(/^the total for the order should be "([^"]*)"$/) do |total|
  @order.reload
  expect(@order.total).to eq Money.new(total.to_f * 100)
end
Then(/^the tax for the order should be "([^"]*)"$/) do |tax|
  @order = @order || Order.last
  @order.reload
  expect(@order.taxes).to eq Money.new(tax.to_f * 100)
end
When /^(?:I )fill in "([^"]*)" with "([^"]*)"$/ do |field, value|
  fill_in(field, with: value)
end
Then /^(?:I )should( not)? see( the element)? "([^"]*)"$/ do |negate, is_css, text|
  should = negate ? :not_to : :to
  have = is_css ? have_css(text) : have_content(text)
  expect(page).send should, have
end
