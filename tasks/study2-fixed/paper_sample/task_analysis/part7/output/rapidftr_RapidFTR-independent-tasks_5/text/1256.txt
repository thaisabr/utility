Feature:
So that we can update details of children that are found in the field, a user should be able to go to a website and change
the child's record
Scenario: Photo is required
Given I am logged in
Given I am on children listing page
And I follow "New Child"
When I fill in "Mumbai" for "Last known location"
And I press "Save"
Then I should see "error prohibited this child from being saved"
And I should see "Photo must be provided"
