Classes: 1
[name:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:Given ]

Methods: 14
[name:Hpricot, type:Object, file:null, step:Then ]
[name:at, type:Object, file:null, step:Then ]
[name:be_empty, type:Object, file:null, step:Then ]
[name:body, type:Object, file:null, step:Then ]
[name:click_link, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:Given ]
[name:create_new_custom, type:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:Given ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:first, type:Object, file:null, step:Then ]
[name:have_selector, type:Object, file:null, step:Then ]
[name:inner_html, type:Object, file:null, step:Then ]
[name:response, type:Object, file:null, step:Then ]
[name:response_body, type:Object, file:null, step:Then ]
[name:search, type:Child, file:rapidftr_RapidFTR/app/models/child.rb, step:Then ]

Referenced pages: 0

