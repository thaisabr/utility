Classes: 13
[name:Array, file:null, step:Given ]
[name:Child, file:rapidftr_RapidFTR/app/models/child.rb, step:Then ]
[name:Field, file:rapidftr_RapidFTR/app/models/field.rb, step:Given ]
[name:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:Given ]
[name:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:Then ]
[name:Options, file:rapidftr_RapidFTR/vendor/plugins/rspec/lib/spec/runner/options.rb, step:Then ]
[name:Spec, file:rapidftr_RapidFTR/vendor/plugins/rspec/lib/spec.rb, step:Then ]
[name:SuggestedField, file:rapidftr_RapidFTR/app/models/suggested_field.rb, step:Given ]
[name:Summary, file:rapidftr_RapidFTR/app/models/summary.rb, step:Then ]
[name:URI, file:null, step:Then ]
[name:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Then ]
[name:User, file:rapidftr_RapidFTR/vendor/gems/couchrest-0.34/spec/fixtures/more/user.rb, step:Then ]
[name:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:Then ]

Methods: 73
[name:Hpricot, type:Object, file:null, step:Then ]
[name:all, type:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:Given ]
[name:assert_contain, type:Object, file:null, step:Then ]
[name:assert_equal, type:Object, file:null, step:Then ]
[name:at, type:Object, file:null, step:Then ]
[name:be_nil, type:Object, file:null, step:Then ]
[name:body, type:Object, file:null, step:Then ]
[name:by_name, type:Summary, file:rapidftr_RapidFTR/app/models/summary.rb, step:Then ]
[name:by_user_name, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Then ]
[name:by_user_name, type:User, file:rapidftr_RapidFTR/vendor/gems/couchrest-0.34/spec/fixtures/more/user.rb, step:Then ]
[name:cancel, type:Object, file:null, step:Then ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:compact, type:Object, file:null, step:Then ]
[name:contain, type:Object, file:null, step:Then ]
[name:create!, type:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:Given ]
[name:create!, type:SuggestedField, file:rapidftr_RapidFTR/app/models/suggested_field.rb, step:Given ]
[name:current_url, type:Object, file:null, step:Then ]
[name:current_url_with_format_of, type:ApplicationHelper, file:rapidftr_RapidFTR/app/helpers/application_helper.rb, step:Then ]
[name:destroy, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:Given ]
[name:destroy, type:SessionsController, file:rapidftr_RapidFTR/app/controllers/sessions_controller.rb, step:Given ]
[name:destroy, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:Given ]
[name:destroy, type:SessionsController, file:rapidftr_RapidFTR/app/controllers/sessions_controller.rb, step:Then ]
[name:downcase, type:Object, file:null, step:Given ]
[name:each, type:Object, file:null, step:Given ]
[name:each, type:Object, file:null, step:Then ]
[name:edit, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:Then ]
[name:edit, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:Then ]
[name:eval, type:Object, file:null, step:Given ]
[name:find_by_user_name, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Then ]
[name:find_by_user_name, type:User, file:rapidftr_RapidFTR/vendor/gems/couchrest-0.34/spec/fixtures/more/user.rb, step:Then ]
[name:form_field_for, type:Object, file:null, step:Then ]
[name:form_fields_list, type:Object, file:null, step:Then ]
[name:full_name, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Then ]
[name:full_name, type:User, file:rapidftr_RapidFTR/vendor/gems/couchrest-0.34/spec/fixtures/more/user.rb, step:Then ]
[name:gsub, type:Object, file:null, step:Given ]
[name:hashes, type:Object, file:null, step:Given ]
[name:hashes, type:Object, file:null, step:Then ]
[name:have_selector, type:Object, file:null, step:Then ]
[name:index, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:Then ]
[name:index, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:Then ]
[name:index, type:FormSectionController, file:rapidftr_RapidFTR/app/controllers/form_section_controller.rb, step:Then ]
[name:index, type:FieldsController, file:rapidftr_RapidFTR/app/controllers/fields_controller.rb, step:Then ]
[name:index, type:AdminController, file:rapidftr_RapidFTR/app/controllers/admin_controller.rb, step:Then ]
[name:inner_html, type:Object, file:null, step:Then ]
[name:join, type:Object, file:null, step:Then ]
[name:name, type:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:Then ]
[name:new, type:Field, file:rapidftr_RapidFTR/app/models/field.rb, step:Given ]
[name:new, type:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:Then ]
[name:new, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:Then ]
[name:new, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:Then ]
[name:new, type:SessionsController, file:rapidftr_RapidFTR/app/controllers/sessions_controller.rb, step:Then ]
[name:new, type:FormSectionController, file:rapidftr_RapidFTR/app/controllers/form_section_controller.rb, step:Then ]
[name:new, type:FieldsController, file:rapidftr_RapidFTR/app/controllers/fields_controller.rb, step:Then ]
[name:path_to, type:Paths, file:rapidftr_RapidFTR/features/support/paths.rb, step:Then ]
[name:raise, type:Object, file:null, step:Then ]
[name:response, type:Object, file:null, step:Then ]
[name:response_body, type:Object, file:null, step:Then ]
[name:reverse_merge!, type:Object, file:null, step:Given ]
[name:search, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:Then ]
[name:section_name, type:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:Then ]
[name:select, type:Object, file:null, step:Then ]
[name:send, type:Object, file:null, step:Then ]
[name:show, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:Then ]
[name:show, type:HistoriesController, file:rapidftr_RapidFTR/app/controllers/histories_controller.rb, step:Then ]
[name:show, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:Then ]
[name:strip, type:Object, file:null, step:Then ]
[name:suggested_field_display_for, type:Object, file:null, step:Then ]
[name:suggested_fields_list, type:Object, file:null, step:Then ]
[name:to_i, type:Object, file:null, step:Given ]
[name:user_name, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Then ]
[name:user_name, type:User, file:rapidftr_RapidFTR/vendor/gems/couchrest-0.34/spec/fixtures/more/user.rb, step:Then ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 28
rapidftr_RapidFTR/app/views/admin/index.html.erb
rapidftr_RapidFTR/app/views/children/_form_section.html.erb
rapidftr_RapidFTR/app/views/children/_search_results.html.erb
rapidftr_RapidFTR/app/views/children/_summary_row.html.erb
rapidftr_RapidFTR/app/views/children/_tabs.html.erb
rapidftr_RapidFTR/app/views/children/edit.html.erb
rapidftr_RapidFTR/app/views/children/index.html.erb
rapidftr_RapidFTR/app/views/children/new.html.erb
rapidftr_RapidFTR/app/views/children/search.html.erb
rapidftr_RapidFTR/app/views/children/show.html.erb
rapidftr_RapidFTR/app/views/fields/_direction_button.html.erb
rapidftr_RapidFTR/app/views/fields/_suggested_field.html.erb
rapidftr_RapidFTR/app/views/fields/index.html.erb
rapidftr_RapidFTR/app/views/fields/new.html.erb
rapidftr_RapidFTR/app/views/fields/new_select_drop_down.html.erb
rapidftr_RapidFTR/app/views/fields/new_text_field.html.erb
rapidftr_RapidFTR/app/views/form_section/index.html.erb
rapidftr_RapidFTR/app/views/form_section/new.html.erb
rapidftr_RapidFTR/app/views/histories/_history_change.html.erb
rapidftr_RapidFTR/app/views/histories/_photo_history_change.html.erb
rapidftr_RapidFTR/app/views/histories/show.html.erb
rapidftr_RapidFTR/app/views/sessions/new.html.erb
rapidftr_RapidFTR/app/views/sessions/show.html.erb
rapidftr_RapidFTR/app/views/users/_edittable_user.html.erb
rapidftr_RapidFTR/app/views/users/edit.html.erb
rapidftr_RapidFTR/app/views/users/index.html.erb
rapidftr_RapidFTR/app/views/users/new.html.erb
rapidftr_RapidFTR/app/views/users/show.html.erb

