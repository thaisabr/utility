Feature: Disable and enable forms
In order to customise the view
As an admin user
wants to be able to enable and disable particular forms
Background:
Given the following form sections exist in the system:
| name              | unique_id         | editable | order | enabled |
| Basic details     | basic_details     | false    | 1     | true    |
| Caregiver details | caregiver_details | true     | 2     | false   |
Scenario Outline: Register new disable_and_enable_forms
Given I am logged in as an admin
And I am on the form section page
When I check "sections_caregiver_details"
Then the checkbox with id "<checkbox_id>" <has_this_value>
Examples:
| name              | order | unique_id         | enabled | checkbox_id                | has_this_value        |
| Caregiver details | 2     | caregiver_details | false   | sections_caregiver_details | should not be checked |
