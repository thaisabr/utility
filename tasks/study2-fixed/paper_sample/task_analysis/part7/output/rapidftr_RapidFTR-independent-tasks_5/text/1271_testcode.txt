Given /^there is a admin$/ do
  Given "a admin \"admin\" with a password \"123\""
end
Given /^I am logged in as an admin$/ do
  Given "there is a admin"
  Given "I am on the login page"
  Given "I fill in \"admin\" for \"user name\""
  Given "I fill in \"123\" for \"password\""
  Given "I press \"Log In\""
end
Given /^I am logged in$/ do
  Given "there is a User"
  Given "I am on the login page"
  Given "I fill in \"#{User.first.user_name}\" for \"user name\""
  Given "I fill in \"123\" for \"password\""
  Given "I press \"Log In\""
end
Given /^the following form sections exist in the system:$/ do |form_sections_table|
  FormSection.all.each {|u| u.destroy }
  
  form_sections_table.hashes.each do |form_section_hash|
    form_section_hash.reverse_merge!(
      'unique_id'=> form_section_hash["name"].gsub(/\s/, "_").downcase,
      'enabled' => true,
      'fields'=> Array.new # todo:build these FSDs in a nicer way...
    )
    
    form_section_hash["order"] = form_section_hash["order"].to_i
    FormSection.create!(form_section_hash)
  end
end
Given /^the "([^\"]*)" form section has the field "([^\"]*)" with help text "([^\"]*)"$/ do |form_section, field_name, field_help_text|
  form_section = FormSection.get_by_unique_id(form_section.downcase.gsub(/\s/, "_"))
  field = Field.new(:name => field_name, :help_text => field_help_text)
  FormSection.add_field_to_formsection(form_section, field)
end
Then /^I should see a (\w+) in the enabled column for the form section "([^\"]*)"$/ do |expected_icon, form_section|
  row = Hpricot(response.body).form_section_row_for form_section
  row.should_not be_nil

  enabled_icon = row.enabled_icon
  enabled_icon["class"].should contain(expected_icon)
end
Then /^I should see the "([^\"]*)" form section link$/ do |form_section_name|
  form_section_names = Hpricot(response.body).form_section_names.collect {|item| item.inner_html}
  form_section_names.should contain(form_section_name)

end
Then /^I should see the text "([^\"]*)" in the enabled column for the form section "([^\"]*)"$/ do |expected_text, form_section|
  row = Hpricot(response.body).form_section_row_for form_section
  row.should_not be_nil

  enabled_icon = row.enabled_icon
  enabled_icon.inner_html.strip.should == expected_text
end
Then /^I should see the description text "([^\"]*)" for form section "([^\"]*)"$/ do |expected_description, form_section|
  row = Hpricot(response.body).form_section_row_for form_section
  description_text_cell = row.search("td").detect {|cell| cell.inner_html.strip == expected_description}
  description_text_cell.should_not be_nil
end
Then /^I should see the form section "([^\"]*)" in row (\d+)$/ do |form_section, expected_row_position|
  row = Hpricot(response.body).form_section_row_for form_section
  rows =  Hpricot(response.body).form_section_rows
  rows[expected_row_position.to_i].inner_html.should == row.inner_html
end
And /^I should see a current order of "([^\"]*)" for the "([^\"]*)" form section$/ do |expected_order, form_section|
  row = Hpricot(response.body).form_section_row_for form_section
  order_display = row.form_section_order
  order_display.inner_html.strip.should == expected_order
end
Given /^the following suggested fields exist in the system:$/ do |suggested_fields_table|
  suggested_fields_table.hashes.each do |suggested_field_hash|
    suggested_field_hash.reverse_merge!(
            'unique_id'=> suggested_field_hash["name"].gsub(/\s/, "_").downcase)
    
    field =  (Field.new :name=> suggested_field_hash["name"], :type=>suggested_field_hash["type"],:option_strings=>(eval suggested_field_hash["option_strings"]) )
    suggested_field_hash[:field] = field
    suggested_field_hash[:is_used] = false
    temp1 = SuggestedField.create!(suggested_field_hash)
  end
end
Then /^I should see the following suggested fields:$/ do |suggested_fields_table|
  suggested_fields_list = Hpricot(response.body).suggested_fields_list
  suggested_fields_list.should_not be_nil
  suggested_fields_table.hashes.each do |suggested_field_hash|
    display = suggested_fields_list.suggested_field_display_for suggested_field_hash[:unique_id]
    display.should_not be_nil
    display.at("input")[:value].strip.should == suggested_field_hash[:name]
    display.inner_html.should contain suggested_field_hash[:description]
  end
end
Then /^I should not see the following suggested fields:$/ do |suggested_fields_table|
  suggested_fields_list = Hpricot(response.body).suggested_fields_list
  suggested_fields_list.should_not be_nil
  suggested_fields_table.hashes.each do |suggested_field_hash|
    display = suggested_fields_list.suggested_field_display_for suggested_field_hash[:unique_id]
    display.should be_nil
  end
end
And /^I should see "([^\"]*)" in the list of fields$/ do |field_id|
  fields = Hpricot(response.body).form_fields_list
  fields.should_not be_nil
  field_row = fields.form_field_for(field_id)
  field_row.should_not be_nil
end
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^\"]*)"$/ do |button|
  click_button(button)
end
When /^(?:|I )follow "([^\"]*)"$/ do |link|
  click_link(link)
end
When /^(?:|I )fill in "([^\"]*)" for "([^\"]*)"$/ do |value, field|
  fill_in(field, :with => value)
end
When /^(?:|I )choose "([^\"]*)"$/ do |field|
  choose(field)
end
Then /^(?:|I )should see "([^\"]*)"$/ do |text|
  if defined?(Spec::Rails::Matchers)
    response.should contain(text)
  else
    assert_contain text
  end
end
Then /^the "([^\"]*)" checkbox should be checked$/ do |label|
  if defined?(Spec::Rails::Matchers)
    field_labeled(label).should be_checked
  else
    assert field_labeled(label).checked?
  end
end
Then /^(?:|I )should be on (.+)$/ do |page_name|
  current_path = URI.parse(current_url).select(:path, :query).compact.join('?')
  if defined?(Spec::Rails::Matchers)
    current_path.should == path_to(page_name)
  else
    assert_equal path_to(page_name), current_path
  end
end
  def path_to(page_name, options = {})

    format = page_name[/^(?:|the )(\w+) formatted/,1]
    options.reverse_merge!( :format => format )

    case page_name

      when /the home\s?page/
        '/'
      when /the new add_suggested_field_to_form_section page/
        new_add_suggested_field_to_form_section_path

      when /the new assign_unique_id_to_a_child page/
        new_assign_unique_id_to_a_child_path(options)

      when /new child page/
        new_child_path(options)

      when /children listing page/
        children_path(options)

      when /saved record page for child with name "(.+)"/
        child_name = $1
        child = Summary.by_name(:key => child_name)
        raise "no child named '#{child_name}'" if child.nil?
        child_path( child, options )
        
      when /child record page for "(.+)"/
        child_name = $1
        child = Summary.by_name(:key => child_name)
        raise "no child named '#{child_name}'" if child.nil?
        child_path( child, options )
        
      when /change log page for "(.+)"/
        child_name = $1
        child = Summary.by_name(:key => child_name)
        raise "no child named '#{child_name}'" if child.nil?
        child_history_path( child, options )

      when /new user page/
        new_user_path(options)

      when /manage users page/
        users_path(options)

      when /edit user page for "(.+)"/
        user = User.find_by_user_name($1)
        edit_user_path(user, options)

      when /child search page/
        search_children_path(options)
        
      when /login page/
        login_path(options)
        
      when /logout page/
        logout_path(options)
        
      when /child search results page/
        search_children_path(options)
        
      when /create form section page/
        new_formsection_path(options)
        
      when /form section page/
        formsections_path(options) 

      when /choose field type page/
        arbitrary_form_section = FormSection.new
        new_formsection_field_path( arbitrary_form_section, options )

      when /the edit user page for "(.+)"$/
        user = User.by_user_name(:key => $1)
        raise "no user named #{$1}" if user.nil?
        edit_user_path(user)

      when /photo resource for child with name "(.+)"/
        child_name = $1
        child = Summary.by_name(:key => child_name)
        raise "no child named '#{child_name}'" if child.nil?
        child_path( child, :format => 'jpg' )

      when /new field page for "(.+)"/
        field_type = $1
        send( "new_#{field_type}_formsection_fields_path" )

      when /the manage fields page for "(.+)"/
        form_section = $1
        formsection_fields_path(form_section)

      when /the admin page/
        admin_path(options) 

      # Add more mappings here.
      # Here is an example that pulls values out of the Regexp:
      #
      #   when /^(.*)'s profile page$/i
      #     user_profile_path(User.find_by_login($1))

      else
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
                "Now, go and add a mapping in #{__FILE__}"
    end
  end
Then /^I should find the following links:$/ do |table|
  table.rows_hash.each do |label, named_path|
    href = path_to(named_path)
    assert_have_xpath "//a[@href='#{href}' and text()='#{label}']"
  end
end
Then /^I should find the form with following attributes:$/ do |table|
  table.raw.each do |attribute|
    assert_contain attribute.first
  end
end
Then /^the "([^\"]*)" field should be disabled$/ do |label|
  field_labeled(label).should be_disabled
end
Then /^I should see the select named "([^\"]*)"$/ do |select_name|
  	response_body.should have_selector("select[name='#{select_name}']")
end
Then /^I should see an option "([^\"]*)" for select "([^\"]*)"$/  do | option_value, select_name|
  	response_body.should have_selector("select[name='#{select_name}'] option[value=#{option_value}]")
end
Then /^I should not be able to see (.+)$/ do |page_name|
  lambda { visit path_to(page_name) }.should raise_error(AuthorizationFailure)
end
Then /^I should be able to see (.+)$/ do |page_name|
  When "I go to #{page_name}"
  Then "I should be on #{page_name}"
end
Then /^I should not see the "([^\"]*)" link for the "([^\"]*)" section$/ do |link, section_name|
  row = Hpricot(response.body).search("tr[@id=basic_details_row]").first

  row.inner_html.should_not include(link)
end
When /^I add a new text field with "([^\"]*)" and "([^\"]*)"$/ do |name, help_text|
  When 'I follow "Add Custom Field"'
  And 'I follow "TextField"'
  And 'I fill in "#{name}" for "name"'
  And 'I fill in "#{help_text}" for "Help text"'
  And 'I press "Create"'
end
Then /^I should not see the "([^\"]*)" arrow for the "([^\"]*)" field$/ do |arrow_name, field_name|
  row = Hpricot(response.body).search("tr[@id=#{field_name}Row]").first
  row.inner_html.should_not include(arrow_name)
end
Then /^I should see the "([^\"]*)" arrow for the "([^\"]*)" field$/ do |arrow_name, field_name|
  row = Hpricot(response.body).search("tr[@id=#{field_name}Row]").first
  row.inner_html.should include(arrow_name)
end
And /^I click the "([^\"]*)" arrow on "([^\"]*)" field$/ do |arrow_name, field_name|
  click_button("#{field_name}_#{arrow_name}")
end
Then /^the "([^\"]*)" field should be above the "([^\"]*)" field$/ do |first_field_name, second_field_name|
  table_rows = Hpricot(response.body).search("table tr")
  row_ids = table_rows.collect {|row| row[:id]}

  index_of_first_row = row_ids.index(first_field_name + "Row")
  index_of_second_row = row_ids.index(second_field_name + "Row")

  index_of_first_row.should < index_of_second_row
end
Given /^an? (user|admin) "([^\"]*)" with(?: a)? password "([^\"]*)"$/ do |user_type, username, password|
  user_type = user_type == 'user' ? 'User' : 'Administrator'
  @user = User.new(
    :user_name=>username, 
    :password=>password, 
    :password_confirmation=>password, 
    :user_type=> user_type, 
    :full_name=>username, 
    :email=>"#{username}@test.com")
  @user.save!
end
