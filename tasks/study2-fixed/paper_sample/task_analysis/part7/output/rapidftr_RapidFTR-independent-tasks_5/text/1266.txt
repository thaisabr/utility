Feature:
So that changes to the child record are kept for historical purposed and can be viewed
Scenario:  I log in as a different user, upload a new photo and view the record log
Given the date/time is "July 19 2010 13:05:32"
And the following children exist in the system:
| name       | age | age_is | gender | last_known_location |
| Jorge Just | 27  | Exact  | Male   | Haiti               |
And the date/time is "Sept 29 2010 17:59:33"
And "Mary" is logged in
And I am on the children listing page
When I follow "Edit"
And I attach the file "features/resources/jeff.png" to "photo"
And I press "Save"
And I follow "View the change log"
Then I should see "29/09/2010 17:59 Photo changed from"
And I should see the thumbnail of "Jorge Just" with key "photo-2010-07-19T130532"
And I should see the thumbnail of "Jorge Just" with key "photo-2010-09-29T175933"
And I should see "by mary"
When I follow "photo-2010-07-19T130532"
Then I should see the photo corresponding to "features/resources/jorge.jpg"
When I am on the children listing page
And I follow "Jorge Just"
And I follow "View the change log"
When I follow "photo-2010-09-29T175933"
Then I should see the photo corresponding to "features/resources/jeff.png"
Scenario:  I log in as a different user, edit and view the record log
Given the date/time is "July 19 2010 13:05:15"
And the following children exist in the system:
| name       | age | age_is | gender | last_known_location |
| Jorge Just | 27  | Exact  | Male   | Haiti               |
And the date/time is "Oct 29 2010 10:12"
And "Bobby" is logged in
And I am on the children listing page
When I follow "Edit"
Then I fill in "George Harrison" for "Name"
And I fill in "56" for "Age"
And I select "Approximate" from "Age is"
And I choose "Female"
And I fill in "Bombay" for "Origin"
And I fill in "Zambia" for "Last known location"
And I select "6 months to 1 year ago" from "Date of separation"
And the date/time is "Oct 29 2010 10:12:15"
And I press "Save"
When I follow "View the change log"
Then I should see "29/10/2010 10:12 Last known location changed from Haiti to Zambia by bobby"
And I should see "29/10/2010 10:12 Origin initially set to Bombay by bobby"
And I should see "29/10/2010 10:12 Age changed from 27 to 56 by bobby"
And I should see "29/10/2010 10:12 Name changed from Jorge Just to George Harrison by bobby"
And I should see "29/10/2010 10:12 Date of separation initially set to 6 months to 1 year ago by bobby"
And I should see "29/10/2010 10:12 Gender changed from Male to Female by bobby"
And I should see "29/10/2010 10:12 Age is changed from Exact to Approximate"
Feature: So that I can find a child that has been entered in to RapidFTR
As a user of the website
I want to enter a search query in to a search box and see all relevant results
Background:
Given I am logged in
And I am on the child search page
Scenario: Thumbnails are displayed for each search result, if requested
Given the following children exist in the system:
| name   	|
| Willis	|
| Will	|
When I fill in "W" for "Name"
And I check "Show thumbnails"
And I press "Search"
Then I should see the thumbnail of "Willis"
And I should see the thumbnail of "Will"
Scenario: Thumbnails are not displayed for each search result, if not requested
Given the following children exist in the system:
| name   	|
| Willis	|
| Will	|
When I fill in "W" for "Name"
And I uncheck "Show thumbnails"
And I press "Search"
Then I should not see the thumbnail of "Willis"
Feature:
So that we can update details of children that are found in the field, a user should be able to go to a website and change
the child's record
Scenario: Editing a child record
Given I am logged in
Given I am on children listing page
And I follow "New child"
When I fill in "Jorge Just" for "Name"
And I fill in "27" for "Age"
And I select "Exact" from "Age is"
And I choose "Male"
And I fill in "London" for "Origin"
And I fill in "Haiti" for "Last known location"
And I select "1-2 weeks ago" from "Date of separation"
And I attach the file "features/resources/jorge.jpg" to "photo"
And I press "Save"
Then I follow "Edit"
When I fill in "George Harrison" for "Name"
And I fill in "56" for "Age"
And I select "Approximate" from "Age is"
And I choose "Female"
And I fill in "Bombay" for "Origin"
And I fill in "Zambia" for "Last known location"
And I select "6 months to 1 year ago" from "Date of separation"
And I attach the file "features/resources/jeff.png" to "photo"
And I press "Save"
Then I should see "George Harrison"
And I should see "56"
And I should see "Approximate"
And I should see "Female"
And I should see "Bombay"
And I should see "Zambia"
And I should see "6 months to 1 year ago"
And I should see the photo of "George Harrison"
And I should see "Child was successfully updated."
Then I follow "Edit"
And I attach the file "features/resources/textfile.txt" to "photo"
And I fill in "" for "Last known location"
And I press "Save"
Then I should see "Please upload a valid photo file (jpg or png) for this child record"
Then I should see "Last known location cannot be empty"
Feature:
So that all child records contains a photo of that child
As a field agent using the website
I want to upload a picture of the child record that I'm adding
Scenario: Uploading a standard JPG image
Given I am logged in
Given I am on the new child page
When I fill in "Haiti" for "Last known location"
And I fill in "John" for "Name"
And I attach the file "features/resources/jorge.jpg" to "photo"
And I press "Save"
Then I should see "Child record successfully created"
And I should see the photo of "John"
Feature:
So that we can have a preview of the most recent uploaded photo of a child, a user should be able to see the thumbnail
of the child's photo while editing the child's record
Scenario: Seeing thumbnail when editing a child record
Given I am logged in
And an existing child with name "John" and a photo from "features/resources/jorge.jpg"
When I am editing the child with name "John"
Then I should see the thumbnail of "John"
