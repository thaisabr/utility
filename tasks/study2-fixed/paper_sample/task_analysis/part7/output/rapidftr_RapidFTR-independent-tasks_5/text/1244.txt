Feature: So that admin can customize fields in a form section
Background:
Given the following form sections exist in the system:
| name | unique_id | editable | order | enabled |
| Basic details | basic_details | false | 1 | true |
| Family details | family_details | true | 2 | true |
Scenario: Admins should be able to add new new text fields
Given I am logged in as an admin
And I am on the manage fields page for "family_details"
When I follow "Add Custom Field"
Then I should find the following links:
| Text Field | new field page for "text_field" |
| Text Area  | new field page for "textarea"  |
| Check box  | new field page for "check_box"  |
| Select drop down | new field page for "select_drop_down" |
When I follow "Text Field"
Then I should find the form with following attributes:
| Name |
| Help text |
| Enabled |
And the "Enabled" checkbox should be checked
When I fill in "Anything" for "name"
When I fill in "Really anything" for "Help text"
And I press "Create"
Then I should see "Anything"
When I am on children listing page
And I follow "New child"
Then I should see "Anything"
Feature:As a user ,when I login with my credentials,my username should be used to create a unique child id
Scenario: The unique Id of child should use the logged-in user's username
Given I am logged in
When I am on the new child page
When I fill in the basic details of a child
And I attach the file "features/resources/jeff.png" to "photo"
And I press "Save"
Then I should see "Child record successfully created."
Feature:
So that we can keep track of children that are found in the field, a user should be able to go to a website and upload
basic information about the lost child.
Scenario: creating a child record
Given I am logged in
Given I am on children listing page
And I follow "New child"
When I fill in "Jorge Just" for "Name"
And I fill in "27" for "Age"
And I select "Exact" from "Age is"
And I choose "Male"
And I fill in "London" for "Origin"
And I fill in "Haiti" for "Last known location"
And I select "1-2 weeks ago" from "Date of separation"
And I attach the file "features/resources/jorge.jpg" to "photo"
And I press "Save"
Then I should see "Child record successfully created."
And I should see "Jorge Just"
And I should see "27"
And I should see "1"
And I should see "Male"
And I should see "London"
And I should see "Haiti"
And I should see "1-2 weeks ago"
When I follow "Back"
Then I should see "Listing children"
And I should see "Jorge Just"
When I follow "Jorge Just"
Then I follow "Back"
And I should see "Listing children"
And I should see "Jorge Just"
Scenario: create child record with no details filled in
Given I am logged in
Given I am on new child page
And I press "Save"
Then I should see "Child record successfully created."
