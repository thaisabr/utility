Classes: 6
[name:Child, file:rapidftr_RapidFTR/app/models/child.rb, step:Then ]
[name:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:null]
[name:Spec, file:null, step:Then ]
[name:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Given ]
[name:User, file:rapidftr_RapidFTR/app/models/user.rb, step:null]
[name:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:null]

Methods: 48
[name:Hpricot, type:Object, file:null, step:Then ]
[name:assert_contain, type:Object, file:null, step:Then ]
[name:at, type:Object, file:null, step:Then ]
[name:attach_file, type:Object, file:null, step:When ]
[name:attach_file, type:Object, file:null, step:Then ]
[name:be_nil, type:Object, file:null, step:Then ]
[name:body, type:Object, file:null, step:Then ]
[name:check_link, type:PhotoThumbnailSteps, file:rapidftr_RapidFTR/features/step_definitions/photo_thumbnail_steps.rb, step:Then ]
[name:choose, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:Given ]
[name:click_link, type:Object, file:null, step:Then ]
[name:contain, type:Object, file:null, step:Then ]
[name:downcase, type:Object, file:null, step:Then ]
[name:each, type:Object, file:null, step:Then ]
[name:edit, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:null]
[name:edit, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:null]
[name:fill_in, type:Object, file:null, step:When ]
[name:find_child_by_name, type:Object, file:null, step:Then ]
[name:first, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Given ]
[name:full_name, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:null]
[name:hashes, type:Object, file:null, step:Then ]
[name:have_selector, type:Object, file:null, step:Then ]
[name:include?, type:Object, file:null, step:Then ]
[name:index, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:null]
[name:index, type:AdminController, file:rapidftr_RapidFTR/app/controllers/admin_controller.rb, step:null]
[name:index, type:FormSectionController, file:rapidftr_RapidFTR/app/controllers/form_section_controller.rb, step:null]
[name:index, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:null]
[name:index, type:FieldsController, file:rapidftr_RapidFTR/app/controllers/fields_controller.rb, step:null]
[name:inner_html, type:Object, file:null, step:Then ]
[name:inner_text, type:Object, file:null, step:Then ]
[name:name, type:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:null]
[name:new, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:null]
[name:new, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:null]
[name:represent_inline_attachment, type:Object, file:null, step:Then ]
[name:response, type:Object, file:null, step:Then ]
[name:search, type:Child, file:rapidftr_RapidFTR/app/models/child.rb, step:Then ]
[name:select, type:Object, file:null, step:When ]
[name:show_photo, type:ChildMediaController, file:rapidftr_RapidFTR/app/controllers/child_media_controller.rb, step:Then ]
[name:split, type:Object, file:null, step:When ]
[name:split, type:Object, file:null, step:Then ]
[name:strip, type:Object, file:null, step:Then ]
[name:suggested_field_display_for, type:Object, file:null, step:Then ]
[name:suggested_fields_list, type:Object, file:null, step:Then ]
[name:to_html, type:Object, file:null, step:Then ]
[name:uploadable_photo, type:Object, file:null, step:Then ]
[name:user_name, type:Session, file:rapidftr_RapidFTR/app/models/session.rb, step:Given ]
[name:user_name, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:null]

Referenced pages: 14
rapidftr_RapidFTR/app/views/admin/index.html.erb
rapidftr_RapidFTR/app/views/children/_form_section.html.erb
rapidftr_RapidFTR/app/views/children/_tabs.html.erb
rapidftr_RapidFTR/app/views/children/edit.html.erb
rapidftr_RapidFTR/app/views/children/index.html.erb
rapidftr_RapidFTR/app/views/children/new.html.erb
rapidftr_RapidFTR/app/views/fields/_delete_button.html.erb
rapidftr_RapidFTR/app/views/fields/_direction_button.html.erb
rapidftr_RapidFTR/app/views/fields/index.html.erb
rapidftr_RapidFTR/app/views/form_section/index.html.erb
rapidftr_RapidFTR/app/views/users/_edittable_user.html.erb
rapidftr_RapidFTR/app/views/users/edit.html.erb
rapidftr_RapidFTR/app/views/users/index.html.erb
rapidftr_RapidFTR/app/views/users/new.html.erb

