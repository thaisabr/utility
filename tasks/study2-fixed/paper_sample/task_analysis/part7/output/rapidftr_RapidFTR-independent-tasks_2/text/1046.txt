Feature: So that admin can customize fields in a form section
Background:
Given the following form sections exist in the system:
| name           | unique_id      | editable | order | enabled | perm_enabled |
| Basic details  | basic_details  | false    | 1     | true    | true         |
| Family details | family_details | true     | 2     | true    | false        |
Scenario: should be able to go back to edit form section from add custom field page
Given I am on the edit form section page for "family_details"
And I follow "Add Custom Field"
And I follow "Go Back To Edit Forms Page"
Then I am on edit form section page
