Classes: 2
[name:Array, file:null, step:Given ]
[name:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:Given ]

Methods: 16
[name:all, type:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:Given ]
[name:click_link, type:Object, file:null, step:Given ]
[name:create!, type:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:Given ]
[name:destroy, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:Given ]
[name:destroy, type:SessionsController, file:rapidftr_RapidFTR/app/controllers/sessions_controller.rb, step:Given ]
[name:destroy, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:Given ]
[name:downcase, type:Object, file:null, step:Given ]
[name:each, type:Object, file:null, step:Given ]
[name:gsub, type:Object, file:null, step:Given ]
[name:hashes, type:Object, file:null, step:Given ]
[name:index, type:FieldsController, file:rapidftr_RapidFTR/app/controllers/fields_controller.rb, step:null]
[name:new, type:FieldsController, file:rapidftr_RapidFTR/app/controllers/fields_controller.rb, step:null]
[name:option_strings_text, type:Field, file:rapidftr_RapidFTR/app/models/field.rb, step:null]
[name:reverse_merge!, type:Object, file:null, step:Given ]
[name:save, type:FieldsController, file:rapidftr_RapidFTR/app/controllers/fields_controller.rb, step:null]
[name:to_i, type:Object, file:null, step:Given ]

Referenced pages: 7
rapidftr_RapidFTR/app/views/fields/_basic_form.html.erb
rapidftr_RapidFTR/app/views/fields/_common.html.erb
rapidftr_RapidFTR/app/views/fields/_multiple_choice_form.html.erb
rapidftr_RapidFTR/app/views/fields/_suggested_field.html.erb
rapidftr_RapidFTR/app/views/fields/choose.html.erb
rapidftr_RapidFTR/app/views/fields/edit.html.erb
rapidftr_RapidFTR/app/views/fields/new.html.erb

