Feature: As an admin, I should be able to edit existing users.
Scenario: Password field should not be blank if re-enter password field is filled in and vice versa
Given I am logged in as an admin
And I am on manage users page
And I follow "New user"
When I fill in "John Doe" for "Full name"
And I fill in "johndoe1" for "user name"
And I fill in "password" for "password"
And I fill in "password" for "Re-enter password"
And I choose "User"
And I fill in "abcde@unicef.com" for "email"
And I fill in "UNICEF" for "organisation"
And I fill in "Rescuer" for "position"
And I fill in "Amazon" for "location"
And I press "Create"
Then I follow "Edit"
And I fill in "pass" for "Re-enter password"
And I press "Update"
Then I should see "Password does not match the confirmation"
When I am on the edit user page for "johndoe1"
Then I fill in "pass" for "password"
And I press "Update"
Then I should see "Password does not match the confirmation"
Feature:
As a user
I want to go to flag a child's record
So that I can identify suspect and duplicate records to admin
Background:
Given "Praful" is logged in
And the following children exist in the system:
| name   |
| Peter |
Scenario: Flagging a child record
When I flag "Peter" as suspect with the following reason:
"""
He is a bad guy.
"""
Then the view record page should show the record is flagged
And the edit record page should show the record is flagged
And the record history should log "Record was flagged by praful because: He is a bad guy."
And the suspect records page should show the following children:
| Peter |
Feature: So that admin can see Manage Users Page
Background:
Given I am logged in as an admin
And I follow "Admin"
And I follow "Manage Users"
Scenario: Admins should be able view himself
Then I should see "Show"
Then I should see "Edit"
Then I should not see "Delete User"
Feature: As an user, I should be able to request my password to be recovered.
Scenario: To check that an user is able to request password recovery
Given I am on the login page
When I follow "Request Password Reset"
And I fill in "any old thing" for "Enter your user name"
And I press "Request Password"
Then I should see "Thank you. A RapidFTR administrator will contact you shortly. If possible, contact the admin directly."
Scenario: An Admin user can see and hide notifications
Given a password recovery request for duck
And I am logged in as an admin
And I am on the home page
Then I should see "duck"
When I follow "hide"
Then I should not see "duck"
Scenario: An Admin user is able to see a link to the user profile of a given password recovery request
Given a user "duck" with password "iamevil"
And a password recovery request for duck
And I am logged in as an admin
And I am on the home page
Then I should see a link to the user details page for "duck"
Feature:
As an Admin
I want to ...
So that changes to the child record are kept for historical purposed and can be viewed
Scenario:  The change log page displays date-times in my local timezone
Given the date/time is "July 19 2010 13:05:15UTC"
And the following children exist in the system:
| name       | age | age_is | gender | last_known_location |
| Jorge Just | 27  | Exact  | Male   | Haiti               |
And the date/time is "Oct 29 2010 10:12UTC"
And "Bobby" is logged in
And I am on the children listing page
When I follow "Edit"
Then I fill in "George Harrison" for "Name"
And I attach a photo "features/resources/jorge.jpg"
And I attach the file "features/resources/sample.mp3" to "Recorded Audio"
And the date/time is "Oct 29 2010 14:12:15UTC"
And I press "Save"
When the user's time zone is "(GMT-11:00) Samoa"
And I am on the change log page for "George Harrison"
Then I should see "2010-10-29 03:12:15 SST Audio"
Then I should see "2010-10-29 03:12:15 SST Photo"
And I should see "2010-10-29 03:12:15 SST Name changed"
And I should see "2010-07-19 02:05:15 SST Record created"
Feature: So that I can filter the types of records being show when viewing search results
As a user of the website
I want to enter a search query in to a search box and see all relevant results
I want to filter the search results by either all, reunited, flagged or active
Background:
Given I am logged in
And I am on the children listing page
Scenario: Checking to verify there is a filter box
Then I should see "Filter by:"
And I should see "All"
And I should see "Reunited"
And I should see "Flagged"
And I should see "Active"
Scenario: Checking filter by All returns all the children in the system
Given the following children exist in the system:
| name   	| last_known_location 	| reporter | unique_id    | reunited | flag  |
| andreas	| London		            | zubair   | zubairlon123 | true     | false |
| zak	    | London		            | zubair   | zubairlon456 | false    | true  |
| jaco	  | NYC		                | james    | james456     | true     | true  |
| meredith| Austin	              | james    | james123     | false    | false |
When I am on the children listing page
Then I should see "andreas"
And I should see "zak"
And I should see "jaco"
And I should see "meredith"
Scenario: Checking filter by All should by default show all children in alphabetical order
Given the following children exist in the system:
| name   	| last_known_location 	| reporter | unique_id    |
| andreas	| London		            | zubair   | zubairlon123 |
| zak	    | London		            | zubair   | zubairlon456 |
| jaco	  | NYC		                | james    | james456     |
| meredith| Austin	              | james    | james123     |
When I am on the children listing page
Then I should see the order andreas,jaco,meredith,zak
Scenario: Checking filter by All shows the Order by options
Given the following children exist in the system:
| name   	| last_known_location 	| reporter | unique_id    |
| andreas	| London		            | zubair   | zubairlon123 |
| zak	    | London		            | zubair   | zubairlon456 |
| jaco	  | NYC		                | james    | james456     |
| meredith| Austin	              | james    | james123     |
When I am on the children listing page
Then I should see "Order by"
And I should see "Most recently created"
Scenario: Checking filter by All and then ordering by most recently added returns all the children in order of most recently added
Given the following children exist in the system:
| name   	| last_known_location 	| reporter | unique_id    | created_at                  |
| andreas	| London		            | zubair   | zubairlon123 | DateTime.new(2004,2,3,4,5,6)|
| zak	    | London		            | zubair   | zubairlon456 | DateTime.new(2003,2,3,4,5,6)|
| jaco	  | NYC		                | james    | james456     | DateTime.new(2002,2,3,4,5,6)|
| meredith| Austin	              | james    | james123     | DateTime.new(2001,2,3,4,5,6)|
When I am on the children listing page
Then I follow "Most recently created"
Then I should see the order andreas,zak,jaco,meredith
Scenario: Checking filter by All sand then ordering by Name should return all the children in alphabetical order
Given the following children exist in the system:
| name   	| last_known_location 	| reporter | unique_id    |
| andreas	| London		            | zubair   | zubairlon123 |
| zak	    | London		            | zubair   | zubairlon456 |
| jaco	  | NYC		                | james    | james456     |
| meredith| Austin	              | james    | james123     |
When I am on the children listing page
Then I follow "Most recently created"
And I follow "Name"
Then I should see the order andreas,jaco,meredith,zak
Scenario: Checking filter by Reunited returns all the reunited children in the system
Given the following children exist in the system:
| name   	| last_known_location 	| reporter | unique_id    | reunited | flag  |
| andreas	| London		            | zubair   | zubairlon123 | true     | false |
| zak	    | London		            | zubair   | zubairlon456 | false    | true  |
| jaco	  | NYC		                | james    | james456     | true     | true  |
| meredith| Austin	              | james    | james123     | false    | false |
When I follow "Reunited"
Then I should see "andreas"
And I should see "jaco"
Scenario: Checking filter by Reunited shows the Order by options
Given the following children exist in the system:
| name   	| last_known_location 	| reporter | unique_id    | reunited | flag  | flagged_at                  |
| andreas	| London		            | zubair   | zubairlon123 | true     | true  | DateTime.new(2001,2,3,4,5,6)|
| zak	    | London		            | zubair   | zubairlon456 | false    | true  | DateTime.new(2004,2,3,4,5,6)|
| jaco	  | NYC		                | james    | james456     | true     | true  | DateTime.new(2002,2,3,4,5,6)|
| meredith| Austin	              | james    | james123     | false    | true  | DateTime.new(2003,2,3,4,5,6)|
When I follow "Reunited"
Then I should see "Order by"
And I should see "Most recently reunited"
Scenario: Checking filter by Reunited should by default show the records ordered alphabetically
Given the following children exist in the system:
| name   	| last_known_location 	| reporter | unique_id    | reunited | flag  | reunited_at                 |
| andreas	| London		            | zubair   | zubairlon123 | true     | true  | DateTime.new(2001,2,3,4,5,6)|
| zak	    | London		            | zubair   | zubairlon456 | true     | false | DateTime.new(2004,2,3,4,5,6)|
| jaco	  | NYC		                | james    | james456     | true     | true  | DateTime.new(2002,2,3,4,5,6)|
| meredith| Austin	              | james    | james123     | true     | false | DateTime.new(2003,2,3,4,5,6)|
When I follow "Reunited"
Then I should see the order andreas,jaco,meredith,zak
Scenario: Checking filter by Reunited and then selecting order by most recently reunited children returns the children in the order of most recently reunited
Given the following children exist in the system:
| name   	| last_known_location 	| reporter | unique_id    | reunited | flag  | reunited_at                 |
| andreas	| London		            | zubair   | zubairlon123 | true     | true  | DateTime.new(2001,2,3,4,5,6)|
| zak	    | London		            | zubair   | zubairlon456 | true     | false | DateTime.new(2004,2,3,4,5,6)|
| jaco	  | NYC		                | james    | james456     | true     | true  | DateTime.new(2002,2,3,4,5,6)|
| meredith| Austin	              | james    | james123     | true     | false | DateTime.new(2003,2,3,4,5,6)|
When I follow "Reunited"
And I follow "Most recently reunited"
Then I should see the order zak,meredith,jaco,andreas
Scenario: Checking filter by Reunited by name should show records in alphabetical order
Given the following children exist in the system:
| name   	| last_known_location 	| reporter | unique_id    | reunited | flag  | reunited_at                 |
| andreas	| London		            | zubair   | zubairlon123 | true     | true  | DateTime.new(2001,2,3,4,5,6)|
| zak	    | London		            | zubair   | zubairlon456 | true     | false | DateTime.new(2004,2,3,4,5,6)|
| jaco	  | NYC		                | james    | james456     | true     | true  | DateTime.new(2002,2,3,4,5,6)|
| meredith| Austin	              | james    | james123     | true     | false | DateTime.new(2003,2,3,4,5,6)|
When I follow "Reunited"
And I follow "Most recently reunited"
And I follow "Name"
Then I should see the order andreas,jaco,meredith,zak
Scenario: Checking filter by Flagged returns all the flagged children in the system
Given the following children exist in the system:
| name   	| last_known_location 	| reporter | unique_id    | reunited | flag  |
| andreas	| London		            | zubair   | zubairlon123 | true     | false |
| zak	    | London		            | zubair   | zubairlon456 | false    | true  |
| jaco	  | NYC		                | james    | james456     | true     | true  |
| meredith| Austin	              | james    | james123     | false    | false |
When I follow "Flagged"
Then I should see "zak"
And I should see "jaco"
Scenario: Checking filter by Flagged shows the Order by options
Given the following children exist in the system:
| name   	| last_known_location 	| reporter | unique_id    | reunited | flag  | flagged_at                  |
| andreas	| London		            | zubair   | zubairlon123 | true     | true  | DateTime.new(2001,2,3,4,5,6)|
| zak	    | London		            | zubair   | zubairlon456 | false    | true  | DateTime.new(2004,2,3,4,5,6)|
| jaco	  | NYC		                | james    | james456     | true     | true  | DateTime.new(2002,2,3,4,5,6)|
| meredith| Austin	              | james    | james123     | false    | true  | DateTime.new(2003,2,3,4,5,6)|
When I follow "Flagged"
Then I should see "Order by"
And I should see "Most recently flagged"
Scenario: Checking filter by Flagged returns all the flagged children in the system by order of most recently flagged
Given the following children exist in the system:
| name   	| last_known_location 	| reporter | unique_id    | reunited | flag  | flagged_at                  |
| andreas	| London		            | zubair   | zubairlon123 | true     | true  | DateTime.new(2001,2,3,4,5,6)|
| zak	    | London		            | zubair   | zubairlon456 | false    | true  | DateTime.new(2004,2,3,4,5,6)|
| jaco	  | NYC		                | james    | james456     | true     | true  | DateTime.new(2002,2,3,4,5,6)|
| meredith| Austin	              | james    | james123     | false    | true  | DateTime.new(2003,2,3,4,5,6)|
When I follow "Flagged"
Then I should see the order zak,meredith,jaco,andreas
Scenario: Checking filter by Flagged and then ordering by name returns the flagged children in alphabetical order
Given the following children exist in the system:
| name   	| last_known_location 	| reporter | unique_id    | reunited | flag  | flagged_at                  |
| andreas	| London		            | zubair   | zubairlon123 | true     | true  | DateTime.new(2001,2,3,4,5,6)|
| zak	    | London		            | zubair   | zubairlon456 | false    | true  | DateTime.new(2004,2,3,4,5,6)|
| jaco	  | NYC		                | james    | james456     | true     | true  | DateTime.new(2002,2,3,4,5,6)|
| meredith| Austin	              | james    | james123     | false    | true  | DateTime.new(2003,2,3,4,5,6)|
When I follow "Flagged"
And I follow "Name"
Then I should see the order andreas,jaco,meredith,zak
Scenario: Checking filter by Flagged and ordering by most recently flagged returns the children in most recently flagged order
Given the following children exist in the system:
| name   	| last_known_location 	| reporter | unique_id    | reunited | flag  | flagged_at                  |
| andreas	| London		            | zubair   | zubairlon123 | true     | true  | DateTime.new(2001,2,3,4,5,6)|
| zak	    | London		            | zubair   | zubairlon456 | false    | true  | DateTime.new(2004,2,3,4,5,6)|
| jaco	  | NYC		                | james    | james456     | true     | true  | DateTime.new(2002,2,3,4,5,6)|
| meredith| Austin	              | james    | james123     | false    | true  | DateTime.new(2003,2,3,4,5,6)|
When I follow "Flagged"
And I follow "Name"
And I follow "Most recently flagged"
Then I should see the order zak,meredith,jaco,andreas
Scenario: Checking filter by Active returns all the children who are not reunited in the system
Given the following children exist in the system:
| name   	| last_known_location 	| reporter | unique_id    | reunited | flag  |
| andreas	| London		            | zubair   | zubairlon123 | true     | false |
| zak	    | London		            | zubair   | zubairlon456 | false    | true  |
| jaco	  | NYC		                | james    | james456     | true     | true  |
| meredith| Austin	              | james    | james123     | false    | false |
When I follow "Active"
Then I should see "zak"
And I should see "meredith"
Scenario: Checking filter by Active should by default show the records ordered alphabetically
Given the following children exist in the system:
| name   	| last_known_location 	| reporter | unique_id    | reunited | flag  |
| andreas	| London		            | zubair   | zubairlon123 | true     | false |
| zak	    | London		            | zubair   | zubairlon456 | false    | true  |
| jaco	  | NYC		                | james    | james456     | true     | true  |
| meredith| Austin	              | james    | james123     | false    | false |
When I follow "Active"
Then I should see the order meredith,zak
Scenario: Checking filter by Active shows the Order by options
Given the following children exist in the system:
| name   	| last_known_location 	| reporter | unique_id    | reunited | flag  |
| andreas	| London		            | zubair   | zubairlon123 | true     | true  |
| zak	    | London		            | zubair   | zubairlon456 | false    | true  |
| jaco	  | NYC		                | james    | james456     | true     | true  |
| meredith| Austin	              | james    | james123     | false    | true  |
When I follow "Active"
Then I should see "Order by"
And I should see "Most recently created"
Scenario: Checking filter by Active and then ordering by most recently created returns the children in the order of most recently created
Given the following children exist in the system:
| name   	| last_known_location 	| reporter | unique_id    | reunited | flag  | created_at                   |
| andreas	| London		            | zubair   | zubairlon123 | true     | true  | DateTime.new(2004,2,3,4,5,6) |
| zak	    | London		            | zubair   | zubairlon456 | false    | true  | DateTime.new(2003,2,3,4,5,6) |
| jaco	  | NYC		                | james    | james456     | false    | true  | DateTime.new(2002,2,3,4,5,6) |
| meredith| Austin	              | james    | james123     | false    | true  | DateTime.new(2001,2,3,4,5,6) |
When I follow "Active"
And I follow "Most recently created"
Then I should see the order zak,jaco,meredith
Scenario: Checking filter by Active and order by name should return the children in alphabetical order
Given the following children exist in the system:
| name   	| last_known_location 	| reporter | unique_id    | reunited | flag  |
| andreas	| London		            | zubair   | zubairlon123 | true     | false |
| zak	    | London		            | zubair   | zubairlon456 | false    | true  |
| jaco	  | NYC		                | james    | james456     | false    | true  |
| meredith| Austin	              | james    | james123     | false    | false |
When I follow "Active"
And I follow "Most recently created"
And I follow "Name"
Then I should see the order jaco,meredith,zak
