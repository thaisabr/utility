Classes: 12
[name:CONFIG, file:null, step:Given ]
[name:Deposit, file:tip4commit_tip4commit/app/models/deposit.rb, step:Given ]
[name:Deposit, file:tip4commit_tip4commit/spec/factories/deposit.rb, step:Given ]
[name:Digest, file:null, step:Given ]
[name:Project, file:tip4commit_tip4commit/app/models/project.rb, step:Given ]
[name:Project, file:tip4commit_tip4commit/spec/factories/project.rb, step:Given ]
[name:Project, file:tip4commit_tip4commit/app/models/project.rb, step:When ]
[name:Project, file:tip4commit_tip4commit/spec/factories/project.rb, step:When ]
[name:Tip, file:tip4commit_tip4commit/app/models/tip.rb, step:Then ]
[name:Tip, file:tip4commit_tip4commit/spec/factories/tip.rb, step:Then ]
[name:User, file:tip4commit_tip4commit/app/models/user.rb, step:And ]
[name:User, file:tip4commit_tip4commit/spec/factories/user.rb, step:And ]

Methods: 27
[name:add_new_commit, type:Common, file:tip4commit_tip4commit/features/step_definitions/common.rb, step:And ]
[name:amount, type:Object, file:null, step:Then ]
[name:and_return, type:Object, file:null, step:When ]
[name:be_nil, type:Object, file:null, step:Then ]
[name:create!, type:Project, file:tip4commit_tip4commit/app/models/project.rb, step:Given ]
[name:create!, type:Project, file:tip4commit_tip4commit/spec/factories/project.rb, step:Given ]
[name:create!, type:Deposit, file:tip4commit_tip4commit/app/models/deposit.rb, step:Given ]
[name:create!, type:Deposit, file:tip4commit_tip4commit/spec/factories/deposit.rb, step:Given ]
[name:deep_merge, type:Object, file:null, step:And ]
[name:deep_merge!, type:Object, file:null, step:And ]
[name:eq, type:Object, file:null, step:Then ]
[name:find_by, type:Tip, file:tip4commit_tip4commit/app/models/tip.rb, step:Then ]
[name:find_by, type:Tip, file:tip4commit_tip4commit/spec/factories/tip.rb, step:Then ]
[name:find_new_commit, type:Common, file:tip4commit_tip4commit/features/step_definitions/common.rb, step:And ]
[name:find_or_create_by, type:User, file:tip4commit_tip4commit/app/models/user.rb, step:And ]
[name:find_or_create_by, type:User, file:tip4commit_tip4commit/spec/factories/user.rb, step:And ]
[name:find_or_create_by!, type:User, file:tip4commit_tip4commit/app/models/user.rb, step:And ]
[name:find_or_create_by!, type:User, file:tip4commit_tip4commit/spec/factories/user.rb, step:And ]
[name:map, type:Object, file:null, step:When ]
[name:parameterize, type:Object, file:null, step:And ]
[name:skip_confirmation!, type:Object, file:null, step:And ]
[name:tip_commits, type:Project, file:tip4commit_tip4commit/app/models/project.rb, step:When ]
[name:tip_commits, type:Project, file:tip4commit_tip4commit/spec/factories/project.rb, step:When ]
[name:to_d, type:Object, file:null, step:Given ]
[name:to_d, type:Object, file:null, step:Then ]
[name:to_f, type:Object, file:null, step:Given ]
[name:values, type:Object, file:null, step:When ]

Referenced pages: 0

