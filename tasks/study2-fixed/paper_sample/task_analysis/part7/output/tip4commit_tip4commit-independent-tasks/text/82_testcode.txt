Then(/^there should be (\d+) email sent$/) do |arg1|
  ActionMailer::Base.deliveries.size.should eq(arg1.to_i)
end
Given(/^our fee is "(.*?)"$/) do |arg1|
  CONFIG["our_fee"] = arg1.to_f
end
Given(/^a project "(.*?)"$/) do |arg1|
  @project = Project.create!(full_name: "example/#{arg1}", github_id: Digest::SHA1.hexdigest(arg1), bitcoin_address: 'mq4NtnmQoQoPfNWEPbhSvxvncgtGo6L8WY')
end
Given(/^a user "(.*?)" has opted\-in$/) do |arg1|
  User.find_or_create_by!(nickname: arg1) do |user|
    user.nickname = arg1
    user.password = "password"
    user.email = "#{arg1.parameterize}@example.com"
    user.skip_confirmation!
  end
end
Given(/^a deposit of "(.*?)"$/) do |arg1|
  Deposit.create!(project: @project, amount: arg1.to_d * 1e8, confirmations: 2)
end
Given(/^the last known commit is "(.*?)"$/) do |arg1|
  @project.update!(last_commit: arg1)
end
Given(/^a new commit "(.*?)" with parent "([^"]*?)"$/) do |arg1, arg2|
  add_new_commit(arg1, parents: [{sha: arg2}])
end
Given(/^the message of commit "(.*?)" is "(.*?)"$/) do |arg1, arg2|
  find_new_commit(arg1).deep_merge!(commit: {message: arg2})
end
When(/^the new commits are read$/) do
  @project.reload
  @project.should_receive(:new_commits).and_return(@new_commits.values.map(&:to_ostruct))
  @project.tip_commits
end
Then(/^there should be no tip for commit "(.*?)"$/) do |arg1|
  Tip.where(commit: arg1).to_a.should eq([])
end
Then(/^there should be a tip of "(.*?)" for commit "(.*?)"$/) do |arg1, arg2|
  amount = Tip.find_by(commit: arg2).amount
  amount.should_not be_nil
  (amount.to_d / 1e8).should eq(arg1.to_d)
end
Given(/^the author of commit "(.*?)" is "(.*?)"$/) do |arg1, arg2|
  find_new_commit(arg1).deep_merge!(author: {login: arg2}, commit: {author: {email: "#{arg2}@example.com"}})
end
def add_new_commit(id, params = {})
  @new_commits ||= {}
  defaults = {
    sha: id,
    commit: {
      message: "Some changes",
      author: {
        email: "anonymous@example.com",
      },
    },
  }

  User.find_or_create_by(email: "anonymous@example.com") do |user|
    user.nickname = "anonymous"
    user.email = "anonymous@example.com"
    user.password = "password"
    user.skip_confirmation!
  end

  @new_commits[id] = defaults.deep_merge(params)
end
def find_new_commit(id)
  @new_commits[id]
end
Given(/^I'm logged in as "(.*?)"$/) do |arg1|
  email = "#{arg1.parameterize}@example.com"

  OmniAuth.config.test_mode = true
  OmniAuth.config.mock_auth[:github] = {
    "info" => {
      "nickname" => arg1,
      "primary_email" => email,
      "verified_emails" => [email],
    },
  }.to_ostruct
  visit root_path
  first(:link, "Sign in").click
  click_on "Sign in with Github"
  page.should have_content("Successfully authenticated")
end
