Classes: 9
[name:Array, file:null, step:Given ]
[name:Field, file:rapidftr_RapidFTR/app/models/field.rb, step:Given ]
[name:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:null]
[name:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:Given ]
[name:Inspector, file:rapidftr_RapidFTR/vendor/plugins/pdf-inspector/lib/pdf/inspector.rb, step:Then ]
[name:Nokogiri, file:null, step:Then ]
[name:PDF, file:null, step:Then ]
[name:Spec, file:null, step:Then ]
[name:Tempfile, file:null, step:Then ]

Methods: 49
[name:all, type:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:Given ]
[name:analyze, type:Text, file:rapidftr_RapidFTR/vendor/plugins/pdf-inspector/lib/pdf/inspector/text.rb, step:Then ]
[name:assert_contain, type:Object, file:null, step:Then ]
[name:assert_not_contain, type:Object, file:null, step:Then ]
[name:attach_file, type:Object, file:null, step:Given ]
[name:be_nil, type:Object, file:null, step:Given ]
[name:body, type:Object, file:null, step:Then ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_link, type:Object, file:null, step:Given ]
[name:click_link, type:Object, file:null, step:Then ]
[name:close, type:Object, file:null, step:Then ]
[name:contain, type:Object, file:null, step:Then ]
[name:create!, type:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:Given ]
[name:css, type:Object, file:null, step:Then ]
[name:destroy, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:Given ]
[name:destroy, type:SessionsController, file:rapidftr_RapidFTR/app/controllers/sessions_controller.rb, step:Given ]
[name:destroy, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:Given ]
[name:display_type, type:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:null]
[name:downcase, type:Object, file:null, step:Given ]
[name:download_audio, type:ChildMediaController, file:rapidftr_RapidFTR/app/controllers/child_media_controller.rb, step:null]
[name:each, type:Object, file:null, step:Given ]
[name:edit, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:null]
[name:edit_photo, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:null]
[name:export_photo_to_pdf, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:null]
[name:fields, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:get_by_unique_id, type:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:Given ]
[name:gsub, type:Object, file:null, step:Given ]
[name:hashes, type:Object, file:null, step:Given ]
[name:include, type:Object, file:null, step:Then ]
[name:index, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:null]
[name:map, type:Object, file:null, step:Then ]
[name:new, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:null]
[name:new, type:Field, file:rapidftr_RapidFTR/app/models/field.rb, step:Given ]
[name:path, type:Object, file:null, step:Then ]
[name:push, type:Object, file:null, step:Given ]
[name:response, type:Object, file:null, step:Then ]
[name:response_body, type:Object, file:null, step:Then ]
[name:reverse_merge!, type:Object, file:null, step:Given ]
[name:save, type:FormSectionController, file:rapidftr_RapidFTR/app/controllers/form_section_controller.rb, step:null]
[name:save!, type:Object, file:null, step:Given ]
[name:show, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:null]
[name:show, type:HistoriesController, file:rapidftr_RapidFTR/app/controllers/histories_controller.rb, step:null]
[name:show_resized_photo, type:ChildMediaController, file:rapidftr_RapidFTR/app/controllers/child_media_controller.rb, step:null]
[name:strings, type:Object, file:null, step:Then ]
[name:to_i, type:Object, file:null, step:Given ]
[name:write, type:Object, file:null, step:Then ]

Referenced pages: 35
rapidftr_RapidFTR/app/views/children/_audio_player.html.erb
rapidftr_RapidFTR/app/views/children/_audio_upload_box.html.erb
rapidftr_RapidFTR/app/views/children/_check_boxes.html.erb
rapidftr_RapidFTR/app/views/children/_date_field.html.erb
rapidftr_RapidFTR/app/views/children/_field_display_audio.html.erb
rapidftr_RapidFTR/app/views/children/_field_display_basic.html.erb
rapidftr_RapidFTR/app/views/children/_field_display_photo.html.erb
rapidftr_RapidFTR/app/views/children/_form_section.html.erb
rapidftr_RapidFTR/app/views/children/_form_section_info.html.erb
rapidftr_RapidFTR/app/views/children/_mark_as.html.erb
rapidftr_RapidFTR/app/views/children/_numeric_field.html.erb
rapidftr_RapidFTR/app/views/children/_photo_upload_box.html.erb
rapidftr_RapidFTR/app/views/children/_picture.html.erb
rapidftr_RapidFTR/app/views/children/_radio_button.html.erb
rapidftr_RapidFTR/app/views/children/_repeatable_text_field.html.erb
rapidftr_RapidFTR/app/views/children/_search_results.html.erb
rapidftr_RapidFTR/app/views/children/_select_box.html.erb
rapidftr_RapidFTR/app/views/children/_show_child_toolbar.erb
rapidftr_RapidFTR/app/views/children/_show_form_section.html.erb
rapidftr_RapidFTR/app/views/children/_summary_row.html.erb
rapidftr_RapidFTR/app/views/children/_tabs.html.erb
rapidftr_RapidFTR/app/views/children/_text_field.html.erb
rapidftr_RapidFTR/app/views/children/_textarea.html.erb
rapidftr_RapidFTR/app/views/children/edit.html.erb
rapidftr_RapidFTR/app/views/children/edit_photo.html.erb
rapidftr_RapidFTR/app/views/children/index.html.erb
rapidftr_RapidFTR/app/views/children/new.html.erb
rapidftr_RapidFTR/app/views/children/search.html.erb
rapidftr_RapidFTR/app/views/children/show.html.erb
rapidftr_RapidFTR/app/views/histories/_audio_history_change.html.erb
rapidftr_RapidFTR/app/views/histories/_flag_change.erb
rapidftr_RapidFTR/app/views/histories/_history_change.html.erb
rapidftr_RapidFTR/app/views/histories/_photo_history_change.html.erb
rapidftr_RapidFTR/app/views/histories/_reunited_change.erb
rapidftr_RapidFTR/app/views/histories/show.html.erb

