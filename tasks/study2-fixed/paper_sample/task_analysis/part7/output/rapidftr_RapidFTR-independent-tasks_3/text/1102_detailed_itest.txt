Classes: 4
[name:Child, file:rapidftr_RapidFTR/app/models/child.rb, step:Given ]
[name:Spec, file:null, step:Then ]
[name:Time, file:null, step:Given ]
[name:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Given ]

Methods: 16
[name:and_return, type:Object, file:null, step:Given ]
[name:assert_contain, type:Object, file:null, step:Then ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:contain, type:Object, file:null, step:Then ]
[name:create!, type:Object, file:null, step:Given ]
[name:delete, type:Object, file:null, step:Given ]
[name:each, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:first, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Given ]
[name:hashes, type:Object, file:null, step:Given ]
[name:new_with_user_name, type:Child, file:rapidftr_RapidFTR/app/models/child.rb, step:Given ]
[name:response, type:Object, file:null, step:Then ]
[name:reverse_merge!, type:Object, file:null, step:Given ]
[name:uploadable_photo, type:Object, file:null, step:Given ]
[name:user_name, type:Session, file:rapidftr_RapidFTR/app/models/session.rb, step:Given ]

Referenced pages: 0

