Classes: 3
[name:Child, file:rapidftr_RapidFTR/app/models/child.rb, step:Then ]
[name:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:null]
[name:Spec, file:null, step:Then ]

Methods: 29
[name:all, type:Child, file:rapidftr_RapidFTR/app/models/child.rb, step:Then ]
[name:assert_contain, type:Object, file:null, step:Then ]
[name:attach_file, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:Given ]
[name:contain, type:Object, file:null, step:Then ]
[name:download_audio, type:ChildMediaController, file:rapidftr_RapidFTR/app/controllers/child_media_controller.rb, step:null]
[name:edit, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:null]
[name:edit_photo, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:null]
[name:export_photo_to_pdf, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:null]
[name:fill_in, type:Object, file:null, step:Given ]
[name:find_child_by_name, type:Object, file:null, step:When ]
[name:flag, type:Object, file:null, step:null]
[name:flag_message, type:Child, file:rapidftr_RapidFTR/app/models/child.rb, step:null]
[name:have_selector, type:Object, file:null, step:Then ]
[name:id, type:Searchable, file:rapidftr_RapidFTR/app/models/searchable.rb, step:When ]
[name:id, type:Searchable, file:rapidftr_RapidFTR/app/models/searchable.rb, step:Then ]
[name:index, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:null]
[name:name, type:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:null]
[name:new, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:null]
[name:request, type:Object, file:null, step:Then ]
[name:response, type:Object, file:null, step:Then ]
[name:response_body, type:Object, file:null, step:Then ]
[name:save, type:FormSectionController, file:rapidftr_RapidFTR/app/controllers/form_section_controller.rb, step:null]
[name:show, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:null]
[name:show, type:HistoriesController, file:rapidftr_RapidFTR/app/controllers/histories_controller.rb, step:null]
[name:show_resized_photo, type:ChildMediaController, file:rapidftr_RapidFTR/app/controllers/child_media_controller.rb, step:null]
[name:split, type:Object, file:null, step:Given ]
[name:unflag, type:Object, file:null, step:null]
[name:url, type:Object, file:null, step:Then ]

Referenced pages: 30
rapidftr_RapidFTR/app/views/children/_audio_player.html.erb
rapidftr_RapidFTR/app/views/children/_audio_upload_box.html.erb
rapidftr_RapidFTR/app/views/children/_check_box.html.erb
rapidftr_RapidFTR/app/views/children/_date_field.html.erb
rapidftr_RapidFTR/app/views/children/_form_section.html.erb
rapidftr_RapidFTR/app/views/children/_form_section_info.html.erb
rapidftr_RapidFTR/app/views/children/_numeric_field.html.erb
rapidftr_RapidFTR/app/views/children/_photo_upload_box.html.erb
rapidftr_RapidFTR/app/views/children/_picture.html.erb
rapidftr_RapidFTR/app/views/children/_radio_button.html.erb
rapidftr_RapidFTR/app/views/children/_repeatable_text_field.html.erb
rapidftr_RapidFTR/app/views/children/_search_results.html.erb
rapidftr_RapidFTR/app/views/children/_select_box.html.erb
rapidftr_RapidFTR/app/views/children/_show_child_toolbar.erb
rapidftr_RapidFTR/app/views/children/_show_form_section.html.erb
rapidftr_RapidFTR/app/views/children/_summary_row.html.erb
rapidftr_RapidFTR/app/views/children/_tabs.html.erb
rapidftr_RapidFTR/app/views/children/_text_field.html.erb
rapidftr_RapidFTR/app/views/children/_textarea.html.erb
rapidftr_RapidFTR/app/views/children/edit.html.erb
rapidftr_RapidFTR/app/views/children/edit_photo.html.erb
rapidftr_RapidFTR/app/views/children/index.html.erb
rapidftr_RapidFTR/app/views/children/new.html.erb
rapidftr_RapidFTR/app/views/children/search.html.erb
rapidftr_RapidFTR/app/views/children/show.html.erb
rapidftr_RapidFTR/app/views/histories/_audio_history_change.html.erb
rapidftr_RapidFTR/app/views/histories/_flag_change.erb
rapidftr_RapidFTR/app/views/histories/_history_change.html.erb
rapidftr_RapidFTR/app/views/histories/_photo_history_change.html.erb
rapidftr_RapidFTR/app/views/histories/show.html.erb

