Classes: 6
[name:Child, file:rapidftr_RapidFTR/app/models/child.rb, step:Then ]
[name:Device, file:rapidftr_RapidFTR/app/models/device.rb, step:Given ]
[name:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:Given ]
[name:Spec, file:null, step:Then ]
[name:Spec, file:null, step:Given ]
[name:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Given ]

Methods: 36
[name:Hpricot, type:Object, file:null, step:Then ]
[name:assert_contain, type:Object, file:null, step:Then ]
[name:assert_contain, type:Object, file:null, step:Given ]
[name:at, type:Object, file:null, step:Then ]
[name:attach_file, type:Object, file:null, step:Given ]
[name:be_empty, type:Object, file:null, step:Then ]
[name:body, type:Object, file:null, step:Then ]
[name:check, type:Object, file:null, step:When ]
[name:check_link, type:PhotoThumbnailSteps, file:rapidftr_RapidFTR/features/step_definitions/photo_thumbnail_steps.rb, step:Then ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:Given ]
[name:contain, type:Object, file:null, step:Then ]
[name:contain, type:Object, file:null, step:Given ]
[name:create_new_custom, type:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:Given ]
[name:each, type:Object, file:null, step:Given ]
[name:edit, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:null]
[name:field_by_xpath, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:find_child_by_name, type:Object, file:null, step:Then ]
[name:first, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Given ]
[name:first, type:Object, file:null, step:Then ]
[name:hashes, type:Object, file:null, step:Given ]
[name:have_selector, type:Object, file:null, step:Then ]
[name:inner_html, type:Object, file:null, step:Then ]
[name:new, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Given ]
[name:new, type:Device, file:rapidftr_RapidFTR/app/models/device.rb, step:Given ]
[name:response, type:Object, file:null, step:Then ]
[name:response, type:Object, file:null, step:Given ]
[name:save!, type:Object, file:null, step:Given ]
[name:search, type:Child, file:rapidftr_RapidFTR/app/models/child.rb, step:Then ]
[name:search, type:SearchService, file:rapidftr_RapidFTR/app/models/search_service.rb, step:Then ]
[name:show_resized_photo, type:ChildMediaController, file:rapidftr_RapidFTR/app/controllers/child_media_controller.rb, step:Then ]
[name:split, type:Object, file:null, step:Given ]
[name:user_name, type:Session, file:rapidftr_RapidFTR/app/models/session.rb, step:Given ]

Referenced pages: 4
rapidftr_RapidFTR/app/views/users/_devices.html.erb
rapidftr_RapidFTR/app/views/users/_edittable_user.html.erb
rapidftr_RapidFTR/app/views/users/_mobile_login_history.html.erb
rapidftr_RapidFTR/app/views/users/edit.html.erb

