Feature: As an user, I should be able to log in.
Scenario: I should see the Contact & Help page even when I'm not logged in
Given the following admin contact info:
| key | value |
| name | John Smith |
| id | administrator |
And I am on the login page
Then I should see "Contact & Help"
When I follow "Contact & Help"
Then I should be on the administrator contact page
