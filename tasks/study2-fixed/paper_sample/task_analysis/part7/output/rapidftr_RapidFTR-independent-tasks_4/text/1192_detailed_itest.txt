Classes: 4
[name:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:null]
[name:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Given ]
[name:User, file:rapidftr_RapidFTR/app/models/user.rb, step:null]
[name:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:null]

Methods: 20
[name:click_link, type:Object, file:null, step:When ]
[name:download_audio, type:ChildMediaController, file:rapidftr_RapidFTR/app/controllers/child_media_controller.rb, step:null]
[name:edit, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:null]
[name:edit, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:null]
[name:first, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Given ]
[name:full_name, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:null]
[name:index, type:HomeController, file:rapidftr_RapidFTR/app/controllers/home_controller.rb, step:null]
[name:index, type:AdminController, file:rapidftr_RapidFTR/app/controllers/admin_controller.rb, step:null]
[name:index, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:null]
[name:index, type:FormSectionController, file:rapidftr_RapidFTR/app/controllers/form_section_controller.rb, step:null]
[name:index, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:null]
[name:name, type:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:null]
[name:new, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:null]
[name:new, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:null]
[name:save, type:FormSectionController, file:rapidftr_RapidFTR/app/controllers/form_section_controller.rb, step:null]
[name:show, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:null]
[name:show, type:HistoriesController, file:rapidftr_RapidFTR/app/controllers/histories_controller.rb, step:null]
[name:url_for, type:Object, file:null, step:null]
[name:user_name, type:Session, file:rapidftr_RapidFTR/app/models/session.rb, step:Given ]
[name:user_name, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:null]

Referenced pages: 19
rapidftr_RapidFTR/app/views/admin/index.html.erb
rapidftr_RapidFTR/app/views/children/_form_section.html.erb
rapidftr_RapidFTR/app/views/children/_tabs.html.erb
rapidftr_RapidFTR/app/views/children/edit.html.erb
rapidftr_RapidFTR/app/views/children/edit_photo.html.erb
rapidftr_RapidFTR/app/views/children/index.html.erb
rapidftr_RapidFTR/app/views/children/new.html.erb
rapidftr_RapidFTR/app/views/children/show.html.erb
rapidftr_RapidFTR/app/views/form_section/index.html.erb
rapidftr_RapidFTR/app/views/histories/_audio_history_change.html.erb
rapidftr_RapidFTR/app/views/histories/_history_change.html.erb
rapidftr_RapidFTR/app/views/histories/_photo_history_change.html.erb
rapidftr_RapidFTR/app/views/histories/show.html.erb
rapidftr_RapidFTR/app/views/home/index.html.erb
rapidftr_RapidFTR/app/views/users/_edittable_user.html.erb
rapidftr_RapidFTR/app/views/users/_mobile_login_history.html.erb
rapidftr_RapidFTR/app/views/users/edit.html.erb
rapidftr_RapidFTR/app/views/users/index.html.erb
rapidftr_RapidFTR/app/views/users/new.html.erb

