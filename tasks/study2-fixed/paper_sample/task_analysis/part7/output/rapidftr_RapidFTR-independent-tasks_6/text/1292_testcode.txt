Given /^an? (user|admin) "([^\"]*)" with(?: a)? password "([^\"]*)"$/ do |user_type, username, password|
  user_type = user_type == 'user' ? 'User' : 'Administrator'
  @user = User.new(
    :user_name=>username, 
    :password=>password, 
    :password_confirmation=>password, 
    :user_type=> user_type, 
    :full_name=>username, 
    :email=>"#{username}@test.com")
  @user.save!
end
Given /^an? (user|admin) "([^"]+)"$/ do |user_type, user_name|
  Given %(a #{user_type} "#{user_name}" with password "123")
end
Given /^I am logged in as "(.+)"/ do |user_name|
  session = Session.for_user(User.find_by_user_name(user_name))
  session.save!
  session.put_in_cookie cookies
end
Given /^user "(.+)" is disabled$/ do |username|
  user = User.find_by_user_name(username)
  user.disabled = true
  user.save!
end
Then /^user "(.+)" should be disabled$/ do |username|
  User.find_by_user_name(username).should be_disabled
end
Then /^user "(.+)" should not be disabled$/ do |username|
  User.find_by_user_name(username).should_not be_disabled
end
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^\"]*)"$/ do |button|
  click_button(button)
end
When /^(?:|I )follow "([^\"]*)"$/ do |link|
  click_link(link)
end
When /^(?:|I )fill in "([^\"]*)" for "([^\"]*)"$/ do |value, field|
  fill_in(field, :with => value)
end
When /^(?:|I )check "([^\"]*)"$/ do |field|
  check(field)
end
When /^(?:|I )uncheck "([^\"]*)"$/ do |field|
  uncheck(field)
end
  def path_to(page_name, options = {})

    format = page_name[/^(?:|the )(\w+) formatted/,1]
    options.reverse_merge!( :format => format )

    case page_name

      when /the home\s?page/
        '/'
      when /the new assign_unique_id_to_a_child page/
        new_assign_unique_id_to_a_child_path(options)


      when /new child page/
        new_child_path(options)

      when /children listing page/
        children_path(options)

      when /saved record page for child with name "(.+)"/
        child_name = $1
        child = Summary.by_name(:key => child_name)
        raise "no child named '#{child_name}'" if child.nil?
        child_path( child, options )

      when /new user page/
        new_user_path(options)

      when /manage users page/
        users_path(options)

      when /edit user page for "(.+)"/
        user = User.find_by_user_name($1)
        edit_user_path(user, options)

      when /child search page/
        search_children_path(options)
        
      when /login page/
        login_path(options)
        
      when /logout page/
        logout_path(options)
        
      when /child search results page/
        search_children_path(options)



      when /photo resource for child with name "(.+)"/
        child_name = $1
        child = Summary.by_name(:key => child_name)
        raise "no child named '#{child_name}'" if child.nil?
        child_path( child, :format => 'jpg' )


      # Add more mappings here.
      # Here is an example that pulls values out of the Regexp:
      #
      #   when /^(.*)'s profile page$/i
      #     user_profile_path(User.find_by_login($1))

      else
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
                "Now, go and add a mapping in #{__FILE__}"
    end
  end
Then /^I should have received a "(.+)" status code$/ do |status_code|
  response.status.should == status_code
end
Given /^I am logged in$/ do
  Given "there is a User"
  Given "I am on the login page"
  Given "I fill in \"#{User.first.user_name}\" for \"user name\""
  Given "I fill in \"#{User.first.password}\" for \"password\""
  Given "I press \"Log In\""
end
