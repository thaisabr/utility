Classes: 13
[name:Child, file:rapidftr_RapidFTR/app/models/child.rb, step:Then ]
[name:Child, file:rapidftr_RapidFTR/app/models/child.rb, step:Given ]
[name:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:Then ]
[name:Options, file:rapidftr_RapidFTR/vendor/plugins/rspec/lib/spec/runner/options.rb, step:Then ]
[name:Session, file:rapidftr_RapidFTR/app/models/session.rb, step:Given ]
[name:Spec, file:rapidftr_RapidFTR/vendor/plugins/rspec/lib/spec.rb, step:Then ]
[name:Summary, file:rapidftr_RapidFTR/app/models/summary.rb, step:Then ]
[name:Time, file:null, step:When ]
[name:URI, file:null, step:Then ]
[name:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Given ]
[name:User, file:rapidftr_RapidFTR/vendor/gems/couchrest-0.34/spec/fixtures/more/user.rb, step:Given ]
[name:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Then ]
[name:User, file:rapidftr_RapidFTR/vendor/gems/couchrest-0.34/spec/fixtures/more/user.rb, step:Then ]

Methods: 59
[name:all, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Given ]
[name:all, type:User, file:rapidftr_RapidFTR/vendor/gems/couchrest-0.34/spec/fixtures/more/user.rb, step:Given ]
[name:all, type:Child, file:rapidftr_RapidFTR/app/models/child.rb, step:Given ]
[name:and_return, type:Object, file:null, step:When ]
[name:assert_contain, type:Object, file:null, step:Then ]
[name:assert_equal, type:Object, file:null, step:Then ]
[name:attach_file, type:Object, file:null, step:When ]
[name:attach_file, type:Object, file:null, step:Given ]
[name:by_name, type:Summary, file:rapidftr_RapidFTR/app/models/summary.rb, step:Then ]
[name:choose, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_link, type:Object, file:null, step:Given ]
[name:click_link, type:Object, file:null, step:When ]
[name:compact, type:Object, file:null, step:Then ]
[name:contain, type:Object, file:null, step:Then ]
[name:content_type, type:Object, file:null, step:Then ]
[name:cookies, type:Object, file:null, step:Then ]
[name:current_url, type:Object, file:null, step:Then ]
[name:current_url_with_format_of, type:ApplicationHelper, file:rapidftr_RapidFTR/app/helpers/application_helper.rb, step:Then ]
[name:destroy, type:SessionsController, file:rapidftr_RapidFTR/app/controllers/sessions_controller.rb, step:Then ]
[name:destroy, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:Then ]
[name:destroy, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:Given ]
[name:destroy, type:SessionsController, file:rapidftr_RapidFTR/app/controllers/sessions_controller.rb, step:Given ]
[name:destroy, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:Given ]
[name:each, type:Object, file:null, step:Given ]
[name:edit, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:Then ]
[name:edit, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:Then ]
[name:fill_in, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:for_user, type:Session, file:rapidftr_RapidFTR/app/models/session.rb, step:Given ]
[name:full_name, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Then ]
[name:full_name, type:User, file:rapidftr_RapidFTR/vendor/gems/couchrest-0.34/spec/fixtures/more/user.rb, step:Then ]
[name:header, type:Object, file:null, step:Given ]
[name:id, type:Object, file:null, step:Given ]
[name:index, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:Then ]
[name:index, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:Then ]
[name:inspect, type:Object, file:null, step:Then ]
[name:join, type:Object, file:null, step:Then ]
[name:new, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:Then ]
[name:new, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:Then ]
[name:new, type:SessionsController, file:rapidftr_RapidFTR/app/controllers/sessions_controller.rb, step:Then ]
[name:new, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Given ]
[name:new, type:User, file:rapidftr_RapidFTR/vendor/gems/couchrest-0.34/spec/fixtures/more/user.rb, step:Given ]
[name:path_to, type:Paths, file:rapidftr_RapidFTR/features/support/paths.rb, step:Then ]
[name:raise, type:Object, file:null, step:Given ]
[name:raise, type:Object, file:null, step:Then ]
[name:response, type:Object, file:null, step:Then ]
[name:save!, type:Object, file:null, step:Given ]
[name:search, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:Then ]
[name:select, type:Object, file:null, step:When ]
[name:select, type:Object, file:null, step:Then ]
[name:show, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:Then ]
[name:show, type:HistoriesController, file:rapidftr_RapidFTR/app/controllers/histories_controller.rb, step:Then ]
[name:show, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:Then ]
[name:split, type:Object, file:null, step:When ]
[name:status, type:Object, file:null, step:Then ]
[name:user_name, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Then ]
[name:user_name, type:User, file:rapidftr_RapidFTR/vendor/gems/couchrest-0.34/spec/fixtures/more/user.rb, step:Then ]

Referenced pages: 17
rapidftr_RapidFTR/app/views/children/_form_section.html.erb
rapidftr_RapidFTR/app/views/children/_summary_row.html.erb
rapidftr_RapidFTR/app/views/children/_tabs.html.erb
rapidftr_RapidFTR/app/views/children/edit.html.erb
rapidftr_RapidFTR/app/views/children/index.html.erb
rapidftr_RapidFTR/app/views/children/new.html.erb
rapidftr_RapidFTR/app/views/children/search.html.erb
rapidftr_RapidFTR/app/views/children/show.html.erb
rapidftr_RapidFTR/app/views/histories/_photo_record.html.erb
rapidftr_RapidFTR/app/views/histories/show.html.erb
rapidftr_RapidFTR/app/views/sessions/new.html.erb
rapidftr_RapidFTR/app/views/sessions/show.html.erb
rapidftr_RapidFTR/app/views/users/_edittable_user.html.erb
rapidftr_RapidFTR/app/views/users/edit.html.erb
rapidftr_RapidFTR/app/views/users/index.html.erb
rapidftr_RapidFTR/app/views/users/new.html.erb
rapidftr_RapidFTR/app/views/users/show.html.erb

