When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    expect(page).to have_content(text)
  end
end
Then /^(?:|I )should not see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    expect(page).to have_no_content(text)
  end
end
Then /^(?:|I )should be on (.+)$/ do |page_name|
  current_path = URI.parse(current_url).path
  expect(current_path).to eq(path_to(page_name))
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)
    options = {}
    options[:format] = :m if @mobile_interface 
    options[:locale] = @locale if @locale
    options[:_group_view_by] = @group_view_by if @group_view_by
    @source_view = nil
    
    case page_name

    when /the home\s?page/
      @source_view = "todos"
      root_path(options)

    when /the done page/
      @source_view = "done"
      done_overview_path(options)
    when /the done actions page for context "([^"]*)"/i
      @source_view = "done"
      context = @current_user.contexts.where(:name => $1).first
      done_todos_context_path(context, options)
    when /the done actions page for project "([^"]*)"/i
      @source_view = "done"
      project = @current_user.projects.where(:name => $1).first
      done_todos_project_path(project, options)
    when /the done actions page for tag "([^"]*)"/i
      @source_view = "done"
      done_tag_path($1, options)
    when /the done actions page/
      @source_view = "done"
      done_todos_path(options)
    when /the all done actions page for context "([^"]*)"/i
      @source_view = "done"
      context = @current_user.contexts.where(:name => $1).first
      all_done_todos_context_path(context, options)
    when /the all done actions page for project "([^"]*)"/i
      @source_view = "done"
      project = @current_user.projects.where(:name => $1).first
      all_done_todos_project_path(project, options)
    when /the all done actions page for tag "([^"]*)"/i
      @source_view = "done"
      all_done_tag_path($1, options)
    when /the all done actions page/
      @source_view = "done"
      all_done_todos_path(options)

    when /the statistics page/
      @source_view = "stats"
      stats_path(options)
    when /the signup page/
      signup_path(options)
    when /the login page/
      login_path(options)
    when /the logout page/
      logout_path(options)
    when /the notes page/
      notes_path(options)
    when /the calendar page/
      calendar_path(options)
    when /the review page/
      @source_view = "review"
      review_path(options)
    when /the contexts page/
      @source_view = "context"
      contexts_path(options)
    when /the projects page/
      @source_view = "project"
      projects_path(options)
    when /the manage users page/
      users_path(options)
    when /the recurring todos page/
      recurring_todos_path(options)
    when /the integrations page/
      integrations_path(options)
    when /the tickler page/
      @source_view = "deferred"
      tickler_path(options)
    when /the export page/
      data_path(options)
    when /the preference page/
      preferences_path(options)
    when /the rest api docs page/
      rest_api_docs_path(options)
    when /the search page/
      search_path(options)
    when /the starred page/
      tag_path("starred", options)
    when /the feeds page/
      feeds_path(options)
    when /the context page for "([^\"]*)" for user "([^\"]*)"/i
      @source_view = "context"
      @context = User.where(:login => $2).first.contexts.where(:name => $1).first
      context_path(@context, options)
    when /the context page for "([^\"]*)"/i
      @source_view = "context"
      @context = @current_user.contexts.where(:name => $1).first
      context_path(@context, options)
    when /the "([^\"]*)" context/i
      @source_view = "context"
      @context = @current_user.contexts.where(:name => $1).first
      context_path(@context, options)
    when /the "([^\"]*)" project for user "([^\"]*)"/i
      @source_view = "project"
      @project = User.where(:login => $2).first.projects.where(:name => $1).first
      project_path(@project, options)
    when /the "([^\"]*)" project/i
      @source_view = "project"
      @project = @current_user.projects.where(:name => $1).first
      project_path(@project, options)
    when /the tag page for "([^"]*)"/i
      @source_view = "tag"
      tag_path($1, options)
    when /the change password page/
      change_password_user_path @current_user
      
    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.where(:login => $1))first.

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
When /^I submit a new action with description "([^"]*)" in the context "([^"]*)"$/ do |description, context_name|
  within "form#todo-form-new-action" do
    fill_in "todo[description]", :with => description

    clear_context_name_from_next_action_form
    fill_in "todo_context_name", :with => context_name
  end

  submit_next_action_form
end
  def submit_next_action_form
    submit_form("//form[@id='todo-form-new-action']", "todo_new_action_submit")
  end
  def clear_context_name_from_next_action_form
    execute_javascript("$('#todo_context_name').val('');")
  end
  def submit_form(form_xpath, button_name)
    handle_js_confirm do
      # on calendar page there can be more than 1 occurance of a todo, so we select the first here
      within all(:xpath, form_xpath)[0] do
        click_button(button_name)
      end
      wait_for_ajax
      wait_for_animations_to_end
    end
  end
  def execute_javascript(js)
    page.execute_script(js)
  end
  def wait_for_animations_to_end
    wait_until do
      page.evaluate_script('$(":animated").length').zero?
    end
  end
  def wait_for_ajax
    wait_until do
      page.evaluate_script('jQuery.active').zero?
    end
  end
  def handle_js_confirm(accept=true)
    execute_javascript "window.original_confirm_function = window.confirm"
    execute_javascript "window.confirmMsg = null"
    execute_javascript "window.confirm = function(msg) { window.confirmMsg = msg; return #{!!accept}; }"
    yield
  ensure
    execute_javascript "window.confirm = window.original_confirm_function"
  end
  def wait_until(wait_time = Capybara.default_wait_time)
    Timeout.timeout(wait_time) do
      loop until yield
    end
  end
When /^I defer "([^"]*)" for 1 day$/ do |action_description|
  todo = @current_user.todos.where(:description => action_description).first
  expect(todo).to_not be_nil

  open_submenu_for(todo) do
    click_link "defer_1_todo_#{todo.id}"
  end

  wait_for_ajax
  wait_for_animations_to_end
end
When /^I make a project of "([^"]*)"$/ do |action_description|
  todo = @current_user.todos.where(:description => action_description).first
  expect(todo).to_not be_nil

  open_submenu_for(todo) do
    click_link "to_project_todo_#{todo.id}"
  end

  expect(page).to have_no_css("div#line_todo_#{todo.id}")
  wait_for_ajax
  wait_for_animations_to_end
end
  def wait_for_animations_to_end
    wait_until do
      page.evaluate_script('$(":animated").length').zero?
    end
  end
  def wait_for_ajax
    wait_until do
      page.evaluate_script('jQuery.active').zero?
    end
  end
  def open_submenu_for(todo)
    wait_for_animations_to_end

    submenu_css = "#ultodo_#{todo.id}"

    execute_javascript "$('#{submenu_css}').parent().showSuperfishUl()"

    expect(page).to have_css(submenu_css, visible: true)
    submenu = page.first(submenu_css, visible: true)

    within submenu do
      yield
    end
  end
  def wait_until(wait_time = Capybara.default_wait_time)
    Timeout.timeout(wait_time) do
      loop until yield
    end
  end
  def execute_javascript(js)
    page.execute_script(js)
  end
Given /^the following user records?$/ do |table|
  User.delete_all
  table.hashes.each do |hash|
    user = FactoryGirl.create(:user, hash)
    user.create_preference({:locale => 'en'})
  end
end
Given /^I have logged in as "(.*)" with password "(.*)"$/ do |username, password|
  user = User.where(:login => username).first
  request_signin_as(user)
  @current_user = user
end
  def request_signin_as(user)
    visit "/test_login_backdoor?user_id=#{user.id}"
  end
Given /^I have a context called "([^\"]*)"$/ do |context_name|
  step "there exists an active context called \"#{context_name}\" for user \"#{@current_user.login}\""
end
Given /^there exists (an active|a hidden|a closed) context called "([^"]*)" for user "([^"]*)"$/ do |state, context_name, login|
  user = User.where(:login => login).first
  expect(user).to_not be_nil
  context_state = {"an active" => "active", "a hidden" => "hidden", "a closed" => "closed"}[state]
  @context = user.contexts.where(:name => context_name, :state => context_state).first_or_create
end
