When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
Then /^(?:|I )should see "([^"]*)"$/ do |text|
  if response.respond_to? :should
    response.should contain(text)
  else
    assert_contain text
  end
end
  def path_to(page_name)
    options = @mobile_interface ? {:format => :m} : {}
    options = {:locale => @locale}.merge(options) if @locale
    @source_view = nil

    case page_name

    when /the home\s?page/
      @source_view = "todos"
      root_path(options)

    when /the done page/
      @source_view = "done"
      done_overview_path(options)
    when /the done actions page for context "([^"]*)"/i
      @source_view = "done"
      context = @current_user.contexts.find_by_name($1)
      done_todos_context_path(context, options)
    when /the done actions page for project "([^"]*)"/i
      @source_view = "done"
      project = @current_user.projects.find_by_name($1)
      done_todos_project_path(project, options)
    when /the done actions page for tag "([^"]*)"/i
      @source_view = "done"
      done_tag_path($1, options)
    when /the done actions page/
      @source_view = "done"
      done_todos_path(options)
    when /the all done actions page for context "([^"]*)"/i
      @source_view = "done"
      context = @current_user.contexts.find_by_name($1)
      all_done_todos_context_path(context, options)
    when /the all done actions page for project "([^"]*)"/i
      @source_view = "done"
      project = @current_user.projects.find_by_name($1)
      all_done_todos_project_path(project, options)
    when /the all done actions page for tag "([^"]*)"/i
      @source_view = "done"
      all_done_tag_path($1, options)
    when /the all done actions page/
      @source_view = "done"
      all_done_todos_path(options)

    when /the statistics page/
      @source_view = "stats"
      stats_path(options)
    when /the signup page/
      signup_path(options)
    when /the login page/
      login_path(options)
    when /the logout page/
      logout_path(options)
    when /the notes page/
      notes_path(options)
    when /the calendar page/
      calendar_path(options)
    when /the contexts page/
      @source_view = "contexts"
      contexts_path(options)
    when /the projects page/
      @source_view = "projects"
      projects_path(options)
    when /the manage users page/
      users_path(options)
    when /the repeating todos page/
      recurring_todos_path(options)
    when /the integrations page/
      integrations_path(options)
    when /the tickler page/
      tickler_path(options)
    when /the export page/
      data_path(options)
    when /the preference page/
      preferences_path(options)
    when /the rest api docs page/
      rest_api_docs_path(options)
    when /the search page/
      search_path(options)
    when /the starred page/
      tag_path("starred", options)
    when /the feeds page/
      feeds_path(options)
    when /the context page for "([^\"]*)" for user "([^\"]*)"/i
      @source_view = "context"
      context_path(User.find_by_login($2).contexts.find_by_name($1), options)
    when /the context page for "([^\"]*)"/i
      @source_view = "context"
      context_path(@current_user.contexts.find_by_name($1), options)
    when /the "([^\"]*)" project for user "([^\"]*)"/i
      @source_view = "project"
      project_path(User.find_by_login($2).projects.find_by_name($1), options)
    when /the "([^\"]*)" project/i
      @source_view = "project"
      @project = @current_user.projects.find_by_name($1)
      project_path(@project, options)
    when /the tag page for "([^"]*)"/i
      @source_view = "tag"
      tag_path($1, options)

      # Add more mappings here.
      # Here is an example that pulls values out of the Regexp:
      #
      #   when /^(.*)'s profile page$/i
      #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
When /^I edit my last name to "([^"]*)"$/ do |last_name|
  fill_in "user[last_name]", :with => last_name
  click_button "prefs_submit"
end
When /^I set the password and confirmation to "([^"]*)"$/ do |new_password|
  When "I set the password to \"#{new_password}\" and confirmation to \"#{new_password}\""
end
When /^I set the password to "([^"]*)" and confirmation to "([^"]*)"$/ do |new_password, new_password_confirmation|
  fill_in "user[password]", :with => new_password
  fill_in "user[password_confirmation]", :with => new_password_confirmation
  click_button "prefs_submit"
end
Given /^I have logged in as "(.*)" with password "(.*)"$/ do |username, password|
  When "I go to the login page"
  fill_in "Login", :with => username
  fill_in "Password", :with => password
  uncheck "Stay logged in:"
  click_button
  if response.respond_to? :selenium
    selenium.wait_for_page_to_load(5000)
  end
  logout_regexp = @mobile_interface ? "Logout" : "Logout \(#{username}\)"
  response.should contain(logout_regexp)
  @current_user = User.find_by_login(username)
end
When /^I submit the login form as user "([^\"]*)" with password "([^\"]*)"$/ do |username, password|
  fill_in 'Login', :with => username
  fill_in 'Password', :with => password
  uncheck "Stay logged in:"
  click_button
end
When /^my session expires$/ do
  selenium.wait_for_page_to_load(5000)

  # use expire_session to force expiry of session
  js = '$.ajax({type: "GET", url: "/login/expire_session", dataType: "script", async: false});'
  selenium.run_script(js);

  # force check of expiry bypassing timeout
  js = '$.ajax({type: "GET", url: "/login/check_expiry", dataType: "script", async: false});'
  selenium.run_script(js);

  sleep(2)
end
When /^I log out of Tracks$/ do
  When "I go to the logout page"
end
Given /^the following user records?$/ do |table|
  User.delete_all
  table.hashes.each do |hash|
    user = Factory(:user, hash)
    user.create_preference({:locale => 'en'})
  end
end
