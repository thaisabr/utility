Classes: 16
[name:Capybara, file:null, step:When ]
[name:Context, file:TracksApp_tracks/app/models/context.rb, step:Then ]
[name:Context, file:TracksApp_tracks/app/models/context.rb, step:Given ]
[name:FactoryGirl, file:null, step:Given ]
[name:Object, file:null, step:Then ]
[name:Project, file:TracksApp_tracks/app/models/project.rb, step:Then ]
[name:SourceView, file:TracksApp_tracks/lib/tracks/source_view.rb, step:Then ]
[name:Timeout, file:null, step:When ]
[name:Todo, file:TracksApp_tracks/app/models/todo.rb, step:When ]
[name:URI, file:null, step:Then ]
[name:User, file:TracksApp_tracks/app/models/user.rb, step:Then ]
[name:User, file:TracksApp_tracks/app/models/user.rb, step:Given ]
[name:create, file:null, step:Then ]
[name:login, file:null, step:Then ]
[name:update_auth_type, file:null, step:Then ]
[name:update_password, file:null, step:Then ]

Methods: 111
[name:all, type:Object, file:null, step:When ]
[name:all_done, type:TodosController, file:TracksApp_tracks/app/controllers/todos_controller.rb, step:Then ]
[name:all_done_tag, type:TodosController, file:TracksApp_tracks/app/controllers/todos_controller.rb, step:Then ]
[name:all_done_todos, type:ContextsController, file:TracksApp_tracks/app/controllers/contexts_controller.rb, step:Then ]
[name:all_done_todos, type:ProjectsController, file:TracksApp_tracks/app/controllers/projects_controller.rb, step:Then ]
[name:be_nil, type:Object, file:null, step:When ]
[name:be_nil, type:Object, file:null, step:Given ]
[name:change_password, type:UsersController, file:TracksApp_tracks/app/controllers/users_controller.rb, step:Then ]
[name:clear_context_name_from_next_action_form, type:TracksFormHelper, file:TracksApp_tracks/features/support/tracks_form_helper.rb, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:contexts, type:Object, file:null, step:Then ]
[name:contexts, type:Totals, file:TracksApp_tracks/app/models/stats/totals.rb, step:Then ]
[name:contexts, type:UserStats, file:TracksApp_tracks/app/models/stats/user_stats.rb, step:Then ]
[name:contexts, type:Totals, file:TracksApp_tracks/app/models/stats/totals.rb, step:Given ]
[name:contexts, type:UserStats, file:TracksApp_tracks/app/models/stats/user_stats.rb, step:Given ]
[name:create_preference, type:Object, file:null, step:Given ]
[name:csv_actions, type:DataController, file:TracksApp_tracks/app/controllers/data_controller.rb, step:Then ]
[name:csv_notes, type:DataController, file:TracksApp_tracks/app/controllers/data_controller.rb, step:Then ]
[name:current_url, type:Object, file:null, step:Then ]
[name:delete_all, type:User, file:TracksApp_tracks/app/models/user.rb, step:Given ]
[name:determine_all_done_path, type:ApplicationHelper, file:TracksApp_tracks/app/helpers/application_helper.rb, step:Then ]
[name:done, type:StatsController, file:TracksApp_tracks/app/controllers/stats_controller.rb, step:Then ]
[name:done, type:TodosController, file:TracksApp_tracks/app/controllers/todos_controller.rb, step:Then ]
[name:done, type:ProjectsController, file:TracksApp_tracks/app/controllers/projects_controller.rb, step:Then ]
[name:done, type:RecurringTodosController, file:TracksApp_tracks/app/controllers/recurring_todos_controller.rb, step:Then ]
[name:done_tag, type:TodosController, file:TracksApp_tracks/app/controllers/todos_controller.rb, step:Then ]
[name:done_todos, type:ContextsController, file:TracksApp_tracks/app/controllers/contexts_controller.rb, step:Then ]
[name:done_todos, type:ProjectsController, file:TracksApp_tracks/app/controllers/projects_controller.rb, step:Then ]
[name:each, type:Object, file:null, step:Given ]
[name:eq, type:Object, file:null, step:Then ]
[name:evaluate_script, type:Object, file:null, step:When ]
[name:execute_javascript, type:TracksStepHelper, file:TracksApp_tracks/features/support/tracks_step_helper.rb, step:When ]
[name:execute_script, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:first, type:Object, file:null, step:Then ]
[name:first, type:Object, file:null, step:When ]
[name:first, type:Object, file:null, step:Given ]
[name:first_or_create, type:Object, file:null, step:Given ]
[name:handle_js_confirm, type:TracksStepHelper, file:TracksApp_tracks/features/support/tracks_step_helper.rb, step:When ]
[name:hashes, type:Object, file:null, step:Given ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_css, type:Object, file:null, step:When ]
[name:have_no_content, type:Object, file:null, step:Then ]
[name:have_no_css, type:Object, file:null, step:When ]
[name:id, type:Context, file:TracksApp_tracks/app/models/context.rb, step:When ]
[name:id, type:Project, file:TracksApp_tracks/app/models/project.rb, step:When ]
[name:id, type:Todo, file:TracksApp_tracks/app/models/todo.rb, step:When ]
[name:id, type:User, file:TracksApp_tracks/app/models/user.rb, step:Given ]
[name:index, type:TodosController, file:TracksApp_tracks/app/controllers/todos_controller.rb, step:Then ]
[name:index, type:StatsController, file:TracksApp_tracks/app/controllers/stats_controller.rb, step:Then ]
[name:index, type:NotesController, file:TracksApp_tracks/app/controllers/notes_controller.rb, step:Then ]
[name:index, type:ContextsController, file:TracksApp_tracks/app/controllers/contexts_controller.rb, step:Then ]
[name:index, type:ProjectsController, file:TracksApp_tracks/app/controllers/projects_controller.rb, step:Then ]
[name:index, type:UsersController, file:TracksApp_tracks/app/controllers/users_controller.rb, step:Then ]
[name:index, type:RecurringTodosController, file:TracksApp_tracks/app/controllers/recurring_todos_controller.rb, step:Then ]
[name:index, type:IntegrationsController, file:TracksApp_tracks/app/controllers/integrations_controller.rb, step:Then ]
[name:index, type:DataController, file:TracksApp_tracks/app/controllers/data_controller.rb, step:Then ]
[name:index, type:PreferencesController, file:TracksApp_tracks/app/controllers/preferences_controller.rb, step:Then ]
[name:index, type:SearchController, file:TracksApp_tracks/app/controllers/search_controller.rb, step:Then ]
[name:index, type:FeedlistController, file:TracksApp_tracks/app/controllers/feedlist_controller.rb, step:Then ]
[name:join, type:Object, file:null, step:Then ]
[name:list_deferred, type:TodosController, file:TracksApp_tracks/app/controllers/todos_controller.rb, step:Then ]
[name:login, type:LoginController, file:TracksApp_tracks/app/controllers/login_controller.rb, step:Then ]
[name:login, type:Object, file:null, step:Given ]
[name:logout, type:LoginController, file:TracksApp_tracks/app/controllers/login_controller.rb, step:Then ]
[name:loop, type:Object, file:null, step:When ]
[name:new, type:UsersController, file:TracksApp_tracks/app/controllers/users_controller.rb, step:Then ]
[name:open_submenu_for, type:TracksStepHelper, file:TracksApp_tracks/features/support/tracks_step_helper.rb, step:When ]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:When ]
[name:path, type:Object, file:null, step:Then ]
[name:path_to, type:Paths, file:TracksApp_tracks/features/support/paths.rb, step:Then ]
[name:projects, type:Object, file:null, step:Then ]
[name:projects, type:Totals, file:TracksApp_tracks/app/models/stats/totals.rb, step:Then ]
[name:projects, type:UserStats, file:TracksApp_tracks/app/models/stats/user_stats.rb, step:Then ]
[name:projects, type:Calendar, file:TracksApp_tracks/app/models/todos/calendar.rb, step:Then ]
[name:push, type:Object, file:null, step:Then ]
[name:raise, type:Object, file:null, step:Then ]
[name:refresh_token, type:UsersController, file:TracksApp_tracks/app/controllers/users_controller.rb, step:Then ]
[name:request_signin_as, type:TracksLoginHelper, file:TracksApp_tracks/features/support/tracks_login_helper.rb, step:Given ]
[name:rest_api, type:IntegrationsController, file:TracksApp_tracks/app/controllers/integrations_controller.rb, step:Then ]
[name:review, type:ProjectsController, file:TracksApp_tracks/app/controllers/projects_controller.rb, step:Then ]
[name:send, type:Object, file:null, step:Then ]
[name:show, type:CalendarController, file:TracksApp_tracks/app/controllers/calendar_controller.rb, step:Then ]
[name:show, type:ContextsController, file:TracksApp_tracks/app/controllers/contexts_controller.rb, step:Then ]
[name:show, type:ProjectsController, file:TracksApp_tracks/app/controllers/projects_controller.rb, step:Then ]
[name:submit_form, type:TracksFormHelper, file:TracksApp_tracks/features/support/tracks_form_helper.rb, step:When ]
[name:submit_next_action_form, type:TracksFormHelper, file:TracksApp_tracks/features/support/tracks_form_helper.rb, step:When ]
[name:tag, type:TodosController, file:TracksApp_tracks/app/controllers/todos_controller.rb, step:Then ]
[name:to, type:Object, file:null, step:Then ]
[name:to, type:Object, file:null, step:When ]
[name:to_not, type:Object, file:null, step:When ]
[name:to_not, type:Object, file:null, step:Given ]
[name:to_sym, type:Object, file:null, step:Then ]
[name:todos, type:Object, file:null, step:When ]
[name:url_for, type:Object, file:null, step:Then ]
[name:wait_for_ajax, type:TracksStepHelper, file:TracksApp_tracks/features/support/tracks_step_helper.rb, step:When ]
[name:wait_for_animations_to_end, type:TracksStepHelper, file:TracksApp_tracks/features/support/tracks_step_helper.rb, step:When ]
[name:wait_until, type:TracksStepHelper, file:TracksApp_tracks/features/support/tracks_step_helper.rb, step:When ]
[name:where, type:Object, file:null, step:Then ]
[name:where, type:User, file:TracksApp_tracks/app/models/user.rb, step:Then ]
[name:where, type:Object, file:null, step:When ]
[name:where, type:User, file:TracksApp_tracks/app/models/user.rb, step:Given ]
[name:where, type:Object, file:null, step:Given ]
[name:with_scope, type:WebSteps, file:TracksApp_tracks/features/step_definitions/web_steps.rb, step:Then ]
[name:within, type:Object, file:null, step:Then ]
[name:within, type:Object, file:null, step:When ]
[name:xml_export, type:DataController, file:TracksApp_tracks/app/controllers/data_controller.rb, step:Then ]
[name:yaml_export, type:DataController, file:TracksApp_tracks/app/controllers/data_controller.rb, step:Then ]
[name:zero?, type:Object, file:null, step:When ]

Referenced pages: 82
TracksApp_tracks/app/views/calendar/show.html.erb
TracksApp_tracks/app/views/contexts/_context.html.erb
TracksApp_tracks/app/views/contexts/_context_form.html.erb
TracksApp_tracks/app/views/contexts/_context_listing.html.erb
TracksApp_tracks/app/views/contexts/_context_state_group.html.erb
TracksApp_tracks/app/views/contexts/_new_context_form.html.erb
TracksApp_tracks/app/views/contexts/index.html.erb
TracksApp_tracks/app/views/contexts/show.html.erb
TracksApp_tracks/app/views/data/csv_import.html.erb
TracksApp_tracks/app/views/data/csv_map.html.erb
TracksApp_tracks/app/views/data/import.html.erb
TracksApp_tracks/app/views/data/index.de.html.erb
TracksApp_tracks/app/views/data/index.en.html.erb
TracksApp_tracks/app/views/data/index.es.html.erb
TracksApp_tracks/app/views/data/yaml_export.html.erb
TracksApp_tracks/app/views/data/yaml_form.de.html.erb
TracksApp_tracks/app/views/data/yaml_form.en.html.erb
TracksApp_tracks/app/views/data/yaml_import.html.erb
TracksApp_tracks/app/views/feedlist/_feed_for_context.html.erb
TracksApp_tracks/app/views/feedlist/_feed_for_project.html.erb
TracksApp_tracks/app/views/feedlist/_legend.html.erb
TracksApp_tracks/app/views/feedlist/index.html.erb
TracksApp_tracks/app/views/integrations/index.de.html.erb
TracksApp_tracks/app/views/integrations/index.en.html.erb
TracksApp_tracks/app/views/integrations/index.nl.html.erb
TracksApp_tracks/app/views/integrations/rest_api.html.erb
TracksApp_tracks/app/views/login/login.html.erb
TracksApp_tracks/app/views/notes/_note_edit_form.html.erb
TracksApp_tracks/app/views/notes/_notes_summary.html.erb
TracksApp_tracks/app/views/notes/index.html.erb
TracksApp_tracks/app/views/preferences/_authentication.html.erb
TracksApp_tracks/app/views/preferences/_date_and_time.html.erb
TracksApp_tracks/app/views/preferences/_profile.html.erb
TracksApp_tracks/app/views/preferences/_tracks_behavior.html.erb
TracksApp_tracks/app/views/preferences/index.html.erb
TracksApp_tracks/app/views/projects/_new_project_form.html.erb
TracksApp_tracks/app/views/projects/_project.html.erb
TracksApp_tracks/app/views/projects/_project_form.html.erb
TracksApp_tracks/app/views/projects/_project_listing.html.erb
TracksApp_tracks/app/views/projects/_project_settings.html.erb
TracksApp_tracks/app/views/projects/_project_settings_container.html.erb
TracksApp_tracks/app/views/projects/_project_state_group.html.erb
TracksApp_tracks/app/views/projects/done.html.erb
TracksApp_tracks/app/views/projects/index.html.erb
TracksApp_tracks/app/views/projects/review.html.erb
TracksApp_tracks/app/views/projects/show.html.erb
TracksApp_tracks/app/views/recurring_todos/_recurring_todo.html.erb
TracksApp_tracks/app/views/recurring_todos/_recurring_todo_form.html.erb
TracksApp_tracks/app/views/recurring_todos/done.html.erb
TracksApp_tracks/app/views/recurring_todos/index.html.erb
TracksApp_tracks/app/views/search/index.html.erb
TracksApp_tracks/app/views/shared/_add_new_item_form.html.erb
TracksApp_tracks/app/views/stats/_actions.html.erb
TracksApp_tracks/app/views/stats/_chart.html.erb
TracksApp_tracks/app/views/stats/_contexts.html.erb
TracksApp_tracks/app/views/stats/_contexts_list.html.erb
TracksApp_tracks/app/views/stats/_null_list_item.html.erb
TracksApp_tracks/app/views/stats/_projects.html.erb
TracksApp_tracks/app/views/stats/_projects_list.html.erb
TracksApp_tracks/app/views/stats/_tags.html.erb
TracksApp_tracks/app/views/stats/_time_to_complete.html.erb
TracksApp_tracks/app/views/stats/_totals.html.erb
TracksApp_tracks/app/views/stats/done.html.erb
TracksApp_tracks/app/views/stats/index.html.erb
TracksApp_tracks/app/views/todos/_collection.html.erb
TracksApp_tracks/app/views/todos/_edit_form.html.erb
TracksApp_tracks/app/views/todos/_new_multi_todo_form.html.erb
TracksApp_tracks/app/views/todos/_new_todo_form.html.erb
TracksApp_tracks/app/views/todos/_successor.html.erb
TracksApp_tracks/app/views/todos/_todo.html.erb
TracksApp_tracks/app/views/todos/all_done.html.erb
TracksApp_tracks/app/views/todos/done.html.erb
TracksApp_tracks/app/views/todos/index.html.erb
TracksApp_tracks/app/views/todos/list_deferred.html.erb
TracksApp_tracks/app/views/todos/tag.html.erb
TracksApp_tracks/app/views/users/_update_password.html.erb
TracksApp_tracks/app/views/users/change_auth_type.html.erb
TracksApp_tracks/app/views/users/change_password.html.erb
TracksApp_tracks/app/views/users/index.html.erb
TracksApp_tracks/app/views/users/new.html.erb
TracksApp_tracks/app/views/users/nosignup.de.html.erb
TracksApp_tracks/app/views/users/nosignup.en.html.erb

