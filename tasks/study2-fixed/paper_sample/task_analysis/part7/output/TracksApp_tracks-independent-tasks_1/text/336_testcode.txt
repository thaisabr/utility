Given /^I have a context called "([^\"]*)"$/ do |context_name|
  Given "there exists an active context called \"#{context_name}\" for user \"#{@current_user.login}\""
end
Given /^I have the following contexts$/ do |table|
  Context.delete_all
  table.hashes.each do |hash|
    context = @current_user.contexts.create!(:name => hash[:name])
    unless hash[:hide].blank?
      context.hide = hash[:hide] == true
      context.save!
    end
  end
end
When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )follow "([^"]*)"$/ do |link|
  click_link(link)
end
Then /^(?:|I )should see "([^"]*)"$/ do |text|
  if response.respond_to? :should
    response.should contain(text)
  else
    assert_contain text
  end
end
Then /^(?:|I )should be on (.+)$/ do |page_name|
  current_path = URI.parse(current_url).path
  if current_path.respond_to? :should
    current_path.should == path_to(page_name)
  else
    assert_equal path_to(page_name), current_path
  end
end
  def path_to(page_name)
    options = @mobile_interface ? {:format => :m} : {}
    case page_name

    when /the home\s?page/
      root_path(options)
    when /the statistics page/
      stats_path(options)
    when /the signup page/
      signup_path(options)
    when /the login page/
      login_path(options)
    when /the notes page/
      notes_path(options)
    when /the contexts page/
      contexts_path(options)
    when /the projects page/
      projects_path(options)
    when /the manage users page/
      users_path(options)
    when /the repeating todos page/
      recurring_todos_path(options)
    when /the integrations page/
      integrations_path(options)
    when /the tickler page/
      tickler_path(options)
    when /the export page/
      data_path(options)
    when /the preference page/
      preferences_path(options)
    when /the rest api docs page/
      rest_api_docs_path(options)
    when /the search page/
      search_path(options)
    when /the starred page/
      tag_path("starred", options)
    when /the feeds page/
      feeds_path(options)
    when /the context page for "([^\"]*)" for user "([^\"]*)"/i
      context_path(User.find_by_login($2).contexts.find_by_name($1), options)
    when /the context page for "([^\"]*)"/i
      context_path(@current_user.contexts.find_by_name($1), options)
    when /the "([^\"]*)" project for user "([^\"]*)"/i
      project_path(User.find_by_login($2).projects.find_by_name($1), options)
    when /the "([^\"]*)" project/i
      @project = @current_user.projects.find_by_name($1)
      project_path(@project, options)
    when /the tag page for "([^"]*)"/i
      tag_path($1, options)

      # Add more mappings here.
      # Here is an example that pulls values out of the Regexp:
      #
      #   when /^(.*)'s profile page$/i
      #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Then /the badge should show (.*)/ do |number|
  badge = -1
  xpath= "//span[@id='badge_count']"

  if response.respond_to? :selenium
    response.should have_xpath(xpath) 
    badge = response.selenium.get_text("xpath=#{xpath}").to_i
  else
    response.should have_xpath(xpath) do |node|
      badge = node.first.content.to_i
    end
  end

  badge.should == number.to_i
end
Then /^I should see an error flash message saying "([^"]*)"$/ do |message|
  xpath = "//div[@id='message_holder']/h4[@id='flash']"
  text = response.selenium.get_text("xpath=#{xpath}")
  text.should == message
end
Then /^I should see "([^"]*)" in context container for "([^"]*)"$/ do |todo_description, context_name|
  context = @current_user.contexts.find_by_name(context_name)
  context.should_not be_nil
  todo = @current_user.todos.find_by_description(todo_description)
  todo.should_not be_nil

  xpath = "xpath=//div[@id=\"c#{context.id}\"]//div[@id='line_todo_#{todo.id}']"
  selenium.wait_for_element(xpath, :timeout_in_seconds => 5)
  selenium.is_visible(xpath).should be_true
end
Then /^I should see "([^"]*)" in project container for "([^"]*)"$/ do |todo_description, project_name|
  todo = @current_user.todos.find_by_description(todo_description)
  todo.should_not be_nil

  project = @current_user.projects.find_by_name(project_name)
  project.should_not be_nil

  xpath = "//div[@id='p#{project.id}items']//div[@id='line_todo_#{todo.id}']"

  selenium.wait_for_element("xpath=#{xpath}", :timeout_in_seconds => 5)
  selenium.is_visible(xpath).should be_true
end
Given /^the following user records?$/ do |table|
  User.delete_all
  table.hashes.each do |hash|
    user = Factory(:user, hash)
    user.create_preference
  end
end
Given /^I have logged in as "(.*)" with password "(.*)"$/ do |username, password|
  When "I go to the login page"
  fill_in "Login", :with => username
  fill_in "Password", :with => password
  uncheck "Stay logged in:"
  click_button
  if response.respond_to? :selenium
    selenium.wait_for_page_to_load(5000)
  end
  logout_regexp = @mobile_interface ? "Logout" : "Logout \(#{username}\)"
  response.should contain(logout_regexp)
  @current_user = User.find_by_login(username)
end
Then /^context "([^"]*)" should be above context "([^"]*)"$/ do |context_high, context_low|
  high_id = @current_user.contexts.find_by_name(context_high).id
  low_id = @current_user.contexts.find_by_name(context_low).id
  high_pos = selenium.get_element_position_top("//div[@id='context_#{high_id}']").to_i
  low_pos = selenium.get_element_position_top("//div[@id='context_#{low_id}']").to_i
  (high_pos < low_pos).should be_true
end
When /^I drag context "([^"]*)" below context "([^"]*)"$/ do |context_drag, context_drop|
  drag_id = @current_user.contexts.find_by_name(context_drag).id
  drop_id = @current_user.contexts.find_by_name(context_drop).id

  container_height = selenium.get_element_height("//div[@id='container_context_#{drag_id}']").to_i
  vertical_offset = container_height*2
  coord_string = "10,#{vertical_offset}"

  drag_context_handle_xpath = "//div[@id='context_#{drag_id}']//span[@class='handle']"
  drop_context_container_xpath = "//div[@id='container_context_#{drop_id}']"

  selenium.mouse_down_at(drag_context_handle_xpath,"2,2")
  selenium.mouse_move_at(drop_context_container_xpath,coord_string)
  # no need to simulate mouse_over for this test
  selenium.mouse_up_at(drop_context_container_xpath,coord_string)
end
Then /^the new context form should be visible$/ do
  selenium.is_visible("context_new").should be_true
end
Then /^the new context form should not be visible$/ do
  selenium.is_visible("context_new").should be_false
end
Given /^there exists a project "([^\"]*)" for user "([^\"]*)"$/ do |project_name, user_name|
  user = User.find_by_login(user_name)
  user.should_not be_nil
  @project = user.projects.create!(:name => project_name)
end
When /^I visit the "([^\"]*)" project$/ do |project_name|
  @project = Project.find_by_name(project_name)
  @project.should_not be_nil
  visit project_path(@project)
end
Given /^I have a project "([^"]*)" that has the following todos$/ do |project_name, todos|
  Given "I have a project called \"#{project_name}\""
  @project.should_not be_nil
  todos.hashes.each do |todo|
    context = @current_user.contexts.find_by_name(todo[:context])
    context.should_not be_nil
    new_todo = @current_user.todos.create!(
      :description => todo[:description],
      :context_id => context.id,
      :project_id=>@project.id)
    unless todo[:tags].nil?
      new_todo.tag_with(todo[:tags])
    end
    unless todo[:completed].nil?
      new_todo.complete! if todo[:completed] == 'yes'
    end
  end
end
Then /^I should see "([^"]*)" in the due next month container$/ do |todo_description|
  todo = @current_user.todos.find_by_description(todo_description)
  todo.should_not be_nil

  xpath = "//div[@id='due_after_this_month']//div[@id='line_todo_#{todo.id}']"

  wait_for :timeout => 5 do
    selenium.is_element_present(xpath)
  end
end
When /^I drag "(.*)" to "(.*)"$/ do |dragged, target|
  drag_id = Todo.find_by_description(dragged).id
  drop_id = Todo.find_by_description(target).id
  drag_name = "xpath=//div[@id='line_todo_#{drag_id}']//img[@class='grip']"
  drop_name = "xpath=//div[@id='line_todo_#{drop_id}']//div[@class='description']"

  selenium.drag_and_drop_to_object(drag_name, drop_name)

  wait_for_ajax
end
  def wait_for_ajax
    selenium.wait_for :wait_for => :ajax, :javascript_framework => :jquery
  end
