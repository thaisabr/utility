Feature: As an admin, I should be able to create and view users.
Scenario:      To check that an admin creates a user record and is able to view it
Given no users exist
Given I am on manage users page
And I follow "New user"
When I fill in "George Harrison" for "Full name"
And I fill in "george" for "user name"
And I fill in "password" for "password"
And I fill in "password" for "Re-enter password"
And I choose "User"
And I fill in "abcd@unicef.com" for "email"
And I fill in "UNICEF" for "organisation"
And I fill in "Rescuer" for "position"
And I fill in "Amazon" for "location"
And I press "Create"
Then I should see "User was successfully created."
Then I should see "George Harrison"
And I should see "george"
And I should see "User"
And I should see "abcd@unicef.com"
And I should see "UNICEF"
And I should see "Rescuer"
And I should see "Amazon"
Scenario: To check that email address is case insensitive
Given I am on new user page
When I fill in "George Bush" for "Full Name"
And I fill in "george1" for "user name"
And I fill in "password" for "password"
And I fill in "password" for "Re-enter password"
And I choose "User"
And I fill in "Aaa@Bbbb.com" for "email"
When I press "Create"
Then I should see "User was successfully created."
Feature:
So that we can keep track of children that are found in the field, a user should be able to go to a website and upload
basic information about the lost child.
Scenario: creating a child record
Given no children exist
Given I am on children listing page
And I follow "New child"
When I fill in "Jorge Just" for "Name"
And I fill in "27" for "Age"
And I select "Exact" from "Is age exact"
And I choose "Male"
And I fill in "London" for "Origin"
And I fill in "Haiti" for "Last known location"
And I select "1-2 weeks ago" from "Date of separation"
And I attach the file "features/resources/jorge.jpg" to "photo"
And I press "Create"
Then I should see "Child record successfully created."
And I should see "Jorge Just"
And I should see "27"
And I should see "1"
And I should see "Male"
And I should see "London"
And I should see "Haiti"
And I should see "1-2 weeks ago"
When I follow "Back"
Then I should see "Listing children"
And I should see "Jorge Just"
When I follow "Show"
Then I follow "Back"
And I should see "Listing children"
And I should see "Jorge Just"
Feature: As an user, I should be able to log in.
Scenario: To check that a user can log in
Given no users exist
Given a user "Harry" with a password "password"
Given I am on the login page
When I fill in "Harry" for "user name"
And I fill in "123" for "password"
And I press "Log in"
Then I should see "Hello Harry"
