When(/^I (?:go to|am on) the "([^"]*)" page$/) do |page|
  visit path_to(page)
end
When(/^I go to the path "(.*?)"$/) do |page|
  visit path_to(page)
end
When(/^I click "([^"]*)"$/) do |text|
  click_link_or_button text
end
When(/^I click the "([^"]*)" button$/) do |button|
  click_link_or_button button
end
When(/^I follow "([^"]*)"$/) do |text|
  click_link text
end
When /^I fill in(?: "([^"]*)")?:$/ do |name, table|
  with_scope(name) do
    table.rows.each do |row|
      fill_in row[0], with: row[1]
    end
  end
end
Then /^I should be on the "([^"]*)" page$/ do |page|
  expect(current_path).to eq path_to(page)
end
Then /^I am redirected to the "([^"]*)" page$/ do |page|
  expect(current_path).to eq path_to(page)
end
Then /^I should( not)? see:$/ do |negative, table|
  expectation = negative ? :should_not : :should
  table.rows.flatten.each do |string|
    page.send(expectation, have_text(string))
  end
end
Then /^I should( not)? see "([^"]*)"$/ do |negative, string|
  unless negative
    page.should have_text string
  else
    page.should_not have_text string
  end
end
Given(/^I am on the "([^"]*)" page for ([^"]*) "([^"]*)"$/) do |action, controller, title|
  visit url_for_title(action: action, controller: controller, title: title)
end
When(/^I select "([^"]*)" to "([^"]*)"$/) do |field, option|
  find(:select, field).find(:option, option).select_option
end
Given(/^I want to use third party authentications$/) do
  OmniAuth.config.test_mode = true
  OmniAuth.config.mock_auth[:github] = {
      'provider' => 'github',
      'uid' => '12345678',
      'info' => {
          'email' => 'mock@email.com'
      }
  }
  OmniAuth.config.mock_auth[:gplus] = {
      'provider' => 'gplus',
      'uid' => '12345678',
      'info' => {
          'email' => 'mock@email.com'
      }
  }
end
  def with_scope(locator)
    locator ? within(*selector_for(locator)) { yield } : yield
  end
def path_to(page_name, id = '')
  name = page_name.downcase
  case name
    when 'home' then
      root_path
    when 'registration' then
      new_user_registration_path
    when 'sign in' then
      new_user_session_path
    when 'projects' then
      projects_path
    when 'new project' then
      new_project_path
    when 'edit' then
      edit_project_path(id)
    when 'show' then
      project_path(id)
    when 'our members' then
      users_index_path
    when 'user profile' then
      users_show_path(id)
    when 'my account' then
      edit_user_registration_path(id)
    when "foobar" then
      visit ("/#{page}")

    else
      raise('path to specified is not listed in #path_to')
  end
end
  def selector_for(locator)
    case locator

      when "the page"
        "html > body"

      # Add more mappings here.
      # Here is an example that pulls values out of the Regexp:
      #
      #  when /^the (notice|error|info) flash$/
      #    ".flash.#{$1}"

      # You can also return an array to use a different selector
      # type, like:
      #
      #  when /the header/
      #    [:xpath, "//header"]

      # This allows you to provide a quoted selector as the scope
      # for "within" steps as was previously the default for the
      # web steps:
      when "Account details"
        'form#edit_user'
      when "Contact form"
        'form#contact_form'
      when /^"(.+)"$/
        $1

      else
        raise "Can't find mapping from \"#{locator}\" to a selector.\n" +
                  "Now, go and add a mapping in #{__FILE__}"
    end
  end
def url_for_title(options)
  controller = options[:controller]
  eval("#{controller.capitalize.singularize}.find_by_title('#{options[:title]}').url_for_me(options[:action].downcase)")
end
Given(/^the following projects exist:$/) do |table|
  table.hashes.each do |hash|
    project = Project.create(hash)
    project.save
  end
end
Given /^I am logged in$/ do
  create_user
  sign_in
end
  def create_user
    create_visitor
    delete_user
    @user = FactoryGirl.create(:user, @visitor)
  end
  def sign_in
    visit new_user_session_path
    within ('#main') do
      fill_in 'user_email', :with => @visitor[:email]
      fill_in 'user_password', :with => @visitor[:password]
      click_button 'Sign in'
    end
  end
  def create_visitor
    #@visitor =FactoryGirl(:user)
    @visitor ||= { :email => "example@example.com",
                   :password => "changeme",
                   :password_confirmation => "changeme" }
  end
  def delete_user
    @user ||= User.where(:email => @visitor[:email]).first
    @user.destroy unless @user.nil?
  end
Given(/^the following pages exist:$/) do |table|
  # table is a Cucumber::Ast::Table
  pending # express the regexp above with the code you wish you had
end
Given(/^I visit "(.*?)"$/) do |path|
  visit path
end
Then(/^the page should be titled "(.*?)"$/) do |title|
  page.source.should have_css("title", :text => title, :visible => false)
end
And(/^the response status should be "([^"]*)"$/) do |code|
  page.status_code.should eql(code.to_i)
end
When(/^I encounter an internal server error$/) do
  VisitorsController.any_instance.should_receive(:index).and_raise(Exception)
  visit root_path
end
