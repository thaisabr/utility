Feature: Manage Document
As a project member
So that I can share my work related to a project
I would like to be able to create and edit new documents
Background:
Given the following projects exist:
| title       | description          | status   |
| hello world | greetings earthlings | active   |
| hello mars  | greetings aliens     | inactive |
And the following documents exist:
| title         | body             | project     |
| Guides        | My guide to      | hello mars  |
| Documentation | My documentation | hello world |
Scenario: Create a new document
Given I am logged in
Given I am on the "Show" page for project "hello world"
When I click the very stylish "New Document" button
And I fill in "Title" with "New doc title"
And I click "Submit"
Then I should see "Document was successfully created."
Scenario: Create a new document page should have a back button
Given I am logged in
Given I am on the "Show" page for project "hello world"
When I click the very stylish "New Document" button
And I click "Back"
Then I should be on the "Show" page for project "hello world"
Scenario: A document can have children
Given the document "Guides" has a child document with title "Howto"
Given I am on the "Show" page for document "Guides"
Then I should see "Howto"
When I click "Howto"
Then I should be on the "Show" page for document "Howto"
And I should see "Guides"
Scenario: Has a link to edit a document using the Mercury Editor
Given the document "Guides" has a child document with title "Howto"
Given I am logged in
And I am on the "Show" page for document "Howto"
When I click the very stylish "Edit" button
Then I should be in the Mercury Editor
Feature: Join projects
As a user
So that I can participate in projects
I would like to join projects as a member
And I can see who is a member of which project
https://www.pivotaltracker.com/story/show/63372740
Background:
Given the following projects exist:
| title       | description          | status   |
| hello world | greetings earthlings | active   |
| hello mars  | greetings aliens     | inactive |
Scenario: Join a project
Given I am logged in
And I am not a member of project "hello mars"
And I am on the "Show" page for project "hello mars"
And I click the very stylish "Join Project" button
Then I should become a member of project "hello mars"
And I should see "You just joined hello mars"
Scenario: Leave a project
Given I am logged in
And I am a member of project "hello mars"
And I am on the "Show" page for project "hello mars"
And I click the very stylish "Leave Project" button
Then I should stop being a member of project "hello mars"
And I should see "You are no longer a member of hello mars"
Feature: Create and maintain projects
As a member of AgileVentures
So that I can participate in AgileVentures activities
I would like to add a new project
Background:
Given the following projects exist:
| title         | description             | status   |
| hello world   | greetings earthlings    | active   |
| hello mars    | greetings aliens        | inactive |
| hello jupiter | greetings jupiter folks | active   |
| hello mercury | greetings mercury folks | inactive |
| hello saturn  | greetings saturn folks  | active   |
| hello sun     | greetings sun folks     | active   |
Scenario: Show New Project button if user is logged in
When I am logged in
And I am on the "projects" page
Then I should see the very stylish "New Project" button
Scenario: Do not show New Project button if user is not logged in
Given I am not logged in
When I am on the "projects" page
Then I should not see the very stylish "New Project" button
Scenario: Alphabetically display pagination in "Our Projects" page
Given I am on the home page
When I follow "Our projects"
Then I should see:
| greetings aliens        |
| greetings jupiter folks |
| greetings mercury folks |
| greetings saturn folks  |
| greetings sun folks     |
And I should not see "greetings earthlings"
When I go to the next page
Then I should not see:
| greetings aliens        |
| greetings jupiter folks |
| greetings mercury folks |
| greetings saturn folks  |
| greetings sun folks     |
And I should see "greetings earthlings"
Scenario: Creating a new project
Given I am logged in
And I am on the "projects" page
When I click the very stylish "New Project" button
Then I should see "Creating a new Project"
And I should see a form with:
| Field       |
| Title       |
| Description |
| Status      |
Scenario: Saving a new project: success
Given I am logged in
And I am on the "Projects" page
When I click the very stylish "New Project" button
When I fill in:
| Field       | Text            |
| Title       | Title New       |
| Description | Description New |
And I select "Status" to "Active"
And I click the "Submit" button
Then I should be on the "projects" page
And I should see "Project was successfully created."
When I go to the next page
And I should see:
| Text            |
| Title New       |
| Description New |
| ACTIVE          |
Scenario: Saving a new project: failure
Given I am logged in
And I am on the "projects" page
And I click the very stylish "New Project" button
When I fill in "Title" with ""
And I click the "Submit" button
Then I should see "Project was not saved. Please check the input."
Feature: Sidebar navigation
As a user
So that I may navigate through project documents with ease
I want to have a sidebar to navigate project documents with
Background:
Given the following projects exist:
| title       | description          | status   | id |
| hello world | greetings earthlings | active   | 1  |
| hello mars  | greetings aliens     | inactive | 2  |
And the following documents exist:
| title         | body             | project_id |
| Howto         | How to start     |          1 |
| Documentation | My documentation |          1 |
| Another doc   | My content       |          2 |
| Howto 2       | My documentation |          2 |
Scenario: Sidebar is always visible
Given I am logged in
And I am on the "Edit" page for project "hello mars"
Then I should see the sidebar
Given I am on the "Show" page for project "hello mars"
Then I should see the sidebar
Given I am on the "projects" page
Then I should see the sidebar
When I click the very stylish "New Project" button
Then I should see the sidebar
