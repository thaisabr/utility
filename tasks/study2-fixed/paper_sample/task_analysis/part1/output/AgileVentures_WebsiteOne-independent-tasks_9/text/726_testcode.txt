Given(/^the following projects exist:$/) do |table|
  table.hashes.each do |hash|
    project = Project.create(hash)
    project.save
  end
end
When(/^I click the "([^"]*)" button for project "([^"]*)"$/) do |button, project_name|
  project = Project.find_by_title(project_name)
  if project
    within("tr##{project.id}") do
      click_link_or_button button
    end
  else
    visit path_to(button, 'non-existent')
  end
end
When(/^(.*) in the list of projects$/) do |s|
  page.within(:css, 'table#projects') { step(s) }
end
Given(/^the document "([^"]*)" has a child document with title "([^"]*)"$/) do |parent, child|
  parent_doc = Document.find_by_title(parent)
  parent_project_id = parent_doc.project_id
  child_doc = parent_doc.children.create!( { :project_id => parent_project_id,:title => child })
end
Given(/^I am on the home page$/) do
  visit "/"
end
def path_to(page_name, id = '')
  name = page_name.downcase
  case name
    when 'home' then
      root_path
    when 'registration' then
      new_user_registration_path
    when 'sign in' then
      new_user_session_path
    when 'projects' then
      projects_path
    when 'new project' then
      new_project_path
    when 'edit' then
      edit_project_path(id)
    when 'show' then
      project_path(id)
    when 'our members' then
      users_index_path
    when 'user profile' then
      users_show_path(id)
    when 'my account' then
      edit_user_registration_path(id)
    else
      raise('path to specified is not listed in #path_to')
  end
end
When /^(?:|I )click "([^"]*)" within Mercury Editor toolbar$/ do |button|
  selector_for = {
      'save' => 'mercury-save-button'
  }
  page.execute_script("$('.#{selector_for[button.downcase]}').click()")
  puts 'sleep(0.1)'
  sleep(0.1)
end
Given(/^I am going to use the Mercury Editor/) do
  Capybara.current_driver = :selenium
end
When(/^I fill in the editable field "([^"]*)" with "([^"]*)"$/) do |field, s|
  page.driver.within_frame('mercury_iframe') {
    field = field.downcase.singularize
    # This selector is specific to the mercury region used!
    if field == 'title'
      find(:css, 'div#document_title>textarea').set(s)
    elsif field == 'body'
      page.execute_script("$('#document_body').text('#{s}')")
      #find(:css, 'div#document_body').set(s)
    end
  }
end
When(/^I (try to use|am using) the Mercury Editor to edit ([^"]*) "([^"]*)"$/) do |opt, model, title|
  visit "/editor#{url_for_title(action: 'show', controller: model, title: title)}"
end
Then /^I should( not)? see button "([^"]*)" in Mercury Editor$/ do |negative, button|
  button = 'new_document_link' if button == 'New document'
  page.driver.within_frame('mercury_iframe') {
    unless negative
      expect(has_link? button).to be_true
    else
      expect(has_link? button).to be_false
    end
  }
end
When /I click "([^"]*)" in Mercury Editor/ do |button|
  page.execute_script('Mercury.silent = true')   # disabling the confirmation dialog for saving changes
  page.driver.within_frame('mercury_iframe') {
    click_link button
  }
end
Given /^I am logged in as user with email "([^"]*)", with password "([^"]*)"$/ do |email, password|
  @user = FactoryGirl.create(:user, email: email, password: password, password_confirmation: password )
  visit new_user_session_path
  within ('#main') do
    fill_in 'user_email', :with => email
    fill_in 'user_password', :with => password
    click_button 'Sign in'
  end
end
Given /^I am logged in$/ do
  create_user
  sign_in
end
Given /user "([^"]*)" has joined on "([^"]*)"/ do |user_name, date|
  user = User.find_by_first_name(user_name)
  user.created_at = date.to_date
  user.save!
end
Given /^the following users exist$/ do |table|
  table.hashes.each do |hash|
    @users = User.create(hash)
    @users.save
  end
end
Given(/^I should be on the "([^"]*)" page for "(.*?)"$/) do |page, user|
  this_user = User.find_by_first_name(user) || User.find_by_email(user)
  # p user
  # p this_user.inspect
  expect(current_path).to eq path_to(page, this_user)
end
  def create_user
    create_visitor
    delete_user
    @user = FactoryGirl.create(:user, @visitor)
  end
  def sign_in
    visit new_user_session_path
    within ('#main') do
      fill_in 'user_email', :with => @visitor[:email]
      fill_in 'user_password', :with => @visitor[:password]
      click_button 'Sign in'
    end
  end
  def create_visitor
    #@visitor =FactoryGirl(:user)
    @visitor ||= { :email => "example@example.com",
                   :password => "changeme",
                   :password_confirmation => "changeme" }
  end
  def delete_user
    @user ||= User.where(:email => @visitor[:email]).first
    @user.destroy unless @user.nil?
  end
Given(/^I am on the "([^"]*)" page$/) do |page|
  visit path_to(page)
end
When(/^I click "([^"]*)"$/) do |text|
  click_link_or_button text
end
When(/^I click the "([^"]*)" button$/) do |button|
  click_link_or_button button
end
When(/^I follow "([^"]*)"$/) do |text|
  click_link text
end
When /^I fill in(?: "([^"]*)")?:$/ do |name, table|
  with_scope(name) do
    table.rows.each do |row|
      fill_in row[0], with: row[1]
    end
  end
end
Then /^I should be on the "([^"]*)" page$/ do |page|
  expect(current_path).to eq path_to(page)
end
Then /^I should see a form(?: "([^"]*)")? with:$/ do |name, table|
  with_scope(name) do
    table.rows.each do |row|
      step %Q{the "#{row[0]}" field should contain "#{row[1]}"}
    end
  end
end
Then /^I should( not)? see:$/ do |negative, table|
  expectation = negative ? :should_not : :should
  table.rows.flatten.each do |string|
    page.send(expectation, have_text(string))
  end
end
Then /^I should( not)? see "([^"]*)"$/ do |negative, string|
  unless negative
    page.should have_text string
  else
    page.should_not have_text string
  end
end
Then /^I should( not)? see button "([^"]*)"$/ do |negative, button|
  unless negative
    expect(page.has_link_or_button? button).to be_true
  else
    expect(page.has_link_or_button? button).to be_false
  end
end
Then(/^I should be on the "([^"]*)" page for ([^"]*) "([^"]*)"/) do |action, controller, title|
  expect(current_path).to eq url_for_title(action: action, controller: controller, title: title)
end
Given(/^I am on the "([^"]*)" page for ([^"]*) "([^"]*)"$/) do |action, controller, title|
  visit url_for_title(action: action, controller: controller, title: title)
end
When(/^I select "([^"]*)" to "([^"]*)"$/) do |field, option|
  find(:select, field).find(:option, option).select_option
end
When(/^I should see a selector with options$/) do |table|
  table.rows.flatten.each do |option|
    page.should have_select(:options => [option])
  end
end
  def with_scope(locator)
    locator ? within(*selector_for(locator)) { yield } : yield
  end
  def has_link_or_button?(page, name)
    page.has_link?(name) || page.has_button?(name)
  end
    def has_link_or_button?(name)
      has_link?(name) || has_button?(name)
    end
  def selector_for(locator)
    case locator

      when "the page"
        "html > body"

      # Add more mappings here.
      # Here is an example that pulls values out of the Regexp:
      #
      #  when /^the (notice|error|info) flash$/
      #    ".flash.#{$1}"

      # You can also return an array to use a different selector
      # type, like:
      #
      #  when /the header/
      #    [:xpath, "//header"]

      # This allows you to provide a quoted selector as the scope
      # for "within" steps as was previously the default for the
      # web steps:
      when "Account details"
        'form#edit_user'
      when "Contact form"
        'form#contact_form'
      when /^"(.+)"$/
        $1

      else
        raise "Can't find mapping from \"#{locator}\" to a selector.\n" +
                  "Now, go and add a mapping in #{__FILE__}"
    end
  end
def url_for_title(options)
  controller = options[:controller]
  eval("#{controller.capitalize.singularize}.find_by_title('#{options[:title]}').url_for_me(options[:action].downcase)")
end
Given(/^the following documents exist:$/) do |table|
  table.hashes.each do |hash|
    project = Document.create(hash)
    project.save
  end
end
When /^I click on the avatar for "(.*?)"$/ do | user |
  this_user = User.find_by_first_name(user) || User.find_by_email(user)
  step %Q{I follow "avatar-#{this_user.id}"}
end
And(/^I should see the avatar for "(.*?)"$/) do |user|
  this_user = User.find_by_first_name(user)
  p this_user.email
  p page.body
  expect(page).to have_xpath("//img[contains(@src, '#{gravatar_for(this_user.email)}')]")
end
And(/^I should see the avatar for "(.*?)" at (\d*?) px$/) do |user, size|
  this_user = User.find_by_first_name(user)
  expect(page).to have_xpath("//img[contains(@src, '#{gravatar_for(this_user.email, size: size)}')]")
end
When(/^I follow "([^"]*)"$/) do |text|
  click_link text
end
Then /^the "([^"]*)" field(?: within (.*))? should( not)? contain "([^"]*)"$/ do |field, parent, negative, value|
  with_scope(parent) do
    field = find_field(field)
    field_value = (field.tag_name == 'textarea') ? field.text : field.value
    field_value ||= ''
    unless negative
      field_value.should =~ /#{value}/
    else
      field_value.should_not =~ /#{value}/
    end
  end
end
  def with_scope(locator)
    locator ? within(*selector_for(locator)) { yield } : yield
  end
