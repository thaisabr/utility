Classes: 5
[name:Article, file:AgileVentures_WebsiteOne/app/models/article.rb, step:null]
[name:Document, file:AgileVentures_WebsiteOne/app/models/document.rb, step:Given ]
[name:FactoryGirl, file:null, step:Given ]
[name:Project, file:AgileVentures_WebsiteOne/app/models/project.rb, step:Given ]
[name:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]

Methods: 91
[name:+, type:Object, file:null, step:null]
[name:add, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_link_or_button, type:Object, file:null, step:Given ]
[name:click_link_or_button, type:Object, file:null, step:When ]
[name:create!, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:create_user, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:create_visitor, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:created_at, type:Article, file:AgileVentures_WebsiteOne/app/models/article.rb, step:null]
[name:created_at, type:HangoutPresenter, file:AgileVentures_WebsiteOne/app/presenters/hangout_presenter.rb, step:null]
[name:current_path, type:Object, file:null, step:Then ]
[name:current_path, type:Object, file:null, step:When ]
[name:current_state, type:Object, file:null, step:null]
[name:default_test_author, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:default_test_user_details, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:documents, type:DocumentsHelper, file:AgileVentures_WebsiteOne/app/helpers/documents_helper.rb, step:Given ]
[name:downcase, type:Object, file:null, step:When ]
[name:driver, type:Object, file:null, step:When ]
[name:each, type:Object, file:null, step:Given ]
[name:edit, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:eq, type:Object, file:null, step:Then ]
[name:estimate, type:Object, file:null, step:null]
[name:event, type:Object, file:null, step:null]
[name:except, type:Object, file:null, step:Given ]
[name:except!, type:Object, file:null, step:Given ]
[name:execute_script, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:find, type:Object, file:null, step:When ]
[name:find_by_email, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:find_by_first_name, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:find_by_title, type:Project, file:AgileVentures_WebsiteOne/app/models/project.rb, step:Given ]
[name:find_by_title, type:Document, file:AgileVentures_WebsiteOne/app/models/document.rb, step:Given ]
[name:hashes, type:Object, file:null, step:Given ]
[name:have_text, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:null]
[name:id, type:Object, file:null, step:Given ]
[name:index, type:VisitorsController, file:AgileVentures_WebsiteOne/app/controllers/visitors_controller.rb, step:null]
[name:index, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:null]
[name:index, type:UsersController, file:AgileVentures_WebsiteOne/app/controllers/users_controller.rb, step:null]
[name:index, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:index, type:DashboardController, file:AgileVentures_WebsiteOne/app/controllers/dashboard_controller.rb, step:null]
[name:index, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:null]
[name:index, type:HangoutsController, file:AgileVentures_WebsiteOne/app/controllers/hangouts_controller.rb, step:null]
[name:match, type:Object, file:null, step:Then ]
[name:merge, type:Object, file:null, step:Given ]
[name:name, type:Object, file:null, step:null]
[name:new, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:new, type:Project, file:AgileVentures_WebsiteOne/app/models/project.rb, step:Given ]
[name:new, type:Object, file:null, step:Given ]
[name:nil?, type:Object, file:null, step:Given ]
[name:owned_by, type:Object, file:null, step:null]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:When ]
[name:present?, type:Object, file:null, step:Given ]
[name:projects, type:Object, file:null, step:Given ]
[name:save!, type:Object, file:null, step:Given ]
[name:send message, type:Object, file:null, step:null]
[name:set, type:Object, file:null, step:When ]
[name:show, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:show, type:StaticPagesController, file:AgileVentures_WebsiteOne/app/controllers/static_pages_controller.rb, step:null]
[name:show, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:null]
[name:show, type:UsersController, file:AgileVentures_WebsiteOne/app/controllers/users_controller.rb, step:null]
[name:show, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:null]
[name:show, type:DocumentsController, file:AgileVentures_WebsiteOne/app/controllers/documents_controller.rb, step:null]
[name:sign in, type:Object, file:null, step:Given ]
[name:sign in, type:Object, file:null, step:null]
[name:sign up, type:Object, file:null, step:null]
[name:sign_in, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:singularize, type:Object, file:null, step:When ]
[name:story_type, type:Object, file:null, step:null]
[name:stub_request, type:Object, file:null, step:Given ]
[name:tag_list, type:Object, file:null, step:Given ]
[name:test_ip_address, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:test_user_password, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:times, type:Object, file:null, step:Given ]
[name:title, type:Article, file:AgileVentures_WebsiteOne/app/models/article.rb, step:null]
[name:to, type:Object, file:null, step:Then ]
[name:to_i, type:Object, file:null, step:Given ]
[name:to_return, type:Object, file:null, step:Given ]
[name:update, type:HangoutsController, file:AgileVentures_WebsiteOne/app/controllers/hangouts_controller.rb, step:null]
[name:update, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:null]
[name:update, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:update, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:null]
[name:update, type:RegistrationsController, file:AgileVentures_WebsiteOne/app/controllers/registrations_controller.rb, step:null]
[name:update, type:Object, file:null, step:Given ]
[name:url_for_title, type:Object, file:null, step:Then ]
[name:url_for_title, type:Object, file:null, step:Given ]
[name:user, type:Article, file:AgileVentures_WebsiteOne/app/models/article.rb, step:null]
[name:whodunnit, type:Object, file:null, step:null]
[name:within, type:Object, file:null, step:Given ]
[name:within_frame, type:Object, file:null, step:When ]

Referenced pages: 46
AgileVentures_WebsiteOne/app/views/articles/_article.html.erb
AgileVentures_WebsiteOne/app/views/articles/index.html.erb
AgileVentures_WebsiteOne/app/views/articles/show.html.erb
AgileVentures_WebsiteOne/app/views/dashboard/index.html.erb
AgileVentures_WebsiteOne/app/views/devise/registrations/edit.html.erb
AgileVentures_WebsiteOne/app/views/devise/registrations/new.html.erb
AgileVentures_WebsiteOne/app/views/devise/sessions/new.html.erb
AgileVentures_WebsiteOne/app/views/disqus/_disqus.html.erb
AgileVentures_WebsiteOne/app/views/documents/show.html.erb
AgileVentures_WebsiteOne/app/views/events/_hangouts_management.html.erb
AgileVentures_WebsiteOne/app/views/events/edit.html.erb
AgileVentures_WebsiteOne/app/views/events/index.html.erb
AgileVentures_WebsiteOne/app/views/events/new.html.erb
AgileVentures_WebsiteOne/app/views/events/show.html.erb
AgileVentures_WebsiteOne/app/views/hangouts/_hangout_button.html.erb
AgileVentures_WebsiteOne/app/views/hangouts/_hangout_status.html.erb
AgileVentures_WebsiteOne/app/views/hangouts/_hangouts.html.erb
AgileVentures_WebsiteOne/app/views/hangouts/_index_basic_info.html.erb
AgileVentures_WebsiteOne/app/views/hangouts/_index_extra_info.html.erb
AgileVentures_WebsiteOne/app/views/hangouts/_index_header.html.erb
AgileVentures_WebsiteOne/app/views/hangouts/index.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_event_link.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_flash.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_footer.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_head.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_hire_me.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_meta_tags.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_navbar.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_round_banners.html.erb
AgileVentures_WebsiteOne/app/views/projects/_activity.html.erb
AgileVentures_WebsiteOne/app/views/projects/_connections.html.erb
AgileVentures_WebsiteOne/app/views/projects/_documents_list.html.erb
AgileVentures_WebsiteOne/app/views/projects/_highlight_box.html.erb
AgileVentures_WebsiteOne/app/views/projects/_listing.html.erb
AgileVentures_WebsiteOne/app/views/projects/_members_list.html.erb
AgileVentures_WebsiteOne/app/views/projects/_videos_list.html.erb
AgileVentures_WebsiteOne/app/views/projects/index.html.erb
AgileVentures_WebsiteOne/app/views/projects/show.html.erb
AgileVentures_WebsiteOne/app/views/static_pages/show.html.erb
AgileVentures_WebsiteOne/app/views/users/_user_avatar.html.erb
AgileVentures_WebsiteOne/app/views/users/index.html.erb
AgileVentures_WebsiteOne/app/views/users/profile/_detail.html.erb
AgileVentures_WebsiteOne/app/views/users/profile/_summary.html.erb
AgileVentures_WebsiteOne/app/views/users/profile/_videos.html.erb
AgileVentures_WebsiteOne/app/views/users/show.html.erb
AgileVentures_WebsiteOne/app/views/visitors/index.html.erb

