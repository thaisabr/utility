Feature: Create and maintain projects
As a member of AgileVentures
So that I can participate in AgileVentures activities
I would like to add a new project
Background:
Given the following projects exist:
| title         | description             | pitch       | status   | github_url                                  | pivotaltracker_url                               |
| hello world   | greetings earthlings    |             | active   | https://github.com/AgileVentures/WebsiteOne | https://www.pivotaltracker.com/s/projects/742821 |
| hello mars    | greetings aliens        |             | inactive |                                             |                                                  |
| hello jupiter | greetings jupiter folks |             | active   |                                             |                                                  |
| hello mercury | greetings mercury folks |             | inactive |                                             |                                                  |
| hello saturn  | greetings saturn folks  | My pitch... | active   |                                             |                                                  |
| hello sun     | greetings sun folks     |             | active   |                                             |                                                  |
@github_query
Scenario: See total github commit count for projects
Given  I am on the "home" page
And I fetch the GitHub contribution statistics
When I follow "Projects" within the navbar
And I go to the next page
Then I should see:
| Text                    |
| hello world             |
| ACTIVE                  |
| 2795                    |
