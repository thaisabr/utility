Classes: 10
[name:FactoryGirl, file:null, step:Given ]
[name:Newsletter, file:AgileVentures_WebsiteOne/app/models/newsletter.rb, step:Given ]
[name:Newsletter, file:AgileVentures_WebsiteOne/app/models/newsletter.rb, step:Then ]
[name:Newsletter, file:AgileVentures_WebsiteOne/app/models/newsletter.rb, step:When ]
[name:Newsletter, file:AgileVentures_WebsiteOne/app/models/newsletter.rb, step:null]
[name:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:Given ]
[name:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:Then ]
[name:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:null]
[name:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:UsersController, file:AgileVentures_WebsiteOne/app/controllers/users_controller.rb, step:Given ]

Methods: 67
[name:+, type:Object, file:null, step:Given ]
[name:be_false, type:Object, file:null, step:Then ]
[name:be_true, type:Object, file:null, step:Then ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_link_or_button, type:Object, file:null, step:Given ]
[name:click_link_or_button, type:Object, file:null, step:Then ]
[name:created_at, type:EventInstancePresenter, file:AgileVentures_WebsiteOne/app/presenters/event_instance_presenter.rb, step:Given ]
[name:current_state, type:Object, file:null, step:Given ]
[name:downcase!, type:Object, file:null, step:Given ]
[name:each, type:Object, file:null, step:Given ]
[name:edit, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:Given ]
[name:edit, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Given ]
[name:edit, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:Then ]
[name:edit, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:null]
[name:estimate, type:Object, file:null, step:Given ]
[name:event, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:Then ]
[name:find_by_first_name, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:first, type:Object, file:null, step:Given ]
[name:first, type:Object, file:null, step:When ]
[name:has_button?, type:Object, file:null, step:Then ]
[name:has_link?, type:Object, file:null, step:Then ]
[name:has_link_or_button?, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Then ]
[name:hashes, type:Object, file:null, step:Given ]
[name:have_text, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:Given ]
[name:index, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:Given ]
[name:index, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Given ]
[name:index, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Given ]
[name:index, type:EventInstancesController, file:AgileVentures_WebsiteOne/app/controllers/event_instances_controller.rb, step:Given ]
[name:index, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:Then ]
[name:index, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:null]
[name:name, type:Object, file:null, step:Given ]
[name:new, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:Given ]
[name:new, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Given ]
[name:new, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:Then ]
[name:new, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:null]
[name:owned_by, type:Object, file:null, step:Given ]
[name:page, type:Object, file:null, step:Then ]
[name:pending, type:Object, file:null, step:Given ]
[name:send message, type:Object, file:null, step:Given ]
[name:show, type:UsersController, file:AgileVentures_WebsiteOne/app/controllers/users_controller.rb, step:Given ]
[name:show, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Given ]
[name:show, type:DocumentsController, file:AgileVentures_WebsiteOne/app/controllers/documents_controller.rb, step:Given ]
[name:show, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Given ]
[name:show, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:Given ]
[name:show, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:Then ]
[name:show, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:null]
[name:sign in, type:Object, file:null, step:Given ]
[name:status, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:story_type, type:Object, file:null, step:Given ]
[name:test_user_password, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:title, type:Newsletter, file:AgileVentures_WebsiteOne/app/models/newsletter.rb, step:Given ]
[name:title, type:Newsletter, file:AgileVentures_WebsiteOne/app/models/newsletter.rb, step:Then ]
[name:title, type:Newsletter, file:AgileVentures_WebsiteOne/app/models/newsletter.rb, step:null]
[name:to, type:Object, file:null, step:Then ]
[name:update, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:Given ]
[name:update, type:EventInstancesController, file:AgileVentures_WebsiteOne/app/controllers/event_instances_controller.rb, step:Given ]
[name:update, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Given ]
[name:update, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:Given ]
[name:update, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Given ]
[name:update, type:RegistrationsController, file:AgileVentures_WebsiteOne/app/controllers/registrations_controller.rb, step:Given ]
[name:where, type:Newsletter, file:AgileVentures_WebsiteOne/app/models/newsletter.rb, step:Given ]
[name:where, type:Newsletter, file:AgileVentures_WebsiteOne/app/models/newsletter.rb, step:When ]
[name:whodunnit, type:Object, file:null, step:Given ]
[name:within, type:Object, file:null, step:Given ]

Referenced pages: 37
AgileVentures_WebsiteOne/app/views/devise/registrations/edit.html.erb
AgileVentures_WebsiteOne/app/views/devise/sessions/new.html.erb
AgileVentures_WebsiteOne/app/views/disqus/_disqus.html.erb
AgileVentures_WebsiteOne/app/views/documents/show.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_hangout_button.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_hangout_status.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_hangouts.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_index_basic_info.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_index_extra_info.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_index_header.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/index.html.erb
AgileVentures_WebsiteOne/app/views/events/_hangouts_management.html.erb
AgileVentures_WebsiteOne/app/views/events/edit.html.erb
AgileVentures_WebsiteOne/app/views/events/index.html.erb
AgileVentures_WebsiteOne/app/views/events/new.html.erb
AgileVentures_WebsiteOne/app/views/events/show.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_hire_me.html.erb
AgileVentures_WebsiteOne/app/views/newsletters/_form.html.erb
AgileVentures_WebsiteOne/app/views/newsletters/edit.html.erb
AgileVentures_WebsiteOne/app/views/newsletters/index.html.erb
AgileVentures_WebsiteOne/app/views/newsletters/new.html.erb
AgileVentures_WebsiteOne/app/views/newsletters/show.html.erb
AgileVentures_WebsiteOne/app/views/projects/_activity.html.erb
AgileVentures_WebsiteOne/app/views/projects/_connections.html.erb
AgileVentures_WebsiteOne/app/views/projects/_documents_list.html.erb
AgileVentures_WebsiteOne/app/views/projects/_highlight_box.html.erb
AgileVentures_WebsiteOne/app/views/projects/_listing.html.erb
AgileVentures_WebsiteOne/app/views/projects/_members_list.html.erb
AgileVentures_WebsiteOne/app/views/projects/_videos_list.html.erb
AgileVentures_WebsiteOne/app/views/projects/index.html.erb
AgileVentures_WebsiteOne/app/views/projects/show.html.erb
AgileVentures_WebsiteOne/app/views/users/_user_avatar.html.erb
AgileVentures_WebsiteOne/app/views/users/profile/_detail.html.erb
AgileVentures_WebsiteOne/app/views/users/profile/_modal.html.erb
AgileVentures_WebsiteOne/app/views/users/profile/_summary.html.erb
AgileVentures_WebsiteOne/app/views/users/profile/_videos.html.erb
AgileVentures_WebsiteOne/app/views/users/show.html.erb

