Given(/^following events exist:$/) do |table|
  table.hashes.each do |hash|
    Event.create!(hash)
  end
end
Given(/^following events exist with active hangouts:$/) do |table|
  table.hashes.each do |hash|
    event = Event.create!(hash)
    event.event_instances.create(hangout_url: 'x@x.com',
                          updated_at: 1.minute.ago,
                          category: event.category,
                          title: event.name)
  end
end
Given(/^following hangouts exist:$/) do |table|
  table.hashes.each do |hash|
    EventInstance.create!(hash)
  end
end
Given(/^the date is "([^"]*)"$/) do |jump_date|
  Delorean.time_travel_to(Time.parse(jump_date))
end
Given /^the Hangout for event "([^"]*)" has been started with details:$/ do |event_name, table|
  ho_details = table.transpose.hashes
  hangout = ho_details[0]


  start_time = hangout['Started at'] ? hangout['Started at'] : Time.now
  event = Event.find_by_name(event_name)

  FactoryGirl.create(:event_instance, event: event,
               hangout_url: hangout['EventInstance link'],
               updated_at: start_time)
end
Given /^the following hangouts exist:$/ do |table|
  table.hashes.each do |hash|
    participants = hash['Participants'] || []
    participants = participants.split(',')

    participants = participants.map do |participant|
      break if participant.empty?
      name = participant.squish
      user = User.find_by_first_name(name)
      gplus_id = user.authentications.find_by(provider: 'gplus').try!(:uid) if user.present?
      [ "0", { :person => { displayName: "#{name}", id: gplus_id } } ]
    end

    event_instance = FactoryGirl.create(:event_instance,
                 title: hash['Title'],
                 project: Project.find_by_title(hash['Project']),
                 event: Event.find_by_name(hash['Event']),
                 category: hash['Category'],
                 user: User.find_by_first_name(hash['Host']),
                 hangout_url: hash['EventInstance url'],
                 participants: participants,
                 yt_video_id: hash['Youtube video id'],
                 created: hash['Start time'],
                 updated: hash['End time'])
  end
end
Then /^I have Slack notifications enabled$/ do
  stub_request(:post, 'https://agile-bot.herokuapp.com/hubot/hangouts-notify').to_return(status: 200)
end
Given(/^I visit "(.*?)"$/) do |path|
  visit path
end
Then /^I should see link "([^"]*)" with "([^"]*)"$/ do |link, url|
  expect(page).to have_link(link, href: url)
end
Then /^I should( not)? see:$/ do |negative, table|
  expectation = negative ? :should_not : :should
  table.rows.flatten.each do |string|
    page.send(expectation, have_text(string))
  end
end
And(/^I should see the avatar for "(.*?)"$/) do |user|
  user = User.find_by_first_name(user)
  expect(page).to have_xpath("//img[contains(@alt, '#{user.presenter.display_name}')]")
end
Given(/^the following projects exist:$/) do |table|
  #TODO YA rewrite with factoryGirl
  table.hashes.each do |hash|
    if hash[:author].present?
      u = User.find_by_first_name hash[:author]
      project = Project.new(hash.except('author', 'tags').merge(user_id: u.id))
    else
      project = default_test_author.projects.new(hash.except('author', 'tags'))
    end
    if hash[:tags]
      project.tag_list.add(hash[:tags], parse: true)
    end
    project.save!
  end
end
  def default_test_author
    @default_test_author ||= User.find_by_email(default_test_user_details[:email])
    if @default_test_author.nil?
      @default_test_author = User.create!(default_test_user_details)
    end
    @default_test_author
  end
  def default_test_user_details
    {
      first_name: 'Tester',
      last_name: 'Person',
      email: 'testuser@agileventures.org',
      last_sign_in_ip: test_ip_address,
      password: test_user_password,
      password_confirmation: test_user_password,
      display_profile: true
    }
  end
  def test_ip_address
    '127.0.0.1'
  end
  def test_user_password
    '12345678'
  end
Given /^I am logged in$/ do
  create_user
  sign_in
end
Given /^the following users exist$/ do |table|
  table.hashes.each do |attributes|
    FactoryGirl.create(:user, attributes)
  end
end
  def create_user
    @user ||= FactoryGirl.create(:user, create_visitor)
    @current_user = @user
  end
  def sign_in
    visit new_user_session_path
    within ('#main') do
      fill_in 'user_email', :with => @visitor[:email]
      fill_in 'user_password', :with => @visitor[:password]
      click_button 'Sign in'
    end
  end
  def create_visitor
    @visitor ||= { :email => 'example@example.com',
                   :password => 'changemesomeday',
                   :password_confirmation => 'changemesomeday',
                   :slug => 'slug-ma'}
  end
Given /^there are no videos$/ do
  stub_request(:get, /youtube/).to_return(body: '')
end
