Given /^I am logged in$/ do
  create_user
  sign_in
end
  def create_user
    @user ||= FactoryGirl.create(:user, create_visitor)
    @current_user = @user
  end
  def sign_in
    visit new_user_session_path
    within ('#main') do
      fill_in 'user_email', :with => @visitor[:email]
      fill_in 'user_password', :with => @visitor[:password]
      click_button 'Sign in'
    end
  end
  def create_visitor
    @visitor ||= { :email => 'example@example.com',
                   :password => 'changemesomeday',
                   :password_confirmation => 'changemesomeday',
                   :slug => 'slug-ma'}
  end
Given(/^I (?:visit|am on) the site$/) do
  visit root_path
end
When(/^I (?:go to|am on) the "([^"]*)" page$/) do |page|
  visit path_to(page)
end
When(/^I click the "([^"]*)" button$/) do |button|
  click_link_or_button button
end
Then /^I should( not)? see "([^"]*)"$/ do |negative, string|
  unless negative
    page.should have_text string
  else
    page.should_not have_text string
  end
end
Then(/^I should be on the "([^"]*)" page for ([^"]*) "([^"]*)"/) do |action, controller, title|
  expect(current_path).to eq url_for_title(action: action, controller: controller, title: title)
end
Given(/^I (?:am on|go to) the "([^"]*)" page for ([^"]*) "([^"]*)"$/) do |action, controller, title|
  visit url_for_title(action: action, controller: controller, title: title)
end
def path_to(page_name, id = '')
  name = page_name.downcase
  case name
    when 'home' then
      root_path
    when 'registration' then
      new_user_registration_path
    when 'sign in' then
      new_user_session_path
    when 'projects' then
      projects_path
    when 'new project' then
      new_project_path
    when 'articles' then
      articles_path
    when 'edit' then
      edit_project_path(id)
    when 'show' then
      project_path(id)
    when 'our members' then
      users_path
    when 'user profile' then
      user_path(id)
    when 'my account' then
      edit_user_registration_path(id)
    when 'scrums' then
      scrums_index_path
    when 'hangouts' then
      hangouts_path
    when 'foobar' then
      "/#{page}"
    when 'password reset' then
      edit_user_password_path(id)
    when 'hookups' then
      hookups_path
    when 'dashboard' then
      '/dashboard' 
    else
      raise('path to specified is not listed in #path_to')
  end
end
def url_for_title(options)
  controller = options[:controller]
  eval("#{controller.capitalize.singularize}.find_by_title('#{options[:title]}').url_for_me(options[:action].downcase)")
end
When /^(?:|I )click "([^"]*)" within Mercury Editor toolbar$/ do |button|
  selector_for = {
      'save' => 'mercury-save-button'
  }
  page.execute_script("$('.#{selector_for[button.downcase]}').click()")
  #puts 'sleep(0.1)'
  #sleep(0.1)
end
When(/^I fill in the editable field "([^"]*)" for "([^"]*)" with "([^"]*)"$/) do |field, type, s|
  page.driver.within_frame('mercury_iframe') {
    field = field.downcase.singularize
    # This selector is specific to the mercury region used!
    if field == 'title'
      find(:css, "div##{type}_title>textarea").set(s)
    elsif field == 'body'
      page.execute_script("$('##{type}_body').text('#{s}')")
    elsif field == 'pitch'
      find(:css, "#pitch_content").set(s)
    end
  }
end
Then(/^I should be in the Mercury Editor$/) do
  expect(current_path).to match(/\/editor\//i)
end
When(/^I (try to use|am using) the Mercury Editor to edit ([^"]*) "([^"]*)"$/) do |opt, model, title|
  visit "/editor#{url_for_title(action: 'show', controller: model, title: title)}"
end
When(/^I try to edit the page$/) do
  visit '/editor' + current_path
end
Given(/^the following projects exist:$/) do |table|
  #TODO YA rewrite with factoryGirl
  table.hashes.each do |hash|
    if hash[:author].present?
      u = User.find_by_first_name hash[:author]
      project = Project.new(hash.except('author', 'tags').merge(user_id: u.id))
    else
      project = default_test_author.projects.new(hash.except('author', 'tags'))
    end
    if hash[:tags]
      project.tag_list.add(hash[:tags], parse: true)
    end
    project.save!
  end
end
  def default_test_author
    @default_test_author ||= User.find_by_email(default_test_user_details[:email])
    if @default_test_author.nil?
      @default_test_author = User.create!(default_test_user_details)
    end
    @default_test_author
  end
  def default_test_user_details
    {
      first_name: 'Tester',
      last_name: 'Person',
      email: 'testuser@agileventures.org',
      last_sign_in_ip: test_ip_address,
      password: test_user_password,
      password_confirmation: test_user_password,
      display_profile: true
    }
  end
  def test_ip_address
    '127.0.0.1'
  end
  def test_user_password
    '12345678'
  end
Given /^there are no videos$/ do
  stub_request(:get, /youtube/).to_return(body: '')
end
Given(/^the following documents exist:$/) do |table|
  table.hashes.each do |hash|
    if hash[:project].present?
      hash[:project_id] = Project.find_by_title(hash[:project]).id
      hash.except! 'project'
    end
    if hash[:author].present?
      u = User.find_by_first_name hash[:author]
      hash.except! 'author'
      document = u.documents.new hash
    else
      document = default_test_author.documents.new hash
    end

    document.save!
  end
end
Given(/^the following revisions exist$/) do |table|
  table.hashes.each do |hash|
    hash[:revisions].to_i.times do |number|
      doc = Document.find_by_title(hash[:title])
      doc.update(:body => "New content #{number}")
      doc.save!
    end
  end
end
