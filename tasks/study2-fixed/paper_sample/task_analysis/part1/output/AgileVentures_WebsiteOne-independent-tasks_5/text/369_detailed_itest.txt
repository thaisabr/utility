Classes: 11
[name:ActionMailer, file:null, step:Then ]
[name:ActionMailer, file:null, step:When ]
[name:ActionMailer, file:null, step:Given ]
[name:Article, file:AgileVentures_WebsiteOne/app/models/article.rb, step:Then ]
[name:Article, file:AgileVentures_WebsiteOne/app/models/article.rb, step:Given ]
[name:Project, file:AgileVentures_WebsiteOne/app/models/project.rb, step:Then ]
[name:Project, file:AgileVentures_WebsiteOne/app/models/project.rb, step:Given ]
[name:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Then ]
[name:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Given ]
[name:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Then ]
[name:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]

Methods: 112
[name:body, type:Object, file:null, step:When ]
[name:change my password, type:Object, file:null, step:Then ]
[name:change my password, type:Object, file:null, step:null]
[name:change my password, type:Object, file:null, step:Given ]
[name:children, type:Object, file:null, step:Then ]
[name:children, type:Object, file:null, step:Given ]
[name:clear, type:Object, file:null, step:Given ]
[name:click_link_or_button, type:Object, file:null, step:When ]
[name:click_link_or_button, type:Object, file:null, step:Then ]
[name:create_test_user, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:created_at, type:Object, file:null, step:Then ]
[name:created_at, type:Object, file:null, step:Given ]
[name:current_path, type:Object, file:null, step:Then ]
[name:current_state, type:Object, file:null, step:Then ]
[name:current_state, type:Object, file:null, step:Given ]
[name:default_test_user_details, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:delete, type:Object, file:null, step:Given ]
[name:downcase, type:Object, file:null, step:Then ]
[name:driver, type:Object, file:null, step:Given ]
[name:each, type:Object, file:null, step:Given ]
[name:edit, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Then ]
[name:edit, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Then ]
[name:edit, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Given ]
[name:edit, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Given ]
[name:eq, type:Object, file:null, step:Then ]
[name:eq, type:Object, file:null, step:When ]
[name:estimate, type:Object, file:null, step:Then ]
[name:estimate, type:Object, file:null, step:Given ]
[name:event, type:Object, file:null, step:Then ]
[name:event, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:Then ]
[name:find_by_email, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Then ]
[name:find_by_first_name, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Then ]
[name:find_by_slug, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Then ]
[name:hashes, type:Object, file:null, step:Given ]
[name:have_text, type:Object, file:null, step:Then ]
[name:have_text, type:Object, file:null, step:When ]
[name:id, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:Given ]
[name:include, type:Object, file:null, step:Then ]
[name:index, type:VisitorsController, file:AgileVentures_WebsiteOne/app/controllers/visitors_controller.rb, step:Then ]
[name:index, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Then ]
[name:index, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:Then ]
[name:index, type:UsersController, file:AgileVentures_WebsiteOne/app/controllers/users_controller.rb, step:Then ]
[name:index, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Then ]
[name:index, type:VisitorsController, file:AgileVentures_WebsiteOne/app/controllers/visitors_controller.rb, step:Given ]
[name:index, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Given ]
[name:index, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:Given ]
[name:index, type:UsersController, file:AgileVentures_WebsiteOne/app/controllers/users_controller.rb, step:Given ]
[name:index, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Given ]
[name:last, type:Object, file:null, step:When ]
[name:match, type:Object, file:null, step:When ]
[name:merge, type:Object, file:null, step:Given ]
[name:name, type:Object, file:null, step:Then ]
[name:name, type:Object, file:null, step:Given ]
[name:new, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Then ]
[name:new, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Then ]
[name:new, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:new, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Given ]
[name:new, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Given ]
[name:owned_by, type:Object, file:null, step:Then ]
[name:owned_by, type:Object, file:null, step:Given ]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:When ]
[name:page, type:Object, file:null, step:Given ]
[name:path_to, type:BasicSteps, file:AgileVentures_WebsiteOne/features/step_definitions/basic_steps.rb, step:Then ]
[name:raise, type:Object, file:null, step:Then ]
[name:send message, type:Object, file:null, step:Then ]
[name:send message, type:Object, file:null, step:Given ]
[name:show, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Then ]
[name:show, type:UsersController, file:AgileVentures_WebsiteOne/app/controllers/users_controller.rb, step:Then ]
[name:show, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:Then ]
[name:show, type:DocumentsController, file:AgileVentures_WebsiteOne/app/controllers/documents_controller.rb, step:Then ]
[name:show, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Then ]
[name:show, type:StaticPagesController, file:AgileVentures_WebsiteOne/app/controllers/static_pages_controller.rb, step:Then ]
[name:show, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Given ]
[name:show, type:UsersController, file:AgileVentures_WebsiteOne/app/controllers/users_controller.rb, step:Given ]
[name:show, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:Given ]
[name:show, type:DocumentsController, file:AgileVentures_WebsiteOne/app/controllers/documents_controller.rb, step:Given ]
[name:show, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Given ]
[name:show, type:StaticPagesController, file:AgileVentures_WebsiteOne/app/controllers/static_pages_controller.rb, step:Given ]
[name:sign in, type:Object, file:null, step:Then ]
[name:sign in, type:Object, file:null, step:Given ]
[name:sign in, type:Object, file:null, step:null]
[name:sign up, type:Object, file:null, step:Then ]
[name:sign up, type:Object, file:null, step:Given ]
[name:size, type:Object, file:null, step:Then ]
[name:size, type:Object, file:null, step:When ]
[name:story_type, type:Object, file:null, step:Then ]
[name:story_type, type:Object, file:null, step:Given ]
[name:subject, type:Object, file:null, step:Then ]
[name:submit, type:Object, file:null, step:Given ]
[name:test_ip_address, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:test_user_password, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:title, type:Object, file:null, step:Then ]
[name:title, type:Object, file:null, step:Given ]
[name:to, type:Object, file:null, step:Then ]
[name:update, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:Then ]
[name:update, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Then ]
[name:update, type:HangoutsController, file:AgileVentures_WebsiteOne/app/controllers/hangouts_controller.rb, step:Then ]
[name:update, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Then ]
[name:update, type:RegistrationsController, file:AgileVentures_WebsiteOne/app/controllers/registrations_controller.rb, step:Then ]
[name:update, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:Given ]
[name:update, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Given ]
[name:update, type:HangoutsController, file:AgileVentures_WebsiteOne/app/controllers/hangouts_controller.rb, step:Given ]
[name:update, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Given ]
[name:update, type:RegistrationsController, file:AgileVentures_WebsiteOne/app/controllers/registrations_controller.rb, step:Given ]
[name:user, type:Object, file:null, step:Then ]
[name:user, type:Object, file:null, step:Given ]
[name:whodunnit, type:Object, file:null, step:Then ]
[name:whodunnit, type:Object, file:null, step:Given ]

Referenced pages: 39
AgileVentures_WebsiteOne/app/views/articles/_article.html.erb
AgileVentures_WebsiteOne/app/views/articles/index.html.erb
AgileVentures_WebsiteOne/app/views/articles/show.html.erb
AgileVentures_WebsiteOne/app/views/devise/passwords/edit.html.erb
AgileVentures_WebsiteOne/app/views/devise/registrations/edit.html.erb
AgileVentures_WebsiteOne/app/views/devise/registrations/new.html.erb
AgileVentures_WebsiteOne/app/views/devise/sessions/new.html.erb
AgileVentures_WebsiteOne/app/views/disqus/_disqus.html.erb
AgileVentures_WebsiteOne/app/views/documents/show.html.erb
AgileVentures_WebsiteOne/app/views/events/edit.html.erb
AgileVentures_WebsiteOne/app/views/events/index.html.erb
AgileVentures_WebsiteOne/app/views/events/new.html.erb
AgileVentures_WebsiteOne/app/views/events/show.html.erb
AgileVentures_WebsiteOne/app/views/hangouts/_hangout_button.html.erb
AgileVentures_WebsiteOne/app/views/hangouts/_hangout_status.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_contact_form.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_event_link.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_flash.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_footer.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_head.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_hire_me.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_meta_tags.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_navbar.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_round_banners.html.erb
AgileVentures_WebsiteOne/app/views/projects/_activity.html.erb
AgileVentures_WebsiteOne/app/views/projects/_form.html.erb
AgileVentures_WebsiteOne/app/views/projects/_listing.html.erb
AgileVentures_WebsiteOne/app/views/projects/_members_list.html.erb
AgileVentures_WebsiteOne/app/views/projects/edit.html.erb
AgileVentures_WebsiteOne/app/views/projects/index.html.erb
AgileVentures_WebsiteOne/app/views/projects/new.html.erb
AgileVentures_WebsiteOne/app/views/projects/show.html.erb
AgileVentures_WebsiteOne/app/views/static_pages/show.html.erb
AgileVentures_WebsiteOne/app/views/users/index.html.erb
AgileVentures_WebsiteOne/app/views/users/profile/_detail.html.erb
AgileVentures_WebsiteOne/app/views/users/profile/_summary.html.erb
AgileVentures_WebsiteOne/app/views/users/profile/_videos.html.erb
AgileVentures_WebsiteOne/app/views/users/show.html.erb
AgileVentures_WebsiteOne/app/views/visitors/index.html.erb

