Given(/^I visit "(.*?)"$/) do |path|
  visit path
end
When(/^I click "([^"]*)"$/) do |text|
  click_link_or_button text
end
Given(/^I am on the "([^"]*)" page for ([^"]*) "([^"]*)"$/) do |action, controller, title|
  visit url_for_title(action: action, controller: controller, title: title)
end
When(/^I refresh the page$/) do
  visit current_url
end
def url_for_title(options)
  controller = options[:controller]
  eval("#{controller.capitalize.singularize}.find_by_title('#{options[:title]}').url_for_me(options[:action].downcase)")
end
Then(/^I should see "(.*?)" tab is active$/) do |tab|
  # add timeout untill the page gets built
  Timeout::timeout(3.0) do
    until page.has_css?("##{tab}.active") do
      sleep(0.005)
    end
  end
  page.should have_css "##{tab}.active"
end
Given(/^the following projects exist:$/) do |table|
  #TODO YA rewrite with factoryGirl
  table.hashes.each do |hash|
    if hash[:author].present?
      u = User.find_by_first_name hash[:author]
      project = Project.new(hash.except('author', 'tags').merge(user_id: u.id))
    else
      project = default_test_author.projects.new(hash.except('author', 'tags'))
    end
    if hash[:tags]
      project.tag_list.add(hash[:tags], parse: true)
    end
    project.save!
  end
end
  def default_test_author
    @default_test_author ||= User.find_by_email('testuser@agileventures.org')
    if @default_test_author.nil?
      @default_test_author = User.create!(default_test_user_details)
    end
    @default_test_author
  end
  def default_test_user_details
    {
      first_name: 'Tester',
      last_name: 'Man',
      email: 'testuser@agileventures.org',
      last_sign_in_ip: test_ip_address,
      password: test_user_password,
      password_confirmation: test_user_password,
      display_profile: true
    }
  end
  def test_ip_address
    '127.0.0.1'
  end
  def test_user_password
    '12345678'
  end
Given /^there are no videos$/ do
  stub_request(:get, /youtube/).to_return(body: '')
end
Then(/^(.*) within the (.*)$/) do |s, container|
  case container.downcase
    # Bryan: when xxx for more control

    when 'mercury editor'
      page.driver.within_frame('mercury_iframe') { step(s) }

    else
      page.within(css_selector_for(container.downcase)) { step (s) }
  end
end
def css_selector_for(container)
  case container.downcase
    when 'navigation bar', 'navbar'
      '#nav'

    when 'sidebar'
      '#sidebar'

    when 'main content'
      '#main'

    when 'footer'
      '#footer'

    when 'list of projects'
      '#project-list'

    when 'modal dialog'
      '#modal-window'

    when 'mercury toolbar', 'mercury editor toolbar'
      '.mercury-toolbar-container'

    when 'mercury modal', 'mercury editor modal'
      '.mercury-modal'

    else
      pending
  end
end
Then(/^I should see 20 scrums in descending order by published date:$/) do
  dates = page.text.scan(/\d{4}-\d{2}-\d{2}/)
  clocks = page.all(:css, ".glyphicon-time")
  expect(clocks.count).to eq(20)
  expect(dates.sort { |x,y| y <=> x }).to eq(dates)
end
Then(/^I should see a modal window with the (first|second) scrum$/) do |ord|
  ord_hash ={"first" => 0, "second" => 1}
  expect(page.find("#player")[:style]).to eq("display: block; ")

  title = page.body.gsub(/\n/,'').scan(/<\/span><\/a>\s*(.*?)\s*<\/h4>/)[ord_hash[ord]]
  page.should have_selector('#playerTitle', text: title[0])
end
Then(/^I should not see a modal window$/) do
  page.evaluate_script("$('.modal').css('display')").should eq "none"
end
When(/^I click the first scrum in the timeline$/) do
  title = page.body.gsub(/\n/,'').scan(/<\/span><\/a>\s*(.*?)\s*<\/h4>/)[0]
  vid = page.body.gsub(/\n/,'').scan(/<a class=\"scrum_yt_link.*?id=\"(.*?)"/).flatten
  page.find(:xpath, "//a[@id=\"#{vid[0]}\"]").click
end
When(/^I click the second scrum in the timeline$/) do
  vid = page.body.gsub(/\n/,'').scan(/<a class=\"scrum_yt_link.*?id=\"(.*?)"/).flatten
  page.find(:xpath, "//a[@id=\"#{vid[1]}\"]").trigger('click')
  title = page.body.gsub(/\n/,'').scan(/<\/span><\/a>\s*(.*?)\s*<\/h4>/)[1]
end
When(/^I close the modal$/) do
  page.find(:css,".close").click
end
