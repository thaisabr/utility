Given(/^I (?:visit|am on) the site$/) do
  visit root_path
end
When(/^I click "([^"]*)"$/) do |text|
  click_link_or_button text
end
When(/^I click the "([^"]*)" button$/) do |button|
  click_link_or_button button
end
When(/^I dropdown the "([^"]*)" menu$/) do |text|
  within ('.navbar') do
    click_link text
  end

end
When /^I fill in event field(?: "([^"]*)")?:$/ do |name, table|
  with_scope(name) do
    table.rows.each do |row|
      within('form#event-form') do
        fill_in row[0], with: row[1]
      end
    end
  end
end
Then /^I should( not)? see "([^"]*)"$/ do |negative, string|
  unless negative
    page.should have_text string
  else
    page.should_not have_text string
  end
end
Given(/^I (?:am on|go to) the "([^"]*)" page for ([^"]*) "([^"]*)"$/) do |action, controller, title|
  visit url_for_title(action: action, controller: controller, title: title)
end
When(/^I select "([^"]*)" to "([^"]*)"$/) do |field, option|
  find(:select, field).find(:option, option).select_option
end
Then(/^I check "([^"]*)"$/) do |item|
  check item
end
Then(/^I should see a link "([^"]*)" to "([^"]*)"$/) do |text, link|
  page.should have_css "a[href='#{link}']", text: text
end
  def with_scope(locator)
    locator ? within(*selector_for(locator)) { yield } : yield
  end
  def selector_for(locator)
    case locator

      when "the page"
        "html > body"

      # Add more mappings here.
      # Here is an example that pulls values out of the Regexp:
      #
      #  when /^the (notice|error|info) flash$/
      #    ".flash.#{$1}"

      # You can also return an array to use a different selector
      # type, like:
      #
      #  when /the header/
      #    [:xpath, "//header"]

      # This allows you to provide a quoted selector as the scope
      # for "within" steps as was previously the default for the
      # web steps:
      when "Account details"
        'form#edit_user'
      when 'video description'
        '#video_contents'
      when 'player'
        '#ytplayer'
      when /^"(.+)"$/
        $1

      else
        raise "Can't find mapping from \"#{locator}\" to a selector.\n" +
                  "Now, go and add a mapping in #{__FILE__}"
    end
  end
def url_for_title(options)
  controller = options[:controller]
  eval("#{controller.capitalize.singularize}.find_by_title('#{options[:title]}').url_for_me(options[:action].downcase)")
end
When(/^I should see "([^"]*)" in footer$/) do |string|
  within('#footer') do
    page.should have_text string
  end
end
Given /^I am logged in$/ do
  create_user
  sign_in
end
  def create_user
    @user ||= FactoryGirl.create(:user, create_visitor)
    @current_user = @user
  end
  def sign_in
    visit new_user_session_path
    within ('#main') do
      fill_in 'user_email', :with => @visitor[:email]
      fill_in 'user_password', :with => @visitor[:password]
      click_button 'Sign in'
    end
  end
  def create_visitor
    @visitor ||= { :email => 'example@example.com',
                   :password => 'changemesomeday',
                   :password_confirmation => 'changemesomeday',
                   :slug => 'slug-ma'}
  end
Given(/^I am on Events index page$/) do
  visit events_path
end
Given(/^following events exist:$/) do |table|
  table.hashes.each do |hash|
    Event.create!(hash)
  end
end
Then(/^I should see multiple "([^"]*)" events$/) do |event|
  #puts Time.now
  page.all(:css, 'a', text: event, visible: false).count.should be > 1
end
Then(/^I should be on the event "([^"]*)" page for "([^"]*)"$/) do |page, name|
  event = Event.find_by_name(name)
  page.downcase!
  case page
    when 'show'
      current_path.should eq event_path(event)
    else
      current_path.should eq eval("#{page}_event_path(event)")
  end
end
Given(/^the following documents exist:$/) do |table|
  table.hashes.each do |hash|
    if hash[:project].present?
      hash[:project_id] = Project.find_by_title(hash[:project]).id
      hash.except! 'project'
    end
    if hash[:author].present?
      u = User.find_by_first_name hash[:author]
      hash.except! 'author'
      document = u.documents.new hash
    else
      document = default_test_author.documents.new hash
    end

    document.save!
  end
end
Given(/^the following revisions exist$/) do |table|
  table.hashes.each do |hash|
    hash[:revisions].to_i.times do |number|
      doc = Document.find_by_title(hash[:title])
      doc.update(:body => "New content #{number}")
      doc.save!
    end
  end
end
When(/^I should see ([^"]*) revisions for "([^"]*)"$/) do |revisions, document|
  doc = Document.find_by_title(document)
  expect doc.versions.count == revisions
  expect(page).to have_xpath('//div[@id="revisions"]/b', count: revisions)
end
When(/^I should not see any revisions$/) do
  expect(page).not_to have_css('#revisions')
end
  def default_test_author
    @default_test_author ||= User.find_by_email(default_test_user_details[:email])
    if @default_test_author.nil?
      @default_test_author = User.create!(default_test_user_details)
    end
    @default_test_author
  end
  def default_test_user_details
    {
      first_name: 'Tester',
      last_name: 'Person',
      email: 'testuser@agileventures.org',
      last_sign_in_ip: test_ip_address,
      password: test_user_password,
      password_confirmation: test_user_password,
      display_profile: true
    }
  end
  def test_ip_address
    '127.0.0.1'
  end
  def test_user_password
    '12345678'
  end
Given(/^the following projects exist:$/) do |table|
  #TODO YA rewrite with factoryGirl
  table.hashes.each do |hash|
    if hash[:author].present?
      u = User.find_by_first_name hash[:author]
      project = Project.new(hash.except('author', 'tags').merge(user_id: u.id))
    else
      project = default_test_author.projects.new(hash.except('author', 'tags'))
    end
    if hash[:tags]
      project.tag_list.add(hash[:tags], parse: true)
    end
    project.save!
  end
end
Given /^there are no videos$/ do
  stub_request(:get, /youtube/).to_return(body: '')
end
Then(/^I should see 20 scrums in descending order by published date:$/) do
  dates = page.text.scan(/\d{4}-\d{2}-\d{2}/)
  clocks = page.all(:css, '.fa-clock-o')
  expect(clocks.count).to eq(20)
  expect(dates.sort { |x,y| y <=> x }).to eq(dates)
end
Then(/^I should see a modal window with the (first|second) scrum$/) do |ord|
  ord_hash ={"first" => 0, "second" => 1}
  expect(page.find("#player")[:style]).to eq("display: block; ")
  title = page.body.gsub(/\n/,'').scan(/<\/i><\/a>\s*(.*?)\s*<\/h4>/)[ord_hash[ord]]
  page.should have_selector("#playerTitle", text: title[0])
end
Then(/^the modall should have a scrum title$/) do
  #title = page.body.gsub(/\n/,'').scan(/<\/span><\/a>\s*(.*?)\s*<\/h4>/)[ord_hash[ord]]
  #page.should have_selector('#playerTitle', text: title[0])
end
When(/^I click the second scrum in the timeline$/) do
  vid = page.body.gsub(/\n/,'').scan(/<a class=\"scrum_yt_link.*?id=\"(.*?)"/).flatten
  page.find(:xpath, "//a[@id=\"#{vid[1]}\"]").trigger('click')
  title = page.body.gsub(/\n/,'').scan(/<\/i><\/a>\s*(.*?)\s*<\/h4>/)[1]
end
When(/^I close the modal$/) do
  page.find(:css,'.close').click
end
