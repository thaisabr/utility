Classes: 7
[name:Event, file:AgileVentures_WebsiteOne/app/models/event.rb, step:Given ]
[name:Event, file:AgileVentures_WebsiteOne/app/models/event.rb, step:Then ]
[name:FactoryGirl, file:null, step:Given ]
[name:Hangout, file:AgileVentures_WebsiteOne/app/models/hangout.rb, step:Given ]
[name:Hangout, file:AgileVentures_WebsiteOne/spec/factories/hangout.rb, step:Given ]
[name:Time, file:null, step:Given ]
[name:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]

Methods: 48
[name:ago, type:Object, file:null, step:Given ]
[name:all, type:Object, file:null, step:When ]
[name:be_false, type:Object, file:null, step:Then ]
[name:be_true, type:Object, file:null, step:Then ]
[name:body, type:Object, file:null, step:Then ]
[name:category, type:Object, file:null, step:Given ]
[name:click, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:Given ]
[name:create, type:Object, file:null, step:Given ]
[name:create!, type:Event, file:AgileVentures_WebsiteOne/app/models/event.rb, step:Given ]
[name:create!, type:Hangout, file:AgileVentures_WebsiteOne/app/models/hangout.rb, step:Given ]
[name:create!, type:Hangout, file:AgileVentures_WebsiteOne/spec/factories/hangout.rb, step:Given ]
[name:create_user, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:create_visitor, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:current_path, type:Object, file:null, step:Then ]
[name:downcase, type:Object, file:null, step:Then ]
[name:downcase!, type:Object, file:null, step:Then ]
[name:each, type:Object, file:null, step:Then ]
[name:each, type:Object, file:null, step:Given ]
[name:edit, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:eq, type:Object, file:null, step:Then ]
[name:eval, type:Object, file:null, step:Then ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:find_by_name, type:Event, file:AgileVentures_WebsiteOne/app/models/event.rb, step:Then ]
[name:flatten, type:Object, file:null, step:Then ]
[name:hangouts, type:Object, file:null, step:Given ]
[name:has_field?, type:Object, file:null, step:Then ]
[name:has_link?, type:Object, file:null, step:Then ]
[name:hashes, type:Object, file:null, step:Given ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_text, type:Object, file:null, step:Then ]
[name:index, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:match, type:Object, file:null, step:Then ]
[name:name, type:Object, file:null, step:Given ]
[name:new, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:not_to, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:When ]
[name:pending, type:Object, file:null, step:Then ]
[name:rows, type:Object, file:null, step:Then ]
[name:send, type:Object, file:null, step:Then ]
[name:show, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:sign in, type:Object, file:null, step:Given ]
[name:sign_in, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:to, type:Object, file:null, step:Then ]
[name:to_i, type:Object, file:null, step:When ]
[name:within, type:Object, file:null, step:Then ]
[name:within, type:Object, file:null, step:Given ]

Referenced pages: 7
AgileVentures_WebsiteOne/app/views/devise/sessions/new.html.erb
AgileVentures_WebsiteOne/app/views/events/edit.html.erb
AgileVentures_WebsiteOne/app/views/events/index.html.erb
AgileVentures_WebsiteOne/app/views/events/new.html.erb
AgileVentures_WebsiteOne/app/views/events/show.html.erb
AgileVentures_WebsiteOne/app/views/hangouts/_hangout_button.html.erb
AgileVentures_WebsiteOne/app/views/hangouts/_hangout_status.html.erb

