Classes: 4
[name:Article, file:AgileVentures_WebsiteOne/app/models/article.rb, step:Then ]
[name:Newsletter, file:AgileVentures_WebsiteOne/app/models/newsletter.rb, step:Then ]
[name:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:Then ]
[name:UsersController, file:AgileVentures_WebsiteOne/app/controllers/users_controller.rb, step:Then ]

Methods: 55
[name:+, type:Object, file:null, step:Then ]
[name:created_at, type:Article, file:AgileVentures_WebsiteOne/app/models/article.rb, step:Then ]
[name:created_at, type:EventInstancePresenter, file:AgileVentures_WebsiteOne/app/presenters/event_instance_presenter.rb, step:Then ]
[name:current_state, type:Object, file:null, step:Then ]
[name:each, type:Object, file:null, step:Then ]
[name:edit, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Then ]
[name:edit, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:Then ]
[name:estimate, type:Object, file:null, step:Then ]
[name:event, type:Object, file:null, step:Then ]
[name:find_field, type:Object, file:null, step:Then ]
[name:have_selector, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:Then ]
[name:index, type:VisitorsController, file:AgileVentures_WebsiteOne/app/controllers/visitors_controller.rb, step:Then ]
[name:index, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Then ]
[name:index, type:UsersController, file:AgileVentures_WebsiteOne/app/controllers/users_controller.rb, step:Then ]
[name:index, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Then ]
[name:index, type:DashboardController, file:AgileVentures_WebsiteOne/app/controllers/dashboard_controller.rb, step:Then ]
[name:index, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:Then ]
[name:index, type:EventInstancesController, file:AgileVentures_WebsiteOne/app/controllers/event_instances_controller.rb, step:Then ]
[name:index, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:Then ]
[name:name, type:Object, file:null, step:Then ]
[name:new, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Then ]
[name:new, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:Then ]
[name:owned_by, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:Then ]
[name:raise, type:Object, file:null, step:Then ]
[name:rows, type:Object, file:null, step:Then ]
[name:selector_for, type:Selectors, file:AgileVentures_WebsiteOne/features/support/selectors.rb, step:Then ]
[name:send message, type:Object, file:null, step:Then ]
[name:show, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Then ]
[name:show, type:StaticPagesController, file:AgileVentures_WebsiteOne/app/controllers/static_pages_controller.rb, step:Then ]
[name:show, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:Then ]
[name:show, type:UsersController, file:AgileVentures_WebsiteOne/app/controllers/users_controller.rb, step:Then ]
[name:show, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Then ]
[name:show, type:DocumentsController, file:AgileVentures_WebsiteOne/app/controllers/documents_controller.rb, step:Then ]
[name:show, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:Then ]
[name:sign in, type:Object, file:null, step:Then ]
[name:sign up, type:Object, file:null, step:Then ]
[name:status, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Then ]
[name:story_type, type:Object, file:null, step:Then ]
[name:tag_name, type:Object, file:null, step:Then ]
[name:text, type:Object, file:null, step:Then ]
[name:title, type:Article, file:AgileVentures_WebsiteOne/app/models/article.rb, step:Then ]
[name:title, type:Newsletter, file:AgileVentures_WebsiteOne/app/models/newsletter.rb, step:Then ]
[name:update, type:EventInstancesController, file:AgileVentures_WebsiteOne/app/controllers/event_instances_controller.rb, step:Then ]
[name:update, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:Then ]
[name:update, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Then ]
[name:update, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:Then ]
[name:update, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Then ]
[name:update, type:RegistrationsController, file:AgileVentures_WebsiteOne/app/controllers/registrations_controller.rb, step:Then ]
[name:user, type:Article, file:AgileVentures_WebsiteOne/app/models/article.rb, step:Then ]
[name:value, type:Object, file:null, step:Then ]
[name:whodunnit, type:Object, file:null, step:Then ]
[name:with_scope, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Then ]
[name:within, type:Object, file:null, step:Then ]

Referenced pages: 53
AgileVentures_WebsiteOne/app/views/articles/_article.html.erb
AgileVentures_WebsiteOne/app/views/articles/index.html.erb
AgileVentures_WebsiteOne/app/views/articles/show.html.erb
AgileVentures_WebsiteOne/app/views/dashboard/index.html.erb
AgileVentures_WebsiteOne/app/views/devise/registrations/edit.html.erb
AgileVentures_WebsiteOne/app/views/devise/registrations/new.html.erb
AgileVentures_WebsiteOne/app/views/devise/sessions/new.html.erb
AgileVentures_WebsiteOne/app/views/disqus/_disqus.html.erb
AgileVentures_WebsiteOne/app/views/documents/show.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_hangout_button.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_hangout_status.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_hangouts.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_index_basic_info.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_index_extra_info.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_index_header.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/index.html.erb
AgileVentures_WebsiteOne/app/views/events/_hangouts_management.html.erb
AgileVentures_WebsiteOne/app/views/events/edit.html.erb
AgileVentures_WebsiteOne/app/views/events/index.html.erb
AgileVentures_WebsiteOne/app/views/events/new.html.erb
AgileVentures_WebsiteOne/app/views/events/show.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_event_link.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_flash.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_footer.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_head.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_hire_me.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_meta_tags.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_navbar.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_round_banners.html.erb
AgileVentures_WebsiteOne/app/views/newsletters/_form.html.erb
AgileVentures_WebsiteOne/app/views/newsletters/edit.html.erb
AgileVentures_WebsiteOne/app/views/newsletters/index.html.erb
AgileVentures_WebsiteOne/app/views/newsletters/new.html.erb
AgileVentures_WebsiteOne/app/views/newsletters/show.html.erb
AgileVentures_WebsiteOne/app/views/projects/_activity.html.erb
AgileVentures_WebsiteOne/app/views/projects/_connections.html.erb
AgileVentures_WebsiteOne/app/views/projects/_documents_list.html.erb
AgileVentures_WebsiteOne/app/views/projects/_highlight_box.html.erb
AgileVentures_WebsiteOne/app/views/projects/_listing.html.erb
AgileVentures_WebsiteOne/app/views/projects/_members_list.html.erb
AgileVentures_WebsiteOne/app/views/projects/_videos_list.html.erb
AgileVentures_WebsiteOne/app/views/projects/index.html.erb
AgileVentures_WebsiteOne/app/views/projects/show.html.erb
AgileVentures_WebsiteOne/app/views/static_pages/show.html.erb
AgileVentures_WebsiteOne/app/views/users/_user_avatar.html.erb
AgileVentures_WebsiteOne/app/views/users/_user_list.html.erb
AgileVentures_WebsiteOne/app/views/users/index.html.erb
AgileVentures_WebsiteOne/app/views/users/profile/_detail.html.erb
AgileVentures_WebsiteOne/app/views/users/profile/_modal.html.erb
AgileVentures_WebsiteOne/app/views/users/profile/_summary.html.erb
AgileVentures_WebsiteOne/app/views/users/profile/_videos.html.erb
AgileVentures_WebsiteOne/app/views/users/show.html.erb
AgileVentures_WebsiteOne/app/views/visitors/index.html.erb

