Classes: 8
[name:Article, file:AgileVentures_WebsiteOne/app/models/article.rb, step:null]
[name:FactoryGirl, file:null, step:Given ]
[name:GithubCommitsJob, file:AgileVentures_WebsiteOne/app/jobs/github_commits_job.rb, step:Given ]
[name:Newsletter, file:AgileVentures_WebsiteOne/app/models/newsletter.rb, step:null]
[name:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:null]
[name:Project, file:AgileVentures_WebsiteOne/app/models/project.rb, step:Given ]
[name:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:UsersController, file:AgileVentures_WebsiteOne/app/controllers/users_controller.rb, step:null]

Methods: 72
[name:+, type:Object, file:null, step:null]
[name:add, type:Object, file:null, step:Given ]
[name:create!, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:created_at, type:EventInstancePresenter, file:AgileVentures_WebsiteOne/app/presenters/event_instance_presenter.rb, step:null]
[name:created_at, type:Article, file:AgileVentures_WebsiteOne/app/models/article.rb, step:null]
[name:current_state, type:Object, file:null, step:null]
[name:default_test_author, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:default_test_user_details, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:each, type:Object, file:null, step:Given ]
[name:edit, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:null]
[name:edit, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:estimate, type:Object, file:null, step:null]
[name:event, type:Object, file:null, step:null]
[name:except, type:Object, file:null, step:Given ]
[name:find_by_email, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:find_by_first_name, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:find_by_title, type:Project, file:AgileVentures_WebsiteOne/app/models/project.rb, step:Given ]
[name:follow, type:Object, file:null, step:Given ]
[name:github_url, type:GithubCommitsJob, file:AgileVentures_WebsiteOne/app/jobs/github_commits_job.rb, step:null]
[name:hashes, type:Object, file:null, step:Given ]
[name:have_text, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:null]
[name:id, type:Object, file:null, step:Given ]
[name:index, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:null]
[name:index, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:null]
[name:index, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:index, type:EventInstancesController, file:AgileVentures_WebsiteOne/app/controllers/event_instances_controller.rb, step:null]
[name:index, type:VisitorsController, file:AgileVentures_WebsiteOne/app/controllers/visitors_controller.rb, step:null]
[name:index, type:UsersController, file:AgileVentures_WebsiteOne/app/controllers/users_controller.rb, step:null]
[name:index, type:DashboardController, file:AgileVentures_WebsiteOne/app/controllers/dashboard_controller.rb, step:null]
[name:index, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:null]
[name:merge, type:Object, file:null, step:Given ]
[name:name, type:Object, file:null, step:null]
[name:new, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:null]
[name:new, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:new, type:Project, file:AgileVentures_WebsiteOne/app/models/project.rb, step:Given ]
[name:new, type:Object, file:null, step:Given ]
[name:nil?, type:Object, file:null, step:Given ]
[name:owned_by, type:Object, file:null, step:null]
[name:page, type:Object, file:null, step:Then ]
[name:present?, type:Object, file:null, step:Given ]
[name:profile_link, type:UserPresenter, file:AgileVentures_WebsiteOne/app/presenters/users/user_presenter.rb, step:null]
[name:projects, type:Object, file:null, step:Given ]
[name:run, type:GithubCommitsJob, file:AgileVentures_WebsiteOne/app/jobs/github_commits_job.rb, step:Given ]
[name:save!, type:Object, file:null, step:Given ]
[name:send message, type:Object, file:null, step:null]
[name:show, type:UsersController, file:AgileVentures_WebsiteOne/app/controllers/users_controller.rb, step:null]
[name:show, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:null]
[name:show, type:DocumentsController, file:AgileVentures_WebsiteOne/app/controllers/documents_controller.rb, step:null]
[name:show, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:show, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:null]
[name:show, type:StaticPagesController, file:AgileVentures_WebsiteOne/app/controllers/static_pages_controller.rb, step:null]
[name:show, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:null]
[name:sign in, type:Object, file:null, step:null]
[name:sign up, type:Object, file:null, step:null]
[name:status, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:null]
[name:story_type, type:Object, file:null, step:null]
[name:tag_list, type:Object, file:null, step:Given ]
[name:test_ip_address, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:test_user_password, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:title, type:Newsletter, file:AgileVentures_WebsiteOne/app/models/newsletter.rb, step:null]
[name:title, type:Article, file:AgileVentures_WebsiteOne/app/models/article.rb, step:null]
[name:to, type:Object, file:null, step:Then ]
[name:to_not, type:Object, file:null, step:Then ]
[name:update, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:null]
[name:update, type:EventInstancesController, file:AgileVentures_WebsiteOne/app/controllers/event_instances_controller.rb, step:null]
[name:update, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:update, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:null]
[name:update, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:null]
[name:update, type:RegistrationsController, file:AgileVentures_WebsiteOne/app/controllers/registrations_controller.rb, step:null]
[name:user, type:Article, file:AgileVentures_WebsiteOne/app/models/article.rb, step:null]
[name:whodunnit, type:Object, file:null, step:null]

Referenced pages: 53
AgileVentures_WebsiteOne/app/views/articles/_article.html.erb
AgileVentures_WebsiteOne/app/views/articles/index.html.erb
AgileVentures_WebsiteOne/app/views/articles/show.html.erb
AgileVentures_WebsiteOne/app/views/dashboard/index.html.erb
AgileVentures_WebsiteOne/app/views/devise/registrations/edit.html.erb
AgileVentures_WebsiteOne/app/views/devise/registrations/new.html.erb
AgileVentures_WebsiteOne/app/views/devise/sessions/new.html.erb
AgileVentures_WebsiteOne/app/views/disqus/_disqus.html.erb
AgileVentures_WebsiteOne/app/views/documents/show.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_hangout_button.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_hangout_status.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_hangouts.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_index_basic_info.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_index_extra_info.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_index_header.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/index.html.erb
AgileVentures_WebsiteOne/app/views/events/edit.html.erb
AgileVentures_WebsiteOne/app/views/events/index.html.erb
AgileVentures_WebsiteOne/app/views/events/new.html.erb
AgileVentures_WebsiteOne/app/views/events/show.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_event_link.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_flash.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_footer.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_head.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_hire_me.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_meta_tags.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_navbar.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_require_users_profile.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_round_banners.html.erb
AgileVentures_WebsiteOne/app/views/newsletters/_form.html.erb
AgileVentures_WebsiteOne/app/views/newsletters/edit.html.erb
AgileVentures_WebsiteOne/app/views/newsletters/index.html.erb
AgileVentures_WebsiteOne/app/views/newsletters/new.html.erb
AgileVentures_WebsiteOne/app/views/newsletters/show.html.erb
AgileVentures_WebsiteOne/app/views/projects/_activity.html.erb
AgileVentures_WebsiteOne/app/views/projects/_connections.html.erb
AgileVentures_WebsiteOne/app/views/projects/_documents_list.html.erb
AgileVentures_WebsiteOne/app/views/projects/_highlight_box.html.erb
AgileVentures_WebsiteOne/app/views/projects/_listing.html.erb
AgileVentures_WebsiteOne/app/views/projects/_members_list.html.erb
AgileVentures_WebsiteOne/app/views/projects/_videos_list.html.erb
AgileVentures_WebsiteOne/app/views/projects/index.html.erb
AgileVentures_WebsiteOne/app/views/projects/show.html.erb
AgileVentures_WebsiteOne/app/views/static_pages/show.html.erb
AgileVentures_WebsiteOne/app/views/users/_user_avatar.html.erb
AgileVentures_WebsiteOne/app/views/users/_user_list.html.erb
AgileVentures_WebsiteOne/app/views/users/index.html.erb
AgileVentures_WebsiteOne/app/views/users/profile/_detail.html.erb
AgileVentures_WebsiteOne/app/views/users/profile/_modal.html.erb
AgileVentures_WebsiteOne/app/views/users/profile/_summary.html.erb
AgileVentures_WebsiteOne/app/views/users/profile/_videos.html.erb
AgileVentures_WebsiteOne/app/views/users/show.html.erb
AgileVentures_WebsiteOne/app/views/visitors/index.html.erb

