Classes: 11
[name:Article, file:AgileVentures_WebsiteOne/app/models/article.rb, step:Given ]
[name:Delorean, file:null, step:Given ]
[name:Event, file:AgileVentures_WebsiteOne/app/models/event.rb, step:Given ]
[name:Event, file:AgileVentures_WebsiteOne/app/models/event.rb, step:When ]
[name:FactoryGirl, file:null, step:Given ]
[name:Newsletter, file:AgileVentures_WebsiteOne/app/models/newsletter.rb, step:Given ]
[name:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:Given ]
[name:Project, file:AgileVentures_WebsiteOne/app/models/project.rb, step:Given ]
[name:Time, file:null, step:Given ]
[name:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:UsersController, file:AgileVentures_WebsiteOne/app/controllers/users_controller.rb, step:Given ]

Methods: 92
[name:+, type:Object, file:null, step:Given ]
[name:add, type:Object, file:null, step:Given ]
[name:be_visible, type:Object, file:null, step:Then ]
[name:click_button, type:Object, file:null, step:Given ]
[name:create!, type:Event, file:AgileVentures_WebsiteOne/app/models/event.rb, step:Given ]
[name:create!, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:create_user, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:create_visitor, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:created_at, type:Article, file:AgileVentures_WebsiteOne/app/models/article.rb, step:Given ]
[name:created_at, type:EventInstancePresenter, file:AgileVentures_WebsiteOne/app/presenters/event_instance_presenter.rb, step:Given ]
[name:current_state, type:Object, file:null, step:Given ]
[name:default_test_author, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:default_test_user_details, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:each, type:Object, file:null, step:Then ]
[name:each, type:Object, file:null, step:Given ]
[name:edit, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:edit, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Given ]
[name:edit, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:Given ]
[name:estimate, type:Object, file:null, step:Given ]
[name:event, type:Object, file:null, step:Given ]
[name:except, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:find, type:Object, file:null, step:Then ]
[name:find_by_email, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:find_by_first_name, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:find_by_name, type:Event, file:AgileVentures_WebsiteOne/app/models/event.rb, step:Given ]
[name:find_by_name, type:Event, file:AgileVentures_WebsiteOne/app/models/event.rb, step:When ]
[name:flatten, type:Object, file:null, step:Then ]
[name:hashes, type:Object, file:null, step:Given ]
[name:have_link, type:Object, file:null, step:Then ]
[name:have_text, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:Given ]
[name:index, type:EventInstancesController, file:AgileVentures_WebsiteOne/app/controllers/event_instances_controller.rb, step:null]
[name:index, type:VisitorsController, file:AgileVentures_WebsiteOne/app/controllers/visitors_controller.rb, step:Given ]
[name:index, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Given ]
[name:index, type:UsersController, file:AgileVentures_WebsiteOne/app/controllers/users_controller.rb, step:Given ]
[name:index, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Given ]
[name:index, type:DashboardController, file:AgileVentures_WebsiteOne/app/controllers/dashboard_controller.rb, step:Given ]
[name:index, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:Given ]
[name:index, type:EventInstancesController, file:AgileVentures_WebsiteOne/app/controllers/event_instances_controller.rb, step:Given ]
[name:index, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:Given ]
[name:merge, type:Object, file:null, step:Given ]
[name:name, type:Object, file:null, step:Given ]
[name:new, type:Project, file:AgileVentures_WebsiteOne/app/models/project.rb, step:Given ]
[name:new, type:Object, file:null, step:Given ]
[name:new, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Given ]
[name:new, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:Given ]
[name:nil?, type:Object, file:null, step:Given ]
[name:not_to, type:Object, file:null, step:Then ]
[name:owned_by, type:Object, file:null, step:Given ]
[name:page, type:Object, file:null, step:Then ]
[name:present?, type:Object, file:null, step:Given ]
[name:profile_link, type:UserPresenter, file:AgileVentures_WebsiteOne/app/presenters/users/user_presenter.rb, step:null]
[name:profile_link, type:UserPresenter, file:AgileVentures_WebsiteOne/app/presenters/users/user_presenter.rb, step:Given ]
[name:projects, type:Object, file:null, step:Given ]
[name:rows, type:Object, file:null, step:Then ]
[name:save!, type:Object, file:null, step:Given ]
[name:send, type:Object, file:null, step:Then ]
[name:send message, type:Object, file:null, step:Given ]
[name:show, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:show, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Given ]
[name:show, type:StaticPagesController, file:AgileVentures_WebsiteOne/app/controllers/static_pages_controller.rb, step:Given ]
[name:show, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:Given ]
[name:show, type:UsersController, file:AgileVentures_WebsiteOne/app/controllers/users_controller.rb, step:Given ]
[name:show, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Given ]
[name:show, type:DocumentsController, file:AgileVentures_WebsiteOne/app/controllers/documents_controller.rb, step:Given ]
[name:show, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:Given ]
[name:sign in, type:Object, file:null, step:Given ]
[name:sign up, type:Object, file:null, step:Given ]
[name:sign_in, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:status, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:story_type, type:Object, file:null, step:Given ]
[name:stub_request, type:Object, file:null, step:Given ]
[name:tag_list, type:Object, file:null, step:Given ]
[name:test_ip_address, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:test_user_password, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:title, type:Article, file:AgileVentures_WebsiteOne/app/models/article.rb, step:Given ]
[name:title, type:Newsletter, file:AgileVentures_WebsiteOne/app/models/newsletter.rb, step:Given ]
[name:to, type:Object, file:null, step:Then ]
[name:to_not, type:Object, file:null, step:Then ]
[name:to_return, type:Object, file:null, step:Given ]
[name:transpose, type:Object, file:null, step:Given ]
[name:update, type:EventInstancesController, file:AgileVentures_WebsiteOne/app/controllers/event_instances_controller.rb, step:null]
[name:update, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:Given ]
[name:update, type:EventInstancesController, file:AgileVentures_WebsiteOne/app/controllers/event_instances_controller.rb, step:Given ]
[name:update, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Given ]
[name:update, type:NewslettersController, file:AgileVentures_WebsiteOne/app/controllers/newsletters_controller.rb, step:Given ]
[name:update, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Given ]
[name:update, type:RegistrationsController, file:AgileVentures_WebsiteOne/app/controllers/registrations_controller.rb, step:Given ]
[name:user, type:Article, file:AgileVentures_WebsiteOne/app/models/article.rb, step:Given ]
[name:whodunnit, type:Object, file:null, step:Given ]
[name:within, type:Object, file:null, step:Given ]

Referenced pages: 54
AgileVentures_WebsiteOne/app/views/articles/_article.html.erb
AgileVentures_WebsiteOne/app/views/articles/index.html.erb
AgileVentures_WebsiteOne/app/views/articles/show.html.erb
AgileVentures_WebsiteOne/app/views/dashboard/index.html.erb
AgileVentures_WebsiteOne/app/views/devise/registrations/edit.html.erb
AgileVentures_WebsiteOne/app/views/devise/registrations/new.html.erb
AgileVentures_WebsiteOne/app/views/devise/sessions/new.html.erb
AgileVentures_WebsiteOne/app/views/disqus/_disqus.html.erb
AgileVentures_WebsiteOne/app/views/documents/show.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_hangout_button.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_hangout_status.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_hangouts.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_index_basic_info.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_index_extra_info.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/_index_header.html.erb
AgileVentures_WebsiteOne/app/views/event_instances/index.html.erb
AgileVentures_WebsiteOne/app/views/events/_hangouts_management.html.erb
AgileVentures_WebsiteOne/app/views/events/edit.html.erb
AgileVentures_WebsiteOne/app/views/events/index.html.erb
AgileVentures_WebsiteOne/app/views/events/new.html.erb
AgileVentures_WebsiteOne/app/views/events/show.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_event_link.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_flash.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_footer.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_head.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_hire_me.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_meta_tags.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_navbar.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_require_users_profile.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_round_banners.html.erb
AgileVentures_WebsiteOne/app/views/newsletters/_form.html.erb
AgileVentures_WebsiteOne/app/views/newsletters/edit.html.erb
AgileVentures_WebsiteOne/app/views/newsletters/index.html.erb
AgileVentures_WebsiteOne/app/views/newsletters/new.html.erb
AgileVentures_WebsiteOne/app/views/newsletters/show.html.erb
AgileVentures_WebsiteOne/app/views/projects/_activity.html.erb
AgileVentures_WebsiteOne/app/views/projects/_connections.html.erb
AgileVentures_WebsiteOne/app/views/projects/_documents_list.html.erb
AgileVentures_WebsiteOne/app/views/projects/_highlight_box.html.erb
AgileVentures_WebsiteOne/app/views/projects/_listing.html.erb
AgileVentures_WebsiteOne/app/views/projects/_members_list.html.erb
AgileVentures_WebsiteOne/app/views/projects/_videos_list.html.erb
AgileVentures_WebsiteOne/app/views/projects/index.html.erb
AgileVentures_WebsiteOne/app/views/projects/show.html.erb
AgileVentures_WebsiteOne/app/views/static_pages/show.html.erb
AgileVentures_WebsiteOne/app/views/users/_user_avatar.html.erb
AgileVentures_WebsiteOne/app/views/users/_user_list.html.erb
AgileVentures_WebsiteOne/app/views/users/index.html.erb
AgileVentures_WebsiteOne/app/views/users/profile/_detail.html.erb
AgileVentures_WebsiteOne/app/views/users/profile/_modal.html.erb
AgileVentures_WebsiteOne/app/views/users/profile/_summary.html.erb
AgileVentures_WebsiteOne/app/views/users/profile/_videos.html.erb
AgileVentures_WebsiteOne/app/views/users/show.html.erb
AgileVentures_WebsiteOne/app/views/visitors/index.html.erb

