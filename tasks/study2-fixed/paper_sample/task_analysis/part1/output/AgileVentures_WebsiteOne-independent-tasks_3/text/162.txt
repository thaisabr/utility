Feature: Events
As a site user
In order to be able to plan activities
I would like to see event CRUD functionality
Pivotal Tracker:  https://www.pivotaltracker.com/story/show/66655876
Background:
Given following events exist:
| name       | description             | category        | start_datetime              | duration                | repeats | time_zone                  |
| Scrum      | Daily scrum meeting     | Scrum           | 2014/02/03 07:00:00 UTC | 150 | never   | UTC                  |
| PP Session | Pair programming on WSO | PairProgramming | 2014/02/07 10:00:00 UTC | 15 | never   | UTC |
@time-travel-step
Scenario: Show index of events
Given the date is "2014/02/01 09:15:00 UTC"
And I am on Events index page
Then I should see "AgileVentures Events"
And I should see "Scrum"
And I should see "Starts at 7:00 AM (UTC)"
And I should see "PP Session"
And I should see "Starts at 10:00 AM (UTC)"
@javascript
Scenario: Show events which are in progress
Given the date is "2014/02/03 07:10:00 UTC"
And I am on Events index page
Then I should see "Scrum"
And I should see "Started at 7:00 AM (UTC)"
