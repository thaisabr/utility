Given /^the Hangout for event "([^"]*)" has been started with details:$/ do |event_name, table|
  ho_details = table.transpose.hashes
  hangout = ho_details[0]


  start_time = hangout['Started at'] ? hangout['Started at'] : Time.now
  event = Event.find_by_name(event_name)

  FactoryGirl.create(:event_instance, event: event,
               hangout_url: hangout['EventInstance link'],
               updated_at: start_time)
end
Then /^I should( not)? see Hangouts details section$/ do |negative|
  section = page.find('.hangout-details', visible: false)
  if negative
    expect(section).not_to be_visible
  else
    expect(section).to be_visible
  end
end
Then /^I have Slack notifications enabled$/ do
  stub_request(:post, 'https://agile-bot.herokuapp.com/hubot/hangouts-notify').to_return(status: 200)
end
Given /^the time now is "([^"]*)"$/ do |time|
  Time.stub(now: Time.parse(time))
end
Then /^I should see link "([^"]*)" with "([^"]*)"$/ do |link, url|
  expect(page).to have_link(link, href: url)
end
Then /^I should( not)? see:$/ do |negative, table|
  expectation = negative ? :should_not : :should
  table.rows.flatten.each do |string|
    page.send(expectation, have_text(string))
  end
end
Then /^I should( not)? see "([^"]*)"$/ do |negative, string|
  unless negative
    expect(page).to have_text string
  else
    expect(page).to_not have_text string
  end
end
Given(/^following events exist:$/) do |table|
  table.hashes.each do |hash|
    Event.create!(hash)
  end
end
Given(/^I am on the show page for event "([^"]*)"$/) do |name|
  event = Event.find_by_name(name)
  visit event_path(event)
end
Given(/^the date is "([^"]*)"$/) do |jump_date|
  Delorean.time_travel_to(Time.parse(jump_date))
end
Given(/^the following projects exist:$/) do |table|
  #TODO YA rewrite with factoryGirl
  table.hashes.each do |hash|
    if hash[:author].present?
      u = User.find_by_first_name hash[:author]
      project = Project.new(hash.except('author', 'tags').merge(user_id: u.id))
    else
      project = default_test_author.projects.new(hash.except('author', 'tags'))
    end
    if hash[:tags]
      project.tag_list.add(hash[:tags], parse: true)
    end
    project.save!
  end
end
Given(/^I am on the home page$/) do
  visit root_path
end
  def default_test_author
    @default_test_author ||= User.find_by_email(default_test_user_details[:email])
    if @default_test_author.nil?
      @default_test_author = User.create!(default_test_user_details)
    end
    @default_test_author
  end
  def default_test_user_details
    {
      first_name: 'Tester',
      last_name: 'Person',
      email: 'testuser@agileventures.org',
      last_sign_in_ip: test_ip_address,
      password: test_user_password,
      password_confirmation: test_user_password,
      display_profile: true,
      latitude: 59.33,
      longitude: 18.06,
      country_name: 'Stockholm',
      bio: 'Full time Tester',
      skill_list: 'Test'
    }
  end
  def test_ip_address
    '127.0.0.1'
  end
  def test_user_password
    '12345678'
  end
Given /^I am logged in$/ do
  create_user
  sign_in
end
Given /^the following users exist$/ do |table|
  table.hashes.each do |attributes|
    FactoryGirl.create(:user, attributes)
  end
end
  def create_user
    @user ||= FactoryGirl.create(:user, create_visitor)
    @current_user = @user
  end
  def sign_in
    visit new_user_session_path
    within ('#main') do
      fill_in 'user_email', :with => @visitor[:email]
      fill_in 'user_password', :with => @visitor[:password]
      click_button 'Sign in'
    end
  end
  def create_visitor
    @visitor ||= { first_name: 'Anders',
                   last_name: 'Persson',
                   email: 'example@example.com',
                   password: 'changemesomeday',
                   password_confirmation: 'changemesomeday',
                   slug: 'slug-ma',
                   country_name: 'Sweden'}
  end
Given /^there are no videos$/ do
  # No videos created
end
