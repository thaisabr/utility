Feature: Events
As a site user
In order to be able to plan activities
I would like to see event CRUD functionality
Pivotal Tracker:  https://www.pivotaltracker.com/story/show/66655876
Background:
Given following events exist:
| name       | description             | category        | start_datetime          | duration | repeats | time_zone |
| Scrum      | Daily scrum meeting     | Scrum           | 2014/02/03 07:00:00 UTC | 150      | never   | UTC       |
| PP Session | Pair programming on WSO | PairProgramming | 2014/02/07 10:00:00 UTC | 15       | never   | UTC       |
Scenario: Show an planned event when a user is not logged in
Given the date is "2014/02/01 09:15:00 UTC"
And I am on Events index page
And I click "Scrum"
Then I should see "Scrum"
And I should see "Daily scrum meeting"
And I should see "Next scheduled event"
And I should see "Monday, February 03, 2014"
And I should see "Starts at 07:00 - Ends at 09:30 (UTC)"
And I should not see "Edit"
And I should not see "Event Actions"
Scenario: Show an planned event when a user is logged in
Given I am logged in
And the date is "2014/02/01 09:15:00 UTC"
And I am on Events index page
And I click "Scrum"
Then I should see "Scrum"
And I should see "Daily scrum meeting"
And I should see "Next scheduled event"
And I should see "Monday, February 03, 2014"
And I should see "Starts at 07:00 - Ends at 09:30 (UTC)"
And I should see "Edit"
@time-travel-step
Scenario: Show an in progress event
Given I am logged in
And the date is "2014/02/03 07:15:00 UTC"
And the Hangout for event "Scrum" has been started with details:
| EventInstance link | http://hangout.test |
| Started at         | 07:00:00 UTC        |
And I am on Events index page
And I click "Scrum"
Then I should see "Scrum"
And I should see "Daily scrum meeting"
And I should not see "Starts at 07:00 - Ends at 09:30 (UTC)"
And I should see "This event is now live!"
And I should see "Join now!"
