Feature: Create and maintain projects
As a member of AgileVentures
So that I can participate in AgileVentures activities
I would like to add a new project
Background:
Given the follow projects exist:
| title       | description          | status   |
| hello world | greetings earthlings | active   |
| hello mars  | greetings aliens     | inactive |
Scenario: Edit page exists
Given I am logged in
And I am on the "projects" page
When I click the "Edit" button for project "hello mars"
Then I should be on the "Edit" page for project "hello mars"
And I should see form button "Update Project"
Scenario: Saving project edits at Edit page
Given I am logged in
When I go to the "Edit" page for project "hello mars"
When I fill in "Status" with "undetermined"
Then I click "Update Project"
And I should see "undetermined"
Scenario: Destroying a project
Given I am logged in
And I am on the "projects" page
Then the Destroy button works for "hello world"
And I should see "Project was successfully deleted."
