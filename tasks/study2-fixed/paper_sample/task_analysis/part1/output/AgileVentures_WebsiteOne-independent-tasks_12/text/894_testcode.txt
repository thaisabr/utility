Then(/^I should see a navigation bar$/) do
  within('section#header') do
    find ('div.navbar')
  end
end
Then /^I should see a button "([^"]*)"$/ do |name|
  page.should have_link name
end
Given(/^I visit the site$/) do
  visit root_path
end
Given(/^I am on the "([^"]*)" page$/) do |page|
  visit path_to(page)
end
Given(/^I go to the "([^"]*)" page$/) do |page|
  visit path_to(page)
end
Then(/^I should be on the ([^"]*) page$/) do |page|
  expect(current_path).to eq path_to(page)
end
When(/^I submit "([^"]*)" as username$/) do |email|
  fill_in('Email', :with => email)
end
When(/^I submit "([^"]*)" as password$/) do |password|
  fill_in('Password', :with => password)
  fill_in('Password confirmation', :with => password)
end
When(/^I click "([^"]*)"$/) do |text|
  click_button text
end
When(/^I follow "([^"]*)"$/) do |text|
  click_link text
end
When /^I should see "([^"]*)"$/ do |string|
  page.should have_text string
end
When(/^I should see a "([^"]*)" link$/) do |link|
  page.should have_link link
end
Then(/^show me the page$/) do
  save_and_open_page
end
When(/^I am logged in as a user$/) do
  #page.stub(:user_signed_in?).and_return(true)
end
Then(/^I should see field "([^"]*)"$/) do |field|
  page.should have_field(field)
end
Then /^I should see a form for "([^"]*)"$/ do |form_purpose|
  #TODO YA check if capybara has form lookup method
  case form_purpose
    when 'creating a new project'
      page.should have_text form_purpose
      page.should have_css('form#new_project')
  end
end
When(/^I should see button "([^"]*)"$/) do |link|
  page.should have_link link
end
When(/^I should see form button "([^"]*)"$/) do |button|
  page.should have_button button
end
And(/^I click the "(.*?)" button$/) do |button|
  click_button button
end
When(/^I fill in "([^"]*)" with "([^"]*)"$/) do |field, value|
    fill_in field, :with => value
end
def path_to(page)
  case page
    when 'home' then
      root_path
    when 'registration' then
      new_user_registration_path
    when 'sign in' then
      new_user_session_path
    when 'projects' then
      projects_path
    when 'new project' then
      new_project_path
  end
end
Given /^I am not logged in$/ do
  visit destroy_user_session_path
end
Given /^I am logged in$/ do
  create_user
  sign_in
end
Given /^I exist as a user$/ do
  create_user
end
Given /^I do not exist as a user$/ do
  create_visitor
  delete_user
end
Given /^I exist as an unconfirmed user$/ do
  create_unconfirmed_user
end
When /^I sign in with valid credentials$/ do
  create_visitor
  sign_in
end
When /^I sign out$/ do
  visit '/users/sign_out'
end
When /^I sign up with valid user data$/ do
  create_visitor
  sign_up
end
When /^I sign up with an invalid email$/ do
  create_visitor
  @visitor = @visitor.merge(:email => "notanemail")
  sign_up
end
When /^I sign up without a password confirmation$/ do
  create_visitor
  @visitor = @visitor.merge(:password_confirmation => "")
  sign_up
end
When /^I sign up without a password$/ do
  create_visitor
  @visitor = @visitor.merge(:password => "")
  sign_up
end
When /^I sign up with a mismatched password confirmation$/ do
  create_visitor
  @visitor = @visitor.merge(:password_confirmation => "changeme123")
  sign_up
end
When /^I return to the site$/ do
  visit root_path
end
When /^I sign in with a wrong email$/ do
  @visitor = @visitor.merge(:email => "wrong@example.com")
  sign_in
end
When /^I sign in with a wrong password$/ do
  @visitor = @visitor.merge(:password => "wrongpass")
  sign_in
end
When /^I edit my account details$/ do
  visit '/users/edit'
  #click_link "Edit account"
  within ('section#devise')  do
    fill_in "user_first_name", :with => "newname"
    fill_in "user_last_name", :with => "Lastname"
    fill_in "user_organization", :with => "Company"
    #fill_in "user_current_password", :with => @visitor[:password]
    click_button "Update"
  end

end
When /^I look at the list of users$/ do
  visit '/'
end
Then /^I should be signed in$/ do
  find_user.should == @user
  page.should have_content "Log out"
  page.should_not have_content "Sign up"
  page.should_not have_content "Check-in"
end
Then /^I should be signed out$/ do
  page.should have_content "Sign up"
  page.should have_content "Check in"
  page.should_not have_content "Log out"
end
Then /^I see an unconfirmed account message$/ do
  page.should have_content "You have to confirm your account before continuing."
end
Then /^I see a successful sign in message$/ do
  page.should have_content "Signed in successfully."
end
Then /^I should see a successful sign up message$/ do
  page.should have_content "Welcome! You have signed up successfully."
end
Then /^I should see an invalid email message$/ do
  page.should have_content "Email is invalid"
end
Then /^I should see a missing password message$/ do
  page.should have_content "Password can't be blank"
end
Then /^I should see a missing password confirmation message$/ do
  page.should have_content "Password confirmation doesn't match"
end
Then /^I should see a mismatched password message$/ do
  page.should have_content "Password confirmation doesn't match "
end
Then /^I should see a signed out message$/ do
  page.should have_content "Signed out successfully."
end
Then /^I see an invalid login message$/ do
  page.should have_content "Invalid email or password."
end
Then /^I should see an account edited message$/ do
  page.should have_content "You updated your account successfully."
end
Then /^I should see my name$/ do
  create_user
  page.should have_content @user[:first_name]
end
Given /^the sign in form is visible$/ do
  #expect(page).to have_form('loginForm')
  expect(page).to have_field('user_email')
  expect(page).to have_field('user_password')
  expect(page).to have_button('signin')
  #click_link 'Org Login'
end
Given(/^The database is clean$/) do
  DatabaseCleaner.clean
end
  def create_visitor
    #@visitor =FactoryGirl(:user)
    @visitor ||= {:email => "example@example.com",
                  :password => "changeme",
                  :password_confirmation => "changeme"}
  end
  def find_user
    @user ||= User.where(:email => @visitor[:email]).first
  end
  def create_unconfirmed_user
    create_visitor
    delete_user
    sign_up
    visit destroy_user_session_path
  end
  def create_user
    create_visitor
    delete_user
    @user = FactoryGirl.create(:user, @visitor)
  end
  def delete_user
    @user ||= User.where(:email => @visitor[:email]).first
    @user.destroy unless @user.nil?
  end
  def sign_up
    delete_user
    visit new_user_registration_path
    within ('#devise') do
      fill_in 'Email', :with => @visitor[:email]
      fill_in 'Password', :with => @visitor[:password]
      fill_in 'Password confirmation', :with => @visitor[:password_confirmation]
      click_button 'Sign up'
    end
    find_user
  end
  def sign_in
    visit new_user_session_path
    within ('#devise') do
      fill_in 'user_email', :with => @visitor[:email]
      fill_in 'user_password', :with => @visitor[:password]
      click_button 'Sign in'
    end
  end
  def find_user
    @user ||= User.where(:email => @visitor[:email]).first
  end
  def delete_user
    @user ||= User.where(:email => @visitor[:email]).first
    @user.destroy unless @user.nil?
  end
  def sign_up
    delete_user
    visit new_user_registration_path
    within ('#devise') do
      fill_in 'Email', :with => @visitor[:email]
      fill_in 'Password', :with => @visitor[:password]
      fill_in 'Password confirmation', :with => @visitor[:password_confirmation]
      click_button 'Sign up'
    end
    find_user
  end
  def find_user
    @user ||= User.where(:email => @visitor[:email]).first
  end
Then(/^I should see a "([^"]*)" table$/) do |legend|
  within('table#projects') do
    page.should have_css('legend', :text => legend)
  end
end
When(/^I should see column "([^"]*)"$/) do |column|
  within('table#projects thead') do
    page.should have_css('th', :text => column)
  end
end
When(/^There are projects in the database$/) do
  #TODO Y use factoryGirl
  Project.create(title: "Title 1", description: "Description 1", status: "Status 1")
  Project.create(title: "Title 2", description: "Description 2", status: "Status 2")
end
