Then(/^I should see a footer area$/) do
  page.should have_selector('section#footer')
end
Then /^I should see "Contact us" form in footer$/ do
  within('section#footer') do
    page.should have_css('form#contact_form')
    page.should have_text('Contact us')
  end
end
Then /administrator should receive email with the message/ do
  expect(ActionMailer::Base.deliveries[0].body).to include('Love your site!')
end
Then /I should receive confirmation email/ do
  expect(ActionMailer::Base.deliveries[1].to[0]).to eq('ivan@petrov.com')
  expect(ActionMailer::Base.deliveries[1].body).to include('Thank you for your feedback')
end
Given(/^I visit the site$/) do
  visit root_path
end
Given(/^I am on the "([^"]*)" page$/) do |page|
  visit path_to(page)
end
When(/^I click the "([^"]*)" button$/) do |button|
  click_link_or_button button
end
When /^I fill in(?: "([^"]*)")?:$/ do |name, table|
  with_scope(name) do
    table.rows.each do |row|
      fill_in row[0], with: row[1]
    end
  end
end
Then /^I should be on the "([^"]*)" page$/ do |page|
  expect(current_path).to eq path_to(page)
end
Then /^I should see a form(?: "([^"]*)")? with:$/ do |name, table|
  with_scope(name) do
    table.rows.each do |row|
      step %Q{the "#{row[0]}" field should contain "#{row[1]}"}
    end
  end
end
Then /^I should( not)? see "([^"]*)"$/ do |negative, string|
  unless negative
    page.should have_text string
  else
    page.should_not have_text string
  end
end
Then /^I should( not)? see button "([^"]*)"$/ do |negative, button|
  unless negative
    expect(page.has_link_or_button? button).to be_true
  else
    expect(page.has_link_or_button? button).to be_false
  end
end
  def with_scope(locator)
    locator ? within(*selector_for(locator)) { yield } : yield
  end
  def has_link_or_button?(page, name)
    page.has_link?(name) || page.has_button?(name)
  end
    def has_link_or_button?(name)
      has_link?(name) || has_button?(name)
    end
def path_to(page_name, id = '')
  name = page_name.downcase
  case name
    when 'home' then
      root_path
    when 'registration' then
      new_user_registration_path
    when 'sign in' then
      new_user_session_path
    when 'projects' then
      projects_path
    when 'new project' then
      new_project_path
    when 'edit' then
      edit_project_path(id)
    when 'show' then
      project_path(id)
    when 'our members' then
      users_index_path
    else
      raise('path to specified is not listed in #path_to')
  end
end
  def selector_for(locator)
    case locator

      when "the page"
        "html > body"

      # Add more mappings here.
      # Here is an example that pulls values out of the Regexp:
      #
      #  when /^the (notice|error|info) flash$/
      #    ".flash.#{$1}"

      # You can also return an array to use a different selector
      # type, like:
      #
      #  when /the header/
      #    [:xpath, "//header"]

      # This allows you to provide a quoted selector as the scope
      # for "within" steps as was previously the default for the
      # web steps:
      when "Account details"
        'form#edit_user'
      when "Contact form"
        'form#contact_form'
      when /^"(.+)"$/
        $1

      else
        raise "Can't find mapping from \"#{locator}\" to a selector.\n" +
                  "Now, go and add a mapping in #{__FILE__}"
    end
  end
Given /^I am logged in as user with email "([^"]*)", with password "([^"]*)"$/ do |email, password|
  @user = FactoryGirl.create(:user, email: email, password: password, password_confirmation: password )
  visit new_user_session_path
  within ('#main') do
    fill_in 'user_email', :with => email
    fill_in 'user_password', :with => password
    click_button 'Sign in'
  end
end
When(/^I click pulldown link "([^"]*)"$/) do |text|
  page.find("#user_info").click
  click_link_or_button text
end
Then /^the "([^"]*)" field(?: within (.*))? should( not)? contain "([^"]*)"$/ do |field, parent, negative, value|
  with_scope(parent) do
    field = find_field(field)
    field_value = (field.tag_name == 'textarea') ? field.text : field.value
    field_value ||= ''
    unless negative
      field_value.should =~ /#{value}/
    else
      field_value.should_not =~ /#{value}/
    end
  end
end
  def with_scope(locator)
    locator ? within(*selector_for(locator)) { yield } : yield
  end
