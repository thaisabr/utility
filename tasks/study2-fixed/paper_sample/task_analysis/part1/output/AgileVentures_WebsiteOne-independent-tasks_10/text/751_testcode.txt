Then(/^I should see a footer area$/) do
  page.should have_selector('section#footer')
end
Then /^I should see "Contact us" form in footer$/ do
  within('section#footer') do
    page.should have_css('form#contact_form')
    page.should have_text('Contact us')
  end
end
Given(/^I visit the site$/) do
  visit root_path
end
Then /^I should( not)? see "([^"]*)"$/ do |negative, string|
  unless negative
    page.should have_text string
  else
    page.should_not have_text string
  end
end
Then /^I should see link "([^"]*)"$/ do |link|
  page.should have_link link
end
