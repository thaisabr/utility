Given(/^I am on the "([^"]*)" page$/) do |page|
  visit path_to(page)
end
When(/^I click the "([^"]*)" button$/) do |button|
  click_link_or_button button
end
Then /^I should be on the "([^"]*)" page$/ do |page|
  expect(current_path).to eq path_to(page)
end
Then /^I should( not)? see button "([^"]*)"$/ do |negative, button|
  unless negative
    expect(page.has_link_or_button? button).to be_true
  else
    expect(page.has_link_or_button? button).to be_false
  end
end
def path_to(page_name, id = '')
  name = page_name.downcase
  case name
    when 'home' then
      root_path
    when 'registration' then
      new_user_registration_path
    when 'sign in' then
      new_user_session_path
    when 'projects' then
      projects_path
    when 'new project' then
      new_project_path
    when 'edit' then
      edit_project_path(id)
    when 'show' then
      project_path(id)
    when 'our members' then
      users_index_path
    when 'user profile' then
      users_show_path(id)
    when 'my accout' then
      edit_user_registration_path(id)
    else
      raise('path to specified is not listed in #path_to')
  end
end
  def has_link_or_button?(page, name)
    page.has_link?(name) || page.has_button?(name)
  end
    def has_link_or_button?(name)
      has_link?(name) || has_button?(name)
    end
When /^I click on the avatar for "(.*?)"$/ do | user |
  this_user = User.find_by_first_name(user) || User.find_by_email(user)
  step %Q{I follow "avatar-#{this_user.id}"}
end
Given /^I am logged in as user with email "([^"]*)", with password "([^"]*)"$/ do |email, password|
  @user = FactoryGirl.create(:user, email: email, password: password, password_confirmation: password )
  visit new_user_session_path
  within ('#main') do
    fill_in 'user_email', :with => email
    fill_in 'user_password', :with => password
    click_button 'Sign in'
  end
end
Given /^the following users exist$/ do |table|
  table.hashes.each do |hash|
    @users = User.create(hash)
    @users.save
  end
end
Given(/^I should be on the "([^"]*)" page for "(.*?)"$/) do |page, user|
  this_user = User.find_by_first_name(user) || User.find_by_email(user)
  # p user
  # p this_user.inspect
  expect(current_path).to eq path_to(page, this_user)
end
When(/^I follow "([^"]*)"$/) do |text|
  click_link text
end
