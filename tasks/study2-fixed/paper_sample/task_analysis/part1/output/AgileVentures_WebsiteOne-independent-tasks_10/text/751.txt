Feature: Rendering contact us form
So that I can receive feedback from users
As a site administrator
I would like to display a "contact us" form on the sites index(landing page)
https://www.pivotaltracker.com/story/show/63103292
Add functionality to add a contact us form to index page in #footer
Background:
Given I visit the site
Scenario: Rendering contact us form
Then I should see a footer area
And I should see "Contact us" form in footer
And I should see "Send a traditional email to info@agileventures.org, or use the contact form."
And I should see link "info@agileventures.org"
