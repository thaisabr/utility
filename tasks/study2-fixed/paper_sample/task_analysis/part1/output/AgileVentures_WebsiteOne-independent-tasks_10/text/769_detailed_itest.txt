Classes: 3
[name:FactoryGirl, file:null, step:Given ]
[name:Project, file:AgileVentures_WebsiteOne/app/models/project.rb, step:Given ]
[name:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]

Methods: 22
[name:click_button, type:Object, file:null, step:Given ]
[name:create, type:Project, file:AgileVentures_WebsiteOne/app/models/project.rb, step:Given ]
[name:create_user, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:create_visitor, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:delete_user, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:destroy, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:each, type:Object, file:null, step:Then ]
[name:each, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:first, type:Object, file:null, step:Given ]
[name:flatten, type:Object, file:null, step:Then ]
[name:hashes, type:Object, file:null, step:Given ]
[name:have_text, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:When ]
[name:rows, type:Object, file:null, step:Then ]
[name:save, type:Object, file:null, step:Given ]
[name:sign in, type:Object, file:null, step:Given ]
[name:sign_in, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:where, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:within, type:Object, file:null, step:Given ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 1
AgileVentures_WebsiteOne/app/views/devise/sessions/new.html.erb

