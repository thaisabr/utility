Feature: Manage Document
As a project member
So that I can share my work related to a project
I would like to be able to create and edit new documents
Background:
Given the following projects exist:
| title       | description          | status   | id |
| hello world | greetings earthlings | active   | 1  |
| hello mars  | greetings aliens     | inactive | 2  |
And the following documents exist:
| title         | body             | project_id | id | parent_id |
| Guides        | My guide to      | 2          | 33 |           |
| Documentation | My documentation | 1          | 44 |           |
Scenario: The Mercury Editor should only work for the documents
Given I am logged in
And I visit the site
When I try to edit the page
Then I should see "You do not have the right privileges to complete action."
Given I am on the "Projects" page
When I try to edit the page
Then I should see "You do not have the right privileges to complete action."
Given I am on the "Show" page for project "hello world"
When I try to edit the page
Then I should see "You do not have the right privileges to complete action."
