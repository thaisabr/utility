Classes: 10
[name:Delorean, file:null, step:Given ]
[name:Delorean, file:null, step:Then ]
[name:ENV, file:null, step:Given ]
[name:ENV, file:null, step:Then ]
[name:Event, file:AgileVentures_WebsiteOne/app/models/event.rb, step:Given ]
[name:Event, file:AgileVentures_WebsiteOne/app/models/event.rb, step:null]
[name:FactoryGirl, file:null, step:Given ]
[name:Time, file:null, step:Given ]
[name:Time, file:null, step:Then ]
[name:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]

Methods: 55
[name:all, type:Object, file:null, step:Then ]
[name:be, type:Object, file:null, step:Then ]
[name:check, type:Object, file:null, step:When ]
[name:children, type:Object, file:null, step:null]
[name:click, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_link_or_button, type:Object, file:null, step:When ]
[name:count, type:Object, file:null, step:Then ]
[name:create, type:Event, file:AgileVentures_WebsiteOne/app/models/event.rb, step:Given ]
[name:create_user, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:create_visitor, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:created_at, type:Object, file:null, step:null]
[name:current_path, type:Object, file:null, step:Then ]
[name:delete_user, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:destroy, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:each, type:Object, file:null, step:Given ]
[name:each, type:Object, file:null, step:When ]
[name:edit, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:eq, type:Object, file:null, step:Then ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find, type:Object, file:null, step:When ]
[name:first, type:Object, file:null, step:Given ]
[name:hashes, type:Object, file:null, step:Given ]
[name:have_content, type:Object, file:null, step:When ]
[name:have_text, type:Object, file:null, step:Then ]
[name:index, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:index, type:VisitorsController, file:AgileVentures_WebsiteOne/app/controllers/visitors_controller.rb, step:null]
[name:index, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:null]
[name:new, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:When ]
[name:preview, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:null]
[name:preview, type:RegistrationsController, file:AgileVentures_WebsiteOne/app/controllers/registrations_controller.rb, step:null]
[name:raise, type:Object, file:null, step:When ]
[name:rows, type:Object, file:null, step:When ]
[name:select_option, type:Object, file:null, step:When ]
[name:selector_for, type:Selectors, file:AgileVentures_WebsiteOne/features/support/selectors.rb, step:When ]
[name:show, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:show, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:null]
[name:show, type:UsersController, file:AgileVentures_WebsiteOne/app/controllers/users_controller.rb, step:null]
[name:show, type:DocumentsController, file:AgileVentures_WebsiteOne/app/controllers/documents_controller.rb, step:null]
[name:sign in, type:Object, file:null, step:Given ]
[name:sign_in, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:title, type:Object, file:null, step:null]
[name:update, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:update, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:null]
[name:update, type:DocumentsController, file:AgileVentures_WebsiteOne/app/controllers/documents_controller.rb, step:null]
[name:update, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:null]
[name:update, type:RegistrationsController, file:AgileVentures_WebsiteOne/app/controllers/registrations_controller.rb, step:null]
[name:user, type:Object, file:null, step:null]
[name:where, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:with_scope, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:When ]
[name:within, type:Object, file:null, step:Given ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 14
AgileVentures_WebsiteOne/app/views/devise/registrations/_preview.html.erb
AgileVentures_WebsiteOne/app/views/devise/registrations/edit.html.erb
AgileVentures_WebsiteOne/app/views/devise/sessions/new.html.erb
AgileVentures_WebsiteOne/app/views/documents/show.html.erb
AgileVentures_WebsiteOne/app/views/events/edit.html.erb
AgileVentures_WebsiteOne/app/views/events/index.html.erb
AgileVentures_WebsiteOne/app/views/events/new.html.erb
AgileVentures_WebsiteOne/app/views/events/show.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_round_banners.html.erb
AgileVentures_WebsiteOne/app/views/projects/_listing.html.erb
AgileVentures_WebsiteOne/app/views/projects/index.html.erb
AgileVentures_WebsiteOne/app/views/projects/show.html.erb
AgileVentures_WebsiteOne/app/views/users/show.html.erb
AgileVentures_WebsiteOne/app/views/visitors/index.html.erb

