When(/^I (?:go to|am on) the "([^"]*)" page$/) do |page|
  visit path_to(page)
end
When(/^I click "([^"]*)" button$/) do |button|
  click_button button
end
Then /^I should be on the "([^"]*)" page$/ do |page|
  expect(current_path).to eq path_to(page)
end
def path_to(page_name, id = '')
  name = page_name.downcase
  case name
    when 'home' then
      root_path
    when 'registration' then
      new_user_registration_path
    when 'sign in' then
      new_user_session_path
    when 'projects' then
      projects_path
    when 'new project' then
      new_project_path
    when 'articles' then
      articles_path
    when 'edit' then
      edit_project_path(id)
    when 'show' then
      project_path(id)
    when 'our members' then
      users_index_path
    when 'user profile' then
      users_show_path(id)
    when 'my account' then
      edit_user_registration_path(id)
    when "foobar" then
      visit ("/#{page}")

    else
      raise('path to specified is not listed in #path_to')
  end
end
Given /^I am not logged in$/ do
  step 'I sign out'
end
When(/^I submit "([^"]*)" as username$/) do |email|
  fill_in('user_email', :with => email)
end
When(/^I submit "([^"]*)" as password$/) do |password|
  fill_in('user_password', :with => password)
  fill_in('user_password_confirmation', :with => password)
end
Then /^I should see a successful sign up message$/ do
  page.should have_content "Welcome! You have signed up successfully."
end
And /^I should receive a "(.*?)" email$/ do |arg1|
  @email = ActionMailer::Base.deliveries.last
  @email.subject.should include(arg1)
  ActionMailer::Base.deliveries.size.should eq 1
end
When(/^replies to the email should go to "([^"]*)"$/) do |arg|
  @email = ActionMailer::Base.deliveries.last
  @email.reply_to.should include('info@agileventure.org')
end
When /^I sign out$/ do
  page.driver.submit :delete, destroy_user_session_path, {}
end
