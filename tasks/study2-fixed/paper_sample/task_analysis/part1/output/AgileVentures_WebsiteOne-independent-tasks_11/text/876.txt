Feature: Create and maintain projects
As a member of AgileVentures
So that I can participate in AgileVentures activities
I would like to add a new project
Background:
Given the following projects exist:
| title       | description          | status   |
| hello world | greetings earthlings | active   |
| hello mars  | greetings aliens     | inactive |
Scenario: Show New Project button if user is logged in
When I am logged in
And I am on the "projects" page
Then I should see button "New Project"
Scenario: Do not show New Project button if user is not logged in
Given I am not logged in
When I am on the "projects" page
Then I should not see button "New Project"
Scenario: Display Show, edit, delete buttons in projects table
Given I am logged in
When I go to the "projects" page
Then I should see a "List of Projects" table
And I should see button "Show"
And I should see button "Edit"
And I should see button "Destroy"
Scenario: Do not display Show, edit, delete buttons in projects table when not logged in
Given I am not logged in
When I go to the "projects" page
Then I should see a "List of Projects" table
And I should not see button "Show"
And I should not see button "Edit"
And I should not see button "Destroy"
Scenario: Saving a new project: show successful message
Given I am logged in
And I am on the "projects" page
And I follow "New Project"
When I fill in "Title" with "Title 1"
And I fill in "Description" with "Description 1"
And I fill in "Status" with "Status 1"
And I click the "Submit" button
Then I should see "Project was successfully created."
Scenario: Saving a new project: show error message
Given I am logged in
And I am on the "projects" page
And I follow "New Project"
When I fill in "Title" with ""
And I click the "Submit" button
Then I should see "Project was not saved. Please check the input."
