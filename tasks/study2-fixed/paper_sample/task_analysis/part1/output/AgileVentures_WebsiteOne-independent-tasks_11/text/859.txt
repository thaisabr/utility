Feature: Create and maintain projects
As a member of AgileVentures
So that I can participate in AgileVentures activities
I would like to add a new project
Background:
Given the following projects exist:
| title         | description           | status   |
| hello world   | greetings earthlings  | active   |
| hello mars    | greetings aliens      | inactive |
| delete-in-bg! | deleted while in use  | missing  |
Scenario: List of projects in table layout
Given  I am on the "home" page
When I follow "Our projects"
And There are projects in the database
Then I should see "List of Projects"
And I should see "Created"
And I should see "Status"
Scenario: See a list of current projects
Given  I am on the "home" page
And There are projects in the database
When I follow "Our projects"
And I should see a "Title 1" link
And I should see "Description 1"
And I should see "Status 1"
And I should see a "Title 2" link
And I should see "Description 2"
And I should see "Status 2"
Scenario: Columns in projects table
When I go to the "projects" page
Then I should see a "List of Projects" table
Scenario: Display Show, edit, delete buttons in projects table
Given I am logged in
When I go to the "projects" page
Then I should see a "List of Projects" table
And I should see button "Edit"
And I should see button "Destroy"
Scenario: Edit page has a return link
Given I am logged in
Given I am on the "Edit" page for project "hello mars"
When I click the "Back" button
Then I am redirected to the "projects" page
Scenario: Edit a project which has been deleted after rendering
Given I am logged in
And I am on the "projects" page
When I click the "Edit" button for project "delete-in-bg!"
Then I am redirected to the "projects" page
And I should see "Project not found."
