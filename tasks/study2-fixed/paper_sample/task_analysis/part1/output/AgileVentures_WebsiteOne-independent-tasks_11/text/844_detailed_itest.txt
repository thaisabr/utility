Classes: 2
[name:FactoryGirl, file:null, step:Given ]
[name:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]

Methods: 19
[name:click_button, type:Object, file:null, step:Given ]
[name:click_link_or_button, type:Object, file:null, step:When ]
[name:each, type:Object, file:null, step:Then ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:find_field, type:Object, file:null, step:Then ]
[name:have_text, type:Object, file:null, step:Then ]
[name:have_xpath, type:Object, file:null, step:Then ]
[name:index, type:VisitorsController, file:AgileVentures_WebsiteOne/app/controllers/visitors_controller.rb, step:null]
[name:page, type:Object, file:null, step:Then ]
[name:rows, type:Object, file:null, step:Then ]
[name:selector_for, type:Object, file:null, step:Then ]
[name:sign in, type:Object, file:null, step:null]
[name:tag_name, type:Object, file:null, step:Then ]
[name:text, type:Object, file:null, step:Then ]
[name:to, type:Object, file:null, step:Then ]
[name:value, type:Object, file:null, step:Then ]
[name:with_scope, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Then ]
[name:within, type:Object, file:null, step:Given ]
[name:within, type:Object, file:null, step:Then ]

Referenced pages: 3
AgileVentures_WebsiteOne/app/views/devise/sessions/new.html.erb
AgileVentures_WebsiteOne/app/views/devise/shared/_links.erb
AgileVentures_WebsiteOne/app/views/visitors/index.html.erb

