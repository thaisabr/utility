Feature: Sign in
As an existing User
So that I can use the systems all functions
I want to be able to login
Scenario: User is not signed up
Given I do not exist as a user
When I sign in with valid credentials
Then I see an invalid login message
And I should be signed out
Scenario: User signs in successfully
Given I exist as a user
And I am not logged in
When I sign in with valid credentials
Then I see a successful sign in message
When I return to the site
Then I should be signed in
Scenario Outline: User enters wrong credentials
Given I exist as a user
And I am not logged in
When I sign in with a wrong <credential>
Then I see an invalid login message
And I should be signed out
Examples:
|credential|
|password  |
|email     |
Feature: Sign out
To protect my account from unauthorized access
A signed in user
Should be able to sign out
Scenario: User signs out
Given I am logged in
When I sign out
Then I should see a signed out message
When I return to the site
Then I should be signed out
Feature: Create and maintain projects
In order to manage my account settings
As I user I would like to have a "My account" page
And I would like to be able to edit change my credentials
Background:
Given I am logged in as user with email "current@email.com", with password "12345678"
And I am on the "home" page
Scenario: Having My account page
When I click "My Account"
Then I should see "Edit your details:"
And I should see a form with:
| Field                 |                     |
| Email                 | current@email.com   |
| Password              |                     |
| Password confirmation |                     |
| Current password      |                     |
Scenario: Editing details: successful
Given I click "My Account"
And I fill in:
| Field                 | Text      |
| Email                 | a@a.com   |
| Password              | 87654321  |
| Password confirmation | 87654321  |
| Current password      | 12345678  |
When I click the "Update" button
Then I should be on the "home" page
And I should see "You updated your account successfully."
Scenario: Editing details: failure
Given I follow "My Account"
And I fill in:
| Field                 | Text      |
| Email                 | a@a.com   |
| Password              | 87654321  |
| Password confirmation | 87654321  |
| Current password      | wrong     |
When I click the "Update" button
Then I should see "There was an error updating your account."
And I should see "Current password is invalid"
Feature: Setting up basic page layout for site
As a user
So that I can navigate the site
I should see a basic navigation elements
https://www.pivotaltracker.com/story/show/63523208
Background:
Given I visit the site
Scenario: Render navigation bar
Then I should see a navigation bar
And I should see link "Our projects"
And I should see link "Check in"
And I should see link "Sign up"
Feature: As a developer
In order to be able to use the sites features
I want to register as a user
https://www.pivotaltracker.com/story/show/63047058
Background:
Given I am not logged in
Scenario: Let a visitor register as a site user
Given I am on the "registration" page
And I submit "user@example.com" as username
And I submit "password" as password
And I click the "Sign up" button
Then I should be on the "home" page
And I should see a successful sign up message
Feature: Manage Document
As a project member
So that I can share my work related to a project
I would like to be able to create and edit new documents
Background:
Given the following projects exist:
| title       | description          | status   | id |
| hello world | greetings earthlings | active   | 1  |
| hello mars  | greetings aliens     | inactive | 2  |
And the following documents exist:
| title         | body             | project_id |
| Howto         | How to start     |          1 |
| Documentation | My documentation |          1 |
| Another doc   | My content       |          2 |
| Howto 2       | My documentation |          2 |
Scenario: Create a new document
Given I am logged in
And I am on the "Documents" page for project "hello world"
When I click "New Document"
And I fill in "Title" with "New doc title"
And I fill in "Body" with "Document content"
And I click "Create Document"
Then I should see "Document was successfully created."
Scenario: Edit a document
Given I am logged in
And I am on the "Documents" page for project "hello world"
When I click the "Edit" button for document "Howto"
Then I should be on the "Edit" page for document "Howto"
When I fill in "Title" with "My new title"
And I fill in "Body" with "New document body"
And I click "Update Document"
Then I should see "Document was successfully updated."
Feature: Create and maintain projects
As a member of AgileVentures
So that I can participate in AgileVentures activities
I would like to add a new project
Background:
Given the following projects exist:
| title       | description          | status   |
| hello world | greetings earthlings | active   |
| hello mars  | greetings aliens     | inactive |
Scenario: List of projects in table layout
Given  I am on the "home" page
When I follow "Our projects"
Then I should see "List of Projects"
Then I should see:
| Text          |
| Create        |
| Status        |
Scenario: Columns in projects table
When I go to the "projects" page
Then I should see "List of Projects" table
Scenario: See a list of current projects
Given  I am on the "home" page
When I follow "Our projects"
Then I should see:
| Text                 |
| hello world          |
| greetings earthlings |
| active               |
| hello mars           |
| greetings aliens     |
| inactive             |
Scenario: Display Show, edit, delete buttons in projects table
Given I am logged in
When I go to the "projects" page
Then I should see "List of Projects" table
And I should see buttons:
| Button  |
| Documents    |
| Edit         |
| Destroy      |
Scenario: Do not display Show, edit, delete buttons in projects table when not logged in
Given I am not logged in
When I go to the "projects" page
Then I should see "List of Projects" table
And I should not see buttons:
| Button       |
| Edit         |
| Destroy      |
Scenario: Creating a new project
Given I am logged in
And I am on the "projects" page
When I click "New Project"
Then I should see "Creating a new Project"
And I should see a form with:
| Field        |   |
| Title        |   |
| Description  |   |
| Status       |   |
Scenario: Saving a new project: success
Given I am logged in
And I am on the "projects" page
And I follow "New Project"
When I fill in:
| Field        | Text              |
| Title        | Title New         |
| Description  | Description New   |
| Status       | Status New        |
And I click the "Submit" button
Then I should be on the "projects" page
And I should see:
| Text              |
| Title New         |
| Description New   |
| Status New        |
And I should see "Project was successfully created."
Scenario: Saving a new project: failure
Given I am logged in
And I am on the "projects" page
And I click "New Project"
When I fill in "Title" with ""
And I click the "Submit" button
Then I should see "Project was not saved. Please check the input."
Scenario: opens "Show" page with projects details
Given I am logged in
And I am on the "Projects" page
When I click "hello world"
Then I should see:
| Text                  |
| hello world           |
| greetings earthlings  |
| active                |
Scenario: Show page has a return link
Given I am on the "Show" page for project "hello mars"
When I click "Back"
Then I should be on the "projects" page
Scenario: opens "Edit" page with projects details
Given I am logged in
And I am on the "Projects" page
When I click the "Edit" button for project "hello world"
Then I should see a form with:
| Field        | Text                  |
| Title        | hello world           |
| Description  | greetings earthlings  |
| Status       | active                |
Scenario: Edit page has a return link
Given I am on the "Edit" page for project "hello mars"
When I click "Back"
Then I should be on the "projects" page
Scenario: Updating a project: success
Given I am on the "Edit" page for project "hello mars"
And I fill in "Description" with "Hello, Uranus!"
And I click the "Submit" button
Then I should be on the "projects" page
And I should see "Project was successfully updated."
And I should see "Hello, Uranus!"
Scenario: Saving a project: failure
Given I am on the "Edit" page for project "hello mars"
When I fill in "Title" with ""
And I click the "Submit" button
Then I should see "Project was not updated."
Scenario: Destroying a project: successful
Given I am logged in
And I am on the "projects" page
When I click the "Destroy" button for project "hello mars"
Then I should be on the "projects" page
And I should see "Project was successfully deleted."
