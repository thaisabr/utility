Given(/^I am on the "([^"]*)" page$/) do |page|
  visit path_to(page)
end
When(/^I click "([^"]*)"$/) do |text|
  click_link_or_button text
end
When(/^I click the "([^"]*)" button$/) do |button|
  click_button button
end
When(/^I follow "([^"]*)"$/) do |text|
  click_link text
end
When(/^I fill in:$/) do |table|
  table.rows.each do |row|
    fill_in row[0], with: row[1]
  end
end
When /^I accept the warning popup$/ do
  # works only with @javascript tagged scenario
  page.driver.browser.accept_js_confirms
end
Then /^I should be on the "([^"]*)" page$/ do |page|
  expect(current_path).to eq path_to(page)
end
Then /^I should see a form with:$/ do |table|
  table.rows.each do |row|
    step %Q{the "#{row[0]}" field should contain "#{row[1]}"}
  end
end
Then /^I should( not)? see "([^"]*)"$/ do |negative, string|
  unless negative
    page.should have_text string
  else
    page.should_not have_text string
  end
end
def path_to(page_name, id = '')
  name = page_name.downcase
  case name
    when 'home' then
      root_path
    when 'registration' then
      new_user_registration_path
    when 'sign in' then
      new_user_session_path
    when 'projects' then
      projects_path
    when 'new project' then
      new_project_path
    when 'edit' then
      edit_project_path(id)
    when 'show' then
      project_path(id)
    else
      raise('path to specified is not listed in #path_to')
  end
end
Given /^I have an avatar image at "([^"]*)"$/ do |link|
  @avatar_link = link
end
Given /^I am logged in as user with email "([^"]*)", with password "([^"]*)"$/ do |email, password|
  @user = FactoryGirl.create(:user, email: email, password: password, password_confirmation: password )
  visit new_user_session_path
  within ('#devise') do
    fill_in 'user_email', :with => email
    fill_in 'user_password', :with => password
    click_button 'Sign in'
  end
end
Then /^my account should be deleted$/ do
  expect(User.find_by_id(@user)).to be_false
end
Then /^I should see my avatar image$/ do
  expect(page).to have_xpath("//img[contains(@src, '#{@avatar_link}')]")
end
Then /^the "([^"]*)" field(?: within (.*))? should( not)? contain "([^"]*)"$/ do |field, parent, negative, value|
  with_scope(parent) do
    field = find_field(field)
    field_value = (field.tag_name == 'textarea') ? field.text : field.value
    field_value ||= ''
    unless negative
      field_value.should =~ /#{value}/
    else
      field_value.should_not =~ /#{value}/
    end
  end
end
  def with_scope(locator)
    locator ? within(*selector_for(locator)) { yield } : yield
  end
