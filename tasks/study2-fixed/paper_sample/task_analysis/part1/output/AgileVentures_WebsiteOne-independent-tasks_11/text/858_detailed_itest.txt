Classes: 8
[name:Document, file:AgileVentures_WebsiteOne/app/models/document.rb, step:Then ]
[name:Document, file:AgileVentures_WebsiteOne/app/models/document.rb, step:Given ]
[name:FactoryGirl, file:null, step:Given ]
[name:Project, file:AgileVentures_WebsiteOne/app/models/project.rb, step:Then ]
[name:Project, file:AgileVentures_WebsiteOne/app/models/project.rb, step:When ]
[name:Project, file:AgileVentures_WebsiteOne/app/models/project.rb, step:Given ]
[name:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Then ]
[name:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]

Methods: 72
[name:all, type:Object, file:null, step:When ]
[name:be_false, type:Object, file:null, step:Then ]
[name:be_true, type:Object, file:null, step:Then ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:Given ]
[name:click_link_or_button, type:Object, file:null, step:When ]
[name:create, type:Project, file:AgileVentures_WebsiteOne/app/models/project.rb, step:When ]
[name:create, type:Project, file:AgileVentures_WebsiteOne/app/models/project.rb, step:Given ]
[name:create, type:Document, file:AgileVentures_WebsiteOne/app/models/document.rb, step:Given ]
[name:create_user, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:create_visitor, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:current_path, type:Object, file:null, step:Then ]
[name:delete_user, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:destroy, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:downcase, type:Object, file:null, step:Then ]
[name:driver, type:Object, file:null, step:Given ]
[name:each, type:Object, file:null, step:When ]
[name:each, type:Object, file:null, step:Then ]
[name:each, type:Object, file:null, step:Given ]
[name:edit, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Then ]
[name:edit, type:DocumentsController, file:AgileVentures_WebsiteOne/app/controllers/documents_controller.rb, step:Then ]
[name:eq, type:Object, file:null, step:Then ]
[name:fill_in, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:find, type:Object, file:null, step:Then ]
[name:find, type:Object, file:null, step:When ]
[name:find_by_title, type:Project, file:AgileVentures_WebsiteOne/app/models/project.rb, step:When ]
[name:find_field, type:Object, file:null, step:Then ]
[name:first, type:Object, file:null, step:Given ]
[name:flatten, type:Object, file:null, step:Then ]
[name:has_button?, type:Object, file:null, step:Then ]
[name:has_link?, type:Object, file:null, step:Then ]
[name:has_link_or_button?, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Then ]
[name:hashes, type:Object, file:null, step:Given ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_css, type:Object, file:null, step:Then ]
[name:have_link, type:Object, file:null, step:Then ]
[name:have_text, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:When ]
[name:index, type:VisitorsController, file:AgileVentures_WebsiteOne/app/controllers/visitors_controller.rb, step:Then ]
[name:index, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Then ]
[name:index, type:DocumentsController, file:AgileVentures_WebsiteOne/app/controllers/documents_controller.rb, step:Then ]
[name:new, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Then ]
[name:new, type:DocumentsController, file:AgileVentures_WebsiteOne/app/controllers/documents_controller.rb, step:Then ]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:Given ]
[name:path_to, type:BasicSteps, file:AgileVentures_WebsiteOne/features/step_definitions/basic_steps.rb, step:Then ]
[name:raise, type:Object, file:null, step:Then ]
[name:rows, type:Object, file:null, step:When ]
[name:rows, type:Object, file:null, step:Then ]
[name:save, type:Object, file:null, step:Given ]
[name:selector_for, type:Object, file:null, step:Then ]
[name:show, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Then ]
[name:show, type:DocumentsController, file:AgileVentures_WebsiteOne/app/controllers/documents_controller.rb, step:Then ]
[name:sign in, type:Object, file:null, step:Then ]
[name:sign in, type:Object, file:null, step:Given ]
[name:sign in, type:Object, file:null, step:null]
[name:sign up, type:Object, file:null, step:Then ]
[name:sign_in, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:submit, type:Object, file:null, step:Given ]
[name:tag_name, type:Object, file:null, step:Then ]
[name:text, type:Object, file:null, step:Then ]
[name:to, type:Object, file:null, step:Then ]
[name:url_for_title, type:Object, file:null, step:Then ]
[name:value, type:Object, file:null, step:Then ]
[name:where, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:with_scope, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Then ]
[name:within, type:Object, file:null, step:Then ]
[name:within, type:Object, file:null, step:When ]
[name:within, type:Object, file:null, step:Given ]

Referenced pages: 14
AgileVentures_WebsiteOne/app/views/devise/registrations/new.html.erb
AgileVentures_WebsiteOne/app/views/devise/sessions/new.html.erb
AgileVentures_WebsiteOne/app/views/devise/shared/_links.erb
AgileVentures_WebsiteOne/app/views/documents/_form.html.erb
AgileVentures_WebsiteOne/app/views/documents/edit.html.erb
AgileVentures_WebsiteOne/app/views/documents/index.html.erb
AgileVentures_WebsiteOne/app/views/documents/new.html.erb
AgileVentures_WebsiteOne/app/views/documents/show.html.erb
AgileVentures_WebsiteOne/app/views/projects/_form.html.erb
AgileVentures_WebsiteOne/app/views/projects/edit.html.erb
AgileVentures_WebsiteOne/app/views/projects/index.html.erb
AgileVentures_WebsiteOne/app/views/projects/new.html.erb
AgileVentures_WebsiteOne/app/views/projects/show.html.erb
AgileVentures_WebsiteOne/app/views/visitors/index.html.erb

