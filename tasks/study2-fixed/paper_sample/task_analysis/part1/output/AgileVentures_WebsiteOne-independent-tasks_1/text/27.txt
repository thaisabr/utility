Feature: As a member of the Agile Ventures team
To provide employment opportunities to team members
We want to provide a "Hire Me" button for visitors to be able to contact members
Background:
Given the following users exist
| first_name  | last_name   | email                   | display_profile | display_hire_me |
| Alice       | Jones       | alice@btinternet.co.uk  |     false       |      false      |
| Bob         | Butcher     | bobb112@hotmail.com     |     true        |      true       |
Scenario: Sending a message to user with 'Hire me' button
Given I visit Bob's profile page
When I click "Hire me"
Then I should see a modal window with a form "Contact Bob Butcher"
And I fill in "f-name" with "Anonymous user"
And I fill in "f-email" with "anonymous@isp.net"
And I fill in "f-message" with "I want to hire you"
And I click the "Send message" button within the modal dialog
Then "bobb112@hotmail.com" should receive a "message from Anonymous user" email
Then I should see "Your message has been sent successfully!"
Scenario: Sending a message to user with 'Hire me' button
Given I visit Bob's profile page
When I click "Hire me"
Then I should see a modal window with a form "Contact Bob Butcher"
And I fill in "f-name" with "Anonymous user"
And I fill in "f-email" with ""
And I fill in "f-message" with "I want to hire you"
And I click the "Send message" button within the modal dialog
Then I should see "Please fill in Name, Email and Message field"
Feature: Allow Users to Sponsor other members
As a user
So that I can help someone else get premium services
I would like to be able to pay for their premium service
Background:
Given the following plans exist
| name    | id      |
| Premium | premium |
And the following users exist
| first_name | last_name | email                  | github_profile_url         | last_sign_in_ip |
| Alice      | Jones     | alice@btinternet.co.uk | http://github.com/AliceSky | 127.0.0.1       |
Scenario: User upgrades another user from free tier to premium via card
Given I am logged in
And I visit Alice's profile page
And I click "Sponsor for Premium"
And I click "Subscribe" within the card_section
When I fill in appropriate card details for premium
Then I should see "you have sponsored Alice Jones as a Premium Member"
And "alice@btinternet.co.uk" should receive a "You've been sponsored for AgileVentures Premium Membership" email
Given I visit Alice's profile page
Then I should see "Premium Member"
Then I should not see "Basic Member"
Then I should not see "Sponsor for Premium"
And I should not see "Upgrade to Premium"
Scenario: User upgrades another user from free tier to premium via PayPal
Given I am logged in
And I visit Alice's profile page
And I click "Sponsor for Premium"
Then I should see a paypal form within the paypal_section
When Paypal updates our endpoint after sponsoring Alice
Then I should see "you have sponsored Alice Jones as a Premium Member" in last_response
Given I visit Alice's profile page
And "alice@btinternet.co.uk" should receive a "You've been sponsored for AgileVentures Premium Membership" email
Then I should see "Premium Member"
Then I should not see "Basic Member"
Then I should not see "Sponsor for Premium"
And I should not see "Upgrade to Premium"
Scenario: non logged in user upgrades another user from free tier to premium
Given I visit Alice's profile page
And I click "Sponsor for Premium"
And I click "Subscribe" within the card_section
When I fill in appropriate card details for premium
Then I should see "you have sponsored Alice Jones as a Premium Member"
And "alice@btinternet.co.uk" should receive a "You've been sponsored for AgileVentures Premium Membership" email
Given I visit Alice's profile page
Then I should see "Premium Member"
Then I should not see "Basic Member"
Then I should not see "Sponsor for Premium"
And I should not see "Upgrade to Premium"
Scenario: non logged in user upgrades another user from free tier to premium via PayPal
Given I visit Alice's profile page
And I click "Sponsor for Premium"
Then I should see a paypal form within the paypal_section
When Paypal updates our endpoint after sponsoring Alice
Then I should see "you have sponsored Alice Jones as a Premium Member" in last_response
And "alice@btinternet.co.uk" should receive a "You've been sponsored for AgileVentures Premium Membership" email
Given I visit Alice's profile page
Then I should see "Premium Member"
Then I should not see "Basic Member"
Then I should not see "Sponsor for Premium"
And I should not see "Upgrade to Premium"
Feature: Subscribe Self to Premium
As a developer
So that I can get recurring professsional development support and code review
I would like to take out an AV Premium Subscription
Background:
Given the following plans exist
| name        | id          |
| Premium     | premium     |
Scenario: Pay by card
Given I visit "subscriptions/new"
And I click "Subscribe" within the card_section
When I fill in appropriate card details for premium
Then I should see "Thanks, you're now an AgileVentures Premium Member!"
And "random@morerandom.com" should receive a "Welcome to AgileVentures Premium" email
Scenario: Pay by PayPal
Given I visit "subscriptions/new"
Then I should see a paypal form within the paypal_section
When Paypal updates our endpoint
Then "sam-buyer@agileventures.org" should receive a "Welcome to AgileVentures Premium" email
And I should see "Thanks, you're now an AgileVentures Premium Member!" in last_response
Scenario: Pay by card, but encounter error
Given my card will be rejected
And I visit "/subscriptions/new"
And I click "Subscribe" within the card_section
When I fill in appropriate card details for premium
Then I should not see "Thanks, you're now an AgileVentures Premium Member!"
And I should see "The card was declined"
And "random@morerandom.com" should not receive a "Welcome to AgileVentures Premium" email
Scenario: Pay by PayPal, but encounter error
Given I visit "subscriptions/new"
Then I should see a paypal form within the paypal_section
When Paypal updates our endpoint incorrectly
Then "sam-buyer@agileventures.org" should not receive a "Welcome to AgileVentures Premium" email
And I should see "redirected" in last_response
