Given /^I am logged in as( a premium)? user with (?:name "([^"]*)", )?email "([^"]*)", with password "([^"]*)"$/ do |premium, name, email, password|
  @current_user = @user = FactoryGirl.create(:user, first_name: name, email: email, password: password, password_confirmation: password, stripe_customer: premium ? 'cus_8l47KNxEp3qMB8' : nil)
  visit new_user_session_path
  within ('#main') do
    fill_in 'user_email', :with => email
    fill_in 'user_password', :with => password
    click_button 'Sign in'
  end
end
Given /^I am logged in$/ do
  create_user
  sign_in
end
Given /^the following users exist$/ do |table|
  table.hashes.each do |attributes|
    FactoryGirl.create(:user, attributes)
  end
end
Given(/^I visit (.*)'s profile page$/) do |name|
  user = User.find_by_first_name name
  visit user_path user
end
Given(/^I am on my profile page$/) do
  visit user_path @current_user
end
  def create_user
    @user ||= FactoryGirl.create(:user, create_visitor)
    @current_user = @user
  end
  def sign_in
    visit new_user_session_path
    within ('#main') do
      fill_in 'user_email', :with => @visitor[:email]
      fill_in 'user_password', :with => @visitor[:password]
      click_button 'Sign in'
    end
  end
  def create_visitor
    @visitor ||= { first_name: 'Anders',
                   last_name: 'Persson',
                   email: 'example@example.com',
                   password: 'changemesomeday',
                   password_confirmation: 'changemesomeday',
                   slug: 'slug-ma',
                   country_name: 'Sweden'}
  end
When(/^(?:when I|I) click "([^"]*)"$/) do |text|
  click_link_or_button text
end
Then /^I should( not)? see "([^"]*)"$/ do |negative, string|
  unless negative
    expect(page).to have_text string
  else
    expect(page).to_not have_text string
  end
end
Given(/^I fill in appropriate card details for premium(?: for user with email "([^"]*)")?$/) do |email|
  stripe_iframe = all('iframe[name=stripe_checkout_app]').last
  # byebug
  email = email.present? ? email : 'random@morerandom.com'
  Capybara.within_frame stripe_iframe do
    fill_in 'Email', with: email
    fill_in 'Card number', with: '4242 4242 4242 4242'
    fill_in 'CVC', with: '123'
    fill_in 'cc-exp', with: "12/2019"
    click_button "Pay £10.00"
  end
  sleep(3)
end
