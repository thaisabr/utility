Feature:
As a visitor
In order to understand what happened when something goes wrong
I would like to have a comprehensible error message
https://www.pivotaltracker.com/s/projects/982890/stories/64956494
Background:
Given App is in production
Scenario: 404 page when visiting an invalid URL
When I visit "/foobar"
And the page should be titled "404 - Page Not Found"
And the response status should be "404"
And I should see "We're sorry, but we couldn't find the page you requested"
Scenario: 404 page when opening an invalid project
When I visit "/projects/foo-bar-project"
And the page should be titled "404 - Page Not Found"
And the response status should be "404"
And I should see "We're sorry, but we couldn't find the page you requested"
Feature: Manage Document
As a project member
So that I can share my work related to a project
I would like to be able to create and edit new documents
Background:
Given the following projects exist:
| title       | description          | status   |
| hello world | greetings earthlings | active   |
| hello mars  | greetings aliens     | inactive |
And the following documents exist:
| title         | body             | project     |
| Guides        | My guide to      | hello mars  |
| Documentation | My documentation | hello world |
And the following revisions exist
| title         | revisions  |
| Guides        | 1          |
| Documentation | 3          |
And the following sub-documents exist:
| title   | body         | created_at          | project    |
| SubDoc1 | Blog One     | created 3 days ago  | hello mars |
| SubDoc2 | Another Blog | created 10 days ago | hello mars |
Scenario: Documents children should be sorted in DESCENDING order by create date
Given the document "Guides" has a sub-document with title "SubDoc1" created 3 days ago
Given the document "Guides" has a sub-document with title "SubDoc2" created 10 days ago
Given I am on the "Show" page for document "Guides"
Then I should see the sub-documents in this order:
| SubDoc1 |
| SubDoc2 |
