Classes: 11
[name:Article, file:AgileVentures_WebsiteOne/app/models/article.rb, step:Given ]
[name:Event, file:AgileVentures_WebsiteOne/app/models/event.rb, step:Given ]
[name:Event, file:AgileVentures_WebsiteOne/app/models/event.rb, step:null]
[name:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Given ]
[name:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:FactoryGirl, file:null, step:Given ]
[name:Faker, file:null, step:Given ]
[name:Project, file:AgileVentures_WebsiteOne/app/models/project.rb, step:Given ]
[name:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Given ]
[name:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Then ]

Methods: 74
[name:all, type:Object, file:null, step:Then ]
[name:children, type:Object, file:null, step:Given ]
[name:children, type:Object, file:null, step:null]
[name:click_button, type:Object, file:null, step:Given ]
[name:collect, type:Object, file:null, step:Then ]
[name:create!, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:create_test_user, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:created_at, type:Object, file:null, step:Given ]
[name:created_at, type:Object, file:null, step:null]
[name:current_path, type:Object, file:null, step:Then ]
[name:default_test_user_details, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:delete, type:Object, file:null, step:Given ]
[name:downcase, type:Object, file:null, step:Then ]
[name:downcase!, type:Object, file:null, step:Given ]
[name:each, type:Object, file:null, step:Given ]
[name:edit, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Given ]
[name:edit, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Given ]
[name:edit, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:eq, type:Object, file:null, step:Then ]
[name:execute_script, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:find_by_email, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Then ]
[name:find_by_first_name, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Then ]
[name:hashes, type:Object, file:null, step:Given ]
[name:index, type:VisitorsController, file:AgileVentures_WebsiteOne/app/controllers/visitors_controller.rb, step:Given ]
[name:index, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Given ]
[name:index, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:Given ]
[name:index, type:UsersController, file:AgileVentures_WebsiteOne/app/controllers/users_controller.rb, step:Given ]
[name:index, type:VisitorsController, file:AgileVentures_WebsiteOne/app/controllers/visitors_controller.rb, step:null]
[name:index, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:null]
[name:merge, type:Object, file:null, step:Given ]
[name:new, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Given ]
[name:page, type:Object, file:null, step:Given ]
[name:page, type:Object, file:null, step:Then ]
[name:path_to, type:BasicSteps, file:AgileVentures_WebsiteOne/features/step_definitions/basic_steps.rb, step:Then ]
[name:pending, type:Object, file:null, step:Given ]
[name:preview, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:Given ]
[name:preview, type:RegistrationsController, file:AgileVentures_WebsiteOne/app/controllers/registrations_controller.rb, step:Given ]
[name:preview, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:null]
[name:preview, type:RegistrationsController, file:AgileVentures_WebsiteOne/app/controllers/registrations_controller.rb, step:null]
[name:raise, type:Object, file:null, step:Then ]
[name:show, type:UsersController, file:AgileVentures_WebsiteOne/app/controllers/users_controller.rb, step:Given ]
[name:show, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Given ]
[name:show, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:Given ]
[name:show, type:DocumentsController, file:AgileVentures_WebsiteOne/app/controllers/documents_controller.rb, step:Given ]
[name:show, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Given ]
[name:show, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:show, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:null]
[name:show, type:UsersController, file:AgileVentures_WebsiteOne/app/controllers/users_controller.rb, step:null]
[name:show, type:DocumentsController, file:AgileVentures_WebsiteOne/app/controllers/documents_controller.rb, step:null]
[name:sign in, type:Object, file:null, step:Given ]
[name:sign up, type:Object, file:null, step:Given ]
[name:sort, type:Object, file:null, step:Then ]
[name:split, type:Object, file:null, step:Given ]
[name:split, type:Object, file:null, step:Then ]
[name:test_ip_address, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:test_user_password, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:text, type:Object, file:null, step:Then ]
[name:title, type:Object, file:null, step:Given ]
[name:title, type:Object, file:null, step:null]
[name:to, type:Object, file:null, step:Then ]
[name:update, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:Given ]
[name:update, type:DocumentsController, file:AgileVentures_WebsiteOne/app/controllers/documents_controller.rb, step:Given ]
[name:update, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:Given ]
[name:update, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:Given ]
[name:update, type:RegistrationsController, file:AgileVentures_WebsiteOne/app/controllers/registrations_controller.rb, step:Given ]
[name:update, type:ArticlesController, file:AgileVentures_WebsiteOne/app/controllers/articles_controller.rb, step:null]
[name:update, type:DocumentsController, file:AgileVentures_WebsiteOne/app/controllers/documents_controller.rb, step:null]
[name:update, type:EventsController, file:AgileVentures_WebsiteOne/app/controllers/events_controller.rb, step:null]
[name:update, type:ProjectsController, file:AgileVentures_WebsiteOne/app/controllers/projects_controller.rb, step:null]
[name:update, type:RegistrationsController, file:AgileVentures_WebsiteOne/app/controllers/registrations_controller.rb, step:null]
[name:user, type:Object, file:null, step:Given ]
[name:user, type:Object, file:null, step:null]
[name:within, type:Object, file:null, step:Given ]

Referenced pages: 20
AgileVentures_WebsiteOne/app/views/articles/_article.html.erb
AgileVentures_WebsiteOne/app/views/articles/index.html.erb
AgileVentures_WebsiteOne/app/views/articles/show.html.erb
AgileVentures_WebsiteOne/app/views/devise/registrations/_preview.html.erb
AgileVentures_WebsiteOne/app/views/devise/registrations/edit.html.erb
AgileVentures_WebsiteOne/app/views/devise/registrations/new.html.erb
AgileVentures_WebsiteOne/app/views/devise/sessions/new.html.erb
AgileVentures_WebsiteOne/app/views/documents/show.html.erb
AgileVentures_WebsiteOne/app/views/events/edit.html.erb
AgileVentures_WebsiteOne/app/views/events/show.html.erb
AgileVentures_WebsiteOne/app/views/layouts/_round_banners.html.erb
AgileVentures_WebsiteOne/app/views/projects/_form.html.erb
AgileVentures_WebsiteOne/app/views/projects/_listing.html.erb
AgileVentures_WebsiteOne/app/views/projects/edit.html.erb
AgileVentures_WebsiteOne/app/views/projects/index.html.erb
AgileVentures_WebsiteOne/app/views/projects/new.html.erb
AgileVentures_WebsiteOne/app/views/projects/show.html.erb
AgileVentures_WebsiteOne/app/views/users/index.html.erb
AgileVentures_WebsiteOne/app/views/users/show.html.erb
AgileVentures_WebsiteOne/app/views/visitors/index.html.erb

