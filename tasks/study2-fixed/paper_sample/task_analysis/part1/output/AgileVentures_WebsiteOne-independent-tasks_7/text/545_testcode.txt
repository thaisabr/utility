Given /^I am not logged in$/ do
  step 'I sign out'
end
Given /^I am logged in$/ do
  create_user
  sign_in
end
Given /^the following users exist$/ do |table|
  table.hashes.each do |attributes|
    create_test_user(attributes)
  end
end
Given(/^I visit (.*)'s profile page$/) do |name|
  user = User.find_by_first_name name
  visit users_show_path user
end
  def create_test_user(options = {})
    skills = options.delete "skills"
    options = default_test_user_details.merge options
    user = User.new(options)
    user.skill_list = skills
    user.save!
  end
  def create_user
    create_visitor
    delete_user
    @user = FactoryGirl.create(:user, @visitor)
    @current_user = @user
  end
  def sign_in
    visit new_user_session_path
    within ('#main') do
      fill_in 'user_email', :with => @visitor[:email]
      fill_in 'user_password', :with => @visitor[:password]
      click_button 'Sign in'
    end
  end
  def default_test_user_details
    {
        email: Faker::Internet.email,
        last_sign_in_ip: test_ip_address,
        password: test_user_password,
        password_confirmation: test_user_password,
        display_profile: true
    }
  end
  def create_visitor
    @visitor ||= { :email => 'example@example.com',
                   :password => 'changemesomeday',
                   :password_confirmation => 'changemesomeday',
                   :slug => 'slug-ma'}
  end
  def delete_user
    @user ||= User.where(:email => @visitor[:email]).first
    @user.destroy unless @user.nil?
  end
  def test_ip_address
    '127.0.0.1'
  end
  def test_user_password
    '12345678'
  end
Given(/^I (?:visit|am on) the site$/) do
  visit root_path
end
Given(/^I visit "(.*?)"$/) do |path|
  visit path
end
When(/^I (?:go to|am on) the "([^"]*)" page$/) do |page|
  visit path_to(page)
end
When(/^I click "([^"]*)"$/) do |text|
  click_link_or_button text
end
When(/^I fill in "([^"]*)" with "([^"]*)"$/) do |field, value|
  fill_in field, :with => value
end
When /^I accept the warning popup$/ do
  # works only with webkit javascript drivers
  page.driver.browser.accept_js_confirms
end
Then /^I should( not)? see "([^"]*)"$/ do |negative, string|
  unless negative
    page.should have_text string
  else
    page.should_not have_text string
  end
end
Then(/^I should be on the "([^"]*)" page for ([^"]*) "([^"]*)"/) do |action, controller, title|
  expect(current_path).to eq url_for_title(action: action, controller: controller, title: title)
end
Given(/^I am on the "([^"]*)" page for ([^"]*) "([^"]*)"$/) do |action, controller, title|
  visit url_for_title(action: action, controller: controller, title: title)
end
When(/^I refresh the page$/) do
  visit current_url
end
def path_to(page_name, id = '')
  name = page_name.downcase
  case name
    when 'home' then
      root_path
    when 'registration' then
      new_user_registration_path
    when 'sign in' then
      new_user_session_path
    when 'projects' then
      projects_path
    when 'new project' then
      new_project_path
    when 'articles' then
      articles_path
    when 'edit' then
      edit_project_path(id)
    when 'show' then
      project_path(id)
    when 'our members' then
      users_index_path
    when 'user profile' then
      users_show_path(id)
    when 'my account' then
      edit_user_registration_path(id)
    when 'foobar' then
      visit ("/#{page}")
    when 'supporters' then
      page_path('sponsors')
    else
      raise('path to specified is not listed in #path_to')
  end
end
def url_for_title(options)
  controller = options[:controller]
  eval("#{controller.capitalize.singularize}.find_by_title('#{options[:title]}').url_for_me(options[:action].downcase)")
end
Then(/^I should see a modal window with a form "([^"]*)"$/) do |arg|
  page.should have_content(arg, visible: true)
end
Then(/^(.*) within the (.*)$/) do |s, container|
  case container.downcase
    # Bryan: when xxx for more control

    when 'mercury editor'
      page.driver.within_frame('mercury_iframe') { step(s) }

    else
      page.within(css_selector_for(container.downcase)) { step (s) }
  end
end
def css_selector_for(container)
  case container
    when 'navigation bar' || 'navbar'
      '#nav'

    when 'sidebar'
      '#sidebar'

    when 'main content'
      '#main'

    when 'list of projects'
      '#project-list'

    when 'modal dialog'
      '#modal-window'

    else
      pending
  end
end
And /^(?:The user|I) should receive a "(.*?)" email$/ do |subject|
  expect(ActionMailer::Base.deliveries[0].subject).to include(subject)
  ActionMailer::Base.deliveries.size.should eq 1
end
Then(/^I should see "(.*?)" tab is active$/) do |tab|
    page.should have_css "##{tab}.active"
end
Given(/^the following projects exist:$/) do |table|
  #TODO YA rewrite with factoryGirl
  table.hashes.each do |hash|
    if hash[:author].present?
      u = User.find_by_first_name hash[:author]
      project = Project.new(hash.except('author', 'tags').merge(user_id: u.id))
    else
      project = default_test_author.projects.new(hash.except('author', 'tags'))
    end
    if hash[:tags]
      project.tag_list.add(hash[:tags], parse: true)
    end
    project.save!
  end
end
Then /^I should see a form for "([^"]*)"$/ do |form_purpose|
  case form_purpose
    when 'creating a new project'
      page.should have_text form_purpose
      page.should have_css('form#new_project')

    else
      pending
  end
end
Given(/^the document "([^"]*)" has a child document with title "([^"]*)"$/) do |parent, child|
  parent_doc = Document.find_by_title(parent)
  parent_doc.children.create!(
      {
          :project_id => parent_doc.project_id,
          :title => child,
          user_id: parent_doc.user_id
      }
  )
end
And(/^the following sub-documents exist:$/) do |table|
  table.hashes
end
  def default_test_author
    @default_test_author ||= User.create! first_name: 'Tester',
                                          last_name: 'Man',
                                          email: 'testing@test.agileventures.org',
                                          password: test_user_password,
                                          password_confirmation: test_user_password
  end
  def test_user_password
    '12345678'
  end
Given /^user "(?:[^"]*)" has YouTube Channel ID with (some|no) videos in it/ do |qty|
  #TODO YA check what the real response would be for no videos
  @user_youtube_response = (qty == 'some') ? File.read('spec/fixtures/youtube_user_response.json') : nil
  @user_youtube_filtered_response = (qty == 'some') ? File.read('spec/fixtures/youtube_user_filtered_response.json') : nil
end
Given /^user "([^"]*)" has YouTube Channel connected/ do |user|
  user = (user == 'me') ? @current_user : User.find_by_first_name(user)
  user.youtube_id = 'test_id'
  user.youtube_user_name = 'John Doe'
  user.save!

  stub_request(:get, /youtube.*(videos|uploads)/).to_return(body: @user_youtube_response)
  stub_request(:get, /youtube.*WSO|WebsiteOne/i).to_return(body: @user_youtube_filtered_response)
end
Given /^user "([^"]*)" has YouTube Channel not connected/ do |user|
  user = (user == 'me') ? @current_user : User.find_by_first_name(user)
  user.youtube_id = nil
  user.save!

  stub_request(:get, /youtube.*title/).to_return(body: '{"entry":{"title":{"$t":"John Doe"}}}')
  stub_request(:get, /googleapis/).to_return(body: '{ "items": [{"id": "test_id"}]}')
  stub_request(:get, /youtube.*(videos|uploads)/).to_return(body: @user_youtube_response)
end
Then /^I should see "([^"]*)" before "([^"]*)"$/ do |title_1, title_2|
  expect(page.body).to match(/#{title_1}.*#{title_2}/m)
end
Given /^there are no videos$/ do
  stub_request(:get, /youtube/).to_return(body: '')
end
When /^(?:|I )click "([^"]*)" within Mercury Editor toolbar$/ do |button|
  selector_for = {
      'save' => 'mercury-save-button'
  }
  page.execute_script("$('.#{selector_for[button.downcase]}').click()")
  #puts 'sleep(0.1)'
  #sleep(0.1)
end
When(/^I fill in the editable field "([^"]*)" with "([^"]*)"$/) do |field, s|
  page.driver.within_frame('mercury_iframe') {
    field = field.downcase.singularize
    # This selector is specific to the mercury region used!
    if field == 'title'
      find(:css, 'div#document_title>textarea').set(s)
    elsif field == 'body'
      page.execute_script("$('#document_body').text('#{s}')")
      #find(:css, 'div#document_body').set(s)
    end
  }
end
When(/^I (try to use|am using) the Mercury Editor to edit ([^"]*) "([^"]*)"$/) do |opt, model, title|
  visit "/editor#{url_for_title(action: 'show', controller: model, title: title)}"
end
When /I click "([^"]*)" in Mercury Editor/ do |button|
  page.execute_script('Mercury.silent = true')   # disabling the confirmation dialog for saving changes
  page.driver.within_frame('mercury_iframe') {
    click_link button
  }
end
Given(/^the following documents exist:$/) do |table|
  table.hashes.each do |hash|
    if hash[:project].present?
      hash[:project_id] = Project.find_by_title(hash[:project]).id
      hash.except! 'project'
    end
    if hash[:author].present?
      u = User.find_by_first_name hash[:author]
      hash.except! 'author'
      document = u.documents.new hash
    else
      document = default_test_author.documents.new hash
    end

    document.save!
  end
end
Given(/^the following revisions exist$/) do |table|
  table.hashes.each do |hash|
    hash[:revisions].to_i.times do |number|
      doc = Document.find_by_title(hash[:title])
      doc.update(:body => "New content #{number}")
      doc.save!
    end
  end
end
Given(/^the following articles exist:$/) do |table|
  table.hashes.each do |raw_hash|
    hash = {}
    raw_hash.each_pair { |k, v| hash[k.to_s.downcase.squish.gsub(/\s+/, '_')] = v }
    if hash['author'].present?
      u = User.find_by_first_name hash['author']
      hash.except! 'author'
      article = u.articles.new hash
    else
      article = default_test_author.articles.new hash
    end

    article.save!
  end
end
Then(/^I should see a preview containing:$/)  do   |table|
  content = table.raw.flatten

  # Bryan: for selenium javascript drivers
  new_window=page.driver.browser.window_handles.last
  page.within_window new_window do
    content.each do |text|
      page.should have_text text
    end
  end
end
Given(/^following events exist:$/) do |table|
  table.hashes.each do |hash|
    Event.create!(hash)
  end
end
When(/^the next event should be in:$/) do |table|
  table.rows.each do |period, interval|
    page.should have_content([period, interval].join(' '))
  end
end
When /^I sign out$/ do
  page.driver.submit :delete, destroy_user_session_path, {}
end
