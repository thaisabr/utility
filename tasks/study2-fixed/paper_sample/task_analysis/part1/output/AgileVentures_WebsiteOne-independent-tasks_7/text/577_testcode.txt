Given /^I am logged in as user with email "([^"]*)", with password "([^"]*)"$/ do |email, password|
  @user = FactoryGirl.create(:user, email: email, password: password, password_confirmation: password)
  visit new_user_session_path
  within ('#main') do
    fill_in 'user_email', :with => email
    fill_in 'user_password', :with => password
    click_button 'Sign in'
  end
end
Given /^the following users exist$/ do |table|
  table.hashes.each do |attributes|
    create_test_user(attributes)
  end
end
Given(/^I should be on the "([^"]*)" page for "(.*?)"$/) do |page, user|
  this_user = User.find_by_first_name(user) || User.find_by_email(user)
  expect(current_path).to eq path_to(page, this_user)
end
Given(/^I (?:am on|go to) my "([^"]*)" page$/) do |page|
  page.downcase!
  if page == 'profile'
    visit users_show_path(@user)
  elsif page == 'edit profile'
    visit edit_user_registration_path(@user)
  else
    pending
  end
end
Given(/^I add skills "(.*)"/) do |skills|
  skills.split(",").each { |s| page.execute_script "$('#skills').tags().addTag('#{s}')"}
end
Then(/^I should see skills "(.*)" on my profile/) do |skills|
  page.all(:css, "#skills .tag span").collect { |e| e.text }.sort.should == skills.split(",").sort
end
  def create_test_user(options = {})
    skills = options.delete "skills"
    options = default_test_user_details.merge options
    user = User.create!(options)
    user.skill_list = skills
    user.save!
  end
def path_to(page_name, id = '')
  name = page_name.downcase
  case name
    when 'home' then
      root_path
    when 'registration' then
      new_user_registration_path
    when 'sign in' then
      new_user_session_path
    when 'projects' then
      projects_path
    when 'new project' then
      new_project_path
    when 'articles' then
      articles_path
    when 'edit' then
      edit_project_path(id)
    when 'show' then
      project_path(id)
    when 'our members' then
      users_index_path
    when 'user profile' then
      users_show_path(id)
    when 'my account' then
      edit_user_registration_path(id)
    when "foobar" then
      visit ("/#{page}")
    when "supporters" then
      page_path('sponsors')
    else
      raise('path to specified is not listed in #path_to')
  end
end
  def default_test_user_details
    {
        email: Faker::Internet.email,
        last_sign_in_ip: test_ip_address,
        password: test_user_password,
        password_confirmation: test_user_password,
        display_profile: true
    }
  end
  def test_ip_address
    '127.0.0.1'
  end
  def test_user_password
    '12345678'
  end
When(/^I (?:go to|am on) the "([^"]*)" page$/) do |page|
  visit path_to(page)
end
When(/^I click "([^"]*)" button$/) do |button|
  click_button button
end
