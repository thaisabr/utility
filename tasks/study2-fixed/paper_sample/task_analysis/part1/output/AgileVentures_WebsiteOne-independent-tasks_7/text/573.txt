Feature: As a user of the site
In order to get to know other users
I want to be able to view a user profile page with information about the user.
Background:
Given I am on the "home" page
And the following users exist
| first_name | last_name | email                  | github_profile_url         |
| Alice      | Jones     | alice@btinternet.co.uk | http://github.com/AliceSky |
| Bob        | Butcher   | bobb112@hotmail.com    |                            |
And the following projects exist:
| title         | description             | status   |
| hello world   | greetings earthlings    | active   |
| hello mars    | greetings aliens        | inactive |
| hello jupiter | greetings jupiter folks | active   |
| hello mercury | greetings mercury folks | inactive |
| hello saturn  | greetings saturn folks  | active   |
| hello sun     | greetings sun folks     | active   |
Scenario: Show link to user's github page if authenticated with GitHub
When I am on "profile" page for user "Alice"
Then I should see "GitHub profile: AliceSky"
Scenario: Show 'profile not linked'
When I am on "profile" page for user "Bob"
Then I should see "GitHub profile: not linked"
