Feature: As a user of the site
In order to get to know other users
I want to be able to view a user profile page with information about the user.
Background:
Given I am on the "home" page
And the following users exist
| first_name | last_name | email                  | github_profile_url         |
| Alice      | Jones     | alice@btinternet.co.uk | http://github.com/AliceSky |
| Bob        | Butcher   | bobb112@hotmail.com    |                            |
And the following projects exist:
| title         | description             | status   |
| hello world   | greetings earthlings    | active   |
| hello mars    | greetings aliens        | inactive |
| hello jupiter | greetings jupiter folks | active   |
| hello mercury | greetings mercury folks | inactive |
| hello saturn  | greetings saturn folks  | active   |
| hello sun     | greetings sun folks     | active   |
Scenario: Show link to user's github page if authenticated with GitHub
When I am on "profile" page for user "Alice"
Then I should see "GitHub profile: AliceSky"
Scenario: Show 'profile not linked'
When I am on "profile" page for user "Bob"
Then I should see "GitHub profile: not linked"
Feature: Create and maintain projects
In order to manage my account settings
As I user I would like to have a "My account" page
And I would like to be able to edit change my credentials
Background:
Given I am logged in as user with email "current@email.com", with password "12345678"
And I am on the "home" page
Scenario: Link my GitHub profile link to my profile
Given I have a GitHub profile with username "tochman"
And I want to use third party authentications
And I am on my "Edit Profile" page
When I click "GitHub"
And my profile should be updated with my GH username
When I am on "profile" page for user "me"
Then I should see a link "tochman" to "https://github.com/tochman"
