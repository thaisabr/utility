Classes: 2
[name:Project, file:AgileVentures_WebsiteOne/app/models/project.rb, step:Given ]
[name:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]

Methods: 25
[name:add, type:Object, file:null, step:Given ]
[name:create!, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:default_test_author, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:each, type:Object, file:null, step:Given ]
[name:except, type:Object, file:null, step:Given ]
[name:find_by_first_name, type:User, file:AgileVentures_WebsiteOne/app/models/user.rb, step:Given ]
[name:find_by_title, type:Project, file:AgileVentures_WebsiteOne/app/models/project.rb, step:Given ]
[name:github_url, type:Object, file:null, step:Given ]
[name:hashes, type:Object, file:null, step:Given ]
[name:have_link, type:Object, file:null, step:Given ]
[name:id, type:Object, file:null, step:Given ]
[name:last, type:Object, file:null, step:Given ]
[name:merge, type:Object, file:null, step:Given ]
[name:new, type:Project, file:AgileVentures_WebsiteOne/app/models/project.rb, step:Given ]
[name:new, type:Object, file:null, step:Given ]
[name:page, type:Object, file:null, step:Given ]
[name:present?, type:Object, file:null, step:Given ]
[name:projects, type:Object, file:null, step:Given ]
[name:save!, type:Object, file:null, step:Given ]
[name:split, type:Object, file:null, step:Given ]
[name:stub_request, type:Object, file:null, step:Given ]
[name:tag_list, type:Object, file:null, step:Given ]
[name:test_user_password, type:Helpers, file:AgileVentures_WebsiteOne/features/support/helpers.rb, step:Given ]
[name:title, type:Object, file:null, step:Given ]
[name:to_return, type:Object, file:null, step:Given ]

Referenced pages: 0

