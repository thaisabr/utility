Given /^I am logged in as user with email "([^"]*)", with password "([^"]*)"$/ do |email, password|
  @user = FactoryGirl.create(:user, email: email, password: password, password_confirmation: password)
  visit new_user_session_path
  within ('#main') do
    fill_in 'user_email', :with => email
    fill_in 'user_password', :with => password
    click_button 'Sign in'
  end
end
Given /^the following users exist$/ do |table|
  table.hashes.each do |attributes|
    create_test_user(attributes)
  end
end
Given(/^I (?:am on|go to) my "([^"]*)" page$/) do |page|
  page.downcase!
  if page == 'profile'
    visit users_show_path(@user)
  elsif page == 'edit profile'
    visit edit_user_registration_path(@user)
  else
    pending
  end
end
Given /^I am on "(.*?)" page for user "(.*?)"$/ do |page, user_name|
  if user_name == 'me'
    user = @user
  else
    user = User.find_by_first_name(user_name)
  end

  case page
    when 'profile' then
      visit users_show_path(user)
    when page == 'edit profile'
      visit edit_user_registration_path(user)
  end
end
And(/^I have a GitHub profile with username "([^"]*)"$/) do |username|
  @github_profile_url = "https://github.com/#{username}"
end
When(/^my profile should be updated with my GH username$/) do
  @user.github_profile_url = @github_profile_url
  @user.save
  expect(@user.github_profile_url).to eq @github_profile_url
end
Then(/^the request should be to "(.*)"$/) do |url|
  expect(current_url).to eq url
end
  def create_test_user(options = {})
    skills = options.delete "skills"
    options = default_test_user_details.merge options
    user = User.create!(options)
    user.skill_list = skills
    user.save!
  end
  def default_test_user_details
    {
        email: Faker::Internet.email,
        last_sign_in_ip: test_ip_address,
        password: test_user_password,
        password_confirmation: test_user_password,
        display_profile: true
    }
  end
  def test_ip_address
    '127.0.0.1'
  end
  def test_user_password
    '12345678'
  end
When(/^I (?:go to|am on) the "([^"]*)" page$/) do |page|
  visit path_to(page)
end
When(/^I click "([^"]*)"$/) do |text|
  click_link_or_button text
end
Then /^I should( not)? see "([^"]*)"$/ do |negative, string|
  unless negative
    page.should have_text string
  else
    page.should_not have_text string
  end
end
Given(/^I want to use third party authentications$/) do
  OmniAuth.config.test_mode = true
  OmniAuth.config.mock_auth[:github] = {
      'provider' => 'github',
      'uid' => '12345678',
      'info' => {
          'email' => 'mock@email.com'
      }
  }
  OmniAuth.config.mock_auth[:gplus] = {
      'provider' => 'gplus',
      'uid' => '12345678',
      'info' => {
          'email' => 'mock@email.com'
      },
      'credentials' => {'token' => 'test_token'}
  }
end
Then(/^I should see a link "([^"]*)" to "([^"]*)"$/) do |text, link|
  page.should have_css "a[href='#{link}']", text: text
end
def path_to(page_name, id = '')
  name = page_name.downcase
  case name
    when 'home' then
      root_path
    when 'registration' then
      new_user_registration_path
    when 'sign in' then
      new_user_session_path
    when 'projects' then
      projects_path
    when 'new project' then
      new_project_path
    when 'articles' then
      articles_path
    when 'edit' then
      edit_project_path(id)
    when 'show' then
      project_path(id)
    when 'our members' then
      users_index_path
    when 'user profile' then
      users_show_path(id)
    when 'my account' then
      edit_user_registration_path(id)
    when "foobar" then
      visit ("/#{page}")
    when "supporters" then
      page_path('sponsors')
    else
      raise('path to specified is not listed in #path_to')
  end
end
Given(/^the following projects exist:$/) do |table|
  #TODO YA rewrite with factoryGirl
  temp_author = nil
  table.hashes.each do |hash|
    if hash[:author].present?
      u = User.find_by_first_name hash[:author]
      project = u.projects.create(hash.except('author', 'tags'))
    else
      if temp_author.nil?
        temp_author = User.create first_name: 'First',
                                  last_name: 'Last',
                                  email: "dummy#{User.count}@users.co",
                                  password: '1234124124'
      end
      project = temp_author.projects.create(hash.except('author', 'tags'))
    end
    if hash[:tags]
      project.tag_list.add(hash[:tags], parse: true)
      project.save
    end
  end
end
