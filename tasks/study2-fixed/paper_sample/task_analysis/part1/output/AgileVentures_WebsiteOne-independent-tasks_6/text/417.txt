Feature: Static pages
As the site administrator
So that I can get information across to sites visitors
I want there to be static pages
Background:
Given the following pages exist
| title         | body                      |
| About Us      | Agile Ventures            |
| Sponsors      | AV Sponsors               |
| Getting Started | Remote Pair Programming |
And the following page revisions exist
| title         | revisions  |
| About Us      | 1          |
Scenario: See Sponsor Banners
When I am on the "projects" page
Then I should see sponsor banner for "Makers Academy"
Then I should see sponsor banner for "AirPair"
And I should see sponsor banner for "Agile Ventures"
And I should see link "Become a supporter"
