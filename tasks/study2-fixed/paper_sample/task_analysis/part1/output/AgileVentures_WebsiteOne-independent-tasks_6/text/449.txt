Feature: Rendering contact us form
So that I can receive feedback from users
As a site administrator
I would like to display a "contact us" form on the sites index(landing page)
https://www.pivotaltracker.com/story/show/63103292
Add functionality to add a contact us form to index page in #footer
Background:
Given I visit the site
Scenario: Rendering contact us form
Then I should see a footer area
And I should see "Contact us" form within the footer
And I should see "Send a traditional email to info@agileventures.org, or use the contact form."
And I should see link "info@agileventures.org"
Feature: Static pages
As the site administrator
So that I can get information across to sites visitors
I want there to be static pages
Background:
Given the following pages exist
| title         | body                      |
| About Us      | Agile Ventures            |
| Sponsors      | AV Sponsors               |
| Getting Started | Remote Pair Programming |
And the following page revisions exist
| title         | revisions  |
| About Us      | 1          |
Scenario: Render About Us page
Then I should see link "About Us"
When I click "About Us"
Then I should be on the static "About Us" page
And I should see "About Us"
Feature: Create and maintain projects
As a member of AgileVentures
So that I can participate in AgileVentures activities
I would like to add a new project
Background:
Given the following projects exist:
| title         | description             | status   | github_url                                  | pivotaltracker_url                               |
| hello world   | greetings earthlings    | active   | https://github.com/agileventures/helloworld | https://www.pivotaltracker.com/s/projects/742821 |
| hello mars    | greetings aliens        | inactive |                                             |                                                  |
| hello jupiter | greetings jupiter folks | active   |                                             |                                                  |
| hello mercury | greetings mercury folks | inactive |                                             |                                                  |
| hello saturn  | greetings saturn folks  | active   |                                             |                                                  |
| hello sun     | greetings sun folks     | active   |                                             |                                                  |
Scenario: List of projects in table layout
Given  I am on the "home" page
When I follow "Projects" within the navbar
Then I should see "List of Projects"
Then I should see:
| Text   |
| Create |
| Status |
Scenario: See a list of current projects
Given  I am on the "home" page
When I follow "Projects" within the navbar
Then I should see:
| Text                    |
| hello jupiter           |
| greetings jupiter folks |
| ACTIVE                  |
| hello mars              |
| greetings aliens        |
| INACTIVE                |
Scenario: Alphabetically display pagination in "Our Projects" page
Given I am on the "home" page
When I follow "Projects" within the navbar
Then I should see:
| greetings aliens        |
| greetings jupiter folks |
| greetings mercury folks |
| greetings saturn folks  |
| greetings sun folks     |
And I should not see "greetings earthlings"
When I go to the next page
Then I should not see:
| greetings aliens        |
| greetings jupiter folks |
| greetings mercury folks |
| greetings saturn folks  |
| greetings sun folks     |
And I should see "greetings earthlings"
