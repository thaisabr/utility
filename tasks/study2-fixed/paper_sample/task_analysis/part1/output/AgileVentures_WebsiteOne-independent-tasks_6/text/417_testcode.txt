When(/^I (?:go to|am on) the "([^"]*)" page$/) do |page|
  visit path_to(page)
end
Then /^I should( not)? see link "([^"]*)"$/ do |negative, link|
  unless negative
    expect(page.has_link? link).to be_true
  else
    expect(page.has_link? link).to be_false
  end
end
def path_to(page_name, id = '')
  name = page_name.downcase
  case name
    when 'home' then
      root_path
    when 'registration' then
      new_user_registration_path
    when 'sign in' then
      new_user_session_path
    when 'projects' then
      projects_path
    when 'new project' then
      new_project_path
    when 'articles' then
      articles_path
    when 'edit' then
      edit_project_path(id)
    when 'show' then
      project_path(id)
    when 'our members' then
      users_path
    when 'user profile' then
      user_path(id)
    when 'my account' then
      edit_user_registration_path(id)
    when 'foobar' then
      visit ("/#{page}")
    else
      raise('path to specified is not listed in #path_to')
  end
end
Then(/^I should see sponsor banner for "(.*?)"$/) do |supporter_name|
  page.should have_selector('div#sponsorsBar')
  page.should have_css("img[alt*='#{supporter_name}']")
end
Given(/^the following pages exist$/) do |table|
  table.hashes.each do |hash|
    StaticPage.create!(hash)
  end
end
Given(/^the following page revisions exist$/) do |table|
  table.hashes.each do |hash|
    hash[:revisions].to_i.times do |number|
      page = StaticPage.find_by_title(hash[:title])
      page.update(:body => "New content #{number}")
      page.save!
    end
  end
end
