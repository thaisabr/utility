Given(/^I am on ([^"]*) index page$/) do |page|
  case page.downcase
    when 'events'
      visit events_path
    when 'projects'
      visit projects_path
    else
      visit project_events_path(Project.find_by(title: "cs169"))
  end
end
Given(/^following events exist:$/) do |table|
  table.hashes.each do |hash|
    hash[:project_id] = Project.find_by(title: hash['project']).id unless hash['project'].blank?
    hash.delete('project')
    Event.create!(hash)
  end
end
Given(/^I am on the show page for event "([^"]*)"$/) do |name|
  event = Event.find_by_name(name)
  visit event_path(event)
end
Given(/^the date is "([^"]*)"$/) do |jump_date|
  Delorean.time_travel_to(Time.parse(jump_date))
end
And(/^I select "([^"]*)" from the project dropdown$/) do |project_name|
  page.select project_name, from: "project_id"
end
And(/^the short local date element should be set to "([^"]*)"$/) do |datetime|
  expect(page).to have_css %Q{time[datetime="#{datetime}"][data-format="%a, %b %d, %Y"]}
end
And(/^the local time element should be set to "([^"]*)"$/) do |datetime|
  expect(page).to have_css %Q{time[datetime="#{datetime}"][data-format="%H:%M"]}
end
And(/^"([^"]*)" is selected in the project dropdown$/) do |project_slug|
  project_id = project_slug == 'All' ? '' : Project.friendly.find(project_slug).id
  expect(find("#project_id").value).to eq project_id.to_s
end
Given(/^I visit "(.*?)"$/) do |path|
  visit path
end
When(/^I open the Edit URL controls/) do
  page.execute_script(  %q{$('li[role="edit_hoa_link"] > a').trigger('click')}  )
end
When(/^I click on the Save button/) do
  page.find(:css, %q{input[id="hoa_link_save"]}).trigger('click')
end
When(/^I click on the Cancel button/) do
  page.find(:css, %q{button[id="hoa_link_cancel"]}).trigger('click')
end
Then(/^I should see the Edit URL controls/) do
  expect(page).to have_css 'div#edit-link-form.collapse.in'
end
Then(/^I should not see the Edit URL controls/) do
  expect(page).to have_css 'div#edit-link-form[style*="height: 0px"]'
end
When(/^I click "([^"]*)" button$/) do |button|
  click_button button
end
When(/^I fill in "([^"]*)" with "([^"]*)"$/) do |field, value|
  fill_in field, :with => value
end
Given /^the time now is "([^"]*)"$/ do |time|
  # use delorean
  Time.stub(now: Time.parse(time))
end
Then /^I should see link "([^"]*)" with "([^"]*)"$/ do |link, url|
  expect(page).to have_link(link, href: url)
end
Then /^I should( not)? see:$/ do |negative, table|
  expectation = negative ? :should_not : :should
  table.rows.flatten.each do |string|
    page.send(expectation, have_text(string))
  end
end
Then /^I should( not)? see "([^"]*)"$/ do |negative, string|
  unless negative
    expect(page).to have_text string
  else
    expect(page).to_not have_text string
  end
end
Given(/^I (?:am on|go to) the "([^"]*)" page for ([^"]*) "([^"]*)"$/) do |action, controller, title|
  visit url_for_title(action: action, controller: controller, title: title)
end
def url_for_title(options)
  controller = options[:controller]
  eval("#{controller.capitalize.singularize}.find_by_title('#{options[:title]}').url_for_me(options[:action].downcase)")
end
Given(/^the following projects exist:$/) do |table|
  #TODO YA rewrite with factoryGirl
  table.hashes.each do |hash|
    if hash[:author].present?
      u = User.find_by_first_name hash[:author]
      project = Project.new(hash.except('author', 'tags').merge(user_id: u.id))
    else
      project = default_test_author.projects.new(hash.except('author', 'tags'))
    end
    if hash[:tags]
      project.tag_list.add(hash[:tags], parse: true)
    end
    project.save!
  end
end
When(/^I am a member of project "([^"]*)"$/) do |name|
  step %Q{I should become a member of project "#{name}"}
end
Given(/^I am on the home page$/) do
  visit root_path
end
  def default_test_author
    @default_test_author ||= User.find_by_email(default_test_user_details[:email])
    if @default_test_author.nil?
      @default_test_author = User.create!(default_test_user_details)
    end
    @default_test_author
  end
  def default_test_user_details
    {
        first_name: 'Tester',
        last_name: 'Person',
        email: 'testuser@agileventures.org',
        last_sign_in_ip: test_ip_address,
        password: test_user_password,
        password_confirmation: test_user_password,
        display_profile: true,
        latitude: 59.33,
        longitude: 18.06,
        country_name: 'Stockholm',
        bio: 'Full time Tester',
        skill_list: 'Test'
    }
  end
  def test_ip_address
    '127.0.0.1'
  end
  def test_user_password
    '12345678'
  end
Then /^I should (not )?see hangout button$/ do |absent|
  if absent
    expect(page).not_to have_css '#liveHOA-placeholder'
  else
    expect(page).to have_css "#liveHOA-placeholder"
    src = page.find(:css, '#liveHOA-placeholder iframe')['src']
    expect(src).to match /talkgadget.google.com/
  end
end
Given /^the Hangout for event "([^"]*)" has been started with details:$/ do |event_name, table|
  ho_details = table.transpose.hashes
  hangout = ho_details[0]


  start_time = hangout['Started at'] ? Time.parse(hangout['Started at']) : Time.now
  update_time = hangout['Updated at'] ? Time.parse(hangout['Updated at']) : start_time
  event = Event.find_by_name(event_name)
  FactoryGirl.create(:event_instance,
                     event_id: event.id,
                     hangout_url: hangout['EventInstance link'],
                     created: start_time,
                     updated_at: update_time,
                     hoa_status: 'live')

end
Given /^the following hangouts exist:$/ do |table|
  table.hashes.each do |hash|
    participants = hash['Participants'] || []
    participants = participants.split(',')

    participants = participants.map do |participant|
      break if participant.empty?
      name = participant.squish
      user = User.find_by_first_name(name)
      gplus_id = user.authentications.find_by(provider: 'gplus').try!(:uid) if user.present?
      ["0", {:person => {displayName: "#{name}", id: gplus_id}}]
    end

    FactoryGirl.create(:event_instance,
                       title: hash['Title'],
                       project: Project.find_by_title(hash['Project']),
                       event: Event.find_by_name(hash['Event']),
                       category: hash['Category'],
                       user: User.find_by_first_name(hash['Host']),
                       hangout_url: hash['EventInstance url'],
                       participants: participants,
                       yt_video_id: hash['Youtube video id'],
                       created: hash['Start time'],
                       updated: hash['End time'])
  end
end
Then /^I have Slack notifications enabled$/ do
  stub_request(:post, 'https://agile-bot.herokuapp.com/hubot/hangouts-notify').to_return(status: 200)
end
Given(/^(\d+) hangouts exists$/) do |count|
  count.to_i.times { FactoryGirl.create :event_instance }
end
Then(/^I should see (\d+) hangouts$/) do |count|
  expect(page).to have_content("Hangout_", count: count.to_i)
end
When(/^I scroll to bottom of page$/) do
  page.evaluate_script("window.scrollTo(0, $(document).height());")
  sleep 2
end
Given /^I am logged in$/ do
  create_user
  sign_in
end
Given /^the following users exist$/ do |table|
  table.hashes.each do |attributes|
    FactoryGirl.create(:user, attributes)
  end
end
  def create_user
    @user ||= FactoryGirl.create(:user, create_visitor)
    @current_user = @user
  end
  def sign_in
    visit new_user_session_path
    within ('#main') do
      fill_in 'user_email', :with => @visitor[:email]
      fill_in 'user_password', :with => @visitor[:password]
      click_button 'Sign in'
    end
  end
  def create_visitor
    @visitor ||= { first_name: 'Anders',
                   last_name: 'Persson',
                   email: 'example@example.com',
                   password: 'changemesomeday',
                   password_confirmation: 'changemesomeday',
                   slug: 'slug-ma',
                   country_name: 'Sweden'}
  end
Given /^there are no videos$/ do
  # No videos created
end
And(/^I should see the avatar for "(.*?)"$/) do |user|
  user = User.find_by_first_name(user)
  expect(page).to have_xpath("//img[contains(@alt, '#{user.presenter.display_name}')]")
end
Given(/^a hangout$/) do
  @hangout = EventInstance.find_by title: 'HangoutsFlow'
end
And(/^that the HangoutConnection has pinged to indicate the hangout is live$/) do
  participants = {"0" => {"id" => "hangout2750757B_ephemeral.id.google.com^a85dcb4670", "hasMicrophone" => "true", "hasCamera" => "true", "hasAppEnabled" => "true", "isBroadcaster" => "true", "isInBroadcast" => "true", "displayIndex" => "0", "person" => {"id" => "108533475599002820142", "displayName" => "Alejandro Babio", "image" => {"url" => "https://lh4.googleusercontent.com/-p4ahDFi9my0/AAAAAAAAAAI/AAAAAAAAAAA/n-WK7pTcJa0/s96-c/photo.jpg"}, "na" => "false"}, "locale" => "en", "na" => "false"}}
  ping_with(participants)
end
Then(/^the host should be a participant$/) do
  @hangout.reload
  expect(@hangout.participants.values.any? { |struc| struc['person']['displayName'] == 'Alejandro Babio' }).to be true
end
Given(/^that another person joins the hangout$/) do
  participants = {"0" => {"id" => "hangout2750757B_ephemeral.id.google.com^a85dcb4670", "hasMicrophone" => "true", "hasCamera" => "true", "hasAppEnabled" => "true", "isBroadcaster" => "true", "isInBroadcast" => "true", "displayIndex" => "0", "person" => {"id" => "108533475599002820142", "displayName" => "Alejandro Babio", "image" => {"url" => "https://lh4.googleusercontent.com/-p4ahDFi9my0/AAAAAAAAAAI/AAAAAAAAAAA/n-WK7pTcJa0/s96-c/photo.jpg"}, "na" => "false"}, "locale" => "en", "na" => "false"},
                  "1" => {"id" => "hangout2750757B_ephemeral.id.google.com^a85dcb4670", "hasMicrophone" => "true", "hasCamera" => "true", "hasAppEnabled" => "true", "isBroadcaster" => "false", "isInBroadcast" => "true", "displayIndex" => "0", "person" => {"id" => "108533475599002820143", "displayName" => "Alexander Great", "image" => {"url" => "https://lh4.googleusercontent.com/-p4ahDFi9my0/AAAAAAAAAAI/AAAAAAAAAAA/n-WK7pTcJa0/s96-c/photo.jpg"}, "na" => "false"}, "locale" => "en", "na" => "false"}}
  ping_with(participants)
end
Then(/^the hangout participants should be updated$/) do
  @hangout.reload
  expect(@hangout.participants.values.any? { |struc| struc['person']['displayName'] == 'Alejandro Babio' }).to be true
  expect(@hangout.participants.values.any? { |struc| struc['person']['displayName'] == 'Alexander Great' }).to be true
end
When(/^one person leaves and another joins$/) do
  participants = {"0" => {"id" => "hangout2750757B_ephemeral.id.google.com^a85dcb4670", "hasMicrophone" => "true", "hasCamera" => "true", "hasAppEnabled" => "true", "isBroadcaster" => "true", "isInBroadcast" => "true", "displayIndex" => "0", "person" => {"id" => "108533475599002820142", "displayName" => "Alejandro Babio", "image" => {"url" => "https://lh4.googleusercontent.com/-p4ahDFi9my0/AAAAAAAAAAI/AAAAAAAAAAA/n-WK7pTcJa0/s96-c/photo.jpg"}, "na" => "false"}, "locale" => "en", "na" => "false"},
                  "1" => {"id" => "hangout2750757B_ephemeral.id.google.com^a85dcb4670", "hasMicrophone" => "true", "hasCamera" => "true", "hasAppEnabled" => "true", "isBroadcaster" => "false", "isInBroadcast" => "true", "displayIndex" => "0", "person" => {"id" => "108533475599002820144", "displayName" => "Alexander Little", "image" => {"url" => "https://lh4.googleusercontent.com/-p4ahDFi9my0/AAAAAAAAAAI/AAAAAAAAAAA/n-WK7pTcJa0/s96-c/photo.jpg"}, "na" => "false"}, "locale" => "en", "na" => "false"}}
  ping_with(participants)
end
Then(/^the hangout participants should still include everyone who ever joined$/) do
  @hangout.reload
  expect(@hangout.participants.values.any? { |struc| struc['person']['displayName'] == 'Alejandro Babio' }).to be true
  expect(@hangout.participants.values.any? { |struc| struc['person']['displayName'] == 'Alexander Little' }).to be true
  expect(@hangout.participants.values.any? { |struc| struc['person']['displayName'] == 'Alexander Great' }).to be true
end
def ping_with(participants)
  header 'ORIGIN', 'a-hangout-opensocial.googleusercontent.com'
  put "/hangouts/#{@hangout.uid}", {title: @hangout.title, host_id: '3', event_id: '',
                                    participants: participants, hangout_url: 'http://hangout.test',
                                    hoa_status: 'live', project_id: '1', category: 'PairProgramming',
                                    yt_video_id: '11'}
end
Then(/^I should become a member of project "([^"]*)"$/) do | name|
  object = Project.find_by_title(name)
  @user.follow(object)
end
