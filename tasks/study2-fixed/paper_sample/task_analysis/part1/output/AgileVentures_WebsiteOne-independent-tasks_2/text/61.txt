Feature: Events
As a site user
In order to be able to plan activities
I would like to create events
Background:
Given I am logged in
And the following projects exist:
| title | description          | pitch | status | commit_count |
| WSO   | greetings earthlings |       | active | 2795         |
| EdX   | greetings earthlings |       | active | 2795         |
| AAA   | for roadists         |       | active |              |
Scenario: New event defaults to cs169 project when it exists
Given the following projects exist:
|title | description | pitch | status | commit_count|
|CS169 | stuff       |       | active |  5000       |
And I am on Events index page
When I click "New Event"
Then "cs169" is selected in the event project dropdown
Feature: Events
As a site user
In order to be able to plan activities
I would like to see event CRUD functionality
Pivotal Tracker:  https://www.pivotaltracker.com/story/show/66655876
Background:
Given following events exist:
| name       | description             | category        | start_datetime          | duration | repeats | time_zone | project | repeats_weekly_each_days_of_the_week_mask | repeats_every_n_weeks |
| Scrum      | Daily scrum meeting     | Scrum           | 2014/02/03 07:00:00 UTC | 150      | never   | UTC       |         |                                           |                       |
| PP Session | Pair programming on WSO | PairProgramming | 2014/02/07 10:00:00 UTC | 15       | never   | UTC       |         |                                           |                       |
| Standup    | Daily standup meeting   | Scrum           | 2014/02/03 07:00:00 UTC | 150      | weekly  | UTC       |         | 15                                        | 1                     |
@javascript
Scenario Outline: Show correct time, date, timezone and user location
Given the date is "2016/05/01 09:15:00 UTC"
And the user is in "<zone>"
When they view the event "Standup"
Then I should see "Standup"
And the local date element should be set to "2016-05-02T07:00:00Z"
And the local time element should be set to "2016-05-02T07:00:00Z"
And I should see "<zone>"
Examples:
| zone             |
| Europe/London    |
| America/New_York |
Feature: Managing hangouts of scrums and PairProgramming sessions
In order to manage hangouts of scrums and PP sessions  easily
As a site user
I would like to have means of creating, joining, editing and watching hangouts
Background:
Given following events exist:
| name          | description          | category      | start_datetime          | duration | repeats | time_zone |
| Scrum         | Daily scrum meeting  | Scrum         | 2014/02/03 07:00:00 UTC | 150      | never   | UTC       |
| Retrospective | Weekly retrospective | ClientMeeting | 2014/02/03 07:00:00 UTC | 150      | never   | UTC       |
And the following projects exist:
| title       | description          | status |
| WebsiteOne  | greetings earthlings | active |
| Autograders | greetings earthlings | active |
And the following users exist
| first_name | last_name | email                  | password | gplus   |
| Alice      | Jones     | alice@btinternet.co.uk | 12345678 | yt_id_1 |
| Bob        | Anchous   | bob@btinternet.co.uk   | 12345678 | yt_id_2 |
| Jane       | Anchous   | jan@btinternet.co.uk   | 12345678 | yt_id_3 |
Scenario: Show hangout details for live event
Given the Hangout for event "Scrum" has been started with details:
| EventInstance link | http://hangout.test |
| Started at         | 10:25:00 UTC        |
And the time now is "10:26:00 UTC"
When I am on the show page for event "Scrum"
Then I should see:
| Scrum               |
| Scrum               |
| Daily scrum meeting |
And I should see link "Join now" with "http://hangout.test"
And I should not see hangout button
Feature: As a developer
In order to be able to use the sites features
I want to register as a user
https://www.pivotaltracker.com/story/show/63047058
Background:
Given I am not logged in
Scenario: Let a visitor register as a site user
Given I am on the "registration" page
And I submit "user@example.com" as username
And I submit "password" as password
And I click "Sign up" button
Then I should be on the "home" page
And I should see a successful sign up message
And I should receive a "Welcome to AgileVentures.org" email
And replies to that email should go to "info@agileventures.org"
Scenario: User signs up with valid data
When I sign up with valid user data
Then I should see a successful sign up message
Scenario: User signs up with invalid email
When I sign up with an invalid email
Then I should see an invalid email message
Scenario: User signs up without password
When I sign up without a password
Then I should see a missing password message
Scenario: User signs up without password confirmation
When I sign up without a password confirmation
Then I should see a missing password confirmation message
Scenario: User signs up with mismatched password and confirmation
When I sign up with a mismatched password confirmation
Then I should see a mismatched password message
@omniauth
Scenario: User signs up with a GitHub account
Given I am on the "registration" page
When I click "GitHub"
Then I should see "Signed in successfully."
@omniauth
Scenario: User signs up with a Google+ account
Given I am on the "registration" page
When I click "Google+"
Then I should see "Signed in successfully."
@omniauth-without-email
Scenario: User signs up with a GitHub account having no public email (sad path)
Given I am on the "registration" page
When I sign up with GitHub
Then I should see link for instructions to sign up
@omniauth-without-email
Scenario: User signs up with a Google+ account having no public email (sad path)
Given I am on the "registration" page
When I click "Google+"
Then I should see the "google-plus" icon
Then I should see "Your Gplus account needs to have a public email address for sign up"
And I should not see "Password can't be blank"
