Feature: Show Events
As a site user
In order to be able to plan activities
I would like to see event CRUD functionality
Pivotal Tracker:  https://www.pivotaltracker.com/story/show/66655876
Background:
Given following events exist:
| name       | description             | category        | start_datetime          | duration | repeats | time_zone | project | repeats_weekly_each_days_of_the_week_mask | repeats_every_n_weeks |
| Scrum      | Daily scrum meeting     | Scrum           | 2014/02/03 07:00:00 UTC | 150      | never   | UTC       |         |                                           |                       |
| PP Session | Pair programming on WSO | PairProgramming | 2014/02/07 10:00:00 UTC | 15       | never   | UTC       |         |                                           |                       |
| Standup    | Daily standup meeting   | Scrum           | 2014/02/03 07:00:00 UTC | 150      | weekly  | UTC       |         | 15                                        | 1                     |
@javascript
Scenario Outline: Do not show hangout button until 10 minutes before scheduled start time, and while event is running
Given the date is "<date>"
And I am logged in
And I am on the show page for event "Standup"
Then I <assertion> see hangout button
Examples:
| date                    | assertion  |
| 2014/02/03 07:55:00 UTC | should     |
| 2014/02/03 06:55:00 UTC | should     |
| 2014/02/03 06:49:00 UTC | should not |
| 2014/02/03 09:40:00 UTC | should not |
| 2014/02/04 06:55:00 UTC | should     |
| 2014/02/04 06:49:00 UTC | should not |
| 2014/02/04 09:40:00 UTC | should not |
