Given /I am signed up as a student advisor/ do
  user = User.new({:first_name => 'Sangyoon',
                :last_name => 'Park',
                :street_address => '346 soda UC Berkeley',
                :city => 'Berkeley',
                :state => 'CA',
                :zipcode => '94000',
                :phone_number => '123-456-7890',
                :email => 'myemail@nowhere.com',
                :password => '1234',
                :password_confirmation => '1234'})

  owner = User.build_owner("Advisor")
  user.owner = owner
  user.save!
  owner.save!
end
Given /we are currently in a semester/ do
  Semester.create!(
    :name => Semester::FALL, 
    :year => "2012", 
    :start_date => Date.today - 10, 
    :end_date => Date.today + 10,
    :registration_deadline => Deadline.new(
      :title => "Registraiton Deadline",
      :summary => "You must have you preferences selected by this deadline",
      :due_date => Date.new(2012, 1, 16),
    ),
    :status => Semester::PUBLIC,
  )
end
Then /^(?:|I )should be located at "([^"]*)"$/ do |page_name|
  current_path = URI.parse(current_url).path
  if current_path.respond_to? :should
    current_path.should == page_name
  else
    assert_equal page_name, current_path
  end
end
When /^(?:|I )go to (.+)$/ do |page_path|
  visit page_path
end
Then /^(?:|I )should see "([^"]*)"$/ do |text|
  if page.respond_to? :should
    page.should have_content(text)
  else
    assert page.has_content?(text)
  end
end
