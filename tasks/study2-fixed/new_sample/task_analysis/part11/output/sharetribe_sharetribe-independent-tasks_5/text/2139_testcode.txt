Given /^there is a listing with title "([^"]*)"(?: from "([^"]*)")?(?: with category "([^"]*)")?(?: and with transaction type "([^"]*)")?$/ do |title, author, category_name, transaction_type|
  opts = Hash.new
  opts[:title] = title
  opts[:category] = find_category_by_name(category_name) if category_name
  opts[:transaction_type] = find_transaction_type_by_name(transaction_type) if transaction_type
  opts[:author] = Person.find_by_username(author) if author
  opts[:communities] = [@current_community]

  @listing = FactoryGirl.create(:listing, opts)
end
Given /^the price of that listing is "([^"]*)"?$/ do |price|
  @listing.update_attribute(:price, price) 
end
Given(/^that listing has a numeric answer "(.*?)" for "(.*?)"$/) do |answer, custom_field|
  numeric_custom_field = find_numeric_custom_field_type_by_name(custom_field)
  FactoryGirl.create(:custom_numeric_field_value, listing: @listing, numeric_value: answer, question: numeric_custom_field)
end
When(/^I set search range for "(.*?)" between "(.*?)" and "(.*?)"$/) do |selector, min, max|
  page.execute_script("$('#{selector}').val([#{min.to_f}, #{max.to_f}])");
end
When(/^I set price range between "(.*?)" and "(.*?)"$/) do |min, max|
  steps %Q{
    When I set search range for "#range-slider-price-desktop" between "#{min}" and "#{max}"
  }
end
When(/^I set search range for numeric filter "(.*?)" between "(.*?)" and "(.*?)"$/) do |custom_field, min, max|
  numeric_custom_field = find_numeric_custom_field_type_by_name(custom_field)
  selector = "#range-slider-#{numeric_custom_field.id}-desktop"
  
  steps %Q{
    When I set search range for "#{selector}" between "#{min}" and "#{max}"
  }
end
Given /^that listing has a description "(.*?)"$/ do |description|
  @listing.update_attribute(:description, description)
end
  def find_category_by_name(category_name)
    Category.all.inject("") do |memo, c|
      memo = c if c.display_name("en").eql?(category_name) && c.community_id == @current_community.id
      memo
    end
  end
  def find_transaction_type_by_name(transaction_type_name)
    TransactionType.all.inject("") do |memo, tt|
      memo = tt if tt.display_name("en").eql?(transaction_type_name) && tt.community_id == @current_community.id
      memo
    end
  end
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    click_button(button)
  end
end
When /^(?:|I )fill in "([^"]*)" with "([^"]*)"(?: within "([^"]*)")?$/ do |field, value, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_content(text)
    else
      assert page.has_content?(text)
    end
  end
end
Then /^(?:|I )should not see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_no_content(text)
    else
      assert page.has_no_content?(text)
    end
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)
    case page_name

    when /the home\s?page/
      '/'
    when /the signup page/
      '/en/signup'
    when /the private community sign in page/
      '/en/homepage/sign_in'
    when /the english private community sign in page/
      '/en/homepage/sign_in'  
    when /the requests page/
      '/en/requests'
    when /the offers page/
      '/en/offers'
    when /the login page/
      login_path(:locale => "en")
    when /the new listing page/
      new_listing_path(:locale => "en") 
    when /the edit listing page/
      edit_listing_path(:id => @listing.id, :locale => "en") 
    when /^the give feedback path of "(.*)"$/i
      new_person_message_feedback_path(:person_id => @people[$1].id, :message_id => @conversation.id.to_s, :locale => "en")
    when /^the conversation path of "(.*)"$/i
      person_message_path(:person_id => @people[$1].id, :id => @conversation.id.to_s, :locale => "en")
    when /^the messages page$/i
      received_person_messages_path(:person_id => @logged_in_user.id, :locale => "en")
    when /^the profile page of "(.*)"$/i
      person_path(:id => @people[$1].id, :locale => "en")
    when /^my profile page$/i
      person_path(:id => @logged_in_user.id, :locale => "en")
    when /^the badges page of "(.*)"$/i
      person_badges_path(:person_id => @people[$1].id, :locale => "en")
    when /^the testimonials page of "(.*)"$/i
      person_testimonials_path(:person_id => @people[$1].id, :locale => "en")
    when /the listing page/
      listing_path(:id => @listing.id, :locale => "en")
    when /^the registration page with invitation code "(.*)"$/i
      "/en/signup?code=#{$1}"
    when /^the admin view of community "(.*)"$/i
      edit_details_admin_community_path(:id => Community.find_by_domain($1).id, :locale => "en")
    when /the infos page/
      about_infos_path(:locale => "en")
    when /the terms page/
      terms_infos_path(:locale => "en")
    when /the privacy policy page/
      privacy_infos_path(:locale => "en")
    when /the news page/
      news_items_path(:locale => "en")
    when /new tribe in English/
      new_tribe_path(:community_locale => "en", :locale => "en")
    when /invitations page/
      new_invitation_path(:locale => "en")
    when /the settings page/
      "#{person_path(:id => @logged_in_user.id, :locale => "en")}/settings"
    when /the profile settings page/
      "#{person_path(:id => @logged_in_user.id, :locale => "en")}/settings"
    when /the payment settings page/
      "#{person_path(:id => @logged_in_user.id, :locale => "en")}/settings/payments"
    when /the new Braintree account page/
      "#{person_path(:id => @logged_in_user.id, :locale => "en")}/settings/payments/braintree/new"
    when /the account settings page/
      "#{person_path(:id => @logged_in_user.id, :locale => "en")}/settings/account"
    when /the about page$/
      about_infos_path(:locale => "en")
    when /the feedback page$/
      new_user_feedback_path(:locale => "en")
    when /the custom fields admin page/
      admin_custom_fields_path(:locale => "en")
    when /the categories admin page/
      admin_categories_path(:locale => "en")
    when /the manage members admin page/
      manage_members_admin_community_path(:id => @current_community.id)

    # the following are examples using path_to_pickle

    when /^#{capture_model}(?:'s)? page$/                           # eg. the forum's page
      path_to_pickle $1

    when /^#{capture_model}(?:'s)? #{capture_model}(?:'s)? page$/   # eg. the forum's post's page
      path_to_pickle $1, $2

    when /^#{capture_model}(?:'s)? #{capture_model}'s (.+?) page$/  # eg. the forum's post's comments page
      path_to_pickle $1, $2, :extra => $3                           #  or the forum's post's edit page

    when /^#{capture_model}(?:'s)? (.+?) page$/                     # eg. the forum's posts page
      path_to_pickle $1, :extra => $2                               #  or the forum's edit page
    

    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Given /^there is a numeric field "(.*?)" in community "(.*?)" for category "(.*?)" with min value "(.*?)" and max value "(.*?)"$/ do |name, community, category_name, min, max|
  current_community = Community.find_by_domain(community)
  @custom_field = FactoryGirl.build(:custom_numeric_field, {
    :community_id => current_community.id,
    :names => [CustomFieldName.create(:value => name, :locale => "en")],
    :category_custom_fields => [FactoryGirl.build(:category_custom_field, :category => find_category_by_name(category_name), :custom_field => @custom_field)],
    :min => min.to_i,
    :max => max.to_i
  })
  @custom_field.save
end
  def find_category_by_name(category_name)
    Category.all.inject("") do |memo, c|
      memo = c if c.display_name("en").eql?(category_name) && c.community_id == @current_community.id
      memo
    end
  end
Given(/^this community has price filter enabled with min value (\d+) and max value (\d+)$/) do |min, max|
  @current_community.show_price_filter = true
  @current_community.price_filter_min = min.to_i * 100 # Cents
  @current_community.price_filter_max = max.to_i * 100 # Cents
  @current_community.save!
end
Given /^the (\w+) indexes are processed$/ do |model|
  ThinkingSphinx::Test.index "#{model.underscore}_core", "#{model.underscore}_delta"
  wait_until_index_finished()
end
When(/^I set search range for "(.*?)" between "(.*?)" and "(.*?)"$/) do |selector, min, max|
  page.execute_script("$('#{selector}').val([#{min.to_f}, #{max.to_f}])");
end
