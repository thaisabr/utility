Classes: 5
[name:ContactRequest, file:sharetribe_sharetribe/app/models/contact_request.rb, step:null]
[name:FactoryGirl, file:null, step:Given ]
[name:File, file:null, step:When ]
[name:Person, file:sharetribe_sharetribe/app/models/person.rb, step:Given ]
[name:Rails, file:null, step:When ]

Methods: 21
[name:attach_file, type:Object, file:null, step:When ]
[name:attach_image, type:GeneralSteps, file:sharetribe_sharetribe/features/step_definitions/general_steps.rb, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:execute_script, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find_by_username, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:Given ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_xpath, type:Object, file:null, step:Then ]
[name:homepage, type:Object, file:null, step:When ]
[name:index, type:DashboardController, file:sharetribe_sharetribe/app/controllers/dashboard_controller.rb, step:null]
[name:index, type:HomepageController, file:sharetribe_sharetribe/app/controllers/homepage_controller.rb, step:When ]
[name:login_as, type:Object, file:null, step:Given ]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:When ]
[name:respond_to?, type:Object, file:null, step:Then ]
[name:select, type:Object, file:null, step:When ]
[name:with_scope, type:WebSteps, file:sharetribe_sharetribe/features/step_definitions/web_steps.rb, step:When ]
[name:with_scope, type:WebSteps, file:sharetribe_sharetribe/features/step_definitions/web_steps.rb, step:Then ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 11
sharetribe_sharetribe/app/views/dashboard/_contact_request_form.haml
sharetribe_sharetribe/app/views/dashboard/index.haml
sharetribe_sharetribe/app/views/homepage/_custom_filters.haml
sharetribe_sharetribe/app/views/homepage/_filters.haml
sharetribe_sharetribe/app/views/homepage/_grid_item.haml
sharetribe_sharetribe/app/views/homepage/_list_item.haml
sharetribe_sharetribe/app/views/homepage/_map.haml
sharetribe_sharetribe/app/views/homepage/_price_filter.haml
sharetribe_sharetribe/app/views/homepage/_search_bar.haml
sharetribe_sharetribe/app/views/homepage/index.haml
sharetribe_sharetribe/app/views/layouts/_grid_item_listing_image.haml

