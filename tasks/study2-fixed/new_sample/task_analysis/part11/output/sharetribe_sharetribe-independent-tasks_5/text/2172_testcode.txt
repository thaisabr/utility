When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
  def path_to(page_name)
    case page_name

    when /the home\s?page/
      '/'
    when /the signup page/
      '/en/signup'
    when /the private community sign in page/
      '/en/homepage/sign_in'
    when /the english private community sign in page/
      '/en/homepage/sign_in'  
    when /the requests page/
      '/en/requests'
    when /the offers page/
      '/en/offers'
    when /the login page/
      login_path(:locale => "en")
    when /the new listing page/
      new_listing_path(:locale => "en") 
    when /the edit listing page/
      edit_listing_path(:id => @listing.id, :locale => "en") 
    when /^the give feedback path of "(.*)"$/i
      new_person_message_feedback_path(:person_id => @people[$1].id, :message_id => @conversation.id.to_s, :locale => "en")
    when /^the conversation path of "(.*)"$/i
      person_message_path(:person_id => @people[$1].id, :id => @conversation.id.to_s, :locale => "en")
    when /^the messages page$/i
      received_person_messages_path(:person_id => @logged_in_user.id, :locale => "en")
    when /^the profile page of "(.*)"$/i
      person_path(:id => @people[$1].id, :locale => "en")
    when /^my profile page$/i
      person_path(:id => @logged_in_user.id, :locale => "en")
    when /^the badges page of "(.*)"$/i
      person_badges_path(:person_id => @people[$1].id, :locale => "en")
    when /^the testimonials page of "(.*)"$/i
      person_testimonials_path(:person_id => @people[$1].id, :locale => "en")
    when /the listing page/
      listing_path(:id => @listing.id, :locale => "en")
    when /^the registration page with invitation code "(.*)"$/i
      "/en/signup?code=#{$1}"
    when /^the admin view of community "(.*)"$/i
      edit_details_admin_community_path(:id => Community.find_by_domain($1).id, :locale => "en")
    when /the infos page/
      about_infos_path(:locale => "en")
    when /the terms page/
      terms_infos_path(:locale => "en")
    when /the privacy policy page/
      privacy_infos_path(:locale => "en")
    when /the news page/
      news_items_path(:locale => "en")
    when /new tribe in English/
      new_tribe_path(:community_locale => "en", :locale => "en")
    when /invitations page/
      new_invitation_path(:locale => "en")
    when /the settings page/
      "#{person_path(:id => @logged_in_user.id, :locale => "en")}/settings"
    when /the profile settings page/
      "#{person_path(:id => @logged_in_user.id, :locale => "en")}/settings"
    when /the payment settings page/
      "#{person_path(:id => @logged_in_user.id, :locale => "en")}/settings/payments"
    when /the new Braintree account page/
      "#{person_path(:id => @logged_in_user.id, :locale => "en")}/settings/payments/braintree/new"
    when /the account settings page/
      "#{person_path(:id => @logged_in_user.id, :locale => "en")}/settings/account"
    when /the about page$/
      about_infos_path(:locale => "en")
    when /the custom fields admin page/
      admin_custom_fields_path(:locale => "en")
    when /the categories admin page/
      admin_categories_path(:locale => "en")
    when /the manage members admin page/
      manage_members_admin_community_path(:id => @current_community.id)

    # the following are examples using path_to_pickle

    when /^#{capture_model}(?:'s)? page$/                           # eg. the forum's page
      path_to_pickle $1

    when /^#{capture_model}(?:'s)? #{capture_model}(?:'s)? page$/   # eg. the forum's post's page
      path_to_pickle $1, $2

    when /^#{capture_model}(?:'s)? #{capture_model}'s (.+?) page$/  # eg. the forum's post's comments page
      path_to_pickle $1, $2, :extra => $3                           #  or the forum's post's edit page

    when /^#{capture_model}(?:'s)? (.+?) page$/                     # eg. the forum's posts page
      path_to_pickle $1, :extra => $2                               #  or the forum's edit page
    

    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Given(/^user "(.*?)" is banned in this community$/) do |arg1|
  pending # express the regexp above with the code you wish you had
end
Then(/^I should see a message that I have been banned$/) do
  pending # express the regexp above with the code you wish you had
end
Then(/^I should be able to send a message to admin$/) do
  pending # express the regexp above with the code you wish you had
end
Given /^I am logged in(?: as "([^"]*)")?$/ do |person|
  username = person || "kassi_testperson1"
  person = Person.find_by_username(username) || FactoryGirl.create(:person, :username => username)
  login_as(person, :scope => :person)
  visit root_path(:locale => :en)
  @logged_in_user = person
end
Given /^there are following users:$/ do |person_table|
  @people = {}
  person_table.hashes.each do |hash|
    defaults = { 
      password: "testi",
      given_name: "Test",
      family_name: "Person"
    }

    username = hash['person']
    id = hash['id']

    person_opts = defaults.merge({
      username: hash['person'],
    }).merge(hash.except('person'))

    @hash_person, @hash_session = Person.find_by_username(username) || FactoryGirl.create(:person, person_opts)
    @hash_person.save!

    @hash_person = force_override_model_id(id, @hash_person, Person, [Email]) if id

    if hash['email'] then
      @hash_person.emails = [Email.create(:address => hash['email'], :send_notifications => true, :person => @hash_person, :confirmed_at => DateTime.now)]
      @hash_person.save!
    end

    @hash_person.update_attributes({:preferences => { "email_about_new_comments_to_own_listing" => "true", "email_about_new_messages" => "true" }})
    #unless CommunityMembership.find_by_person_id_and_community_id(@hash_person.id, Community.first.id)
      CommunityMembership.create(:community_id => Community.first.id, :person_id => @hash_person.id, :consent => Community.first.consent, :status => "accepted")
    #end
    attributes_to_update = hash.except('person','person_id', 'locale')
    @hash_person.update_attributes(attributes_to_update) unless attributes_to_update.empty?
    @hash_person.set_default_preferences
    if hash['locale'] 
      @hash_person.locale = hash['locale']
      @hash_person.save
    end
    @people[username] = @hash_person
  end
end
  def force_override_model_id(id, model_instance, model_class, associated_model_classes=[])
    old_id = model_instance.id
    model_class.update_all({:id => id}, {:id => old_id})
    
    # Associates
    foreign_key = "#{model_class.name.downcase}_id".to_sym
    associated_model_classes.each do |associated_model_class|
      associated_model_class.update_all({foreign_key => id}, {foreign_key => old_id})
    end

    # Reload
    model_class.find(id)
  end
