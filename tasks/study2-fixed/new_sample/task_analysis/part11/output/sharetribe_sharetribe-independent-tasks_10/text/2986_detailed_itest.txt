Classes: 11
[name:ActionMailer, file:null, step:Given ]
[name:Capybara, file:null, step:Given ]
[name:Conversation, file:sharetribe_sharetribe/app/models/conversation.rb, step:When ]
[name:Conversation, file:sharetribe_sharetribe/app/models/conversation.rb, step:Then ]
[name:ConversationsController, file:sharetribe_sharetribe/app/controllers/conversations_controller.rb, step:When ]
[name:Listing, file:sharetribe_sharetribe/app/models/listing.rb, step:Given ]
[name:Listing, file:sharetribe_sharetribe/app/models/listing.rb, step:Then ]
[name:Object, file:null, step:Then ]
[name:Person, file:sharetribe_sharetribe/app/models/person.rb, step:null]
[name:Person, file:sharetribe_sharetribe/app/models/person.rb, step:Given ]
[name:Person, file:sharetribe_sharetribe/app/models/person.rb, step:When ]

Methods: 106
[name:be_any, type:Object, file:null, step:Then ]
[name:be_nil, type:Object, file:null, step:Then ]
[name:body, type:Object, file:null, step:Then ]
[name:capture_email, type:Object, file:null, step:When ]
[name:capture_email, type:Object, file:null, step:Then ]
[name:capture_fields, type:Object, file:null, step:Then ]
[name:capture_fields, type:Object, file:null, step:Given ]
[name:capture_model, type:Object, file:null, step:Then ]
[name:capture_model, type:Object, file:null, step:Given ]
[name:capture_plural_factory, type:Object, file:null, step:Given ]
[name:capture_plural_factory, type:Object, file:null, step:Then ]
[name:capture_predicate, type:Object, file:null, step:Then ]
[name:capture_value, type:Object, file:null, step:Then ]
[name:clear, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_first_link_in_email, type:Object, file:null, step:When ]
[name:cookie, type:Object, file:null, step:Given ]
[name:create!, type:Listing, file:sharetribe_sharetribe/app/models/listing.rb, step:Given ]
[name:create_model, type:Object, file:null, step:Given ]
[name:create_models_from_table, type:Object, file:null, step:Given ]
[name:each, type:Object, file:null, step:Given ]
[name:email, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:null]
[name:email, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:When ]
[name:email, type:Object, file:null, step:Then ]
[name:email, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:Then ]
[name:email_for, type:Email, file:sharetribe_sharetribe/features/support/email.rb, step:Then ]
[name:emails, type:Object, file:null, step:Given ]
[name:emails, type:Object, file:null, step:Then ]
[name:eql, type:Object, file:null, step:Then ]
[name:eval, type:Object, file:null, step:Then ]
[name:family_name, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:null]
[name:family_name, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find_model, type:Object, file:null, step:Then ]
[name:find_model!, type:Object, file:null, step:Then ]
[name:find_models, type:Object, file:null, step:Then ]
[name:find_models_from_table, type:Object, file:null, step:Then ]
[name:first, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:Given ]
[name:from_now, type:Object, file:null, step:Given ]
[name:get_test_person_and_session, type:Object, file:null, step:Given ]
[name:given_name, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:null]
[name:given_name, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:When ]
[name:gsub, type:Object, file:null, step:Then ]
[name:has_content?, type:Object, file:null, step:When ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:has_no_content?, type:Object, file:null, step:When ]
[name:has_no_content?, type:Object, file:null, step:Then ]
[name:hashes, type:Object, file:null, step:Given ]
[name:have_content, type:Object, file:null, step:When ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_no_content, type:Object, file:null, step:When ]
[name:have_no_content, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:Given ]
[name:id, type:Object, file:null, step:Then ]
[name:include, type:Object, file:null, step:Then ]
[name:index, type:HomepageController, file:sharetribe_sharetribe/app/controllers/homepage_controller.rb, step:When ]
[name:join, type:Object, file:null, step:Then ]
[name:listing, type:Object, file:null, step:When ]
[name:model, type:Object, file:null, step:Then ]
[name:model!, type:Object, file:null, step:Then ]
[name:new, type:SessionsController, file:sharetribe_sharetribe/app/controllers/sessions_controller.rb, step:null]
[name:new, type:PeopleController, file:sharetribe_sharetribe/app/controllers/people_controller.rb, step:null]
[name:new, type:ListingsController, file:sharetribe_sharetribe/app/controllers/listings_controller.rb, step:When ]
[name:new, type:SessionsController, file:sharetribe_sharetribe/app/controllers/sessions_controller.rb, step:When ]
[name:new, type:PeopleController, file:sharetribe_sharetribe/app/controllers/people_controller.rb, step:When ]
[name:offers, type:ListingsController, file:sharetribe_sharetribe/app/controllers/listings_controller.rb, step:When ]
[name:other_party, type:Conversation, file:sharetribe_sharetribe/app/models/conversation.rb, step:When ]
[name:page, type:Object, file:null, step:When ]
[name:page, type:Object, file:null, step:Then ]
[name:password, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:null]
[name:password, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:When ]
[name:password2, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:null]
[name:password2, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:When ]
[name:path_to, type:Paths, file:sharetribe_sharetribe/features/support/paths.rb, step:Then ]
[name:path_to_pickle, type:Object, file:null, step:Then ]
[name:push, type:Object, file:null, step:Then ]
[name:raise, type:Object, file:null, step:Then ]
[name:received, type:ConversationsController, file:sharetribe_sharetribe/app/controllers/conversations_controller.rb, step:When ]
[name:requests, type:ListingsController, file:sharetribe_sharetribe/app/controllers/listings_controller.rb, step:When ]
[name:respond_to?, type:Object, file:null, step:When ]
[name:respond_to?, type:Object, file:null, step:Then ]
[name:save_and_open_emails, type:Object, file:null, step:Then ]
[name:send, type:Object, file:null, step:Then ]
[name:send, type:SmsHelper, file:sharetribe_sharetribe/app/helpers/sms_helper.rb, step:Then ]
[name:send_button, type:Object, file:null, step:When ]
[name:show, type:ConversationsController, file:sharetribe_sharetribe/app/controllers/conversations_controller.rb, step:When ]
[name:singularize, type:Object, file:null, step:Given ]
[name:singularize, type:Object, file:null, step:Then ]
[name:size, type:Object, file:null, step:Given ]
[name:size, type:Object, file:null, step:Then ]
[name:split, type:Object, file:null, step:Given ]
[name:terms, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:null]
[name:terms, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:When ]
[name:times, type:Object, file:null, step:Given ]
[name:to_f, type:Object, file:null, step:Then ]
[name:to_i, type:Object, file:null, step:Given ]
[name:to_i, type:Object, file:null, step:Then ]
[name:to_s, type:Object, file:null, step:Then ]
[name:to_sym, type:Object, file:null, step:Then ]
[name:update_attributes, type:Object, file:null, step:Given ]
[name:username, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:null]
[name:username, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:When ]
[name:visit_in_email, type:Object, file:null, step:When ]
[name:with_scope, type:WebSteps, file:sharetribe_sharetribe/features/step_definitions/web_steps.rb, step:When ]
[name:with_scope, type:WebSteps, file:sharetribe_sharetribe/features/step_definitions/web_steps.rb, step:Then ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 48
sharetribe_sharetribe/app/views/common/_staples.haml
sharetribe_sharetribe/app/views/conversations/_additional_messages.haml
sharetribe_sharetribe/app/views/conversations/_additional_notifications.haml
sharetribe_sharetribe/app/views/conversations/_inbox_spacer.haml
sharetribe_sharetribe/app/views/conversations/_message.haml
sharetribe_sharetribe/app/views/conversations/_notification.haml
sharetribe_sharetribe/app/views/conversations/_received_conversation.haml
sharetribe_sharetribe/app/views/conversations/_sent_conversation.haml
sharetribe_sharetribe/app/views/conversations/_status.haml
sharetribe_sharetribe/app/views/conversations/_status_link.haml
sharetribe_sharetribe/app/views/conversations/index.haml
sharetribe_sharetribe/app/views/conversations/new.haml
sharetribe_sharetribe/app/views/conversations/notifications.haml
sharetribe_sharetribe/app/views/conversations/show.haml
sharetribe_sharetribe/app/views/homepage/_recent_listing.haml
sharetribe_sharetribe/app/views/homepage/index.haml
sharetribe_sharetribe/app/views/listings/_additional_listings.haml
sharetribe_sharetribe/app/views/listings/_comment.haml
sharetribe_sharetribe/app/views/listings/_comment_form.haml
sharetribe_sharetribe/app/views/listings/_edit_links.haml
sharetribe_sharetribe/app/views/listings/_help_texts.haml
sharetribe_sharetribe/app/views/listings/_left_panel_link.haml
sharetribe_sharetribe/app/views/listings/_listed_listing.haml
sharetribe_sharetribe/app/views/listings/_listed_listings.haml
sharetribe_sharetribe/app/views/listings/_listing_spacer.haml
sharetribe_sharetribe/app/views/listings/_reply_link.haml
sharetribe_sharetribe/app/views/listings/edit.haml
sharetribe_sharetribe/app/views/listings/form/_departure_time.haml
sharetribe_sharetribe/app/views/listings/form/_description.haml
sharetribe_sharetribe/app/views/listings/form/_destination.haml
sharetribe_sharetribe/app/views/listings/form/_form_content.haml
sharetribe_sharetribe/app/views/listings/form/_images.haml
sharetribe_sharetribe/app/views/listings/form/_javascripts.haml
sharetribe_sharetribe/app/views/listings/form/_origin.haml
sharetribe_sharetribe/app/views/listings/form/_send_button.haml
sharetribe_sharetribe/app/views/listings/form/_share_type.haml
sharetribe_sharetribe/app/views/listings/form/_tag_list.haml
sharetribe_sharetribe/app/views/listings/form/_title.haml
sharetribe_sharetribe/app/views/listings/form/_valid_until.haml
sharetribe_sharetribe/app/views/listings/form/_valid_until_radio_buttons.haml
sharetribe_sharetribe/app/views/listings/form/_visibility.haml
sharetribe_sharetribe/app/views/listings/index.haml
sharetribe_sharetribe/app/views/listings/new.haml
sharetribe_sharetribe/app/views/listings/show.haml
sharetribe_sharetribe/app/views/people/_help_texts.haml
sharetribe_sharetribe/app/views/people/new.haml
sharetribe_sharetribe/app/views/sessions/_password_forgotten.haml
sharetribe_sharetribe/app/views/sessions/new.haml

