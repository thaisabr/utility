Feature: User creates a new request
In order to perform a certain task using an item, a skill, or a transport
As a person who does not have the required item, skill, or transport
I want to be able to request an item, a favor, or transport I need from other users
Scenario: Creating a new item request successfully
Given I am logged in
And I am on the home page
When I follow "Request something"
And I fill in "listing_title" with "My request"
And I fill in "listing_description" with "My description"
And I check "buy"
And I press "Save request"
Then I should see "Item request: My request" within "h1"
And I should see "Request created successfully" within "#notifications"
