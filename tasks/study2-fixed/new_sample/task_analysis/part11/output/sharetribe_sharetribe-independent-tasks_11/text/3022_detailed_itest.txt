Classes: 1
[name:create, file:null, step:null]

Methods: 17
[name:attach_file, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:Then ]
[name:index, type:HomepageController, file:sharetribe_sharetribe/app/controllers/homepage_controller.rb, step:When ]
[name:new, type:SessionsController, file:sharetribe_sharetribe/app/controllers/sessions_controller.rb, step:null]
[name:new, type:ListingsController, file:sharetribe_sharetribe/app/controllers/listings_controller.rb, step:When ]
[name:page, type:Object, file:null, step:Then ]
[name:respond_to?, type:Object, file:null, step:Then ]
[name:select, type:Object, file:null, step:When ]
[name:uncheck, type:Object, file:null, step:When ]
[name:with_scope, type:WebSteps, file:sharetribe_sharetribe/features/step_definitions/web_steps.rb, step:When ]
[name:with_scope, type:WebSteps, file:sharetribe_sharetribe/features/step_definitions/web_steps.rb, step:Then ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 7
sharetribe_sharetribe/app/views/common/_staples.haml
sharetribe_sharetribe/app/views/homepage/_recent_event.haml
sharetribe_sharetribe/app/views/homepage/_recent_listing.haml
sharetribe_sharetribe/app/views/homepage/index.haml
sharetribe_sharetribe/app/views/listings/new.haml
sharetribe_sharetribe/app/views/sessions/_password_forgotten.haml
sharetribe_sharetribe/app/views/sessions/new.haml

