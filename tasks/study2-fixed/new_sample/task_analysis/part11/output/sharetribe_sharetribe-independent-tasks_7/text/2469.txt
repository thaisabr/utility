Feature: User logging in and out
In order to log in and out of Sharetribe
As a user
I want to be able to enter username and password and log in to Sharetribe and also log out
Scenario: User tries to log in to organization community with personal account
Given community "test" allows only organizations
And "kassi_testperson1" is not an organization
And I am not logged in
And I am on the login page
When I fill in "main_person_login" with "kassi_testperson1"
And I fill in "main_person_password" with "testi"
And I click "#main_log_in_button"
Then I should see "You can not login to this community with your personal account"
Then I should not be logged in
