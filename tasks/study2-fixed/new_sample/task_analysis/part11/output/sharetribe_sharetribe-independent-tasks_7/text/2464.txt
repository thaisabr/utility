Feature: User creates a new listing
In order to perform a certain task using an item, a skill, or a transport, or to help others
As a person who does not have the required item, skill, or transport, or has them and wants offer them to others
I want to be able to offer and request an item, a favor, a transport or housing
@javascript
Scenario: User creates a new listing with price
Given I am logged in
When I create a new listing "Sledgehammer" with price
Then I should see "Sledgehammer" within "#listing-title"
Feature: User creates a new listing with payments
Background:
Given there is an organization "company"
And "company" is a member of community "test"
And community "test" has payments in use via Checkout
And I am logged in as "company"
@javascript
Scenario: Creating a new offer with payment
Given "company" has Checkout account
When I create a new listing "Sledgehammer" with price
Then I should see "Sledgehammer" within "#listing-title"
And I should receive no emails
@javascript
Scenario: Creating a new offer with payment but without payment settings
Given "company" does not have Checkout account
When I create a new listing "Sledgehammer" with price
Then I should see "Sledgehammer" within "#listing-title"
And I should receive an email about missing payment details
When I follow "Payment settings" in the email
Then I should be on the payment settings page
