Given /^I am logged in(?: as "([^"]*)")?$/ do |person|
  username = person || "kassi_testperson1"
  person = Person.find_by_username(username) || FactoryGirl.create(:person, :username => username)
  login_as(person, :scope => :person)
  visit root_path(:locale => :en)
  @logged_in_user = person
end
When /^I create a new listing "([^"]*)" with price$/ do |title|
  steps %Q{
    Given I am on the home page
    When I follow "new-listing-link"
    And I follow "I have something to offer to others"
    And I follow "An item"
    And I follow "Tools" within "#option-groups"
    And I follow "I'm selling it"
    And I fill in "listing_title" with "#{title}"
    And I fill in "listing_price" with "dsfsdf"
    And I press "Save listing"
    Then I should see "Price must be a whole number."
    When I fill in "listing_price" with "20"
    And I press "Save listing"
  }
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_content(text)
    else
      assert page.has_content?(text)
    end
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
Given /^there is an organization "(.*?)"$/ do |org_username|
  FactoryGirl.create(:person, :username => org_username, :is_organization => true)
end
Given /^"(.*?)" has Checkout account$/ do |org_username|
  org = Person.find_by_username(org_username)
  org.checkout_merchant_key = "SAIPPUAKAUPPIAS"
  org.checkout_merchant_id = "375917"
  org.save!
end
Given /^"(.*?)" does not have Checkout account$/ do |org_username|
  org = Person.find_by_username(org_username)
  org.checkout_merchant_key = nil
  org.checkout_merchant_id = nil
  org.save!
end
Then /^I should receive an email about missing payment details$/ do
  steps %Q{
    Then I should receive an email with subject "Remember to add your payment details to receive payments"
    When I open the email
    And I should see "However, you haven't yet added your payment details. In order to receive the payment you have to add your payment information." in the email body
  }
end
Then /^(?:I|they|"([^"]*?)") should receive (an|no|\d+) emails?$/ do |address, amount|
  unread_emails_for(address).size.should == parse_email_count(amount)
end
When /^(?:I|they) follow "([^"]*?)" in the email$/ do |link|
  visit_in_email(link)
end
Given /^"(.*?)" is a member of community "(.*?)"$/ do |username, community_name|
  community = Community.find_by_name!(community_name)
  person = Person.find_by_username!(username)
  membership = FactoryGirl.create(:community_membership, :person => person, :community => community)
  membership.save!
end
Given /^community "([^"]*)" has payments in use(?: via (\w+))?$/ do |community_domain, gateway_name|
  gateway_name ||= "Checkout"
  gateway = PaymentGateway.find_by_type(gateway_name)
  
  if gateway.nil?
    # if missing, create it
    gateway = Kernel.const_get(gateway_name).create
  end
  
  community = Community.find_by_domain(community_domain)
  community.update_attributes(:payments_in_use => true, :vat => "24", :commission_percentage => "8")
  community.payment_gateways << gateway
end
Then(/^#{capture_model} should be #{capture_model}$/) do |a, b|
  model!(a).should == model!(b)
end
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    click_button(button)
  end
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
When /^(?:|I )fill in "([^"]*)" with "([^"]*)"(?: within "([^"]*)")?$/ do |field, value, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_content(text)
    else
      assert page.has_content?(text)
    end
  end
end
  def path_to(page_name)
    case page_name

    when /the home\s?page/
      '/'
    when /the signup page/
      '/en/signup'
    when /the private community sign in page/
      '/en/homepage/sign_in'
    when /the english private community sign in page/
      '/en/homepage/sign_in'  
    when /the requests page/
      '/en/requests'
    when /the offers page/
      '/en/offers'
    when /the login page/
      login_path(:locale => "en")
    when /the edit listing page/
      edit_listing_path(:id => @listing.id, :locale => "en") 
    when /^the give feedback path of "(.*)"$/i
      new_person_message_feedback_path(:person_id => @people[$1].id, :message_id => @conversation.id.to_s, :locale => "en")
    when /^the conversation path of "(.*)"$/i
      person_message_path(:person_id => @people[$1].id, :id => @conversation.id.to_s, :locale => "en")
    when /^the profile page of "(.*)"$/i
      person_path(:id => @people[$1].id, :locale => "en")
    when /^my profile page$/i
      person_path(:id => @logged_in_user.id, :locale => "en")
    when /^the badges page of "(.*)"$/i
      person_badges_path(:person_id => @people[$1].id, :locale => "en")
    when /^the testimonials page of "(.*)"$/i
      person_testimonials_path(:person_id => @people[$1].id, :locale => "en")
    when /the listing page/
      listing_path(:id => @listing.id, :locale => "en")
    when /^the registration page with invitation code "(.*)"$/i
      "/en/signup?code=#{$1}"
    when /the infos page/
      about_infos_path(:locale => "en")
    when /the news page/
      news_items_path(:locale => "en")
    when /new tribe in English/
      new_tribe_path(:community_locale => "en", :locale => "en")
    when /invitations page$/i
      new_invitation_path(:locale => "en")
    when /the settings page/
      "#{person_path(:id => @logged_in_user.id, :locale => "en")}/settings"
    when /the payment settings page/
      "#{person_path(:id => @logged_in_user.id, :locale => "en")}/settings/payments"

    # the following are examples using path_to_pickle

    when /^#{capture_model}(?:'s)? page$/                           # eg. the forum's page
      path_to_pickle $1

    when /^#{capture_model}(?:'s)? #{capture_model}(?:'s)? page$/   # eg. the forum's post's page
      path_to_pickle $1, $2

    when /^#{capture_model}(?:'s)? #{capture_model}'s (.+?) page$/  # eg. the forum's post's comments page
      path_to_pickle $1, $2, :extra => $3                           #  or the forum's post's edit page

    when /^#{capture_model}(?:'s)? (.+?) page$/                     # eg. the forum's posts page
      path_to_pickle $1, :extra => $2                               #  or the forum's edit page
    

    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Then /^(?:I|they|"([^"]*?)") should receive (an|no|\d+) emails? with subject "([^"]*?)"$/ do |address, amount, subject|
  unread_emails_for(address).select { |m| m.subject =~ Regexp.new(Regexp.escape(subject)) }.size.should == parse_email_count(amount)
end
When /^(?:I|they|"([^"]*?)") opens? the email$/ do |address|
  open_email(address)
end
Then /^(?:I|they) should see "([^"]*?)" in the email body$/ do |text|
  current_email.default_part_body.to_s.should include(text)
end
