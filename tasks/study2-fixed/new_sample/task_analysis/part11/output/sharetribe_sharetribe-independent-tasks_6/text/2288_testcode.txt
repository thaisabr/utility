Given /^I am logged in(?: as "([^"]*)")?$/ do |person|
  username = person || "kassi_testperson1"
  person = Person.find_by_username(username) || FactoryGirl.create(:person, :username => username)
  login_as(person, :scope => :person)
  visit root_path(:locale => :en)
  @logged_in_user = person
end
Given /^community "(.*?)" has custom fields enabled$/ do |community_domain|
  Community.find_by_domain(community_domain).update_attributes({:custom_fields_allowed => true})
end
When /^I add a new custom field "(.*?)"$/ do |field_name|
  steps %Q{
    When I select "Dropdown" from "field_type"
    And I fill in "custom_field[name_attributes][en]" with "#{field_name}"
    And I fill in "custom_field[name_attributes][fi]" with "Talon tyyppi"
    And I toggle category "Spaces"
    And I fill in "custom_field[option_attributes][new-1][title_attributes][en]" with "Room"
    And I fill in "custom_field[option_attributes][new-1][title_attributes][fi]" with "Huone"
    And I fill in "custom_field[option_attributes][new-2][title_attributes][en]" with "Appartment"
    And I fill in "custom_field[option_attributes][new-2][title_attributes][fi]" with "Asunto"
    And I follow "custom-fields-add-option"
    When I fill in "custom_field[option_attributes][jsnew-1][title_attributes][en]" with "House"
    And I fill in "custom_field[option_attributes][jsnew-1][title_attributes][fi]" with "Talo"
    And I press submit
  }
end
When /^I add a new custom field "(.*?)" with invalid data$/ do |field_name|
  steps %Q{
    When I select "Dropdown" from "field_type" 
    And I fill in "custom_field[name_attributes][en]" with "#{field_name}"
    And I fill in "custom_field[option_attributes][new-1][title_attributes][en]" with "Room"
    And I fill in "custom_field[option_attributes][new-1][title_attributes][fi]" with "Huone"
    And I fill in "custom_field[option_attributes][new-2][title_attributes][en]" with "Appartment"
    And I follow "custom-fields-add-option"
    And I fill in "custom_field[option_attributes][jsnew-1][title_attributes][en]" with "House"
    And I fill in "custom_field[option_attributes][jsnew-1][title_attributes][fi]" with "Talo"
    And I press submit
  }
end
Given /^there is a custom field "(.*?)" in community "(.*?)"$/ do |name, community|
  current_community = Community.find_by_domain(community)
  @custom_field = FactoryGirl.build(:custom_dropdown_field, :community_id => current_community.id)
  @custom_field.names << CustomFieldName.create(:value => name, :locale => "en")
  @custom_field.category_custom_fields.build(:category => current_community.categories.first)
  @custom_field.options << FactoryGirl.build(:custom_field_option)
  @custom_field.options << FactoryGirl.build(:custom_field_option)
  @custom_field.save
end
Given /^there is a custom dropdown field "(.*?)" in community "(.*?)"(?: in category "([^"]*)")? with options:$/ do |name, community, category, options|
  current_community = Community.find_by_domain(community)
  custom_field = FactoryGirl.build(:custom_dropdown_field, :community_id => current_community.id)
  custom_field.names << CustomFieldName.create(:value => name, :locale => "en")
  
  if category
    custom_field.category_custom_fields.build(:category => Category.find_by_name(category))
  else
    custom_field.category_custom_fields.build(:category => current_community.categories.first)
  end

  custom_field.options << options.hashes.map do |hash|
    en = FactoryGirl.build(:custom_field_option_title, :value => hash['fi'], :locale => 'fi')
    fi = FactoryGirl.build(:custom_field_option_title, :value => hash['en'], :locale => 'en')
    FactoryGirl.build(:custom_field_option, :titles => [en, fi])
  end

  custom_field.save!
  
  @custom_fields ||= []
  @custom_fields << custom_field 
end
Given /^there is a custom text field "(.*?)" in community "(.*?)"(?: in category "([^"]*)")?$/ do |name, community, category|
  current_community = Community.find_by_domain(community)
  custom_field = FactoryGirl.build(:custom_text_field, :community_id => current_community.id)
  custom_field.names << CustomFieldName.create(:value => name, :locale => "en")
  
  if category
    custom_field.category_custom_fields.build(:category => Category.find_by_name(category))
  else
    custom_field.category_custom_fields.build(:category => current_community.categories.first)
  end

  custom_field.save!
  
  @custom_fields ||= []
  @custom_fields << custom_field 
end
When /^(?:|I )select "([^"]*)" from dropdown "([^"]*)"$/ do |value, field_name|
  field_id = find_custom_field_by_name(field_name).id
  select(value, :from => "custom_fields_#{field_id}")
end
When /^custom field "(.*?)" is not required$/ do |field_name|
  find_custom_field_by_name(field_name).update_attribute(:required, false)
end
When /^(?:|I )fill in text field "([^"]*)" with "([^"]*)"$/ do |field_name, value|
  field_id = find_custom_field_by_name(field_name).id
  fill_in("custom_fields_#{field_id}", :with => value)
end
  def find_custom_field_by_name(field_name)
    @custom_fields.inject("") do |memo, f|
      memo = f if f.name.eql?(field_name)
      memo
    end
  end
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    click_button(button)
  end
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
When /^(?:|I )fill in "([^"]*)" with "([^"]*)"(?: within "([^"]*)")?$/ do |field, value, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_content(text)
    else
      assert page.has_content?(text)
    end
  end
end
Then /^(?:|I )should not see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_no_content(text)
    else
      assert page.has_no_content?(text)
    end
  end
end
Then /^I should see (\d+) validation errors$/ do |errors_count|
  errors = all("label.error");
  errors.size.should eql(errors_count.to_i)
  all("label.error").each { |error| error.should be_visible }
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)
    case page_name

    when /the home\s?page/
      '/'
    when /the signup page/
      '/en/signup'
    when /the private community sign in page/
      '/en/homepage/sign_in'
    when /the english private community sign in page/
      '/en/homepage/sign_in'  
    when /the requests page/
      '/en/requests'
    when /the offers page/
      '/en/offers'
    when /the login page/
      login_path(:locale => "en")
    when /the new listing page/
      new_listing_path(:locale => "en") 
    when /the edit listing page/
      edit_listing_path(:id => @listing.id, :locale => "en") 
    when /^the give feedback path of "(.*)"$/i
      new_person_message_feedback_path(:person_id => @people[$1].id, :message_id => @conversation.id.to_s, :locale => "en")
    when /^the conversation path of "(.*)"$/i
      person_message_path(:person_id => @people[$1].id, :id => @conversation.id.to_s, :locale => "en")
    when /^the messages page$/i
      received_person_messages_path(:person_id => @logged_in_user.id, :locale => "en")
    when /^the profile page of "(.*)"$/i
      person_path(:id => @people[$1].id, :locale => "en")
    when /^my profile page$/i
      person_path(:id => @logged_in_user.id, :locale => "en")
    when /^the badges page of "(.*)"$/i
      person_badges_path(:person_id => @people[$1].id, :locale => "en")
    when /^the testimonials page of "(.*)"$/i
      person_testimonials_path(:person_id => @people[$1].id, :locale => "en")
    when /the listing page/
      listing_path(:id => @listing.id, :locale => "en")
    when /^the registration page with invitation code "(.*)"$/i
      "/en/signup?code=#{$1}"
    when /^the admin view of community "(.*)"$/i
      edit_details_admin_community_path(:id => Community.find_by_domain($1).id, :locale => "en")
    when /the infos page/
      about_infos_path(:locale => "en")
    when /the terms page/
      terms_infos_path(:locale => "en")
    when /the privacy policy page/
      privacy_infos_path(:locale => "en")
    when /the news page/
      news_items_path(:locale => "en")
    when /new tribe in English/
      new_tribe_path(:community_locale => "en", :locale => "en")
    when /invitations page/
      new_invitation_path(:locale => "en")
    when /the settings page/
      "#{person_path(:id => @logged_in_user.id, :locale => "en")}/settings"
    when /the profile settings page/
      "#{person_path(:id => @logged_in_user.id, :locale => "en")}/settings"
    when /the payment settings page/
      "#{person_path(:id => @logged_in_user.id, :locale => "en")}/settings/payments"
    when /the new Braintree account page/
      "#{person_path(:id => @logged_in_user.id, :locale => "en")}/settings/payments/braintree/new"
    when /the account settings page/
      "#{person_path(:id => @logged_in_user.id, :locale => "en")}/settings/account"
    when /the about page$/
      about_infos_path(:locale => "en")
    when /the custom fields admin page/
      admin_custom_fields_path(:locale => "en")

    # the following are examples using path_to_pickle

    when /^#{capture_model}(?:'s)? page$/                           # eg. the forum's page
      path_to_pickle $1

    when /^#{capture_model}(?:'s)? #{capture_model}(?:'s)? page$/   # eg. the forum's post's page
      path_to_pickle $1, $2

    when /^#{capture_model}(?:'s)? #{capture_model}'s (.+?) page$/  # eg. the forum's post's comments page
      path_to_pickle $1, $2, :extra => $3                           #  or the forum's post's edit page

    when /^#{capture_model}(?:'s)? (.+?) page$/                     # eg. the forum's posts page
      path_to_pickle $1, :extra => $2                               #  or the forum's edit page
    

    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Given /^the (\w+) indexes are processed$/ do |model|
  model = model.titleize.gsub(/\s/, '').constantize
  ThinkingSphinx::Test.index *model.sphinx_index_names
  sleep(0.25) # Wait for Sphinx to catch up
end
Given /^there is a dropdown field "(.*?)" for category "(.*?)" in community "(.*?)" with options:$/ do |field_title, category_name, community_domain, opts_table|
  @community = Community.find_by_domain(community_domain)
  @category = Category.find_by_name(category_name)
  @custom_field = FactoryGirl.build(:custom_dropdown_field, :type => "Dropdown", :community => @community)
  @custom_field.category_custom_fields << FactoryGirl.build(:category_custom_field, :category => @category, :custom_field => @custom_field)
  @custom_field.names << CustomFieldName.create(:value => field_title, :locale => "en")
  
  opts_table.hashes.each do |hash|
    title = CustomFieldOptionTitle.create(:value => hash[:title], :locale => "en")
    option = FactoryGirl.build(:custom_field_option, :titles => [title])
    @custom_field.options << option
  end

  @custom_field.save!
end
When /^(?:|I )press submit(?: within "([^"]*)")?$/ do |selector|
  with_scope(selector) do
    find("[type=submit]").click
  end
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
When /^(?:|I )fill in "([^"]*)" with "([^"]*)"(?: within "([^"]*)")?$/ do |field, value, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
When /^(?:|I )select "([^"]*)" from "([^"]*)"(?: within "([^"]*)")?$/ do |value, field, selector|
  with_scope(selector) do
    select(value, :from => field)
  end
end
When /^I toggle category "(.*?)"$/ do |category|
  find(:css, "label", :text => category).click()
end
