Classes: 8
[name:Community, file:sharetribe_sharetribe/app/models/community.rb, step:Given ]
[name:CommunityMembership, file:sharetribe_sharetribe/app/models/community_membership.rb, step:Given ]
[name:ContactRequest, file:sharetribe_sharetribe/app/models/contact_request.rb, step:Given ]
[name:Email, file:sharetribe_sharetribe/app/models/email.rb, step:Given ]
[name:Email, file:sharetribe_sharetribe/features/support/email.rb, step:Given ]
[name:FactoryGirl, file:null, step:Given ]
[name:Listing, file:sharetribe_sharetribe/app/models/listing.rb, step:Given ]
[name:Person, file:sharetribe_sharetribe/app/models/person.rb, step:Given ]

Methods: 51
[name:click, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:Then ]
[name:click_link, type:Object, file:null, step:When ]
[name:consent, type:Object, file:null, step:Given ]
[name:create, type:Email, file:sharetribe_sharetribe/app/models/email.rb, step:Given ]
[name:create, type:Email, file:sharetribe_sharetribe/features/support/email.rb, step:Given ]
[name:create, type:CommunityMembership, file:sharetribe_sharetribe/app/models/community_membership.rb, step:Given ]
[name:downcase, type:Object, file:null, step:Given ]
[name:each, type:Object, file:null, step:Given ]
[name:empty?, type:Object, file:null, step:Given ]
[name:except, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:Then ]
[name:find, type:Object, file:null, step:When ]
[name:find, type:Object, file:null, step:Given ]
[name:find_by_domain, type:Community, file:sharetribe_sharetribe/app/models/community.rb, step:Given ]
[name:find_by_username, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:Given ]
[name:find_or_create_category, type:Object, file:null, step:Given ]
[name:find_or_create_share_type, type:Object, file:null, step:Given ]
[name:first, type:Community, file:sharetribe_sharetribe/app/models/community.rb, step:Given ]
[name:first, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:Given ]
[name:force_override_model_id, type:UserSteps, file:sharetribe_sharetribe/features/step_definitions/user_steps.rb, step:Given ]
[name:get_test_person_and_session, type:Object, file:null, step:Given ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:has_content?, type:Object, file:null, step:When ]
[name:has_no_content?, type:Object, file:null, step:Then ]
[name:hashes, type:Object, file:null, step:Given ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:When ]
[name:have_no_content, type:Object, file:null, step:Then ]
[name:homepage, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:Given ]
[name:index, type:DashboardController, file:sharetribe_sharetribe/app/controllers/dashboard_controller.rb, step:Given ]
[name:index, type:HomepageController, file:sharetribe_sharetribe/app/controllers/homepage_controller.rb, step:Then ]
[name:login_as, type:Object, file:null, step:Given ]
[name:name, type:Object, file:null, step:Given ]
[name:new, type:SessionsController, file:sharetribe_sharetribe/app/controllers/sessions_controller.rb, step:Given ]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:When ]
[name:respond_to?, type:Object, file:null, step:Then ]
[name:respond_to?, type:Object, file:null, step:When ]
[name:save, type:Object, file:null, step:Given ]
[name:save!, type:Object, file:null, step:Given ]
[name:set_default_preferences, type:Object, file:null, step:Given ]
[name:update_all, type:Object, file:null, step:Given ]
[name:update_attributes, type:Object, file:null, step:Given ]
[name:update_attributes, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:Given ]
[name:with_scope, type:WebSteps, file:sharetribe_sharetribe/features/step_definitions/web_steps.rb, step:Then ]
[name:with_scope, type:WebSteps, file:sharetribe_sharetribe/features/step_definitions/web_steps.rb, step:When ]
[name:within, type:Object, file:null, step:Then ]

Referenced pages: 10
sharetribe_sharetribe/app/views/dashboard/_contact_request_form.haml
sharetribe_sharetribe/app/views/dashboard/index.haml
sharetribe_sharetribe/app/views/homepage/_custom_filters.haml
sharetribe_sharetribe/app/views/homepage/_grid_item.haml
sharetribe_sharetribe/app/views/homepage/_list_item.haml
sharetribe_sharetribe/app/views/homepage/_map.haml
sharetribe_sharetribe/app/views/homepage/_search_bar.haml
sharetribe_sharetribe/app/views/homepage/index.haml
sharetribe_sharetribe/app/views/layouts/_grid_item_listing_image.haml
sharetribe_sharetribe/app/views/sessions/new.haml

