Feature: Admin removes custom field from category
Background:
Given there is a dropdown field "House type" for category "housing" in community "test" with options:
| title |
| condo |
| house |
@javascript
Scenario: Admin removes custom fields from category
When I remove custom field "House type"
And I remove custom field "Additional details"
Then I should see that I do not have any custom fields
