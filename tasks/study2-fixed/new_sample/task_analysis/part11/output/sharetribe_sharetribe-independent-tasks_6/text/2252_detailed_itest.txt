Classes: 10
[name:Community, file:sharetribe_sharetribe/app/models/community.rb, step:Given ]
[name:Community, file:sharetribe_sharetribe/app/models/community.rb, step:When ]
[name:CommunityMembership, file:sharetribe_sharetribe/app/models/community_membership.rb, step:Given ]
[name:ContactRequest, file:sharetribe_sharetribe/app/models/contact_request.rb, step:Given ]
[name:DateTime, file:null, step:Given ]
[name:Email, file:sharetribe_sharetribe/app/models/email.rb, step:Given ]
[name:Email, file:sharetribe_sharetribe/features/support/email.rb, step:Given ]
[name:FactoryGirl, file:null, step:Given ]
[name:Listing, file:sharetribe_sharetribe/app/models/listing.rb, step:Given ]
[name:Person, file:sharetribe_sharetribe/app/models/person.rb, step:Given ]

Methods: 47
[name:Integer, type:Object, file:null, step:When ]
[name:choose, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:consent, type:Object, file:null, step:Given ]
[name:create, type:Email, file:sharetribe_sharetribe/app/models/email.rb, step:Given ]
[name:create, type:Email, file:sharetribe_sharetribe/features/support/email.rb, step:Given ]
[name:create, type:CommunityMembership, file:sharetribe_sharetribe/app/models/community_membership.rb, step:Given ]
[name:downcase, type:Object, file:null, step:Given ]
[name:each, type:Object, file:null, step:Given ]
[name:empty?, type:Object, file:null, step:Given ]
[name:except, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find, type:Object, file:null, step:Given ]
[name:find_by_domain, type:Community, file:sharetribe_sharetribe/app/models/community.rb, step:Given ]
[name:find_by_domain, type:Community, file:sharetribe_sharetribe/app/models/community.rb, step:When ]
[name:find_by_username, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:Given ]
[name:find_or_create_category, type:Object, file:null, step:Given ]
[name:find_or_create_share_type, type:Object, file:null, step:Given ]
[name:first, type:Community, file:sharetribe_sharetribe/app/models/community.rb, step:Given ]
[name:first, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:Given ]
[name:force_override_model_id, type:UserSteps, file:sharetribe_sharetribe/features/step_definitions/user_steps.rb, step:Given ]
[name:get_test_person_and_session, type:Object, file:null, step:Given ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:has_no_content?, type:Object, file:null, step:Then ]
[name:hashes, type:Object, file:null, step:Given ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_no_content, type:Object, file:null, step:Then ]
[name:homepage, type:Object, file:null, step:When ]
[name:id, type:Object, file:null, step:Given ]
[name:index, type:DashboardController, file:sharetribe_sharetribe/app/controllers/dashboard_controller.rb, step:Given ]
[name:index, type:HomepageController, file:sharetribe_sharetribe/app/controllers/homepage_controller.rb, step:When ]
[name:login_as, type:Object, file:null, step:Given ]
[name:name, type:Object, file:null, step:Given ]
[name:page, type:Object, file:null, step:Then ]
[name:respond_to?, type:Object, file:null, step:Then ]
[name:save, type:Object, file:null, step:Given ]
[name:save!, type:Object, file:null, step:Given ]
[name:select, type:Object, file:null, step:When ]
[name:set_default_preferences, type:Object, file:null, step:Given ]
[name:sleep, type:Object, file:null, step:When ]
[name:update_all, type:Object, file:null, step:Given ]
[name:update_attributes, type:Object, file:null, step:Given ]
[name:update_attributes, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:When ]
[name:with_scope, type:WebSteps, file:sharetribe_sharetribe/features/step_definitions/web_steps.rb, step:When ]
[name:with_scope, type:WebSteps, file:sharetribe_sharetribe/features/step_definitions/web_steps.rb, step:Then ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 9
sharetribe_sharetribe/app/views/dashboard/_contact_request_form.haml
sharetribe_sharetribe/app/views/dashboard/index.haml
sharetribe_sharetribe/app/views/homepage/_custom_filters.haml
sharetribe_sharetribe/app/views/homepage/_grid_item.haml
sharetribe_sharetribe/app/views/homepage/_list_item.haml
sharetribe_sharetribe/app/views/homepage/_map.haml
sharetribe_sharetribe/app/views/homepage/_search_bar.haml
sharetribe_sharetribe/app/views/homepage/index.haml
sharetribe_sharetribe/app/views/layouts/_grid_item_listing_image.haml

