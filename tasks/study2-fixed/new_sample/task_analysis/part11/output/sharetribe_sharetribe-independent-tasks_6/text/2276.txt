Feature: User views homepage
In order to see the latest activity in Sharetribe
As a user
I want see latest offers, requests and transactions on the home page
@javascript
Scenario: Latest offers on the homepage
Given there are following users:
| person |
| kassi_testperson1 |
And there is item offer with title "car spare parts" from "kassi_testperson1" and with share type "sell"
And there is item offer with title "bike" from "kassi_testperson1" and with share type "sell"
And that listing is closed
And there is item request with title "saw" from "kassi_testperson2" and with share type "buy"
And privacy of that listing is "private"
When I am on the homepage
Then I should see "car spare parts"
And I should not see "bike"
And I should not see "saw"
And I should not see "Sign up"
When I log in as "kassi_testperson1"
Then I should see "saw"
And I should see "car spare parts"
And I should not see "bike"
@javascript
Scenario: Unlogged user views private community
Given there are following users:
| person |
| kassi_testperson1 |
And there is item offer with title "car spare parts" from "kassi_testperson1" and with share type "sell"
And community "test" is private
When I am on the home page
Then I should not see "car spare parts"
And I should see "Sign up"
Feature: User creates a new listing
In order to perform a certain task using an item, a skill, or a transport, or to help others
As a person who does not have the required item, skill, or transport, or has them and wants offer them to others
I want to be able to offer and request an item, a favor, a transport or housing
@phantomjs_skip
@javascript
Scenario: User creates a new listing in private community
Given I am logged in
And community "test" is private
And I am on the home page
When I follow "new-listing-link"
And I follow "I need something"
And I follow "An item"
And I should see "What kind of an item are we talking about?"
And I follow "Tools" within "#option-groups"
And I should see "How do you want to get it?"
And I follow "buy it"
Then I should not see "Privacy*"
And I fill in "listing_title" with "Sledgehammer"
And I fill in "listing_description" with "My description"
And I press "Save listing"
Then I should see "Sledgehammer" within "#listing-title"
When I go to the home page
Then I should see "Sledgehammer"
When I log out
And I go to the home page
Then I should not see "Sledgehammer"
Feature: User views profile page
In order to find information about a user
As a user
I want to
@javascript
Scenario: Unlogged user tries to view profile page in a private community
Given there are following users:
| person |
| kassi_testperson1 |
And community "test" is private
When I go to the profile page of "kassi_testperson1"
Then I should see "Sign up"
