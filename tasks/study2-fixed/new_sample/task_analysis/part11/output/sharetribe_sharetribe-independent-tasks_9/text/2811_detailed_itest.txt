Classes: 1
[name:Person, file:sharetribe_sharetribe/app/models/person.rb, step:Given ]

Methods: 29
[name:Factory, type:Object, file:null, step:Given ]
[name:Integer, type:Object, file:null, step:When ]
[name:allowed_emails, type:Object, file:null, step:Given ]
[name:api, type:DashboardController, file:sharetribe_sharetribe/app/controllers/dashboard_controller.rb, step:When ]
[name:check, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_link, type:Object, file:null, step:When ]
[name:faq, type:DashboardController, file:sharetribe_sharetribe/app/controllers/dashboard_controller.rb, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:find_by_username, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:Given ]
[name:generate_random_username, type:Object, file:null, step:When ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:has_no_content?, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_no_content, type:Object, file:null, step:Then ]
[name:index, type:DashboardController, file:sharetribe_sharetribe/app/controllers/dashboard_controller.rb, step:When ]
[name:login_as, type:Object, file:null, step:Given ]
[name:new, type:SessionsController, file:sharetribe_sharetribe/app/controllers/sessions_controller.rb, step:When ]
[name:page, type:Object, file:null, step:Then ]
[name:pricing, type:DashboardController, file:sharetribe_sharetribe/app/controllers/dashboard_controller.rb, step:When ]
[name:respond_to?, type:Object, file:null, step:Then ]
[name:root, type:ApplicationController, file:sharetribe_sharetribe/app/controllers/application_controller.rb, step:When ]
[name:sleep, type:Object, file:null, step:When ]
[name:use_asi?, type:ApplicationHelper, file:sharetribe_sharetribe/app/helpers/application_helper.rb, step:Given ]
[name:with_scope, type:WebSteps, file:sharetribe_sharetribe/features/step_definitions/web_steps.rb, step:When ]
[name:with_scope, type:WebSteps, file:sharetribe_sharetribe/features/step_definitions/web_steps.rb, step:Then ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 10
sharetribe_sharetribe/app/views/communities/_select_pricing_plan.haml
sharetribe_sharetribe/app/views/dashboard/_footer.haml
sharetribe_sharetribe/app/views/dashboard/_header.haml
sharetribe_sharetribe/app/views/dashboard/_lower_middle.haml
sharetribe_sharetribe/app/views/dashboard/api.haml
sharetribe_sharetribe/app/views/dashboard/faq.haml
sharetribe_sharetribe/app/views/dashboard/index.haml
sharetribe_sharetribe/app/views/dashboard/pricing.haml
sharetribe_sharetribe/app/views/sessions/_new.haml
sharetribe_sharetribe/app/views/sessions/new.haml

