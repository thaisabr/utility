Feature: User views dashboard
In order to read stuff about Sharetribe
As a user
I want to be able to view the dashboard
@no_subdomain
@no_subdomain @javascript
Scenario: User views dashboard
Given I am on the home page
Then I should see "Create your own community marketplace"
@no_subdomain
Feature: User creates a new community
In order to enable members of my community to share their assets with each other
As a community admin
I want to be able to create a new community
@no_subdomain
@no_subdomain @javascript
Scenario: Existing logged in user creates a new free non-profit community
Given I am logged in as "kassi_testperson1"
And I am on the home page
When I follow "GET STARTED NOW!"
And I follow "Association"
And I go to new tribe in English
And I fill in "community_name" with "Test tribe"
And I fill in "community_domain" with "testtribe"
And I fill in "community_address" with "Otaniemi"
And I check "community_terms"
And wait for 2 seconds
And I press "Create your tribe"
Then I should see "Done!"
When I follow "Go to your tribe"
Then I should see "Lend, help, share"
And community "testtribe" should not require invite to join
@no_subdomain
@no_subdomain @javascript
Scenario: Existing user creates a new free for-profit community
Given I am logged in as "kassi_testperson1"
And I am on the home page
When I follow "GET STARTED NOW!"
And I follow "Company"
And I fill in "email" with "test@mycompany.com"
And I press "Continue"
Then I should see "Please confirm your email address"
@www_subdomain
@www_subdomain @javascript
Scenario: New user signs up and creates a new non-profit community
Given I am on the home page
When I follow "GET STARTED NOW!"
And I follow "Association"
And I fill in "Your email address" with "test@example.com"
And I fill in "Pick a username" with random username
And I fill in "Your given name" with "Testmanno"
And I fill in "Your family name" with "Namez"
And I fill in "Pick a password" with "test"
And I fill in "Confirm your password" with "test"
And I check "person_terms"
And I press "Create account"
Then I should see "Please confirm your email address"
When I follow "Home"
And I follow "GET STARTED NOW!"
And I follow "Association"
Then I should see "Please confirm your email address"
@no_subdomain
@no_subdomain @javascript
Scenario: Existing user tries to create a for-profit community and sign up with an email that is already in use in another organization
Given I am logged in as "kassi_testperson1"
And there is an existing community with "@mycompany.com" in allowed emails and with slogan "Hey hey my my"
And I am on the home page
When I follow "GET STARTED NOW!"
And I follow "Company"
And I fill in "email" with "test@mycompany.com"
And I press "Continue"
Then I should see "There already exists a tribe for this company."
When I follow "here"
Then I should see "Hey hey my my"
@no_subdomain
@no_subdomain @javascript
Scenario: New user tries to create a for-profit community and sign up with an email that is already use in another organization
Given there is an existing community with "@mycompany.com" in allowed emails and with slogan "Hey hey my my"
And I am on the home page
When I follow "GET STARTED NOW!"
And I follow "Company"
And I fill in "Your company email address" with "test@mycompany.com"
And I press "Create account"
Then I should see "There already exists a tribe for this company"
When I follow "here"
Then I should see "Hey hey my my"
@no_subdomain
@no_subdomain @javascript
Scenario: Existing logged in user tries to create a new community with insufficient information
Given I am logged in as "kassi_testperson1"
And I am on the home page
When I follow "GET STARTED NOW!"
And I follow "Association"
And I go to new tribe in English
And I fill in "community_name" with "S"
And I fill in "community_domain" with "test"
And I fill in "community_address" with "dsfdsfdsfdsfdsfdssd"
And wait for 2 seconds
And I press "Create your tribe"
Then I should not see "Done!"
And I should see "This field is required"
@no_subdomain
@no_subdomain @javascript
Scenario: Existing logged in user creates an invite-only community
Given I am logged in as "kassi_testperson1"
And I am on the home page
When I follow "GET STARTED NOW!"
And I follow "Association"
And I go to new tribe in English
And I fill in "community_name" with "Test tribe"
And I fill in "community_domain" with "testtribe"
And I fill in "community_address" with "Otaniemi"
And I check "community_terms"
And I check "community_join_with_invite_only"
And wait for 2 seconds
And I press "Create your tribe"
Then I should see "Done!"
And community "testtribe" should require invite to join
