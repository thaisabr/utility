Feature: User joins invite only community
In order to get critical mass to Sharetribe
As a user in a community
I want to invite my friend in this Sharetribe community
@javascript
Scenario: User invites another user successfully
Given there are following users:
| person |
| kassi_testperson2 |
And I am on the homepage
And I should not see "Invite friends"
When I log in as "kassi_testperson2"
And I am on invitation page of "kassi_testperson2"
Then I should not see "Invite your friends"
And I should see "Post a new listing"
When users can invite new users to join community "test"
And I am on invitation page of "kassi_testperson2"
Then I should see "Email address(es)"
And I fill in "invitation_email" with "test@example.com"
And I fill in "invitation_message" with "test"
And I press "Send invitation"
Then I should see "Invitation sent successfully"
When I fill in "invitation_email" with "test@example.com"
And I press "Send invitation"
Then I should see "Invitation sent successfully"
When I fill in "invitation_email" with "test@example.com, another.test@example.com,third.strange.guy@example.com"
And I fill in "invitation_message" with "test"
And I press "Send invitation"
Then I should see "Invitation sent successfully"
