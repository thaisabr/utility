Given /^I am logged in(?: as "([^"]*)")?$/ do |person|
  username = person || "kassi_testperson1"
  person = Person.find_by_username(username)
  login_user_without_browser(person.username)
end
Given /^I log in(?: as "([^"]*)")?$/ do |person|
  logout_and_login_user(person)
end
Given /^I log in to this private community(?: as "([^"]*)")?$/ do |person|
  visit login_path(:locale => :en)
  fill_in("person[login]", :with => (person ? person : "kassi_testperson1"))
  fill_in("person[password]", :with => "testi")
  click_button("Log in")
end
Given /^I am not logged in$/ do
  # TODO Check here that not logged in
end
Given /^there are following users:$/ do |person_table|
  @people = {}
  person_table.hashes.each do |hash|
    defaults = {
      password: "testi",
      given_name: "Test",
      family_name: "Person"
    }

    username = hash['person']
    id = hash['id']
    membership_created_at = hash['membership_created_at']

    person_opts = defaults.merge({
      username: hash['person'],
    }).merge(hash.except('person', 'membership_created_at'))

    @hash_person, @hash_session = Person.find_by_username(username) || FactoryGirl.create(:person, person_opts)
    @hash_person.save!

    @hash_person = force_override_model_id(id, @hash_person, Person, [Email]) if id

    if hash['email'] then
      @hash_person.emails = [Email.create(:address => hash['email'], :send_notifications => true, :person => @hash_person, :confirmed_at => DateTime.now)]
      @hash_person.save!
    end

    @hash_person.update_attributes({:preferences => { "email_about_new_comments_to_own_listing" => "true", "email_about_new_messages" => "true" }})
    cm = CommunityMembership.find_by_person_id_and_community_id(@hash_person.id, Community.first.id) ||
         CommunityMembership.create(:community_id => Community.first.id,
                                    :person_id => @hash_person.id,
                                    :consent => Community.first.consent,
                                    :status => "accepted")
    cm.update_attribute(:created_at, membership_created_at) if membership_created_at && !membership_created_at.empty?

    attributes_to_update = hash.except('person','person_id', 'locale', 'membership_created_at')
    @hash_person.update_attributes(attributes_to_update) unless attributes_to_update.empty?
    @hash_person.set_default_preferences
    if hash['locale']
      @hash_person.locale = hash['locale']
      @hash_person.save
    end
    @people[username] = @hash_person
  end
end
Given /^"([^"]*)" has admin rights in community "([^"]*)"$/ do |username, community|
  user = Person.find_by_username(username)
  community = Community.where(ident: community).first
  CommunityMembership.find_by_person_id_and_community_id(user.id, community.id).update_attribute(:admin, true)
end
  def logout_and_login_user(username = "kassi_testperson1", password = "testi")
    logout() if @current_user
    login_user(username, password)
  end
  def login_user_without_browser(username)
    person = Person.find_by_username(username)
    login_as(person, :scope => :person)
    visit root_path(:locale => :en)
    @logged_in_user = person
    @current_user = person
  end
  def force_override_model_id(id, model_instance, model_class, associated_model_classes=[])
    old_id = model_instance.id
    model_class.update_all({:id => id}, {:id => old_id})

    # Associates
    foreign_key = "#{model_class.name.downcase}_id".to_sym
    associated_model_classes.each do |associated_model_class|
      associated_model_class.update_all({foreign_key => id}, {foreign_key => old_id})
    end

    # Reload
    model_class.find(id)
  end
  def login_user(username = "kassi_testperson1", password = "testi")
    visit login_path(:locale => :en)
    fill_in("main_person_login", :with => username)
    fill_in("main_person_password", :with => password)
    click_button(:main_log_in_button)

    # Warning! This sets @current_user even if the login fails.
    # This should be fixed.
    @current_user = Person.find_by_username(username)
  end
  def logout()
    steps %Q{
      When I open user menu
    }
    click_link "Log out"

    @current_user = nil
  end
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    click_button(button)
  end
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
When /^(?:|I )fill in "([^"]*)" with "([^"]*)"(?: within "([^"]*)")?$/ do |field, value, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_content(text)
    else
      assert page.has_content?(text)
    end
  end
end
Then /^(?:|I )should not see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_no_content(text)
    else
      assert page.has_no_content?(text)
    end
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)
    case page_name

    when /the home\s?page/
      '/'
    when /the signup page/
      '/en/signup'
    when /the private community sign in page/
      '/en/homepage/sign_in'
    when /the english private community sign in page/
      '/en/homepage/sign_in'
    when /the requests page/
      '/en/requests'
    when /the offers page/
      '/en/offers'
    when /the login page/
      login_path(:locale => "en")
    when /the new listing page/
      new_listing_path(:locale => "en")
    when /the edit listing page/
      edit_listing_path(:id => @listing.id, :locale => "en")
    when /^the give feedback path of "(.*)"$/i
      new_person_message_feedback_path(:person_id => @people[$1].id, :message_id => @transaction.id.to_s, :locale => "en")
    when /^the conversation path of "(.*)"$/i
      person_message_path(:person_id => @people[$1].id, :id => @conversation.id.to_s, :locale => "en")
    when /^the conversation page of "(.*)"$/
      single_conversation_path(:person_id => @logged_in_user.id, :conversation_type => "received", :id => $1,  :locale => "en")
    when /^the transaction page of "(.*)"$/
      person_transaction_path(:person_id => @logged_in_user.id, :conversation_type => "received", :id => $1,  :locale => "en")
    when /^the messages page$/i
      person_inbox_path(:person_id => @logged_in_user.id, :locale => "en")
    when /^the profile page of "(.*)"$/i
      person_path(@people[$1], :locale => "en")
    when /^my profile page$/i
      person_path( @logged_in_user, :locale => "en")
    when /^the testimonials page of "(.*)"$/i
      person_testimonials_path(:person_id => @people[$1].id, :locale => "en")
    when /the listing page/
      listing_path(:id => @listing.id, :locale => "en")
    when /^the registration page with invitation code "(.*)"$/i
      "/en/signup?code=#{$1}"
    when /^the admin view of community "(.*)"$/i
      edit_details_admin_community_path(:id => Community.where(ident: $1).first.id, :locale => "en")
    when /the infos page/
      about_infos_path(:locale => "en")
    when /the terms page/
      terms_infos_path(:locale => "en")
    when /the privacy policy page/
      privacy_infos_path(:locale => "en")
    when /new tribe in English/
      new_tribe_path(:community_locale => "en", :locale => "en")
    when /invitations page/
      new_invitation_path(:locale => "en")
    when /the settings page/
      "#{person_path(@logged_in_user, :locale => "en")}/settings"
    when /the profile settings page/
      "#{person_path(@logged_in_user, :locale => "en")}/settings"
    when /the new Checkout account page/
      "#{person_path(@logged_in_user, :locale => "en")}/checkout_account/new"
    when /the new Braintree account page/
      "#{person_path(@logged_in_user, :locale => "en")}/settings/payments/braintree/new"
    when /the account settings page/
      "#{person_path(@logged_in_user, :locale => "en")}/settings/account"
    when /the about page$/
      about_infos_path(:locale => "en")
    when /the feedback page$/
      new_user_feedback_path(:locale => "en")
    when /the custom fields admin page/
      admin_custom_fields_path(:locale => "en")
    when /the categories admin page/
      admin_categories_path(:locale => "en")
    when /the manage members admin page/
      admin_community_community_memberships_path(:community_id => @current_community.id)
    when /the edit look-and-feel page/
      edit_look_and_feel_admin_community_path(:id => @current_community.id)
    when /the text instructions admin page/
      edit_text_instructions_admin_community_path(:id => @current_community.id)
    when /the social media admin page/
      social_media_admin_community_path(:id => @current_community.id)
    when /the analytics admin page/
      analytics_admin_community_path(:id => @current_community.id)
    when /the menu links admin page/
      menu_links_admin_community_path(:id => @current_community.id)
    when /the transactions admin page/
      admin_community_transactions_path(:community_id => @current_community.id)
    when /the getting started page for admins/
      getting_started_admin_community_path(:id => @current_community.id)

    # the following are examples using path_to_pickle

    when /^#{capture_model}(?:'s)? page$/                           # eg. the forum's page
      path_to_pickle $1

    when /^#{capture_model}(?:'s)? #{capture_model}(?:'s)? page$/   # eg. the forum's post's page
      path_to_pickle $1, $2

    when /^#{capture_model}(?:'s)? #{capture_model}'s (.+?) page$/  # eg. the forum's post's comments page
      path_to_pickle $1, $2, :extra => $3                           #  or the forum's post's edit page

    when /^#{capture_model}(?:'s)? (.+?) page$/                     # eg. the forum's posts page
      path_to_pickle $1, :extra => $2                               #  or the forum's edit page

    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
When /^(?:|I )click "([^"]*)"(?: within "([^"]*)")?$/ do |css_selector, scope_selector|
  with_scope(scope_selector) do
    find(css_selector).click
  end
end
When(/^I click the community logo$/) do
  find("#header-logo").click
end
When(/^I log out$/) do
  logout()
end
Then(/^I should not be logged in$/) do
  if page.respond_to? :should
    page.should have_css("#header-login-link")
  else
    assert page.has_css?("#header-login-link")
  end
end
  def logout()
    steps %Q{
      When I open user menu
    }
    click_link "Log out"

    @current_user = nil
  end
Given /^community "(.*?)" allows only organizations$/ do |community|
  c = Community.where(ident: community).first
  c.only_organizations = true
  c.save!
end
Given /^"(.*?)" is not an organization$/ do |username|
  user = Person.find_by_username(username)
  user.is_organization = false
  user.save!
end
When /^I click save on the editor$/ do
  find("em", :text => "Save").click
end
When /^(?:|I )(?:change|set) the contents? of "(.*?)" to "(.*?)"$/ do |region_id, content|
  page.driver.within_frame('mercury_iframe') do
    find("##{region_id}", :visible => false)
    page.driver.execute_script <<-JAVASCRIPT
      jQuery(document).find('##{region_id}').html('#{content}');
    JAVASCRIPT
  end
end
Given /^the terms of community "([^"]*)" are changed to "([^"]*)"$/ do |community, terms|
  Community.where(ident: community).first.update_attribute(:consent, terms)
end
Given /^the community has payments in use(?: via (\w+))?(?: with seller commission (\w+))?$/ do |gateway_name, commission|
  use_payment_gateway(@current_community.ident, gateway_name, commission)
end
Given /^this community is private$/ do
  @current_community.private = true
  @current_community.save!
end
  def use_payment_gateway(community_ident, gateway_name, commission)
    gateway_name ||= "Checkout"
    commission ||= "8"

    community = Community.where(ident: community_ident).first
    community.update_attributes(:vat => "24", :commission_from_seller => commission.to_i)

    if gateway_name == "Checkout"
      FactoryGirl.create(:checkout_payment_gateway, :community => community, :type => gateway_name)
    else
      FactoryGirl.create(:braintree_payment_gateway, :community => community, :type => gateway_name)
    end

    listings_api = ListingService::API::Api
    shapes = listings_api.shapes.get(community_id: community.id)[:data]

    shapes.select { |s| s[:price_enabled] }.each { |s|
      TransactionProcess.find(s[:transaction_process_id]).update_attribute(:process, :postpay)
    }
  end
Given /^there is a pending request "([^"]*)" from "([^"]*)" about that listing$/ do |message, sender|
  @transaction = create_transaction(@current_community, @listing, @people[sender], message, @current_community.payment_gateway.gateway_type)
  @conversation = @transaction.conversation
  MarketplaceService::Transaction::Command.transition_to(@transaction.id, "pending")
  @transaction.reload
end
When /^I try to go to inbox of "([^"]*)"$/ do |person|
  visit person_inbox_path(:locale => :en, :person_id => @people[person].id)
end
Given /^the (offer|request) is (accepted|rejected|confirmed|canceled|paid)$/ do |listing_type, status|
  if listing_type == "request" && @transaction.listing.payment_required_at?(@transaction.community)
    if status == "accepted" || status == "paid"
      # In this case there should be a pending payment done when this got accepted.
      type = if @transaction.community.payment_gateway.type == "BraintreePaymentGateway"
        :braintree_payment
      else
        :checkout_payment
      end

      recipient = @transaction.listing.author

      if @transaction.payment == nil
        payment = FactoryGirl.build(type, :transaction => @transaction, :recipient => recipient, :status => "pending", :community => @current_community)
        payment.default_sum(@transaction.listing, 24)
        payment.save!

        @transaction.payment = payment
      end
    end
  end

  # TODO Change status step by step
  if @transaction.status == "pending" && status == "confirmed"
    MarketplaceService::Transaction::Command.transition_to(@transaction.id, :accepted)
    @transaction.payment.update_attribute(:status, "paid") if @transaction.payment
    MarketplaceService::Transaction::Command.transition_to(@transaction.id, :paid) if @transaction.payment
    MarketplaceService::Transaction::Command.transition_to(@transaction.id, :confirmed)
  elsif @transaction.status == "pending" && status == "paid"
    MarketplaceService::Transaction::Command.transition_to(@transaction.id, :accepted)
    @transaction.payment.update_attribute(:status, "paid") if @transaction.payment
    MarketplaceService::Transaction::Command.transition_to(@transaction.id, :paid) if @transaction.payment
  elsif @transaction.status == "not_started" && status == "accepted"
    MarketplaceService::Transaction::Command.transition_to(@transaction.id, :pending)
    MarketplaceService::Transaction::Command.transition_to(@transaction.id, :accepted)
  else
    MarketplaceService::Transaction::Command.transition_to(@transaction.id, status.to_sym)
  end

  @transaction.reload
end
def create_transaction(community, listing, starter, message, payment_gateway = :none)
  transaction = FactoryGirl.create(:transaction,
    listing: listing,
    community: community,
    starter: starter,
    conversation: build_conversation(community, listing, starter, message),
    payment_gateway: payment_gateway,
    payment_process: TransactionProcess.find(listing.transaction_process_id).process == :preauthorize ? :preauthorize : :postpay,
    automatic_confirmation_after_days: community.automatic_confirmation_after_days
  )
end
def build_conversation(community, listing, starter, message)
  conversation = FactoryGirl.build(:conversation,
    community: community,
    listing: listing )

  conversation.participations.build({
    person_id: starter.id,
    is_starter: true,
    is_read: true
  })

  conversation.participations.build({
    person_id: listing.author.id,
    is_starter: false,
    is_read: false
  })

  conversation.messages.build({
    content: message,
    sender: starter
  })

  conversation
end
Given(/^"(.*?)" has an? (active) Braintree account$/) do |username, status|
  person = Person.find_by_username(username)
  @account = create_braintree_account(person, @current_community)
end
Given /^there is a payment for that request from "(.*?)" with price "(.*?)"$/ do |payer_username, price|
  listing = @transaction.listing
  payer = Person.find_by_username(payer_username)
  @payment = FactoryGirl.create(:braintree_payment, payer: payer, recipient: listing.author, community: @current_community, sum_cents: price.to_i * 100, transaction: @transaction)
end
Given /^Braintree escrow release is mocked$/ do
  BraintreeService::EscrowReleaseHelper.should_receive(:release_from_escrow).at_least(1).times.and_return(true)
end
  def create_braintree_account(person, community, opts={})
    account = FactoryGirl.create(:braintree_account, :person => person, :community => community)
    account.update_attributes(opts) unless opts.empty?
    account
  end
Given /^there is a listing with title "([^"]*)"(?: from "([^"]*)")?(?: with category "([^"]*)")?(?: and with listing shape "([^"]*)")?$/ do |title, author, category_name, shape_name|
  opts = Hash.new
  opts[:title] = title
  opts[:category] = find_category_by_name(category_name) if category_name
  opts[:author] = Person.find_by_username(author) if author

  shape =
    if shape_name
      find_shape(name: shape_name)
    else
      all_shapes.first
    end

  create_listing(shape: shape, opts: opts)
end
Given /^the price of that listing is (\d+)\.(\d+) (EUR|USD)(?: per (.*?))?$/ do |price, price_decimal, currency, price_per|
  unit_type = if ["piece", "hour", "day", "night", "week", "month"].include?(price_per)
    price_per.to_sym
  else
    nil
  end

  @listing.update_attribute(:price, Money.new(price.to_i * 100 + price_decimal.to_i, currency))
  @listing.update_attribute(:unit_type, unit_type) unless unit_type.nil?
end
Given /^listing comments are in use in community "(.*?)"$/ do |community_ident|
  community = Community.where(ident: community_ident).first
  community.update_attribute(:listing_comments_in_use, true)
end
  def find_category_by_name(category_name)
    Category.all.inject("") do |memo, c|
      memo = c if c.display_name("en").eql?(category_name) && c.community_id == @current_community.id
      memo
    end
  end
  def all_shapes(community: nil)
    community ||= @current_community
    ListingService::API::Api.shapes.get(community_id: community.id)[:data]
  end
When(/^I open user menu$/) do
  find("#header-user-desktop-anchor").click
end
