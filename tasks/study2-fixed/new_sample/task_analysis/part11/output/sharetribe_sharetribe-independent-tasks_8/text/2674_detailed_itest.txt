Classes: 10
[name:Community, file:sharetribe_sharetribe/app/models/community.rb, step:Given ]
[name:Conversation, file:sharetribe_sharetribe/app/models/conversation.rb, step:Given ]
[name:FactoryGirl, file:null, step:Given ]
[name:File, file:null, step:When ]
[name:I18n, file:sharetribe_sharetribe/config/initializers/i18n.rb, step:Given ]
[name:Listing, file:sharetribe_sharetribe/app/models/listing.rb, step:Given ]
[name:Participation, file:sharetribe_sharetribe/app/models/participation.rb, step:Given ]
[name:Person, file:sharetribe_sharetribe/app/models/person.rb, step:Given ]
[name:Rails, file:null, step:When ]
[name:Testimonial, file:sharetribe_sharetribe/app/models/testimonial.rb, step:Given ]

Methods: 49
[name:api, type:DashboardController, file:sharetribe_sharetribe/app/controllers/dashboard_controller.rb, step:null]
[name:attach_file, type:Object, file:null, step:When ]
[name:attach_image, type:GeneralSteps, file:sharetribe_sharetribe/features/step_definitions/general_steps.rb, step:When ]
[name:chop, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:create!, type:Testimonial, file:sharetribe_sharetribe/app/models/testimonial.rb, step:Given ]
[name:create!, type:Conversation, file:sharetribe_sharetribe/app/models/conversation.rb, step:Given ]
[name:create_listing, type:Object, file:null, step:Given ]
[name:create_listing, type:Object, file:null, step:When ]
[name:faq, type:DashboardController, file:sharetribe_sharetribe/app/controllers/dashboard_controller.rb, step:null]
[name:fill_in, type:Object, file:null, step:When ]
[name:find_by_domain, type:Community, file:sharetribe_sharetribe/app/models/community.rb, step:Given ]
[name:find_by_person_id_and_conversation_id, type:Participation, file:sharetribe_sharetribe/app/models/participation.rb, step:Given ]
[name:find_by_username, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:Given ]
[name:find_or_create_category, type:Object, file:null, step:Given ]
[name:find_or_create_share_type, type:Object, file:null, step:Given ]
[name:first, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:Given ]
[name:has_content?, type:Object, file:null, step:When ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:has_no_content?, type:Object, file:null, step:When ]
[name:have_content, type:Object, file:null, step:When ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_no_content, type:Object, file:null, step:When ]
[name:have_xpath, type:Object, file:null, step:Then ]
[name:homepage, type:Object, file:null, step:When ]
[name:id, type:Object, file:null, step:Given ]
[name:index, type:DashboardController, file:sharetribe_sharetribe/app/controllers/dashboard_controller.rb, step:null]
[name:index, type:HomepageController, file:sharetribe_sharetribe/app/controllers/homepage_controller.rb, step:When ]
[name:listing, type:Object, file:null, step:When ]
[name:listing_type, type:Listing, file:sharetribe_sharetribe/app/models/listing.rb, step:Given ]
[name:login_as, type:Object, file:null, step:Given ]
[name:name, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:Given ]
[name:new, type:SessionsController, file:sharetribe_sharetribe/app/controllers/sessions_controller.rb, step:When ]
[name:page, type:Object, file:null, step:When ]
[name:page, type:Object, file:null, step:Then ]
[name:participants, type:Object, file:null, step:Given ]
[name:respond_to?, type:Object, file:null, step:When ]
[name:respond_to?, type:Object, file:null, step:Then ]
[name:root, type:ApplicationController, file:sharetribe_sharetribe/app/controllers/application_controller.rb, step:null]
[name:split, type:Object, file:null, step:Given ]
[name:t, type:I18n, file:sharetribe_sharetribe/config/initializers/i18n.rb, step:Given ]
[name:times, type:Object, file:null, step:Given ]
[name:times, type:Object, file:null, step:When ]
[name:to_i, type:Uuidtools, file:sharetribe_sharetribe/lib/np_guid/uuidtools.rb, step:Given ]
[name:to_i, type:Uuidtools, file:sharetribe_sharetribe/lib/np_guid/uuidtools.rb, step:When ]
[name:with_scope, type:WebSteps, file:sharetribe_sharetribe/features/step_definitions/web_steps.rb, step:When ]
[name:with_scope, type:WebSteps, file:sharetribe_sharetribe/features/step_definitions/web_steps.rb, step:Then ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 13
sharetribe_sharetribe/app/views/dashboard/_footer.haml
sharetribe_sharetribe/app/views/dashboard/_header.haml
sharetribe_sharetribe/app/views/dashboard/_lower_middle.haml
sharetribe_sharetribe/app/views/dashboard/api.haml
sharetribe_sharetribe/app/views/dashboard/faq.haml
sharetribe_sharetribe/app/views/dashboard/index.haml
sharetribe_sharetribe/app/views/homepage/_filter_categories.haml
sharetribe_sharetribe/app/views/homepage/_filter_share_types.haml
sharetribe_sharetribe/app/views/homepage/_map.haml
sharetribe_sharetribe/app/views/homepage/_recent_listing.haml
sharetribe_sharetribe/app/views/homepage/index.haml
sharetribe_sharetribe/app/views/listings/_share_type_link.haml
sharetribe_sharetribe/app/views/sessions/new.haml

