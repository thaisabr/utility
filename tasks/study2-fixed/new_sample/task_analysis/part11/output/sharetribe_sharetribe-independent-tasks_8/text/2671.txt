Feature: User browses listings
In order to find out what kind of offers and requests there are available in Sharetribe
As a person who needs something or has something
I want to be able to browse offers and requests
@javascript
Scenario: User browses offers page
Given there are following users:
| person |
| kassi_testperson1 |
| kassi_testperson2 |
And there is item offer with title "car spare parts" from "kassi_testperson2" and with share type "sell"
And there is favor offer with title "massage" from "kassi_testperson1"
And there is rideshare offer from "Helsinki" to "Turku" by "kassi_testperson1"
And there is housing offer with title "Apartment" from "kassi_testperson2" and with share type "sell"
And there is item offer with title "saw" from "kassi_testperson2" and with share type "lend"
And there is item offer with title "axe" from "kassi_testperson2" and with share type "lend"
And that listing is closed
And there is item request with title "toolbox" from "kassi_testperson2" and with share type "buy"
And I am on the home page
And I select "Offer" from "share_type"
Then I should see "car spare parts"
And I should see "massage"
And I should see "Helsinki - Turku"
And I should see "Apartment"
And I should see "saw"
And I should not see "axe"
And I should not see "toolbox"
And I select "Items" from "listing_category"
And I should see "car spare parts"
And I should not see "massage"
And I should not see "Helsinki - Turku"
And I should not see "Apartment"
And I should see "saw"
And I should not see "axe"
And I should not see "toolbox"
And I select "- Lending" from "share_type"
And I should not see "car spare parts"
And I should not see "massage"
And I should not see "Helsinki - Turku"
And I should not see "Apartment"
And I should see "saw"
And I should not see "axe"
And I should not see "toolbox"
And I select "- Selling" from "share_type"
And I should see "car spare parts"
And I should not see "massage"
And I should not see "Helsinki - Turku"
And I should not see "Apartment"
And I should not see "saw"
And I should not see "axe"
And I should not see "toolbox"
And I select "Services" from "listing_category"
And I should not see "car spare parts"
And I should not see "massage"
And I should not see "Helsinki - Turku"
And I should not see "Apartment"
And I should not see "saw"
And I should not see "axe"
And I should not see "toolbox"
And I select "All listing types" from "share_type"
And I should not see "car spare parts"
And I should see "massage"
And I should not see "Helsinki - Turku"
And I should not see "Apartment"
And I should not see "saw"
And I should not see "axe"
And I should not see "toolbox"
@javascript
Scenario: User browses requests page
Given there are following users:
| person |
| kassi_testperson1 |
| kassi_testperson2 |
And there is item request with title "car spare parts" from "kassi_testperson2" and with share type "buy"
And there is favor request with title "massage" from "kassi_testperson1"
And there is rideshare request from "Helsinki" to "Turku" by "kassi_testperson1"
And there is housing request with title "Apartment" from "kassi_testperson2" and with share type "buy"
And there is item request with title "saw" from "kassi_testperson2" and with share type "borrow"
And there is item request with title "axe" from "kassi_testperson2" and with share type "borrow"
And that listing is closed
And there is item offer with title "toolbox" from "kassi_testperson2" and with share type "sell"
And I am on the home page
And I select "Request" from "share_type"
Then I should see "car spare parts"
And I should see "massage"
And I should see "Helsinki - Turku"
And I should see "Apartment"
And I should see "saw"
And I should not see "axe"
And I should not see "toolbox"
And I select "Items" from "listing_category"
And I should see "car spare parts"
And I should not see "massage"
And I should not see "Helsinki - Turku"
And I should not see "Apartment"
And I should see "saw"
And I should not see "axe"
And I should not see "toolbox"
And I select "- Borrowing" from "share_type"
And I should not see "car spare parts"
And I should not see "massage"
And I should not see "Helsinki - Turku"
And I should not see "Apartment"
And I should see "saw"
And I should not see "axe"
And I should not see "toolbox"
And I select "- Buying" from "share_type"
And I should see "car spare parts"
And I should not see "massage"
And I should not see "Helsinki - Turku"
And I should not see "Apartment"
And I should not see "saw"
And I should not see "axe"
And I should not see "toolbox"
And I select "Services" from "listing_category"
And I should not see "car spare parts"
And I should not see "massage"
And I should not see "Helsinki - Turku"
And I should not see "Apartment"
And I should not see "saw"
And I should not see "axe"
And I should not see "toolbox"
And I select "All listing types" from "share_type"
And I should not see "car spare parts"
And I should see "massage"
And I should not see "Helsinki - Turku"
And I should not see "Apartment"
And I should not see "saw"
And I should not see "axe"
And I should not see "toolbox"
@javascript
Scenario: User browses requests with visibility settings
Given there are following users:
| person |
| kassi_testperson1 |
And there is item request with title "car spare parts" from "kassi_testperson2" and with share type "buy"
And privacy of that listing is "private"
And there is favor request with title "massage" from "kassi_testperson1"
And there is housing request with title "apartment" and with share type "rent"
And visibility of that listing is "this_community"
And privacy of that listing is "private"
And that listing is closed
And I am on the home page
And I select "Request" from "share_type"
And I should not see "car spare parts"
And I should see "massage"
And I should not see "apartment"
When I log in as "kassi_testperson1"
And I select "Request" from "share_type"
Then I should see "car spare parts"
And I should see "massage"
And I should not see "apartment"
@pending
@javascript
Feature: User creates a new listing
In order to perform a certain task using an item, a skill, or a transport, or to help others
As a person who does not have the required item, skill, or transport, or has them and wants offer them to others
I want to be able to offer and request an item, a favor, a transport or housing
@javascript
Scenario: Trying to create a new item request with insufficient information
Given I am logged in
And I am on the home page
When I follow "Post a new listing!"
And I follow "I need something"
And I follow "An item"
And I follow "Sports"
And I follow "borrow it"
And I attach an image with invalid extension to "listing_listing_images_attributes_0_image"
And I select "31" from "listing_valid_until_3i"
And I select "December" from "listing_valid_until_2i"
And I select "2014" from "listing_valid_until_1i"
And I press "Save request"
Then I should see "This field is required."
And I should see "This date must be between current time and one year from now."
And I should see "The image file must be either in GIF, JPG or PNG format."
Feature: User browses offered rides
In order to get from place A to B a cheap and environmentally friendly way
As a carless Sharetribe-user
I want to check if someone is offering ridesharing in Sharetribe for the same route that I'm going to take
@javascript
Scenario: Browsing all ridesharing offers
Given there are following users:
| person |
| kassi_testperson1 |
| kassi_testperson2 |
And there is rideshare offer from "tkk" to "kamppi" by "kassi_testperson1"
And there is rideshare offer from "Sydney" to "Melbourne" by "kassi_testperson2"
And there is rideshare request from "Oulu" to "Helsinki" by "kassi_testperson2"
And there is item offer with title "axe" from "kassi_testperson1" and with share type "lend"
And I am on the home page
When I select "Rideshare" from "listing_category"
And I select "Offer" from "share_type"
Then I should see "tkk - kamppi"
And I should see "Sydney - Melbourne"
But I should not see "axe"
And I should not see "Oulu"
And I should not see "Helsinki"
