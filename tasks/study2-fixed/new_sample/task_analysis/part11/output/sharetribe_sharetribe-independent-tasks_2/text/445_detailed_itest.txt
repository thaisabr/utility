Classes: 14
[name:Community, file:sharetribe_sharetribe/app/models/community.rb, step:Given ]
[name:Community, file:sharetribe_sharetribe/app/services/marketplace_service/community.rb, step:Given ]
[name:CommunityMembership, file:sharetribe_sharetribe/app/models/community_membership.rb, step:Given ]
[name:DateTime, file:null, step:Given ]
[name:Email, file:sharetribe_sharetribe/app/models/email.rb, step:Given ]
[name:Email, file:sharetribe_sharetribe/features/support/email.rb, step:Given ]
[name:FactoryGirl, file:null, step:Given ]
[name:Person, file:sharetribe_sharetribe/app/models/person.rb, step:Given ]
[name:Person, file:sharetribe_sharetribe/app/services/marketplace_service/person.rb, step:Given ]
[name:Person, file:sharetribe_sharetribe/app/models/person.rb, step:Then ]
[name:Person, file:sharetribe_sharetribe/app/services/marketplace_service/person.rb, step:Then ]
[name:Test, file:sharetribe_sharetribe/config/environments/test.rb, step:Given ]
[name:Warden, file:sharetribe_sharetribe/config/initializers/warden.rb, step:Given ]
[name:Warden, file:sharetribe_sharetribe/features/support/warden.rb, step:Given ]

Methods: 68
[name:automatic_newsletters, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:consent, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:Given ]
[name:create, type:Email, file:sharetribe_sharetribe/app/models/email.rb, step:Given ]
[name:create, type:Email, file:sharetribe_sharetribe/features/support/email.rb, step:Given ]
[name:create, type:CommunityMembership, file:sharetribe_sharetribe/app/models/community_membership.rb, step:Given ]
[name:downcase, type:Object, file:null, step:Given ]
[name:each, type:Object, file:null, step:Given ]
[name:email_admins_about_new_members, type:Object, file:null, step:When ]
[name:empty?, type:Object, file:null, step:Given ]
[name:except, type:Object, file:null, step:Given ]
[name:execute_script, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find, type:Object, file:null, step:Given ]
[name:find_by, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:Given ]
[name:find_by, type:Person, file:sharetribe_sharetribe/app/services/marketplace_service/person.rb, step:Given ]
[name:find_by, type:Community, file:sharetribe_sharetribe/app/models/community.rb, step:Given ]
[name:find_by, type:Community, file:sharetribe_sharetribe/app/services/marketplace_service/community.rb, step:Given ]
[name:find_by_person_id_and_community_id, type:CommunityMembership, file:sharetribe_sharetribe/app/models/community_membership.rb, step:Given ]
[name:find_field, type:Object, file:null, step:Then ]
[name:first, type:Community, file:sharetribe_sharetribe/app/models/community.rb, step:Given ]
[name:first, type:Community, file:sharetribe_sharetribe/app/services/marketplace_service/community.rb, step:Given ]
[name:force_override_model_id, type:UserSteps, file:sharetribe_sharetribe/features/step_definitions/user_steps.rb, step:Given ]
[name:hashes, type:Object, file:null, step:Given ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_css, type:Object, file:null, step:When ]
[name:have_no_content, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:Given ]
[name:join_with_invite_only, type:Object, file:null, step:When ]
[name:last, type:Object, file:null, step:Then ]
[name:listing_comments_in_use, type:Object, file:null, step:When ]
[name:login_user_without_browser, type:LoginHelpers, file:sharetribe_sharetribe/features/support/login_helpers.rb, step:Given ]
[name:match, type:Object, file:null, step:Then ]
[name:merge, type:FormUtils, file:sharetribe_sharetribe/app/utils/form_utils.rb, step:Given ]
[name:name, type:Object, file:null, step:Given ]
[name:order, type:Person, file:sharetribe_sharetribe/app/models/person.rb, step:Then ]
[name:order, type:Person, file:sharetribe_sharetribe/app/services/marketplace_service/person.rb, step:Then ]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:When ]
[name:private, type:Object, file:null, step:When ]
[name:require_verification_to_post_listings, type:Object, file:null, step:When ]
[name:save, type:Object, file:null, step:Given ]
[name:save!, type:Object, file:null, step:Given ]
[name:search_path, type:ApplicationHelper, file:sharetribe_sharetribe/app/helpers/application_helper.rb, step:When ]
[name:search_path, type:PathHelpers, file:sharetribe_sharetribe/app/view_utils/path_helpers.rb, step:When ]
[name:search_url, type:ApplicationHelper, file:sharetribe_sharetribe/app/helpers/application_helper.rb, step:When ]
[name:search_url, type:PathHelpers, file:sharetribe_sharetribe/app/view_utils/path_helpers.rb, step:When ]
[name:set_default_preferences, type:Object, file:null, step:Given ]
[name:show_category_in_listing_list, type:Object, file:null, step:When ]
[name:show_listing_publishing_date, type:Object, file:null, step:When ]
[name:tag_name, type:Object, file:null, step:Then ]
[name:target_user, type:Object, file:null, step:When ]
[name:text, type:Object, file:null, step:Then ]
[name:to, type:Object, file:null, step:Then ]
[name:to, type:Object, file:null, step:When ]
[name:update_all, type:Object, file:null, step:Given ]
[name:update_all, type:Listings, file:sharetribe_sharetribe/app/services/listing_service/api/listings.rb, step:Given ]
[name:update_all, type:Listing, file:sharetribe_sharetribe/app/services/listing_service/store/listing.rb, step:Given ]
[name:update_attribute, type:Object, file:null, step:Given ]
[name:update_attributes, type:Object, file:null, step:Given ]
[name:username, type:Object, file:null, step:Given ]
[name:username, type:Object, file:null, step:Then ]
[name:users_can_invite_new_users, type:InvitationsController, file:sharetribe_sharetribe/app/controllers/invitations_controller.rb, step:When ]
[name:value, type:Object, file:null, step:Then ]
[name:where, type:Object, file:null, step:Given ]
[name:with_scope, type:WebSteps, file:sharetribe_sharetribe/features/step_definitions/web_steps.rb, step:When ]
[name:with_scope, type:WebSteps, file:sharetribe_sharetribe/features/step_definitions/web_steps.rb, step:Then ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 9
sharetribe_sharetribe/app/views/admin/_left_hand_navigation.haml
sharetribe_sharetribe/app/views/admin/communities/settings.haml
sharetribe_sharetribe/app/views/layouts/_left_hand_navigation.haml
sharetribe_sharetribe/app/views/settings/_community_updates_radiobutton.haml
sharetribe_sharetribe/app/views/settings/_notification_checkbox.haml
sharetribe_sharetribe/app/views/settings/account.haml
sharetribe_sharetribe/app/views/settings/notifications.haml
sharetribe_sharetribe/app/views/settings/show.haml
sharetribe_sharetribe/app/views/settings/unsubscribe.haml

