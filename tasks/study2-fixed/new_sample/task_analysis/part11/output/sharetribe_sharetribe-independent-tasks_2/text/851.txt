Feature: Admin edits community look-and-feel
In order to give diversify my marketplace from my competitors
As an admin
I want to be able to modify look-and-feel
Background:
Given there are following users:
| person            | given_name | family_name | email               | membership_created_at     |
| manager           | matti      | manager     | manager@example.com | 2014-03-01 00:12:35 +0200 |
| kassi_testperson1 | john       | doe         | test2@example.com   | 2013-03-01 00:12:35 +0200 |
| kassi_testperson2 | jane       | doe         | test1@example.com   | 2012-03-01 00:00:00 +0200 |
Scenario: Admin changes main color
Then I should see that the background color of Post a new listing button is "00A26C"
And I set the main color to "FF0099"
And I press submit
Then I should see that the background color of Post a new listing button is "FF0099"
