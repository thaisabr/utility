When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    click_button(button)
  end
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
When /^(?:|I )fill in "([^"]*)" for "([^"]*)"(?: within "([^"]*)")?$/ do |value, field, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
When /^(?:|I )select "([^"]*)" from "([^"]*)"(?: within "([^"]*)")?$/ do |value, field, selector|
  with_scope(selector) do
    select(value, :from => field)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    expect(page).to have_content(text)
  end
end
Given /^I will(?:| (not)) confirm all following confirmation dialogs in this page if I am running PhantomJS$/ do |do_not_confirm|
  confirm = do_not_confirm != "not"
  if ENV['PHANTOMJS'] then
    page.execute_script("window.__original_confirm = window.confirm; window.confirm = function() { return #{confirm}; };")
  end
end
When /^I confirm alert popup$/ do
  unless ENV['PHANTOMJS']
    # wait is necessary for firefox if alerts have slide-out animations
    wait = Selenium::WebDriver::Wait.new ignore: Selenium::WebDriver::Error::NoAlertPresentError
    alert = wait.until { page.driver.browser.switch_to.alert }
    alert.accept
    begin
      page.driver.browser.switch_to.alert
    rescue Selenium::WebDriver::Error::NoSuchAlertError
    end
  end
end
  def with_scope(locator)
    if locator
      expect(page).to have_css(locator)
      within(locator) { yield }
    else
      yield
    end
  end
  def path_to(page_name)
    case page_name

    when /the home\s?page/
      '/'
    when /the signup page/
      '/en/signup'
    when /the private community sign in page/
      '/en/homepage/sign_in'
    when /the english private community sign in page/
      '/en/homepage/sign_in'
    when /the requests page/
      '/en/requests'
    when /the offers page/
      '/en/offers'
    when /the login page/
      login_path(:locale => "en")
    when /the new listing page/
      new_listing_path(:locale => "en")
    when /the edit listing page/
      edit_listing_path(:id => @listing.id, :locale => "en")
    when /^the give feedback path of "(.*)"$/i
      new_person_message_feedback_path(:person_id => @people[$1].id, :message_id => @transaction.id.to_s, :locale => "en")
    when /^the conversation path of "(.*)"$/i
      person_message_path(:person_id => @people[$1].id, :id => @conversation.id.to_s, :locale => "en")
    when /^the conversation page of "(.*)"$/
      single_conversation_path(:person_id => @logged_in_user.id, :conversation_type => "received", :id => $1,  :locale => "en")
    when /^the transaction page of "(.*)"$/
      person_transaction_path(:person_id => @logged_in_user.id, :conversation_type => "received", :id => $1,  :locale => "en")
    when /^the messages page$/i
      person_inbox_path(:person_id => @logged_in_user.id, :locale => "en")
    when /^the profile page of "(.*)"$/i
      person_path(@people[$1], :locale => "en")
    when /^my profile page$/i
      person_path( @logged_in_user, :locale => "en")
    when /the listing page/
      listing_path(:id => @listing.id, :locale => "en")
    when /^the registration page with invitation code "(.*)"$/i
      "/en/signup?code=#{$1}"
    when /^the admin view of community "(.*)"$/i
      admin_details_edit_path(locale: "en")
    when /the infos page/
      about_infos_path(:locale => "en")
    when /the terms page/
      terms_infos_path(:locale => "en")
    when /the privacy policy page/
      privacy_infos_path(:locale => "en")
    when /new tribe in English/
      new_tribe_path(:community_locale => "en", :locale => "en")
    when /invitations page/
      new_invitation_path(:locale => "en")
    when /the settings page/
      "#{person_path(@logged_in_user, :locale => "en")}/settings"
    when /the profile settings page/
      "#{person_path(@logged_in_user, :locale => "en")}/settings"
    when /the account settings page/
      "#{person_path(@logged_in_user, :locale => "en")}/settings/account"
    when /the about page$/
      about_infos_path(:locale => "en")
    when /the feedback page$/
      new_user_feedback_path(:locale => "en")
    when /the custom fields admin page/
      admin_custom_fields_path(:locale => "en")
    when /the categories admin page/
      admin_categories_path(:locale => "en")
    when /the manage members admin page/
      admin_community_community_memberships_path(:community_id => @current_community.id)
    when /the edit look-and-feel page/
      admin_look_and_feel_edit_path
    when /the text instructions admin page/
      edit_text_instructions_admin_community_path(:id => @current_community.id)
    when /the social media admin page/
      social_media_admin_community_path(:id => @current_community.id)
    when /the analytics admin page/
      analytics_admin_community_path(:id => @current_community.id)
    when /the top bar admin page/
      admin_topbar_edit_path
    when /the transactions admin page/
      admin_community_transactions_path(:community_id => @current_community.id)
    when /the getting started guide for admins/
      admin_getting_started_guide_path
    when /^the admin view of payment preferences of community "(.*)"$/i
      admin_payment_preferences_path(locale: "en")
    when /the order types admin page/
      admin_listing_shapes_path
    when /the edit "(.*)" order type admin page/
      edit_admin_listing_shape_path(id: $1)
    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Given /^I am logged in(?: as "([^"]*)")?$/ do |person|
  username = person || "kassi_testperson1"
  person = Person.find_by(username: username)
  login_user_without_browser(person.username)
end
Given /^"([^"]*)" has admin rights in community "([^"]*)"$/ do |username, community|
  user = Person.find_by(username: username)
  community = Community.where(ident: community).first
  CommunityMembership.find_by_person_id_and_community_id(user.id, community.id).update_attribute(:admin, true)
end
  def login_user_without_browser(username)
    person = Person.find_by(username: username)
    Warden::Test::Helpers.login_as(person, :scope => :person)
    visit homepage_with_locale_path(:locale => :en)
    @logged_in_user = person
    @current_user = person
  end
Given(/^community "(.*?)" has order type "(.*?)"$/) do |community, order_type|
  community = Community.where(ident: community).first
  FactoryGirl.create(:listing_shape, community: community,
                                     transaction_process: FactoryGirl.create(:transaction_process),
                                     name: order_type)
end
