Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
Given /^I will(?:| (not)) confirm all following confirmation dialogs in this page if I am running PhantomJS$/ do |do_not_confirm|
  confirm = do_not_confirm != "not"
  if ENV['PHANTOMJS'] then
    page.execute_script("window.__original_confirm = window.confirm; window.confirm = function() { return #{confirm}; };")
  end
end
  def path_to(page_name)
    case page_name

    when /the home\s?page/
      '/'
    when /the signup page/
      '/en/signup'
    when /the private community sign in page/
      '/en/homepage/sign_in'
    when /the english private community sign in page/
      '/en/homepage/sign_in'
    when /the requests page/
      '/en/requests'
    when /the offers page/
      '/en/offers'
    when /the login page/
      login_path(:locale => "en")
    when /the new listing page/
      new_listing_path(:locale => "en")
    when /the edit listing page/
      edit_listing_path(:id => @listing.id, :locale => "en")
    when /^the give feedback path of "(.*)"$/i
      new_person_message_feedback_path(:person_id => @people[$1].id, :message_id => @transaction.id.to_s, :locale => "en")
    when /^the conversation path of "(.*)"$/i
      person_message_path(:person_id => @people[$1].id, :id => @conversation.id.to_s, :locale => "en")
    when /^the conversation page of "(.*)"$/
      single_conversation_path(:person_id => @logged_in_user.id, :conversation_type => "received", :id => $1,  :locale => "en")
    when /^the transaction page of "(.*)"$/
      person_transaction_path(:person_id => @logged_in_user.id, :conversation_type => "received", :id => $1,  :locale => "en")
    when /^the messages page$/i
      person_inbox_path(:person_id => @logged_in_user.id, :locale => "en")
    when /^the profile page of "(.*)"$/i
      person_path(@people[$1], :locale => "en")
    when /^my profile page$/i
      person_path( @logged_in_user, :locale => "en")
    when /the listing page/
      listing_path(:id => @listing.id, :locale => "en")
    when /^the registration page with invitation code "(.*)"$/i
      "/en/signup?code=#{$1}"
    when /^the admin view of community "(.*)"$/i
      edit_details_admin_community_path(:id => Community.where(ident: $1).first.id, :locale => "en")
    when /the infos page/
      about_infos_path(:locale => "en")
    when /the terms page/
      terms_infos_path(:locale => "en")
    when /the privacy policy page/
      privacy_infos_path(:locale => "en")
    when /new tribe in English/
      new_tribe_path(:community_locale => "en", :locale => "en")
    when /invitations page/
      new_invitation_path(:locale => "en")
    when /the settings page/
      "#{person_path(@logged_in_user, :locale => "en")}/settings"
    when /the profile settings page/
      "#{person_path(@logged_in_user, :locale => "en")}/settings"
    when /the new Checkout account page/
      "#{person_path(@logged_in_user, :locale => "en")}/checkout_account/new"
    when /the new Braintree account page/
      "#{person_path(@logged_in_user, :locale => "en")}/settings/payments/braintree/new"
    when /the account settings page/
      "#{person_path(@logged_in_user, :locale => "en")}/settings/account"
    when /the about page$/
      about_infos_path(:locale => "en")
    when /the feedback page$/
      new_user_feedback_path(:locale => "en")
    when /the custom fields admin page/
      admin_custom_fields_path(:locale => "en")
    when /the categories admin page/
      admin_categories_path(:locale => "en")
    when /the manage members admin page/
      admin_community_community_memberships_path(:community_id => @current_community.id)
    when /the edit look-and-feel page/
      edit_look_and_feel_admin_community_path(:id => @current_community.id)
    when /the text instructions admin page/
      edit_text_instructions_admin_community_path(:id => @current_community.id)
    when /the social media admin page/
      social_media_admin_community_path(:id => @current_community.id)
    when /the analytics admin page/
      analytics_admin_community_path(:id => @current_community.id)
    when /the menu links admin page/
      menu_links_admin_community_path(:id => @current_community.id)
    when /the transactions admin page/
      admin_community_transactions_path(:community_id => @current_community.id)
    when /the getting started page for admins/
      getting_started_admin_community_path(:id => @current_community.id)
    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Then /^I should not be able to remove email "(.*?)"$/ do |email|
  should_not_find_remove_link_for(email)
end
When /^I remove email "(.*?)"$/ do |email|
  find_remove_link_for_email(email).click
  steps %Q{
    And I confirm alert popup
  }
end
Then /^I should not have email "(.*?)"$/ do |email|
  steps %Q{
    Then I should not see "#{email}"
  }
  expect(Email.find_by_address_and_person_id(email, @logged_in_user.id)).to be_nil
end
  def find_remove_link_for_email(email)
    find_row_for_email(email).find(DELETE_LINK_SELECTOR)
  end
  def should_not_find_remove_link_for(email)
    expect(find_row_for_email(email)).to have_no_selector(DELETE_LINK_SELECTOR)
  end
  def find_row_for_email(email)
    email_div = find(".account-settings-email-row-address", :text => "#{email}")
    email_row = email_div.first(:xpath, ".//..")
  end
Given /^there are following communities:$/ do |communities_table|
  communities_table.hashes.each do |hash|
    ident = hash[:community]
    existing_community = Community.where(ident: ident).first
    existing_community.destroy if existing_community
    @hash_community = FactoryGirl.create(:community, :ident => ident, :settings => {"locales" => ["en", "fi"]})

    attributes_to_update = hash.except('community')
    @hash_community.update_attributes(attributes_to_update) unless attributes_to_update.empty?
  end
end
When /^I move to community "([^"]*)"$/ do |community|
  Capybara.default_host = "#{community}.lvh.me"
  Capybara.app_host = "http://#{community}.lvh.me:9887"
  @current_community = Community.where(ident: community).first
end
Given /^I am logged in(?: as "([^"]*)")?$/ do |person|
  username = person || "kassi_testperson1"
  person = Person.find_by(username: username)
  login_user_without_browser(person.username)
end
Given /^there are following users:$/ do |person_table|
  @people = {}
  person_table.hashes.each do |hash|
    community = Community.find_by(ident: hash['community']) || Community.first

    defaults = {
      password: "testi",
      given_name: "Test",
      family_name: "Person"
    }

    username = hash['person']
    id = hash['id']
    membership_created_at = hash['membership_created_at']

    person_opts = defaults.merge({
      username: hash['person'],
    }).merge(hash.except('person', 'membership_created_at', 'community'))

    @hash_person, @hash_session = Person.find_by(username: username) || FactoryGirl.create(:person, person_opts)
    @hash_person.community_id = community.id
    @hash_person.save!

    @hash_person = force_override_model_id(id, @hash_person, Person, [Email]) if id

    if hash['email'] then
      @hash_person.emails = [Email.create(
                              address: hash['email'],
                              send_notifications: true,
                              person: @hash_person,
                              confirmed_at: DateTime.now,
                              community_id: community.id)]
      @hash_person.save!
    end

    @hash_person.update_attributes({:preferences => { "email_about_new_comments_to_own_listing" => "true", "email_about_new_messages" => "true" }})
    cm = CommunityMembership.find_by_person_id_and_community_id(@hash_person.id, community.id) ||
         CommunityMembership.create(:community_id => community.id,
                                    :person_id => @hash_person.id,
                                    :consent => community.consent,
                                    :status => "accepted")
    cm.update_attribute(:created_at, membership_created_at) if membership_created_at && !membership_created_at.empty?

    attributes_to_update = hash.except('person','person_id', 'locale', 'membership_created_at', 'community')
    @hash_person.update_attributes(attributes_to_update) unless attributes_to_update.empty?
    @hash_person.set_default_preferences
    if hash['locale']
      @hash_person.locale = hash['locale']
      @hash_person.save
    end
    @people[username] = @hash_person
  end
end
  def login_user_without_browser(username)
    person = Person.find_by(username: username)
    login_as(person, :scope => :person)
    visit root_path(:locale => :en)
    @logged_in_user = person
    @current_user = person
  end
  def force_override_model_id(id, model_instance, model_class, associated_model_classes=[])
    old_id = model_instance.id
    model_class.where(id: old_id).update_all(id: id)

    # Associates
    foreign_key = "#{model_class.name.downcase}_id".to_sym
    associated_model_classes.each do |associated_model_class|
      associated_model_class.where(foreign_key => old_id).update_all(foreign_key => id)
    end

    # Reload
    model_class.find(id)
  end
Given /^there are following emails:$/ do |emails_table|
  # Clean up old emails first
  emails_table.hashes.each do |hash|
    person = Person.find_by(username: hash[:person])
    person.emails.each { |email| email.destroy }
  end

  # Create new emails
  emails_table.hashes.each do |hash|
    person = Person.find_by(username: hash[:person])
    @hash_email = FactoryGirl.create(:email, :person => person)

    attributes_to_update = hash.except('person')
    @hash_email.update_attributes(attributes_to_update) unless attributes_to_update.empty?
    @hash_email

    # Save
    person.emails << @hash_email
    person.save!
  end
end
Given /^there is an organization "(.*?)" in community "(.*?)"$/ do |org_username, community|
  c = Community.find_by!(ident: community)
  p = FactoryGirl.create(:person, :username => org_username, :is_organization => true, community_id: c.id)
  FactoryGirl.create(:community_membership, person: p, community: c)
end
Then /^(?:|I )should not see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    expect(page).to have_no_content(text)
  end
end
When /^I confirm alert popup$/ do
  page.driver.browser.switch_to.alert.accept unless ENV['PHANTOMJS']
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
