Feature: Merge Child Records
As a Field Worker
I want to Merge duplicate records together
So that I don't waste time working on two identical records
Background:
Given I am logged in as an admin
And the following children exist in the system:
| name   | unique_id  | flag    |flagged_at                   | short_id |
| Bob    | bob_uid    | true    |DateTime.new(2001,2,3,4,5,6) | bob_uid  |
| Steve  | steve_uid  | true    |DateTime.new(2004,2,3,4,5,6) | eve_uid  |
| Dave   | dave_uid   | true    |DateTime.new(2002,2,3,4,5,6) | ave_uid  |
| Fred   | fred_uid   | false   |DateTime.new(2003,2,3,4,5,6) | red_uid  |
@javascript
Scenario: Should see view child page when I click OK on confirmation
When I am on the children listing page
And I select "Flagged" from "filter"
And I click mark as duplicate for "Steve"
And I fill in "parent_id" with "red_uid"
And I press "Mark as Duplicate"
And I accept the modal
Then I am on the child record page for "Steve"
And I should see "This record has been marked as a duplicate and is no longer active. To see the Active record click here."
@javascript
Scenario: Should see error message when wrong Duplicate id/name is given
When I am on the children listing page
And I select "Flagged" from "filter"
And I click mark as duplicate for "Steve"
And I fill in "parent_id" with "fred_uid"
And I press "Mark as Duplicate"
And I accept the modal
And I should see "A valid duplicate ID must be provided"
Feature: So that admin can see Manage Form Sections Page, customize form section details, Create new forms，Disable and enable forms,
delete fields from a form section, delete form sections
Background:
Given I am logged in as an admin
And the following form sections exist in the system on the "Children" form:
| name                  | description                   | unique_id         | order | perm_enabled |visible|editable |
| Basic Identity        | Basic identity about a child  | basic_identity    | 1     | true         |true   |true     |
| Family Details        | Details of the child's family | family_details    | 2     | false        |true   |true     |
| Care Arrangements     |                               | care_arrangements | 3     | false        |true   |true     |
| Other hidden section  |                               | hidden_section    | 4     | false        |false  |true     |
| Other visible section |                               | visible_section   | 5     | false        |true   |true     |
And the following fields exists on "basic_identity":
| name           | type       | display_name | editable |
| name           | text_field | Name         | false    |
| nick_name      | text_field | Nick Name    | true     |
| second_name    | text_field | Second Name  | true     |
| characteristic | text_field | Characteristic  | true     |
| nationality    | text_field | Nationality  | true     |
And the following fields exists on "family_details":
| name          | type       | display_name  |
| another_field | text_field | another field |
@javascript
Scenario: Should not be able to mark date fields as matchable
When I follow "FORMS"
And I follow "Children"
And I follow "Basic Identity"
Then I should not be able to mark 'date_of_birth' as matchable
