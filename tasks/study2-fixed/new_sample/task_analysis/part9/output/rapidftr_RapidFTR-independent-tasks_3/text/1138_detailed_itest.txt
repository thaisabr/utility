Classes: 5
[name:Device, file:rapidftr_RapidFTR/app/models/device.rb, step:Given ]
[name:Session, file:rapidftr_RapidFTR/app/models/session.rb, step:Given ]
[name:Session, file:rapidftr_RapidFTR/app/models/session.rb, step:And ]
[name:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Given ]
[name:User, file:rapidftr_RapidFTR/app/models/user.rb, step:And ]

Methods: 15
[name:cookies, type:Object, file:null, step:Given ]
[name:each, type:Object, file:null, step:Given ]
[name:find_by_user_name, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Given ]
[name:for_user, type:Session, file:rapidftr_RapidFTR/app/models/session.rb, step:Given ]
[name:for_user, type:Session, file:rapidftr_RapidFTR/app/models/session.rb, step:And ]
[name:hashes, type:Object, file:null, step:Given ]
[name:id, type:Searchable, file:rapidftr_RapidFTR/app/models/searchable.rb, step:Given ]
[name:new, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Given ]
[name:new, type:Device, file:rapidftr_RapidFTR/app/models/device.rb, step:Given ]
[name:post, type:Object, file:null, step:When ]
[name:put_in_cookie, type:Session, file:rapidftr_RapidFTR/app/models/session.rb, step:Given ]
[name:raise, type:Object, file:null, step:Given ]
[name:response_body, type:Object, file:null, step:Then ]
[name:save!, type:Object, file:null, step:Given ]
[name:save!, type:Object, file:null, step:And ]

Referenced pages: 0

