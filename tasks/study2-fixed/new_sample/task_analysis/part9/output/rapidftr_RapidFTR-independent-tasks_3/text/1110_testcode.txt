When /^I fill in the basic details of a child$/ do
  fill_in("Birthplace", :with => "Haiti")
  attach_file("photo", "features/resources/jorge.jpg", "image/jpg")
end
When /^the date\/time is "([^\"]*)"$/ do |datetime|
  current_time = Time.parse(datetime)
  Time.stub!(:now).and_return current_time
end
When /^the local date\/time is "([^\"]*)" and UTC time is "([^\"]*)"$/ do |datetime, utcdatetime|
  current_time = Time.parse(datetime)
  current_time_in_utc = Time.parse(utcdatetime)
  Time.stub!(:now).and_return current_time
  current_time.stub!(:getutc).and_return current_time_in_utc
end
Given /^someone has entered a child with the name "([^\"]*)"$/ do |child_name|
  visit path_to('new child page')
  fill_in('Name', :with => child_name)
  fill_in('Birthplace', :with => 'Haiti')
  attach_file("photo", "features/resources/jorge.jpg", "image/jpg")
  click_button('Finish')
end
Given /^the following children exist in the system:$/ do |children_table|
  children_table.hashes.each do |child_hash|
    child_hash.reverse_merge!(
            'birthplace' => 'Cairo',
            'photo_path' => 'features/resources/jorge.jpg',
            'reporter' => 'zubair',
            'age_is' => 'Approximate'
    )

    photo = uploadable_photo(child_hash.delete('photo_path')) if child_hash['photo_path'] != ''
    unique_id = child_hash.delete('unique_id')
    child = Child.new_with_user_name(child_hash['reporter'], child_hash)
    child.photo = photo
    child['unique_identifier'] = unique_id if unique_id
    child.create!
  end
end
Given /^I am editing an existing child record$/ do
  child = Child.new
  child["birthplace"] = "haiti"
  child.photo = uploadable_photo
  raise "Failed to save a valid child record" unless child.save

  visit children_path+"/#{child.id}/edit"
end
Given /^an existing child with name "([^\"]*)" and a photo from "([^\"]*)"$/ do |name, photo_file_path|
  child = Child.new( :name => name, :birthplace => 'unknown' )
  child.photo = uploadable_photo(photo_file_path)
  child.create
end
Given /^I am logged in as an admin$/ do
  Given "there is a admin"
  Given "I am on the login page"
  Given "I fill in \"admin\" for \"user name\""
  Given "I fill in \"123\" for \"password\""
  Given "I press \"Log In\""
end
Given /^I am logged in$/ do
  Given "there is a User"
  Given "I am on the login page"
  Given "I fill in \"#{User.first.user_name}\" for \"user name\""
  Given "I fill in \"123\" for \"password\""
  Given "I press \"Log In\""
end
Given /"([^\"]*)" is logged in/ do |user_name|
  Given "\"#{user_name}\" is the user"
  Given "I am on the login page"
  Given "I fill in \"#{user_name}\" for \"user name\""
  Given "I fill in \"123\" for \"password\""
  Given "I press \"Log In\""  
end
When /^I create a new child$/ do
  child = Child.new
  child["birthplace"] = "haiti"
  child.photo = uploadable_photo
  child.create!
end
Given /^there is a child with the name "([^\"]*)" and a photo from "([^\"]*)"$/ do |child_name, photo_file_path|
  child = Child.new( :name => child_name, :birthplace => 'Chile' )

  child.photo = uploadable_photo(photo_file_path)

  child.create!
end
Then /^the "([^\"]*)" button presents a confirmation message$/ do |button_name|
  Hpricot(response.body).search("//p[@class=#{button_name.downcase}Button]/a").to_html.should include("confirm")
end
  def path_to(page_name, options = {})

    format = page_name[/^(?:|the )(\w+) formatted/,1]
    options.reverse_merge!( :format => format )

    case page_name

      when /the home\s?page/
        '/'
    when /the new create_custom_field page/
      new_create_custom_field_path

    when /the new create_custom_fields.feature page/
      new_create_custom_fields.feature_path

      when /the new add_suggested_field_to_form_section page/
        new_add_suggested_field_to_form_section_path

      when /the new assign_unique_id_to_a_child page/
        new_assign_unique_id_to_a_child_path(options)

      when /add child page/
        new_child_path(options)

      when /new child page/
        new_child_path(options)

      when /children listing page/
        children_path(options)

      when /saved record page for child with name "(.+)"/
        child_name = $1
        child = Summary.by_name(:key => child_name)
        raise "no child named '#{child_name}'" if child.nil?
        child_path( child, options )
        
      when /child record page for "(.+)"/
        child_name = $1
        child = Summary.by_name(:key => child_name)
        raise "no child named '#{child_name}'" if child.nil?
        child_path( child, options )
        
      when /change log page for "(.+)"/
        child_name = $1
        child = Summary.by_name(:key => child_name)
        raise "no child named '#{child_name}'" if child.nil?
        child_history_path( child, options )

      when /new user page/
        new_user_path(options)

      when /manage users page/
        users_path(options)

      when /edit user page for "(.+)"/
        user = User.find_by_user_name($1)
        edit_user_path(user, options)

      when /child search page/
        search_children_path(options)
        
      when /login page/
        login_path(options)
        
      when /logout page/
        logout_path(options)
        
      when /child search results page/
        search_children_path(options)
        
      when /create form section page/
        new_formsection_path(options)
      
      when /edit form section page for "(.+)"$/
        edit_form_section_path(:id => $1)
        
      when /edit field page for "(.+)" on "(.+)" form$/
        edit_formsection_field_path(:formsection_id => $2, :id => $1)
          
      when /form section page/
        formsections_path(options) 

      when /choose field type page/
        arbitrary_form_section = FormSection.new
        new_formsection_field_path( arbitrary_form_section, options )

      when /the edit user page for "(.+)"$/
        user = User.by_user_name(:key => $1)
        raise "no user named #{$1}" if user.nil?
        edit_user_path(user)

      when /new field page for "(.+)"/
        field_type = $1
        new_formsection_field_path(:type => field_type)

      when /the edit form section page for "(.+)"/
        form_section = $1
        formsection_fields_path(form_section)

      when /the admin page/
        admin_path(options)

      when /the edit administrator contact information page/
        edit_contact_information_path(:administrator)
      when /(the )?administrator contact page/
          contact_information_path(:administrator, options)

      when /all child Ids/
        child_ids_path

      # Add more mappings here.
      # Here is an example that pulls values out of the Regexp:
      #
      #   when /^(.*)'s profile page$/i
      #     user_profile_path(User.find_by_login($1))

      else
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
                "Now, go and add a mapping in #{__FILE__}"
    end
  end
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^\"]*)"$/ do |button|
  click_button(button)
end
When /^(?:|I )follow "([^\"]*)"$/ do |link|
  click_link(link)
end
When /^(?:|I )fill in "([^\"]*)" for "([^\"]*)"$/ do |value, field|
  fill_in(field, :with => value)
end
When /^(?:|I )select "([^\"]*)" from "([^\"]*)"$/ do |value, field|
  select(value, :from => field)
end
When /^(?:|I )attach the file "([^\"]*)" to "([^\"]*)"$/ do |path, field|
  type = path.split(".")[1]

  case type
    when "jpg"
      type = "image/jpg"
    when "jpeg"
      type = "image/jpeg"
    when "png"
      type = "image/png"
    when "gif"
      type = "image/gif"
  end

  attach_file(field, path, type)
end
Then /^(?:|I )should see "([^\"]*)"$/ do |text|
  if defined?(Spec::Rails::Matchers)
    response.should contain(text)
  else
    assert_contain text
  end
end
Then /^(?:|I )should not see "([^\"]*)"$/ do |text|
  if defined?(Spec::Rails::Matchers)
    response.should_not contain(text)
  else
    assert_not_contain(text)
  end
end
Then /^I should see the photo of "([^\"]*)"$/ do |child_name|
  check_link(response, child_name) {|child| child_resized_photo_path(child, 328)}
end
When /^I fill in the basic photo details of a child$/ do
  attach_file("photo", "features/resources/jorge.jpg", "image/jpg")
end
def check_link(response, child_name)
  child = find_child_by_name child_name
  image_link = yield(child)
  response.body.should have_selector("img[@src='#{image_link}']")
end
Then /^I should see the "([^\"]*)" section without any ordering links$/ do |section_name|
  row = Hpricot(response.body).search("tr[@id=#{section_name}_row]").first
  row.search("span.moveUp").should be_empty
  row.search("span.moveDown").should be_empty
end
Then /^I should see the "([^\"]*)" section without an enabled checkbox$/ do |section_name|
  row = Hpricot(response.body).search("tr[@id=#{section_name}_row]").first
  row.search("input#sections_#{section_name}").should be_empty
end
Then /^I should see the "([^\"]*)" section with an enabled checkbox$/ do |section_name|
  row = Hpricot(response.body).search("tr[@id=#{section_name}_row]").first
  row.search("input#sections_#{section_name}").should_not be_empty
end
Given /^I create a new form called "([^\"]*)"$/ do |form_name|
  FormSection.create_new_custom form_name
end
Then /^I should see "([^\"]*)" with order of "([^\"]*)"$/ do |form_name, form_order|
  # Convert form name into unique_id, find this row, find order, make sure matches form_order
  row = Hpricot(response.body).at("tr[@id=#{form_name}_row]")
  order = row.at("span[@class='formSectionOrder']").inner_html
  order.should == form_order
end
Then /^I should receive a PDF file$/ do
  Tempfile.open('rapidftr_cuke_tests') do |temp_file|
    temp_file.write( response_body )
    temp_file.close
    mimetype = `file --brief --mime #{temp_file.path}`.gsub(/\n/,"")
    mimetype.should =~ /application\/pdf/
  end
end
Then /^the PDF file should have (\d+) page(?:|s)$/ do |num_pages|
  num_pages = num_pages.to_i

  pdf = PDF::Inspector::Page.analyze( response_body )
  pdf.should have(num_pages).pages
end
Then /^the PDF file should contain the string "([^\"]*)"$/ do |expected_string|
  pdf = PDF::Inspector::Text.analyze( response_body )
  pdf.strings.should include(expected_string)
end
