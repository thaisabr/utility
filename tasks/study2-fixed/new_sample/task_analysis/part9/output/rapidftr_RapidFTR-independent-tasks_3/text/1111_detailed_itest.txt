Classes: 3
[name:Child, file:rapidftr_RapidFTR/app/models/child.rb, step:Then ]
[name:Spec, file:null, step:Then ]
[name:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Given ]

Methods: 15
[name:assert_contain, type:Object, file:null, step:Then ]
[name:attach_file, type:Object, file:null, step:When ]
[name:body, type:Object, file:null, step:Then ]
[name:check_link, type:PhotoThumbnailSteps, file:rapidftr_RapidFTR/features/step_definitions/photo_thumbnail_steps.rb, step:Then ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:contain, type:Object, file:null, step:Then ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find_child_by_name, type:Object, file:null, step:Then ]
[name:first, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Given ]
[name:have_selector, type:Object, file:null, step:Then ]
[name:response, type:Object, file:null, step:Then ]
[name:show_resized_photo, type:ChildMediaController, file:rapidftr_RapidFTR/app/controllers/child_media_controller.rb, step:Then ]
[name:split, type:Object, file:null, step:When ]
[name:user_name, type:Session, file:rapidftr_RapidFTR/app/models/session.rb, step:Given ]

Referenced pages: 0

