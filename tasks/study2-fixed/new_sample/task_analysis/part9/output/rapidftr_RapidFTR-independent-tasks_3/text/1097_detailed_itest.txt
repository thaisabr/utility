Classes: 9
[name:Child, file:rapidftr_RapidFTR/app/models/child.rb, step:Given ]
[name:Child, file:rapidftr_RapidFTR/app/models/child.rb, step:Then ]
[name:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:Then ]
[name:Spec, file:null, step:Then ]
[name:Summary, file:rapidftr_RapidFTR/app/models/summary.rb, step:Then ]
[name:URI, file:null, step:Then ]
[name:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Given ]
[name:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Then ]
[name:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:Then ]

Methods: 56
[name:all, type:ChildIdsController, file:rapidftr_RapidFTR/app/controllers/child_ids_controller.rb, step:Then ]
[name:assert_equal, type:Object, file:null, step:Then ]
[name:by_name, type:Summary, file:rapidftr_RapidFTR/app/models/summary.rb, step:Then ]
[name:by_user_name, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Then ]
[name:check, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:compact, type:Object, file:null, step:Then ]
[name:create!, type:Object, file:null, step:Given ]
[name:current_url, type:Object, file:null, step:Then ]
[name:delete, type:Object, file:null, step:Given ]
[name:destroy, type:SessionsController, file:rapidftr_RapidFTR/app/controllers/sessions_controller.rb, step:Then ]
[name:download_audio, type:ChildMediaController, file:rapidftr_RapidFTR/app/controllers/child_media_controller.rb, step:Then ]
[name:each, type:Object, file:null, step:Given ]
[name:edit, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:Then ]
[name:edit, type:FieldsController, file:rapidftr_RapidFTR/app/controllers/fields_controller.rb, step:Then ]
[name:edit, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:Then ]
[name:edit_photo, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:Then ]
[name:export_photo_to_pdf, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:Then ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find_by_user_name, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Then ]
[name:first, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Given ]
[name:flag, type:Object, file:null, step:Then ]
[name:flag_message, type:Child, file:rapidftr_RapidFTR/app/models/child.rb, step:Then ]
[name:full_name, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Then ]
[name:hashes, type:Object, file:null, step:Given ]
[name:index, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:Then ]
[name:index, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:Then ]
[name:index, type:FormSectionController, file:rapidftr_RapidFTR/app/controllers/form_section_controller.rb, step:Then ]
[name:index, type:FieldsController, file:rapidftr_RapidFTR/app/controllers/fields_controller.rb, step:Then ]
[name:index, type:AdminController, file:rapidftr_RapidFTR/app/controllers/admin_controller.rb, step:Then ]
[name:join, type:Object, file:null, step:Then ]
[name:name, type:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:Then ]
[name:new, type:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:Then ]
[name:new, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:Then ]
[name:new, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:Then ]
[name:new, type:SessionsController, file:rapidftr_RapidFTR/app/controllers/sessions_controller.rb, step:Then ]
[name:new, type:FormSectionController, file:rapidftr_RapidFTR/app/controllers/form_section_controller.rb, step:Then ]
[name:new, type:FieldsController, file:rapidftr_RapidFTR/app/controllers/fields_controller.rb, step:Then ]
[name:new_create_custom_fields, type:Object, file:null, step:Then ]
[name:new_with_user_name, type:Child, file:rapidftr_RapidFTR/app/models/child.rb, step:Given ]
[name:option_strings_text, type:Field, file:rapidftr_RapidFTR/app/models/field.rb, step:Then ]
[name:path_to, type:Paths, file:rapidftr_RapidFTR/features/support/paths.rb, step:Then ]
[name:raise, type:Object, file:null, step:Then ]
[name:reverse_merge!, type:Object, file:null, step:Given ]
[name:reverse_merge!, type:Object, file:null, step:Then ]
[name:save, type:FieldsController, file:rapidftr_RapidFTR/app/controllers/fields_controller.rb, step:Then ]
[name:save, type:FormSectionController, file:rapidftr_RapidFTR/app/controllers/form_section_controller.rb, step:Then ]
[name:search, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:Then ]
[name:select, type:Object, file:null, step:Then ]
[name:show, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:Then ]
[name:show, type:HistoriesController, file:rapidftr_RapidFTR/app/controllers/histories_controller.rb, step:Then ]
[name:show_resized_photo, type:ChildMediaController, file:rapidftr_RapidFTR/app/controllers/child_media_controller.rb, step:Then ]
[name:uploadable_photo, type:Object, file:null, step:Given ]
[name:url_for, type:Object, file:null, step:Then ]
[name:user_name, type:Session, file:rapidftr_RapidFTR/app/models/session.rb, step:Given ]
[name:user_name, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Then ]

Referenced pages: 49
rapidftr_RapidFTR/app/views/admin/index.html.erb
rapidftr_RapidFTR/app/views/children/_audio_player.html.erb
rapidftr_RapidFTR/app/views/children/_audio_upload_box.html.erb
rapidftr_RapidFTR/app/views/children/_check_box.html.erb
rapidftr_RapidFTR/app/views/children/_date_field.html.erb
rapidftr_RapidFTR/app/views/children/_form_section.html.erb
rapidftr_RapidFTR/app/views/children/_form_section_info.html.erb
rapidftr_RapidFTR/app/views/children/_numeric_field.html.erb
rapidftr_RapidFTR/app/views/children/_photo_upload_box.html.erb
rapidftr_RapidFTR/app/views/children/_picture.html.erb
rapidftr_RapidFTR/app/views/children/_radio_button.html.erb
rapidftr_RapidFTR/app/views/children/_repeatable_text_field.html.erb
rapidftr_RapidFTR/app/views/children/_search_results.html.erb
rapidftr_RapidFTR/app/views/children/_select_box.html.erb
rapidftr_RapidFTR/app/views/children/_show_child_toolbar.erb
rapidftr_RapidFTR/app/views/children/_show_form_section.html.erb
rapidftr_RapidFTR/app/views/children/_summary_row.html.erb
rapidftr_RapidFTR/app/views/children/_tabs.html.erb
rapidftr_RapidFTR/app/views/children/_text_field.html.erb
rapidftr_RapidFTR/app/views/children/_textarea.html.erb
rapidftr_RapidFTR/app/views/children/edit.html.erb
rapidftr_RapidFTR/app/views/children/edit_photo.html.erb
rapidftr_RapidFTR/app/views/children/index.html.erb
rapidftr_RapidFTR/app/views/children/new.html.erb
rapidftr_RapidFTR/app/views/children/search.html.erb
rapidftr_RapidFTR/app/views/children/show.html.erb
rapidftr_RapidFTR/app/views/fields/_basic_form.html.erb
rapidftr_RapidFTR/app/views/fields/_common.html.erb
rapidftr_RapidFTR/app/views/fields/_multiple_choice_form.html.erb
rapidftr_RapidFTR/app/views/fields/_suggested_field.html.erb
rapidftr_RapidFTR/app/views/fields/choose.html.erb
rapidftr_RapidFTR/app/views/fields/edit.html.erb
rapidftr_RapidFTR/app/views/fields/new.html.erb
rapidftr_RapidFTR/app/views/form_section/index.html.erb
rapidftr_RapidFTR/app/views/form_section/new.html.erb
rapidftr_RapidFTR/app/views/histories/_audio_history_change.html.erb
rapidftr_RapidFTR/app/views/histories/_flag_change.erb
rapidftr_RapidFTR/app/views/histories/_flag_message_change.erb
rapidftr_RapidFTR/app/views/histories/_history_change.html.erb
rapidftr_RapidFTR/app/views/histories/_photo_history_change.html.erb
rapidftr_RapidFTR/app/views/histories/show.html.erb
rapidftr_RapidFTR/app/views/sessions/new.html.erb
rapidftr_RapidFTR/app/views/sessions/show.html.erb
rapidftr_RapidFTR/app/views/users/_devices.html.erb
rapidftr_RapidFTR/app/views/users/_edittable_user.html.erb
rapidftr_RapidFTR/app/views/users/_mobile_login_history.html.erb
rapidftr_RapidFTR/app/views/users/edit.html.erb
rapidftr_RapidFTR/app/views/users/index.html.erb
rapidftr_RapidFTR/app/views/users/new.html.erb

