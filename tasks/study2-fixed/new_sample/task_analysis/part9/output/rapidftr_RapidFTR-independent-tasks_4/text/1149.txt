Feature:
As a user I should not be able to add more than 200 characters for a text field or 700 for a text area so that I don't crash the blackberry client
Scenario: Should be prevented from saving a record that has no data filled in
Given I am logged in
And I am on children listing page
And I follow "New child"
And I press "Save"
Then I should see "Please fill in at least one field or upload a file"
And there should be 0 child records in the database
