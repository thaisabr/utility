Given /^an? (user|admin) "([^"]+)"$/ do |user_type, user_name|
  Given %(a #{user_type} "#{user_name}" with password "123")
end
Given /^I am logged in as "(.+)"/ do |user_name|
  @session = Session.for_user(User.find_by_user_name(user_name))
  @session.save!
  @session.put_in_cookie cookies
end
Given /^user "(.+)" is disabled$/ do |username|
  user = User.find_by_user_name(username)
  user.disabled = true
  user.save!
end
Then /^user "(.+)" should be disabled$/ do |username|
  User.find_by_user_name(username).should be_disabled
end
Then /^user "(.+)" should not be disabled$/ do |username|
  User.find_by_user_name(username).should_not be_disabled
end
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^\"]*)"$/ do |button|
  click_button(button)
end
When /^(?:|I )follow "([^\"]*)"$/ do |link|
  click_link(link)
end
When /^(?:|I )follow "([^\"]*)" within "([^\"]*)"$/ do |link, parent|
  click_link_within(parent, link)
end
When /^(?:|I )fill in "([^\"]*)" for "([^\"]*)"$/ do |value, field|
  fill_in(field, :with => value)
end
When /^(?:|I )check "([^\"]*)"$/ do |field|
  check(field)
end
When /^(?:|I )uncheck "([^\"]*)"$/ do |field|
  uncheck(field)
end
Then /^(?:|I )should see "([^\"]*)"$/ do |text|
  if defined?(Spec::Rails::Matchers)
    response.should contain(text)
  else
    assert_contain text
  end
end
When /^I fill in a (\d+) character long string for "([^"]*)"$/ do |length, field|
  fill_in(field, :with=>(0...length.to_i).map{ ('a'..'z').to_a[rand(26)] }.join)
end
  def path_to(page_name, options = {})

    format = page_name[/^(?:|the )(\w+) formatted/,1]
    options.reverse_merge!( :format => format )

    case page_name

      when /the home\s?page/
        '/'
    when /the new create_custom_field page/
      new_create_custom_field_path

    when /the new create_custom_fields.feature page/
      new_create_custom_fields.feature_path

      when /the new add_suggested_field_to_form_section page/
        new_add_suggested_field_to_form_section_path

      when /the new assign_unique_id_to_a_child page/
        new_assign_unique_id_to_a_child_path(options)

      when /add child page/
        new_child_path(options)

      when /new child page/
        new_child_path(options)

      when /children listing page/
        children_path(options)

      when /saved record page for child with name "(.+)"/
        child_name = $1
        child = Summary.by_name(:key => child_name)
        raise "no child named '#{child_name}'" if child.nil?
        child_path( child, options )
        
      when /child record page for "(.+)"/
        child_name = $1
        child = Summary.by_name(:key => child_name)
        raise "no child named '#{child_name}'" if child.nil?
        child_path( child, options )
        
      when /change log page for "(.+)"/
        child_name = $1
        child = Summary.by_name(:key => child_name)
        raise "no child named '#{child_name}'" if child.nil?
        child_history_path( child, options )

      when /new user page/
        new_user_path(options)

      when /manage users page/
        users_path(options)

      when /edit user page for "(.+)"/
        user = User.find_by_user_name($1)
        edit_user_path(user, options)

      when /child search page/
        search_children_path(options)
        
      when /login page/
        login_path(options)
        
      when /logout page/
        logout_path(options)
        
      when /child search results page/
        search_children_path(options)
        
      when /create form section page/
        new_formsection_path(options)
      
      when /edit form section page for "(.+)"$/
        edit_form_section_path(:id => $1)
          
      when /form section page/
        formsections_path(options) 

      when /choose field type page/
        arbitrary_form_section = FormSection.new
        new_formsection_field_path( arbitrary_form_section, options )

      when /the edit user page for "(.+)"$/
        user = User.by_user_name(:key => $1)
        raise "no user named #{$1}" if user.nil?
        edit_user_path(user)

      when /new field page for "(.+)"/
        field_type = $1
        send( "new_#{field_type}_formsection_fields_path" )

      when /the edit form section page for "(.+)"/
        form_section = $1
        formsection_fields_path(form_section)

      when /the admin page/
        admin_path(options)

      when /the edit administrator contact information page/
        edit_contact_information_path(:administrator)
      when /the administrator contact page/
          contact_information_path(:administrator)
        
      when /all child Ids/
        child_ids_path

      # Add more mappings here.
      # Here is an example that pulls values out of the Regexp:
      #
      #   when /^(.*)'s profile page$/i
      #     user_profile_path(User.find_by_login($1))

      else
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
                "Now, go and add a mapping in #{__FILE__}"
    end
  end
Then /^the user "([^\"]*)" should be marked as (disabled|enabled)$/ do |username, status|
  response_body.should have_selector("#user-row-#{username} td.user-status") do |content|
  	content.inner_html.downcase.should == status
  end
end
Given /^I am logged in$/ do
  Given "there is a User"
  Given "I am on the login page"
  Given "I fill in \"#{User.first.user_name}\" for \"user name\""
  Given "I fill in \"123\" for \"password\""
  Given "I press \"Log In\""
end
Given /^the following form sections exist in the system:$/ do |form_sections_table|
  FormSection.all.each {|u| u.destroy }
  
  form_sections_table.hashes.each do |form_section_hash|
    form_section_hash.reverse_merge!(
      'unique_id'=> form_section_hash["name"].gsub(/\s/, "_").downcase,
      'enabled' => true,
      'fields'=> Array.new
    )
    
    form_section_hash["order"] = form_section_hash["order"].to_i
    FormSection.create!(form_section_hash)
  end
end
Given /^the following fields exists on "([^"]*)":$/ do |form_section_name, table|
  form_section = FormSection.get_by_unique_id(form_section_name)
  form_section.should_not be_nil
  form_section.fields = []
  table.hashes.each do |field_hash|
    field_hash.reverse_merge!(
      'enabled' => true,
      'type'=> Field::TEXT_FIELD 
    )
    form_section.fields.push Field.new(field_hash)
  end
  form_section.save
end
Then /^there should be (\d+) child records in the database$/ do |number_of_records|
  Child.all.length.should == number_of_records.to_i
end
