Feature:
As an Admin
I want to disable and later re-enable user accounts via the web interface
So that only those who should have access are able to access the system,
Scenario: Admin disables a user from the edit page
Given a user "george"
And an admin "adam"
And I am logged in as "adam"
And I am on edit user page for "george"
When I check "Disabled?"
And I fill in "123" for "Re-enter password"
And I press "Update"
Then user "george" should be disabled
When I am on the manage users page
And the user "george" should be marked as disabled
When I follow "Show" within "#user-row-george"
Then I should see "Disabled"
Scenario: Admin re-enables a user from the edit page
Given a user "george"
And an admin "adam"
And user "george" is disabled
And I am logged in as "adam"
And I am on edit user page for "george"
When I uncheck "Disabled?"
And I fill in "123" for "Re-enter password"
And I press "Update"
Then user "george" should not be disabled
When I go to the manage users page
And the user "george" should be marked as enabled
When I follow "Show" within "#user-row-george"
Then I should see "Enabled"
Feature:
As a user I should not be able to add more than 200 characters for a text field or 700 for a text area so that I don't crash the blackberry client
Scenario: Should be restricted to 200 characters in a text field
Given I am logged in
And I am on children listing page
And I follow "New child"
When I fill in a 201 character long string for "Name"
And I press "Save"
Then I should see "Name cannot be more than 200 characters long"
And there should be 0 child records in the database
Scenario: Should be restricted to 400 characters in a text area
Given the following form sections exist in the system:
| name | unique_id | editable | order |
| Basic details | basic_details | true | 1 |
Given the following fields exists on "basic_details":
| name | type | display_name |
| my_text_area | textarea | my text area |
Given I am logged in
And I am on children listing page
And I follow "New child"
When I fill in a 401 character long string for "my text area"
And I press "Save"
Then I should see "my text area cannot be more than 400 characters long"
And there should be 0 child records in the database
