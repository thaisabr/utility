Classes: 5
[name:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:null]
[name:Spec, file:rapidftr_RapidFTR/vendor/plugins/rspec/lib/spec.rb, step:Then ]
[name:User, file:rapidftr_RapidFTR/app/models/user.rb, step:null]
[name:User, file:rapidftr_RapidFTR/vendor/gems/couchrest-0.34/spec/fixtures/more/user.rb, step:null]
[name:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:null]

Methods: 21
[name:assert_contain, type:Object, file:null, step:Then ]
[name:cancel, type:Object, file:null, step:null]
[name:click_link, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:Given ]
[name:contain, type:Object, file:null, step:Then ]
[name:edit, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:null]
[name:edit, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:null]
[name:full_name, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:null]
[name:full_name, type:User, file:rapidftr_RapidFTR/vendor/gems/couchrest-0.34/spec/fixtures/more/user.rb, step:null]
[name:index, type:AdminController, file:rapidftr_RapidFTR/app/controllers/admin_controller.rb, step:null]
[name:index, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:null]
[name:index, type:FormSectionController, file:rapidftr_RapidFTR/app/controllers/form_section_controller.rb, step:null]
[name:index, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:null]
[name:index, type:FieldsController, file:rapidftr_RapidFTR/app/controllers/fields_controller.rb, step:null]
[name:name, type:FormSection, file:rapidftr_RapidFTR/app/models/form_section.rb, step:null]
[name:new, type:ChildrenController, file:rapidftr_RapidFTR/app/controllers/children_controller.rb, step:null]
[name:new, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:null]
[name:response, type:Object, file:null, step:Then ]
[name:show, type:UsersController, file:rapidftr_RapidFTR/app/controllers/users_controller.rb, step:null]
[name:user_name, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:null]
[name:user_name, type:User, file:rapidftr_RapidFTR/vendor/gems/couchrest-0.34/spec/fixtures/more/user.rb, step:null]

Referenced pages: 14
rapidftr_RapidFTR/app/views/admin/index.html.erb
rapidftr_RapidFTR/app/views/children/_form_section.html.erb
rapidftr_RapidFTR/app/views/children/_tabs.html.erb
rapidftr_RapidFTR/app/views/children/edit.html.erb
rapidftr_RapidFTR/app/views/children/index.html.erb
rapidftr_RapidFTR/app/views/children/new.html.erb
rapidftr_RapidFTR/app/views/fields/_direction_button.html.erb
rapidftr_RapidFTR/app/views/fields/index.html.erb
rapidftr_RapidFTR/app/views/form_section/index.html.erb
rapidftr_RapidFTR/app/views/users/_edittable_user.html.erb
rapidftr_RapidFTR/app/views/users/edit.html.erb
rapidftr_RapidFTR/app/views/users/index.html.erb
rapidftr_RapidFTR/app/views/users/new.html.erb
rapidftr_RapidFTR/app/views/users/show.html.erb

