Classes: 3
[name:Child, file:rapidftr_RapidFTR/app/models/child.rb, step:Given ]
[name:Tempfile, file:null, step:Then ]
[name:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Given ]

Methods: 14
[name:click_link, type:Object, file:null, step:Given ]
[name:close, type:Object, file:null, step:Then ]
[name:create!, type:Object, file:null, step:Given ]
[name:delete, type:Object, file:null, step:Given ]
[name:each, type:Object, file:null, step:Given ]
[name:first, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Given ]
[name:hashes, type:Object, file:null, step:Given ]
[name:new_with_user_name, type:Child, file:rapidftr_RapidFTR/app/models/child.rb, step:Given ]
[name:path, type:Object, file:null, step:Then ]
[name:response_body, type:Object, file:null, step:Then ]
[name:reverse_merge!, type:Object, file:null, step:Given ]
[name:uploadable_photo, type:Object, file:null, step:Given ]
[name:user_name, type:Session, file:rapidftr_RapidFTR/app/models/session.rb, step:Given ]
[name:write, type:Object, file:null, step:Then ]

Referenced pages: 0

