Feature:
Features related to enquiry record, including view enquiry record, create enquiry record
Background:
Given I am logged in as a user with "Create Enquiry,View Enquiries" permissions
And the following forms exist in the system:
| name         |
| Enquiries    |
| Children     |
And the following form sections exist in the system on the "Enquiries" form:
| name              | unique_id         | editable | order | visible | perm_enabled |
| Enquiry Criteria  | enquiry_criteria  | false    | 1     | true    | true         |
And the following fields exists on "enquiry_criteria":
| name           | type       | display_name     | editable |
| enquirer_name  | text_field | Enquirer Name    | false    |
| child_name     | text_field | Child's Name     | false    |
| location       | text_field | Location         | false    |
Scenario: Adding/Viewing Enquiry Record
When I follow "Register New Enquiry"
And I fill in "Enquirer Name" with "Charles"
And I fill in "Child's Name" with "Jorge Just"
And I fill in "Location" with "Kampala"
And I press "Save"
Then I should see "Charles"
And I should see "Jorge Just"
And I should see "Kampala"
Feature: So that a user can get back to their initial start page from anywhere within the application
As a user of the website
I want to click on 'CHILDREN' link and return to initial start page
Scenario: The homepage should contain useful links and welcome text
Given I am logged in as an admin
And I am on the home page
Then I should see "Welcome to RapidFTR"
And I should see "Register New Child"
And I should see "View Records"
And I should see "0 Records need Attention"
And I should see "Register New Enquiry"
