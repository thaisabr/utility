Classes: 7
[name:Child, file:rapidftr_RapidFTR/app/models/child.rb, step:Given ]
[name:Inspector, file:null, step:Then ]
[name:PDF, file:null, step:Then ]
[name:Spec, file:rapidftr_RapidFTR/vendor/plugins/rspec/lib/spec.rb, step:Then ]
[name:Tempfile, file:null, step:Then ]
[name:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Given ]
[name:User, file:rapidftr_RapidFTR/vendor/gems/couchrest-0.34/spec/fixtures/more/user.rb, step:Given ]

Methods: 36
[name:Hpricot, type:Object, file:null, step:When ]
[name:assert_contain, type:Object, file:null, step:Then ]
[name:at, type:Object, file:null, step:When ]
[name:body, type:Object, file:null, step:When ]
[name:check, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:Given ]
[name:close, type:Object, file:null, step:Then ]
[name:contain, type:Object, file:null, step:Then ]
[name:create!, type:Object, file:null, step:Given ]
[name:each, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:first, type:User, file:rapidftr_RapidFTR/app/models/user.rb, step:Given ]
[name:first, type:User, file:rapidftr_RapidFTR/vendor/gems/couchrest-0.34/spec/fixtures/more/user.rb, step:Given ]
[name:has_key?, type:Object, file:null, step:Given ]
[name:hashes, type:Object, file:null, step:Given ]
[name:have, type:Object, file:null, step:Then ]
[name:include, type:Object, file:null, step:Then ]
[name:new_with_user_name, type:Child, file:rapidftr_RapidFTR/app/models/child.rb, step:Given ]
[name:nil?, type:Object, file:null, step:When ]
[name:pages, type:Object, file:null, step:Then ]
[name:password, type:Object, file:null, step:Given ]
[name:path, type:Object, file:null, step:Then ]
[name:raise, type:Object, file:null, step:When ]
[name:response, type:Object, file:null, step:Then ]
[name:response, type:Object, file:null, step:When ]
[name:response_body, type:Object, file:null, step:Then ]
[name:reverse_merge!, type:Object, file:null, step:Given ]
[name:search, type:Object, file:null, step:When ]
[name:slice, type:Object, file:null, step:Given ]
[name:strings, type:Object, file:null, step:Then ]
[name:to_i, type:Object, file:null, step:When ]
[name:to_i, type:Object, file:null, step:Then ]
[name:uploadable_photo, type:Object, file:null, step:Given ]
[name:user_name, type:Object, file:null, step:Given ]
[name:write, type:Object, file:null, step:Then ]

Referenced pages: 0

