When /^I fill in the basic details of a child$/ do

  fill_in("Last known location", :with => "Haiti")
  attach_file("photo", "features/resources/jorge.jpg", "image/jpg")

end
When /^I fill in the required details for a child$/ do
  fill_in("Last known location", :with => "Haiti")
end
Given /^the following children exist in the system:$/ do |children_table|
  children_table.hashes.each do |child_hash|
    child_hash.reverse_merge!(
            'last_known_location' => 'Cairo',
            'photo_path' => 'features/resources/jorge.jpg',
            'reporter' => 'zubair',
            'age_is' => 'Approximate'
    )

    photo = uploadable_photo(child_hash.delete('photo_path'))
    unique_id = child_hash.delete('unique_id')
    child = Child.new_with_user_name(child_hash['reporter'], child_hash)
    child.photo = photo
    child['unique_identifier'] = unique_id if unique_id
    child.create!
  end
end
Then /^I should see "([^\"]*)" in the column "([^\"]*)"$/ do |value, column|

  column_index = -1

  Hpricot(response.body).search("table tr th").each_with_index do |cell, index|
    if (cell.to_plain_text == column )
      column_index = index
    end
  end

  column_index.should be > -1
  rows = Hpricot(response.body).search("table tr")

  match = rows.find do |row|
    cells = row.search("td")
    (cells[column_index] != nil && cells[column_index].to_plain_text == value)
  end

  raise Spec::Expectations::ExpectationNotMetError, "Could not find the value: #{value} in the table" unless match
end
Given /^I am logged in$/ do
  Given "there is a User"
  Given "I am on the login page"
  Given "I fill in \"#{User.first.user_name}\" for \"user name\""
  Given "I fill in \"123\" for \"password\""
  Given "I press \"Log In\""
end
Given /"([^\"]*)" is logged in/ do |user_name|
  Given "\"#{user_name}\" is the user"
  Given "I am on the login page"
  Given "I fill in \"#{user_name}\" for \"user name\""
  Given "I fill in \"123\" for \"password\""
  Given "I press \"Log In\""  
end
Given /^there is a child with the name "([^\"]*)" and a photo from "([^\"]*)"$/ do |child_name, photo_file_path|
  child = Child.new( :name => child_name, :last_known_location => 'Chile' )

  child.photo = uploadable_photo(photo_file_path)

  child.create!
end
Given /^the following form sections exist in the system:$/ do |form_sections_table|
  form_sections_table.hashes.each do |form_section_hash|
    form_section_hash.reverse_merge!(
            'unique_id'=> form_section_hash["name"].gsub(/\s/, "_").downcase 
    )
    FormSectionDefinition.create!(form_section_hash)
  end
end
Then /^I should see a (\w+) in the enabled column for the form section "([^\"]*)"$/ do |expected_icon, form_section|
  row = Hpricot(response.body).form_section_row_for form_section
  row.should_not be_nil

  enabled_icon = row.enabled_icon
  enabled_icon["class"].should contain(expected_icon)
end
Then /^I should see the "([^\"]*)" form section link$/ do |form_section_name|

  form_section_names = Hpricot(response.body).form_section_names.collect {|item| item.inner_html}
  form_section_names.should contain(form_section_name)

end
Then /^I should see the text "([^\"]*)" in the enabled column for the form section "([^\"]*)"$/ do |expected_text, form_section|
  row = Hpricot(response.body).form_section_row_for form_section
  row.should_not be_nil

  enabled_icon = row.enabled_icon
  enabled_icon.inner_html.strip.should == expected_text
end
Then /^I should see the description text "([^\"]*)" for form section "([^\"]*)"$/ do |expected_description, form_section|
  row = Hpricot(response.body).form_section_row_for form_section
  description_text_cell = row.search("td").detect {|cell| cell.inner_html.strip == expected_description}
  description_text_cell.should_not be_nil
end
Then /^I should see the form section "([^\"]*)" in row (\d+)$/ do |form_section, expected_row_position|
  row = Hpricot(response.body).form_section_row_for form_section
  rows =  Hpricot(response.body).form_section_rows
  rows[expected_row_position.to_i].inner_html.should == row.inner_html
end
And /^I should see a current order of "([^\"]*)" for the "([^\"]*)" form section$/ do |expected_order, form_section|
  row = Hpricot(response.body).form_section_row_for form_section
  order_display = row.form_section_order
  order_display.inner_html.strip.should == expected_order
end
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^\"]*)"$/ do |button|
  click_button(button)
end
When /^(?:|I )follow "([^\"]*)"$/ do |link|
  click_link(link)
end
When /^(?:|I )fill in "([^\"]*)" for "([^\"]*)"$/ do |value, field|
  fill_in(field, :with => value)
end
When /^(?:|I )attach the file "([^\"]*)" to "([^\"]*)"$/ do |path, field|
  type = path.split(".")[1]

  case type
    when "jpg"
      type = "image/jpg"
    when "jpeg"
      type = "image/jpeg"
    when "png"
      type = "image/png"
    when "gif"
      type = "image/gif"
  end

  attach_file(field, path, type)
end
Then /^(?:|I )should see "([^\"]*)"$/ do |text|
  if defined?(Spec::Rails::Matchers)
    response.should contain(text)
  else
    assert_contain text
  end
end
  def path_to(page_name, options = {})

    format = page_name[/^(?:|the )(\w+) formatted/,1]
    options.reverse_merge!( :format => format )

    case page_name

      when /the home\s?page/
        '/'
      when /the new assign_unique_id_to_a_child page/
        new_assign_unique_id_to_a_child_path(options)


      when /new child page/
        new_child_path(options)

      when /children listing page/
        children_path(options)

      when /saved record page for child with name "(.+)"/
        child_name = $1
        child = Summary.by_name(:key => child_name)
        raise "no child named '#{child_name}'" if child.nil?
        child_path( child, options )

      when /new user page/
        new_user_path(options)

      when /manage users page/
        users_path(options)

      when /edit user page for "(.+)"/
        user = User.find_by_user_name($1)
        edit_user_path(user, options)

      when /child search page/
        search_children_path(options)
        
      when /login page/
        login_path(options)
        
      when /logout page/
        logout_path(options)
        
      when /child search results page/
        search_children_path(options)

      when /form section page/
        '/formsection'

      when /choose field type page/
        '/fields/new'

      when /photo resource for child with name "(.+)"/
        child_name = $1
        child = Summary.by_name(:key => child_name)
        raise "no child named '#{child_name}'" if child.nil?
        child_path( child, :format => 'jpg' )

      when /new field page for "(.+)"/
        field_type = $1
        new_field_path(:fieldtype=>field_type)

      # Add more mappings here.
      # Here is an example that pulls values out of the Regexp:
      #
      #   when /^(.*)'s profile page$/i
      #     user_profile_path(User.find_by_login($1))

      else
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
                "Now, go and add a mapping in #{__FILE__}"
    end
  end
Then /^(?:|I )should see a link to the (.+)$/ do |page_name|
  response_body.should have_selector("a[href='#{path_to(page_name)}']")
end
Then /^I should find the following links:$/ do |table|
  table.rows_hash.each do |label, named_path|
    href = path_to(named_path)
    assert_have_xpath "//a[@href='#{href}' and text()='#{label}']"
  end
end
Then /^I should find the form with following attributes:$/ do |table|
  table.raw.each do |attribute|
    assert_contain attribute.first
  end
end
