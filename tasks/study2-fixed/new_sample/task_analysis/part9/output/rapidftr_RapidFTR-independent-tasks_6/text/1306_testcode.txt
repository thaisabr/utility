When /^I search using a name of "([^\"]*)"$/ do |name|
  Given %q{I am on the child search page}
  When "I fill in \"#{name}\" for \"Name\""
  And %q{I press "Search"}
end
When /^I select search result \#(\d+)$/ do |ordinal|
  ordinal = ordinal.to_i
  result_row_to_select = Hpricot(response.body).search("table tr")[ordinal]
  raise "table does not have a row #{ordinal}" if result_row_to_select.nil?
  checkbox = result_row_to_select.at("td input")
  raise 'result row to select has not checkbox' if checkbox.nil?
  check(checkbox[:id])
end
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^\"]*)"$/ do |button|
  click_button(button)
end
When /^(?:|I )follow "([^\"]*)"$/ do |link|
  click_link(link)
end
When /^(?:|I )fill in "([^\"]*)" for "([^\"]*)"$/ do |value, field|
  fill_in(field, :with => value)
end
Then /^(?:|I )should not see "([^\"]*)"$/ do |text|
  if defined?(Spec::Rails::Matchers)
    response.should_not contain(text)
  else
    assert_not_contain(text)
  end
end
Then /^the "([^\"]*)" field should contain "([^\"]*)"$/ do |field, value|
  if defined?(Spec::Rails::Matchers)
    field_labeled(field).value.should =~ /#{value}/
  else
    assert_match(/#{value}/, field_labeled(field).value)
  end
end
Then /^(?:|I )should be on (.+)$/ do |page_name|
  current_path = URI.parse(current_url).select(:path, :query).compact.join('?')
  if defined?(Spec::Rails::Matchers)
    current_path.should == path_to(page_name)
  else
    assert_equal path_to(page_name), current_path
  end
end
  def path_to(page_name)
    case page_name

      when /the home\s?page/
        '/'
      when /the new assign_unique_id_to_a_child page/
        new_assign_unique_id_to_a_child_path


      when /new child page/
        new_child_path

      when /children listing page/
        children_path

      when /saved record page for child with name "(.+)"/
        child_name = $1
        child = Summary.by_name(:key => child_name)
        raise "no child named '#{child_name}'" if child.nil?
        child_path( child )

      when /new user page/
        new_user_path

      when /manage users page/
        users_path

      when /child search page/
        search_children_path
        
      when /login page/
        login_path
        
      when /child search results page/
        search_children_path



      when /photo resource for child with name "(.+)"/
        child_name = $1
        child = Summary.by_name(:key => child_name)
        raise "no child named '#{child_name}'" if child.nil?
        child_path( child, :format => 'jpg' )


      # Add more mappings here.
      # Here is an example that pulls values out of the Regexp:
      #
      #   when /^(.*)'s profile page$/i
      #     user_profile_path(User.find_by_login($1))

      else
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
                "Now, go and add a mapping in #{__FILE__}"
    end
  end
Then /^I should receive a CSV file with (\d+) line(?:|s)?$/ do |num_lines|
  num_lines = num_lines.to_i
  response_body.chomp.split("\n").length.should == num_lines
end
Given /^the following children exist in the system:$/ do |children_table|
  Given "no children exist"
  children_table.hashes.each do |child_hash|
    child_hash.reverse_merge!( 
      'last_known_location' => 'Cairo', 
      'photo_path' => 'features/resources/jorge.jpg',
      'reporter' => 'zubair'
    )

    child = Child.new_with_user_name( child_hash['reporter'], child_hash.slice('name','last_known_location') )
    child.photo = uploadable_photo(child_hash['photo_path'])
    child['unique_identifier'] = child_hash['unique_id'] if child_hash.has_key?('unique_id')
    child.create!
  end
end
Then /^I should see "([^\"]*)" in the column "([^\"]*)"$/ do |value, column|

  column_index = -1

  Hpricot(response.body).search("table tr th").each_with_index do |cell, index|
    if (cell.to_plain_text == column )
      column_index = index
    end
  end

  column_index.should be > -1
  rows = Hpricot(response.body).search("table tr")

  match = rows.find do |row|
    cells = row.search("td")
    (cells[column_index] != nil && cells[column_index].to_plain_text == value)
  end
  
  raise Spec::Expectations::ExpectationNotMetError, "Could not find the value: #{value} in the table" unless match
end
Given /I am logged in/ do
  Given "there is a User"
  Given "I am on the login page"
  Given "I fill in \"#{User.first.user_name}\" for \"user name\""
  Given "I fill in \"#{User.first.password}\" for \"password\""
  Given "I press \"Log In\""
end
Then /^I should receive a PDF file$/ do
  Tempfile.open('rapidftr_cuke_tests') do |temp_file|
    temp_file.write( response_body )
    temp_file.close
    mimetype = `file --brief --mime #{temp_file.path}`.gsub(/\n/,"")
    mimetype.should == "application/pdf"
  end
end
Then /^the PDF file should have (\d+) pages$/ do |num_pages|
  num_pages = num_pages.to_i

  pdf = PDF::Inspector::Page.analyze( response_body )
  pdf.should have(num_pages).pages
end
Then /^the PDF file should contain the string "([^\"]*)"$/ do |expected_string|
  pdf = PDF::Inspector::Text.analyze( response_body )
  pdf.strings.should include(expected_string)
end
Given /^no children exist$/ do
  Child.all.each{|child| child.destroy }
end
