= render partial: 'layouts/header', locals: {page_heading: 'API release notes'}

%section.api-github-issue
  Any development issues? aside from our
  %a{href: 'https://laavendorsupport.slack.com', target: '_blank'}
    vendor support slack channel
  you can raise
  %a{href: 'https://github.com/ministryofjustice/Claim-for-Crown-Court-Defence/issues', target: '_blank'}
    Github issues
  for bugs or other problems.

%section.api-documentation
  %hr
  %h2 20th December 2018
  %ul.list-bullet
    %li
      Deprecation of the LGFS Miscellaneous defendant uplift fee type on all but fixed fee claims.
      %br/
      %br/
      The following fee type should only be used for LGFS final claim fixed fee
      cases:
      %pre.code
        {
        &nbsp;"id": 90,
        &nbsp;"type": "Fee::MiscFeeType",
        &nbsp;"description: "Defendant uplift",
        &nbsp;"code": "XUPL",
        &nbsp;"unique_code": "MIUPL",
        &nbsp;"max_amount": null,
        &nbsp;"calculated": false,
        &nbsp;"roles": [
        &nbsp; "lgfs"
        &nbsp;],
        &nbsp;"quantity_is_decimal": false,
        &nbsp;"case_numbers_required": false
        }
      %br/
      You can get the whole list of LGFS miscellaneous fee types using this query:
      %pre.code
        curl -X GET --header 'Accept: application/json' &&
        'https://api-sandbox-adp.dsd.io/api/fee_types?api_key=[your-api-key]&role=lgfs&category=misc'
      %br/
      This is being done as part of fee calculation integration into the CCCD web application. We are applying defendant uplifts to LGFS graduated, transfer and interim
      fees based on the number of defendants supplied on the claim.
      %br/
      %br/
      As a result the Miscellaneous fee type of Defendant uplift is no longer
      required and is being removed from the web interface for these fees. It
      is solely available for use in respect of LGFS final fixed fee claims.
      %br/
      %br/
      API submitted fees of this type will continue to be created, even for inapplicable/ineligible claim types, but could result in double claiming of defendant uplifts and rejection by case workers.
      %br/
      %br/
      API consuming software should apply defendant uplifts as part of the graduated
      fee amount, transfer fee amount or interim fee amount as applicable.

%section.api-documentation
  %hr
  %h2 3rd December 2018
  %ul.list-bullet
    %li
      Adding new travel expense additional information field to claim endpoints
      %br/
      %br/
      An additional field,
      %code.code
        travel_expense_additional_information
      , has been added to all of the claim endpoints in the
      API.  It is currently optional, but at a future date we will start to enforce validation in line with the web
      interface.
      %br/
      %br/
      The validation rule that will come into effect on the API in the future is:
      %br/
      If any of the travel expenses have their
      %code.code
        mileage_rate_id
      values set to
      %code.code
        2
      then the associated claim must have been created with
      %code.code
        travel_expense_additional_information
      set

%section.api-documentation
  %hr
  %h2 05 November 2018
  %ul.list-bullet
    %li
      Implementation of LGFS fixed fee calculation.
      %br/
      %br/
      The CCCD web interface has been amended to calculate LGFS fixed fees. As part of this work the interface
      has (re)introduced the
      %code.code
        quantity
      and
      %code.code
        rate
      attributes for all
      %code.code
        LGFS Fee::FixedFeeType
      fee objects. The
      %code.code
        amount
      attribute will be calculated on a simple quantity x rate basis.
      %br/
      %br/
      The interface will populate the
      %code.code
        rate
      attribute by querying the
      %a{:href => 'https://github.com/ministryofjustice/laa-fee-calculator', target: '_blank'}
        LAA fee calculation API
      \. The
      %code.code
        quantity
      is required.
      %br/
      %br/
      Any API consumers (applications) that POST LGFS fixed fees with an amount, but no rate and
      quantity will default the quantity to 1 and rate to the amount provided.
      %br/
      %br/
      For example:
      %br/
      %pre.code
        {
        &nbsp;api_key: you-provider-api-key,
        &nbsp;claim_id: claim-uuid,
        &nbsp;fee_type_id: fixed-fee-type-id,
        &nbsp;date: "2018-04-19",
        &nbsp;amount: 349.47
        }
      %br/
      will result in a fixed fee with the following attributes:
      %br/
      %pre.code
        {
        &ensp;claim_id: claim-uuid,
        &ensp;fee_type_id: fixed-fee-type-id,
        &ensp;date: "2018-04-19",
        &ensp;quantity: 1
        &ensp;rate: 349.47
        &ensp;amount: 349.47}
        }
      %br/
      %br/
      Long term, API consumers should look to provide quantity and rate separately where quantity represents the number of days (in court) typically.

%section.api-documentation
  %hr
  %h2 10 September 2018
  %ul.list-bullet
    %li
      The miscellaneous fee type "Adjourned appeals" has been repurposed as a fixed fee type. The
      %code.code
        id
      remains the same but the
      %code.code
        type
      ,
      %code.code
        unique_code
      and
      %code.code
        code
      and
      %code.code
        description
      have been modified as below.

      %br/
      %br/
      %table
        %thead
          %tr
            %th
              Field
            %th
              Old value
            %th
              New value
        %tbody
          %tr
            %td
              id
            %td
              33
            %td
              33
          %tr
            %td
              type
            %td
              Fee::MiscFeeType
            %td
              Fee::FixedFeeType
          %tr
            %td
              unique_code
            %td
              MISAF
            %td
              FXADJ
          %tr
            %td
              code
            %td
              SAF
            %td
              ADJ
          %tr
            %td
              description
            %td
              Adjourned appeals
            %td
              Adjourned appeals, committals and breaches
      This change relates to business logic and user research findings that show that claims for remuneration in relation to Adjournments of appeals against conviction or sentence, committal for sentence and breach of crown court order hearings are typically claimed for in isolation and users are having to work around the current system to submit such claims.
      %br/
      %br/
      All existing fees of this type will be updated in line with these changes, but will not affect their value.

  %hr
  %h2 3 September 2018
  %ul.list-bullet
    %li
      AGFS fixed fee eligiblity logic has been applied to web interface.
      %br/
      %br/
      It was previously possible to add a fixed fee of any type to any
      fixed fee case type. However, certain combinations are not applicable and can result in claims be rejected in full or in part. The web interface has been modifed to ensure that only applicable fixed fees can be claimed, based on the case type selected. The table below summarizes the change.
      %br/
      %br/
      %table
        %caption
          Eligible fixed fee types
        %thead
          %tr
            %th
              Case type
            %th
              Eligible fixed fees
        %tbody
          %tr
            %td
              Appeal against conviction
            %td
              Appeals to the crown court against conviction; Standard appearance fee; Number of cases uplift; Number of defendants uplift
          %tr
            %td
              Appeal against sentence
            %td
              Appeals to the crown court against sentence;Standard appearance fee; Number of cases uplift; Number of defendants uplift
          %tr
            %td
              Breach of Crown Court order
            %td
              Breach of a crown court order; Standard appearance fee; Number of cases uplift; Number of defendants uplift
          %tr
            %td
              Committal for Sentence
            %td
              Committal for sentence hearings; Standard appearance fee; Number of cases uplift; Number of defendants uplift
          %tr
            %td
              Contempt
            %td
              Contempt; Standard appearance fee
          %tr
            %td
              Elected cases not proceeded
            %td
              Elected case not proceeded; Number of cases uplift; Number of defendants uplift
        %tfoot
          %tr
            %td{ colspan: 4}
              %em
                ** see not below
    %li
      Case type-specific fixed fee case uplifts have been deprecated.
      %br/
      %br/
      The following case uplifts are no longer considered eligible for any case type and are to be considered deprecated.
      %br/
      %br/
      %table
        %caption
          Deprecated case uplift fixed fee types
        %thead
          %tr
            %th
              description
            %th
              unique_code
        %tbody
          %tr
            %td
              Appeals to the crown court against conviction uplift
            %td
              FXACU
          %tr
            %td
              Appeals to the crown court against sentence uplift
            %td
              FXASU
          %tr
            %td
              Breach of a crown court order uplift
            %td
              FXCBU
          %tr
            %td
              Committal for sentence hearings uplift
            %td
              FXCSU
          %tr
            %td
              Elected case not proceeded uplift
            %td
              FXENU
        %tfoot
          %tr
            %td{ colspan: 4}
              %em
                ** see not below
    %li
      "Cracked case discontinued" fixed fee type and its uplift has been deprecated.
      %br/
      %br/
      The following fixed fee type has been deprecated and should not be used.
      %br/
      %br/
      %table
        %caption
          Deprecated fixed fee types
        %thead
          %tr
            %th
              description
            %th
              unique_code
        %tbody
          %tr
            %td
              Cracked case discontinued
            %td
              FXCCD
          %tr
            %td
              Cracked case discontinued uplift
            %td
              FXCDU
        %tfoot
          %tr
            %td{ colspan: 4}
              %em
                ** see not below

    %em
      ** API consumer applications that send ineligible fixed fees will NOT error. However, when reviewing the claim's fixed fees in the web interface any ineligible fees will no longer have a type and will need to be amended to apply an eligible type.


  %hr
  %h2 20 April 2018
  %ul.list-bullet
    %li
      The endpoints for
      %code.code
        \/api/fee_types
      and
      %code.code
        \/api/advocate_categories
      have been updated.
      %br/
      %br/
      The
      %code.code
        role
      options have been updated on
      %code.code
        \/api/fee_types
      and added to
      %code.code
        \/api/advocate_categories
      you can now send
      %code.code
        agfs
      ,
      %code.code
        agfs_scheme_9
      ,
      %code.code
        agfs_scheme_10
      or
      %code.code
        lgfs
      to get the appropriate returns.
      %br/
      %br/
      Sending
      %code.code
        agfs
      will return the
      %code.code
        agfs_scheme_9
      values.
      %br/
      %br/
      Sending no
      %code.code
        role
      filter to either will return all data, regardless of fee scheme.
  %hr
  %h2 12 April 2018
  %ul.list-bullet
    %li
      The LGFS Miscellaneous fee "Case uplift" is being repurposed as a "Defendant uplift"
      %br/
      %br/
      This release changes the LGFS only miscellaneous fee type "Case uplift" with a unique code of
      %code.code
        MIUPL
      to be a Defendant uplift. This means the fee no longer requires case numbers to be supplied.
      %br/
      %br/
      This change encapsulates two distinct changes, namely, Case uplifts are not applicable to LGFS
      claims and Defendant uplifts can be claimed.

  %hr
  %h2 6 April 2018
  %ul.list-bullet
    %li
      A new claim type was introduced as part of the AGFS fee scheme changes on 1st April 2018. This has been implemented on the API as
      %code.code
        \/api/external_users/claims/advocates/interim
      and enables a submission of a warrant fee.
      Please see the
      =link_to 'Crown Court Fee Guidance', 'https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/696617/crown-court-fee-guidance.-post-1-apr-2018.pdf'
      for details.
      %br/
      %br/
      This has a reduced set of fields as the claim will never require a case type value or trial dates.  We have also taken the opportunity to remove the deprecated
      %code.code
        advocate_email
      field.
      %br/
      %br/
      There is the usual
      %code.code
        \/api/external_users/claims/advocates/interim/validate
      endpoint to test your submissions against.  This will return any validation errors that will prevent the record being created on the creation endpoint.

  %hr
  %h2 23 March 2018
  %ul.list-bullet
    %li
      Due to the implementation of AGFS fee scheme 10 in April 2018 significant changes to the offence list are needed.
      %br/
      %br/
      This release adds the new 1244 AGFS offences to the existing offences list, but starting from 1000.  Therefore the
      %code.code
        \/api/external_users/offences
      endpoint has an additional, optional, parameter;
      %code.code
        rep_order_date
      ='.'
      %br/
      %br/
      When left empty or submitted with a date prior to the start of scheme 10, this will return the current offences from AGFS scheme 9.
      %br/
      %br/
      When submitted with a date on or after the start of scheme 10 this will return the new, scheme 10, offences.

  %hr
  %h2 24 February 2018
  %ul.list-bullet
    %li
      The Evidence provision fee is now locked to two values, £45 and £90 as described in the
      = link_to 'guidance', 'https://www.gov.uk/guidance/claim-the-evidence-provision-fee#lower-and-upper-tiers'
      ='.'
      %br/
      %br/
      These values have always been the only valid values but, prior to this release, the validation had not been enforced.
      %br/
      %br/
      From now on submitting data to the
      %code.code
        \/api/external_users/fee
      endpoint with either of the following states will result in a
      %code.code
        Evidence provision fee can only be £45 or £90
      error.
      %br/
      %br/
      %table
        %thead
          %tr
            %th
              Field
            %th
              Value
        %tbody
          %tr
            %td
              fee_type_id
            %td
              87
          %tr
            %td
              fee_type_unique_code
            %td
              MIEVI

  %hr
  %h1 01 November 2017
  %ul.list-bullet
    %li
      The Fee type GET endpoint
      %code.code
        \/api/fee_types
      will return an additional boolean attribute
      %br/
      %br/
      %code.code
        "case_numbers_required": [true|false]
      %br/
      %br
      This denotes whether fees of that type require additional case numbers to be provided. This validation is enforced for claims submitted in the web application but not currently when POSTed via the API. See below for more.
      %br/
      %br/
    %li
      Case uplift fees require additional case numbers.
      %br/
      %br/
      The posting of data to the
      %code.code
        \/api/external_users/fee
      endpoint should include a matching number of comma delimited case numbers for certain fee types.
      %br/
      %br/
      For example, if the
      %code.code
        fee_type_unique_code
      is set to
      %code.code
        BANOC
      and the quantity to 2 then the
      %code.code
        case_numbers
      value should contain two valid case numbers, such as
      %code.code
        T20170001,T20170002
      \.
      %br/
      %br/
      %p
        The following fees will be invalid when
        %em
          submitting a claim in the web application
        unless their quantity value is matched by an equal number of comma separated, valid format of case numbers.
      %br/
      %table
        %thead
          %tr
            %th
              Fee Type
            %th
              Description
            %th
              Unique code
            %th
              Scheme
        %tbody
          %tr
            %td
              Basic
            %td
              Number of cases uplift
            %td
              BANOC
            %td
              AGFS
          %tr
            %td
              Fixed
            %td
              Number of cases uplift
            %td
              FXNOC
            %td
              AGFS
          %tr
            %td
              Fixed
            %td
              Appeals to the crown court against conviction uplift
            %td
              FXACU
            %td
              AGFS
          %tr
            %td
              Fixed
            %td
              Appeals to the crown court against sentence uplift
            %td
              FXASU
            %td
              AGFS
          %tr
            %td
              Fixed
            %td
              Committal for sentence hearings uplift
            %td
              FXCSU
            %td
              AGFS
          %tr
            %td
              Fixed
            %td
              Cracked case discontinued uplift
            %td
              FXCDU
            %td
              AGFS
          %tr
            %td
              Fixed
            %td
              Elected case not proceeded uplift
            %td
              FXENU
            %td
              AGFS
          %tr
            %td
              Fixed
            %td
              Breach of a crown court order uplift
            %td
              FXCBU
            %td
              AGFS
          %tr
            %td
              Miscellaneous
            %td
              Case uplift
            %td
              MIUPL
            %td
              LGFS
        %tfoot
          %tr
            %td{ colspan: 4}
              %em
                * For the time being, such fees will not enforce the presence of case numbers if created via the API. This is to give vendors time to amend and release their respective client applications. The requirement will be fully enforced on or after 1st April 2018.
                %br/
                %br/
                ** You can determine these fee types from the
                %code.code
                  api/fee_types
                endpoint using the
                %code.code
                  case_numbers_required
                boolean attribute
      %p
        This is required as part of work to inject claim data from CCCD into the Case workers internal application (Crown Court Remuneration - CCR), to reduce rekeying and speed up claim processing.

      %p
        %em
          Note: The API will implement the validation fully on or after 1st April 2018.

  %hr
  %h1 30 November 2016
  %ul.list-bullet
    %li
      Transfer Stages result set updated

      The result set returned from /api/transfer_stages now includes an additional field, 'requires_case_conclusion'
      which will be either true or false.  If true, a Litigator Transfer Claim submitted with that transfer_stage_id
      MUST also specify a case_conclusion_id.  If false, the case_conclusion_id MUST NOT be specified.

      There is no change to the validation of transfer claims; the addition of this field is to make it clear to API
      developers when a case conclusion id is or is not required.

  %p
  %hr
  %h1 November 2016
  %ul.list-bullet
    %li
      The case number validation rules have been changed to match XYYYYNNNN, where:
      %p
        X is B, A, S, T or U.
      %p
        Y is 4-digit year number
      %p
        NNNN is numeric 0001 to 9999

    %p
    %li
      A new alpha-numeric field 'unique_code' has been added to the result hash for the disbursement_types endpoint. This value is
      unique to each record.
    %p
    %li
      The Disbursements endpoint now accepts the above unique code as a parameter called 'disbursement_type_unique_code', which internally will be
      translated to the corresponding 'disbursement_type_id'.
      %p
      disbursement_type_id and disbursement_type_unique_code are mutually exclusive.

    %p
    %li
      A new alpha-numeric field 'unique_code' has been added to the result hash for the expense_types endpoint. This value
      is unique to each record.
    %p
    %li
      The Expenses endpoint now accepts the above unique code as a parameter called 'expense_type_unique_code', which internally will be
      translated to the corresponding 'expense_type_id'.
      %p
      expense_type_id and expense_type_unique_code are mutually exclusive.

  %hr
  %h1 October 2016
  %ul.list-bullet
    %li
      Users of the api-sandbox were having difficulties because the ids of the static data records (expense types, fee types etc) were
      liable to change every time we regenerated the database.  Over the weekend of 8th-9th October, the database was recreated and the ids
      of these records fixed to match those on the live database.  These ids are now static and will not change in future.
    %p
    %li
      An extra field "type" has been added to the result hash for fee types.  This will be one of the following values:
      %ul.list-bullet
        %li Fee::MiscFeeType
        %li Fee::GraduatedFeeType
        %li Fee::BasicFeeType
        %li Fee::FixedFeeType
        %li Fee::InterimFeeType
        %li Fee::TransferFeeType
        %li Fee::WarrantFeeType

    %p
    %li
      A new alpha-numeric field 'unique_code' has been added to the result hash for the fee_types endpoint.  This value
      is unique to each record (the 'code' field is not unique; it is the code that was printed on the AF1 form and certain codes
      are shared by several fee types).

    %p
    %li
      The Fees endpoint now accepts the above unique code as a parameter called 'fee_type_unique_code', which internally will be
      translated to the corresponding 'fee_type_id'.
      %br
      fee_type_id and fee_type_unique_code are mutually exclusive so you can only provide one or the other.

  %hr
  %h1 August 2016

  %ul.list-bullet
    %li
      Offence Ids on LGFS claims (Litigator Final Claim, Litigator Transfer Claim, Litigator Interim Claim) are no longer limited to those ids derived from the Offence Class.  Previously,
      the only offence ids that were considered valid on an LGFS claim were those that were derived from the litigator_offence_id on the relevant Offence Class.  This is no longer that case,
      any offence id may be used.

  %hr
  %h2 July 2016

  %ul.list-bullet
    %li
      Litigator Claims can now be submitted via the API. Please consult the
      = link_to "interactive documentation", grape_swagger_rails_path
      for further deatils.
      %p
      An overview of which endpoints to use for claim submission is as follows:
      %table
        %tr
          %th Action
          %th Endpoint URL
        %tr
          %td Validate advocate claim
          %td POST /api/external_users/claims/validate
        %tr
          %td Create advocate claim
          %td POST /api/external_users/claims
        %tr
          %td Validate litigator final claim
          %td POST /api/external_users/claims/final/validate
        %tr
          %td Create litigator interim claim
          %td POST /api/external_users/claims/final
        %tr
          %td Validate litigator interim claim
          %td POST /api/external_users/claims/interim/validate
        %tr
          %td Create litigator interim claim
          %td POST /api/external_users/claims/interim
        %tr
          %td Validate litigator transfer claim
          %td POST /api/external_users/claims/transfer/validate
        %tr
          %td Create litigator transfer claim
          %td POST /api/external_users/claims/transfer

    %li
      Disbursements can now be validated and added to a claim via the API via:
      %table
        %tr
          %td POST /api/external_users/disbursements/validate
        %tr
          %td POST /api/external_users/disbursements

    %li
      A list of valid Disbursement Types can be retrieved via:
      %table
        %tr
          %td GET /api/disbursement_types

    %li
      A list of valid Transfer Stages (to be used when creating a litigator transfer claim) can be retrieved via:
      %table
        %tr
          %td GET /api/transfer_stages

    %li
      A list of valid Transfer Case Conclusions (to be used when creating a litigator transfer claim) can be retrieved via:
      %table
        %tr
          %td GET /api/transfer_case_conclusions


    %li
      An attribute 'roles' has been added to the returned structure for the expense types to indicate their validity:

      The roles attribute is an array and of one or both of the following values:
      %table
        %tr
          %th Value
          %th Meaning
        %tr
          %td agfs
          %td The expense type is valid for advocate claims
        %tr
          %td lgfs
          %td The expense type is valid for litigator final, litigater interim, and litigator transfer claims.

    %li
      An attribute 'roles' has been added to the returned structure for the case types to indicate their validity:

      The roles attribute is an array and of one or many of the following values:
      %table
        %tr
          %th Value
          %th Meaning
        %tr
          %td agfs
          %td The case type is valid for advocate claims
        %tr
          %td lgfs
          %td The case type is valid for litigator final and litigator transfer claims.
        %tr
          %td interim
          %td The case type is valid for litigator interim claims

    %li
      An attribute 'lgfs_offence_id' has been added to the returned structure for offence classes.  For litigator final, litigator
      interim and litigator transfer claims, the offence id specified in the claim
      %strong MUST
      be one of the lgfs_offence_ids returned in the offence_classes list. You should still use the offence ids returned from the /api/offences
      endpoint for advocate claims.

  %hr
  %h2 June 2016

  %ul.list-bullet
    %li
      Expense Types:  The list of valid expense types has changed completely, and now matches those that
      you will see on the website.  A GET request to 'api/expense_types' will return a a JSON response
      detailing all the expense types, including:

      %ul.list-bullet
        %li id: the expense_type_id to specify when creating an expense
        %li role: whether this is a valid expense type for AGFS claims, LGFS claims or both
        %li reason set: What valid reason_ids can be applied to expenses of this type

    %li
      Expense: A number of new fields have been introduced, - see the API documentation for which ones
      are necessary for which types of expense.

    %li
      Fee quantities: Quantities on fees of the following types now accept decimal numbers; all others accept only integers

      %ul.list-bullet
        %li SPF - Special Preparation Fee
        %li WPF - Wasted Preparation Fee
        %li RNF - Research of very unusual or novel factual issue
        %li CAV - Conferences and views
        %li WOA - Written / oral advice

    %li
      API to submit litigator claims: Work on this is starting now and expected to be available for beta testing
    by the end of June. We will release documentation for litigator claims within the next two weeks.

    %li
      Submitted dates must conform to ISO 8601, being the time information and time zone offset optional.
      Examples: 2016-12-31, 2016-12-31T11:45, 2016-12-31T11:45:30Z

  %hr
  %h2
    February 2016

  %ul.list-bullet
    %li case number validation - restricted to A20161234
    %li indictment removed from claim
    %li expense quantities allow decimals
    %li rate added to fees, amount removed except for PPE and NPW
    %li api key not required by claim importer
    %li creator does not have to be an admin
    %li advocates endpoint names renamed to external_users
    %li granting body removed from representation orders
    %li PPE and NPW fee types require quantity and amount - rate must be blank
    %li retrial fields added - required for Retrial case type claims
    %li claim importer - reports specific errors for valid JSON files that contain model validation errors

  %hr
  %h2
    October 2015

  %ul.list-bullet
    %li
      first significant release

  %hr
  %h2
    July 2015

  %ul.list-bullet
    %li
      created
