= render partial: 'layouts/header', locals: {page_heading: 'API release notes'}

%section.api-documentation

  %hr
  %h1 30 November 2016
  %ul.list-bullet
    %li
      Transfer Stages result set updated

      The result set returned from /api/transfer_stages now includes an additional field, 'requires_case_conclusion'
      which will be either true or false.  If true, a Litigator Transfer Claim submitted with that transfer_stage_id
      MUST also specify a case_conclusion_id.  If false, the case_conclusion_id MUST NOT be specified.

      There is no change to the validation of transfer claims; the addition of this field is to make it clear to API
      developers when a case conclusion id is or is not required.

  %p
  %hr
  %h1 November 2016
  %ul.list-bullet
    %li
      The case number validation rules have been changed to match XYYYYNNNN, where:
      %p
        X is B, A, S, T or U.
      %p
        Y is 4-digit year number
      %p
        NNNN is numeric 0001 to 9999

    %p
    %li
      A new alpha-numeric field 'unique_code' has been added to the result hash for the disbursement_types endpoint. This value is
      unique to each record.
    %p
    %li
      The Disbursements endpoint now accepts the above unique code as a parameter called 'disbursement_type_unique_code', which internally will be
      translated to the corresponding 'disbursement_type_id'.
      %p
      disbursement_type_id and disbursement_type_unique_code are mutually exclusive.

    %p
    %li
      A new alpha-numeric field 'unique_code' has been added to the result hash for the expense_types endpoint.  This value
      is unique to each record.
    %p
    %li
      The Expenses endpoint now accepts the above unique code as a parameter called 'expense_type_unique_code', which internally will be
      translated to the corresponding 'expense_type_id'.
      %p
      expense_type_id and expense_type_unique_code are mutually exclusive.



  %hr
  %h1 October 2016
  %ul.list-bullet
    %li
      Users of the api-sandbox were having difficulties because the ids of the static data records (expense types, fee types etc) were
      liable to change every time we regenerated the database.  Over the weekend of 8th-9th October, the database was recreated and the ids
      of these records fixed to match those on the live database.  These ids are now static and will not change in future.
    %p
    %li
      An extra field "type" has been added to the result hash for fee types.  This will be one of the following values:
      %ul.list-bullet
        %li Fee::MiscFeeType
        %li Fee::GraduatedFeeType
        %li Fee::BasicFeeType
        %li Fee::FixedFeeType
        %li Fee::InterimFeeType
        %li Fee::TransferFeeType
        %li Fee::WarrantFeeType

    %p
    %li
      A new alpha-numeric field 'unique_code' has been added to the result hash for the fee_types endpoint.  This value
      is unique to each record (the 'code' field is not unique; it is the code that was printed on the AF1 form and certain codes
      are shared by several fee types).

    %p
    %li
      The Fees endpoint now accepts the above unique code as a parameter called 'fee_type_unique_code', which internally will be
      translated to the corresponding 'fee_type_id'.
      %br
      fee_type_id and fee_type_unique_code are mutually exclusive so you can only provide one or the other.

  %hr
  %h1 August 2016

  %ul.list-bullet
    %li
      Offence Ids on LGFS claims (Litigator Final Claim, Litigator Transfer Claim, Litigator Interim Claim) are no longer limited to those ids derived from the Offence Class.  Previously,
      the only offence ids that were considered valid on an LGFS claim were those that were derived from the litigator_offence_id on the relevant Offence Class.  This is no longer that case,
      any offence id may be used.

  %hr
  %h2 July 2016

  %ul.list-bullet
    %li
      Litigator Claims can now be submitted via the API. Please consult the
      = link_to "interactive documentation", grape_swagger_rails_path
      for further deatils.
      %p
      An overview of which endpoints to use for claim submission is as follows:
      %table
        %tr
          %th Action
          %th Endpoint URL
        %tr
          %td Validate advocate claim
          %td POST /api/external_users/claims/validate
        %tr
          %td Create advocate claim
          %td POST /api/external_users/claims
        %tr
          %td Validate litigator final claim
          %td POST /api/external_users/claims/final/validate
        %tr
          %td Create litigator interim claim
          %td POST /api/external_users/claims/final
        %tr
          %td Validate litigator interim claim
          %td POST /api/external_users/claims/interim/validate
        %tr
          %td Create litigator interim claim
          %td POST /api/external_users/claims/interim
        %tr
          %td Validate litigator transfer claim
          %td POST /api/external_users/claims/transfer/validate
        %tr
          %td Create litigator transfer claim
          %td POST /api/external_users/claims/transfer

    %li
      Disbursements can now be validated and added to a claim via the API via:
      %table
        %tr
          %td POST /api/external_users/disbursements/validate
        %tr
          %td POST /api/external_users/disbursements

    %li
      A list of valid Disbursement Types can be retrieved via:
      %table
        %tr
          %td GET /api/disbursement_types

    %li
      A list of valid Transfer Stages (to be used when creating a litigator transfer claim) can be retrieved via:
      %table
        %tr
          %td GET /api/transfer_stages

    %li
      A list of valid Transfer Case Conclusions (to be used when creating a litigator transfer claim) can be retrieved via:
      %table
        %tr
          %td GET /api/transfer_case_conclusions


    %li
      An attribute 'roles' has been added to the returned structure for the expense types to indicate their validity:

      The roles attribute is an array and of one or both of the following values:
      %table
        %tr
          %th Value
          %th Meaning
        %tr
          %td agfs
          %td The expense type is valid for advocate claims
        %tr
          %td lgfs
          %td The expense type is valid for litigator final, litigater interim, and litigator transfer claims.

    %li
      An attribute 'roles' has been added to the returned structure for the case types to indicate their validity:

      The roles attribute is an array and of one or many of the following values:
      %table
        %tr
          %th Value
          %th Meaning
        %tr
          %td agfs
          %td The case type is valid for advocate claims
        %tr
          %td lgfs
          %td The case type is valid for litigator final and litigator transfer claims.
        %tr
          %td interim
          %td The case type is valid for litigator interim claims

    %li
      An attribute 'lgfs_offence_id' has been added to the returned structure for offence classes.  For litigator final, litigator
      interim and litigator transfer claims, the offence id specified in the claim
      %strong MUST
      be one of the lgfs_offence_ids returned in the offence_classes list. You should still use the offence ids returned from the /api/offences
      endpoint for advocate claims.

  %hr
  %h2 June 2016

  %ul.list-bullet
    %li
      Expense Types:  The list of valid expense types has changed completely, and now matches those that
      you will see on the website.  A GET request to 'api/expense_types' will return a a JSON response
      detailing all the expense types, including:

      %ul.list-bullet
        %li id: the expense_type_id to specify when creating an expense
        %li role: whether this is a valid expense type for AGFS claims, LGFS claims or both
        %li reason set: What valid reason_ids can be applied to expenses of this type

    %li
      Expense: A number of new fields have been introduced, - see the API documentation for which ones
      are necessary for which types of expense.

    %li
      Fee quantities: Quantities on fees of the following types now accept decimal numbers; all others accept only integers

      %ul.list-bullet
        %li SPF - Special Preparation Fee
        %li WPF - Wasted Preparation Fee
        %li RNF - Research of very unusual or novel factual issue
        %li CAV - Conferences and views
        %li WOA - Written / oral advice

    %li
      API to submit litigator claims: Work on this is starting now and expected to be available for beta testing
    by the end of June. We will release documentation for litigator claims within the next two weeks.

    %li
      Submitted dates must conform to ISO 8601, being the time information and time zone offset optional.
      Examples: 2016-12-31, 2016-12-31T11:45, 2016-12-31T11:45:30Z

  %hr
  %h2
    February 2016

  %ul.list-bullet
    %li case number validation - restricted to A20161234
    %li indictment removed from claim
    %li expense quantities allow decimals
    %li rate added to fees, amount removed except for PPE and NPW
    %li api key not required by claim importer
    %li creator does not have to be an admin
    %li advocates endpoint names renamed to external_users
    %li granting body removed from representation orders
    %li PPE and NPW fee types require quantity and amount - rate must be blank
    %li retrial fields added - required for Retrial case type claims
    %li claim importer - reports specific errors for valid JSON files that contain model validation errors

  %hr
  %h2
    October 2015

  %ul.list-bullet
    %li
      first significant release

  %hr
  %h2
    July 2015

  %ul.list-bullet
    %li
      created
