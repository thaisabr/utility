Classes: 13
[name:Advocate, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/advocate.rb, step:Given ]
[name:Capybara, file:null, step:When ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:Then ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:Then ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:Given ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:Given ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:When ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:When ]
[name:Document, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/document.rb, step:When ]
[name:Document, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/document.rb, step:Then ]
[name:Offence, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/offence.rb, step:When ]
[name:Timeout, file:null, step:When ]
[name:User, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/user.rb, step:Given ]

Methods: 117
[name:advocate_id, type:Object, file:null, step:Then ]
[name:ago, type:Object, file:null, step:When ]
[name:all, type:Document, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/document.rb, step:When ]
[name:attach_file, type:Object, file:null, step:When ]
[name:authorised, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:authorised, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:When ]
[name:check, type:Object, file:null, step:When ]
[name:choose, type:Object, file:null, step:When ]
[name:click, type:Object, file:null, step:When ]
[name:click_on, type:Object, file:null, step:When ]
[name:click_on, type:Object, file:null, step:Given ]
[name:count, type:Document, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/document.rb, step:When ]
[name:count, type:Document, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/document.rb, step:Then ]
[name:create, type:JsonDocumentImporter, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/json_document_importer.rb, step:Given ]
[name:create, type:Object, file:null, step:Given ]
[name:create, type:AdvocatesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/admin/advocates_controller.rb, step:null]
[name:create, type:CertificationsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/certifications_controller.rb, step:null]
[name:create, type:ClaimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:create, type:JsonDocumentImporterController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/json_document_importer_controller.rb, step:null]
[name:create, type:AllocationsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/admin/allocations_controller.rb, step:null]
[name:create, type:CaseWorkersController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/admin/case_workers_controller.rb, step:null]
[name:create, type:DocumentsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/documents_controller.rb, step:null]
[name:create, type:MessagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/messages_controller.rb, step:null]
[name:create, type:AdvocatesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/admin/advocates_controller.rb, step:When ]
[name:create, type:CertificationsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/certifications_controller.rb, step:When ]
[name:create, type:ClaimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:When ]
[name:create, type:JsonDocumentImporterController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/json_document_importer_controller.rb, step:When ]
[name:create, type:AllocationsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/admin/allocations_controller.rb, step:When ]
[name:create, type:CaseWorkersController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/admin/case_workers_controller.rb, step:When ]
[name:create, type:DocumentsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/documents_controller.rb, step:When ]
[name:create, type:MessagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/messages_controller.rb, step:When ]
[name:day, type:Object, file:null, step:When ]
[name:days, type:Object, file:null, step:When ]
[name:document, type:Object, file:null, step:When ]
[name:document_file_name, type:Object, file:null, step:Then ]
[name:documents, type:Object, file:null, step:Then ]
[name:documents, type:Object, file:null, step:When ]
[name:download, type:DocumentsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/documents_controller.rb, step:null]
[name:download, type:DocumentsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/documents_controller.rb, step:When ]
[name:download_attachment, type:MessagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/messages_controller.rb, step:null]
[name:download_attachment, type:MessagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/messages_controller.rb, step:When ]
[name:drag_and_drop_file, type:DropzoneHelper, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/support/dropzone_helper.rb, step:When ]
[name:edit, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:edit, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:When ]
[name:eq, type:Object, file:null, step:When ]
[name:eq, type:Object, file:null, step:Then ]
[name:evaluate_script, type:Object, file:null, step:When ]
[name:execute_script, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:find, type:Object, file:null, step:When ]
[name:find_by, type:Offence, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/offence.rb, step:When ]
[name:first, type:Object, file:null, step:When ]
[name:first, type:Document, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/document.rb, step:When ]
[name:first, type:Document, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/document.rb, step:Then ]
[name:first, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:Then ]
[name:first, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:Then ]
[name:first, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:Given ]
[name:first, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:Given ]
[name:first, type:Object, file:null, step:Then ]
[name:first, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:When ]
[name:first, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:When ]
[name:have_content, type:Object, file:null, step:When ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_selector, type:Object, file:null, step:When ]
[name:have_selector, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:When ]
[name:id, type:Object, file:null, step:Given ]
[name:index, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:index, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:When ]
[name:last, type:Document, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/document.rb, step:When ]
[name:last, type:Object, file:null, step:Then ]
[name:map, type:Object, file:null, step:When ]
[name:match_array, type:Object, file:null, step:When ]
[name:month, type:Object, file:null, step:When ]
[name:new, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:new, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:When ]
[name:outstanding, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:outstanding, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:When ]
[name:page, type:Object, file:null, step:When ]
[name:page, type:Object, file:null, step:Then ]
[name:post, type:Object, file:null, step:null]
[name:post, type:Object, file:null, step:When ]
[name:rand, type:Object, file:null, step:When ]
[name:select, type:Object, file:null, step:When ]
[name:set, type:Object, file:null, step:When ]
[name:show, type:DocumentsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/documents_controller.rb, step:null]
[name:show, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:show, type:DocumentsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/documents_controller.rb, step:When ]
[name:show, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:When ]
[name:sign_in, type:UserHelper, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/support/user_helper.rb, step:Given ]
[name:sleep, type:Object, file:null, step:Then ]
[name:strftime, type:Object, file:null, step:When ]
[name:tandcs, type:PagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/pages_controller.rb, step:Given ]
[name:to, type:Object, file:null, step:When ]
[name:to, type:Object, file:null, step:Then ]
[name:to_not, type:Object, file:null, step:Then ]
[name:to_not, type:Object, file:null, step:When ]
[name:to_s, type:DateAttended, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/date_attended.rb, step:When ]
[name:to_s, type:Location, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/location.rb, step:When ]
[name:to_s, type:OffenceClass, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/offence_class.rb, step:When ]
[name:uniq, type:Object, file:null, step:When ]
[name:update, type:AdvocatesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/admin/advocates_controller.rb, step:null]
[name:update, type:ClaimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:update, type:CaseWorkersController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/admin/case_workers_controller.rb, step:null]
[name:update, type:ClaimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/claims_controller.rb, step:null]
[name:update, type:UserMessageStatusesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/user_message_statuses_controller.rb, step:null]
[name:update, type:AdvocatesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/admin/advocates_controller.rb, step:When ]
[name:update, type:ClaimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:When ]
[name:update, type:CaseWorkersController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/admin/case_workers_controller.rb, step:When ]
[name:update, type:ClaimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/claims_controller.rb, step:When ]
[name:update, type:UserMessageStatusesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/user_message_statuses_controller.rb, step:When ]
[name:value, type:Object, file:null, step:When ]
[name:wait_for_ajax, type:WaitForAjax, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/support/wait_for_ajax.rb, step:When ]
[name:within, type:Object, file:null, step:When ]
[name:within, type:Object, file:null, step:Then ]
[name:year, type:Object, file:null, step:When ]

Referenced pages: 34
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_basic_fee_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_claims.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_cracked_trial_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_date_attended_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_defendant_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_evidence_checklist.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_expense_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_financial_summary.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_fixed_fee_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_form.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_misc_fee_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_offence_select.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_representation_order_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_search_form.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/authorised.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/edit.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/index.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/outstanding.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/show.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/sessions/new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/shared/_links.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/pages/tandcs.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim_header.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim_history.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim_status.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_evidence_checklist.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_evidence_list.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_message.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_messages.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_search_summary.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_summary.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_vat_summary.html.haml

