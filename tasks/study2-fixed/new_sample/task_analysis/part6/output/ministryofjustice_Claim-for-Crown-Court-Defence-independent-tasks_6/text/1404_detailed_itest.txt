Classes: 15
[name:Advocate, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/advocate.rb, step:Given ]
[name:CaseType, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/case_type.rb, step:Given ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:Then ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:Then ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:Given ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:Given ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:When ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:When ]
[name:Date, file:null, step:Given ]
[name:Offence, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/offence.rb, step:When ]
[name:Offence, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/offence.rb, step:Given ]
[name:Offence, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/offence.rb, step:Then ]
[name:Rails, file:null, step:Given ]
[name:Scheme, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/scheme.rb, step:Given ]
[name:User, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/user.rb, step:Given ]

Methods: 83
[name:ago, type:Object, file:null, step:When ]
[name:ago, type:Object, file:null, step:Given ]
[name:api_landing, type:PagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/pages_controller.rb, step:Given ]
[name:be, type:Object, file:null, step:Then ]
[name:be_zero, type:Object, file:null, step:Then ]
[name:check, type:Object, file:null, step:When ]
[name:check, type:Object, file:null, step:Given ]
[name:choose, type:Object, file:null, step:When ]
[name:choose, type:Object, file:null, step:Given ]
[name:click_on, type:Object, file:null, step:Given ]
[name:click_on, type:Object, file:null, step:When ]
[name:count, type:Object, file:null, step:Then ]
[name:create, type:Object, file:null, step:Given ]
[name:create, type:JsonDocumentImporter, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/json_document_importer.rb, step:Given ]
[name:current_path, type:Object, file:null, step:Then ]
[name:day, type:Object, file:null, step:When ]
[name:day, type:Object, file:null, step:Given ]
[name:days, type:Object, file:null, step:When ]
[name:days, type:Object, file:null, step:Given ]
[name:destroy_all, type:Object, file:null, step:When ]
[name:download, type:DocumentsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/documents_controller.rb, step:null]
[name:download_attachment, type:MessagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/messages_controller.rb, step:null]
[name:each, type:Object, file:null, step:Then ]
[name:edit, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:eq, type:Object, file:null, step:Then ]
[name:fees, type:RedeterminationPresenter, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/presenters/redetermination_presenter.rb, step:When ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find, type:Object, file:null, step:When ]
[name:find, type:Object, file:null, step:Given ]
[name:find_by, type:Offence, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/offence.rb, step:When ]
[name:find_by, type:Offence, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/offence.rb, step:Given ]
[name:find_by, type:Offence, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/offence.rb, step:Then ]
[name:find_or_create_by, type:Scheme, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/scheme.rb, step:Given ]
[name:find_or_create_by!, type:CaseType, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/case_type.rb, step:Given ]
[name:first, type:Object, file:null, step:When ]
[name:first, type:Object, file:null, step:Given ]
[name:first, type:Advocate, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/advocate.rb, step:Given ]
[name:first, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:When ]
[name:first, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:When ]
[name:has_select?, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_selector, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:When ]
[name:id, type:Object, file:null, step:Given ]
[name:id, type:Object, file:null, step:Then ]
[name:load, type:Object, file:null, step:Given ]
[name:month, type:Object, file:null, step:When ]
[name:month, type:Object, file:null, step:Given ]
[name:new, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:page, type:Object, file:null, step:Then ]
[name:rand, type:Object, file:null, step:When ]
[name:rand, type:Object, file:null, step:Given ]
[name:select, type:Object, file:null, step:When ]
[name:select, type:Object, file:null, step:Given ]
[name:send, type:Object, file:null, step:null]
[name:set, type:Object, file:null, step:When ]
[name:set, type:Object, file:null, step:Given ]
[name:show, type:DocumentsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/documents_controller.rb, step:null]
[name:show, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:sign_in, type:UserHelper, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/support/user_helper.rb, step:Given ]
[name:strftime, type:Object, file:null, step:When ]
[name:strftime, type:Object, file:null, step:Given ]
[name:tandcs, type:PagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/pages_controller.rb, step:Given ]
[name:to, type:Object, file:null, step:Then ]
[name:to_s, type:DateAttended, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/date_attended.rb, step:When ]
[name:to_s, type:Location, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/location.rb, step:When ]
[name:to_s, type:OffenceClass, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/offence_class.rb, step:When ]
[name:to_s, type:DateAttended, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/date_attended.rb, step:Given ]
[name:to_s, type:Location, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/location.rb, step:Given ]
[name:to_s, type:OffenceClass, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/offence_class.rb, step:Given ]
[name:update, type:AdvocatesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/admin/advocates_controller.rb, step:null]
[name:update, type:ClaimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:update, type:CaseWorkersController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/admin/case_workers_controller.rb, step:null]
[name:update, type:ClaimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/claims_controller.rb, step:null]
[name:update, type:UserMessageStatusesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/user_message_statuses_controller.rb, step:null]
[name:where, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:Then ]
[name:where, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:Then ]
[name:within, type:Object, file:null, step:When ]
[name:within, type:Object, file:null, step:Given ]
[name:within, type:Object, file:null, step:Then ]
[name:year, type:Object, file:null, step:When ]
[name:year, type:Object, file:null, step:Given ]

Referenced pages: 28
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_basic_fee_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_cracked_trial_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_date_attended_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_defendant_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_evidence_checklist.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_expense_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_fixed_fee_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_form.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_misc_fee_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_offence_select.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_representation_order_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/edit.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/show.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/sessions/new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/shared/_links.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/pages/api_landing.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/pages/tandcs.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim_header.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim_status.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_evidence_checklist.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_evidence_list.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_message.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_message_list.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_messages.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_summary.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_vat_summary.html.haml

