Classes: 13
[name:Advocate, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/advocate.rb, step:Given ]
[name:CaseWorker, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/case_worker.rb, step:Given ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:Given ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:Given ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:When ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:When ]
[name:Date, file:null, step:Given ]
[name:Message, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/message.rb, step:null]
[name:Messages, file:ministryofjustice_Claim-for-Crown-Court-Defence/spec/factories/messages.rb, step:Given ]
[name:Messages, file:ministryofjustice_Claim-for-Crown-Court-Defence/spec/factories/messages.rb, step:Then ]
[name:MessagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/messages_controller.rb, step:null]
[name:Scheme, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/scheme.rb, step:Given ]
[name:User, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/user.rb, step:Given ]

Methods: 41
[name:authorised, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:body, type:Object, file:null, step:Then ]
[name:click_on, type:Object, file:null, step:Given ]
[name:click_on, type:Object, file:null, step:When ]
[name:create, type:Object, file:null, step:Given ]
[name:create, type:JsonDocumentImporter, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/json_document_importer.rb, step:Given ]
[name:create_list, type:Object, file:null, step:Given ]
[name:download, type:DocumentsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/documents_controller.rb, step:null]
[name:download_attachment, type:MessagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/messages_controller.rb, step:null]
[name:edit, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:fill_in, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find_or_create_by, type:Scheme, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/scheme.rb, step:Given ]
[name:first, type:CaseWorker, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/case_worker.rb, step:Given ]
[name:first, type:Advocate, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/advocate.rb, step:Given ]
[name:have_content, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:Given ]
[name:index, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:join, type:Object, file:null, step:Then ]
[name:last, type:Object, file:null, step:Then ]
[name:map, type:Object, file:null, step:Then ]
[name:match, type:Object, file:null, step:Then ]
[name:new, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:outstanding, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:page, type:Object, file:null, step:Then ]
[name:save_and_open_page, type:Object, file:null, step:When ]
[name:show, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:show, type:CaseWorkers/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/claims_controller.rb, step:null]
[name:show, type:DocumentsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/documents_controller.rb, step:null]
[name:sign_in, type:UserHelper, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/support/user_helper.rb, step:Given ]
[name:tandcs, type:PagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/pages_controller.rb, step:Given ]
[name:to, type:Object, file:null, step:Then ]
[name:update, type:AdvocatesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/admin/advocates_controller.rb, step:null]
[name:update, type:ClaimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:update, type:CaseWorkersController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/admin/case_workers_controller.rb, step:null]
[name:update, type:ClaimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/claims_controller.rb, step:null]
[name:update, type:UserMessageStatusesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/user_message_statuses_controller.rb, step:null]
[name:user, type:Object, file:null, step:Given ]
[name:within, type:Object, file:null, step:When ]
[name:within, type:Object, file:null, step:Then ]

Referenced pages: 36
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_basic_fee_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_claims.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_cracked_trial_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_date_attended_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_defendant_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_evidence_checklist.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_expense_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_financial_summary.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_fixed_fee_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_form.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_json_document_importer.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_misc_fee_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_offence_select.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_representation_order_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_search_form.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/authorised.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/edit.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/index.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/outstanding.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/show.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/case_workers/claims/show.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/sessions/new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/shared/_links.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/pages/tandcs.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim_header.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim_history.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim_status.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_evidence_checklist.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_evidence_list.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_message.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_messages.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_search_summary.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_summary.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_vat_summary.html.haml

