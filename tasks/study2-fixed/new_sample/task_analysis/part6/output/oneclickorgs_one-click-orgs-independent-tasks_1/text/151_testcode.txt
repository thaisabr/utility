When /^I choose yesterday for the date of election$/ do
  yesterday = 1.day.ago
  select(yesterday.year.to_s, :from => 'director[elected_on(1i)]')
  select(yesterday.strftime('%B'), :from => 'director[elected_on(2i)]')
  select(yesterday.day.to_s, :from => 'director[elected_on(3i)]')
end
When /^I check the certification checkbox$/ do
  check('director[certification]')
end
When /^I add a new director$/ do
  When 'I go to the Directors page'
  And 'I press "Add a new director"'
  And 'I fill in "Email" with "bob@example.com"'
  And 'I fill in "First name" with "Bob"'
  And 'I fill in "Last name" with "Smith"'
  And 'I choose yesterday for the date of election'
  And 'I check the certification checkbox'
  And 'I press "Add this director"'
end
When /^I stand down a director$/ do
  Given 'I am on the Directors page'
  When 'I press "Stand down" for another director'
  When 'I submit the form to stand down the director'
end
Given /^I have received an email inviting me to sign up as a director$/ do
  Given "I have been invited to sign up as a director"
  @email = last_email
end
When /^I follow the invitation link in the email$/ do
  When "I click the link in the email"
end
Then /^a director invitation email should be sent to "([^"]*)"$/ do |email_address|
  @emails = ActionMailer::Base.deliveries.select{|m| m.to == [email_address] && m.body =~ /You have been added as a director/}
  @emails.should_not be_empty
end
Then /^everyone should receive an email notifying them of the proposal$/ do
  @proposal ||= Proposal.last
  @organisation.members.each do |member|
    email = ActionMailer::Base.deliveries.reverse.select{|mail| mail.to.first == member.email}.first
    email.body.should =~ Regexp.new(@proposal.title)
  end
end
Then /^all the directors should receive a "([^"]*)" email$/ do |subject_phrase|
  @organisation.directors.active.each do |director|
    mails = ActionMailer::Base.deliveries.select{|m| m.to.include?(director.email)}
    mails.select{|m| m.subject.include?(subject_phrase)}.should_not be_empty
  end
end
def last_email
  ActionMailer::Base.deliveries.last
end
Given /^the application is set up$/ do
  # Using smackaho.st to give us automatic resolution to localhost
  # 
  # Port needs to be saved for Selenium tests (because our app code considers
  # the port as well as the hostname when figuring out how to handle a
  # request, and the Capybara app server doesn't run on port 80).
  # 
  # When using the rack-test driver, don't use port numbers at all,
  # since it makes our test-session-resetting code (see features/support/capybara_domains.rb)
  # more complicated.
  port_segment = Capybara.current_driver == :selenium ? ":#{Capybara.server_port}" : ''
  
  Setting[:base_domain] = "smackaho.st#{port_segment}"
  Setting[:signup_domain] = "create.smackaho.st#{port_segment}"
end
Given /^a company has been added$/ do
  @organisation = @company = Company.make
end
Given /^the company has directors$/ do
  @company.directors.make_n(2, :member_class => @company.member_classes.find_by_name('Director'))
end
Given /^I am a director of the company$/ do
  @company ||= Company.last
  director_member_class = @company.member_classes.find_by_name('Director')
  @user = @company.members.make(:member_class => director_member_class)
end
Given /^there are two other directors of the company$/ do
  @company ||= Company.last
  director_member_class = @company.member_classes.find_by_name('Director')
  @company.members.make_n(2, :member_class => director_member_class)
end
Given /^the subdomain is the organisation's subdomain$/ do
  Given %Q{the subdomain is "#{@organisation.subdomain}"}
end
Given /^I have logged in$/ do
  visit '/'
  fill_in('Email', :with => @user.email)
  fill_in('Password', :with => @user.password)
  click_button("Login")
end
When /^(.*) within ([^:]+)$/ do |step, parent|
  with_scope(parent) { When step }
end
When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^"]*)"$/ do |button|
  click_button(button)
end
When /^(?:|I )fill in "([^"]*)" with "([^"]*)"$/ do |field, value|
  fill_in(field, :with => value)
end
Then /^(?:|I )should see "([^"]*)"$/ do |text|
  if page.respond_to? :should
    page.should have_content(text)
  else
    assert page.has_content?(text)
  end
end
Then /^(?:|I )should be on (.+)$/ do |page_name|
  current_path = URI.parse(current_url).path
  if current_path.respond_to? :should
    current_path.should == path_to(page_name)
  else
    assert_equal path_to(page_name), current_path
  end
end
  def with_scope(locator)
    locator ? within(*selector_for(locator)) { yield } : yield
  end
  def path_to(page_name)
    case page_name

    when /^the home\s?page$/
      '/'
    when /^the setup page$/
      '/setup'
    when /^the new association page$/
      '/associations/new'
    when /^the welcome page$/
      '/welcome'
    when /^the voting and proposals page$/
      '/'
    when /^the proposal page$/
      @proposal ||= Proposal.last
      proposal_path(@proposal)
    when /^the member page for "(.*)"$/
      @member = @organisation.members.find_by_email($1)
      member_path(@member)
    when /^a member's page$/
      @member = @organisation.members.active.last
      member_path(@member)
    when /^the amendments page$/
      edit_constitution_path
    when /^the members page$/
      members_path
    when /^the new company page$/
      new_company_path
    when /^the Votes & Minutes page$/
      '/'
    when /^the page for the minutes$/
      @meeting ||= @organisation.meeting.last
      meeting_path(@meeting)
    when /^the Directors page$/
      members_path

    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
  def selector_for(locator)
    case locator

    when /the page/
      "html > body"

    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #  when /the (notice|error|info) flash/
    #    ".flash.#{$1}"
    
    when /the list of founding members/
      "table.pending_members"
    when /the list of pending members/
      "table.pending_members"
    when /the "(.+)" proposal/
      @proposal = @organisation.proposals.find_by_title($1)
      "#proposal_#{@proposal.id}"
    when /^the timeline$/
      "table.timeline"
    when /the list of directors/
      "table.members"

    # You can also return an array to use a different selector
    # type, like:
    #
    #  when /the header/
    #    [:xpath, "//header"]

    # This allows you to provide a quoted selector as the scope
    # for "within" steps as was previously the default for the
    # web steps:
    when /"(.+)"/
      $1

    else
      raise "Can't find mapping from \"#{locator}\" to a selector.\n" +
        "Now, go and add a mapping in #{__FILE__}"
    end
  end
Given /^I have been invited to sign up as a director$/ do
  @user ||= @organisation.directors.make
  @user.send_welcome = true
  @user.send_welcome_if_requested
end
When /^I enter a comment of "([^"]*)"$/ do |comment_body|
  fill_in('comment[body]', :with => comment_body)
end
Then /^I should see a comment by me saying "([^"]*)"$/ do |comment_body|
  page.should have_css('.comment p.attribution a', :text => @user.name)
  page.should have_css('.comment p', :text => comment_body)
end
Given /^another founding vote has been started$/ do
  sleep(1)
  founder = @organisation.member_classes.where(:name => "Founder").first.members.first
  @organisation.found_association_proposals.make(:proposer => founder, :title => "A second voting proposal.")
  @organisation.reload
  @organisation.propose!
end
Given /^a proposal "([^"]*)" has been made$/ do |proposal_title|
  @organisation.proposals.make(
    :title => proposal_title,
    :proposer => @organisation.members.active.first
  )
end
When /^the proposal closer runs$/ do
  Proposal.close_proposals
end
When /^enough people vote in support of the proposal$/ do
  @proposal ||= Proposal.last
  (@organisation.members.active - [@proposal.proposer]).each do |member|
    member.cast_vote(:for, @proposal)
  end
end
