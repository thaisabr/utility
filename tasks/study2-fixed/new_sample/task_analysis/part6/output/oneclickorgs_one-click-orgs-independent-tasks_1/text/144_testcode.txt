Given /^I am a director of the company$/ do
  @company ||= Company.last
  director_member_class = @company.member_classes.find_by_name('Director')
  @user = @company.members.make(:member_class => director_member_class)
end
Given /^there are two other directors of the company$/ do
  @company ||= Company.last
  director_member_class = @company.member_classes.find_by_name('Director')
  @company.members.make_n(2, :member_class => director_member_class)
end
Given /^I have been stood down$/ do
  @user.eject!
end
When /^I try to log in$/ do
  Given "I have logged in"
end
Then /^I should not be logged in$/ do
  page.should have_no_content("Logout")
end
Given /^the application is set up$/ do
  # Using smackaho.st to give us automatic resolution to localhost
  # 
  # Port needs to be saved for Selenium tests (because our app code considers
  # the port as well as the hostname when figuring out how to handle a
  # request, and the Capybara app server doesn't run on port 80).
  # 
  # When using the rack-test driver, don't use port numbers at all,
  # since it makes our test-session-resetting code (see features/support/capybara_domains.rb)
  # more complicated.
  port_segment = Capybara.current_driver == :selenium ? ":#{Capybara.server_port}" : ''
  
  Setting[:base_domain] = "smackaho.st#{port_segment}"
  Setting[:signup_domain] = "create.smackaho.st#{port_segment}"
end
Given /^a company has been added$/ do
  @organisation = @company = Company.make
end
Given /^the subdomain is the organisation's subdomain$/ do
  Given %Q{the subdomain is "#{@organisation.subdomain}"}
end
