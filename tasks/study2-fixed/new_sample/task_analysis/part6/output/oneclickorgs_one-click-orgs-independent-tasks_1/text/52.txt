Feature: Viewing draft co-ops
In order to monitor incoming co-op registrations
As an administrator
I want to view details of draft co-ops
Background:
Given I am an administrator
And some draft co-ops have been created
Scenario: Administrator views a list of draft co-ops
When I go to the co-op review page
Then I should see a list of the draft co-ops
Scenario: Administrator views details of a draft co-op
When I go to the admin view of a draft co-op
Then I should see the name of the co-op
And I should see the founder members of the co-op
And I should see a link to the co-op's rules
And I should see a link to the co-op's registration form
And I should see a link to the co-op's anti-money laundering form
Feature: Viewing active co-ops
In order to monitor use of the app
As an administrator
I want to view details of active co-ops
Background:
Given I am an administrator
And there are some active co-ops
Scenario: Administrator views a list of active co-ops
When I go to the co-op review page
Then I should see a list of the active co-ops
Scenario: Administrator views details of a active co-op
When I go to the admin view of an active co-op
Then I should see the name of the co-op
And I should see the directors of the co-op
And I should see the members of the co-op
And I should see a link to the co-op's rules
Feature: Approving co-op registration
In order to allow a co-op to progress
As an administrator
I want approve a co-op registration
Background:
Given I am an administrator
Scenario: Administrator receives an email notification of a new draft co-op
When a draft co-op is created
Then I should receive an email notifying me about the new draft co-op
Scenario: Administrator receives an email notification of a co-op submitted for registration
When a draft co-op is submitted for registration
Then I should receive an email notifying me that the co-op has been submitted for registration
Scenario: Administrator approves a co-op registration
Given a co-op has been submitted
When I go to the co-op review page
And I follow "View full information" for the co-op
And I press "Mark this co-op as fully registered"
Then I should see that the co-op is approved
Feature: Editing submitted co-ops
In order to ensure that co-ops are set up properly for registration
As an administrator
I want to edit the details of submitted co-ops
Background:
Given I am an administrator
And some co-ops have been submitted for registration
Scenario: Administrator edits a submitted co-op's Rules
When I go to the admin view of a proposed co-op
And I follow "Edit Rules"
And I make changes to the rules
And I save the changes
And I follow "View rules"
Then I should see the changes I made
Scenario: Administrator edits a submitted co-op's registration form
When I go to the admin view of a proposed co-op
And I follow "Edit registration details"
And I fill in the close links field with "There are no close links."
And I save the changes
And I follow "Download registration form"
Then the PDF should contain "There are no close links."
Scenario: Administrator edits a submitted co-op's anti-money laundering form
When I go to the admin view of a proposed co-op
And I follow "Edit registration details"
And I fill in the finance contact field with "Milburn Pennybags"
And I save the changes
And I follow "View anti-money laundering form"
Then I should see "Milburn Pennybags"
Feature: Viewing submitted co-ops
In order to process co-op registrations
As an administrator
I want to view the co-ops that have been submitted for registration
Background:
Given I am an administrator
And some co-ops have been submitted for registration
Scenario: Administrator view a list of submitted co-ops
When I go to the co-op review page
Then I should see a list of the submitted co-ops
Scenario: Administrator views details of a proposed co-op
When I go to the admin view of a proposed co-op
Then I should see the name of the co-op
And I should see the founder members of the co-op
And I should see a link to the co-op's rules
And I should see a link to the co-op's registration form
Scenario: Administrator downloads a PDF of a proposed co-op's rules
When I go to the admin view of a proposed co-op
And I follow "Download rules"
Then I should get a ".pdf" download
Scenario: Administrator downloads a PDF of a proposed co-op's registration form
When I go to the admin view of a proposed co-op
And I follow "Download registration form"
Then I should get a ".pdf" download
Scenario: Administrator downloads a PDF of a proposed co-op's anti-money laundering form
When I go to the admin view of a proposed co-op
And I follow "Download anti-money laundering form"
Then I should get a ".pdf" download
Feature: Editing money laundering form
In order to prepare our co-op for registration
As a founder member
I want to edit the money laundering form
Background:
Given there is a draft co-op
And I am a founder member of the draft co-op
And there are at least two directors
Scenario: Founder member enters the main contact info for Co-operatives UK
When I go to edit the registration details
And I enter the main contact info
And I save the registration details
Then I should see the main contact info
Scenario: Founder member enters the financial contact info for Co-operatives UK
When I go to edit the registration details
And I enter the financial contact info
And I save the registration details
Then I should see the financial contact info
Scenario: Founder member enters details for the money laundering contacts
When I go to edit the registration details
And I enter details for the two money laundering contacts
And I save the registration details
Then I should see the details for the two money laundering contacts
Scenario: Founder member confirms agreement with the letter of engagement and money laundering form
When I go to edit the registration details
And I check "I have read and agree to the Letter of Engagement and Anti-Money Laundering requirements."
And I save the registration details
Then I should see the agreement checkbox is checked
Feature: Editing registration form
In order to prepare our co-op for registration
As a founder member
I want to edit the registration form
Scenario: Founder member chooses three signatories
Given there is a draft co-op
And I am the founder of the draft co-op
And there are at least three founder members
When I go to edit the registration form
And I choose three signatories
And I save the registration form
Then I should see the three signatories I chose
Feature: Viewing letter of engagement
In order to prepare our co-op for registration
As a founder member
I want to view the Co-operatives UK letter of engagement
Scenario: Founder Members view the letter of engagement
Given there is a draft co-op
And I am a founder member of the draft co-op
When I go to Documents
And I follow "Co-operatives UK Letter of Engagement"
Then I should get a ".pdf" download
Feature: Viewing money laundering form
In order to prepare our co-op for registration
As a founder member
I want to view the Co-operatives UK money laundering form
Scenario: Founder member views the money laundering form
Given there is a draft co-op
And I am a founder member of the draft co-op
And the money laundering form has been filled in
When I go to Documents
And I follow "Co-operatives UK Anti-Money Laundering Form"
Then I should get a ".pdf" download
