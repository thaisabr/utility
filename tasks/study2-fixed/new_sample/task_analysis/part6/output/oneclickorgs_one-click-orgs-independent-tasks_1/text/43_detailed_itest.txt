Classes: 6
[name:Administrator, file:oneclickorgs_one-click-orgs/app/models/administrator.rb, step:Given ]
[name:Capybara, file:null, step:Given ]
[name:Coop, file:oneclickorgs_one-click-orgs/app/models/coop.rb, step:Given ]
[name:Member, file:oneclickorgs_one-click-orgs/app/models/member.rb, step:null]
[name:Member, file:oneclickorgs_one-click-orgs/app/models/member.rb, step:Then ]
[name:Setting, file:oneclickorgs_one-click-orgs/app/models/setting.rb, step:Given ]

Methods: 56
[name:all, type:Member, file:oneclickorgs_one-click-orgs/app/models/member.rb, step:Then ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:consumer_members, type:ConstitutionWrapper, file:oneclickorgs_one-click-orgs/app/models/constitution_wrapper.rb, step:null]
[name:consumer_members_description, type:ConstitutionWrapper, file:oneclickorgs_one-click-orgs/app/models/constitution_wrapper.rb, step:null]
[name:each, type:Object, file:null, step:Given ]
[name:each, type:Object, file:null, step:Then ]
[name:edit, type:Admin/membersController, file:oneclickorgs_one-click-orgs/app/controllers/admin/members_controller.rb, step:null]
[name:edit, type:Admin/constitutionsController, file:oneclickorgs_one-click-orgs/app/controllers/admin/constitutions_controller.rb, step:null]
[name:edit, type:Admin/registrationFormsController, file:oneclickorgs_one-click-orgs/app/controllers/admin/registration_forms_controller.rb, step:null]
[name:email, type:Member, file:oneclickorgs_one-click-orgs/app/models/member.rb, step:null]
[name:email, type:AddMemberProposal, file:oneclickorgs_one-click-orgs/app/models/add_member_proposal.rb, step:Then ]
[name:email, type:Object, file:null, step:Given ]
[name:employee_members, type:ConstitutionWrapper, file:oneclickorgs_one-click-orgs/app/models/constitution_wrapper.rb, step:null]
[name:fill_in, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:first_name, type:Member, file:oneclickorgs_one-click-orgs/app/models/member.rb, step:null]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_css, type:Object, file:null, step:Then ]
[name:have_no_css, type:Object, file:null, step:Then ]
[name:index, type:Admin/membersController, file:oneclickorgs_one-click-orgs/app/controllers/admin/members_controller.rb, step:null]
[name:index, type:AdminController, file:oneclickorgs_one-click-orgs/app/controllers/admin_controller.rb, step:Given ]
[name:last_name, type:Member, file:oneclickorgs_one-click-orgs/app/models/member.rb, step:null]
[name:make!, type:Coop, file:oneclickorgs_one-click-orgs/app/models/coop.rb, step:Given ]
[name:make!, type:Object, file:null, step:Given ]
[name:make!, type:Administrator, file:oneclickorgs_one-click-orgs/app/models/administrator.rb, step:Given ]
[name:max_consumer_directors, type:ConstitutionWrapper, file:oneclickorgs_one-click-orgs/app/models/constitution_wrapper.rb, step:null]
[name:max_employee_directors, type:ConstitutionWrapper, file:oneclickorgs_one-click-orgs/app/models/constitution_wrapper.rb, step:null]
[name:max_producer_directors, type:ConstitutionWrapper, file:oneclickorgs_one-click-orgs/app/models/constitution_wrapper.rb, step:null]
[name:max_supporter_directors, type:ConstitutionWrapper, file:oneclickorgs_one-click-orgs/app/models/constitution_wrapper.rb, step:null]
[name:max_user_directors, type:ConstitutionWrapper, file:oneclickorgs_one-click-orgs/app/models/constitution_wrapper.rb, step:null]
[name:members, type:Object, file:null, step:Given ]
[name:name, type:ChangeBooleanProposal, file:oneclickorgs_one-click-orgs/app/models/change_boolean_proposal.rb, step:Then ]
[name:name, type:ChangeBooleanResolution, file:oneclickorgs_one-click-orgs/app/models/change_boolean_resolution.rb, step:Then ]
[name:name, type:ChangeIntegerResolution, file:oneclickorgs_one-click-orgs/app/models/change_integer_resolution.rb, step:Then ]
[name:name, type:ChangeTextProposal, file:oneclickorgs_one-click-orgs/app/models/change_text_proposal.rb, step:Then ]
[name:name, type:ChangeTextResolution, file:oneclickorgs_one-click-orgs/app/models/change_text_resolution.rb, step:Then ]
[name:name, type:Member, file:oneclickorgs_one-click-orgs/app/models/member.rb, step:Then ]
[name:name, type:Organisation, file:oneclickorgs_one-click-orgs/app/models/organisation.rb, step:Then ]
[name:page, type:Object, file:null, step:Then ]
[name:password, type:Object, file:null, step:Given ]
[name:producer_members, type:ConstitutionWrapper, file:oneclickorgs_one-click-orgs/app/models/constitution_wrapper.rb, step:null]
[name:producer_members_description, type:ConstitutionWrapper, file:oneclickorgs_one-click-orgs/app/models/constitution_wrapper.rb, step:null]
[name:save changes, type:Object, file:null, step:null]
[name:set_domain, type:WebStepsCustom, file:oneclickorgs_one-click-orgs/features/step_definitions/web_steps_custom.rb, step:Given ]
[name:set_subdomain_to_signup_subdomain, type:WebStepsCustom, file:oneclickorgs_one-click-orgs/features/step_definitions/web_steps_custom.rb, step:Given ]
[name:set_up_application, type:WorldExtensions, file:oneclickorgs_one-click-orgs/features/support/world_extensions.rb, step:Given ]
[name:set_up_application_if_necessary, type:WorldExtensions, file:oneclickorgs_one-click-orgs/features/support/world_extensions.rb, step:Given ]
[name:show, type:Admin/coopsController, file:oneclickorgs_one-click-orgs/app/controllers/admin/coops_controller.rb, step:null]
[name:show, type:Admin/constitutionsController, file:oneclickorgs_one-click-orgs/app/controllers/admin/constitutions_controller.rb, step:null]
[name:show, type:Admin/registrationFormsController, file:oneclickorgs_one-click-orgs/app/controllers/admin/registration_forms_controller.rb, step:null]
[name:show, type:Admin/documentsController, file:oneclickorgs_one-click-orgs/app/controllers/admin/documents_controller.rb, step:null]
[name:supporter_members, type:ConstitutionWrapper, file:oneclickorgs_one-click-orgs/app/models/constitution_wrapper.rb, step:null]
[name:to, type:Object, file:null, step:Then ]
[name:user_members, type:ConstitutionWrapper, file:oneclickorgs_one-click-orgs/app/models/constitution_wrapper.rb, step:null]

Referenced pages: 9
oneclickorgs_one-click-orgs/app/views/admin/constitutions/edit.html.haml
oneclickorgs_one-click-orgs/app/views/admin/constitutions/show.html.haml
oneclickorgs_one-click-orgs/app/views/admin/coops/show.html.haml
oneclickorgs_one-click-orgs/app/views/admin/index.html.haml
oneclickorgs_one-click-orgs/app/views/admin/members/edit.html.haml
oneclickorgs_one-click-orgs/app/views/admin/members/index.html.haml
oneclickorgs_one-click-orgs/app/views/admin/registration_forms/edit.html.haml
oneclickorgs_one-click-orgs/app/views/registration_forms/_form.html.haml
oneclickorgs_one-click-orgs/app/views/shared/coop/_constitution.html.haml

