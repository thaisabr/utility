When(/^(?:|I )go to (.+)$/) do |page_name|
  visit path_to(page_name)
end
When(/^(?:|I )press "([^"]*)"$/) do |button|
  click_button(button)
end
When(/^(?:|I )follow "([^"]*)"$/) do |link|
  click_link(link)
end
When(/^(?:|I )check "([^"]*)"$/) do |field|
  check(field)
end
Then(/^(?:|I )should see "([^"]*)"$/) do |text|
  if page.respond_to? :should
    page.should have_content(text)
  else
    assert page.has_content?(text)
  end
end
  def path_to(page_name)
    case page_name

    when /^the home\s?page$/
      '/'
    when /^the setup page$/
      '/setup'
    when /^the new association page$/
      '/associations/new'
    when /^the new co-op page$/
      '/coops/new'
    when /^the welcome page$/
      '/welcome'
    when /^the voting and proposals page$/
      '/'
    when /^my member page$/
      member_path(@user)
    when /^my account page$/
      edit_member_path(@user)
    when /^the proposal page$/
      @proposal ||= Proposal.last
      proposal_path(@proposal)
    when /^the member page for "(.*)"$/
      @member = @organisation.members.find_by_email($1)
      member_path(@member)
    when /^a member's page$/
      @member = @organisation.members.active.last
      member_path(@member)
    when /^the member's profile page$/
      @member ||= @organisation.members.active.last
      member_path(@member)
    when /^the amendments page$/
      edit_constitution_path
    when /^the members page$/
      members_path
    when /^the new company page$/
      new_company_path
    when /^the Votes & Minutes page$/
      '/'
    when /^the page for the minutes$/
      @meeting ||= @organisation.meeting.last
      meeting_path(@meeting)
    when /^the Directors page$/
      case @organisation
      when Company
        members_path
      when Coop
        directors_path
      end
    when /^the (D|d)ashboard( page)?$/
      '/'
    when /^the Resolutions page$/
      proposals_path
    when /^the "Convene a General Meeting" page$/
      new_general_meeting_path
    when /^the Meetings page$/
      meetings_path
    when /^the Members page$/
      members_path
    when /^another member's profile page$/
      @member = (@organisation.members - [@user]).first
      member_path(@member)
    when /^convene a General Meeting$/
      new_general_meeting_path
    when /^the Rules page$/
      constitution_path
    when /^convene an AGM$/
      new_general_meeting_path
    when /^the (D|d)ashboard(| page) for the (new|draft) co-op$/
      root_path
    when /^the Amendments page$/
      edit_constitution_path
    when /^the co-op review page$/
      admin_coops_path
    when /^the Shares page$/
      '/shares'
    when /^the Checklist page$/
      '/checklist'
    when /^the Proposals page$/
      '/proposals'
    when /^the Board page$/
      '/board'
    when /^edit the registration form$/
      edit_registration_form_path
    when /^the checklist$/
      checklist_path
    when /^the admin view of a proposed co-op$/
      @coop ||= (@coops.present? ? @coops[0] : Coop.proposed.first)
      admin_coop_path(@coop)
    when /^the admin view of a draft co-op$/
      @coop ||= (@coops.present? ? @coops[0] : Coop.pending.first)
      admin_coop_path(@coop)
    when /^the admin view of an active co-op$/
      @coop ||= (@coops.present? ? @coops[0] : Coop.active.first)
      admin_coop_path(@coop)
    when /^edit the registration details$/
      edit_registration_form_path
    when /^Documents$/
      documents_path

    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /^the (.*) page$/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue NoMethodError, ArgumentError
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Given(/^a co\-op has been submitted$/) do
  @coop = @organisation = Coop.make!(:proposed)
  @coop.members.make!
end
Given(/^some co\-ops have been submitted for registration$/) do
  @coops = Coop.make!(2, :proposed)
  @coops.each do |coop|
    coop.members.make!(:secretary)
    coop.members.make!(2, :director)
  end
end
Given(/^some draft co\-ops have been created$/) do
  @coops = Coop.make!(2, :pending)
  @coops.each do |coop|
    coop.members.make!(:secretary)
    coop.members.make!(2, :director)
  end
end
Given(/^there are some active co\-ops$/) do
  @coops = Coop.make!(2)
  @coops.each do |coop|
    coop.members.make!(2)
    coop.members.make!(2, :director)
    coop.members.make!(:secretary)
  end
end
When(/^I follow "(.*?)" for the co\-op$/) do |link|
  @coop ||= Coop.proposed.last
  within("#coop_#{@coop.id}") do
    click_link(link)
  end
end
Then(/^I should see that the co\-op is approved$/) do
  @coop ||= Coop.active.last
  within('.active_coops') do
    page.should have_content(@coop.name)
  end
end
Then(/^I should see a list of the submitted co\-ops$/) do
  @coops ||= Coop.proposed

  within('.proposed_coops') do
    @coops.each do |coop|
      page.should have_content(coop.name)
    end
  end
end
Then(/^I should see a list of the draft co\-ops$/) do
  @coops ||= Coop.pending

  within('.pending_coops') do
    @coops.each do |coop|
      expect(page).to have_content(coop.name)
    end
  end
end
Then(/^I should see a list of the active co\-ops$/) do
  @coops ||= Coop.active
  within('.active_coops') do
    @coops.each do |coop|
      expect(page).to have_content(coop.name)
    end
  end
end
Then(/^I should see the name of the co\-op$/) do
  page.should have_content(@coop.name)
end
Then(/^I should see the (?:|founder )members of the co\-op$/) do
  @coop.members.count.should >= 1

  @coop.members.each do |member|
    page.should have_content(member.name)
    page.should have_content(member.email)
    page.should have_content(member.phone)
    page.should have_content(member.address)
  end
end
Then(/^I should see the directors of the co\-op$/) do
  expect(@coop.directors.count).to be >= 1

  within('.directors') do
    @coop.directors.each do |director|
      page.should have_content(director.name)
    end
  end
end
Then(/^I should see a link to the co\-op's rules$/) do
  url = admin_constitution_path(@coop, :format => :pdf)
  page.should have_css("a[href='#{url}']")
end
Then(/^I should see a link to the co\-op's registration form$/) do
  url = admin_registration_form_path(@coop, :format => :pdf)
  page.should have_css("a[href='#{url}']")
end
Then(/^I should see a link to the co\-op's anti\-money laundering form$/) do
  url = admin_coop_document_path(:coop_id => @coop, :id => 'money_laundering', :format => :pdf)
  page.should have_css("a[href='#{url}']")
end
Given(/^I am an administrator$/) do
  set_up_application_if_necessary

  @user = Administrator.make!

  set_subdomain_to_signup_subdomain

  visit '/admin'
  fill_in('Email', :with => @user.email)
  fill_in('Password', :with => @user.password)
  click_button("Login")
end
  def set_up_application_if_necessary
    unless Setting[:base_domain] && Setting[:signup_domain]
      set_up_application
    end
  end
def set_subdomain_to_signup_subdomain
  set_domain(Setting[:signup_domain])
end
  def set_up_application
    # *.ocolocalhost.com resolves to 127.0.0.1. This lets us test subdomain
    # look-up using domains like 'company.ocolocalhost.com' and
    # 'association.ocolocalhost.com', without having to set up local wildcard
    # entries on each developer's machine and on the CI server.
    #
    # N.B. This means some Cucumber scenarios will fail if your machine
    # isn't connected to the internet. We shoud probably fix this.
    #
    # Port needs to be saved for Selenium tests (because our app code considers
    # the port as well as the hostname when figuring out how to handle a
    # request, and the Capybara app server doesn't run on port 80).
    #
    # When using the rack-test driver, don't use port numbers at all,
    # since it makes our test-session-resetting code (see features/support/capybara_domains.rb)
    # more complicated.
    port_segment = Capybara.current_driver == :selenium ? ":#{Capybara.server_port}" : ''

    Setting[:base_domain] = "ocolocalhost.com#{port_segment}"
    Setting[:signup_domain] = "create.ocolocalhost.com#{port_segment}"
  end
def set_domain(domain)
  if Capybara.current_driver == :selenium
    Capybara.default_host = "http://#{domain}:#{Capybara.server_port}"
    Capybara.app_host = "http://#{domain}:#{Capybara.server_port}"
  else
    Capybara.default_host = "http://#{domain}:#{Capybara.server_port}"
    Capybara.app_host = "http://#{domain}"
  end
end
Given(/^there is a draft co\-op$/) do
  set_up_application_if_necessary

  @coop = @organisation = Coop.make!(:pending)

  founder = @coop.members.make!(:founder_member)

  set_subdomain_to_organisation
end
When(/^a draft co\-op is created$/) do
  @coop = Coop.make!(:pending)
end
When(/^a draft co\-op is submitted for registration$/) do
  @coop = Coop.make!(:pending)
  @coop.propose!
end
def set_subdomain_to_organisation
  set_subdomain(@organisation.subdomain)
end
def set_subdomain(subdomain)
  domain = "#{subdomain}.#{Setting[:base_domain].sub(/:\d+$/, '')}"
  set_domain(domain)
end
Then(/^I should receive an email notifying me about the new draft co\-op$/) do
  @coop ||= Coop.pending.last

  @email = last_email_to(@user.email)
  @email.should be_present
  @email.subject.should include(@coop.name)
  @email.subject.should include('new draft')
end
Then(/^I should receive an email notifying me that the co\-op has been submitted for registration$/) do
  @coop ||= Coop.proposed.last

  @email = last_email_to(@user.email)
  @email.should be_present
  @email.subject.should include(@coop.name)
  @email.subject.should include('submitted')
end
def last_email_to(email_address)
  ActionMailer::Base.deliveries.select{|e| e.to.include?(email_address)}.last
end
When(/^I make changes to the rules$/) do
  fill_in('constitution[organisation_name]', :with => "The Tea Co-op")
  fill_in('constitution[registered_office_address]', :with => "1 Main Street")
  fill_in('constitution[objectives]', :with => "buy all the tea.")
  check("constitution[user_members]")
  check("constitution[employee_members]")
  uncheck("constitution[supporter_members]")
  uncheck("constitution[producer_members]")
  uncheck("constitution[consumer_members]")
  choose("constitution_single_shareholding_1")
  %w{user employee supporter producer consumer}.each do |member_type|
    fill_in("constitution[max_#{member_type}_directors]", :with => '2')
  end
  choose("constitution_common_ownership_1")
end
When(/^I save the changes$/) do
  click_button("Save changes")
end
Then(/^I should see the changes I made$/) do
  page.should have_content("The Tea Co-op")
  page.should have_content("1 Main Street")
  page.should have_content("buy all the tea.")
  page.should have_css("h3", :text => "User Members")
  page.should have_css("h3", :text => "Employee Members")
  page.should have_no_css("h3", :text => "Supporter Members")
  page.should have_no_css("h3", :text => "Producer Members")
  page.should have_no_css("h3", :text => "Consumer Members")
  page.should have_content("Each Member shall hold one share only in the Co operative")
  page.should have_content("2 User Members")
  page.should have_content("2 Employee Members")
  page.should have_content("The Co-operative is a common ownership enterprise")
end
Given(/^the requirements for registration have been fulfilled$/) do
  @organisation.members.make!(3, :director)
  @organisation.members.make!(:secretary)

  @organisation.name ||= Faker::Company.name
  @organisation.registered_office_address ||= "#{Faker::Address.street_address}\n#{Faker::Address.city}\n#{Faker::Address.zip_code}"
  @organisation.objectives ||= Faker::Company.bs

  @organisation.signatories = @organisation.members.all[0..2]

  @organisation.reg_form_main_contact_name = "Bob Smith"
  @organisation.reg_form_main_contact_address = "1 Main Street\nLondon\nN1 1AA"
  @organisation.reg_form_main_contact_phone = "01234 567 890"
  @organisation.reg_form_main_contact_email = "bob@example.com"

  @organisation.reg_form_money_laundering_0_name = "Bob Smith"
  @organisation.reg_form_money_laundering_0_date_of_birth = "1 January 1970"
  @organisation.reg_form_money_laundering_0_address = "1 Main Street"
  @organisation.reg_form_money_laundering_0_postcode = "N1 1AA"
  @organisation.reg_form_money_laundering_0_residency_length = "6 years"

  @organisation.reg_form_money_laundering_1_name = "Jane Baker"
  @organisation.reg_form_money_laundering_1_date_of_birth = "1 May 1980"
  @organisation.reg_form_money_laundering_1_address = "40 High Street"
  @organisation.reg_form_money_laundering_1_postcode = "SW1 1AA"
  @organisation.reg_form_money_laundering_1_residency_length = "15 years"

  @organisation.reg_form_money_laundering_agreement = true

  @organisation.save!
end
Given(/^the money laundering form has been filled in$/) do
  @organisation ||= Coop.pending.last

  @organisation.reg_form_main_contact_name = "Bob Smith"
  @organisation.reg_form_main_contact_address = "1 Main Street\nLondon\nN1 1AA"
  @organisation.reg_form_main_contact_phone = "01234 567 890"
  @organisation.reg_form_main_contact_email = "bob@example.com"

  @organisation.reg_form_money_laundering_0_name = "Bob Smith"
  @organisation.reg_form_money_laundering_0_date_of_birth = "1 January 1970"
  @organisation.reg_form_money_laundering_0_address = "1 Main Street"
  @organisation.reg_form_money_laundering_0_postcode = "N1 1AA"
  @organisation.reg_form_money_laundering_0_residency_length = "6 years"

  @organisation.reg_form_money_laundering_1_name = "Jane Baker"
  @organisation.reg_form_money_laundering_1_date_of_birth = "1 May 1980"
  @organisation.reg_form_money_laundering_1_address = "40 High Street"
  @organisation.reg_form_money_laundering_1_postcode = "SW1 1AA"
  @organisation.reg_form_money_laundering_1_residency_length = "15 years"

  @organisation.reg_form_money_laundering_agreement = true

  @organisation.save!
end
When(/^I choose three signatories$/) do
  @coop ||= Coop.pending.last

  @signatories = @coop.founder_members.all[0..2]

  @signatories.each do |signatory|
    check(signatory.name)
  end
end
When(/^I enter the main contact info$/) do
  fill_in('registration_form[reg_form_main_contact_organisation_name]', :with => "Acme Ltd")
  fill_in('registration_form[reg_form_main_contact_name]', :with => "Bob Smith")
  fill_in('registration_form[reg_form_main_contact_address]', :with => "1 Main Street\nLondon\nN1 1AA")
  fill_in('registration_form[reg_form_main_contact_phone]', :with => '01234 567 890')
  fill_in('registration_form[reg_form_main_contact_email]', :with => 'bob@example.com')
end
When(/^I enter the financial contact info$/) do
  fill_in('registration_form[reg_form_financial_contact_name]', :with => "Jane Baker")
  fill_in('registration_form[reg_form_financial_contact_phone]', :with => '020 7777 7777')
  fill_in('registration_form[reg_form_financial_contact_email]', :with => 'jane@example.com')
end
When(/^I enter details for the two money laundering contacts$/) do
  fill_in('registration_form[reg_form_money_laundering_0_name]', :with => "Bob Smith")
  fill_in('registration_form[reg_form_money_laundering_0_date_of_birth]', :with => "1 January 1970")
  fill_in('registration_form[reg_form_money_laundering_0_address]', :with => "1 Main Street")
  fill_in('registration_form[reg_form_money_laundering_0_postcode]', :with => "N1 1AA")
  fill_in('registration_form[reg_form_money_laundering_0_residency_length]', :with => "6 years")

  fill_in('registration_form[reg_form_money_laundering_1_name]', :with => "Jane Baker")
  fill_in('registration_form[reg_form_money_laundering_1_date_of_birth]', :with => "1 May 1980")
  fill_in('registration_form[reg_form_money_laundering_1_address]', :with => "40 High Street")
  fill_in('registration_form[reg_form_money_laundering_1_postcode]', :with => "SW1 1AA")
  fill_in('registration_form[reg_form_money_laundering_1_residency_length]', :with => "15 years")
end
When(/^I save the registration (?:form|details)$/) do
  click_button("Save changes")
end
When(/^I fill in the close links field with "(.*?)"$/) do |value|
  fill_in('registration_form[reg_form_close_links]', :with => value)
end
When(/^I fill in the finance contact field with "(.*?)"$/) do |value|
  fill_in('registration_form[reg_form_financial_contact_name]', :with => value)
end
Then(/^I should see the main contact info$/) do
  expect(find_field('registration_form[reg_form_main_contact_organisation_name]').value).to eq("Acme Ltd")
  expect(find_field('registration_form[reg_form_main_contact_name]').value).to eq("Bob Smith")
  expect(find_field('registration_form[reg_form_main_contact_address]').value).to eq("1 Main Street\nLondon\nN1 1AA")
  expect(find_field('registration_form[reg_form_main_contact_phone]').value).to eq("01234 567 890")
  expect(find_field('registration_form[reg_form_main_contact_email]').value).to eq("bob@example.com")
end
Then(/^I should see the financial contact info$/) do
  expect(find_field('registration_form[reg_form_financial_contact_name]').value).to eq("Jane Baker")
  expect(find_field('registration_form[reg_form_financial_contact_phone]').value).to eq("020 7777 7777")
  expect(find_field('registration_form[reg_form_financial_contact_email]').value).to eq("jane@example.com")
end
Then(/^I should see the details for the two money laundering contacts$/) do
  expect(find_field('registration_form[reg_form_money_laundering_0_name]').value).to eq("Bob Smith")
  expect(find_field('registration_form[reg_form_money_laundering_0_date_of_birth]').value).to eq("1 January 1970")
  expect(find_field('registration_form[reg_form_money_laundering_0_address]').value).to eq("1 Main Street")
  expect(find_field('registration_form[reg_form_money_laundering_0_postcode]').value).to eq("N1 1AA")
  expect(find_field('registration_form[reg_form_money_laundering_0_residency_length]').value).to eq("6 years")

  expect(find_field('registration_form[reg_form_money_laundering_1_name]').value).to eq("Jane Baker")
  expect(find_field('registration_form[reg_form_money_laundering_1_date_of_birth]').value).to eq("1 May 1980")
  expect(find_field('registration_form[reg_form_money_laundering_1_address]').value).to eq("40 High Street")
  expect(find_field('registration_form[reg_form_money_laundering_1_postcode]').value).to eq("SW1 1AA")
  expect(find_field('registration_form[reg_form_money_laundering_1_residency_length]').value).to eq("15 years")
end
Then(/^I should see the agreement checkbox is checked$/) do
  page.should have_css("input[name='registration_form[reg_form_money_laundering_agreement]'][checked=checked]")
end
Then(/^I should see the three signatories I chose$/) do
  @signatories ||= @coop.founder_members.all[0..2]

  @signatories.each do |signatory|
    expect(find_field(signatory.name)['checked']).to be_true
  end
end
Then(/^the PDF should contain "(.*?)"$/) do |text|
  reader = PDF::Reader.new(StringIO.new(page.source, 'rb'))
  content = reader.pages.map(&:text).join(' ')
  expect(content).to include(text)
end
Then(/^I should get a "(.*?)" download$/) do |extension|
  page.response_headers['Content-Disposition'].should =~ Regexp.new("filename=\".*\.#{Regexp.escape(extension)}\"")
end
Given(/^I am the founder of the draft co\-op$/) do
  @organisation ||= Coop.pending.last
  if @organisation.members.founder_members(@organisation).first
    @user = @organisation.members.founder_members(@organisation).first
    @user.password = @user.password_confirmation = "password"
    @user.save!
  else
    @user = @organisation.members.make!(:founder_member)
  end
  user_logs_in
end
Given(/^I am a (?:founding|founder) member of the draft co\-op$/) do
  @organsation ||= Coop.pending.last
  @user = @organisation.members.make!(:founder_member)
  user_logs_in
end
Given(/^there are at least three founder members$/) do
  @coop ||= Coop.pending.last
  founder_member_deficit = 3 - @coop.founder_members.count
  founder_member_deficit.times do
    @coop.members.make!(:founder_member)
  end
end
def user_logs_in
  visit '/'
  fill_in('Email', :with => @user.email)
  fill_in('Password', :with => @user.password)
  click_button("Login")
end
Given(/^there are at least two directors$/) do
  @organisation.members.make!(2, :director)
end
