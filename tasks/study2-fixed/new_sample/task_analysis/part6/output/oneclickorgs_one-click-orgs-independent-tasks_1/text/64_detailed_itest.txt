Classes: 23
[name:ActionMailer, file:null, step:Then ]
[name:Administrator, file:oneclickorgs_one-click-orgs/app/models/administrator.rb, step:Given ]
[name:AnnualGeneralMeeting, file:oneclickorgs_one-click-orgs/app/models/annual_general_meeting.rb, step:Given ]
[name:AnnualGeneralMeetingsController, file:oneclickorgs_one-click-orgs/app/controllers/annual_general_meetings_controller.rb, step:Given ]
[name:BoardMeeting, file:oneclickorgs_one-click-orgs/app/models/board_meeting.rb, step:Given ]
[name:BoardMeetingsController, file:oneclickorgs_one-click-orgs/app/controllers/board_meetings_controller.rb, step:Given ]
[name:Capybara, file:null, step:Given ]
[name:Comment, file:oneclickorgs_one-click-orgs/app/models/comment.rb, step:Given ]
[name:CommentsController, file:oneclickorgs_one-click-orgs/app/controllers/comments_controller.rb, step:Given ]
[name:Coop, file:oneclickorgs_one-click-orgs/app/models/coop.rb, step:Given ]
[name:Coop, file:oneclickorgs_one-click-orgs/app/models/coop.rb, step:When ]
[name:Coop, file:oneclickorgs_one-click-orgs/app/models/coop.rb, step:Then ]
[name:Director, file:oneclickorgs_one-click-orgs/app/models/director.rb, step:Given ]
[name:DirectorsController, file:oneclickorgs_one-click-orgs/app/controllers/directors_controller.rb, step:Given ]
[name:Election, file:oneclickorgs_one-click-orgs/app/models/election.rb, step:Given ]
[name:GeneralMeeting, file:oneclickorgs_one-click-orgs/app/models/general_meeting.rb, step:Given ]
[name:GeneralMeetingsController, file:oneclickorgs_one-click-orgs/app/controllers/general_meetings_controller.rb, step:Given ]
[name:Member, file:oneclickorgs_one-click-orgs/app/models/member.rb, step:Given ]
[name:MembersController, file:oneclickorgs_one-click-orgs/app/controllers/members_controller.rb, step:Given ]
[name:Minute, file:oneclickorgs_one-click-orgs/app/models/minute.rb, step:Given ]
[name:MinutesController, file:oneclickorgs_one-click-orgs/app/controllers/minutes_controller.rb, step:Given ]
[name:Organisation, file:oneclickorgs_one-click-orgs/app/models/organisation.rb, step:Given ]
[name:Setting, file:oneclickorgs_one-click-orgs/app/models/setting.rb, step:Given ]

Methods: 111
[name:active, type:Coop, file:oneclickorgs_one-click-orgs/app/models/coop.rb, step:Then ]
[name:add this director, type:Object, file:null, step:Given ]
[name:be_present, type:Object, file:null, step:Then ]
[name:class, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:When ]
[name:confirm_eject, type:MembersController, file:oneclickorgs_one-click-orgs/app/controllers/members_controller.rb, step:Given ]
[name:create proposal to eject this member, type:Object, file:null, step:Given ]
[name:dashboard, type:OneClickController, file:oneclickorgs_one-click-orgs/app/controllers/one_click_controller.rb, step:Given ]
[name:description, type:AnnualGeneralMeeting, file:oneclickorgs_one-click-orgs/app/models/annual_general_meeting.rb, step:Given ]
[name:description, type:BoardMeeting, file:oneclickorgs_one-click-orgs/app/models/board_meeting.rb, step:Given ]
[name:description, type:GeneralMeeting, file:oneclickorgs_one-click-orgs/app/models/general_meeting.rb, step:Given ]
[name:description, type:VotingSystems, file:oneclickorgs_one-click-orgs/app/models/voting_systems.rb, step:Given ]
[name:domain, type:Organisation, file:oneclickorgs_one-click-orgs/app/models/organisation.rb, step:null]
[name:edit, type:GeneralMeetingsController, file:oneclickorgs_one-click-orgs/app/controllers/general_meetings_controller.rb, step:Given ]
[name:edit, type:AnnualGeneralMeetingsController, file:oneclickorgs_one-click-orgs/app/controllers/annual_general_meetings_controller.rb, step:Given ]
[name:edit, type:BoardMeetingsController, file:oneclickorgs_one-click-orgs/app/controllers/board_meetings_controller.rb, step:Given ]
[name:email, type:Object, file:null, step:Then ]
[name:email, type:Object, file:null, step:Given ]
[name:email, type:Member, file:oneclickorgs_one-click-orgs/app/models/member.rb, step:Given ]
[name:email, type:AddMemberProposal, file:oneclickorgs_one-click-orgs/app/models/add_member_proposal.rb, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:first_name, type:Member, file:oneclickorgs_one-click-orgs/app/models/member.rb, step:Given ]
[name:gravatar_url, type:Object, file:null, step:Given ]
[name:happened_on, type:Object, file:null, step:Given ]
[name:have_content, type:Object, file:null, step:Then ]
[name:id, type:Coop, file:oneclickorgs_one-click-orgs/app/models/coop.rb, step:When ]
[name:id, type:Nomination, file:oneclickorgs_one-click-orgs/app/models/nomination.rb, step:Given ]
[name:id, type:GeneralMeeting, file:oneclickorgs_one-click-orgs/app/models/general_meeting.rb, step:Given ]
[name:id, type:Minute, file:oneclickorgs_one-click-orgs/app/models/minute.rb, step:Given ]
[name:id, type:BoardMeeting, file:oneclickorgs_one-click-orgs/app/models/board_meeting.rb, step:Given ]
[name:include, type:Object, file:null, step:Then ]
[name:include?, type:Object, file:null, step:Then ]
[name:index, type:AdminController, file:oneclickorgs_one-click-orgs/app/controllers/admin_controller.rb, step:Given ]
[name:index, type:Admin/coopsController, file:oneclickorgs_one-click-orgs/app/controllers/admin/coops_controller.rb, step:null]
[name:index, type:MeetingsController, file:oneclickorgs_one-click-orgs/app/controllers/meetings_controller.rb, step:Given ]
[name:index, type:ProposalsController, file:oneclickorgs_one-click-orgs/app/controllers/proposals_controller.rb, step:Given ]
[name:index, type:MembersController, file:oneclickorgs_one-click-orgs/app/controllers/members_controller.rb, step:Given ]
[name:inducted_at, type:Object, file:null, step:Given ]
[name:last, type:Object, file:null, step:Given ]
[name:last, type:Object, file:null, step:When ]
[name:last, type:Object, file:null, step:Then ]
[name:last_email_to, type:EmailSteps, file:oneclickorgs_one-click-orgs/features/step_definitions/email_steps.rb, step:Then ]
[name:last_logged_in_at, type:Object, file:null, step:Given ]
[name:last_name, type:Member, file:oneclickorgs_one-click-orgs/app/models/member.rb, step:Given ]
[name:make!, type:Coop, file:oneclickorgs_one-click-orgs/app/models/coop.rb, step:When ]
[name:make!, type:Administrator, file:oneclickorgs_one-click-orgs/app/models/administrator.rb, step:Given ]
[name:make!, type:Coop, file:oneclickorgs_one-click-orgs/app/models/coop.rb, step:Given ]
[name:make!, type:Object, file:null, step:Given ]
[name:members, type:Object, file:null, step:null]
[name:minuted?, type:Meeting, file:oneclickorgs_one-click-orgs/app/models/meeting.rb, step:Given ]
[name:name, type:Coop, file:oneclickorgs_one-click-orgs/app/models/coop.rb, step:Then ]
[name:name, type:ChangeBooleanProposal, file:oneclickorgs_one-click-orgs/app/models/change_boolean_proposal.rb, step:null]
[name:name, type:ChangeBooleanResolution, file:oneclickorgs_one-click-orgs/app/models/change_boolean_resolution.rb, step:null]
[name:name, type:ChangeIntegerResolution, file:oneclickorgs_one-click-orgs/app/models/change_integer_resolution.rb, step:null]
[name:name, type:ChangeTextProposal, file:oneclickorgs_one-click-orgs/app/models/change_text_proposal.rb, step:null]
[name:name, type:ChangeTextResolution, file:oneclickorgs_one-click-orgs/app/models/change_text_resolution.rb, step:null]
[name:name, type:Member, file:oneclickorgs_one-click-orgs/app/models/member.rb, step:null]
[name:name, type:Organisation, file:oneclickorgs_one-click-orgs/app/models/organisation.rb, step:null]
[name:name, type:Nomination, file:oneclickorgs_one-click-orgs/app/models/nomination.rb, step:Given ]
[name:name, type:GeneralMeeting, file:oneclickorgs_one-click-orgs/app/models/general_meeting.rb, step:Given ]
[name:name, type:ChangeBooleanProposal, file:oneclickorgs_one-click-orgs/app/models/change_boolean_proposal.rb, step:Given ]
[name:name, type:ChangeBooleanResolution, file:oneclickorgs_one-click-orgs/app/models/change_boolean_resolution.rb, step:Given ]
[name:name, type:ChangeIntegerResolution, file:oneclickorgs_one-click-orgs/app/models/change_integer_resolution.rb, step:Given ]
[name:name, type:ChangeTextProposal, file:oneclickorgs_one-click-orgs/app/models/change_text_proposal.rb, step:Given ]
[name:name, type:ChangeTextResolution, file:oneclickorgs_one-click-orgs/app/models/change_text_resolution.rb, step:Given ]
[name:name, type:Member, file:oneclickorgs_one-click-orgs/app/models/member.rb, step:Given ]
[name:name, type:Organisation, file:oneclickorgs_one-click-orgs/app/models/organisation.rb, step:Given ]
[name:name, type:Minute, file:oneclickorgs_one-click-orgs/app/models/minute.rb, step:Given ]
[name:name, type:AnnualGeneralMeeting, file:oneclickorgs_one-click-orgs/app/models/annual_general_meeting.rb, step:Given ]
[name:name, type:BoardMeeting, file:oneclickorgs_one-click-orgs/app/models/board_meeting.rb, step:Given ]
[name:new, type:BallotsController, file:oneclickorgs_one-click-orgs/app/controllers/ballots_controller.rb, step:Given ]
[name:new, type:MinutesController, file:oneclickorgs_one-click-orgs/app/controllers/minutes_controller.rb, step:Given ]
[name:objectives, type:Association, file:oneclickorgs_one-click-orgs/app/models/association.rb, step:null]
[name:objectives, type:Coop, file:oneclickorgs_one-click-orgs/app/models/coop.rb, step:null]
[name:page, type:Object, file:null, step:Then ]
[name:password, type:Object, file:null, step:Given ]
[name:pending, type:Coop, file:oneclickorgs_one-click-orgs/app/models/coop.rb, step:Given ]
[name:pending, type:Coop, file:oneclickorgs_one-click-orgs/app/models/coop.rb, step:Then ]
[name:proposed, type:Coop, file:oneclickorgs_one-click-orgs/app/models/coop.rb, step:When ]
[name:proposed, type:Coop, file:oneclickorgs_one-click-orgs/app/models/coop.rb, step:Then ]
[name:registered_office_address, type:Coop, file:oneclickorgs_one-click-orgs/app/models/coop.rb, step:null]
[name:save changes, type:Object, file:null, step:Given ]
[name:select, type:Object, file:null, step:Then ]
[name:send invitation, type:Object, file:null, step:Given ]
[name:set_domain, type:WebStepsCustom, file:oneclickorgs_one-click-orgs/features/step_definitions/web_steps_custom.rb, step:Given ]
[name:set_subdomain, type:WebStepsCustom, file:oneclickorgs_one-click-orgs/features/step_definitions/web_steps_custom.rb, step:Given ]
[name:set_subdomain_to_organisation, type:OrganisationSteps, file:oneclickorgs_one-click-orgs/features/step_definitions/organisation_steps.rb, step:Given ]
[name:set_subdomain_to_signup_subdomain, type:WebStepsCustom, file:oneclickorgs_one-click-orgs/features/step_definitions/web_steps_custom.rb, step:Given ]
[name:set_up_application, type:WorldExtensions, file:oneclickorgs_one-click-orgs/features/support/world_extensions.rb, step:Given ]
[name:set_up_application_if_necessary, type:WorldExtensions, file:oneclickorgs_one-click-orgs/features/support/world_extensions.rb, step:Given ]
[name:show, type:MeetingsController, file:oneclickorgs_one-click-orgs/app/controllers/meetings_controller.rb, step:Given ]
[name:show, type:BallotsController, file:oneclickorgs_one-click-orgs/app/controllers/ballots_controller.rb, step:Given ]
[name:show, type:AnnualGeneralMeetingsController, file:oneclickorgs_one-click-orgs/app/controllers/annual_general_meetings_controller.rb, step:Given ]
[name:show, type:GeneralMeetingsController, file:oneclickorgs_one-click-orgs/app/controllers/general_meetings_controller.rb, step:Given ]
[name:show, type:BoardsController, file:oneclickorgs_one-click-orgs/app/controllers/boards_controller.rb, step:Given ]
[name:show, type:MembersController, file:oneclickorgs_one-click-orgs/app/controllers/members_controller.rb, step:Given ]
[name:show, type:ProposalsController, file:oneclickorgs_one-click-orgs/app/controllers/proposals_controller.rb, step:Given ]
[name:show, type:ResolutionsController, file:oneclickorgs_one-click-orgs/app/controllers/resolutions_controller.rb, step:Given ]
[name:show, type:BoardMeetingsController, file:oneclickorgs_one-click-orgs/app/controllers/board_meetings_controller.rb, step:Given ]
[name:start_time, type:Object, file:null, step:Given ]
[name:sub, type:Object, file:null, step:Given ]
[name:subject, type:Object, file:null, step:Then ]
[name:submit, type:Object, file:null, step:Given ]
[name:title, type:Object, file:null, step:Given ]
[name:to, type:Object, file:null, step:Then ]
[name:user_logs_in, type:AuthenticationSteps, file:oneclickorgs_one-click-orgs/features/step_definitions/authentication_steps.rb, step:Given ]
[name:venue, type:Object, file:null, step:Given ]
[name:within, type:Object, file:null, step:When ]
[name:within, type:Object, file:null, step:Then ]

Referenced pages: 67
oneclickorgs_one-click-orgs/app/views/add_member_proposals/_form.html.haml
oneclickorgs_one-click-orgs/app/views/admin/coops/index.html.haml
oneclickorgs_one-click-orgs/app/views/admin/index.html.haml
oneclickorgs_one-click-orgs/app/views/annual_general_meetings/_agenda_item.html.haml
oneclickorgs_one-click-orgs/app/views/annual_general_meetings/create.html.haml
oneclickorgs_one-click-orgs/app/views/annual_general_meetings/new.html.haml
oneclickorgs_one-click-orgs/app/views/annual_general_meetings/show.html.haml
oneclickorgs_one-click-orgs/app/views/ballots/new.html.haml
oneclickorgs_one-click-orgs/app/views/board_meetings/edit.html.haml
oneclickorgs_one-click-orgs/app/views/board_meetings/show.html.haml
oneclickorgs_one-click-orgs/app/views/boards/show.html.haml
oneclickorgs_one-click-orgs/app/views/directors/_form.html.haml
oneclickorgs_one-click-orgs/app/views/eject_member_proposals/_form.html.haml
oneclickorgs_one-click-orgs/app/views/founder_members/_form.html.haml
oneclickorgs_one-click-orgs/app/views/founding_members/_form.html.haml
oneclickorgs_one-click-orgs/app/views/general_meetings/edit.html.haml
oneclickorgs_one-click-orgs/app/views/general_meetings/show.html.haml
oneclickorgs_one-click-orgs/app/views/meetings/_form.html.haml
oneclickorgs_one-click-orgs/app/views/meetings/coop/index.html.haml
oneclickorgs_one-click-orgs/app/views/meetings/new.html.haml
oneclickorgs_one-click-orgs/app/views/meetings/show.html.haml
oneclickorgs_one-click-orgs/app/views/members/association/index.html.haml
oneclickorgs_one-click-orgs/app/views/members/association/show.html.haml
oneclickorgs_one-click-orgs/app/views/members/company/index.html.haml
oneclickorgs_one-click-orgs/app/views/members/confirm_resign.html.haml
oneclickorgs_one-click-orgs/app/views/members/coop/confirm_eject.html.haml
oneclickorgs_one-click-orgs/app/views/members/coop/confirm_resign.html.haml
oneclickorgs_one-click-orgs/app/views/members/coop/created.html.haml
oneclickorgs_one-click-orgs/app/views/members/coop/edit.html.haml
oneclickorgs_one-click-orgs/app/views/members/coop/index.html.haml
oneclickorgs_one-click-orgs/app/views/members/coop/new.html.haml
oneclickorgs_one-click-orgs/app/views/members/coop/show.html.haml
oneclickorgs_one-click-orgs/app/views/members/edit.html.haml
oneclickorgs_one-click-orgs/app/views/members/new.html.haml
oneclickorgs_one-click-orgs/app/views/members/resigned.html.haml
oneclickorgs_one-click-orgs/app/views/membership_application_forms/edit.html.haml
oneclickorgs_one-click-orgs/app/views/minutes/new.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/_decision.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/_failed_proposal.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/_meeting.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/_open_proposals.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/_proposal.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/_resignation.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/_timeline.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/_vote.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/association/_new_member.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/association/dashboard.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/company/dashboard.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/coop/_board_meeting.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/coop/_draft_resolution.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/coop/_general_meeting.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/coop/_new_member.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/coop/_resolution.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/coop/_resolution_proposal.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/coop/checklist.html.haml
oneclickorgs_one-click-orgs/app/views/one_click/coop/dashboard.html.haml
oneclickorgs_one-click-orgs/app/views/proposals/_description.html.haml
oneclickorgs_one-click-orgs/app/views/proposals/_vote.html.haml
oneclickorgs_one-click-orgs/app/views/proposals/_vote_count.html.haml
oneclickorgs_one-click-orgs/app/views/proposals/index.html.haml
oneclickorgs_one-click-orgs/app/views/proposals/show.html.haml
oneclickorgs_one-click-orgs/app/views/resolutions/_description.html.haml
oneclickorgs_one-click-orgs/app/views/resolutions/_vote.html.haml
oneclickorgs_one-click-orgs/app/views/resolutions/_vote_count.html.haml
oneclickorgs_one-click-orgs/app/views/resolutions/show.html.haml
oneclickorgs_one-click-orgs/app/views/shared/_propose_freeform_form.html.haml
oneclickorgs_one-click-orgs/app/views/tasks/_task.html.haml

