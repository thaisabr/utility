Given /^the application is set up$/ do
  # Using smackaho.st to give us automatic resolution to localhost
  # 
  # Port needs to be saved for Selenium tests (because our app code considers
  # the port as well as the hostname when figuring out how to handle a
  # request, and the Capybara app server doesn't run on port 80).
  # 
  # When using the rack-test driver, don't use port numbers at all,
  # since it makes our test-session-resetting code (see features/support/capybara_domains.rb)
  # more complicated.
  port_segment = Capybara.current_driver == :selenium ? ":#{Capybara.server_port}" : ''
  
  Setting[:base_domain] = "smackaho.st#{port_segment}"
  Setting[:signup_domain] = "create.smackaho.st#{port_segment}"
end
Given /^an organisation has been created$/ do
  @organisation = Organisation.make
  @organisation.pending!
  @founder = @organisation.members.make(
    :member_class => @organisation.member_classes.find_by_name("Founder"),
    :inducted_at => nil
  )
end
Given /^the subdomain is the organisation's subdomain$/ do
  Given %Q{the subdomain is "#{@organisation.subdomain}"}
end
Given /^there are enough members to start the founding vote$/ do
  extra_members_needed = 3 - @organisation.members.count
  if extra_members_needed > 0
    extra_members_needed.times do
      @organisation.members.make(:pending,
        :member_class => @organisation.member_classes.find_by_name("Founding Member")
      )
    end
  end
end
Given /^the founding vote has been started$/ do
  founder = @organisation.member_classes.where(:name => "Founder").first.members.first
  @organisation.found_organisation_proposals.make(:proposer => founder).start
  @organisation.proposed!
end
Given /^another founding vote has been started$/ do
  sleep(1)
  founder = @organisation.member_classes.where(:name => "Founder").first.members.first
  @organisation.found_organisation_proposals.make(:proposer => founder, :title => "A second voting proposal.").start
  @organisation.proposed!
end
Given /^everyone has voted (to support|against) the founding$/ do |vote|
  fop = @organisation.found_organisation_proposals.last
    verdict = (vote == "against") ? "against" : "for"
    @organisation.members.each do |member|
    member.cast_vote(verdict.to_sym, fop.id)
  end
end
When /^the proposal closer runs$/ do
  Proposal.close_proposals
end
Given /^I am a founding member$/ do
  @user = @organisation.members.make(:pending, :member_class => @organisation.member_classes.find_by_name("Founding Member"))
end
Given /^I have logged in$/ do
  visit '/'
  fill_in('Email', :with => @user.email)
  fill_in('Password', :with => @user.password)
  click_button("Login")
end
When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
Then /^(?:|I )should see "([^"]*)"$/ do |text|
  if page.respond_to? :should
    page.should have_content(text)
  else
    assert page.has_content?(text)
  end
end
Then /^(?:|I )should not see "([^"]*)"$/ do |text|
  if page.respond_to? :should
    page.should have_no_content(text)
  else
    assert page.has_no_content?(text)
  end
end
  def path_to(page_name)
    case page_name

    when /^the home\s?page$/
      '/'
    when /^the setup page$/
      '/setup'
    when /^the signup page$/
      '/organisations/new'
    when /^the welcome page$/
      '/welcome'
    when /^the voting and proposals page$/
      '/'
    when /^the proposal page$/
      @proposal ||= Proposal.last
      proposal_path(@proposal)
    when /^the member page for "(.*)"$/
      @member = @organisation.members.find_by_email($1)
      member_path(@member)
    when /^a member's page$/
      @member = @organisation.members.active.last
      member_path(@member)
    when /^the members page$/
      members_path

    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
