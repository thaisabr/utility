Given(/^I am a signed in advocate$/) do
  @advocate = create(:advocate)
  visit new_user_session_path
  sign_in(@advocate.user, 'password')
end
Given(/^I am a signed in advocate admin$/) do
  @advocate = create(:advocate, :admin)
  visit new_user_session_path
  sign_in(@advocate.user, 'password')
end
  def sign_in(user, password)
    visit new_user_session_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: password
    click_on 'Sign in'
  end
Given(/^There are other advocates in my chamber$/) do
  FactoryGirl.create(:advocate,
        chamber: @advocate.chamber,
        user: FactoryGirl.create(:user, first_name: 'John', last_name: 'Doe'),
        account_number: 'AC135')
  FactoryGirl.create(:advocate,
        chamber: @advocate.chamber,
        user: FactoryGirl.create(:user, first_name: 'Joe', last_name: 'Blow'),
        account_number: 'XY455')
end
Given(/^I am on the new claim page$/) do
  create(:court, name: 'some court')
  create(:offence_class, description: 'A: Homicide and related grave offences')
  create(:offence, description: 'Murder')
  create(:fee_type, :basic, description: 'Basic Fee')
  create(:fee_type, :basic, description: 'Other Basic Fee')
  create(:fee_type, :basic, description: 'Basic Fee with dates attended required', code: 'SAF')
  create(:fee_type, :fixed, description: 'Fixed Fee example')
  create(:fee_type, :misc,  description: 'Miscellaneous Fee example')
  create(:expense_type, name: 'Travel')
  visit new_advocates_claim_path
end
Given(/^There are fee schemes in place$/) do
  Scheme.find_or_create_by(name: 'AGFS Fee Scheme 7', start_date: Date.parse('01/04/2011'), end_date: Date.parse('02/10/2011'))
  Scheme.find_or_create_by(name: 'AGFS Fee Scheme 8', start_date: Date.parse('03/10/2011'), end_date: Date.parse('31/03/2012'))
  Scheme.find_or_create_by(name: 'AGFS Fee Scheme 9', start_date: Date.parse('01/04/2012'), end_date: nil)
end
Given(/^There are case types in place$/) do
  load "#{Rails.root}/db/seeds/case_types.rb"
  CaseType.find_or_create_by!(name: 'Fixed fee', is_fixed_fee: true)
end
When(/^I fill in the certification details and submit/) do
  check 'certification_main_hearing'
  click_on 'Certify and Submit Claim'
end
When(/^I fill in the claim details$/) do
  select('Guilty plea', from: 'claim_case_type_id')
  select('CPS', from: 'claim_prosecuting_authority')
  select('some court', from: 'claim_court_id')
  fill_in 'claim_case_number', with: '123456'
  murder_offence_id = Offence.find_by(description: 'Murder').id.to_s
  first('#claim_offence_id', visible: false).set(murder_offence_id)
  select('QC', from: 'claim_advocate_category')

  within '#defendants' do
    fill_in 'claim_defendants_attributes_0_first_name', with: 'Foo'
    fill_in 'claim_defendants_attributes_0_last_name', with: 'Bar'

    fill_in 'claim_defendants_attributes_0_date_of_birth_3i', with: '04'
    fill_in 'claim_defendants_attributes_0_date_of_birth_2i', with: '10'
    fill_in 'claim_defendants_attributes_0_date_of_birth_1i', with: '1980'

    fill_in 'claim_defendants_attributes_0_representation_orders_attributes_0_maat_reference', with: 'aaa1111'

    date = rand(1..10).days.ago
    fill_in 'claim_defendants_attributes_0_representation_orders_attributes_0_representation_order_date_3i', with: date.strftime('%d')
    fill_in 'claim_defendants_attributes_0_representation_orders_attributes_0_representation_order_date_2i', with: date.strftime('%m')
    fill_in 'claim_defendants_attributes_0_representation_orders_attributes_0_representation_order_date_1i', with: date.strftime('%Y')

    choose 'Crown Court'
  end

  within '#basic-fees' do
    fill_in 'claim_basic_fees_attributes_0_quantity', with: 1
    fill_in 'claim_basic_fees_attributes_0_amount', with: 0.5
    fill_in 'claim_basic_fees_attributes_1_quantity', with: 1
    fill_in 'claim_basic_fees_attributes_1_amount', with: 0.5
  end

  within '#expenses' do
    select 'Travel', from: 'claim_expenses_attributes_0_expense_type_id'
    fill_in 'claim_expenses_attributes_0_location', with: 'London'
    fill_in 'claim_expenses_attributes_0_quantity', with: 1
    fill_in 'claim_expenses_attributes_0_rate', with: 40
  end

  within 'table#evidence-checklist' do
    element = find('td label', text: "Representation Order")
    checkbox_id = element[:for]
    check checkbox_id
  end

  attach_file(:claim_documents_attributes_0_document, 'features/examples/longer_lorem.pdf')
end
When(/^I submit to LAA$/) do
  click_on 'Submit to LAA'
end
Then(/^I should be redirected to the claim confirmation page$/) do
  claim = Claim.first
  expect(page.current_path).to eq(confirmation_advocates_claim_path(claim))
end
Then(/^I should be redirected to the claim certification page$/) do
  claim = Claim.first
  expect(page.current_path).to eq(new_advocates_claim_certification_path(claim))
end
Then(/^I should see the claim totals$/) do
  expect(page).to have_content("Fees total: £1.00")
  expect(page).to have_content("Expenses total: £40.00")
  expect(page).to have_content("Total: £41.00")
end
Then(/^I should be on the claim confirmation page$/) do
  claim = @claim || Claim.first
  expect(page.current_path).to eq(confirmation_advocates_claim_path(claim))
end
Given(/^a claim exists with state "(.*?)"$/) do |claim_state|
  @claim = case claim_state
    when "draft"
      create(:claim, advocate_id: Advocate.first.id)
    else
      create(:claim, advocate_id: Advocate.first.id)
  end
end
Then(/^the claim should be in state "(.*?)"$/) do |claim_state|
  @claim.reload
  expect(@claim.state).to eq(claim_state)
end
When(/^I am on the claim edit page$/) do
  claim = Claim.first
  visit edit_advocates_claim_path(claim)
end
Then(/^I can view a select of all advocates in my chamber$/) do
  expect(page).to have_selector('select#claim_advocate_id')
  expect(page).to have_content('Doe, John: AC135')
  expect(page).to have_content('Blow, Joe: XY455')
end
When(/^I select Advocate name "(.*?)"$/) do |advocate_name|
  select(advocate_name, from: 'claim_advocate_id')
end
Then(/^the claim should be in a "(.*?)" state$/) do |state|
  claim = Claim.first
  expect(claim.state).to eq(state)
end
When(/^I change the case number$/) do
  fill_in 'claim_case_number', with: '543211234'
end
Then(/^the case number should reflect the change$/) do
  claim = Claim.first
  expect(claim.case_number).to eq('543211234')
end
When(/^I add a fixed fee$/) do
    within '#fixed-fees' do
      fill_in 'claim_fixed_fees_attributes_0_quantity', with: 1
      fill_in 'claim_fixed_fees_attributes_0_amount', with: 100.01
      select 'Fixed Fee example', from: 'claim_fixed_fees_attributes_0_fee_type_id'
    end
end
Then(/^I should see the claim totals accounting for only the fixed fee$/) do
  expect(page).to have_content("Fees total: £100.01")
end
When(/^I add a miscellaneous fee$/) do
    within '#misc-fees' do
      fill_in 'claim_misc_fees_attributes_0_quantity', with: 1
      fill_in 'claim_misc_fees_attributes_0_amount', with: 200.01
      select 'Miscellaneous Fee example', from: 'claim_misc_fees_attributes_0_fee_type_id'
    end
end
Then(/^I should see the claim totals accounting for the miscellaneous fee$/) do
  expect(page).to have_content("Fees total: £201.01")
end
When(/^I select a Case Type of "(.*?)"$/) do |case_type|
  select case_type, from: 'claim_case_type_id'
end
Given(/^I have a claim in draft state$/) do
  @claim = create(:claim, advocate: @advocate)
end
Given(/^I submit the claim$/) do
  visit edit_advocates_claim_path(@claim)
  click_on 'Submit to LAA'
end
Then(/^I should see the state change to submitted reflected in the history$/) do
  @claim.reload
  within '#claim-history' do
    within 'table' do
      within all('tr').first do
        expect(page).to have_content(@claim.versions.last.created_at.strftime(Settings.date_time_format))
        expect(page).to have_content('State change')
        expect(page).to have_content('Advocate')
        expect(page).to have_content(/Changed "State" from "draft" to "submitted"/)
      end
    end
  end
end
When(/^I visit the claim's detail page$/) do
  visit advocates_claim_path(@claim)
end
