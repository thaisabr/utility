Feature: Caseworker claims list
Scenario Outline: Search current, paid and part paid claims by defendant name
Given I am signed in and on the case worker dashboard
And I have 2 "allocated" claims involving defendant "Joe Bloggs"
And I have 3 "allocated" claims involving defendant "Fred Bloggs"
And I have 2 "paid" claims involving defendant "Joe Bloggs"
And I have 3 "paid" claims involving defendant "Fred Bloggs"
And I have 2 "part_paid" claims involving defendant "Someone Else"
When I visit my dashboard
And I search claims by defendant name <defendant_name>
Then I should only see <number> "Current" claims
Examples:
| defendant_name | number |
| "Joe Bloggs"   | 2      |
| "Fred Bloggs"  | 3      |
| "Bloggs"       | 5      |
