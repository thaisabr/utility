Given(/^I am a signed in advocate$/) do
  @advocate = create(:advocate)
  visit new_user_session_path
  sign_in(@advocate.user, 'password')
end
Given(/^I am a signed in case worker$/) do
  @case_worker = create(:case_worker)
  visit new_user_session_path
  sign_in(@case_worker.user, 'password')
end
  def sign_in(user, password)
    visit new_user_session_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: password
    click_on 'Sign in'
  end
Given(/^There are fee schemes in place$/) do
  Scheme.find_or_create_by(name: 'AGFS Fee Scheme 7', start_date: Date.parse('01/04/2011'), end_date: Date.parse('02/10/2011'))
  Scheme.find_or_create_by(name: 'AGFS Fee Scheme 8', start_date: Date.parse('03/10/2011'), end_date: Date.parse('31/03/2012'))
  Scheme.find_or_create_by(name: 'AGFS Fee Scheme 9', start_date: Date.parse('01/04/2012'), end_date: nil)
end
Given(/^claims have been assigned to me$/) do
  case_worker = CaseWorker.first
  @claims = create_list(:allocated_claim, 5)
  @other_claims = create_list(:allocated_claim, 3)
  @claims.each_with_index { |claim, index| claim.update_column(:total, index + 1) }
  @claims.each { |claim| claim.case_workers << case_worker }
  create :defendant, claim_id: @claims.first.id, representation_orders: [ FactoryGirl.create(:representation_order, maat_reference: 'AA1245') ]
  create :defendant, claim_id: @claims.second.id, representation_orders: [ FactoryGirl.create(:representation_order, maat_reference: 'BB1245') ]
end
When(/^I visit my dashboard$/) do
  visit case_workers_claims_path
end
When(/^I view status details for a claim$/) do
	first('div.claim-controls').click_link("Detail")
end
When(/^I select status "(.*?)" from select$/) do |status|
	select "#{status}", :from => "claim_state_for_form"
end
When(/^I enter fees assessed of "(.*?)" and expenses assessed of "(.*?)"$/) do |fees, expenses|
  fill_in 'claim_assessment_attributes_fees', with: fees unless fees.empty?
  fill_in 'claim_assessment_attributes_expenses', with: expenses unless expenses.empty?
	# find('div#amountAssessed').fill_in "Amount assessed", with: amount unless amount.empty?
end
When(/^I enter remark "(.*?)"$/) do |remark|
	fill_in	"Remarks", with: remark
end
When(/^I press update button$/) do
  click_button "Update"
end
Given(/^I have (\d+) allocated claims whos status is "(.*?)" with fees assessed of "(.*?)" and expenses assessed of "(.*?)" and remark of "(.*?)"$/) do |number, status, fees, expenses, remark|
  claims = create_list(:allocated_claim, number.to_i, advocate: @advocate)
  claims.each do |claim|
    claim.assessment.update!(fees: fees) unless fees.empty?
    claim.assessment.update!(expenses: expenses) unless expenses.empty?
		claim.additional_information = remark

		case status
			when "Part paid"
				claim.pay_part!
			when "Rejected"
				claim.reject!
			when "Rejected"
				claim.reject!
			when "Awaiting info from court"
				claim.await_info_from_court!
			else
				raise "ERROR: Invalid status specified for advocate view scenario"
		end
  end
end
When(/^I view status details of my first claim$/) do
	@claim = Claim.where(advocate: @advocate).first
  visit advocates_claim_path(@claim)
end
Then(/^I should see "(.*?)" total assessed value of "(.*?)"$/) do |disabled, total|
	total = "£0.00" if total.empty?
	disabled = disabled == "disabled" ? true : false
  expect(find_by_id('assessed-total').text).to eql total
end
Then(/^I should see "(.*?)" remark "(.*?)"$/) do |disabled,remark|
	disabled = disabled == "disabled" ? true : false
  expect(find_field('Remarks', disabled: disabled).value).to eq(remark)
end
Then(/^I should see "(.*?)" status select with "(.*?)" selected$/) do |disabled, status|
	disabled = disabled == "disabled" ? true : false
	expect(find_field('claim_state_for_form', disabled: disabled).find('option[selected]').text).to eql(status)
end
Then(/^I should see an image tag with source "(.*?)" against that claim$/) do |image_source|
	expect(find('.status-indicator')['src'].include?(image_source)).to eql(true)
end
When(/^I visit the advocates dashboard$/) do
  visit advocates_claims_path
end
Given(/^my chamber has (\d+) "(.*?)" claims$/) do |number, state|
  advocate = Advocate.first
  chamber = Chamber.first
  chamber.advocates << advocate

  @claims = state == 'draft' ? create_list(:claim, number.to_i) : create_list("#{state}_claim".to_sym, number.to_i)
  @claims.each do |claim|
    claim.update_column(:advocate_id, advocate.id)
    claim.fees << create(:fee, :random_values, claim: claim, fee_type: create(:fee_type))
    if claim.state == 'completed'
      claim.assessment.update(fees: claim.total)
    elsif claim.state == 'part_paid'
      claim.assessment.update(fees: claim.total / 2)     # arbitrarily pay half the total for part-paid
    end
  end
end
Then(/^a figure representing the amount assessed for "(.*?)" claims$/) do |state|
    within("##{state}") do
      rows = all('tr')
      rows.each do |row|
        within(row) do
          cms = all('td')[3].text
          claim = Claim.find_by(cms_number: cms) # find claim which corresponds to |row|
          expect(claim.cms_number).to eq cms # check that the correct claim was found
          expect(row.text.include?(ActionController::Base.helpers.number_to_currency(claim.assessment.total))).to be true
        end
      end
    end
end
When(/^I mark the claim paid in full$/) do
  select 'Paid in full', from: 'claim_state_for_form'
  fill_in 'claim_assessment_attributes_fees', with: '100.00'
  click_on 'Update'
end
