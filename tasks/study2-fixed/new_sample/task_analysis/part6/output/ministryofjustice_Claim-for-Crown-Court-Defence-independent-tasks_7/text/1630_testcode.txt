Given(/^I am a signed in case worker$/) do
  @case_worker = create(:case_worker)
  visit new_user_session_path
  sign_in(@case_worker.user, 'password')
end
  def sign_in(user, password)
    visit new_user_session_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: password
    click_on 'Sign in'
  end
Given(/^a redetermined claim is assigned to me$/) do
  @claim = create(:redetermination_claim)
  @claim.case_workers << @case_worker
end
Then(/^a form should be visible for me to enter the redetermination amounts$/) do
  expect(page).to have_content('Enter redetermined amounts')
  expect(page).to have_selector('#claim_redeterminations_attributes_0_fees')
end
When(/^I enter redetermination amounts$/) do 
  fill_in 'claim_redeterminations_attributes_0_fees', with: 1577.22
  fill_in 'claim_redeterminations_attributes_0_expenses', with: 805.75
  click_button 'Update'
end
Then(/^There should be no form to enter redetermination amounts$/) do
  expect(page).not_to have_content('Enter redetermined amounts')
  expect(page).not_to have_selector('#claim_redeterminations_attributes_0_fees')
end
Then(/^The redetermination I just entered should be visible$/) do
  expect(page).to have_content('Redetermination of')
  within('#redetermination-fees') do
    expect(page).to have_content('£1,577.22')
  end
  within('#redetermination-expenses') do
    expect(page).to have_content('£805.75')
  end
end
When(/^I visit the claim's case worker detail page$/) do
  visit case_workers_claim_path(@claim)
end
