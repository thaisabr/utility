Given(/^I have (\d+) "(.*?)" claims involving defendant "(.*?)"$/) do |number,state,defendant_name|
  claims = create_list("#{state}_claim".to_sym, number.to_i)
  claims.each do |claim|
    create(:defendant, claim: claim, first_name: defendant_name.split.first, last_name: defendant_name.split.last)
  end
  @case_worker.claims << claims
end
When(/^I visit my dashboard$/) do
  visit case_workers_claims_path
end
Given(/^I am signed in and on the case worker dashboard$/) do
  steps <<-STEPS
    Given I am a signed in case worker
      And There are fee schemes in place
      And claims have been assigned to me
     When I visit my dashboard
     Then I should see only my claims
      And I should see the claims sorted by oldest first
  STEPS
end
When(/^I search claims by defendant name "(.*?)"$/) do |defendant_name|
  fill_in 'search', with: defendant_name
  click_button 'Search'
end
Then(/^I should only see (\d+) "(.*?)" claims$/) do |number, state_name|
  expect(page).to have_content(/#{number} claim?s matching/)
end
Given(/^I am a signed in case worker$/) do
  @case_worker = create(:case_worker)
  visit new_user_session_path
  sign_in(@case_worker.user, 'password')
end
  def sign_in(user, password)
    visit new_user_session_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: password
    click_on 'Sign in'
  end
Given(/^There are fee schemes in place$/) do
  Scheme.find_or_create_by(name: 'AGFS Fee Scheme 7', start_date: Date.parse('01/04/2011'), end_date: Date.parse('02/10/2011'))
  Scheme.find_or_create_by(name: 'AGFS Fee Scheme 8', start_date: Date.parse('03/10/2011'), end_date: Date.parse('31/03/2012'))
  Scheme.find_or_create_by(name: 'AGFS Fee Scheme 9', start_date: Date.parse('01/04/2012'), end_date: nil)
end
Given(/^claims have been assigned to me$/) do
  case_worker = CaseWorker.first
  @claims = create_list(:allocated_claim, 5)
  @other_claims = create_list(:allocated_claim, 3)
  @claims.each_with_index { |claim, index| claim.update_column(:total, index + 1) }
  @claims.each { |claim| claim.case_workers << case_worker }
  create :defendant, claim_id: @claims.first.id, representation_orders: [ FactoryGirl.create(:representation_order, maat_reference: 'AA1245') ]
  create :defendant, claim_id: @claims.second.id, representation_orders: [ FactoryGirl.create(:representation_order, maat_reference: 'BB1245') ]
end
Then(/^I should see only my claims$/) do
  @claims.each do | claim |
    expect(find('#claims-list')).to have_link(claim.case_number,
          :href => case_workers_claim_path(claim,
          claim_ids: @claims.map(&:id), claim_count: @claims.try(:count)))
  end

  @other_claims.each do | other_claim |
    expect(find('#claims-list')).to_not have_link(other_claim.case_number,
          :href => case_workers_claim_path(other_claim,
          claim_ids: @other_claims.map(&:id), claim_count: @other_claims.try(:count)))
  end
end
Then(/^I should see the claims sorted by oldest first$/) do
  @claims.sort_by(&:submitted_at).each do | claim |
    expect(find('#claims-list')).to have_link(claim.case_number,
          :href => case_workers_claim_path(claim,
          claim_ids: @claims.map(&:id), claim_count: @claims.try(:count)))
  end
end
