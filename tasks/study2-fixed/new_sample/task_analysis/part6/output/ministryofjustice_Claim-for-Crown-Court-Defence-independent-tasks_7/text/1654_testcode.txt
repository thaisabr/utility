Given(/^I am a signed in advocate$/) do
  @advocate = create(:advocate)
  visit new_user_session_path
  sign_in(@advocate.user, 'password')
end
Given(/^I am a signed in advocate admin$/) do
  @advocate = create(:advocate, :admin)
  visit new_user_session_path
  sign_in(@advocate.user, 'password')
end
Given(/^I am a signed in case worker admin$/) do
  @case_worker = create(:case_worker, :admin)
  visit new_user_session_path
  sign_in(@case_worker.user, 'password')
end
  def sign_in(user, password)
    visit new_user_session_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: password
    click_on 'Sign in'
  end
Given(/^There are other advocates in my chamber$/) do
  FactoryGirl.create(:advocate,
        chamber: @advocate.chamber,
        user: FactoryGirl.create(:user, first_name: 'John', last_name: 'Doe'),
        account_number: 'AC135')
  FactoryGirl.create(:advocate,
        chamber: @advocate.chamber,
        user: FactoryGirl.create(:user, first_name: 'Joe', last_name: 'Blow'),
        account_number: 'XY455')
end
Given(/^I am on the new claim page$/) do
  create(:court, name: 'some court')
  create(:offence_class, description: 'A: Homicide and related grave offences')
  create(:offence, description: 'Murder')
  create(:fee_type, :basic, description: 'Basic Fee')
  create(:fee_type, :basic, description: 'Other Basic Fee')
  create(:fee_type, :basic, description: 'Basic Fee with dates attended required', code: 'SAF')
  create(:fee_type, :fixed, description: 'Fixed Fee example')
  create(:fee_type, :misc,  description: 'Miscellaneous Fee example')
  create(:expense_type, name: 'Travel')
  visit new_advocates_claim_path
end
Given(/^There are fee schemes in place$/) do
  Scheme.find_or_create_by(name: 'AGFS Fee Scheme 7', start_date: Date.parse('01/04/2011'), end_date: Date.parse('02/10/2011'))
  Scheme.find_or_create_by(name: 'AGFS Fee Scheme 8', start_date: Date.parse('03/10/2011'), end_date: Date.parse('31/03/2012'))
  Scheme.find_or_create_by(name: 'AGFS Fee Scheme 9', start_date: Date.parse('01/04/2012'), end_date: nil)
end
Given(/^There are case types in place$/) do
  load("#{Rails.root}/db/seeds/case_types.rb")
end
When(/^I fill in the claim details$/) do
  select('Guilty plea', from: 'claim_case_type_id')
  select('CPS', from: 'claim_prosecuting_authority')
  select('some court', from: 'claim_court_id')
  fill_in 'claim_case_number', with: '123456'
  murder_offence_id = Offence.find_by(description: 'Murder').id.to_s
  first('#claim_offence_id', visible: false).set(murder_offence_id)
  select('QC', from: 'claim_advocate_category')

  within '#defendants' do
    fill_in 'claim_defendants_attributes_0_first_name', with: 'Foo'
    fill_in 'claim_defendants_attributes_0_last_name', with: 'Bar'

    fill_in 'claim_defendants_attributes_0_date_of_birth_3i', with: '04'
    fill_in 'claim_defendants_attributes_0_date_of_birth_2i', with: '10'
    fill_in 'claim_defendants_attributes_0_date_of_birth_1i', with: '1980'

    fill_in 'claim_defendants_attributes_0_representation_orders_attributes_0_maat_reference', with: 'aaa1111'

    date = rand(1..10).days.ago
    fill_in 'claim_defendants_attributes_0_representation_orders_attributes_0_representation_order_date_3i', with: date.strftime('%d')
    fill_in 'claim_defendants_attributes_0_representation_orders_attributes_0_representation_order_date_2i', with: date.strftime('%m')
    fill_in 'claim_defendants_attributes_0_representation_orders_attributes_0_representation_order_date_1i', with: date.strftime('%Y')

    choose 'Crown Court'
  end

  within '#basic-fees' do
    fill_in 'claim_basic_fees_attributes_0_quantity', with: 1
    fill_in 'claim_basic_fees_attributes_0_amount', with: 0.5
    fill_in 'claim_basic_fees_attributes_1_quantity', with: 1
    fill_in 'claim_basic_fees_attributes_1_amount', with: 0.5
  end

  within '#expenses' do
    select 'Travel', from: 'claim_expenses_attributes_0_expense_type_id'
    fill_in 'claim_expenses_attributes_0_location', with: 'London'
    fill_in 'claim_expenses_attributes_0_quantity', with: 1
    fill_in 'claim_expenses_attributes_0_rate', with: 40
  end

  within 'table#evidence-checklist' do
    element = find('td label', text: "Representation Order")
    checkbox_id = element[:for]
    check checkbox_id
  end

  attach_file(:claim_documents_attributes_0_document, 'features/examples/longer_lorem.pdf')
end
When(/^I submit to LAA$/) do
  click_on 'Submit to LAA'
end
When(/^I save to drafts$/) do
  click_on 'Save to drafts'
end
Then(/^I should be redirected to the claim confirmation page$/) do
  claim = Claim.first
  expect(page.current_path).to eq(confirmation_advocates_claim_path(claim))
end
Then(/^I should be redirected back to the claim form with error$/) do
  expect(page).to have_content('Claim for Advocate Graduated Fees')
  expect(page).to have_content(/\d+ errors? prohibited this claim from being saved:/)
  expect(page).to have_content("Advocate can't be blank")
end
Then(/^I should see the claim totals$/) do
  expect(page).to have_content("Fees total: £1.00")
  expect(page).to have_content("Expenses total: £40.00")
  expect(page).to have_content("Total: £41.00")
end
When(/^I am on the claim edit page$/) do
  claim = Claim.first
  visit edit_advocates_claim_path(claim)
end
Then(/^I can view a select of all advocates in my chamber$/) do
  expect(page).to have_selector('select#claim_advocate_id')
  expect(page).to have_content('Doe, John: AC135')
  expect(page).to have_content('Blow, Joe: XY455')
end
When(/^I select Advocate name "(.*?)"$/) do |advocate_name|
  select(advocate_name, from: 'claim_advocate_id')
end
When(/^I clear the form$/) do
  click_on 'Clear form'
end
Then(/^I should be redirected to the new claim page$/) do
  expect(page.current_path).to eq(new_advocates_claim_path)
end
Then(/^the claim should be in a "(.*?)" state$/) do |state|
  claim = Claim.first
  expect(claim.state).to eq(state)
end
When(/^I add a fixed fee$/) do
    within '#fixed-fees' do
      fill_in 'claim_fixed_fees_attributes_0_quantity', with: 1
      fill_in 'claim_fixed_fees_attributes_0_amount', with: 100.01
      select 'Fixed Fee example', from: 'claim_fixed_fees_attributes_0_fee_type_id'
    end
end
Then(/^I should see the claim totals accounting for only the fixed fee$/) do
  expect(page).to have_content("Fees total: £100.01")
end
When(/^I add a miscellaneous fee$/) do
    within '#misc-fees' do
      fill_in 'claim_misc_fees_attributes_0_quantity', with: 1
      fill_in 'claim_misc_fees_attributes_0_amount', with: 200.01
      select 'Miscellaneous Fee example', from: 'claim_misc_fees_attributes_0_fee_type_id'
    end
end
Then(/^I should see the claim totals accounting for the miscellaneous fee$/) do
  expect(page).to have_content("Fees total: £201.01")
end
When(/^I select a Case Type of "(.*?)"$/) do |case_type|
  select case_type, from: 'claim_case_type_id'
end
Then(/^There should not be any Initial Fees saved$/) do
  # note: cannot rely on size/count since all basic fees are
  #       instantiated as empty but existing records per claim.
  expect(Claim.last.calculate_fees_total(:basic).to_f).to eql(0.0)
end
Then(/^There should not be any Miscellaneous Fees Saved$/) do
  expect(Claim.last.misc_fees.size).to eql(0)
end
Then(/^There should not be any Fixed Fees saved$/) do
  expect(Claim.last.fixed_fees.size).to eql(0)
end
Given(/^I fill in a Miscellaneous Fee$/) do
  within '#misc-fees' do
    select 'Miscellaneous Fee example', from: 'claim_misc_fees_attributes_0_fee_type_id'
    fill_in 'claim_misc_fees_attributes_0_quantity', with: 2
    fill_in 'claim_misc_fees_attributes_0_amount', with: 3.00
  end
end
Given(/^I fill in a Fixed Fee$/) do
  within '#fixed-fees' do
    select 'Fixed Fee example', from: 'claim_fixed_fees_attributes_0_fee_type_id'
    fill_in 'claim_fixed_fees_attributes_0_quantity', with: 2
    fill_in 'claim_fixed_fees_attributes_0_amount', with: 3.00
  end
end
Given(/^a non\-fixed\-fee claim exists with basic and miscellaneous fees$/) do
  claim = create(:submitted_claim, case_type_id: CaseType.by_type('Trial').id, advocate_id: Advocate.first.id)
  create(:fee, :basic, claim: claim, quantity: 3, amount: 7.5)
  create(:fee, :misc,  claim: claim, quantity: 2, amount: 5.0)
end
Given(/^There are basic and non-basic fee types$/) do
  create :fee_type, :basic
  create :fee_type, :misc
  create :fee_type, :fixed
  create :fee_type, :basic
end
Given(/^(\d+) case workers? exists?$/) do |quantity|
  @case_workers = create_list(:case_worker, quantity.to_i)
end
Given(/^(\d+) submitted claims? exists?$/) do |quantity|
  @claims = create_list(:submitted_claim, quantity.to_i)
end
When(/^I visit the allocation page$/) do
  visit case_workers_admin_allocations_path
end
Given(/^there are (\d+) "(.*?)" claims?$/) do |quantity, type|
  Claim.destroy_all

  number = quantity.to_i

  case type
    when 'all'
      create_list(:submitted_claim, number)
    when 'fixed_fee'
      claims = create_list(:submitted_claim, number)
      claims.each { |c| c.fees << create(:fee, :fixed) }
    when 'trial'
      create_list(:submitted_claim, number, case_type_id: CaseType.by_type('Trial').id)
    when 'cracked'
      create_list(:submitted_claim, number, case_type_id: CaseType.by_type('Cracked Trial').id)
    when 'guilty_plea'
      create_list(:submitted_claim, number, case_type_id: CaseType.by_type('Guilty plea').id)
  end
end
When(/^I filter by "(.*?)"$/) do |filter|
  choose filter.humanize
  click_on 'Filter'
end
Then(/^I should only see (\d+) "(.*?)" claims? after filtering$/) do |quantity, type|
  number = quantity.to_i

  claims = type == 'all' ? Claim.all : Claim.send(type.to_sym)

  claims.each do |claim|
    expect(page).to have_selector("#claim_#{claim.id}")
  end

  expect(claims.count).to eq(number)

  within '.claim-count' do
    expect(page).to have_content(/#{number} claims?/)
  end
end
Then(/^I should see an evidence checklist section$/) do
  expect(page).to have_selector('fieldset#evidence-checklist')
end
Then(/^I visit the claim show page$/) do
  @claim = @claim || Claim.first
  visit advocates_claim_path(@claim)
end
Then(/^I should see a list item for "(.*?)" evidence$/) do |text|
  expect(page).to have_selector('li', text: text)
end
Given(/^I fill in the claim details omitting the advocate$/) do
  steps <<-STEPS
    Given I fill in the claim details
  STEPS
end
Given(/^I attempt to submit to LAA without specifying all the details$/) do
  steps <<-STEPS
    Given I fill in the claim details
    And I blank out the case number
    And I submit to LAA
  STEPS
end
Given(/^I should be redirected back to the create claim page$/) do
  expect(current_path).to eq '/advocates/claims'
end
And(/^The entered values should be preserved on the page$/) do
  murder_offence_id = Offence.find_by(description: 'Murder').id
  expect(page).to have_selector("input[value='#{murder_offence_id}']")

  expected_drop_down_values = {
            'claim_case_type_id'          => 'Guilty plea',
            'claim_prosecuting_authority' => 'CPS',
            'claim_court_id'              => 'some court',
            'claim_advocate_category'     => 'QC'
          }
  expected_drop_down_values.each do |selector_id, selected_item|
    within('#new_claim') do
      expect(page.has_select?(selector_id, selected: selected_item)).to be true
    end
  end
end
And(/^I should see the error message "(.+)"$/) do | error_message |
  within('.validation-summary') do
    expect(page).to have_content(error_message)
  end
end
And(/^I blank out the case number$/) do
  fill_in 'claim_case_number', with: ""
end
