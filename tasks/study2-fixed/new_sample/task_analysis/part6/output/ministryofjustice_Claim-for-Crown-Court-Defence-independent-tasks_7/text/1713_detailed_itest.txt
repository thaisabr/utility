Classes: 7
[name:Advocate, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/advocate.rb, step:Given ]
[name:Advocates, file:ministryofjustice_Claim-for-Crown-Court-Defence/db/seeds/advocates.rb, step:Given ]
[name:Advocates, file:ministryofjustice_Claim-for-Crown-Court-Defence/spec/factories/advocates.rb, step:Given ]
[name:CaseWorkers, file:ministryofjustice_Claim-for-Crown-Court-Defence/db/seeds/case_workers.rb, step:Given ]
[name:CaseWorkers, file:ministryofjustice_Claim-for-Crown-Court-Defence/spec/factories/case_workers.rb, step:Given ]
[name:User, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/user.rb, step:Given ]
[name:User, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/user.rb, step:When ]

Methods: 25
[name:advocates_landing_url, type:Object, file:null, step:Then ]
[name:case_workers_admin_root_url, type:Object, file:null, step:Then ]
[name:case_workers_root_url, type:Object, file:null, step:Then ]
[name:click_on, type:Object, file:null, step:When ]
[name:click_on, type:Object, file:null, step:Given ]
[name:create, type:Object, file:null, step:Given ]
[name:create_list, type:Object, file:null, step:Given ]
[name:current_path, type:Object, file:null, step:Then ]
[name:current_url, type:Object, file:null, step:Then ]
[name:eq, type:Object, file:null, step:Then ]
[name:fill_in, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:first, type:User, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/user.rb, step:When ]
[name:gsub, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:When ]
[name:have_content, type:Object, file:null, step:Then ]
[name:make_accounts, type:SignInSteps, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/step_definitions/shared/sign_in_steps.rb, step:Given ]
[name:nil?, type:Object, file:null, step:Given ]
[name:page, type:Object, file:null, step:When ]
[name:page, type:Object, file:null, step:Then ]
[name:sign_in, type:UserHelper, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/support/user_helper.rb, step:Given ]
[name:to, type:Object, file:null, step:When ]
[name:to, type:Object, file:null, step:Then ]
[name:to_i, type:Object, file:null, step:Given ]
[name:user, type:Object, file:null, step:Given ]

Referenced pages: 2
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/sessions/new.html.erb
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/shared/_links.html.erb

