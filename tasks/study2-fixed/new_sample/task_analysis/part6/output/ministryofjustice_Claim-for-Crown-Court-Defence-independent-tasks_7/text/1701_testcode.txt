Given(/an? "(.*?)" user account exists$/) do |role|
  make_accounts(role)
end
Given(/^I am a signed in case worker admin$/) do
  @case_worker = create(:case_worker, :admin)
  visit new_user_session_path
  sign_in(@case_worker.user, 'password')
end
def make_accounts(role, number = 1)
  @password = 'password'
  case role
    when 'advocate'
      @advocates = create_list(:advocate, number)
    when 'advocate admin'
      @advocate_admins = create_list(:advocate, number, :admin)
    when 'case worker'
      @case_workers = create_list(:case_worker, number)
    when 'case worker admin'
      create(:case_worker, :admin)
  end
end
  def sign_in(user, password)
    visit new_user_session_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: password
    click_on 'Sign in'
  end
Given(/^There are fee schemes in place$/) do
  Scheme.find_or_create_by(name: 'AGFS Fee Scheme 7', start_date: Date.parse('01/04/2011'), end_date: Date.parse('02/10/2011'))
  Scheme.find_or_create_by(name: 'AGFS Fee Scheme 8', start_date: Date.parse('03/10/2011'), end_date: Date.parse('31/03/2012'))
  Scheme.find_or_create_by(name: 'AGFS Fee Scheme 9', start_date: Date.parse('01/04/2012'), end_date: nil)
end
Given(/^(\d+) case workers? exists?$/) do |quantity|
  @case_workers = create_list(:case_worker, quantity.to_i)
end
Then(/^I should see an admin link$/) do
  find('#primary-nav').click_link('Admin')
  expect(find('h1')).to have_content('Case workers')
end
When(/^I visit my dashboard$/) do
  visit case_workers_claims_path
end
When(/^I visit the user sign in page$/) do
  visit new_user_session_path
end
Given(/^(?:the|that)(?: (\d+)\w+)? advocate admin signs in$/) do |cardinality|
  card = cardinality.nil? ? 1 : cardinality
  @user = @advocate_admins[card.to_i-1].user
  step "I visit the user sign in page"
  step "I enter my email, password and click sign in"
end
When(/^I enter my email, password and click sign in$/) do
  fill_in 'Email', with: (@user || User.first).email
  fill_in 'Password', with: @password || 'password'
  click_on 'Sign in'
  expect(page).to have_content('Sign out')
end
Then(/^I should be redirected to the "(.*?)" root url$/) do |namespace|
  case namespace.gsub(/\s/, '_')
    when 'case workers'
      expect(current_url).to eq(case_workers_root_url)
    when 'case workers admin'
      expect(current_url).to eq(case_workers_admin_root_url)
  end
end
Then(/^I should be redirected to the advocates landing url$/) do
  expect(current_url).to eq(advocates_landing_url)
end
Then(/^I should be redirected to the advocates root url$/) do
  expect(current_url).to eq(advocates_root_url)
end
Then(/^I should see the advocates correct working primary navigation$/) do
  step "I should see the advocates Home link and it should work"
  step "I should see the advocates New Claim link and it should work"
end
Then(/^I should see the admin advocates correct working primary navigation$/) do
  step "I should see the advocates correct working primary navigation"
  step "I should see the advocates Admin link and it should work"
end
Then(/^I should see the advocates Home link and it should work$/) do
  find('#primary-nav').click_link('Home')
  expect(find('.page-title')).to have_content('Claims')
end
Then(/^I should see the advocates New Claim link and it should work$/) do
  find('#primary-nav').click_link('New Claim')
  expect(find('h1')).to have_content('Claim for Advocate Graduated Fees')
end
Then(/^I should see the advocates Admin link and it should work$/) do
  find('#primary-nav').click_link('Admin')
  expect(find('.page-title')).to have_content('Advocates')
end
Then(/^I should see the caseworkers correct working primary navigation$/) do
  step "I should see the caseworkers Home link and it should work"
end
Then(/^I should see the admin caseworkers correct working primary navigation$/) do
  step "I should see the admin caseworkers Summary link and it should work"
  step "I should see the admin caseworkers Allocation link and it should work"
  step "I should see the admin caseworkers Admin link and it should work"
end
Then(/^I should see the caseworkers Home link and it should work$/) do
  find('#primary-nav').click_link('Home')
end
Then(/^I should see the admin caseworkers Summary link and it should work$/) do
  find('#primary-nav').click_link('Summary')
end
Then(/^I should see the admin caseworkers Allocation link and it should work$/) do
  find('#primary-nav').click_link('Allocation')
end
Then(/^I should see the admin caseworkers Admin link and it should work$/) do
  find('#primary-nav').click_link('Admin')
  expect(find('h1')).to have_content('Case workers')
end
