Feature: Unhappy paths
Scenario: Attempt to submit claim to LAA without specifying all fields
Given I am a signed in advocate
And There are case types in place
And I am on the new claim page
And I attempt to submit to LAA without specifying all the details
Then I should be redirected back to the create claim page
And I should see the error message "Case number cannot be blank"
