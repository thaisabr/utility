Classes: 10
[name:Advocate, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/advocate.rb, step:Given ]
[name:Advocate, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/advocate.rb, step:null]
[name:Advocates, file:ministryofjustice_Claim-for-Crown-Court-Defence/db/seeds/advocates.rb, step:Given ]
[name:Advocates, file:ministryofjustice_Claim-for-Crown-Court-Defence/spec/factories/advocates.rb, step:Given ]
[name:CaseWorkers, file:ministryofjustice_Claim-for-Crown-Court-Defence/db/seeds/case_workers.rb, step:Given ]
[name:CaseWorkers, file:ministryofjustice_Claim-for-Crown-Court-Defence/spec/factories/case_workers.rb, step:Given ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:null]
[name:Claims, file:ministryofjustice_Claim-for-Crown-Court-Defence/spec/factories/claims.rb, step:Given ]
[name:Faker, file:null, step:Given ]
[name:User, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/user.rb, step:Given ]

Methods: 46
[name:authorised, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:authorised, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/api/advocates/claims_controller.rb, step:null]
[name:click_button, type:Object, file:null, step:When ]
[name:click_on, type:Object, file:null, step:Given ]
[name:create, type:Object, file:null, step:Given ]
[name:create_list, type:Object, file:null, step:Given ]
[name:description, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:null]
[name:description, type:Fee, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/fee.rb, step:null]
[name:download, type:DocumentsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/documents_controller.rb, step:null]
[name:download, type:RepresentationOrdersController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/representation_orders_controller.rb, step:null]
[name:edit, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:edit, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/api/advocates/claims_controller.rb, step:null]
[name:edit, type:Advocates/admin/advocatesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/admin/advocates_controller.rb, step:null]
[name:fill_in, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:first, type:Object, file:null, step:Given ]
[name:first, type:User, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/user.rb, step:Given ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:Given ]
[name:index, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:index, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/api/advocates/claims_controller.rb, step:null]
[name:index, type:Advocates/admin/advocatesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/admin/advocates_controller.rb, step:null]
[name:last, type:Object, file:null, step:Given ]
[name:make_accounts, type:SignInSteps, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/step_definitions/sign_in_steps.rb, step:Given ]
[name:new, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:new, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/api/advocates/claims_controller.rb, step:null]
[name:new, type:Advocates/admin/advocatesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/admin/advocates_controller.rb, step:null]
[name:outstanding, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:outstanding, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/api/advocates/claims_controller.rb, step:null]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:Given ]
[name:post, type:Object, file:null, step:null]
[name:select, type:Object, file:null, step:When ]
[name:show, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:show, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/api/advocates/claims_controller.rb, step:null]
[name:show, type:Advocates/admin/advocatesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/admin/advocates_controller.rb, step:null]
[name:show, type:DocumentsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/documents_controller.rb, step:null]
[name:show, type:RepresentationOrdersController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/representation_orders_controller.rb, step:null]
[name:split, type:Object, file:null, step:Given ]
[name:to, type:Object, file:null, step:Then ]
[name:to, type:Object, file:null, step:Given ]
[name:to_i, type:Object, file:null, step:Then ]
[name:to_i, type:Object, file:null, step:Given ]
[name:update, type:ClaimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:update, type:ClaimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/api/advocates/claims_controller.rb, step:null]
[name:update, type:ClaimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/claims_controller.rb, step:null]

Referenced pages: 34
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/admin/advocates/_form.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/admin/advocates/edit.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/admin/advocates/index.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/admin/advocates/new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/admin/advocates/show.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_basic_fees.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_case_notes.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_claims.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_date_attended_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_defendant_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_document_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_dynamic_offence_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_evidence_list.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_expense_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_fee_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_financial_summary.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_form.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_nav_tabs.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_representation_order.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_representation_order_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_search_form.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/authorised.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/edit.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/index.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/outstanding.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/show.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/sessions/new.html.erb
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/shared/_links.html.erb
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim_status.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_message.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_messages.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_summary.html.haml

