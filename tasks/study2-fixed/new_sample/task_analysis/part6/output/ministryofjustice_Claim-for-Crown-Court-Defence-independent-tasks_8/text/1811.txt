Feature: Claim evidence checklist
Scenario: Edit claim checklist
Given I am a signed in advocate
And a claim exists with state "draft"
When I am on the claim edit page
Then I should see an evidence checklist section
And I check the first checkbox
And I submit to LAA
And I visit the claim show page
Then I should see a list item for "Representation Order" evidence
