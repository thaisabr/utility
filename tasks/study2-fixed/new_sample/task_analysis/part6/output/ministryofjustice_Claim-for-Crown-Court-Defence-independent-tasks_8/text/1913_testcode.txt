Given(/^I am a signed in advocate$/) do
  step %Q{an "advocate" user account exists}
  @advocate = @advocates.first
  @user     = @advocate.user
  step "I visit the user sign in page"
  step "I enter my email, password and click log in"
end
When(/^I visit the advocates dashboard$/) do
  visit advocates_claims_path
end
When(/^I search by the advocate name "(.*?)"$/) do |name|
  select 'Advocate', from: 'search_field'
  fill_in 'search', with: name
  click_button 'Search'
end
When(/^I search by the defendant name "(.*?)"$/) do |name|
  select 'Defendant', from: 'search_field'
  fill_in 'search', with: name
  click_button 'Search'
end
When(/^I search by the name "(.*?)"$/) do |name|
  fill_in 'search', with: name
  click_button 'Search'
end
Then(/^I should only see the (\d+) claims involving defendant "(.*?)"$/) do |number, name|
  expect(page).to have_content(name, count: number.to_i)
end
Given(/^I have (\d+) claims involving defendant "(.*?)" amongst others$/) do |number,defendant_name|
  @claims = create_list(:draft_claim, number.to_i, advocate: @advocate)
  @claims.each do |claim|
    create(:defendant, claim: claim, first_name: Faker::Name.first_name, last_name: Faker::Name.last_name)
  end
  @claims = create_list(:submitted_claim, number.to_i, advocate: @advocate)
  @claims.each do |claim|
    create(:defendant, claim: claim, first_name: defendant_name.split.first, last_name: defendant_name.split.last)
  end
end
When(/^I enter advocate name of "(.*?)"$/) do |name|
  select 'Advocate', from: 'search_field'
  fill_in 'search', with: name
end
When(/^I enter defendant name of "(.*?)"$/) do |name|
  select 'Defendant', from: 'search_field'
  fill_in 'search', with: name
end
When (/^I hit search button$/) do
  click_button 'Search'
end
Given(/an? "(.*?)" user account exists$/) do |role|
  make_accounts(role)
end
When(/^I visit the user sign in page$/) do
  visit new_user_session_path
end
When(/^I enter my email, password and click log in$/) do
  fill_in 'Email', with: (@user || User.first).email
  fill_in 'Password', with: @password || 'password'
  click_on 'Log in'
  expect(page).to have_content('Sign out')
end
def make_accounts(role, number = 1)
  @password = 'password'
  case role
    when 'advocate'
      @advocates = create_list(:advocate, number)
    when 'advocate admin'
      @advocate_admins = create_list(:advocate, number, :admin)
    when 'case worker'
      @case_workers = create_list(:case_worker, number)
    when 'case worker admin'
      create(:case_worker, :admin)
  end
end
