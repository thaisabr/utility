Classes: 27
[name:ActionController, file:null, step:Then ]
[name:Advocate, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/advocate.rb, step:Given ]
[name:Advocate, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/advocate.rb, step:null]
[name:Advocate, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/advocate.rb, step:Then ]
[name:Advocate, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/advocate.rb, step:When ]
[name:CaseWorker, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/case_worker.rb, step:Given ]
[name:CaseWorkers, file:ministryofjustice_Claim-for-Crown-Court-Defence/db/seeds/case_workers.rb, step:Then ]
[name:CaseWorkers, file:ministryofjustice_Claim-for-Crown-Court-Defence/spec/factories/case_workers.rb, step:Then ]
[name:CaseWorkers, file:ministryofjustice_Claim-for-Crown-Court-Defence/db/seeds/case_workers.rb, step:Given ]
[name:CaseWorkers, file:ministryofjustice_Claim-for-Crown-Court-Defence/spec/factories/case_workers.rb, step:Given ]
[name:Chamber, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/chamber.rb, step:Then ]
[name:Chamber, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/chamber.rb, step:Given ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:Then ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:Then ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:Given ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:Given ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:When ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:When ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:null]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:null]
[name:Claims, file:ministryofjustice_Claim-for-Crown-Court-Defence/spec/factories/claims.rb, step:Given ]
[name:Claims, file:ministryofjustice_Claim-for-Crown-Court-Defence/spec/factories/claims.rb, step:Then ]
[name:Date, file:null, step:Given ]
[name:FactoryGirl, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/support/factory_girl.rb, step:Given ]
[name:Faker, file:null, step:Given ]
[name:Scheme, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/scheme.rb, step:Given ]
[name:User, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/user.rb, step:Given ]

Methods: 151
[name:advocates, type:Object, file:null, step:Given ]
[name:ago, type:Object, file:null, step:When ]
[name:all, type:Object, file:null, step:When ]
[name:all, type:Object, file:null, step:Then ]
[name:all, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:When ]
[name:all, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:When ]
[name:amount_assessed, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:null]
[name:amount_assessed, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:null]
[name:amount_assessed, type:ClaimPresenter, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/presenters/claim_presenter.rb, step:Then ]
[name:attach_file, type:Object, file:null, step:When ]
[name:authorised, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:authorised, type:ClaimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:Then ]
[name:authorised, type:ClaimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:When ]
[name:await_info_from_court!, type:Object, file:null, step:Given ]
[name:be, type:Object, file:null, step:Then ]
[name:body, type:Object, file:null, step:Then ]
[name:browser, type:Object, file:null, step:Then ]
[name:case_number, type:Object, file:null, step:Then ]
[name:case_workers, type:Object, file:null, step:Given ]
[name:check, type:Object, file:null, step:When ]
[name:choose, type:Object, file:null, step:When ]
[name:claims, type:Allocation, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/allocation.rb, step:Then ]
[name:claims, type:Advocate, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/advocate.rb, step:Then ]
[name:claims, type:Advocate, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/advocate.rb, step:When ]
[name:click, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:click_on, type:Object, file:null, step:Given ]
[name:click_on, type:Object, file:null, step:When ]
[name:click_on, type:Object, file:null, step:Then ]
[name:cms_number, type:Object, file:null, step:Then ]
[name:compact, type:Object, file:null, step:Given ]
[name:confirmation, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:count, type:Object, file:null, step:Then ]
[name:create, type:Object, file:null, step:Given ]
[name:create, type:FactoryGirl, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/support/factory_girl.rb, step:Given ]
[name:create_list, type:Object, file:null, step:Given ]
[name:days, type:Object, file:null, step:When ]
[name:documents, type:Object, file:null, step:Given ]
[name:download, type:DocumentsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/documents_controller.rb, step:null]
[name:driver, type:Object, file:null, step:Then ]
[name:each, type:Object, file:null, step:Then ]
[name:each, type:Object, file:null, step:Given ]
[name:each, type:Object, file:null, step:When ]
[name:edit, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:edit, type:Advocates/admin/advocatesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/admin/advocates_controller.rb, step:null]
[name:empty?, type:Object, file:null, step:When ]
[name:empty?, type:Object, file:null, step:Given ]
[name:empty?, type:Object, file:null, step:Then ]
[name:eq, type:Object, file:null, step:Then ]
[name:eq, type:Object, file:null, step:When ]
[name:eql, type:Object, file:null, step:Then ]
[name:eql, type:Object, file:null, step:When ]
[name:fees, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find, type:Object, file:null, step:When ]
[name:find, type:DocType, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/doc_type.rb, step:Then ]
[name:find, type:DocType, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/doc_type.rb, step:When ]
[name:find_by, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:Then ]
[name:find_by, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:Then ]
[name:find_field, type:Object, file:null, step:Then ]
[name:find_field, type:Object, file:null, step:When ]
[name:find_or_create_by, type:Scheme, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/scheme.rb, step:Given ]
[name:first, type:Object, file:null, step:When ]
[name:first, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:Then ]
[name:first, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:Then ]
[name:first, type:Advocate, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/advocate.rb, step:Given ]
[name:first, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:When ]
[name:first, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:When ]
[name:first, type:Chamber, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/chamber.rb, step:Then ]
[name:first, type:Chamber, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/chamber.rb, step:Given ]
[name:first, type:Object, file:null, step:Given ]
[name:first, type:CaseWorker, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/case_worker.rb, step:Given ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_link, type:Object, file:null, step:Then ]
[name:have_selector, type:Object, file:null, step:Then ]
[name:have_selector, type:Object, file:null, step:When ]
[name:headers, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:Given ]
[name:id, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:When ]
[name:include, type:Object, file:null, step:Then ]
[name:include?, type:Object, file:null, step:Then ]
[name:include?, type:Object, file:null, step:When ]
[name:index, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:index, type:Advocates/admin/advocatesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/admin/advocates_controller.rb, step:null]
[name:index, type:CaseWorkers/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/claims_controller.rb, step:null]
[name:join, type:Object, file:null, step:Then ]
[name:last, type:Object, file:null, step:When ]
[name:last, type:Object, file:null, step:Given ]
[name:map, type:Object, file:null, step:Then ]
[name:map, type:Object, file:null, step:Given ]
[name:map, type:Object, file:null, step:When ]
[name:match, type:Object, file:null, step:Then ]
[name:new, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:new, type:Advocates/admin/advocatesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/admin/advocates_controller.rb, step:null]
[name:not_to, type:Object, file:null, step:Then ]
[name:number_to_currency, type:Object, file:null, step:Then ]
[name:outstanding, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:outstanding, type:ClaimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:Then ]
[name:page, type:Object, file:null, step:When ]
[name:page, type:Object, file:null, step:Then ]
[name:pay_part!, type:Object, file:null, step:Given ]
[name:pluck, type:Object, file:null, step:When ]
[name:post, type:Object, file:null, step:null]
[name:raise, type:Object, file:null, step:Given ]
[name:rand, type:Object, file:null, step:When ]
[name:reject!, type:Object, file:null, step:Given ]
[name:response, type:Object, file:null, step:Then ]
[name:save!, type:Object, file:null, step:Given ]
[name:select, type:Object, file:null, step:When ]
[name:send, type:Object, file:null, step:Then ]
[name:show, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:show, type:DocumentsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/documents_controller.rb, step:null]
[name:show, type:Advocates/admin/advocatesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/admin/advocates_controller.rb, step:null]
[name:sign_in, type:UserHelper, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/support/user_helper.rb, step:Given ]
[name:size, type:Object, file:null, step:Then ]
[name:split, type:Object, file:null, step:Given ]
[name:state, type:Object, file:null, step:Then ]
[name:state, type:Object, file:null, step:Given ]
[name:state_machine, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:Given ]
[name:state_machine, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:Given ]
[name:states, type:Object, file:null, step:Given ]
[name:strftime, type:Object, file:null, step:When ]
[name:sum, type:Object, file:null, step:Then ]
[name:text, type:Object, file:null, step:When ]
[name:text, type:Object, file:null, step:Then ]
[name:titlecase, type:Object, file:null, step:When ]
[name:to, type:Object, file:null, step:Then ]
[name:to, type:Object, file:null, step:When ]
[name:to_i, type:Object, file:null, step:Given ]
[name:to_i, type:Object, file:null, step:Then ]
[name:to_not, type:Object, file:null, step:Then ]
[name:to_s, type:DateAttended, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/date_attended.rb, step:Then ]
[name:to_s, type:Location, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/location.rb, step:Then ]
[name:to_s, type:OffenceClass, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/offence_class.rb, step:Then ]
[name:to_sym, type:Object, file:null, step:Then ]
[name:total, type:ClaimPresenter, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/presenters/claim_presenter.rb, step:Given ]
[name:uniq, type:Object, file:null, step:When ]
[name:update, type:ClaimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:update, type:ClaimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/claims_controller.rb, step:null]
[name:update_column, type:Object, file:null, step:Given ]
[name:user, type:Object, file:null, step:Given ]
[name:value, type:Object, file:null, step:Then ]
[name:value, type:Object, file:null, step:When ]
[name:where, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:When ]
[name:where, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:When ]
[name:window_handles, type:Object, file:null, step:Then ]
[name:within, type:Object, file:null, step:When ]
[name:within, type:Object, file:null, step:Then ]

Referenced pages: 42
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/admin/advocates/_form.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/admin/advocates/edit.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/admin/advocates/index.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/admin/advocates/new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/admin/advocates/show.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_basic_fee_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_claims.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_cracked_trial_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_date_attended_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_defendant_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_document_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_dynamic_offence_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_evidence_checklist.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_expense_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_financial_summary.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_fixed_fee_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_form.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_misc_fee_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_nav_tabs.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_representation_order_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_search_form.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/authorised.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/confirmation.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/edit.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/index.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/outstanding.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/show.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/case_workers/claims/_list_controls.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/case_workers/claims/_nav_tabs.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/case_workers/claims/_search_form.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/case_workers/claims/index.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/sessions/new.html.erb
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/shared/_links.html.erb
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim_status.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_evidence_checklist.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_evidence_list.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_message.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_messages.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_search_summary.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_summary.html.haml

