Classes: 2
[name:Document, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/document.rb, step:Then ]
[name:SecureRandom, file:null, step:When ]

Methods: 11
[name:attach_file, type:Object, file:null, step:When ]
[name:be_present, type:Object, file:null, step:Then ]
[name:click_on, type:Object, file:null, step:When ]
[name:create, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find_by, type:Document, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/document.rb, step:Then ]
[name:new, type:DocumentsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/documents_controller.rb, step:null]
[name:new, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:new, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/api/advocates/claims_controller.rb, step:null]
[name:select, type:Object, file:null, step:When ]
[name:to, type:Object, file:null, step:Then ]

Referenced pages: 2
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/documents/new.html.haml

