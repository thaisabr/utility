Feature: Advocate claims list
Scenario: Search claims by advocate name
Given I am a signed in advocate admin
And my chamber has 4 claims for advocate "John Smith"
When I visit the advocates dashboard
And I search by the advocate name "John Smith"
Then I should only see the 4 claims for the advocate "John Smith"
Scenario: No search by advocate name for non-admin
Given I am a signed in advocate
When I visit the advocates dashboard
Then I should not see the advocate search field
