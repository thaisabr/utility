Feature: Advocate claims
Scenario: Change offence class
Given I am a signed in advocate
And I am on the new claim page
When I select offence class "A: Homicide and related grave offences"
Then the Offence category does NOT contain "Activities relating to opium"
Then the Offence category does contain "Murder"
