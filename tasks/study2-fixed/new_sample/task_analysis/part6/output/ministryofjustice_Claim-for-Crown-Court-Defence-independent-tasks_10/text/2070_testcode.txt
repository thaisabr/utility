Given(/^I have been assigned claims with evidence attached$/) do
  @claims.each do |claim|
    DocumentType.find_or_create_by(description: 'The front sheet(s) from the commital bundle')
    file = File.open('./features/examples/longer_lorem.pdf')
    claim.documents << Document.create!(claim_id: claim.id, document: file, document_content_type: 'application/pdf', document_type_id: DocumentType.first.id)
  end
end
Given(/^I am signed in and on the case worker dashboard$/) do
  steps <<-STEPS
    Given I am a signed in case worker
      And claims have been assigned to me
     When I visit my dashboard
     Then I should see only my claims
      And the claims should be sorted by oldest first
  STEPS
end
When(/^I visit the detailed view for a claim$/) do
  first('div.claim-controls').click_link('Detail')
end
Then(/^I see links to view\/download each document submitted with the claim$/) do
  evidence_list = page.all(:css, '#evidence-list li')
  expect(evidence_list.count).to_not eq 0
  evidence_list.each do |evidence_type|
    expect(evidence_type).to have_link 'View'
    expect(evidence_type).to have_link 'Download'
  end
end
When(/^click on a link to (download|view) some evidence$/) do |link|
  if link == 'download'
    first('div.item-controls').click_link('Download')
  elsif link == 'view'
    first('div.item-controls').click_link('View')
  end
end
Then(/^I should get a download with the filename "(.*)"$/) do |filename|
  page.driver.response.headers['Content-Disposition'].should include("filename=\"#{filename}\"")
end
Then(/^I see "longer_lorem.pdf" in my browser$/) do
  expect(page.body).to match(/.*PDF.*/i)
end
Then(/^a new tab opens$/) do
  expect(page.driver.browser.window_handles.count).to eq 2
end
Given(/^an? "(.*?)" user account exists$/) do |role|
  @password = 'password'
  case role
    when 'advocate'
      create(:advocate)
    when 'advocate admin'
      create(:advocate, :admin)
    when 'case worker'
      create(:case_worker)
    when 'case worker admin'
      create(:case_worker, :admin)
  end
end
When(/^I vist the user sign in page$/) do
  visit new_user_session_path
end
When(/^I enter my email, password and click log in$/) do
  fill_in 'Email', with: User.first.email
  fill_in 'Password', with: @password
  click_on 'Log in'
end
Then(/^I should be redirected to the "(.*?)" root url$/) do |namespace|
  case namespace.gsub(/\s/, '_')
  when 'advocates'
    expect(current_url).to eq(advocates_root_url)
  when 'advocates admin'
    expect(current_url).to eq(advocates_admin_root_url)
  when 'case workers'
    expect(current_url).to eq(case_workers_root_url)
  when 'case workers admin'
    expect(current_url).to eq(case_workers_admin_root_url)
  end
end
Given(/^I am a signed in case worker$/) do
  case_worker = create(:case_worker)
  visit new_user_session_path
  sign_in(case_worker.user, 'password')
end
Given(/^claims have been assigned to me$/) do
  case_worker = CaseWorker.first
  @claims = create_list(:submitted_claim, 5)
  @other_claims = create_list(:submitted_claim, 3)
  @claims.each_with_index { |claim, index| claim.update_column(:total, index + 1) }
  @claims.each { |claim| claim.case_workers << case_worker }
  create(:defendant, maat_reference: 'AA1245', claim_id: @claims.first.id)
  create(:defendant, maat_reference: 'BB1245', claim_id: @claims.second.id)
end
When(/^I visit my dashboard$/) do
  visit case_workers_claims_path
end
Then(/^I should see only my claims$/) do
  claim_dom_ids = @claims.map { |c| "claim_#{c.id}" }
  claim_dom_ids.each do |dom_id|
    expect(page).to have_selector("##{dom_id}")
  end

  other_claim_dom_ids = @other_claims.map { |c| "claim_#{c.id}" }
  other_claim_dom_ids.each do |dom_id|
    expect(page).to_not have_selector("##{dom_id}")
  end
end
Then(/^the claims should be sorted by oldest first$/) do
  claim_dom_ids = @claims.sort_by(&:submitted_at).map { |c| "claim_#{c.id}" }
  expect(page.body).to match(/.*#{claim_dom_ids.join('.*')}.*/m)
end
  def sign_in(user, password)
    visit new_user_session_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: password
    click_on 'Log in'
  end
