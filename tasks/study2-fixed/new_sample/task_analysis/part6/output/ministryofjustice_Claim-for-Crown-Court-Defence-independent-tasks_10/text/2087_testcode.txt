Given(/^I am a signed in advocate$/) do
  advocate = create(:advocate)
  visit new_user_session_path
  sign_in(advocate.user, 'password')
end
Given(/^I am on the new claim page$/) do
  create(:court, name: 'some court')
  create(:offence_class, description: 'A: Homicide and related grave offences')
  create(:offence, description: 'Murder')
  create(:document_type, description: 'Other')
  visit new_advocates_claim_path
end
When(/^I fill in the claim details$/) do
  select('Guilty', from: 'claim_case_type')
  select('CPS', from: 'claim_prosecuting_authority')
  fill_in 'Indictment number', with: '123456'
  select('some court', from: 'claim_court_id')
  fill_in 'Case number', with: '123456'
  select('A: Homicide and related grave offences', from: 'claim_offence_class_id')
  select('Murder', from: 'claim_offence_id')
  select('Qc alone', from: 'claim_advocate_category')
  fill_in 'First name', with: 'Foo'
  fill_in 'Last name', with: 'Bar'
  fill_in 'Date of birth', with: '04/10/1980'
  fill_in 'claim_defendants_attributes_0_maat_reference', with: 'aaa1111'
  select 'Other', from: 'claim_documents_attributes_0_document_type_id'
  fill_in 'claim_documents_attributes_0_notes', with: 'Notes'
  attach_file(:claim_documents_attributes_0_document, 'features/examples/shorter_lorem.docx')
end
When(/^I select offence class "(.*?)"$/) do |offence_class|
  select(offence_class, from: 'claim_offence_class_id')
end
Then(/^the Offence category does NOT contain "(.*?)"$/) do |invalid_offence_category|
  expect(page).not_to have_content(invalid_offence_category)
end
Then(/^the Offence category does contain "(.*?)"$/) do |valid_offence_category|
  expect(page).to have_content(valid_offence_category)
end
When(/^I submit the form$/) do
  click_on 'Submit'
end
Then(/^I should be redirected to the claim summary page$/) do
  claim = Claim.first
  expect(page.current_path).to eq(summary_advocates_claim_path(claim))
end
Then(/^I should see the claim total$/) do
  expect(page).to have_content('Total')
end
Given(/^I am on the claim summary page$/) do
  steps <<-STEPS
    Given I am a signed in advocate
      And I am on the new claim page
     When I fill in the claim details
      And I submit the form
     Then I should be redirected to the claim summary page
      And I should see the claim total
  STEPS
end
Then(/^I should be on the claim confirmation page$/) do
  claim = Claim.first
  expect(page.current_path).to eq(confirmation_advocates_claim_path(claim))
end
Then(/^the claim should be submitted$/) do
  claim = Claim.first
  expect(claim).to be_submitted
end
When(/^I click the back button$/) do
  click_link 'Back'
end
Then(/^I should be on the claim edit form$/) do
  claim = Claim.first
  expect(page.current_path).to eq(edit_advocates_claim_path(claim))
end
Then(/^I should be on the claim summary page$/) do
  claim = Claim.first
  expect(page.current_path).to eq(summary_advocates_claim_path(claim))
end
Given(/^a claim exists$/) do
  create(:claim, advocate_id: Advocate.first.id)
end
When(/^I am on the claim edit page$/) do
  claim = Claim.first
  visit edit_advocates_claim_path(claim)
end
  def sign_in(user, password)
    visit new_user_session_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: password
    click_on 'Log in'
  end
Given(/^I have claims$/) do
  advocate = Advocate.first
  @claims = create_list(:submitted_claim, 5)
  @other_claims = create_list(:submitted_claim, 3)
  @claims.each_with_index { |claim, index| claim.update_column(:total, index + 1) }
  @claims.each { |claim| claim.update_column(:advocate_id, advocate.id) }
  create(:defendant, maat_reference: 'AA1245', claim_id: @claims.first.id)
  create(:defendant, maat_reference: 'BB1245', claim_id: @claims.second.id)
end
When(/^I visit the advocates dashboard$/) do
  visit advocates_claims_path
end
Then(/^I should see only claims that I have created$/) do
  claim_dom_ids = @claims.map { |c| "claim_#{c.id}" }
  claim_dom_ids.each do |dom_id|
    expect(page).to have_selector("##{dom_id}")
  end

  other_claim_dom_ids = @other_claims.map { |c| "claim_#{c.id}" }
  other_claim_dom_ids.each do |dom_id|
    expect(page).to_not have_selector("##{dom_id}")
  end
end
Given(/^I am a signed in advocate admin$/) do
  advocate = create(:advocate, :admin)
  visit new_user_session_path
  sign_in(advocate.user, 'password')
end
Given(/^my chamber has claims$/) do
  advocate = Advocate.first
  another_advocate = create(:advocate)
  chamber = create(:chamber)
  chamber.advocates << advocate
  @claims = create_list(:claim, 5)
  @claims.each { |claim| claim.update_column(:advocate_id, another_advocate.id) }
  @other_claims = create_list(:claim, 3)
end
Then(/^I should see my chamber's claims$/) do
  chamber = Chamber.first
  claim_dom_ids = chamber.claims.map { |c| "claim_#{c.id}" }
  claim_dom_ids.each do |dom_id|
    expect(page).to have_selector("##{dom_id}")
  end

  other_claim_dom_ids = @other_claims.map { |c| "claim_#{c.id}" }
  other_claim_dom_ids.each do |dom_id|
    expect(page).to_not have_selector("##{dom_id}")
  end
end
