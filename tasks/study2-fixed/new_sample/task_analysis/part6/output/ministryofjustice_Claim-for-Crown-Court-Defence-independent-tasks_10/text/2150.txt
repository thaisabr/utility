Feature: Caseworker claims list
Scenario: View completed claims
Given I am a signed in case worker
And I have completed claims
When I visit my dashboard
And I click on the Completed Claims tab
Then I should see only my claims
And the claims should be sorted by oldest first
