Feature: Sign in
Scenario: Sign in as a super admin
Given a "super admin" user account exists
When I visit the user sign in page
And I enter my email, password and click sign in
Then I should be redirected to the "super admins" root url
And I should see the superadmins correct working primary navigation
