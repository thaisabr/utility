Classes: 2
[name:Advocate, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/advocate.rb, step:Given ]
[name:User, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/user.rb, step:Given ]

Methods: 18
[name:api_landing, type:PagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/pages_controller.rb, step:Given ]
[name:call, type:Object, file:null, step:Then ]
[name:choose, type:Object, file:null, step:When ]
[name:click_on, type:Object, file:null, step:Given ]
[name:click_on, type:Object, file:null, step:When ]
[name:create, type:Object, file:null, step:Given ]
[name:downcase, type:Object, file:null, step:Then ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:gsub, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_link, type:Object, file:null, step:Then ]
[name:method, type:Object, file:null, step:Then ]
[name:nil?, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:Then ]
[name:sign_in, type:UserHelper, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/support/user_helper.rb, step:Given ]
[name:tandcs, type:PagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/pages_controller.rb, step:Given ]
[name:to, type:Object, file:null, step:Then ]

Referenced pages: 4
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/sessions/new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/shared/_links.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/pages/api_landing.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/pages/tandcs.haml

