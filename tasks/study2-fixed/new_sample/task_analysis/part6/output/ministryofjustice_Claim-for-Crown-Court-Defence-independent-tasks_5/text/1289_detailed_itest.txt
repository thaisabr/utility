Classes: 6
[name:CaseWorker, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/case_worker.rb, step:Given ]
[name:Claims, file:ministryofjustice_Claim-for-Crown-Court-Defence/spec/factories/claims.rb, step:Given ]
[name:Date, file:null, step:Given ]
[name:FactoryGirl, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/support/factory_girl.rb, step:Given ]
[name:Scheme, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/scheme.rb, step:Given ]
[name:User, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/user.rb, step:Given ]

Methods: 27
[name:api_landing, type:PagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/pages_controller.rb, step:Given ]
[name:case_workers, type:Object, file:null, step:Given ]
[name:choose, type:Object, file:null, step:When ]
[name:choose, type:Object, file:null, step:Then ]
[name:click, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:Then ]
[name:click_on, type:Object, file:null, step:Given ]
[name:click_on, type:Object, file:null, step:When ]
[name:create, type:Object, file:null, step:Given ]
[name:create, type:FactoryGirl, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/support/factory_girl.rb, step:Given ]
[name:create_list, type:Object, file:null, step:Given ]
[name:empty?, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find, type:DocType, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/doc_type.rb, step:Then ]
[name:find_or_create_by, type:Scheme, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/scheme.rb, step:Given ]
[name:first, type:Object, file:null, step:When ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_selector, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:Given ]
[name:index, type:CaseWorkers/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/claims_controller.rb, step:null]
[name:page, type:Object, file:null, step:Then ]
[name:sign_in, type:UserHelper, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/support/user_helper.rb, step:Given ]
[name:tandcs, type:PagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/pages_controller.rb, step:Given ]
[name:to, type:Object, file:null, step:Then ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 10
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/case_workers/claims/_claims.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/case_workers/claims/_claims_list.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/case_workers/claims/_list_controls.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/case_workers/claims/_search_form.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/case_workers/claims/index.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/sessions/new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/shared/_links.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/pages/api_landing.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/pages/tandcs.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_search_summary.html.haml

