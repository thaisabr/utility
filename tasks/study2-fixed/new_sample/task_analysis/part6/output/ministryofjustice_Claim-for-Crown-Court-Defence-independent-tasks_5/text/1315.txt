Feature: Claim status
Scenario Outline: Update claim status
Given I am a signed in case worker
And There are fee schemes in place
And claims have been assigned to me
When I visit my dashboard
And I view status details for a claim
And I select status <status> from select
And I enter fees assessed of <fees> and expenses assessed of <expenses>
And I press update button
Then I should be able to update the status from <status>
And I should see an option select for claim status
Examples:
| status             | fees     | expenses | total     |
| "Part authorised"  | "100.01" | "20.33"  | "£120.34" |
| "Authorised"       | "200.01" | ""       | "£200.01" |
| "Refused"          | ""       | ""       | ""        |
Scenario: Update claim status to rejected
Given I am a signed in case worker
And There are fee schemes in place
And claims have been assigned to me
When I visit my dashboard
And I view status details for a claim
And I select status "Rejected" from select
And I press update button
Then I should not see status select
And I should see the current status set to "Rejected"
Scenario Outline: View claim status
Given I am a signed in advocate
And There are fee schemes in place
And I have 3 allocated claims whos status is <status> with fees assessed of <fees> and expenses assessed of <expenses>
When I visit the advocates dashboard
And I view status details of my first claim
Then I should not see status select
And I should see the current status set to <status>
And I should see "disabled" total assessed value of <total>
Examples:
| status              | fees     |  expenses   | total      |
| "Part authorised"   | "60.01"  |  "40.00"    | "£100.01"  |
| "Rejected"          | ""       |  ""         | ""         |
