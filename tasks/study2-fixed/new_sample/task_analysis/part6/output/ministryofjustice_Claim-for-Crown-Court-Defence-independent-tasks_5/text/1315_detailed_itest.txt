Classes: 10
[name:Advocate, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/advocate.rb, step:Given ]
[name:Advocate, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/advocate.rb, step:When ]
[name:CaseWorker, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/case_worker.rb, step:Given ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:When ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:When ]
[name:Claims, file:ministryofjustice_Claim-for-Crown-Court-Defence/spec/factories/claims.rb, step:Given ]
[name:Date, file:null, step:Given ]
[name:FactoryGirl, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/support/factory_girl.rb, step:Given ]
[name:Scheme, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/scheme.rb, step:Given ]
[name:User, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/user.rb, step:Given ]

Methods: 53
[name:api_landing, type:PagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/pages_controller.rb, step:Given ]
[name:assessment, type:Object, file:null, step:Given ]
[name:authorise_part!, type:Object, file:null, step:Given ]
[name:authorised, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:case_workers, type:Object, file:null, step:Given ]
[name:click, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_on, type:Object, file:null, step:Given ]
[name:create, type:Object, file:null, step:Given ]
[name:create, type:FactoryGirl, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/support/factory_girl.rb, step:Given ]
[name:create_list, type:Object, file:null, step:Given ]
[name:download, type:DocumentsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/documents_controller.rb, step:null]
[name:download_attachment, type:MessagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/messages_controller.rb, step:null]
[name:each, type:Object, file:null, step:Given ]
[name:edit, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:empty?, type:Object, file:null, step:When ]
[name:empty?, type:Object, file:null, step:Given ]
[name:empty?, type:Object, file:null, step:Then ]
[name:eql, type:Object, file:null, step:Then ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find_by_id, type:Object, file:null, step:Then ]
[name:find_or_create_by, type:Scheme, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/scheme.rb, step:Given ]
[name:first, type:Object, file:null, step:When ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_selector, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:Given ]
[name:index, type:CaseWorkers/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/claims_controller.rb, step:null]
[name:index, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:new, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:not_to, type:Object, file:null, step:Then ]
[name:outstanding, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:page, type:Object, file:null, step:Then ]
[name:raise, type:Object, file:null, step:Given ]
[name:reject!, type:Object, file:null, step:Given ]
[name:select, type:Object, file:null, step:When ]
[name:send, type:Object, file:null, step:null]
[name:show, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:show, type:DocumentsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/documents_controller.rb, step:null]
[name:sign_in, type:UserHelper, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/support/user_helper.rb, step:Given ]
[name:tandcs, type:PagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/pages_controller.rb, step:Given ]
[name:text, type:Object, file:null, step:Then ]
[name:to, type:Object, file:null, step:Then ]
[name:to_i, type:Object, file:null, step:Given ]
[name:update, type:AdvocatesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/admin/advocates_controller.rb, step:null]
[name:update, type:ClaimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:null]
[name:update, type:CaseWorkersController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/admin/case_workers_controller.rb, step:null]
[name:update, type:ClaimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/claims_controller.rb, step:null]
[name:update, type:UserMessageStatusesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/user_message_statuses_controller.rb, step:null]
[name:update!, type:Object, file:null, step:Given ]
[name:where, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/advocates/claim.rb, step:When ]
[name:where, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:When ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 44
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_basic_fee_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_claims.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_cracked_trial_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_date_attended_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_defendant_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_evidence_checklist.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_expense_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_financial_summary.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_fixed_fee_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_form.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_json_document_importer.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_misc_fee_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_offence_select.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_representation_order_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_search_form.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/authorised.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/edit.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/index.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/outstanding.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/show.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/case_workers/claims/_claims.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/case_workers/claims/_claims_list.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/case_workers/claims/_list_controls.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/case_workers/claims/_search_form.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/case_workers/claims/index.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/sessions/new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/shared/_links.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/pages/api_landing.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/pages/tandcs.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim_header.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim_status.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_determination_amounts.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_determinations_form.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_determinations_table.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_evidence_checklist.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_evidence_list.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_message.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_message_list.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_messages.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_search_summary.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_summary.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_vat_summary.html.haml

