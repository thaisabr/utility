Given(/^I am a signed in case worker$/) do
  @case_worker = create(:case_worker)
  visit new_user_session_path
  sign_in(@case_worker.user, 'password')
end
  def sign_in(user, password)
    visit new_user_session_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: password
    click_on 'Sign in'
  end
Given(/^There are fee schemes in place$/) do
  Scheme.find_or_create_by(name: 'AGFS Fee Scheme 7', start_date: Date.parse('01/04/2011'), end_date: Date.parse('02/10/2011'))
  Scheme.find_or_create_by(name: 'AGFS Fee Scheme 8', start_date: Date.parse('03/10/2011'), end_date: Date.parse('31/03/2012'))
  Scheme.find_or_create_by(name: 'AGFS Fee Scheme 9', start_date: Date.parse('01/04/2012'), end_date: nil)
end
Given(/^claims have been assigned to me$/) do
  @claims = create_list(:allocated_claim, 5)
  @other_claims = create_list(:allocated_claim, 3)
  @claims.each { |claim| claim.case_workers << @case_worker }
  create :defendant, claim_id: @claims.first.id, representation_orders: [ FactoryGirl.create(:representation_order, maat_reference: '7418529635') ]
  create :defendant, claim_id: @claims.second.id, representation_orders: [ FactoryGirl.create(:representation_order, maat_reference: '9516249873') ]
end
When(/^I visit my dashboard$/) do
  visit case_workers_claims_path
end
When(/^I view status details for a claim$/) do
	within(".claims_table") do
	  first('a.js-test-case-number-link').click
	end
end
When(/^I select status "(.*?)" from select$/) do |status|
	select "#{status}", :from => "claim_state_for_form"
end
When(/^I enter fees assessed of "(.*?)" and expenses assessed of "(.*?)"$/) do |fees, expenses|
  fill_in 'claim_assessment_attributes_fees', with: fees unless fees.empty?
  fill_in 'claim_assessment_attributes_expenses', with: expenses unless expenses.empty?
	# find('div#amountAssessed').fill_in "Amount assessed", with: amount unless amount.empty?
end
When(/^I press update button$/) do
  click_button "Update"
end
Then(/^I should be able to update the status from "(.*)"$/) do |state|
  expect(page).to have_content("Current status: #{state}")
end
