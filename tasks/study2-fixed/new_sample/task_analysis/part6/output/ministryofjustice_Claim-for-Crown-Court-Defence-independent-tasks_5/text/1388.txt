Feature: Caseworker claims list
Scenario: View my current claims
Given I am a signed in case worker admin
And claims have been assigned to me
When I visit my dashboard
And I click "Your Claims"
Then I should see only my claims
And I should see the claims sorted by oldest first
Scenario: View all archived claims
Given I am a signed in case worker admin
And there are archived claims
When I visit my dashboard
And I click "Archive"
Then I should see all archived claims
And I should see the claims sorted by oldest first
Scenario: View allocation tool
Given I am a signed in case worker admin
And there are unallocated claims
When I visit my dashboard
And I click "Allocation"
Then I should see the unallocated claims
Scenario: View re-allocation tool
Given I am a signed in case worker admin
And there are allocated claims
When I visit my dashboard
And I click "Re-allocation"
Then I should see the allocated claims
Scenario: View case workers
Given I am a signed in case worker admin
And There are fee schemes in place
And 2 case workers exist
When I visit my dashboard
Then I should see an admin link and it should work
And I should see the case workers edit and delete link
Feature: Caseworker claims list
Scenario: View archived claims
Given I am a signed in case worker
And There are fee schemes in place
And I have archived claims
When I visit my dashboard
And I click "Archive"
Then I should see only my claims
And I should see the claims sorted by oldest first
Scenario Outline: Search my "Current" and "Archive" claims
Given I am a signed in case worker
And I have 3 "allocated" claims involving defendant "Joex Bloggs"
And I have 1 "allocated" claims involving defendant "Fred Bloggs"
And I have 2 "authorised" claims involving defendant "Joex Bloggs"
And I have 3 "authorised" claims involving defendant "Fred Bloggs"
And I have 1 "part_authorised" claims involving defendant "Fred Bloggs"
And I have 2 "part_authorised" claims involving defendant "Someone Else"
When I visit my dashboard
And I search claims by defendant name <defendant_name>
Then I should only see <current_number> claims
When I click "Archive"
And I search claims by defendant name <defendant_name>
Then I should only see <archive_number> claims
Examples:
| defendant_name | current_number | archive_number |
| "Joex Bloggs"  | 3              | 2              |
| "Fred Bloggs"  | 1              | 4              |
| "Bloggs"       | 4              | 6              |
