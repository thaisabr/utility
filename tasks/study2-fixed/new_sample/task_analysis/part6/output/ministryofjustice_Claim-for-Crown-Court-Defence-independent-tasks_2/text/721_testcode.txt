Given(/^I am a signed in advocate$/) do
  @advocate = create(:external_user, :advocate)
  visit new_user_session_path
  sign_in(@advocate.user, 'password')
end
Given(/^I am a signed in advocate admin$/) do
  @advocate = create(:external_user, :advocate_and_admin)
  visit new_user_session_path
  sign_in(@advocate.user, 'password')
end
  def sign_in(user, password)
    visit new_user_session_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: password
    click_on 'Sign in'
  end
Given(/^There are case and fee types in place$/) do
  load "#{Rails.root}/db/seeds/case_types.rb"
  load "#{Rails.root}/db/seeds/fee_types.rb"
end
Given(/^There are certification types in place$/) do
  load "#{Rails.root}/db/seeds/certification_types.rb"
end
Given(/^There are courts, offences and expense types in place$/) do
  create(:court, name: 'some court')
  offence_class = OffenceClass.find_by(class_letter: "A")
  if offence_class.nil?
    create(:offence_class, class_letter: 'A', description: 'A: Homicide and related grave offences')
  else
    offence_class.update(description: 'A: Homicide and related grave offences')
  end
  create(:offence, description: 'Murder')
  create(:expense_type, name: 'Parking')
  create(:expense_type, name: 'Hotel accommodation')
end
Given(/^I am on the 'Your claims' page$/) do
  @external_user_home_page.load
end
Given(/^I click 'Start a claim'$/) do
  @external_user_home_page.start_a_claim.click
end
Then(/^I should be on the new claim page$/) do
  expect(@claim_form_page).to be_displayed
end
When(/^I select an advocate category of '(.*?)'$/) do |name|
  @claim_form_page.claim_advocate_category_junior_alone.click
end
When(/^I select an advocate$/) do
  @claim_form_page.select_advocate "Doe, John: AC135"
end
When(/^I select a court$/) do
  @claim_form_page.select_court "some court"
end
When(/^I select a case type of '(.*?)'$/) do |name|
  @claim_form_page.select_case_type name
end
When(/^I enter a case number of '(.*?)'$/) do |number|
  @claim_form_page.case_number.set number
end
When(/^I select an offence category$/) do
  @claim_form_page.select_offence_category "Murder"
end
When(/I enter trial start and end dates$/) do
  sleep 3
  @claim_form_page.trial_details.first_day_of_trial.set_date 9.days.ago.to_s
  @claim_form_page.trial_details.trial_concluded_on.set_date 2.days.ago.to_s
  @claim_form_page.trial_details.actual_trial_length.set 8
end
When(/^I enter defendant, representation order and MAAT reference$/) do
  @claim_form_page.defendants.first.first_name.set "Bob"
  @claim_form_page.defendants.first.last_name.set "Billiards"
  @claim_form_page.defendants.first.dob.set_date "1955-01-01"
  @claim_form_page.defendants.last.representation_orders.first.date.set_date "2016-01-01"
  @claim_form_page.defendants.last.representation_orders.first.maat_reference.set "1234567890"
end
When(/^I save as draft$/) do
  @claim_form_page.save_to_drafts.trigger('click')
end
Then(/^I should see '(.*?)'$/) do |content|
  expect(@claim_form_page).to have_content(content)
end
Given(/^I am later on the Your claims page$/) do
  @external_user_home_page.load
end
When(/I click the claim '(.*?)'$/) do |case_number|
  @external_user_home_page.claim_for(case_number).case_number.click
end
When(/I edit this claim/) do
  @external_user_claim_show_page.edit_this_claim.click
end
When(/^I add another defendant, representation order and MAAT reference$/) do
  @claim_form_page.add_another_defendant.click
  @claim_form_page.defendants.last.first_name.set "Ned"
  @claim_form_page.defendants.last.last_name.set "Kelly"
  @claim_form_page.defendants.last.dob.set_date "1912-12-12"
  @claim_form_page.defendants.last.add_another_representation_order.click
  @claim_form_page.defendants.last.representation_orders.first.date.set_date "2016-01-01"
  @claim_form_page.defendants.last.representation_orders.first.maat_reference.set "1234567890"
end
When(/^I add a basic fee with dates attended$/) do
  @claim_form_page.initial_fees.basic_fee.quantity.set "1"
  @claim_form_page.initial_fees.basic_fee.rate.set "3.45"
  @claim_form_page.initial_fees.basic_fee.add_dates.click
  @claim_form_page.initial_fees.basic_fee_dates.from.set_date "2016-01-02"
  @claim_form_page.initial_fees.basic_fee_dates.to.set_date "2016-01-03"
end
When(/^I add a daily attendance fee with dates attended$/) do
  @claim_form_page.initial_fees.daily_attendance_fee_3_to_40.quantity.set "4"
  @claim_form_page.initial_fees.daily_attendance_fee_3_to_40.rate.set "45.77"
  @claim_form_page.initial_fees.daily_attendance_fee_3_to_40.add_dates.click
  @claim_form_page.initial_fees.daily_attendance_fee_3_to_40_dates.from.set_date "2016-01-04"
  @claim_form_page.initial_fees.daily_attendance_fee_3_to_40_dates.to.set_date "2016-01-05"
end
When(/^I add a miscellaneous fee '(.*?)' with dates attended$/) do |name|
  @claim_form_page.add_misc_fee_if_required
  @claim_form_page.miscellaneous_fees.last.select_fee_type name
  @claim_form_page.miscellaneous_fees.last.quantity.set 1
  @claim_form_page.miscellaneous_fees.last.rate.set "34.56"
  @claim_form_page.miscellaneous_fees.last.add_dates.trigger "click"
  @claim_form_page.miscellaneous_fees.last.dates.from.set_date "2016-01-02"
  @claim_form_page.miscellaneous_fees.last.dates.to.set_date "2016-01-03"
end
When(/^I add a fixed fee '(.*?)'$/) do |name|
  @claim_form_page.fixed_fees.last.select_fee_type name
  @claim_form_page.fixed_fees.last.quantity.set 1
  @claim_form_page.fixed_fees.last.rate.set "12.34"
end
When(/^I add an expense '(.*?)'$/) do |name|
  @claim_form_page.expenses.last.expense_type_dropdown.select name
  if name == 'Hotel accommodation'
    @claim_form_page.expenses.last.destination.set 'Liverpool'
  end
  @claim_form_page.expenses.last.reason_for_travel_dropdown.select 'View of crime scene'
  @claim_form_page.expenses.last.amount.set '34.56'
  @claim_form_page.expenses.last.expense_date.set_date '2016-01-02'
end
When(/^I upload (\d+) documents$/) do |count|
  @document_count = count.to_i
  @claim_form_page.attach_evidence(@document_count)
end
When(/^I check the boxes for the uploaded documents$/) do
  @claim_form_page.check_evidence_checklist(@document_count)
end
When(/^I add some additional information$/) do
  @claim_form_page.additional_information.set "Bish bosh bash"
end
When(/^I click "Continue" in the claim form$/) do
  @claim_form_page.continue.click
end
When(/^I click Submit to LAA$/) do
  @claim_form_page.submit_to_laa.trigger "click"
end
Then(/^I should be on the check your claim page$/) do
  @claim_summary_page.wait_for_continue # Allow summary page to appear
  expect(@claim_summary_page).to be_displayed
end
When(/^I click "Continue"$/) do
  @claim_summary_page.continue.click
end
Then(/^I should be on the certification page$/) do
  expect(@certification_page).to be_displayed
end
When(/^I check “I attended the main hearing”$/) do
  @certification_page.attended_main_hearing.click
end
When(/^I click Certify and submit claim$/) do
  @certification_page.certify_and_submit_claim.trigger "click"
end
Then(/^I should be on the page showing basic claim information$/) do
  expect(@confirmation_page).to be_displayed
end
When(/^I click View your claims$/) do
  @confirmation_page.view_your_claims.click
end
Then(/^I should be on the your claims page$/) do
  expect(@external_user_home_page).to be_displayed
end
Then(/^Claim '(.*?)' should be listed with a status of '(.*?)'$/) do |case_number, status|
  my_claim = @external_user_home_page.claim_for(case_number)
  expect(my_claim).not_to be_nil
  expect(my_claim.state.text).to eq(status)
end
Given(/^There are other advocates in my provider$/) do
  FactoryGirl.create(:external_user,
                     :advocate,
                     provider: @advocate.provider,
                     user: FactoryGirl.create(:user, first_name: 'John', last_name: 'Doe'),
                     supplier_number: 'AC135')
  FactoryGirl.create(:external_user,
                     :advocate,
                     provider: @advocate.provider,
                     user: FactoryGirl.create(:user, first_name: 'Joe', last_name: 'Blow'),
                     supplier_number: 'XY455')
end
  def select_fee_type(name)
    id = select2_container[:id]
    select2 name, from: id
  end
  def select_advocate(name)
    select2 name, from: "claim_external_user_id"
  end
  def select_court(name)
    select2 name, from: "claim_court_id"
  end
  def select_case_type(name)
    select2 name, from: "claim_case_type_id"
  end
  def select_offence_category(name)
    select2 name, from: "offence_category_description"
  end
  def add_misc_fee_if_required
    if miscellaneous_fees.last.populated?
      add_another_miscellaneous_fee.trigger "click"
    end
  end
  def attach_evidence(count = 1)
    available_docs = Dir.glob "spec/fixtures/files/*.pdf"

    available_docs[0...count].each do |path|
      puts "      Attaching #{path}"
      drag_and_drop_file("dropzone", path)
    end
  end
  def check_evidence_checklist(count = 1)
    evidence_checklist[0...count].each { |item| item.check.trigger "click" }
  end
  def set_date(date_string)
    date = Time.parse(date_string)
    self.day.set date.day.to_s
    self.month.set date.month.to_s
    self.year.set date.year.to_s
  end
  def select2(value, options)
    select "#{value}", :from => "#{options[:from]}", :visible => false
  end
  def populated?
    rate.value.size > 0
  end
  def drag_and_drop_file(selector, file)
    page.execute_script <<-JS
      tempFileInput = window.$('<input/>').attr(
        {id: 'temp-file-input', type:'file'}
      ).appendTo('body');
    JS

    attach_file('temp-file-input', file)

    page.execute_script('fileList = [tempFileInput.get(0).files[0]];')

    page.execute_script <<-JS
      e = jQuery.Event('drop', { dataTransfer : { files : fileList } });
      $('.#{selector}')[0].dropzone.listeners[0].events.drop(e);
      $('#temp-file-input').remove();
    JS
  end
