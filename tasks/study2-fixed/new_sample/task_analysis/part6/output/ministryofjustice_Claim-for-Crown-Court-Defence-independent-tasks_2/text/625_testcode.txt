Given(/^I am a signed in litigator$/) do
  @litigator = create(:external_user, :litigator)
  visit new_user_session_path
  sign_in(@litigator.user, 'password')
end
  def sign_in(user, password)
    visit new_user_session_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: password
    click_on 'Sign in'
  end
And(/^I select the fee scheme '(.*)'$/) do |fee_scheme|
  method_name = fee_scheme.downcase.gsub(' ', '_').to_sym
  @fee_scheme_selector.send(method_name).click
  @fee_scheme_selector.continue.click
end
Given(/^I am allowed to submit transfer claims$/) do
  allow(Settings).to receive(:allow_lgfs_transfer_fees?).and_return(true)
end
And(/^My provider has supplier numbers$/) do
  %w(1A222Z 2B333Z).each do |number|
    @litigator.provider.supplier_numbers << SupplierNumber.new(supplier_number: number)
  end
end
Then(/^I should be on the litigator new transfer claim page$/) do
  expect(@transfer_claim_form_page).to be_displayed
end
When(/^I select the supplier number '(.*)'$/) do |number|
  @litigator_claim_form_page.select_supplier_number(number)
end
And(/^I select the offence class '(.*)'$/) do |name|
  @litigator_claim_form_page.select_offence_class(name)
end
And(/^I enter the case concluded date$/) do
  @litigator_claim_form_page.case_concluded_date.set_date "2016-01-01"
end
And(/^I add (?:a|another) disbursement '(.*)' with net amount '(.*)' and vat amount '(.*)'$/) do |name, net_amount, vat_amount|
  @litigator_claim_form_page.add_disbursement_if_required
  @litigator_claim_form_page.disbursements.last.select_fee_type name
  @litigator_claim_form_page.disbursements.last.net_amount.set net_amount
  @litigator_claim_form_page.disbursements.last.vat_amount.set vat_amount
end
And(/^I fill in '(.*)' as the transfer fee total$/) do |total|
  @transfer_claim_form_page.transfer_fee_total.set total
end
And(/^I choose the litigator type option '(.*)'$/) do |option|
  @transfer_claim_form_page.litigator_type_new.click if option.downcase == 'original'
  @transfer_claim_form_page.litigator_type_new.click if option.downcase == 'new'
end
And(/^I choose the elected case option '(.*)'$/) do |option|
  @transfer_claim_form_page.elected_case_yes.click if option.downcase == 'yes'
  @transfer_claim_form_page.elected_case_no.click if option.downcase == 'no'
end
And(/^I select the transfer stage '(.*)'$/) do |name|
  @transfer_claim_form_page.select_transfer_stage(name)
end
And(/^I enter the transfer date '(.*)'$/) do |date_string|
  @transfer_claim_form_page.transfer_date.set_date date_string
end
Then(/^I select a case conclusion of '(.*)'$/) do |name|
  @transfer_claim_form_page.select_case_conclusion(name)
end
  def select_supplier_number(number)
    select2 number, from: "claim_supplier_number"
  end
  def select_offence_class(name)
    select2 name, from: "claim_offence_id"
  end
  def add_disbursement_if_required
    if disbursements.last.populated?
      add_another_disbursement.trigger "click"
    end
  end
  def set_date(date_string)
    date = Time.parse(date_string)
    self.day.set date.day.to_s
    self.month.set date.month.to_s
    self.year.set date.year.to_s
  end
  def select_fee_type(name)
    id = select2_container[:id]
    select2 name, from: id
  end
  def select_transfer_stage(name)
    select2 name, from: "claim_transfer_detail_attributes_transfer_stage_id"
  end
  def select_case_conclusion(name)
    select2 name, from: "claim_transfer_detail_attributes_case_conclusion_id"
  end
  def select2(value, options)
    select "#{value}", :from => "#{options[:from]}", :visible => false
  end
  def populated?
    net_amount.value.size > 0
  end
  def populated?
    rate.value.size > 0
  end
Given(/^I am on the 'Your claims' page$/) do
  @external_user_home_page.load
end
Given(/^I click 'Start a claim'$/) do
  @external_user_home_page.start_a_claim.click
end
When(/^I select the court '(.*?)'$/) do |name|
  @claim_form_page.select_court(name)
end
When(/^I select a case type of '(.*?)'$/) do |name|
  @claim_form_page.select_case_type name
end
When(/^I enter a case number of '(.*?)'$/) do |number|
  @claim_form_page.case_number.set number
end
When(/^I enter defendant, representation order and MAAT reference$/) do
  @claim_form_page.defendants.first.first_name.set "Bob"
  @claim_form_page.defendants.first.last_name.set "Billiards"
  @claim_form_page.defendants.first.dob.set_date "1955-01-01"
  @claim_form_page.defendants.last.representation_orders.first.date.set_date "2016-01-01"
  @claim_form_page.defendants.last.representation_orders.first.maat_reference.set "1234567890"
end
When(/^I save as draft$/) do
  @claim_form_page.save_to_drafts.trigger('click')
end
Then(/^I should see '(.*?)'$/) do |content|
  expect(page).to have_content(content)
end
Given(/^I am later on the Your claims page$/) do
  @external_user_home_page.load
end
When(/I click the claim '(.*?)'$/) do |case_number|
  @external_user_home_page.claim_for(case_number).case_number.click
end
When(/I edit this claim/) do
  @external_user_claim_show_page.edit_this_claim.click
end
When(/^I add another defendant, representation order and MAAT reference$/) do
  @claim_form_page.add_another_defendant.click
  @claim_form_page.defendants.last.first_name.set "Ned"
  @claim_form_page.defendants.last.last_name.set "Kelly"
  @claim_form_page.defendants.last.dob.set_date "1912-12-12"
  @claim_form_page.defendants.last.add_another_representation_order.click
  @claim_form_page.defendants.last.representation_orders.first.date.set_date "2016-01-01"
  @claim_form_page.defendants.last.representation_orders.first.maat_reference.set "1234567890"
end
When(/^I upload (\d+) documents?$/) do |count|
  @document_count = count.to_i
  @claim_form_page.attach_evidence(@document_count)
end
When(/^I check the boxes for the uploaded documents$/) do
  @claim_form_page.check_evidence_checklist(@document_count)
end
When(/^I add some additional information$/) do
  @claim_form_page.additional_information.set "Bish bosh bash"
end
When(/^I click "Continue" in the claim form$/) do
  @claim_form_page.continue.click
end
When(/^I click Submit to LAA$/) do
  @claim_form_page.submit_to_laa.trigger "click"
end
Then(/^I should be on the check your claim page$/) do
  @claim_summary_page.wait_for_continue # Allow summary page to appear
  expect(@claim_summary_page).to be_displayed
end
When(/^I click "Continue"$/) do
  @claim_summary_page.continue.click
end
Then(/^I should be on the certification page$/) do
  expect(@certification_page).to be_displayed
end
When(/^I click Certify and submit claim$/) do
  @certification_page.certify_and_submit_claim.trigger "click"
end
Then(/^I should be on the page showing basic claim information$/) do
  expect(@confirmation_page).to be_displayed
end
When(/^I click View your claims$/) do
  @confirmation_page.view_your_claims.click
end
Then(/^I should be on the your claims page$/) do
  expect(@external_user_home_page).to be_displayed
end
Then(/^Claim '(.*?)' should be listed with a status of '(.*?)'$/) do |case_number, status|
  my_claim = @external_user_home_page.claim_for(case_number)
  expect(my_claim).not_to be_nil
  expect(my_claim.state.text).to eq(status)
end
  def select_court(name)
    select2 name, from: "claim_court_id"
  end
  def select_case_type(name)
    select2 name, from: "claim_case_type_id"
  end
  def attach_evidence(count = 1)
    available_docs = Dir.glob "spec/fixtures/files/*.pdf"

    available_docs[0...count].each do |path|
      puts "      Attaching #{path}"
      drag_and_drop_file("dropzone", path)
    end
  end
  def check_evidence_checklist(count = 1)
    evidence_checklist[0...count].each { |item| item.check.trigger "click" }
  end
  def drag_and_drop_file(selector, file)
    page.execute_script <<-JS
      tempFileInput = window.$('<input/>').attr(
        {id: 'temp-file-input', type:'file'}
      ).appendTo('body');
    JS

    attach_file('temp-file-input', file)

    page.execute_script('fileList = [tempFileInput.get(0).files[0]];')

    page.execute_script <<-JS
      e = jQuery.Event('drop', { dataTransfer : { files : fileList } });
      $('.#{selector}')[0].dropzone.listeners[0].events.drop(e);
      $('#temp-file-input').remove();
    JS
  end
