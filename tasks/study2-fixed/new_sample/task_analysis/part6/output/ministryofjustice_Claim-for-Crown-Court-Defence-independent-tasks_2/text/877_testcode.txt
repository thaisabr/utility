When(/^I start a claim/) do
  find('.primary-nav-bar').click_link('Start a claim')
end
Then(/^I should be redirected to the claim scheme choice page$/) do
  expect(page.current_path).to eq(external_users_claims_claim_options_path)
end
Given(/^I am a signed in admin for an AGFS and LGFS firm$/) do
  @admin = create(:external_user, :agfs_lgfs_admin)
  visit new_user_session_path
  sign_in(@admin.user, 'password')
end
  def sign_in(user, password)
    visit new_user_session_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: password
    click_on 'Sign in'
  end
