Feature: Caseworker claims list
Scenario: View current claims
Given I am a signed in case worker
And claims have been assigned to me
When I visit my dashboard
Then I should see only my claims
And I should see the claims sorted by oldest first
Scenario: View completed claims
Given I am a signed in case worker
And I have completed claims
When I visit my dashboard
And I click on the Completed Claims tab
Then I should see only my claims
And I should see the claims sorted by oldest first
Scenario: Sort current claims by oldest first
Given I am signed in and on the case worker dashboard
When I sort the claims by oldest first
Then I should see the claims sorted by oldest first
