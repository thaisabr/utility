Classes: 8
[name:CaseWorker, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/case_worker.rb, step:Given ]
[name:Claims, file:ministryofjustice_Claim-for-Crown-Court-Defence/spec/factories/claims.rb, step:Given ]
[name:Document, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/document.rb, step:Then ]
[name:Document, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/document.rb, step:Given ]
[name:DocumentType, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/document_type.rb, step:Given ]
[name:File, file:null, step:Given ]
[name:SecureRandom, file:null, step:When ]
[name:User, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/user.rb, step:Given ]

Methods: 39
[name:attach_file, type:Object, file:null, step:When ]
[name:be_present, type:Object, file:null, step:Then ]
[name:body, type:Object, file:null, step:Given ]
[name:case_workers, type:Object, file:null, step:Given ]
[name:click_link, type:Object, file:null, step:When ]
[name:click_on, type:Object, file:null, step:When ]
[name:click_on, type:Object, file:null, step:Given ]
[name:create, type:Object, file:null, step:Given ]
[name:create!, type:Document, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/document.rb, step:Given ]
[name:create_list, type:Object, file:null, step:Given ]
[name:documents, type:Object, file:null, step:Given ]
[name:driver, type:Object, file:null, step:Then ]
[name:each, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:find_by, type:Document, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/document.rb, step:Then ]
[name:find_or_create_by, type:DocumentType, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/document_type.rb, step:Given ]
[name:first, type:DocumentType, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/document_type.rb, step:Given ]
[name:first, type:Object, file:null, step:When ]
[name:first, type:CaseWorker, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/case_worker.rb, step:Given ]
[name:have_selector, type:Object, file:null, step:Given ]
[name:headers, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:Given ]
[name:include, type:Object, file:null, step:Then ]
[name:index, type:CaseWorkers/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/claims_controller.rb, step:Given ]
[name:join, type:Object, file:null, step:Given ]
[name:map, type:Object, file:null, step:Given ]
[name:match, type:Object, file:null, step:Given ]
[name:new, type:DocumentsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/documents_controller.rb, step:null]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:Given ]
[name:response, type:Object, file:null, step:Then ]
[name:select, type:Object, file:null, step:When ]
[name:sign_in, type:UserHelper, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/support/user_helper.rb, step:Given ]
[name:to, type:Object, file:null, step:Then ]
[name:to, type:Object, file:null, step:Given ]
[name:to_not, type:Object, file:null, step:Given ]
[name:update_column, type:Object, file:null, step:Given ]
[name:user, type:Object, file:null, step:Given ]

Referenced pages: 2
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/case_workers/claims/index.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/documents/new.html.haml

