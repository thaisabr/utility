Classes: 9
[name:Advocate, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/advocate.rb, step:Given ]
[name:CaseWorker, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/case_worker.rb, step:Given ]
[name:Chamber, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/chamber.rb, step:Given ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:Then ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:Given ]
[name:Claims, file:ministryofjustice_Claim-for-Crown-Court-Defence/spec/factories/claims.rb, step:Given ]
[name:Message, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/message.rb, step:Given ]
[name:MessagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/messages_controller.rb, step:Given ]
[name:User, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/user.rb, step:Given ]

Methods: 65
[name:advocates, type:Object, file:null, step:Given ]
[name:all, type:Object, file:null, step:Then ]
[name:amount_assessed, type:Object, file:null, step:Then ]
[name:authorised, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:Given ]
[name:authorised, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/api/advocates/claims_controller.rb, step:Given ]
[name:be, type:Object, file:null, step:Then ]
[name:case_workers, type:Object, file:null, step:Given ]
[name:choose, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:click_on, type:Object, file:null, step:Given ]
[name:cms_number, type:Object, file:null, step:Then ]
[name:create, type:Object, file:null, step:Given ]
[name:create_list, type:Object, file:null, step:Given ]
[name:description, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:Given ]
[name:download, type:DocumentsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/documents_controller.rb, step:Given ]
[name:each, type:Object, file:null, step:Then ]
[name:edit, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:Given ]
[name:edit, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/api/advocates/claims_controller.rb, step:Given ]
[name:edit, type:Advocates/admin/advocatesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/admin/advocates_controller.rb, step:Given ]
[name:empty?, type:Object, file:null, step:When ]
[name:eq, type:Object, file:null, step:Then ]
[name:eql, type:Object, file:null, step:Then ]
[name:fees, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find, type:Object, file:null, step:When ]
[name:find, type:Object, file:null, step:Then ]
[name:find_by, type:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim.rb, step:Then ]
[name:find_field, type:Object, file:null, step:Then ]
[name:first, type:Advocate, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/advocate.rb, step:Given ]
[name:first, type:Chamber, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/chamber.rb, step:Given ]
[name:first, type:CaseWorker, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/case_worker.rb, step:Given ]
[name:first, type:Object, file:null, step:When ]
[name:has_checked_field?, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:Given ]
[name:include?, type:Object, file:null, step:Then ]
[name:index, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:Given ]
[name:index, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/api/advocates/claims_controller.rb, step:Given ]
[name:index, type:Advocates/admin/advocatesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/admin/advocates_controller.rb, step:Given ]
[name:index, type:CaseWorkers/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/claims_controller.rb, step:Given ]
[name:new, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:Given ]
[name:new, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/api/advocates/claims_controller.rb, step:Given ]
[name:new, type:Advocates/admin/advocatesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/admin/advocates_controller.rb, step:Given ]
[name:outstanding, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:Given ]
[name:outstanding, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/api/advocates/claims_controller.rb, step:Given ]
[name:page, type:Object, file:null, step:Then ]
[name:round, type:Object, file:null, step:Then ]
[name:show, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/claims_controller.rb, step:Given ]
[name:show, type:Advocates/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/api/advocates/claims_controller.rb, step:Given ]
[name:show, type:Advocates/admin/advocatesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/advocates/admin/advocates_controller.rb, step:Given ]
[name:show, type:DocumentsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/documents_controller.rb, step:Given ]
[name:sign_in, type:UserHelper, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/support/user_helper.rb, step:Given ]
[name:split, type:Object, file:null, step:Then ]
[name:state, type:Object, file:null, step:Given ]
[name:text, type:Object, file:null, step:Then ]
[name:to, type:Object, file:null, step:Then ]
[name:to_i, type:Object, file:null, step:Given ]
[name:to_s, type:OffenceClass, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/offence_class.rb, step:Then ]
[name:total, type:Object, file:null, step:Given ]
[name:update_column, type:Object, file:null, step:Given ]
[name:user, type:Object, file:null, step:Given ]
[name:value, type:Object, file:null, step:Then ]
[name:within, type:Object, file:null, step:Then ]

Referenced pages: 33
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/admin/advocates/_form.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/admin/advocates/edit.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/admin/advocates/index.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/admin/advocates/new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/admin/advocates/show.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_case_notes.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_claim.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_claim_status.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_claims.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_defendant_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_document_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_dynamic_offence_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_evidence_list.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_expense_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_fee_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_financial_summary.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_form.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_nav_tabs.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/_representation_order.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/authorised.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/edit.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/index.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/outstanding.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/advocates/claims/show.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/case_workers/claims/index.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/sessions/new.html.erb
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/shared/_links.html.erb
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/documents/_form.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/documents/new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/documents/show.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_messages.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_summary.html.haml

