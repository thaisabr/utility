Given(/^I am a signed in case worker$/) do
  @case_worker = create(:case_worker)
  visit new_user_session_path
  sign_in(@case_worker.user, 'password')
end
Given(/^I am a signed in case worker admin$/) do
  @case_worker = create(:case_worker, :admin)
  visit new_user_session_path
  sign_in(@case_worker.user, 'password')
end
Given(/^claims have been assigned to me$/) do
  case_worker = CaseWorker.first
  @claims = create_list(:allocated_claim, 5)
  @other_claims = create_list(:allocated_claim, 3)
  @claims.each_with_index { |claim, index| claim.update_column(:total, index + 1) }
  @claims.each { |claim| claim.case_workers << case_worker }
  create(:defendant, maat_reference: 'AA1245', claim_id: @claims.first.id)
  create(:defendant, maat_reference: 'BB1245', claim_id: @claims.second.id)
end
Given(/^there are allocated claims$/) do
  @claims = create_list(:allocated_claim, 5)
end
Given(/^there are unallocated claims$/) do
  @claims = create_list(:submitted_claim, 5)
end
Then(/^I should see the allocated claims$/) do
  click_on "Allocated claims (#{@claims.count})"
  expect(page).to have_content("Allocated claims (#{@claims.count})")
end
Then(/^I should see the unallocated claims$/) do
  click_on "Unallocated claims (#{@claims.count})"
  expect(page).to have_content("Unallocated claims (#{@claims.count})")
end
When(/^I visit my dashboard$/) do
  visit case_workers_claims_path
end
Then(/^I should see the claims sorted by oldest first$/) do
  claim_dom_ids = @claims.sort_by(&:submitted_at).map { |c| "claim_#{c.id}" }
  expect(page.body).to match(/.*#{claim_dom_ids.join('.*')}.*/m)
end
  def sign_in(user, password)
    visit new_user_session_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: password
    click_on 'Log in'
  end
When(/^I view status details for a claim$/) do
	first('div.claim-controls').click_link("Detail")
end
When(/^I select status "(.*?)" from select$/) do |status|
	select "#{status}", :from => "claim_state_for_form"
end
When(/^I enter amount assessed value of "(.*?)"$/) do |amount|
	find('div#amountAssessed').fill_in "Amount assessed", with: amount unless amount.empty?
end
When(/^I enter remark "(.*?)"$/) do |remark|
	fill_in	"Remarks", with: remark
end
When(/^I press update button$/) do
  click_button "Update"
end
Given(/^I have (\d+) allocated claims whos status is "(.*?)" with amount assessed of "(.*?)" and remark of "(.*?)"$/) do |number, status, amount, remark|
  claims = create_list(:allocated_claim, number.to_i, advocate: @advocate)
  claims.each do |claim|
		claim.amount_assessed = amount unless amount.empty?
		claim.additional_information = remark
		case status
			when "Part paid"
				claim.pay_part!
			when "Rejected"
				claim.reject!
			when "Rejected"
				claim.reject!
			when "Awaiting info from court"
				claim.await_info_from_court!
			else
				raise "ERROR: Invalid status specified for advocate view scenario"
		end
  end
end
When(/^I view status details of my first claim$/) do
	@claim = Claim.where(advocate: @advocate).first
  visit advocates_claim_path(@claim)
end
Then(/^I should see "(.*?)" amount assessed value of "(.*?)"$/) do |disabled, amount|
	amount = "0.00" if amount.empty?
	disabled = disabled == "disabled" ? true : false
	expect(find_field('Amount assessed', disabled: disabled).value).to eql(amount.to_s)
end
Then(/^I should see "(.*?)" remark "(.*?)"$/) do |disabled,remark|
	disabled = disabled == "disabled" ? true : false
  expect(find_field('Remarks', disabled: disabled).value).to eq(remark)
end
Then(/^I should see "(.*?)" status select with "(.*?)" selected$/) do |disabled, status|
	disabled = disabled == "disabled" ? true : false
	expect(find_field('claim_state_for_form', disabled: disabled).find('option[selected]').text).to eql(status)
end
Then(/^I should see an image tag with source "(.*?)" against that claim$/) do |image_source|
	expect(find('.status-indicator')['src'].include?(image_source)).to eql(true)
end
Given(/^I am a signed in advocate$/) do
  step %Q{an "advocate" user account exists}
  @advocate = @advocates.first
  @user     = @advocate.user
  step "I visit the user sign in page"
  step "I enter my email, password and click log in"
end
When(/^I visit the advocates dashboard$/) do
  visit advocates_claims_path
end
Given(/an? "(.*?)" user account exists$/) do |role|
  make_accounts(role)
end
When(/^I visit the user sign in page$/) do
  visit new_user_session_path
end
When(/^I enter my email, password and click log in$/) do
  fill_in 'Email', with: (@user || User.first).email
  fill_in 'Password', with: @password || 'password'
  click_on 'Log in'
  expect(page).to have_content('Sign out')
end
def make_accounts(role, number = 1)
  @password = 'password'
  case role
    when 'advocate'
      @advocates = create_list(:advocate, number)
    when 'advocate admin'
      @advocate_admins = create_list(:advocate, number, :admin)
    when 'case worker'
      @case_workers = create_list(:case_worker, number)
    when 'case worker admin'
      create(:case_worker, :admin)
  end
end
