Feature: Claim history
Scenario: Advocate claim history should reflect a state change
Given I am a signed in advocate
And certification types are seeded
And I have a claim in draft state
And I submit the claim
Then I should be redirected to the claim certification page
And I fill in the certification details and submit
When I visit the claim's detail page
Then I should see the state change to submitted reflected in the history
