Feature: Advocate new claim
Background:
Given certification types are seeded
@javascript @webmock_allow_localhost_connect
Scenario Outline: Daily attendance fees derived from actual length of Trial
Given I am a signed in advocate
And There are case types in place
And I am on the new claim page with Daily Attendance Fees in place
And I select2 a Case Type of "Trial"
And I fill in actual trial length with <actual_trial_length>
Then The daily attendance fields should have quantities <daf_quantity>, <dah_quantity>, <daj_quantity>
Examples:
| actual_trial_length   | daf_quantity | dah_quantity | daj_quantity |
| 1                     | 0            | 0            | 0            |
| 2                     | 0            | 0            | 0            |
| 3                     | 1            | 0            | 0            |
| 40                    | 38           | 0            | 0            |
| 41                    | 38           | 1            | 0            |
| 50                    | 38           | 10           | 0            |
| 51                    | 38           | 10           | 1            |
| 60                    | 38           | 10           | 10           |
| 70                    | 38           | 10           | 20           |
@javascript @webmock_allow_localhost_connect
Scenario Outline: Daily attendance fees derived from actual length of Retrial
Given I am a signed in advocate
And There are case types in place
And I am on the new claim page with Daily Attendance Fees in place
And I select2 a Case Type of "Retrial"
And I fill in actual retrial length with <actual_retrial_length>
Then The daily attendance fields should have quantities <daf_quantity>, <dah_quantity>, <daj_quantity>
Examples:
| actual_retrial_length | daf_quantity | dah_quantity | daj_quantity |
| 1                     | 0            | 0            | 0            |
| 2                     | 0            | 0            | 0            |
| 3                     | 1            | 0            | 0            |
| 40                    | 38           | 0            | 0            |
| 41                    | 38           | 1            | 0            |
| 50                    | 38           | 10           | 0            |
| 51                    | 38           | 10           | 1            |
| 60                    | 38           | 10           | 10           |
| 70                    | 38           | 10           | 20           |
Feature: Claim details
Scenario: Case worker views trial details
Given I am a signed in case worker
And a trial claim has been assigned to me
And I visit the case worker claim's detail page
Then I should see trial details
Scenario: Case worker views retrial details
Given I am a signed in case worker
And a retrial claim has been assigned to me
And I visit the case worker claim's detail page
Then I should see retrial details
Feature: Trial detail visibility by case type
Background:
As an advocate I want to see trial detail fields for
trial case types only
Given case types are seeded
@javascript @webmock_allow_localhost_connect
Scenario Outline: Case types for which retrial details should or should not be visible on a new claim
Given I am a signed in advocate
And I am on the new claim page
Then I should not see the retrial detail fields
When I select2 "<case_type>" from "claim_case_type_id"
Then I <condition> see the retrial detail fields
Examples:
| case_type                   | condition  |
| Appeal against conviction   | should not |
| Appeal against sentence     | should not |
| Breach of Crown Court order | should not |
| Committal for Sentence      | should not |
| Contempt                    | should not |
| Cracked Trial               | should not |
| Cracked before retrial      | should not |
| Elected cases not proceeded | should not |
| Guilty plea                 | should not |
| Discontinuance              | should not |
| Retrial                     | should     |
| Trial                       | should not |
@javascript @webmock_allow_localhost_connect
Scenario Outline: Case types for which retrial details should or should not be visible on an existing claim
Given I am a signed in advocate
And I am on the edit page for a draft claim of case type "<case_type>"
Then I <condition> see the retrial detail fields
Examples:
| case_type                   | condition  |
| Appeal against conviction   | should not |
| Appeal against sentence     | should not |
| Breach of Crown Court order | should not |
| Committal for Sentence      | should not |
| Contempt                    | should not |
| Cracked Trial               | should not |
| Cracked before retrial      | should not |
| Elected cases not proceeded | should not |
| Guilty plea                 | should not |
| Discontinuance              | should not |
| Retrial                     | should     |
| Trial                       | should not |
