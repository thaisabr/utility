Classes: 4
[name:CaseWorker, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/case_worker.rb, step:Then ]
[name:CaseWorker, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/case_worker.rb, step:Given ]
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/external_users/claim.rb, step:When ]
[name:User, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/user.rb, step:Given ]

Methods: 40
[name:all, type:Object, file:null, step:When ]
[name:all, type:Object, file:null, step:Then ]
[name:api_landing, type:PagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/pages_controller.rb, step:Given ]
[name:api_release_notes, type:PagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/pages_controller.rb, step:Given ]
[name:be_allocated, type:Object, file:null, step:Then ]
[name:be_checked, type:Object, file:null, step:Then ]
[name:claims, type:WidgetsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/geckoboard_api/widgets_controller.rb, step:Then ]
[name:claims, type:Allocation, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/allocation.rb, step:Then ]
[name:click, type:Object, file:null, step:When ]
[name:click_on, type:Object, file:null, step:Given ]
[name:count, type:Object, file:null, step:Then ]
[name:create, type:Object, file:null, step:Given ]
[name:create, type:JsonDocumentImporter, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/json_document_importer.rb, step:Given ]
[name:each, type:Object, file:null, step:Given ]
[name:each, type:Object, file:null, step:When ]
[name:each, type:Object, file:null, step:Then ]
[name:eq, type:Object, file:null, step:Then ]
[name:fill_in, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:find, type:DocType, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/doc_type.rb, step:When ]
[name:find, type:DocType, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/doc_type.rb, step:Then ]
[name:find_by, type:BaseClaim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim/base_claim.rb, step:When ]
[name:first, type:Object, file:null, step:Given ]
[name:last, type:Object, file:null, step:Given ]
[name:last, type:CaseWorker, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/case_worker.rb, step:Then ]
[name:map, type:Object, file:null, step:Given ]
[name:map, type:Object, file:null, step:When ]
[name:map, type:Object, file:null, step:Then ]
[name:match_array, type:Object, file:null, step:Then ]
[name:not_to, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:When ]
[name:page, type:Object, file:null, step:Then ]
[name:sign_in, type:UserHelper, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/support/user_helper.rb, step:Given ]
[name:split, type:Object, file:null, step:Given ]
[name:take, type:Object, file:null, step:When ]
[name:tandcs, type:PagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/pages_controller.rb, step:Given ]
[name:to, type:Object, file:null, step:Then ]
[name:to_i, type:Object, file:null, step:When ]
[name:to_i, type:Object, file:null, step:Then ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 5
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/sessions/new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/shared/_links.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/pages/api_landing.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/pages/api_release_notes.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/pages/tandcs.haml

