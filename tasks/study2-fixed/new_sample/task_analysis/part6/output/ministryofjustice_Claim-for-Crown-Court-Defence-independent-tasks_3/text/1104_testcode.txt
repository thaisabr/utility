Given(/^I am a signed in advocate admin$/) do
  @advocate = create(:external_user, :admin)
  visit new_user_session_path
  sign_in(@advocate.user, 'password')
end
  def sign_in(user, password)
    visit new_user_session_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: password
    click_on 'Sign in'
  end
Given(/^my provider is a "(.*?)"$/) do |provider_type|
  @advocate.provider.update_column(:provider_type, provider_type)
  @advocate.provider.update_column(:vat_registered, false)
end
Given(/^I am on the new "(.*?)" page$/) do |persona|
  personas = persona.pluralize
  visit "/#{personas}/admin/#{personas}/new"
end
When(/^I fill in the "(.*?)" details$/) do |persona|
  fill_in "#{persona}_user_attributes_first_name", with: 'Harold'
  fill_in "#{persona}_user_attributes_last_name", with: 'Hughes'
  fill_in "#{persona}_user_attributes_email", with: 'harold.hughes@example.com'
  fill_in "#{persona}_user_attributes_email_confirmation", with: 'harold.hughes@example.com'
  case persona
  when 'external_user'
    if @advocate.provider.chamber?
      choose('external_user_vat_registered_true')
      fill_in 'external_user_supplier_number', with: '31425'
    end
    check('external_user_roles_admin')
  when 'case_worker'
    check('case_worker[days_worked_0]')
    choose('case_worker[location_id]')
    check('case_worker_roles_case_worker')
  end
end
When(/^I fill in the "(.*?)" details but email and email_confirmation do not match$/) do |persona|
  fill_in "#{persona}_user_attributes_first_name", with: 'Harold'
  fill_in "#{persona}_user_attributes_last_name", with: 'Hughes'
  fill_in "#{persona}_user_attributes_email", with: 'harold.hughes@example.com'
  fill_in "#{persona}_user_attributes_email_confirmation", with: 'another_email@example.com'
  case persona
  when 'external_user'
    choose('external_user_vat_registered_true')
    fill_in 'external_user_supplier_number', with: '31425'
    check('external_user_roles_admin')
  when 'case_worker'
    check('case_worker[days_worked_0]')
    choose('case_worker[location_id]')
    check('case_worker_roles_case_worker')
  end
end
Then(/^I should (not )?see the supplier number or VAT registration fields$/) do |negate|
  if negate.present?
    expect(page).to_not have_content(/supplier number/i)
    expect(page).to_not have_content(/vat registered/i)
  else
    expect(page).to have_content(/supplier number/i)
    expect(page).to have_content(/vat registered/i)
  end
end
When(/^I check "(.*?)"$/) do |role|
  check role
end
