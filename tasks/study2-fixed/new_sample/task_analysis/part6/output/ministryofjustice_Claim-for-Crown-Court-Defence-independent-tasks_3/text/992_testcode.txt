Given(/^case worker "(.*?)" exists$/) do |name|
  first_name = name.split.first
  last_name = name.split.last
  user = create(:user, first_name: first_name, last_name: last_name)
  create(:case_worker, :case_worker, user: user)
end
Given(/^submitted claims? exists? with case numbers? "(.*?)$/) do |case_numbers|
  case_numbers = case_numbers[0..-2].split(',').map(&:strip)
  @claims = []

  case_numbers.each do |case_number|
    @claims << create(:submitted_claim, case_number: case_number)
  end
end
When(/^I visit the allocation page$/) do
  visit case_workers_admin_allocations_path
end
When(/^I enter (\d+) in the quantity text field$/) do |quantity|
  within('.report') do
    @case_numbers = all('.js-test-case-number').map(&:text)
  end
  @claims_on_page = []
  @case_numbers.each do |case_number|
    @claims_on_page << Claim::BaseClaim.find_by(case_number: case_number)
  end
  @claims_to_allocate = @claims_on_page.take(quantity.to_i)
  fill_in 'quantity_to_allocate', with: quantity
end
Then(/^the first (\d+) claims in the list should be allocated to the case worker$/) do |quantity|
  expect(CaseWorker.last.claims.count).to eq(quantity.to_i)
  CaseWorker.last.claims.each do |claim|
    expect(claim).to be_allocated
  end

  expect(CaseWorker.last.claims.map(&:id)).to match_array @claims_to_allocate.map(&:id)

end
When(/^I click on a claim row cell$/) do
  within('.report') do
    #click first row's 2nd column
    page.find('tbody').all('tr')[0].all('td')[1].click()
  end
end
When(/^I click on a claims row cell$/) do
  #click the first row's first 2nd cell
  page.find('tbody').all('tr')[0].all('td')[1].click()
end
Then (/^I should see that claims checkbox (ticked|unticked)$/) do | checkbox_state|
  if checkbox_state == 'ticked'
    expect(page.find('tbody').all('tr')[0].all('input[type=checkbox]')[0]).to be_checked
  else
    expect(page.find('tbody').all('tr')[0].all('input[type=checkbox]')[0]).not_to be_checked
  end
end
Given(/^I am a signed in case worker admin$/) do
  @case_worker = create(:case_worker, :admin)
  visit new_user_session_path
  sign_in(@case_worker.user, 'password')
end
  def sign_in(user, password)
    visit new_user_session_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: password
    click_on 'Sign in'
  end
