Classes: 6
[name:CaseWorker, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/case_worker.rb, step:Given ]
[name:CaseWorkers, file:ministryofjustice_Claim-for-Crown-Court-Defence/db/seeds/case_workers.rb, step:Given ]
[name:CaseWorkers, file:ministryofjustice_Claim-for-Crown-Court-Defence/lib/demo_data/demo_seeds/case_workers.rb, step:Given ]
[name:CaseWorkers, file:ministryofjustice_Claim-for-Crown-Court-Defence/spec/factories/case_workers.rb, step:Given ]
[name:Claims, file:ministryofjustice_Claim-for-Crown-Court-Defence/spec/factories/claims.rb, step:Given ]
[name:User, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/user.rb, step:Given ]

Methods: 19
[name:all, type:Object, file:null, step:When ]
[name:all, type:Object, file:null, step:Then ]
[name:api_landing, type:PagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/pages_controller.rb, step:Given ]
[name:be_checked, type:Object, file:null, step:Then ]
[name:click, type:Object, file:null, step:When ]
[name:click_on, type:Object, file:null, step:Given ]
[name:create, type:JsonDocumentImporter, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/json_document_importer.rb, step:Given ]
[name:create_list, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:find, type:DocType, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/doc_type.rb, step:When ]
[name:find, type:DocType, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/doc_type.rb, step:Then ]
[name:not_to, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:When ]
[name:page, type:Object, file:null, step:Then ]
[name:sign_in, type:UserHelper, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/support/user_helper.rb, step:Given ]
[name:tandcs, type:PagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/pages_controller.rb, step:Given ]
[name:to, type:Object, file:null, step:Then ]
[name:to_i, type:Object, file:null, step:Given ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 4
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/sessions/new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/shared/_links.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/pages/api_landing.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/pages/tandcs.haml

