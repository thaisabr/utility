Classes: 2
[name:Claim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/interfaces/api/v1/external_users/claim.rb, step:Then ]
[name:User, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/user.rb, step:Given ]

Methods: 43
[name:all, type:BaseClaim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim/base_claim.rb, step:Then ]
[name:api_landing, type:PagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/pages_controller.rb, step:Given ]
[name:api_release_notes, type:PagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/pages_controller.rb, step:Given ]
[name:authorised, type:ExternalUsers/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/external_users/claims_controller.rb, step:null]
[name:button button-secondary left, type:Object, file:null, step:null]
[name:button left, type:Object, file:null, step:null]
[name:click_on, type:Object, file:null, step:Given ]
[name:concat, type:Object, file:null, step:Given ]
[name:count, type:Object, file:null, step:Then ]
[name:create, type:JsonDocumentImporter, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/json_document_importer.rb, step:Given ]
[name:create, type:Object, file:null, step:Given ]
[name:create_list, type:Object, file:null, step:Given ]
[name:download, type:DocumentsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/documents_controller.rb, step:null]
[name:each, type:Object, file:null, step:Then ]
[name:edit, type:ExternalUsers/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/external_users/claims_controller.rb, step:null]
[name:eq, type:Object, file:null, step:Then ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:have_selector, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:Then ]
[name:index, type:ExternalUsers/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/external_users/claims_controller.rb, step:null]
[name:new, type:ExternalUsers/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/external_users/claims_controller.rb, step:null]
[name:outstanding, type:ExternalUsers/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/external_users/claims_controller.rb, step:null]
[name:page, type:Object, file:null, step:Then ]
[name:save!, type:Object, file:null, step:Given ]
[name:send, type:BaseClaim, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/claim/base_claim.rb, step:Then ]
[name:show, type:ExternalUsers/claimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/external_users/claims_controller.rb, step:null]
[name:show, type:DocumentsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/documents_controller.rb, step:null]
[name:sign_in, type:UserHelper, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/support/user_helper.rb, step:Given ]
[name:tandcs, type:PagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/pages_controller.rb, step:Given ]
[name:to, type:Object, file:null, step:Then ]
[name:to_i, type:Object, file:null, step:Then ]
[name:to_sym, type:Object, file:null, step:Then ]
[name:update, type:CaseWorkersController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/admin/case_workers_controller.rb, step:null]
[name:update, type:ClaimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/case_workers/claims_controller.rb, step:null]
[name:update, type:ExternalUsersController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/external_users/admin/external_users_controller.rb, step:null]
[name:update, type:ProvidersController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/external_users/admin/providers_controller.rb, step:null]
[name:update, type:CertificationsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/external_users/certifications_controller.rb, step:null]
[name:update, type:ClaimsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/external_users/claims_controller.rb, step:null]
[name:update, type:SuperAdminsController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/super_admins/admin/super_admins_controller.rb, step:null]
[name:update, type:ExternalUsersController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/super_admins/external_users_controller.rb, step:null]
[name:update, type:ProvidersController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/super_admins/providers_controller.rb, step:null]
[name:update, type:UserMessageStatusesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/user_message_statuses_controller.rb, step:null]
[name:user, type:Object, file:null, step:Given ]

Referenced pages: 58
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/sessions/new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/shared/_links.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/_claims.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/_error_summary.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/_financial_summary.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/_form.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/_json_document_importer.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/_search_form.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/additional_information/_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/authorised.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/basic_fees/_basic_fee_calculated_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/basic_fees/_basic_fee_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/basic_fees/_basic_fee_uncalculated_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/basic_fees/_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/buttons/_new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/case_details/_cracked_trial_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/case_details/_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/case_details/_offence_select.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/case_details/_retrial_detail_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/case_details/_trial_detail_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/defendants/_defendant_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/defendants/_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/defendants/_representation_order_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/edit.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/expenses/_expense_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/expenses/_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/fixed_fees/_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/fixed_fees/_fixed_fee_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/index.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/misc_fees/_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/misc_fees/_misc_fee_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/outstanding.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/show.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/supporting_documents/_existing.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/supporting_documents/_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/supporting_documents/_new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/supporting_evidence_checklist/_evidence_checklist.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/external_users/claims/supporting_evidence_checklist/_fields.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/pages/api_landing.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/pages/api_release_notes.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/pages/tandcs.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim_cracked_trial_details.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim_defendant_details.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim_defendants.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim_header.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim_history.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim_retrial_details.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim_status.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_claim_trial_details.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_determination_amounts.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_determinations_form.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_determinations_table.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_evidence_checklist.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_evidence_list.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_search_summary.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/shared/_summary.html.haml

