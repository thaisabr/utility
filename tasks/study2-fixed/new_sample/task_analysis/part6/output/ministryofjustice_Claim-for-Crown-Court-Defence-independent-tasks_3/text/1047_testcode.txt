Given(/^I am a signed in advocate$/) do
  @advocate = create(:external_user)
  visit new_user_session_path
  sign_in(@advocate.user, 'password')
end
Given(/^I am a signed in advocate admin$/) do
  @advocate = create(:external_user, :admin)
  visit new_user_session_path
  sign_in(@advocate.user, 'password')
end
Given(/^I am a signed in case worker$/) do
  @case_worker = create(:case_worker)
  visit new_user_session_path
  sign_in(@case_worker.user, 'password')
end
  def sign_in(user, password)
    visit new_user_session_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: password
    click_on 'Sign in'
  end
Given(/^certification types are seeded$/) do
  CertificationType.find_or_create_by!(name: 'I attended the Main Hearing (1st day of trial)', pre_may_2015: false)
  CertificationType.find_or_create_by!(name: 'I notified the court, in writing before the PCMH that I was the Instructed Advocate. A copy of the letter is attached.', pre_may_2015: true)
  CertificationType.find_or_create_by!(name: 'I attended the PCMH (where the client was arraigned) and no other advocate wrote to the court prior to this to advice that they were the Instructed Advocate.', pre_may_2015: true)
  CertificationType.find_or_create_by!(name: 'I attended the first hearing after the PCMH and no other advocate attended the PCMH or wrote to the court prior to this to advise that they were the Instructed Advocate.', pre_may_2015: true)
  CertificationType.find_or_create_by!(name: 'The previous Instructed Advocate notified the court in writing that they were no longer acting in this case and I was then instructed.', pre_may_2015: true)
  CertificationType.find_or_create_by!(name: 'The case was a fixed fee (with a case number beginning with an S or A) and I attended the main hearing.', pre_may_2015: true)
end
Given(/^I am on the new claim page$/) do
  create(:court, name: 'some court')
  create(:offence_class, description: 'A: Homicide and related grave offences')
  create(:offence, description: 'Murder')
  create(:fee_type, :basic, description: 'Basic Fee', code: 'BAF')
  create(:fee_type, :basic, description: 'Other Basic Fee')
  create(:fee_type, :basic, description: 'Basic Fee with dates attended required', code: 'SAF')
  create(:fee_type, :fixed, description: 'Fixed Fee example')
  create(:fee_type, :misc,  description: 'Miscellaneous Fee example')
  create(:expense_type, name: 'Travel')
  visit new_external_users_claim_path
end
Given(/^There are case types in place$/) do
  load "#{Rails.root}/db/seeds/case_types.rb"
  CaseType.find_or_create_by!(name: 'Fixed fee', is_fixed_fee: true)
end
Then(/^I should not see any dates attended fields for "(.*?)" fees$/) do |fee_type|
  within fee_type_to_id(fee_type) do
    wait_for_ajax
    expect(page).to_not have_content('Date attended (from)')
  end
end
When(/^I fill in the certification details and submit/) do
  choose 'I attended the Main Hearing (1st day of trial)'
  click_on 'Certify and submit claim'
end
When(/^I fill in the claim details(.*)$/) do |details|
  select('Guilty plea', from: 'claim_case_type_id')
  select('some court', from: 'claim_court_id')
  fill_in 'claim_case_number', with: 'A12345678'
  fill_in 'claim_first_day_of_trial_dd', with: 5.days.ago.day.to_s
  fill_in 'claim_first_day_of_trial_mm', with: 5.days.ago.month.to_s
  fill_in 'claim_first_day_of_trial_yyyy', with: 5.days.ago.year.to_s
  fill_in 'claim_trial_concluded_at_dd', with: 2.days.ago.day.to_s
  fill_in 'claim_trial_concluded_at_mm', with: 2.days.ago.month.to_s
  fill_in 'claim_trial_concluded_at_yyyy', with: 2.days.ago.year.to_s
  fill_in 'claim_estimated_trial_length', with: 1
  fill_in 'claim_actual_trial_length', with: 1

  murder_offence_id = Offence.find_by(description: 'Murder').id.to_s
  first('#claim_offence_id', visible: false).set(murder_offence_id)
  choose 'QC'

  within '#defendants' do
    fill_in 'claim_defendants_attributes_0_first_name', with: 'Foo'
    fill_in 'claim_defendants_attributes_0_last_name', with: 'Bar'

    fill_in 'claim_defendants_attributes_0_date_of_birth_dd', with: '04'
    fill_in 'claim_defendants_attributes_0_date_of_birth_mm', with: '10'
    fill_in 'claim_defendants_attributes_0_date_of_birth_yyyy', with: '1980'

    fill_in 'claim_defendants_attributes_0_representation_orders_attributes_0_maat_reference', with: '4561239693'

    date = rand(10..20).days.ago
    fill_in 'claim_defendants_attributes_0_representation_orders_attributes_0_representation_order_date_dd', with: date.strftime('%d')
    fill_in 'claim_defendants_attributes_0_representation_orders_attributes_0_representation_order_date_mm', with: date.strftime('%m')
    fill_in 'claim_defendants_attributes_0_representation_orders_attributes_0_representation_order_date_yyyy', with: date.strftime('%Y')

  end

  unless details == ' but add no fees or expenses' # preceeding space is required for match
    within '#basic-fees' do
      fill_in 'claim_basic_fees_attributes_0_quantity', with: 1
      fill_in 'claim_basic_fees_attributes_0_rate', with: 50
      fill_in 'claim_basic_fees_attributes_1_quantity', with: 1
      fill_in 'claim_basic_fees_attributes_1_rate', with: 50
    end

    within '#expenses' do
      select 'Travel', from: 'claim_expenses_attributes_0_expense_type_id'
      fill_in 'claim_expenses_attributes_0_location', with: 'London'
      fill_in 'claim_expenses_attributes_0_quantity', with: 1.1
      fill_in 'claim_expenses_attributes_0_rate', with: 40
    end
  end

  within 'fieldset#evidence-checklist' do
    element = find('div label', text: "Representation order")
    checkbox_id = element[:for]
    check checkbox_id
  end
end
When(/^I save to drafts$/) do
  click_on 'Save to drafts'
end
Then(/^I should be redirected to the claim certification page$/) do
  claim = Claim.first
  expect(page.current_path).to eq(new_external_users_claim_certification_path(claim))
end
def fee_type_to_id(fee_type)
  div_id = fee_type.downcase == "fixed" ? 'fixed-fees' : 'misc-fees'
  "##{div_id}"
end
  def wait_for_ajax
    Timeout.timeout(Capybara.default_max_wait_time) do
      active = page.evaluate_script('jQuery.active')
      until active == 0
        active = page.evaluate_script('jQuery.active')
      end
    end
  end
Given(/^I have a claim in draft state$/) do
  @claim = create(:claim, external_user: @advocate)
end
Given(/^I submit the claim$/) do
  visit edit_external_users_claim_path(@claim)
  click_on 'Submit to LAA'
end
Then(/^I should see the state change to submitted reflected in the history$/) do
  @claim.reload

  within '#panel1' do
    history = all('.event').last
    expect(history).to have_content(/Your claim has been submitted/)
  end
end
When(/^I visit the claim's case worker detail page$/) do
  visit case_workers_claim_path(@claim)
end
Given(/^I have been allocated a claim$/) do
  @claim = create(:allocated_claim)
  @case_worker.claims << @claim
end
When(/^I mark the claim authorised$/) do
  choose 'Authorised'
  fill_in 'claim_assessment_attributes_fees', with: '100.00'
  click_on 'Update'
end
Then(/^I should see the state change to authorised reflected in the history$/) do
  @claim.reload
  within '#panel1' do
    history = all('.event').last
    expect(history).to have_content(/Claim authorised/)
  end
end
When(/^I visit the claim's detail page$/) do
  visit external_users_claim_path(@claim)
end
Given(/^a claim with messages exists that I have been assigned to$/) do
  @case_worker = CaseWorker.first
  @claim = create(:allocated_claim)
  @messages = create_list(:message, 5, claim_id: @claim.id)
  @messages.each { |m| m.update_column(:sender_id, create(:external_user, :advocate).user.id) }
  @claim.case_workers << @case_worker
end
Then(/^I should see the messages for that claim in chronological order$/) do
  message_dom_ids = @messages.sort_by(&:created_at).map { |m| "message_#{m.id}" }
  expect(page.body).to match(/.*#{message_dom_ids.join('.*')}.*/m)
end
When(/^I leave a message$/) do
  within '#panel1' do
    fill_in 'message_body', with: 'Lorem'
    click_on 'Send'
  end
end
Then(/^I should see my message at the bottom of the message list$/) do
  within '#panel1' do
    message_body = all('.message-body').last
    expect(message_body).to have_content(/Lorem/)
  end
end
Given(/^I have a submitted claim with messages$/) do
  @claim = create(:submitted_claim, external_user_id: ExternalUser.first.id)
  @messages = create_list(:message, 5, claim_id: @claim.id)
  @messages.each { |m| m.update_column(:sender_id, create(:external_user, :advocate).user.id) }
end
When(/^I edit the claim and save to draft$/) do
  claim = Claim.last
  visit "/external_users/claims/#{claim.id}/edit"
  click_on 'Save to drafts'
end
Then(/^I should not see any dates in the message history field$/) do
  expect(page.all('div.event-date').count).to eq 0
end
Then(/^I should see 'no messages found' in the claim history$/) do
  expect(page).to have_content('No messages found')
end
Then(/^I (.*?) see the redetermination button$/) do | radio_button_expectation |
  case radio_button_expectation
    when 'should not'
      within('.messages-container') do
        expect(page).to_not have_content('Apply for redetermination')
      end
    when 'should'
      within('.messages-container') do
        expect(page).to have_content('Apply for redetermination')
      end
  end
end
Then(/^I (.*?) see the request written reason button$/) do | radio_button_expectation |
  case radio_button_expectation
    when 'should not'
      within('.messages-container') do
        expect(page).to_not have_content('Request written reasons')
      end
    when 'should'
      within('.messages-container') do
        expect(page).to have_content('Request written reasons')
      end
  end
end
Then(/^I (.*?) see the controls to send messages$/) do | msg_control_expectation |

  case msg_control_expectation
    when 'should not'
      within('.messages-container') do
        expect(page).to_not have_css('.js-test-send-buttons')
      end
    when 'should'
      within('.messages-container') do
        expect(page).to have_css('.js-test-send-buttons')
      end
  end
end
When(/^click on (.*?) option$/) do | radio_button|
  within('.messages-container') do
    choose radio_button
  end
end
Then(/^I can send a message$/) do
  within '#panel1' do
    fill_in 'message_body', with: 'Lorem'
    click_on 'Send'
  end
  wait_for_ajax
end
When(/^I expand the accordion$/) do
  within('#claim-accordion') do
    page.find('h2', text: 'Messages').click
  end
end
When(/^I visit that claim's "(.*?)" detail page$/) do |namespace|
  case namespace
    when 'advocate'
      visit external_users_claim_path(@claim)
    when 'case worker'
      visit case_workers_claim_path(@claim)
  end
end
When(/^I view the claim$/) do
  visit external_users_root_path
  within('.report') do
    first('a.js-test-case-number-link').click
  end
end
Given(/^I have (\d+) (.*?) claim$/) do |number,state|
  if number.to_i == 1
    @claim = create("#{state}_claim".to_sym,  external_user: @advocate)
  else
    @claims = create_list("#{state}_claim".to_sym, number.to_i, external_user: @advocate)
  end
end
Then(/^the claim should be in the "(.*?)" state$/) do |state|
  @claim.reload
  expect(@claim.state).to eq(state)
end
Then(/^the claim should no longer have case workers assigned$/) do
  @claim.reload
  expect(@claim.case_workers).to be_empty
end
