Classes: 3
[name:ActionMailer, file:null, step:When ]
[name:ActionMailer, file:null, step:Then ]
[name:User, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/user.rb, step:Given ]

Methods: 27
[name:api_landing, type:PagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/pages_controller.rb, step:Given ]
[name:api_release_notes, type:PagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/pages_controller.rb, step:Given ]
[name:body, type:MessagePresenter, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/presenters/message_presenter.rb, step:Then ]
[name:chamber?, type:Object, file:null, step:When ]
[name:check, type:Object, file:null, step:When ]
[name:choose, type:Object, file:null, step:When ]
[name:click_on, type:Object, file:null, step:Given ]
[name:click_on, type:Object, file:null, step:When ]
[name:create, type:JsonDocumentImporter, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/json_document_importer.rb, step:Given ]
[name:eq, type:Object, file:null, step:Then ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:first, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:Then ]
[name:include, type:Object, file:null, step:Then ]
[name:length, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:Then ]
[name:pluralize, type:Object, file:null, step:Given ]
[name:provider, type:Object, file:null, step:Given ]
[name:provider, type:Object, file:null, step:When ]
[name:sign_in, type:UserHelper, file:ministryofjustice_Claim-for-Crown-Court-Defence/features/support/user_helper.rb, step:Given ]
[name:subject, type:BugReport, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/bug_report.rb, step:Then ]
[name:subject, type:Feedback, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/models/feedback.rb, step:Then ]
[name:tandcs, type:PagesController, file:ministryofjustice_Claim-for-Crown-Court-Defence/app/controllers/pages_controller.rb, step:Given ]
[name:to, type:Object, file:null, step:Then ]
[name:update_column, type:Object, file:null, step:Given ]
[name:user, type:Object, file:null, step:Given ]

Referenced pages: 5
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/sessions/new.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/devise/shared/_links.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/pages/api_landing.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/pages/api_release_notes.html.haml
ministryofjustice_Claim-for-Crown-Court-Defence/app/views/pages/tandcs.haml

