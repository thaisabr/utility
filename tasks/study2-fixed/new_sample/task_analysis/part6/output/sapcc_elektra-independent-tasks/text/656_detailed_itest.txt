Classes: 1
[name:URI, file:null, step:Then ]

Methods: 19
[name:click_on, type:Object, file:null, step:Given ]
[name:current_path, type:AuthenticationSteps, file:sapcc_elektra/features/step_definitions/integration/authentication_steps.rb, step:Then ]
[name:current_url, type:Object, file:null, step:Then ]
[name:driver, type:Object, file:null, step:Given ]
[name:eq, type:Object, file:null, step:Given ]
[name:eq, type:Object, file:null, step:Then ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_selector, type:Object, file:null, step:Then ]
[name:monsoon_openstack_auth, type:Object, file:null, step:Given ]
[name:monsoon_openstack_auth, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:Given ]
[name:page, type:Object, file:null, step:Then ]
[name:path, type:Object, file:null, step:Then ]
[name:show, type:PagesController, file:sapcc_elektra/app/controllers/dashboard/pages_controller.rb, step:null]
[name:show, type:PagesController, file:sapcc_elektra/app/controllers/pages_controller.rb, step:null]
[name:status_code, type:Object, file:null, step:Given ]
[name:to, type:Object, file:null, step:Given ]
[name:to, type:Object, file:null, step:Then ]

Referenced pages: 3
sapcc_elektra/app/views/pages/_breadcrumb.html.haml
sapcc_elektra/app/views/pages/landing.html.haml
sapcc_elektra/app/views/pages/start.html.haml

