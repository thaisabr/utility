Feature: Navigating to reports page
Background:
Given there is 1 project with the following:
| name | ParentProject |
| identifier | parent_project_1 |
And the project "ParentProject" has 1 subproject with the following:
| name | SubProject |
| identifier | parent_project_1_sub_1 |
@javascript @selenium
Scenario: Archiving and unarchiving a project with a subproject
Given I am already admin
When I go to the projects admin page
Then I should be on the projects admin page
And I should see "Projects"
And I click on "Archive" within "tbody > tr:nth-of-type(1)"
And I confirm popups
Then I should be on the projects admin page
And I should not see "ParentProject"
And I should not see "SubProject"
When I go to the page of the project called "ParentProject"
Then I should see "403"
When I go to the page of the project called "SubProject"
Then I should see "403"
When I go to the projects admin page
When I select "All" from "status"
And I click on "Unarchive" within "tbody > tr:nth-of-type(1)"
Then I should be on the projects admin page
And I should see "ParentProject"
When I go to the page of the project called "ParentProject"
Then I should see "ParentProject"
