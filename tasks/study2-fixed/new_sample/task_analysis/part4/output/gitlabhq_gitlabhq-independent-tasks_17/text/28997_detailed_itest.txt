Classes: 4
[name:Event, file:gitlabhq_gitlabhq/app/models/event.rb, step:Given ]
[name:Factory, file:null, step:Given ]
[name:Project, file:gitlabhq_gitlabhq/app/models/project.rb, step:Given ]
[name:User, file:gitlabhq_gitlabhq/app/models/user.rb, step:Given ]

Methods: 13
[name:Factory, type:Object, file:null, step:Given ]
[name:create, type:Event, file:gitlabhq_gitlabhq/app/models/event.rb, step:Given ]
[name:find_by_name, type:Project, file:gitlabhq_gitlabhq/app/models/project.rb, step:Given ]
[name:find_by_name, type:User, file:gitlabhq_gitlabhq/app/models/user.rb, step:Given ]
[name:have_content, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:Given ]
[name:index, type:DashboardController, file:gitlabhq_gitlabhq/app/controllers/admin/dashboard_controller.rb, step:null]
[name:index, type:DashboardController, file:gitlabhq_gitlabhq/app/controllers/dashboard_controller.rb, step:null]
[name:login_as, type:Object, file:null, step:Given ]
[name:name, type:User, file:gitlabhq_gitlabhq/app/models/user.rb, step:Given ]
[name:name, type:Project, file:gitlabhq_gitlabhq/app/models/project.rb, step:Given ]
[name:name, type:Project, file:gitlabhq_gitlabhq/app/models/project.rb, step:null]
[name:page, type:Object, file:null, step:Then ]

Referenced pages: 3
gitlabhq_gitlabhq/app/views/dashboard/index.html.haml
gitlabhq_gitlabhq/app/views/events/_event_last_push.html.haml
gitlabhq_gitlabhq/app/views/shared/_no_ssh.html.haml

