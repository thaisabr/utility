Classes: 3
[name:Grit, file:null, step:Given ]
[name:Project, file:gitlabhq_gitlabhq/app/models/project.rb, step:Given ]
[name:User, file:gitlabhq_gitlabhq/app/models/user.rb, step:Given ]

Methods: 21
[name:Factory, type:Object, file:null, step:Given ]
[name:add_access, type:Object, file:null, step:Given ]
[name:and_return, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:Given ]
[name:each, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:find_all, type:Commit, file:gitlabhq_gitlabhq/app/models/commit.rb, step:Given ]
[name:find_by_name, type:Project, file:gitlabhq_gitlabhq/app/models/project.rb, step:Given ]
[name:graph, type:ProjectsController, file:gitlabhq_gitlabhq/app/controllers/admin/projects_controller.rb, step:null]
[name:graph, type:ProjectsController, file:gitlabhq_gitlabhq/app/controllers/projects_controller.rb, step:null]
[name:hashes, type:Object, file:null, step:Given ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:Given ]
[name:index, type:LabelsController, file:gitlabhq_gitlabhq/app/controllers/labels_controller.rb, step:null]
[name:login_as, type:Object, file:null, step:Given ]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:Given ]
[name:repo, type:Repository, file:gitlabhq_gitlabhq/app/roles/repository.rb, step:Given ]
[name:stub, type:Commit, file:gitlabhq_gitlabhq/app/models/commit.rb, step:Given ]
[name:within, type:Object, file:null, step:Then ]
[name:within, type:Object, file:null, step:Given ]

Referenced pages: 4
gitlabhq_gitlabhq/app/views/issues/_head.html.haml
gitlabhq_gitlabhq/app/views/labels/_label.html.haml
gitlabhq_gitlabhq/app/views/labels/index.html.haml
gitlabhq_gitlabhq/app/views/projects/graph.html.haml

