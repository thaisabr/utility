Classes: 3
[name:Issue, file:gitlabhq_gitlabhq/app/models/issue.rb, step:null]
[name:User, file:gitlabhq_gitlabhq/app/models/user.rb, step:Then ]
[name:User, file:gitlabhq_gitlabhq/app/models/user.rb, step:Given ]

Methods: 34
[name:Factory, type:Object, file:null, step:Given ]
[name:add_access, type:Object, file:null, step:Given ]
[name:author_email, type:Commit, file:gitlabhq_gitlabhq/app/models/commit.rb, step:null]
[name:author_name, type:Commit, file:gitlabhq_gitlabhq/app/models/commit.rb, step:null]
[name:blob, type:RefsController, file:gitlabhq_gitlabhq/app/controllers/refs_controller.rb, step:null]
[name:each, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:null]
[name:index, type:IssuesController, file:gitlabhq_gitlabhq/app/controllers/issues_controller.rb, step:null]
[name:index, type:CommitsController, file:gitlabhq_gitlabhq/app/controllers/commits_controller.rb, step:null]
[name:issues, type:User, file:gitlabhq_gitlabhq/app/models/user.rb, step:Then ]
[name:issues, type:DashboardController, file:gitlabhq_gitlabhq/app/controllers/admin/dashboard_controller.rb, step:null]
[name:issues, type:DashboardController, file:gitlabhq_gitlabhq/app/controllers/dashboard_controller.rb, step:null]
[name:login_as, type:Object, file:null, step:Given ]
[name:merge_requests, type:User, file:gitlabhq_gitlabhq/app/models/user.rb, step:Then ]
[name:merge_requests, type:DashboardController, file:gitlabhq_gitlabhq/app/controllers/admin/dashboard_controller.rb, step:null]
[name:merge_requests, type:DashboardController, file:gitlabhq_gitlabhq/app/controllers/dashboard_controller.rb, step:null]
[name:name, type:Snippet, file:gitlabhq_gitlabhq/app/models/snippet.rb, step:Then ]
[name:name, type:Issue, file:gitlabhq_gitlabhq/app/models/issue.rb, step:null]
[name:page, type:Object, file:null, step:Then ]
[name:project, type:ApplicationController, file:gitlabhq_gitlabhq/app/controllers/application_controller.rb, step:Then ]
[name:project, type:DeployKeysController, file:gitlabhq_gitlabhq/app/controllers/deploy_keys_controller.rb, step:Then ]
[name:project, type:ProjectsController, file:gitlabhq_gitlabhq/app/controllers/projects_controller.rb, step:Then ]
[name:remove from team, type:Object, file:null, step:null]
[name:short_id, type:Commit, file:gitlabhq_gitlabhq/app/models/commit.rb, step:null]
[name:show, type:IssuesController, file:gitlabhq_gitlabhq/app/controllers/issues_controller.rb, step:null]
[name:show, type:MergeRequestsController, file:gitlabhq_gitlabhq/app/controllers/merge_requests_controller.rb, step:null]
[name:show, type:MilestonesController, file:gitlabhq_gitlabhq/app/controllers/milestones_controller.rb, step:null]
[name:show, type:CommitsController, file:gitlabhq_gitlabhq/app/controllers/commits_controller.rb, step:null]
[name:show, type:TeamMembersController, file:gitlabhq_gitlabhq/app/controllers/admin/team_members_controller.rb, step:null]
[name:show, type:TeamMembersController, file:gitlabhq_gitlabhq/app/controllers/team_members_controller.rb, step:null]
[name:title, type:CommitDecorator, file:gitlabhq_gitlabhq/app/decorators/commit_decorator.rb, step:Then ]
[name:title, type:CommitDecorator, file:gitlabhq_gitlabhq/app/decorators/commit_decorator.rb, step:null]
[name:tree, type:RefsController, file:gitlabhq_gitlabhq/app/controllers/refs_controller.rb, step:null]

Referenced pages: 38
gitlabhq_gitlabhq/app/views/commits/_commit.html.haml
gitlabhq_gitlabhq/app/views/commits/_commit_box.html.haml
gitlabhq_gitlabhq/app/views/commits/_commits.html.haml
gitlabhq_gitlabhq/app/views/commits/_diff_head.html.haml
gitlabhq_gitlabhq/app/views/commits/_diffs.html.haml
gitlabhq_gitlabhq/app/views/commits/_head.html.haml
gitlabhq_gitlabhq/app/views/commits/_text_file.html.haml
gitlabhq_gitlabhq/app/views/commits/index.html.haml
gitlabhq_gitlabhq/app/views/commits/show.html.haml
gitlabhq_gitlabhq/app/views/dashboard/issues.html.haml
gitlabhq_gitlabhq/app/views/dashboard/merge_requests.html.haml
gitlabhq_gitlabhq/app/views/issues/_head.html.haml
gitlabhq_gitlabhq/app/views/issues/_issues.html.haml
gitlabhq_gitlabhq/app/views/issues/_show.html.haml
gitlabhq_gitlabhq/app/views/issues/index.html.haml
gitlabhq_gitlabhq/app/views/issues/show.html.haml
gitlabhq_gitlabhq/app/views/merge_requests/_merge_request.html.haml
gitlabhq_gitlabhq/app/views/merge_requests/show.html.haml
gitlabhq_gitlabhq/app/views/merge_requests/show/_commits.html.haml
gitlabhq_gitlabhq/app/views/merge_requests/show/_diffs.html.haml
gitlabhq_gitlabhq/app/views/merge_requests/show/_how_to_merge.html.haml
gitlabhq_gitlabhq/app/views/merge_requests/show/_mr_accept.html.haml
gitlabhq_gitlabhq/app/views/merge_requests/show/_mr_box.html.haml
gitlabhq_gitlabhq/app/views/merge_requests/show/_mr_title.html.haml
gitlabhq_gitlabhq/app/views/milestones/show.html.haml
gitlabhq_gitlabhq/app/views/notes/_form.html.haml
gitlabhq_gitlabhq/app/views/notes/_notes.html.haml
gitlabhq_gitlabhq/app/views/notes/_per_line_form.html.haml
gitlabhq_gitlabhq/app/views/notes/_per_line_show.html.haml
gitlabhq_gitlabhq/app/views/refs/_head.html.haml
gitlabhq_gitlabhq/app/views/refs/_submodule_item.html.haml
gitlabhq_gitlabhq/app/views/refs/_tree.html.haml
gitlabhq_gitlabhq/app/views/refs/_tree_commit.html.haml
gitlabhq_gitlabhq/app/views/refs/_tree_file.html.haml
gitlabhq_gitlabhq/app/views/refs/_tree_item.html.haml
gitlabhq_gitlabhq/app/views/refs/blame.html.haml
gitlabhq_gitlabhq/app/views/refs/tree.html.haml
gitlabhq_gitlabhq/app/views/team_members/show.html.haml

