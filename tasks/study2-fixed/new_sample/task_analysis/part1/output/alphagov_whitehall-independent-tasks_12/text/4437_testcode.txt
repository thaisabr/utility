Given /^(\d+) published featured consultations$/ do |number|
  number.to_i.times { create(:featured_consultation) }
end
When /^I visit the consultations page$/ do
  visit consultations_path
end
Then /^I should only see the most recent (\d+) in the list of featured consultations$/ do |number|
  assert has_css?("#{featured_consultations_selector} .consultation", count: number.to_i)
end
