Given /^the topic "([^"]*)" contains some policies$/ do |topic_name|
  topic = create(:topic, name: topic_name)
  5.times do create(:published_policy, topics: [topic]); end
  2.times do create(:draft_policy,     topics: [topic]); end
end
Given /^the topic "([^"]*)" contains a published and a draft specialist guide$/ do |topic_name|
  specialist_guides = [build(:published_specialist_guide), build(:draft_specialist_guide)]
  create(:topic, name: topic_name, specialist_guides: specialist_guides)
end
Given /^other topics also have policies$/ do
  create(:topic, policies: [build(:published_policy)])
  create(:topic, policies: [build(:published_policy)])
end
Given /^the topic "([^"]*)" is related to the topic "([^"]*)"$/ do |name, related_name|
  related_topic = create(:topic, name: related_name)
  topic = Topic.find_by_name(name)
  topic.update_attributes!(related_topics: [related_topic])
end
When /^I visit the "([^"]*)" topic$/ do |name|
  topic = Topic.find_by_name!(name)
  visit topic_path(topic)
end
Then /^I should only see published policies belonging to the "([^"]*)" topic$/ do |name|
  topic = Topic.find_by_name!(name)
  actual_editions = records_from_elements(Edition, page.all(".policy")).sort_by(&:id)
  expected_editions = topic.policies.published.all.sort_by(&:id)
  assert_equal expected_editions, actual_editions
end
Then /^I should only see published specialist guides belonging to the "([^"]*)" topic$/ do |name|
  topic = Topic.find_by_name!(name)
  actual_editions = records_from_elements(Edition, page.all(".specialist_guide")).sort_by(&:id)
  expected_editions = topic.specialist_guides.published.all.sort_by(&:id)
  assert_equal expected_editions, actual_editions
end
Then /^I should see (#{THE_DOCUMENT})$/ do |edition|
  assert has_css?(record_css_selector(edition))
end
