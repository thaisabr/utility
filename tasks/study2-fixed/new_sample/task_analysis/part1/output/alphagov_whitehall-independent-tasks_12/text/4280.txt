Feature: Editing draft policies
In order to send the best version of a policy to the departmental editor
A writer
Should be able to edit and save draft policies
Background:
Given I am a writer
Scenario: Creating a new draft policy that's the responsibility of multiple ministers
Given ministers exist:
| Ministerial Role    | Person     |
| Minister of Finance | John Smith |
| Treasury Secretary  | Jane Doe   |
When I draft a new policy "Pinch more pennies" associated with "John Smith" and "Jane Doe"
Then I should see in the preview that "Pinch more pennies" is associated with "Minister of Finance" and "Treasury Secretary"
