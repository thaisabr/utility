Classes: 6
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Given ]
[name:Edition, file:alphagov_whitehall/app/presenters/publishing_api_presenters/edition.rb, step:Given ]
[name:GDS, file:null, step:Given ]
[name:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Given ]
[name:THE_DOCUMENT, file:null, step:Then ]
[name:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]

Methods: 8
[name:create, type:Object, file:null, step:Given ]
[name:find_by, type:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Given ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:index, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:null]
[name:index, type:OrganisationsController, file:alphagov_whitehall/app/controllers/api/organisations_controller.rb, step:null]
[name:index, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:null]
[name:login_as, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:Given ]
[name:record_css_selector, type:Object, file:null, step:Then ]

Referenced pages: 2
alphagov_whitehall/app/views/organisations/_index_section.html.erb
alphagov_whitehall/app/views/organisations/index.html.erb

