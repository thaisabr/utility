Classes: 5
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Given ]
[name:Edition, file:alphagov_whitehall/app/presenters/publishing_api_presenters/edition.rb, step:Given ]
[name:FactoryGirl, file:alphagov_whitehall/features/support/factory_girl.rb, step:Given ]
[name:GDS, file:null, step:Given ]
[name:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]

Methods: 7
[name:check_for_government, type:Object, file:null, step:Then ]
[name:create, type:Object, file:null, step:Given ]
[name:create, type:SpeechType, file:alphagov_whitehall/app/models/speech_type.rb, step:Given ]
[name:create, type:FactoryGirl, file:alphagov_whitehall/features/support/factory_girl.rb, step:Given ]
[name:create_government, type:Object, file:null, step:When ]
[name:edit_government, type:Object, file:null, step:When ]
[name:login_as, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:Given ]

Referenced pages: 0

