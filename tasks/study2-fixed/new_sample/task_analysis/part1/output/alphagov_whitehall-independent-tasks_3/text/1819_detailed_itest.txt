Classes: 7
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Given ]
[name:Edition, file:alphagov_whitehall/app/presenters/publishing_api_presenters/edition.rb, step:Given ]
[name:GDS, file:null, step:Given ]
[name:Options, file:alphagov_whitehall/lib/whitehall/document_filter/options.rb, step:When ]
[name:Publication, file:alphagov_whitehall/app/models/publication.rb, step:Then ]
[name:Topic, file:alphagov_whitehall/app/models/topic.rb, step:Then ]
[name:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]

Methods: 42
[name:admin_edition_path, type:EditionRoutesHelper, file:alphagov_whitehall/app/helpers/admin/edition_routes_helper.rb, step:When ]
[name:admin_edition_url, type:EditionRoutesHelper, file:alphagov_whitehall/app/helpers/admin/edition_routes_helper.rb, step:When ]
[name:begin_drafting_document, type:Object, file:null, step:When ]
[name:begin_drafting_publication, type:DocumentHelper, file:alphagov_whitehall/features/support/document_helper.rb, step:When ]
[name:check_edition_is_tagged_to_policies, type:Object, file:null, step:Then ]
[name:check_topic_is_tagged_to_policies, type:Object, file:null, step:Then ]
[name:click_link, type:Object, file:null, step:When ]
[name:create, type:Object, file:null, step:Given ]
[name:create, type:SpeechType, file:alphagov_whitehall/app/models/speech_type.rb, step:Given ]
[name:create, type:SpeechType, file:alphagov_whitehall/app/models/speech_type.rb, step:When ]
[name:delete, type:Options, file:alphagov_whitehall/lib/whitehall/document_filter/options.rb, step:When ]
[name:evaluate_script, type:Object, file:null, step:When ]
[name:execute_script, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:fill_in_publication_fields, type:Object, file:null, step:When ]
[name:find, type:OrganisationFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/organisation_finder.rb, step:When ]
[name:find, type:MinisterialRolesFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/ministerial_roles_finder.rb, step:When ]
[name:find, type:NewsArticleTypeFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/news_article_type_finder.rb, step:When ]
[name:find, type:OperationalFieldFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/operational_field_finder.rb, step:When ]
[name:find, type:PublicationTypeFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/publication_type_finder.rb, step:When ]
[name:find, type:RoleAppointmentsFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/role_appointments_finder.rb, step:When ]
[name:find, type:SpeechTypeFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/speech_type_finder.rb, step:When ]
[name:find_field, type:Object, file:null, step:When ]
[name:index, type:Admin/dashboardController, file:alphagov_whitehall/app/controllers/admin/dashboard_controller.rb, step:When ]
[name:index, type:Admin/findInAdminBookmarkletController, file:alphagov_whitehall/app/controllers/admin/find_in_admin_bookmarklet_controller.rb, step:When ]
[name:index, type:Admin/editionsController, file:alphagov_whitehall/app/controllers/admin/editions_controller.rb, step:When ]
[name:last, type:Publication, file:alphagov_whitehall/app/models/publication.rb, step:Then ]
[name:last, type:Topic, file:alphagov_whitehall/app/models/topic.rb, step:Then ]
[name:login_as, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:Given ]
[name:page, type:StatisticsAnnouncementsFilter, file:alphagov_whitehall/app/models/frontend/statistics_announcements_filter.rb, step:When ]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:When ]
[name:policy_1, type:Object, file:null, step:Then ]
[name:select, type:Javascript, file:alphagov_whitehall/features/support/javascript.rb, step:When ]
[name:select_from_chosen, type:Javascript, file:alphagov_whitehall/features/support/javascript.rb, step:When ]
[name:select_option, type:Object, file:null, step:When ]
[name:show, type:Admin/findInAdminBookmarkletController, file:alphagov_whitehall/app/controllers/admin/find_in_admin_bookmarklet_controller.rb, step:When ]
[name:show, type:Admin/supportingPagesController, file:alphagov_whitehall/app/controllers/admin/supporting_pages_controller.rb, step:When ]
[name:show, type:Admin/policiesController, file:alphagov_whitehall/app/controllers/admin/policies_controller.rb, step:When ]
[name:start_creating_topic, type:TopicsHelper, file:alphagov_whitehall/features/support/topics_helper.rb, step:When ]
[name:tag_to_policies, type:Object, file:null, step:Then ]
[name:to_json, type:Object, file:null, step:When ]
[name:visible?, type:Object, file:null, step:When ]

Referenced pages: 18
alphagov_whitehall/app/views/admin/dashboard/_document_table.html.erb
alphagov_whitehall/app/views/admin/dashboard/index.html.erb
alphagov_whitehall/app/views/admin/editions/_filter_options.html.erb
alphagov_whitehall/app/views/admin/editions/_search_results.html.erb
alphagov_whitehall/app/views/admin/editions/index.html.erb
alphagov_whitehall/app/views/admin/find_in_admin_bookmarklet/_bookmarklet.erb
alphagov_whitehall/app/views/admin/find_in_admin_bookmarklet/ie.html.erb
alphagov_whitehall/app/views/admin/find_in_admin_bookmarklet/index.html.erb
alphagov_whitehall/app/views/admin/find_in_admin_bookmarklet/other.html.erb
alphagov_whitehall/app/views/admin/policies/_first_published_at.html.erb
alphagov_whitehall/app/views/admin/policies/_form.html.erb
alphagov_whitehall/app/views/admin/policies/_local_government_fields.html.erb
alphagov_whitehall/app/views/admin/shared/bootstrap/_button_dropdown.html.erb
alphagov_whitehall/app/views/admin/supporting_pages/_edition_view_edit_buttons.html.erb
alphagov_whitehall/app/views/admin/supporting_pages/_extra_metadata.html.erb
alphagov_whitehall/app/views/admin/supporting_pages/_first_published_at.html.erb
alphagov_whitehall/app/views/admin/supporting_pages/_form.html.erb
alphagov_whitehall/app/views/admin/supporting_pages/_related_policy_fields.html.erb

