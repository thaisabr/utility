Given /^I am (?:a|an) (writer|editor|admin|GDS editor|GDS admin|importer|managing editor)(?: called "([^"]*)")?$/ do |role, name|
  @user = case role
  when "writer"
    create(:writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Depteditor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  when "GDS admin"
    create(:gds_admin)
  when 'importer'
    create(:importer)
  when 'managing editor'
    create(:managing_editor)
  end
  login_as @user
end
Given /^I am (?:an?) (admin|writer|editor|GDS editor) in the organisation "([^"]*)"$/ do |role, organisation_name|
  organisation = Organisation.find_by(name: organisation_name) || create(:ministerial_department, name: organisation_name)
  @user = case role
  when "admin"
    create(:user, organisation: organisation)
  when "writer"
    create(:writer, name: "Wally Writer", organisation: organisation)
  when "editor"
    create(:departmental_editor, name: "Eddie Depteditor", organisation: organisation)
  when "GDS editor"
    create(:gds_editor, organisation: organisation)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    Edition::AuditTrail.whodunnit = user
    super(user) # warden
  end
Given /^a draft (document|publication|news article|consultation|speech) "([^"]*)"(?: with summary "([^"]*)")? exists$/ do |document_type, title, summary|
  document_type = 'publication' if document_type == 'document'
  attributes = { title: title }
  attributes.merge!(summary: summary) if summary
  create("draft_#{document_class(document_type).name.underscore}".to_sym, attributes)
end
Given /^a published (publication|news article|consultation|speech|detailed guide) "([^"]*)" exists$/ do |document_type, title|
  create("published_#{document_class(document_type).name.underscore}".to_sym, title: title)
end
Given /^a published document "([^"]*)" exists$/ do |title|
  create(:published_publication, title: title)
end
Given /^a draft (publication|news article|consultation) "([^"]*)" exists in the "([^"]*)" topic$/ do |document_type, title, topic_name|
  topic = Topic.find_by!(name: topic_name)
  create("draft_#{document_class(document_type).name.underscore}".to_sym, title: title, topics: [topic])
end
Given /^a submitted (publication|news article|consultation|detailed guide) "([^"]*)" exists in the "([^"]*)" topic$/ do |document_type, title, topic_name|
  topic = Topic.find_by!(name: topic_name)
  create("submitted_#{document_class(document_type).name.underscore}".to_sym, title: title, topics: [topic])
end
Given /^a published (publication|news article|consultation) "([^"]*)" exists in the "([^"]*)" topic$/ do |document_type, title, topic_name|
  topic = Topic.find_by!(name: topic_name)
  create("published_#{document_class(document_type).name.underscore}".to_sym, title: title, topics: [topic])
end
Given /^a draft (publication|news article|consultation) "([^"]*)" was produced by the "([^"]*)" organisation$/ do |document_type, title, organisation_name|
  organisation = Organisation.find_by!(name: organisation_name)
  create("draft_#{document_class(document_type).name.underscore}".to_sym, title: title, organisations: [organisation])
end
Given /^a published (publication|news article|consultation) "([^"]*)" was produced by the "([^"]*)" organisation$/ do |document_type, title, organisation_name|
  organisation = Organisation.find_by!(name: organisation_name)
  create("published_#{document_class(document_type).name.underscore}".to_sym, title: title, organisations: [organisation])
end
Given /^a published (publication|news article|consultation) "([^"]*)" exists relating to the (?:world location|international delegation) "([^"]*)"$/ do |document_type, title, world_location_name|
  world_location = WorldLocation.find_by!(name: world_location_name)
  create("published_#{document_class(document_type).name.underscore}".to_sym, title: title, world_locations: [world_location])
end
Given /^a published (publication|news article|consultation) "([^"]*)" exists relating to the (?:world location|international delegation) "([^"]*)" produced (\d+) days ago$/ do |document_type, title, world_location_name, days_ago|

  world_location = WorldLocation.find_by!(name: world_location_name)
  create("published_#{document_class(document_type).name.underscore}".to_sym, title: title, first_published_at: days_ago.to_i.days.ago, world_locations: [world_location])
end
Given /^a submitted (publication|news article|consultation|speech|worldwide priority|detailed guide) "([^"]*)" exists$/ do |document_type, title|
  create("submitted_#{document_class(document_type).name.underscore}".to_sym, title: title)
end
Given /^another user edits the (publication|news article|consultation|speech) "([^"]*)" changing the title to "([^"]*)"$/ do |document_type, original_title, new_title|
  edition = document_class(document_type).find_by!(title: original_title)
  edition.update_attributes!(title: new_title)
end
Given /^a published (publication|news article|consultation|speech) "([^"]*)" that's the responsibility of:$/ do |document_type, title, table|
  edition = create(:"published_#{document_type}", title: title)
  table.hashes.each do |row|
    person = find_or_create_person(row["Person"])
    ministerial_role = find_or_create_ministerial_role(row["Ministerial Role"])
    unless RoleAppointment.for_role(ministerial_role).for_person(person).exists?
      role_appointment = create(:role_appointment, role: ministerial_role, person: person)
    end
    edition.role_appointments << role_appointment
  end
end
Given /^a force published (document|publication|news article|consultation|speech) "([^"]*)" was produced by the "([^"]*)" organisation$/ do |document_type, title, organisation_name|
  organisation = Organisation.find_by!(name: organisation_name)
  document_type = 'publication' if document_type == 'document'
  edition = create("draft_#{document_class(document_type).name.underscore}".to_sym, title: title, organisations: [organisation])
  visit admin_editions_path(state: :draft)
  click_link title
  publish(force: true)
end
When /^I view the (publication|news article|consultation|speech|document) "([^"]*)"$/ do |document_type, title|
  click_link title
end
When /^I visit the (publication|news article|consultation) "([^"]*)"$/ do |document_type, title|
  edition = document_class(document_type).find_by!(title: title)
  visit public_document_path(edition)
end
When /^I save my changes to the (publication|news article|consultation|speech)$/ do |document_type|
  click_button "Save"
end
When /^I edit the (publication|news article|consultation) changing the title to "([^"]*)"$/ do |document_type, new_title|
  fill_in "Title", with: new_title
  click_button "Save"
end
Then /^I should see (#{THE_DOCUMENT})$/ do |edition|
  assert has_css?(record_css_selector(edition))
end
Then(/^(#{THE_DOCUMENT}) should no longer be listed on the public site$/) do |edition|
  visit_public_index_for(edition)
  css_selector = edition.is_a?(DetailedGuide) ? 'h1.page_title' : record_css_selector(edition)
  assert page.has_no_content?(edition.title)
end
When /^I edit the new edition$/ do
  fill_in 'Title', with: "New title"
  fill_in 'Body', with: "New body"
  click_button 'Save'
end
  def document_class(type)
    type = 'edition' if type == 'document'
    type.gsub(" ", "_").classify.constantize
  end
  def publish(options = {})
    if options[:force]
      click_link "Force publish"
      page.has_css?("#forcePublishModal", visible: true)
      within '#forcePublishModal' do
        fill_in 'reason', with: "because"
        click_button 'Force publish'
      end
      unless options[:ignore_errors]
        refute_flash_alerts_exist
      end
    else
      click_button "Publish"
      unless options[:ignore_errors]
        refute_flash_alerts_exist
      end
    end
  end
  def find_or_create_person(name)
    find_person(name) || create_person(name)
  end
  def find_or_create_ministerial_role(name)
    MinisterialRole.find_by(name: name) || create(:ministerial_role, name: name)
  end
  def visit_public_index_for(edition)
    case edition
    when Publication
      visit publications_path
    when NewsArticle, Speech
      visit announcements_path
    when Consultation
      visit consultations_path
    when DetailedGuide
      visit detailed_guide_path(edition.document)
    when WorldwidePriority
      visit worldwide_priorities_path
    else
      raise "Don't know where to go for #{edition.class.name}s"
    end
  end
  def refute_flash_alerts_exist
    assert has_no_css?(".flash.alert")
  end
  def create_person(name, attributes = {})
    create(:person, split_person_name(name).merge(attributes))
  end
  def find_person(name)
    Person.where(split_person_name(name)).first
  end
  def split_person_name(name)
    if match = /^(\w+)\s*(.*?)$/.match(name)
      forename, surname = match.captures
      { title: nil, forename: forename, surname: surname, letters: nil }
    else
      raise "couldn't split \"#{name}\""
    end
  end
  def public_document_path(edition, options = {})
    document_path(edition, options)
  end
Given /^a published publication "([^"]*)" with a PDF attachment$/ do |title|
  publication = create(:published_publication, :with_file_attachment, title: title, body: "!@1")
  @attachment = publication.attachments.first
end
When /^I draft a new publication "([^"]*)" relating it to the policies "([^"]*)" and "([^"]*)"$/ do |title, first_policy, second_policy|
  begin_drafting_publication(title)
  select first_policy, from: "Policies"
  select second_policy, from: "Policies"
  click_button "Save"
end
Then /^I should see a link to the public version of the publication "([^"]*)"$/ do |publication_title|
  publication = Publication.published.find_by!(title: publication_title)
  visit admin_edition_path(publication)
  assert_match public_document_path(publication), find("a.public_version")[:href]
end
  def begin_drafting_publication(title, options = {})
    begin_drafting_document type: 'publication', title: title, summary: "Some summary of the content", alternative_format_provider: create(:alternative_format_provider)
    fill_in_publication_fields(options)
  end
  def select(value, options = {})
    if options.has_key?(:from)
      element = find(:select, options[:from], visible: :all).find(:option, value, visible: :all)
      if element.visible?
        from = options.delete(:from)
        find(:select, from, options).find(:option, value, options).select_option
      else
        select_from_chosen(value, options)
      end
    else
      find(:option, value, options).select_option
    end
  end
  def select_from_chosen(value, options = {})
    field = find_field(options[:from], visible: false, match: :first)
    option_value = page.evaluate_script("$(\"##{field[:id]} option:contains('#{value}')\").val()")

    if field[:multiple]
      page.execute_script("value = ['#{option_value}']\; if ($('##{field[:id]}').val()) {$.merge(value, $('##{field[:id]}').val())}")
      option_value = page.evaluate_script("value")
    end

    page.execute_script("$('##{field[:id]}').val(#{option_value.to_json})")
    page.execute_script("$('##{field[:id]}').trigger('liszt:updated').trigger('change')")
  end
  def admin_edition_path(edition, *args)
    polymorphic_path([:admin, edition], *args)
  end
When /^I draft a new news article "([^"]*)" relating it to the policies "([^"]*)" and "([^"]*)"$/ do |title, first_policy, second_policy|
  content_register_has_policies([first_policy, second_policy])

  begin_drafting_news_article title: title
  select first_policy, from: "Policies"
  select second_policy, from: "Policies"
  click_button "Save"
end
  def begin_drafting_news_article(options)
    begin_drafting_document(options.merge(type: "news_article", previously_published: false))
    fill_in_news_article_fields(options.slice(:first_published, :announcement_type))
  end
  def begin_drafting_document(options)
    if Organisation.count == 0
      create(:organisation)
    end
    if Topic.count == 0
      create(:topic)
    end
    visit admin_root_path
    # Make sure the dropdown is visible first, otherwise Capybara won't see the links
    find('li.create-new a', text: 'New document').click
    within 'li.create-new' do
      click_link options[:type].humanize
    end

    within 'form' do
      fill_in "edition_title", with: options[:title]
      fill_in "edition_body", with: options.fetch(:body, "Any old iron")
      fill_in "edition_summary", with: options.fetch(:summary, 'one plus one euals two!')
      fill_in_change_note_if_required
      select_topic_if_required unless options[:skip_topic_selection]

      unless options[:type] == 'world_location_news_article'
        set_lead_organisation_on_document(Organisation.first)
      end

      if options[:alternative_format_provider]
        select options[:alternative_format_provider].name, from: "edition_alternative_format_provider_id"
      end
      if options[:primary_mainstream_category]
        select options[:primary_mainstream_category].title, from: "Primary detailed guidance category"
      end

      case options[:previously_published]
      when false
        choose 'has never been published before. It is new.'
      when true
        choose 'has previously been published on another website.'
      end

    end
  end
  def set_lead_organisation_on_document(organisation, order = 1)
    select organisation.name, from: "edition_lead_organisation_ids_#{order}"
  end
  def fill_in_change_note_if_required
    if has_css?("textarea[name='edition[change_note]']", wait: false)
      fill_in "edition_change_note", with: "changes"
    end
  end
  def select_topic_if_required
    if has_css?(".edition-topic-fields", wait: false)
      within(".edition-topic-fields") do
        select Topic.first.name, from: "Topics"
      end
    end
  end
When /^I draft a new speech "([^"]*)" relating it to the policies "([^"]*)" and "([^"]*)"$/ do |title, first_policy, second_policy|
  begin_drafting_speech title: title
  select first_policy, from: "Policies"
  select second_policy, from: "Policies"
  click_button "Save"
end
  def begin_drafting_speech(options)
    organisation = create(:ministerial_department)
    person = create_person("Colonel Mustard")
    role = create(:ministerial_role, name: "Attorney General", organisations: [organisation])
    role_appointment = create(:role_appointment, person: person, role: role, started_at: Date.parse('2010-01-01'))
    speech_type = SpeechType::Transcript
    begin_drafting_document options.merge(type: 'speech', summary: "Some summary of the content", previously_published: false)
    select speech_type.singular_name, from: "Speech type"
    select "Colonel Mustard, Attorney General", from: "Speaker"
    select_date 1.day.ago.to_s, from: "Delivered on"
    fill_in "Location", with: "The Drawing Room"
  end
  def create_person(name, attributes = {})
    create(:person, split_person_name(name).merge(attributes))
  end
Given /^a published document exists with a slug that does not match the title$/ do
  @document = create(:published_publication, title: 'Some Publication')
  @original_slug = @document.slug
  @document.update_attributes(title: 'Published in error')
end
Given(/^there is a published document that is a duplicate of another page$/) do
  @existing_edition = create(:published_publication, title: 'An existing edition')
  @duplicate_edition = create(:published_publication, title: 'A duplicate edition')
end
When(/^I unpublish the duplicate, marking it as consolidated into the other page$/) do
  visit admin_edition_path(@duplicate_edition)
  click_on 'Withdraw or unpublish'
  choose 'Unpublish: consolidated into another GOV.UK page'

  within '#js-consolidated-form' do
    fill_in 'consolidated_alternative_url', with: Whitehall.url_maker.publication_url(@existing_edition.document)
    click_button 'Unpublish'
  end
end
When(/^I withdraw the publication because it is no longer government publication$/) do
  @publication = Publication.last
  visit admin_edition_path(@publication)
  click_on 'Withdraw or unpublish'
  choose 'Withdraw: no longer current government policy/activity'
  fill_in 'Public explanation (this is shown on the live site) *', with: 'We no longer believe people should shave'
  click_button 'Withdraw'

  assert_equal :withdrawn, @publication.reload.current_state
end
When(/^I edit the public explanation for withdrawal$/) do
  publication = Publication.last
  visit admin_edition_path(publication)
  click_on 'Edit withdrawal explanation'
  fill_in 'Public explanation', with: "We believe people should shave, but the government need not enforce a publication for that"
  click_button 'Update withdrawal explanation'
end
Then(/^I should see the updated explanation on the public site$/) do
  step %{the publication should be marked as withdrawn on the public site}
end
Then(/^the publication should be marked as withdrawn on the public site$/) do
  publication = Publication.last
  visit public_document_path(publication)
  assert page.has_content?(publication.title)
  assert page.has_content?('This publication was withdrawn')
  assert page.has_content?(publication.unpublishing.explanation)
end
Then(/^I should be redirected to the other page when I view the document on the public site$/) do
  visit public_document_path(@duplicate_edition)
  assert_path publication_path(@existing_edition.document)
end
When /^I unpublish the document because it was published in error$/ do
  unpublish_edition(Edition.last)
end
Then(/^there should be an editorial remark recording the fact that the document was unpublished$/) do
  edition = Edition.last
  assert_equal 'Reset to draft', edition.editorial_remarks.last.body
end
Then(/^there should be an editorial remark recording the fact that the document was withdrawn$/) do
  edition = Edition.last
  assert_equal 'Withdrawn', edition.editorial_remarks.last.body
end
Then /^I should see that the document was published in error at the original url$/ do
  visit publication_path(@original_slug)
  assert page.has_no_content?(@document.title)
  assert page.has_content?('The information on this page has been removed because it was published in error')
  assert page.has_content?('This page should never have existed')
end
  def unpublish_edition(edition)
    visit admin_edition_path(edition)
    click_on 'Withdraw or unpublish'
    choose 'Unpublish: published in error'
    within '#js-published-in-error-form' do
      fill_in 'Public explanation (this is shown on the live site)', with: 'This page should never have existed'
      yield if block_given?
      click_button 'Unpublish'
    end
  end
Given /^a fact checker has commented "([^"]*)" on the draft publication "([^"]*)"$/ do |comment, title|
  edition = create(:draft_publication, title: title)
  create(:fact_check_request, edition: edition, comments: comment)
end
Given /^"([^"]*)" has received an email requesting they fact check a draft publication "([^"]*)"$/ do |email, title|
  publication = create(:draft_publication, title: title)
  fact_check_request = create(:fact_check_request, edition: publication, email_address: email)
  Notifications.fact_check_request(fact_check_request, host: "example.com").deliver_now
end
Given /^"([^"]*)" has asked "([^"]*)" for feedback on the draft publication "([^"]*)"$/ do |requestor_email, fact_checker_email, title|
  requestor = create(:user, email: requestor_email)
  edition = create(:draft_publication, title: title)
  create(:fact_check_request, requestor: requestor, edition: edition, email_address: fact_checker_email)
end
Given /^a published publication called "([^"]*)" with feedback "([^"]*)" exists$/ do |title, comments|
  publication = create(:published_publication, title: title)
  fact_check_request = create(:fact_check_request,
                              edition: publication,
                              email_address: "user@example.com",
                              comments: comments)
end
When /^"([^"]*)" clicks the email link to the draft publication$/ do |email_address|
  email = unread_emails_for(email_address).last
  links = URI.extract(email.body.to_s, ["http", "https"])
  visit links.first
end
  def unread_emails_for(address)
    emails_for(address) - read_emails_for(address)
  end
  def emails_for(address)
    ActionMailer::Base.deliveries.select do |d|
      d.to.include?(address)
    end
  end
  def read_emails_for(address)
    read_emails[address] ||= []
  end
  def read_emails
    @read_emails ||= {}
  end
Given(/^there is a topic with published documents$/) do
  @topic = create(:topic, name: "A Topic")
  department = create(:ministerial_department, name: "A Department")

  create(:published_publication, title: "Publication #1", topics: [@topic])
  create(:published_publication, title: "Publication #2", topics: [@topic], organisations: [department])
  create(:published_news_article, title: "News #1", topics: [@topic])

  @news = create(:published_news_article, title: "News #2", topics: [@topic])
end
When(/^I filter by title$/) do
  page.fill_in "Title or slug", with: "publication"
  page.click_on "Search"
end
Then(/^I see documents with that title$/) do
  assert page.has_content?("Publication #1")
  assert page.has_content?("Publication #2")

  assert page.has_no_content?("News #1")
  assert page.has_no_content?("News #2")
end
Then(/^I see documents by that author$/) do
  assert page.has_content?("News #2")

  assert page.has_no_content?("Publication #1")
  assert page.has_no_content?("Publication #2")
  assert page.has_no_content?("News #1")
end
Then(/^I see documents with that organisation$/) do
  assert page.has_content?("Publication #2")

  assert page.has_no_content?("Publication #1")
  assert page.has_no_content?("News #1")
  assert page.has_no_content?("News #2")
end
Then(/^I see documents with that document type$/) do
  assert page.has_content?("News #1")
  assert page.has_content?("News #2")

  assert page.has_no_content?("Publication #1")
  assert page.has_no_content?("Publication #2")
end
Given /^I have imported a file that failed$/ do
  organisation = create(:organisation)
  data = %Q{
old_url,title,summary,body,organisation,publication_type,document_collection_1,publication_date,order_url,price,isbn,urn,command_paper_number,ignore_1,attachment_1_url,attachment_1_title,country_1
http://example.com/1,title,summary,body,weird organisation,weird type,,14-Dec-2011,,,,,,,,,
  }.strip
  @force_publish_import = import_data_as_document_type_for_organisation(data, 'Publication', organisation)
end
Given /^I have imported a file that succeeded$/ do
  organisation = create(:organisation)
  topic = create(:topic)
  data = %Q{
old_url,title,summary,body,organisation,publication_type,document_collection_1,publication_date,order_url,price,isbn,urn,command_paper_number,ignore_1,html_title,html_body,attachment_1_url,attachment_1_title,country_1,topic_1
http://example.com/1,title-1,a-summary,a-body,,,,14-Dec-2011,,,,,,,html-title-A,html-body-A,,,,#{topic.slug}
http://example.com/2,title-2,a-summary,a-body,,,,14-Dec-2011,,,,,,,html-title-B,html-body-B,,,,#{topic.slug}
    }.strip
  @force_publish_import = import_data_as_document_type_for_organisation(data, 'Publication', organisation)
end
  def import_data_as_document_type_for_organisation(data, document_type, organisation)
    make_sure_data_importer_user_exists
    Import.use_separate_connection

    with_import_csv_file(data) do |path|
      visit new_admin_import_path
      select document_type, from: 'Type'
      attach_file 'CSV File', path
      select organisation.name, from: 'Default organisation'
      click_button 'Save'
      click_button 'Run'

      visit current_path
    end
    Import.find(current_path.match(/admin\/imports\/(\d+)\Z/)[1])
  end
  def with_import_csv_file(data)
    tf = Tempfile.new('csv_import')
    tf << data
    tf.close
    yield tf.path
    tf.unlink
  end
  def make_sure_data_importer_user_exists
    unless User.find_by(name: 'Automatic Data Importer')
      create(:importer, name: 'Automatic Data Importer')
    end
  end
Then /^I can see some of the latest documents$/ do
  within('#recently-updated') do
    assert page.has_css?('header', text: 'Latest')
    assert page.has_link?('Policy on Topicals')
    assert page.has_link?('Examination of Events')
  end
end
Given /^there is a user called "([^"]*)"$/ do |name|
  @user = create(:writer, name: name)
end
Given /^there is a user called "([^"]*)" with email address "([^"]*)"$/ do |name, email|
  @user = create(:writer, name: name, email: email)
end
