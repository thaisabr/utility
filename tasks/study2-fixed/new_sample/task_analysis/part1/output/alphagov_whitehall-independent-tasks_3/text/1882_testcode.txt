Given(/^a government exists called "(.*?)" starting on "(.*?)"$/) do |government_name, start_date|
  FactoryGirl.create(:government, name: government_name, start_date: start_date)
end
Given(/^there is a current government$/) do
  FactoryGirl.create(:current_government)
end
When(/^I close the current government$/) do
  close_government(name: Government.current.name)
end
Then(/^there should be no active ministerial role appointments$/) do
  assert_equal 0, count_active_ministerial_role_appointments
end
  def count_active_ministerial_role_appointments
    RoleAppointment.current.for_ministerial_roles.count
  end
Given /^two cabinet ministers "([^"]*)" and "([^"]*)"$/ do |person1, person2|
  create(:role_appointment, person: create(:person, forename: person1), role: create(:ministerial_role, cabinet_member: true))
  create(:role_appointment, person: create(:person, forename: person2), role: create(:ministerial_role, cabinet_member: true))
end
Given /^I am (?:a|an) (writer|editor|admin|GDS editor|GDS admin|importer|managing editor)(?: called "([^"]*)")?$/ do |role, name|
  @user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Depteditor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  when "GDS admin"
    create(:gds_admin)
  when 'importer'
    create(:importer)
  when 'managing editor'
    create(:managing_editor)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    Edition::AuditTrail.whodunnit = user
    super(user) # warden
  end
