Feature: Publishing publications
In order to allow the public to view publications
A writer and editor
Should be able to draft and publish publications
Scenario: Creating a new draft publication related to multiple policies
Given I am a writer
And two published policies "Totally Tangy Tofu" and "Awakened Tastebuds" exist
When I draft a new publication "Healthy Eating" relating it to "Totally Tangy Tofu" and "Awakened Tastebuds"
Then I should see in the preview that "Healthy Eating" should related to "Totally Tangy Tofu" and "Awakened Tastebuds" policies
Feature: Viewing published policies
In order to obtain useful information about government
A member of the public
Should be able to view policies
Scenario: Viewing a policy that has multiple publications associated
Given a published policy "What Makes A Beard" with related published publications "Standard Beard Lengths" and "Exotic Beard Lengths"
Then I can visit the published publication "Standard Beard Lengths" from the "What Makes A Beard" policy
And I can visit the published publication "Exotic Beard Lengths" from the "What Makes A Beard" policy
