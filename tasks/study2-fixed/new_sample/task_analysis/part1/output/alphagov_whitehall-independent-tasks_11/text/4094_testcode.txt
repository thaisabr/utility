Given /^(\d+) published publications for the organisation "([^"]+)"$/ do |count, organisation|
  organisation = create(:organisation, name: organisation)
  (1..count.to_i).to_a.map { |i| create(:published_publication, title: "keyword-#{i}", organisations: [organisation]) }
end
When /^I visit the list of publications$/ do
  visit "/"
  click_link "Publications"
end
When /^I filter publications to only those from the "([^"]*)" department$/ do |department|
  # This call to `unselect` doesn't work with capybara-webkit because it does
  # not recognise the select as a multi-select.
  # Here's the fix, waiting to be merged:
  # https://github.com/thoughtbot/capybara-webkit/pull/361
  # unselect "All departments", from: "Department"
  page.evaluate_script(%{$("#departments option[value='all']").removeAttr("selected"); 1})

  select department, from: "Department"
  click_button "Refresh"
  wait_until { page.evaluate_script("jQuery.active") == 0 }
end
Then /^I should see a link to the next page of publications$/ do
  assert has_css?('#show-more-publications li.next')
end
Then /^I should see that the (next|previous) page is (\d+) of (\d+)$/ do |css_class, next_page, total_pages|
  assert has_css?("#show-more-publications .#{css_class} span", text: "#{next_page} of #{total_pages}")
end
Then /^I should see (#{THE_DOCUMENT})$/ do |edition|
  assert has_css?(record_css_selector(edition))
end
