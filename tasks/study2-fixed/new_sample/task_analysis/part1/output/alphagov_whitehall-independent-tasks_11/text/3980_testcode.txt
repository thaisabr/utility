Given /^I am (?:a|an) (writer|editor|admin)(?: called "([^"]*)")?$/ do |role, name|
  user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Editor"))
  when "admin"
    create(:user)
  end
  login_as user
end
  def login_as(user)
    GDS::SSO.test_user = user
    PaperTrail.whodunnit = user
    super(user) # warden
  end
Given /^"([^"]*)" is the "([^"]*)" for the "([^"]*)"$/ do |person_name, ministerial_role, organisation_name|
  person = find_or_create_person(person_name)
  organisation = Organisation.find_by_name(organisation_name) || create(:organisation, name: organisation_name)
  role = MinisterialRole.create!(name: ministerial_role)
  organisation.ministerial_roles << role
  create(:role_appointment, role: role, person: person, started_at: 1.year.ago, ended_at: nil)
end
When /^I visit the minister page for "([^"]*)"$/ do |name|
  visit homepage
  click_link "Ministers"
  click_link name
end
When /^there is a reshuffle and "([^"]*)" is now "([^"]*)"$/ do |person_name, ministerial_role|
  person = find_or_create_person(person_name)
  role = MinisterialRole.find_by_name(ministerial_role)
  create(:role_appointment, role: role, person: person, make_current: true)
end
  def find_or_create_person(name)
    find_person(name) || create_person(name)
  end
  def homepage
    home_path
  end
  def create_person(name)
    create(:person, split_person_name(name))
  end
  def find_person(name)
    Person.where(split_person_name(name)).first
  end
  def split_person_name(name)
    if match = /^(\w+)\s*(.*?)$/.match(name)
      forename, surname = match.captures
      { title: nil, forename: forename, surname: surname, letters: nil }
    else
      raise "couldn't split \"#{name}\""
    end
  end
Given /^a published news article "([^"]*)" associated with "([^"]*)"$/ do |title, appointee|
  person = find_person(appointee)
  appointment = find_person(appointee).current_role_appointments.last
  create(:published_news_article, title: title, role_appointments: [appointment])
end
When /^I draft a new news article "([^"]*)"$/ do |title|
  begin_drafting_document type: "news_article", title: title
  fill_in "Summary", with: "here's a simple summary"
  within ".images" do
    attach_file "File", Rails.root.join("features/fixtures/portas-review.jpg")
    fill_in "Alt text", with: 'An alternative description'
  end
  click_button "Save"
end
When /^I publish a news article "([^"]*)" associated with "([^"]*)"$/ do |title, person_name|
  begin_drafting_document type: "News Article", title: title
  select person_name, from: "Ministers"
  click_button "Save"
  click_button "Force Publish"
end
Then /^the article mentions "([^"]*)" and links to their bio page$/ do |person_name|
  visit document_path(NewsArticle.last)
  assert has_css?("a.person[href*='#{person_path(find_person(person_name))}']", text: person_name)
end
Then /^the news article tag is the same as the person in the text$/ do
  visit admin_edition_path(NewsArticle.last)
  click_button "Create new edition"
  appointment = NewsArticle.last.role_appointments.first
  assert has_css?("select#edition_role_appointment_ids option[value='#{appointment.id}'][selected=selected]")
end
Then /^I should see both the news articles for the Deputy Prime Minister role$/ do
  assert has_css?(".news_article", text: "News from Don, Deputy PM")
  assert has_css?(".news_article", text: "News from Harriet, Deputy PM")
end
Then /^I should see both the news articles for Harriet Home$/ do
  assert has_css?(".news_article", text: "News from Harriet, Deputy PM")
  assert has_css?(".news_article", text: "News from Harriet, Home Sec")
end
  def find_person(name)
    Person.where(split_person_name(name)).first
  end
  def document_path(edition, options={})
    polymorphic_path(model_name(edition), options.merge(id: edition.document))
  end
  def admin_edition_path(edition, *args)
    polymorphic_path([:admin, edition], *args)
  end
Given /^a draft (document|publication|policy|news article|consultation|speech) "([^"]*)" exists$/ do |document_type, title|
  document_type = 'policy' if document_type == 'document'
  create("draft_#{document_class(document_type).name.underscore}".to_sym, title: title)
end
When /^I visit the (publication|policy|news article|consultation) "([^"]*)"$/ do |document_type, title|
  edition = document_class(document_type).find_by_title!(title)
  visit public_document_path(edition)
end
When /^I submit (#{THE_DOCUMENT})$/ do |edition|
  visit_document_preview edition.title
  click_button "Submit to 2nd pair of eyes"
end
When /^I publish (#{THE_DOCUMENT})$/ do |edition|
  visit_document_preview edition.title
  publish
end
Then /^I should see (#{THE_DOCUMENT})$/ do |edition|
  assert has_css?(record_css_selector(edition))
end
  def document_class(type)
    type = 'edition' if type == 'document'
    type.gsub(" ", "_").classify.constantize
  end
  def visit_document_preview(title, scope = :scoped)
    document = Edition.send(scope).find_by_title(title)
    visit admin_edition_path(document)
  end
  def publish(options = {})
    click_button options[:force] ? "Force Publish" : "Publish"
    unless options[:ignore_errors]
      refute_flash_alerts_exist
    end
  end
  def refute_flash_alerts_exist
    refute has_css?(".flash.alert")
  end
  def public_document_path(edition, options = {})
    document_path(edition, options)
  end
When /^I visit the person page for "([^"]*)"$/ do |name|
  visit person_url(find_person(name))
end
