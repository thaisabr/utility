Feature: Viewing organisations
Scenario: Navigating between pages for an organisation
Given the organisation "Cabinet Office" exists
When I visit the "Cabinet Office" organisation
Then I should see the organisation navigation
When I navigate to the "Cabinet Office" organisation's About page
Then I should see the "Cabinet Office" organisation's about page
And I should see the organisation navigation
When I navigate to the "Cabinet Office" organisation's News page
Then I should see the "Cabinet Office" organisation's news page
And I should see the organisation navigation
When I navigate to the "Cabinet Office" organisation's Policies page
Then I should see the "Cabinet Office" organisation's policies page
And I should see the organisation navigation
When I navigate to the "Cabinet Office" organisation's Home page
Then I should see the "Cabinet Office" organisation's home page
When I navigate to the "Cabinet Office" organisation's Agencies & partners page
Then I should see the "Cabinet Office" organisation's Agencies & partners page
And I should see the organisation navigation
