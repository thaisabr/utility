Classes: 2
[name:SpecialistGuide, file:alphagov_whitehall/app/models/specialist_guide.rb, step:When ]
[name:THE_DOCUMENT, file:null, step:Then ]

Methods: 24
[name:begin_drafting_document, type:Object, file:null, step:When ]
[name:check, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:create, type:Object, file:null, step:Given ]
[name:document, type:Object, file:null, step:When ]
[name:find, type:Object, file:null, step:Then ]
[name:find_by_title!, type:SpecialistGuide, file:alphagov_whitehall/app/models/specialist_guide.rb, step:When ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:index, type:SpecialistGuidesController, file:alphagov_whitehall/app/controllers/admin/specialist_guides_controller.rb, step:null]
[name:index, type:SpecialistGuidesController, file:alphagov_whitehall/app/controllers/specialist_guides_controller.rb, step:null]
[name:name, type:Person, file:alphagov_whitehall/app/models/person.rb, step:null]
[name:name, type:RolePresenter, file:alphagov_whitehall/app/presenters/role_presenter.rb, step:null]
[name:nil?, type:Object, file:null, step:Given ]
[name:page, type:Object, file:null, step:Then ]
[name:public_document_path, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:null]
[name:public_document_url, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:null]
[name:published_specialist_guides, type:Object, file:null, step:null]
[name:record_css_selector, type:Object, file:null, step:Then ]
[name:refute, type:Object, file:null, step:Then ]
[name:select, type:Object, file:null, step:When ]
[name:show, type:SpecialistGuidesController, file:alphagov_whitehall/app/controllers/admin/specialist_guides_controller.rb, step:null]
[name:show, type:SpecialistGuidesController, file:alphagov_whitehall/app/controllers/specialist_guides_controller.rb, step:null]
[name:visible?, type:Object, file:null, step:Then ]

Referenced pages: 2
alphagov_whitehall/app/views/specialist_guides/index.html.erb
alphagov_whitehall/app/views/specialist_guides/show.html.erb

