Feature: Administering Organisations
Background:
Given I am an admin in the organisation "Ministry of Pop"
Scenario: Adding featured links
Given I am a GDS editor in the organisation "Ministry of Pop"
When I add some featured links to the organisation "Ministry of Pop" via the admin
Then the featured links for the organisation "Ministry of Pop" should be visible on the public site
Feature: Topics
As an editor
I want to be able to edit the description text of a topic
So that it best represents the policies that it gathers
Background:
Given I am an editor
Scenario: Adding featured links
Given a topic called "Housing prices" exists
When I add some featured links to the topic "Housing prices" via the admin
Then the featured links for the topic "Housing prices" should be visible on the public site
