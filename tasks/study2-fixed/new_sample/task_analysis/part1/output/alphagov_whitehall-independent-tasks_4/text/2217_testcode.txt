Given(/^there is a document tagged to specialist sectors$/) do
  @document = create_document_tagged_to_a_specialist_sector
  stub_content_api_tags(@document)
end
Then(/^I should not see draft specialist sectors$/) do
  check_for_absence_of_draft_sectors_and_subsectors_in_metadata
end
  def stub_content_api_tags(document)
    artefact_slug = RegisterableEdition.new(document).slug
    tag_slugs = document.specialist_sectors.map(&:tag)

    # These methods are located in gds_api_adapters
    stubbed_artefact = artefact_for_slug_with_a_child_tags('specialist_sector', artefact_slug, tag_slugs)
    content_api_has_an_artefact(artefact_slug, stubbed_artefact)
  end
  def create_document_tagged_to_a_specialist_sector
    create(:published_publication, :guidance,
            primary_specialist_sector_tag: 'oil-and-gas/wells',
            secondary_specialist_sector_tags: ['oil-and-gas/offshore', 'oil-and-gas/fields']
    )
  end
  def check_for_absence_of_draft_sectors_and_subsectors_in_metadata
    refute has_css?('dd', text: 'Distillation', exact: false)
  end
When(/^I view the document$/) do
  visit public_document_path(@document)
end
Then /^I should see (#{THE_DOCUMENT})$/ do |edition|
  assert has_css?(record_css_selector(edition))
end
Then /^I should not see (#{THE_DOCUMENT})$/ do |edition|
  assert has_no_css?(record_css_selector(edition))
end
  def public_document_path(edition, options = {})
    document_path(edition, options)
  end
