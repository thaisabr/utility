Classes: 5
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Given ]
[name:GDS, file:null, step:Given ]
[name:Options, file:alphagov_whitehall/lib/whitehall/document_filter/options.rb, step:When ]
[name:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Then ]
[name:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]

Methods: 38
[name:chars, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:collect, type:Object, file:null, step:When ]
[name:create, type:Object, file:null, step:Given ]
[name:create, type:SpeechType, file:alphagov_whitehall/app/models/speech_type.rb, step:Given ]
[name:create, type:Object, file:null, step:When ]
[name:delete, type:Options, file:alphagov_whitehall/lib/whitehall/document_filter/options.rb, step:When ]
[name:evaluate_script, type:Object, file:null, step:When ]
[name:execute_script, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find, type:OrganisationFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/organisation_finder.rb, step:When ]
[name:find, type:MinisterialRolesFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/ministerial_roles_finder.rb, step:When ]
[name:find, type:NewsArticleTypeFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/news_article_type_finder.rb, step:When ]
[name:find, type:OperationalFieldFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/operational_field_finder.rb, step:When ]
[name:find, type:PublicationTypeFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/publication_type_finder.rb, step:When ]
[name:find, type:RoleAppointmentsFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/role_appointments_finder.rb, step:When ]
[name:find, type:SpeechTypeFinder, file:alphagov_whitehall/lib/whitehall/uploader/finders/speech_type_finder.rb, step:When ]
[name:find_by_name!, type:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Then ]
[name:find_field, type:Object, file:null, step:When ]
[name:first, type:Object, file:null, step:When ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:join, type:Object, file:null, step:When ]
[name:login_as, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:Given ]
[name:new, type:Admin/organisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:When ]
[name:page, type:StatisticsAnnouncementsFilter, file:alphagov_whitehall/app/models/frontend/statistics_announcements_filter.rb, step:Then ]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:Then ]
[name:page, type:StatisticsAnnouncementsFilter, file:alphagov_whitehall/app/models/frontend/statistics_announcements_filter.rb, step:When ]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:When ]
[name:record_css_selector, type:Object, file:null, step:Then ]
[name:save, type:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]
[name:select, type:Javascript, file:alphagov_whitehall/features/support/javascript.rb, step:When ]
[name:select_from_chosen, type:Javascript, file:alphagov_whitehall/features/support/javascript.rb, step:When ]
[name:select_option, type:Object, file:null, step:When ]
[name:split, type:Object, file:null, step:When ]
[name:to_json, type:Object, file:null, step:When ]
[name:visible?, type:Object, file:null, step:When ]
[name:within, type:Object, file:null, step:When ]
[name:within, type:Object, file:null, step:Then ]

Referenced pages: 5
alphagov_whitehall/app/views/admin/organisations/_form.html.erb
alphagov_whitehall/app/views/admin/organisations/_sidebar.html.erb
alphagov_whitehall/app/views/admin/organisations/new.html.erb
alphagov_whitehall/app/views/admin/shared/_default_news_image_fields.html.erb
alphagov_whitehall/app/views/admin/shared/_featured_link_fields.html.erb

