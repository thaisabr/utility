Given /^I am (?:a|an) (writer|editor|admin|GDS editor|importer|managing editor)(?: called "([^"]*)")?$/ do |role, name|
  @user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Depteditor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  when 'importer'
    create(:importer)
  when 'managing editor'
    create(:managing_editor)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    Edition::AuditTrail.whodunnit = user
    super(user) # warden
  end
Given(/^a (draft|submitted) scheduled news article exists$/) do |state|
  @news_article = create(:news_article, state.to_sym, scheduled_publication: 1.day.from_now)
end
When(/^I schedule the news article for publication$/) do
  visit admin_news_article_path(@news_article)
  click_on "Schedule"
end
When(/^I force schedule the news article for publication$/) do
  visit admin_news_article_path(@news_article)
  click_on "Force schedule"
end
Then(/^the news article is published when the scheduled publication time arrives$/) do
  assert scheduled_publishing_job_for(@news_article)

  Timecop.travel(1.day.from_now + 1.second) do
    execute_scheduled_publication_job_for(@news_article)

    visit news_article_path(@news_article.slug)
    assert page.has_content?(@news_article.title)
  end
end
  def execute_scheduled_publication_job_for(edition)
    job = scheduled_publishing_job_for(edition)

    worker = ScheduledPublishingWorker.new
    worker.jid = job.jid
    worker.perform(*job.args)
  end
  def scheduled_publishing_job_for(edition)
    Sidekiq::ScheduledSet.new.detect do |job|
      job.args[0] == edition.id &&
      job.klass == 'ScheduledPublishingWorker'
    end
  end
