Given /^the organisation "([^"]*)" exists$/ do |name|
  create(:ministerial_department, name: name)
end
When /^I add a new "([^"]*)" role named "([^"]*)" to the "([^"]*)"$/ do |role_type, role_name, organisation_name|
  @role_name = role_name

  visit admin_roles_path
  click_on "Create role"
  fill_in "Role title", with: role_name
  select role_type, from: "Type"
  select organisation_name, from: "Organisations"
  click_on "Save"
end
Given(/^a role named "(.*?)" in the "(.*?)" organisation exists$/) do |role_name, organisation_name|
  organisation = Organisation.find_by_name(organisation_name)
  create(:role, name: role_name, people: [], organisations: [organisation])
end
When(/^I mark that role as inactive$/) do
  role = Role.last
  visit admin_roles_path
  click_on role.name
  select "No longer exists", from: "status"
  click_on "Save"
end
Then(/^I should see an inactive role banner on the role page$/) do
  role = Role.last
  visit polymorphic_path(role)
  assert page.has_content?("#{role.name} no longer exists")
end
When(/^I mark the role "(.*?)" as replaced by "(.*?)"$/) do |role_name, replacement_role_name|
  role = Role.find_by_name(role_name)
  replacement_role = Role.find_by_name(replacement_role_name)
  visit admin_roles_path
  click_on role.name
  select "Replaced", from: "status"
  select "#{replacement_role.name}", from: "role_superseding_role_ids"
  click_on "Save"
end
Then(/^I should see a replaced role banner on the "(.*?)" role page$/) do |role_name|
  role = Role.find_by_name(role_name)
  replacement_role = role.superseding_roles.first
  visit polymorphic_path(role)
  assert page.has_content?("#{role.name} was replaced by <a href=\"#{polymorphic_path(replacement_role)}\">#{replacement_role.name}</a>")
end
  def select(value, options = {})
    if options.has_key?(:from)
      element = find(:select, options[:from], visible: :all).find(:option, value, visible: :all)
      if element.visible?
        from = options.delete(:from)
        find(:select, from, options).find(:option, value, options).select_option
      else
        select_from_chosen(value, options)
      end
    else
      find(:option, value, options).select_option
    end
  end
  def select_from_chosen(value, options = {})
    field = find_field(options[:from], visible: false, match: :first)
    option_value = page.evaluate_script("$(\"##{field[:id]} option:contains('#{value}')\").val()")

    if field[:multiple]
      page.execute_script("value = ['#{option_value}']\; if ($('##{field[:id]}').val()) {$.merge(value, $('##{field[:id]}').val())}")
      option_value = page.evaluate_script("value")
    end

    page.execute_script("$('##{field[:id]}').val(#{option_value.to_json})")
    page.execute_script("$('##{field[:id]}').trigger('liszt:updated').trigger('change')")
  end
Then /^I should see (#{THE_DOCUMENT})$/ do |edition|
  assert has_css?(record_css_selector(edition))
end
Given /^I am (?:a|an) (writer|editor|admin|GDS editor|importer|managing editor)(?: called "([^"]*)")?$/ do |role, name|
  @user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Depteditor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  when 'importer'
    create(:importer)
  when 'managing editor'
    create(:managing_editor)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    Edition::AuditTrail.whodunnit = user
    super(user) # warden
  end
