Given /^an? (world location|international delegation) "([^"]*)" exists$/ do |world_location_type, name|
  create(world_location_type.gsub(' ', '_').to_sym, name: name)
end
When /^I view the (?:world location|international delegation) "([^"]*)"$/ do |name|
  world_location = WorldLocation.find_by!(name: name)
  visit world_location_path(world_location)
end
Given(/^govuk delivery exists$/) do
  mock_govuk_delivery_client
end
Given(/^email alert api exists$/) do
  mock_email_alert_api
end
When(/^I sign up for emails$/) do
  within '.feeds' do
    click_on 'email'
  end

  # There is a bug which is causes external urls to get requested from the
  # server. So catch the routing error and handle it so we can continue to
  # assert that the right things have happened to generate the redirect.
  begin
    click_on 'Create subscription'
  rescue ActionController::RoutingError
  end
end
Then(/^I should be signed up for the "(.*?)" world location mailing list$/) do |world_location_name|
  world_location_slug = WorldLocation.find_by!(name: world_location_name).slug
  assert_signed_up_to_mailing_list("/government/world/#{world_location_slug}.atom", world_location_name)
end
Then(/^a govuk_delivery notification should have been sent to the mailing list I signed up for$/) do
  mock_govuk_delivery_client.assert_method_called(:notify, with: ->(feed_urls, _subject, _body, logging_params) {
    feed_urls.include?(@feed_signed_up_to)
  })
end
def mock_govuk_delivery_client
  @mock_client ||= RetrospectiveStub.new.tap { |mock_client|
    mock_client.stub :topic, returns: mock(parsed_content: { 'topic_id' => 'TOPIC_123', 'success' => true })
    mock_client.stub :signup_url, returns: 'http://govdelivery.url'
    mock_client.stub :notify
    Whitehall.stubs(govuk_delivery_client: mock_client)
  }
end
def mock_email_alert_api
  @email_mock_client ||= RetrospectiveStub.new.tap { |mock_client|
    mock_client.stub :find_or_create_subscriber_list, returns: { 'subscriber_list' => { 'topic_id' => 'TOPIC_123'} }
    EmailAlertApiSignupWorker.any_instance.stubs(email_alert_api: mock_client)
  }
end
def assert_signed_up_to_mailing_list(feed_path, description)
  @feed_signed_up_to = public_url(feed_path)
  mock_govuk_delivery_client.assert_method_called(:topic, with: [@feed_signed_up_to, description])
  mock_govuk_delivery_client.assert_method_called(:signup_url, with: [@feed_signed_up_to])
end
  def assert_method_called(method, opts = {})
    raise UnsatisfiedAssertion.new("Expected :#{method} to have been called, but wasn't\n\nCalls: \n#{inspect_calls}") unless @calls.any? { | call |
      call[:method] == method
    }

    if opts[:with].present?
      raise UnsatisfiedAssertion.new("Expected :#{method} to have been called #{inspect_args opts[:with]}, but wasn't\n\nCalls: \n#{inspect_calls}") unless @calls.any? { | call |
        call[:method] == method && (
          opts[:with].is_a?(Proc) ? opts[:with].call(*call[:args]) : opts[:with] == call[:args]
        )
      }
    end
  end
  def stub(method, opts = {})
    stubs << {
      method: method,
      with: opts[:with],
      returns: opts[:returns]
    }
  end
  def inspect_calls
    calls.map { |call|
      ":#{call[:method]}, Arguments: #{call[:args]}"
    }.join "\n"
  end
  def inspect_args(args)
    if args.is_a? Proc
      return "matching block: #{args.source}"
    else
      "with: #{args.inspect}"
    end
  end
  def public_url(path)
    (Plek.new.website_uri + path).to_s
  end
When /^I publish (#{THE_DOCUMENT})$/ do |edition|
  visit_edition_admin edition.title
  publish
end
  def visit_edition_admin(title, scope = :all)
    document = Edition.send(scope).find_by(title: title)
    visit admin_edition_path(document)
  end
  def publish(options = {})
    if options[:force]
      click_link "Force publish"
      page.has_css?(".force-publish-form", visible: true)
      within '.force-publish-form' do
        fill_in 'reason', with: "because"
        click_button 'Force publish'
      end
      unless options[:ignore_errors]
        refute_flash_alerts_exist
      end
    else
      click_button "Publish"
      unless options[:ignore_errors]
        refute_flash_alerts_exist
      end
    end
  end
  def refute_flash_alerts_exist
    assert has_no_css?(".flash.alert")
  end
  def admin_edition_path(edition, *args)
    polymorphic_path([:admin, edition], *args)
  end
Given /^I am (?:a|an) (writer|editor|admin|GDS editor|GDS admin|importer|managing editor)(?: called "([^"]*)")?$/ do |role, name|
  @user = case role
  when "writer"
    create(:writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Depteditor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  when "GDS admin"
    create(:gds_admin)
  when 'importer'
    create(:importer)
  when 'managing editor'
    create(:managing_editor)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    Edition::AuditTrail.whodunnit = user
    super(user) # warden
  end
When(/^I publish a news article "(.*?)" for "(.*?)"$/) do |title, location_name|
  begin_drafting_news_article(title: title, first_published: Time.zone.today.to_s)
  select location_name, from: "Select the world locations this news article is about"
  click_button "Save"
  publish(force: true)
end
  def select(value, options = {})
    if options.has_key?(:from)
      element = find(:select, options[:from], visible: :all).find(:option, value, visible: :all)
      if element.visible?
        from = options.delete(:from)
        find(:select, from, options).find(:option, value, options).select_option
      else
        select_from_chosen(value, options)
      end
    else
      find(:option, value, options).select_option
    end
  end
  def publish(options = {})
    if options[:force]
      click_link "Force publish"
      page.has_css?(".force-publish-form", visible: true)
      within '.force-publish-form' do
        fill_in 'reason', with: "because"
        click_button 'Force publish'
      end
      unless options[:ignore_errors]
        refute_flash_alerts_exist
      end
    else
      click_button "Publish"
      unless options[:ignore_errors]
        refute_flash_alerts_exist
      end
    end
  end
  def select_from_chosen(value, options = {})
    field = find_field(options[:from], visible: false, match: :first)
    option_value = page.evaluate_script("$(\"##{field[:id]} option:contains('#{value}')\").val()")

    if field[:multiple]
      page.execute_script("value = ['#{option_value}']\; if ($('##{field[:id]}').val()) {$.merge(value, $('##{field[:id]}').val())}")
      option_value = page.evaluate_script("value")
    end

    page.execute_script("$('##{field[:id]}').val(#{option_value.to_json})")
    page.execute_script("$('##{field[:id]}').trigger('liszt:updated').trigger('change')")
  end
