Classes: 5
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Given ]
[name:GDS, file:null, step:Given ]
[name:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:When ]
[name:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Then ]
[name:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]

Methods: 60
[name:about, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:Then ]
[name:about, type:OrganisationsController, file:alphagov_whitehall/app/controllers/api/organisations_controller.rb, step:Then ]
[name:about, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:Then ]
[name:announcements_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:Then ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:create, type:Object, file:null, step:Given ]
[name:create, type:SpeechType, file:alphagov_whitehall/app/models/speech_type.rb, step:Given ]
[name:css_classes, type:PromotionalFeature, file:alphagov_whitehall/app/models/promotional_feature.rb, step:Then ]
[name:edit, type:Admin/organisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:Then ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find_by_name!, type:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:When ]
[name:find_by_name!, type:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Then ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:id, type:AttachmentVisibility, file:alphagov_whitehall/app/models/attachment_visibility.rb, step:Then ]
[name:id, type:FeaturePresenter, file:alphagov_whitehall/app/presenters/feature_presenter.rb, step:Then ]
[name:index, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:Then ]
[name:index, type:OrganisationsController, file:alphagov_whitehall/app/controllers/api/organisations_controller.rb, step:Then ]
[name:index, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:Then ]
[name:items, type:HomePageList, file:alphagov_whitehall/app/models/home_page_list.rb, step:Then ]
[name:items, type:PromotionalFeature, file:alphagov_whitehall/app/models/promotional_feature.rb, step:Then ]
[name:login_as, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:Given ]
[name:mainstream_category_path, type:MainstreamCategoryRoutesHelper, file:alphagov_whitehall/app/helpers/mainstream_category_routes_helper.rb, step:Then ]
[name:new, type:EmailSignupsController, file:alphagov_whitehall/app/controllers/email_signups_controller.rb, step:Then ]
[name:organisation, type:EditionFilter, file:alphagov_whitehall/app/models/admin/edition_filter.rb, step:Then ]
[name:organisation, type:ImportRowWorker, file:alphagov_whitehall/app/workers/import_row_worker.rb, step:Then ]
[name:organisation, type:FatalityNoticeRow, file:alphagov_whitehall/lib/whitehall/uploader/fatality_notice_row.rb, step:Then ]
[name:organisation, type:Row, file:alphagov_whitehall/lib/whitehall/uploader/row.rb, step:Then ]
[name:page, type:StatisticsAnnouncementsFilter, file:alphagov_whitehall/app/models/frontend/statistics_announcements_filter.rb, step:Then ]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:Then ]
[name:person, type:Appointment, file:alphagov_whitehall/app/models/edition/appointment.rb, step:Then ]
[name:person, type:RoleAppointmentPresenter, file:alphagov_whitehall/app/presenters/role_appointment_presenter.rb, step:Then ]
[name:policies_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:Then ]
[name:promotional_feature_items, type:PromotionalFeature, file:alphagov_whitehall/app/models/promotional_feature.rb, step:Then ]
[name:public_document_path, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:Then ]
[name:public_document_url, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:Then ]
[name:public_document_url, type:AuthorNotifier, file:alphagov_whitehall/app/services/service_listeners/author_notifier.rb, step:Then ]
[name:publications_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:Then ]
[name:see_all_link, type:Object, file:null, step:Then ]
[name:show, type:Admin/organisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:Then ]
[name:show, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:Then ]
[name:show, type:OrganisationsController, file:alphagov_whitehall/app/controllers/api/organisations_controller.rb, step:Then ]
[name:show, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:Then ]
[name:show, type:CorporateInformationPagesController, file:alphagov_whitehall/app/controllers/admin/corporate_information_pages_controller.rb, step:Then ]
[name:show, type:CorporateInformationPagesController, file:alphagov_whitehall/app/controllers/corporate_information_pages_controller.rb, step:Then ]
[name:show, type:TopicsController, file:alphagov_whitehall/app/controllers/admin/topics_controller.rb, step:Then ]
[name:show, type:TopicsController, file:alphagov_whitehall/app/controllers/topics_controller.rb, step:Then ]
[name:summary, type:DetailedGuide, file:alphagov_whitehall/app/models/detailed_guide.rb, step:Then ]
[name:text, type:Object, file:null, step:Then ]
[name:title, type:PromotionalFeature, file:alphagov_whitehall/app/models/promotional_feature.rb, step:Then ]
[name:title, type:EditionFilter, file:alphagov_whitehall/app/models/admin/edition_filter.rb, step:Then ]
[name:title, type:CorporateInformationPage, file:alphagov_whitehall/app/models/corporate_information_page.rb, step:Then ]
[name:title, type:RegisterableEdition, file:alphagov_whitehall/app/models/registerable_edition.rb, step:Then ]
[name:title, type:FeaturePresenter, file:alphagov_whitehall/app/presenters/feature_presenter.rb, step:Then ]
[name:title, type:PromotionalFeatureItemPresenter, file:alphagov_whitehall/app/presenters/promotional_feature_item_presenter.rb, step:Then ]
[name:title, type:Row, file:alphagov_whitehall/lib/whitehall/uploader/row.rb, step:Then ]
[name:title, type:DetailedGuide, file:alphagov_whitehall/app/models/detailed_guide.rb, step:Then ]
[name:visit_organisation, type:Paths, file:alphagov_whitehall/features/support/paths.rb, step:Then ]
[name:within, type:Object, file:null, step:When ]
[name:within, type:Object, file:null, step:Then ]

Referenced pages: 41
alphagov_whitehall/app/views/admin/organisations/_form.html.erb
alphagov_whitehall/app/views/admin/organisations/_sidebar.html.erb
alphagov_whitehall/app/views/admin/organisations/edit.html.erb
alphagov_whitehall/app/views/admin/organisations/show.html.erb
alphagov_whitehall/app/views/admin/shared/_default_news_image_fields.html.erb
alphagov_whitehall/app/views/admin/shared/_featured_link_fields.html.erb
alphagov_whitehall/app/views/classifications/_list_description.html.erb
alphagov_whitehall/app/views/contacts/_contact.html.erb
alphagov_whitehall/app/views/corporate_information_pages/show.html.erb
alphagov_whitehall/app/views/corporate_information_pages/show_worldwide_organisation.html.erb
alphagov_whitehall/app/views/email_signups/_form.html.erb
alphagov_whitehall/app/views/email_signups/new.html.erb
alphagov_whitehall/app/views/mainstream_categories/_list_description.html.erb
alphagov_whitehall/app/views/organisations/_alternate_style_top_tasks.html.erb
alphagov_whitehall/app/views/organisations/_corporate_information.html.erb
alphagov_whitehall/app/views/organisations/_documents.html.erb
alphagov_whitehall/app/views/organisations/_featured_items.html.erb
alphagov_whitehall/app/views/organisations/_header.html.erb
alphagov_whitehall/app/views/organisations/_index_section.html.erb
alphagov_whitehall/app/views/organisations/_organisations_name_list.html.erb
alphagov_whitehall/app/views/organisations/_promotional_feature_item.html.erb
alphagov_whitehall/app/views/organisations/_transition_state_visualisation.html.erb
alphagov_whitehall/app/views/organisations/about.html.erb
alphagov_whitehall/app/views/organisations/index.html.erb
alphagov_whitehall/app/views/organisations/show-promotional.html.erb
alphagov_whitehall/app/views/organisations/show.html.erb
alphagov_whitehall/app/views/people/_person.html.erb
alphagov_whitehall/app/views/policies/_list_description.html.erb
alphagov_whitehall/app/views/shared/_available_languages.html.erb
alphagov_whitehall/app/views/shared/_featured.html.erb
alphagov_whitehall/app/views/shared/_featured_links.html.erb
alphagov_whitehall/app/views/shared/_featured_news.html.erb
alphagov_whitehall/app/views/shared/_feeds.html.erb
alphagov_whitehall/app/views/shared/_govspeak_header_contents.html.erb
alphagov_whitehall/app/views/shared/_heading.html.erb
alphagov_whitehall/app/views/shared/_list_description.html.erb
alphagov_whitehall/app/views/shared/_recently_updated.html.erb
alphagov_whitehall/app/views/shared/_recently_updated_documents.html.erb
alphagov_whitehall/app/views/shared/_social_media_accounts.html.erb
alphagov_whitehall/app/views/topics/show.html.erb
alphagov_whitehall/app/views/worldwide_organisations/_header.html.erb

