Feature: Creating and publishing topical events
As an editor
I want to be able to create and publish topical events
So that I can communicate about them
Background:
Given I am an editor
Scenario: Deleting a topical event
Given a topical event called "An event" with description "A topical event"
Then I should be able to delete the topical event "An event"
