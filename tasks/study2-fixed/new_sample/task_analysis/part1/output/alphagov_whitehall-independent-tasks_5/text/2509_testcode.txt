Given /^a (topic|mainstream category) with the slug "([^"]*)" exists$/ do |type, slug|
  o = create(type.parameterize.underscore)
  o.update_attributes!(slug: slug)
end
When /^I import the following data as CSV as "([^"]*)" for "([^"]*)":$/ do |document_type, organisation_name, data|
  organisation = Organisation.find_by_name(organisation_name) || create(:organisation, name: organisation_name)
  import_data_as_document_type_for_organisation(data, document_type, organisation)
end
Then /^the import succeeds, creating (\d+) imported speech(?:es)? with "([^"]*)" speech type and with no deliverer set$/ do |edition_count, speech_type_slug|
  speech_type = SpeechType.find_by_slug(speech_type_slug)
  assert_equal edition_count.to_i, Edition.imported.count

  edition = Edition.imported.first
  assert_kind_of Speech, edition
  assert_equal speech_type, edition.speech_type
  assert_nil edition.role_appointment
end
Then /^I can't make the imported (?:publication|speech|news article|consultation) into a draft edition yet$/ do
  visit_edition_admin Edition.imported.last.title

  assert page.has_css?('input[type=submit][disabled=disabled][value="Convert to draft"]')
end
Then /^I can make the imported (?:publication|speech|news article|consultation) into a draft edition$/ do
  edition = Edition.imported.last
  visit_edition_admin edition.title

  click_on 'Convert to draft'

  edition.reload
  assert edition.draft?
end
Then /^the imported speech's organisation is set to "([^"]*)"$/ do |organisation_name|
  assert_equal organisation_name, Edition.imported.last.organisations.first.name
end
When /^I set the imported speech's type to "([^"]*)"$/ do |speech_type|
  begin_editing_document Edition.imported.last.title
  select speech_type, from: 'Speech type'
  click_on 'Save'
end
When /^I set the deliverer of the speech to "([^"]*)" from the "([^"]*)"$/ do |person_name, organisation_name|
  person = find_or_create_person(person_name)
  organisation = create(:ministerial_department, name: organisation_name)
  role = create(:role, organisations: [organisation])
  create(:role_appointment, role: role, person: person)

  begin_editing_document Edition.imported.last.title
  select person_name, from: 'Speaker'
  click_on 'Save'
end
  def import_data_as_document_type_for_organisation(data, document_type, organisation)
    make_sure_data_importer_user_exists
    Import.use_separate_connection

    with_import_csv_file(data) do |path|
      visit new_admin_import_path
      select document_type, from: 'Type'
      attach_file 'CSV File', path
      select organisation.name, from: 'Default organisation'
      click_button 'Save'
      click_button 'Run'

      visit current_path
    end
    Import.find(current_path.match(/admin\/imports\/(\d+)\Z/)[1])
  end
  def begin_editing_document(title)
    visit_edition_admin title
    click_link "Edit draft"
  end
  def visit_edition_admin(title, scope = :scoped)
    document = Edition.send(scope).find_by_title(title)
    visit admin_edition_path(document)
  end
  def select(value, options={})
    if options.has_key?(:from)
      element = find(:select, options[:from], visible: :all).find(:option, value, visible: :all)
      if element.visible?
        from = options.delete(:from)
        find(:select, from, options).find(:option, value, options).select_option
      else
        select_from_chosen(value, options)
      end
    else
      find(:option, value, options).select_option
    end
  end
  def find_or_create_person(name)
    find_person(name) || create_person(name)
  end
  def with_import_csv_file(data)
    tf = Tempfile.new('csv_import')
    tf << data
    tf.close
    yield tf.path
    tf.unlink
  end
  def make_sure_data_importer_user_exists
    unless User.find_by_name('Automatic Data Importer')
      create(:importer, name: 'Automatic Data Importer')
    end
  end
  def visit_edition_admin(title, scope = :scoped)
    document = Edition.send(scope).find_by_title(title)
    visit admin_edition_path(document)
  end
  def select_from_chosen(value, options={})
    field = find_field(options[:from], visible: false, match: :first)
    option_value = page.evaluate_script("$(\"##{field[:id]} option:contains('#{value}')\").val()")

    if field[:multiple]
      page.execute_script("value = ['#{option_value}']\; if ($('##{field[:id]}').val()) {$.merge(value, $('##{field[:id]}').val())}")
      option_value = page.evaluate_script("value")
    end

    page.execute_script("$('##{field[:id]}').val(#{option_value.to_json})")
    page.execute_script("$('##{field[:id]}').trigger('liszt:updated').trigger('change')")
  end
  def create_person(name, attributes = {})
    create(:person, split_person_name(name).merge(attributes))
  end
  def find_person(name)
    Person.where(split_person_name(name)).first
  end
  def split_person_name(name)
    if match = /^(\w+)\s*(.*?)$/.match(name)
      forename, surname = match.captures
      { title: nil, forename: forename, surname: surname, letters: nil }
    else
      raise "couldn't split \"#{name}\""
    end
  end
  def admin_edition_path(edition, *args)
    polymorphic_path([:admin, edition], *args)
  end
Given /^I am (?:a|an) (writer|editor|admin|GDS editor|importer|managing editor)(?: called "([^"]*)")?$/ do |role, name|
  @user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Depteditor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  when 'importer'
    create(:importer)
  when 'managing editor'
    create(:managing_editor)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    Edition::AuditTrail.whodunnit = user
    super(user) # warden
  end
