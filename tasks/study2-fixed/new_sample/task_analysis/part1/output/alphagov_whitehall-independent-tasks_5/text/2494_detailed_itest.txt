Classes: 3
[name:TopicalEvent, file:alphagov_whitehall/app/models/topical_event.rb, step:Given ]
[name:TopicalEvent, file:alphagov_whitehall/app/models/topical_event.rb, step:When ]
[name:TopicalEvent, file:alphagov_whitehall/app/models/topical_event.rb, step:Then ]

Methods: 56
[name:ago, type:Object, file:null, step:Given ]
[name:assert_equal, type:Object, file:null, step:Then ]
[name:body, type:Row, file:alphagov_whitehall/lib/whitehall/uploader/row.rb, step:Then ]
[name:body, type:StatisticalDataSetRow, file:alphagov_whitehall/lib/whitehall/uploader/statistical_data_set_row.rb, step:Then ]
[name:click_link, type:Object, file:null, step:Then ]
[name:create, type:Object, file:null, step:Given ]
[name:create_recently_published_documents_for_topical_event, type:TopicalEventsHelper, file:alphagov_whitehall/features/support/topical_events_helper.rb, step:Given ]
[name:days, type:Object, file:null, step:Given ]
[name:document_path, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:Then ]
[name:document_path, type:Unpublishing, file:alphagov_whitehall/app/models/unpublishing.rb, step:Then ]
[name:document_url, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:Then ]
[name:document_url, type:Mappings, file:alphagov_whitehall/lib/whitehall/exporters/mappings.rb, step:Then ]
[name:each, type:DocumentHistory, file:alphagov_whitehall/app/models/document_history.rb, step:Given ]
[name:each, type:EditionCollectionPresenter, file:alphagov_whitehall/app/presenters/edition_collection_presenter.rb, step:Given ]
[name:each, type:DocumentHistory, file:alphagov_whitehall/app/models/document_history.rb, step:Then ]
[name:each, type:EditionCollectionPresenter, file:alphagov_whitehall/app/presenters/edition_collection_presenter.rb, step:Then ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:has_link?, type:Object, file:null, step:Then ]
[name:index, type:LatestController, file:alphagov_whitehall/app/controllers/latest_controller.rb, step:Then ]
[name:length, type:Object, file:null, step:Then ]
[name:name, type:TopicalEvent, file:alphagov_whitehall/app/models/topical_event.rb, step:Then ]
[name:new, type:EmailSignupsController, file:alphagov_whitehall/app/controllers/email_signups_controller.rb, step:Given ]
[name:new, type:EmailSignupsController, file:alphagov_whitehall/app/controllers/email_signups_controller.rb, step:Then ]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:Then ]
[name:policies_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:Given ]
[name:policies_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:Then ]
[name:public_document_path, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:Given ]
[name:public_document_path, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:Then ]
[name:public_document_url, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:Given ]
[name:public_document_url, type:AuthorNotifier, file:alphagov_whitehall/app/services/service_listeners/author_notifier.rb, step:Given ]
[name:public_document_url, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:Then ]
[name:public_document_url, type:AuthorNotifier, file:alphagov_whitehall/app/services/service_listeners/author_notifier.rb, step:Then ]
[name:sample_document_types_and_titles, type:TopicalEventsHelper, file:alphagov_whitehall/features/support/topical_events_helper.rb, step:Given ]
[name:sample_document_types_and_titles, type:TopicalEventsHelper, file:alphagov_whitehall/features/support/topical_events_helper.rb, step:Then ]
[name:scan, type:Object, file:null, step:Then ]
[name:see_all_link, type:Object, file:null, step:Given ]
[name:see_all_link, type:Object, file:null, step:Then ]
[name:show, type:TopicalEventsController, file:alphagov_whitehall/app/controllers/admin/topical_events_controller.rb, step:Given ]
[name:show, type:TopicalEventsController, file:alphagov_whitehall/app/controllers/topical_events_controller.rb, step:Given ]
[name:show, type:AboutPagesController, file:alphagov_whitehall/app/controllers/about_pages_controller.rb, step:Given ]
[name:show, type:AboutPagesController, file:alphagov_whitehall/app/controllers/admin/about_pages_controller.rb, step:Given ]
[name:show, type:TopicsController, file:alphagov_whitehall/app/controllers/admin/topics_controller.rb, step:Given ]
[name:show, type:TopicsController, file:alphagov_whitehall/app/controllers/topics_controller.rb, step:Given ]
[name:show, type:TopicalEventsController, file:alphagov_whitehall/app/controllers/admin/topical_events_controller.rb, step:Then ]
[name:show, type:TopicalEventsController, file:alphagov_whitehall/app/controllers/topical_events_controller.rb, step:Then ]
[name:show, type:AboutPagesController, file:alphagov_whitehall/app/controllers/about_pages_controller.rb, step:Then ]
[name:show, type:AboutPagesController, file:alphagov_whitehall/app/controllers/admin/about_pages_controller.rb, step:Then ]
[name:show, type:TopicsController, file:alphagov_whitehall/app/controllers/admin/topics_controller.rb, step:Then ]
[name:show, type:TopicsController, file:alphagov_whitehall/app/controllers/topics_controller.rb, step:Then ]
[name:subject, type:LatestController, file:alphagov_whitehall/app/controllers/latest_controller.rb, step:Then ]
[name:summary, type:DetailedGuide, file:alphagov_whitehall/app/models/detailed_guide.rb, step:Given ]
[name:summary, type:DetailedGuide, file:alphagov_whitehall/app/models/detailed_guide.rb, step:Then ]
[name:title, type:DetailedGuide, file:alphagov_whitehall/app/models/detailed_guide.rb, step:Given ]
[name:title, type:DetailedGuide, file:alphagov_whitehall/app/models/detailed_guide.rb, step:Then ]
[name:with_index, type:Object, file:null, step:Given ]
[name:within, type:Object, file:null, step:Then ]

Referenced pages: 16
alphagov_whitehall/app/views/about_pages/show.html.erb
alphagov_whitehall/app/views/email_signups/_form.html.erb
alphagov_whitehall/app/views/email_signups/new.html.erb
alphagov_whitehall/app/views/latest/index.html.erb
alphagov_whitehall/app/views/organisations/_organisations_name_list.html.erb
alphagov_whitehall/app/views/policies/_list_description.html.erb
alphagov_whitehall/app/views/shared/_featured_news.html.erb
alphagov_whitehall/app/views/shared/_feeds.html.erb
alphagov_whitehall/app/views/shared/_govspeak_header_contents.html.erb
alphagov_whitehall/app/views/shared/_heading.html.erb
alphagov_whitehall/app/views/shared/_recently_updated.html.erb
alphagov_whitehall/app/views/shared/_recently_updated_documents.html.erb
alphagov_whitehall/app/views/shared/_social_media_accounts.html.erb
alphagov_whitehall/app/views/shared/_top_tasks.html.erb
alphagov_whitehall/app/views/topical_events/show.html.erb
alphagov_whitehall/app/views/topics/show.html.erb

