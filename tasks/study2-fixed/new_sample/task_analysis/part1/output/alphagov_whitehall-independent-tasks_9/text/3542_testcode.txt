When /^I create a new worldwide organisation "([^"]*)" in "([^"]*)"$/ do |name, location|
  visit new_admin_worldwide_organisation_path
  fill_in "Name", with: name
  fill_in "Logo formatted name", with: name
  fill_in "Summary", with: "Worldwide organisation summary"
  fill_in "Description", with: "Worldwide **organisation** description"
  select location, from: "World location"
  click_on "Save"
end
Then /^I should see the worldwide location name "([^"]*)" on the worldwide organisation page$/ do |location_name|
  location = WorldLocation.find_by_name(location_name)
  worldwide_organisation = WorldwideOrganisation.last
  within record_css_selector(worldwide_organisation) do
    assert page.has_content?(location.name)
  end
end
Given /^that the world location "([^"]*)" exists$/ do |country_name|
  create(:country, name: country_name)
end
Then /^I should see (#{THE_DOCUMENT})$/ do |edition|
  assert has_css?(record_css_selector(edition))
end
Given /^I am (?:a|an) (writer|editor|admin|GDS editor|importer)(?: called "([^"]*)")?$/ do |role, name|
  @user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Depteditor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  when 'importer'
    create(:importer)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    Edition::AuditTrail.whodunnit = user
    super(user) # warden
  end
