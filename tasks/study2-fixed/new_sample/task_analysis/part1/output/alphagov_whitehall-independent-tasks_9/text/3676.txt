Feature: Viewing minister pages
As a citizen
I want to be able to view a page gathering information about a minister
So that I can see what government activities they are involved with
Scenario: Viewing all ministers
Given "Johnny Macaroon" is the "Minister of Crazy" for the "Department of Woah"
And "Fred Bloggs" is the "Minister of Sane" for the "Department of Foo"
When I visit the ministers page
Then I should see that "Johnny Macaroon" is a minister in the "Department of Woah"
And I should see that "Fred Bloggs" is a minister in the "Department of Foo"
Scenario: Viewing ministers and whips
Given "Johnny Macaroon" is the "Minister of Crazy" for the "Department of Woah"
And "Fred Bloggs" is a commons whip "Deputy Chief Whip, Comptroller of HM Household" for the "Department of Foo"
When I visit the ministers page
Then I should see that "Johnny Macaroon" is a minister in the "Department of Woah"
And I should see that "Fred Bloggs" is a commons whip "Deputy Chief Whip, Comptroller of HM Household"
