Classes: 7
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Given ]
[name:GDS, file:null, step:Given ]
[name:Locale, file:alphagov_whitehall/lib/locale.rb, step:Given ]
[name:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:When ]
[name:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Then ]
[name:Translation, file:alphagov_whitehall/lib/whitehall/translation.rb, step:When ]
[name:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]

Methods: 57
[name:about, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:When ]
[name:about, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:When ]
[name:announcements_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:Then ]
[name:click_on, type:Object, file:null, step:When ]
[name:code, type:Object, file:null, step:Given ]
[name:create, type:Object, file:null, step:Given ]
[name:edit, type:Admin/organisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:fill_in_organisation_translation_form, type:OrganisationHelper, file:alphagov_whitehall/features/support/organisation_helper.rb, step:When ]
[name:find_by_language_name, type:Locale, file:alphagov_whitehall/lib/locale.rb, step:Given ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:id, type:AttachmentsPresenter, file:alphagov_whitehall/app/presenters/attachments_presenter.rb, step:When ]
[name:index, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:When ]
[name:index, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:When ]
[name:last, type:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:When ]
[name:last, type:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Then ]
[name:login_as, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:Given ]
[name:name, type:Person, file:alphagov_whitehall/app/models/person.rb, step:When ]
[name:name, type:RolePresenter, file:alphagov_whitehall/app/presenters/role_presenter.rb, step:When ]
[name:name, type:DataMigration, file:alphagov_whitehall/lib/whitehall/data_migration.rb, step:When ]
[name:organisation, type:EditionsController, file:alphagov_whitehall/app/controllers/admin/editions_controller.rb, step:When ]
[name:organisation, type:FatalityNoticeRow, file:alphagov_whitehall/lib/whitehall/uploader/fatality_notice_row.rb, step:When ]
[name:organisation, type:Row, file:alphagov_whitehall/lib/whitehall/uploader/row.rb, step:When ]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:Then ]
[name:person, type:Appointment, file:alphagov_whitehall/app/models/edition/appointment.rb, step:When ]
[name:person, type:RoleAppointmentPresenter, file:alphagov_whitehall/app/presenters/role_appointment_presenter.rb, step:When ]
[name:policies_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:When ]
[name:public_document_path, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:When ]
[name:public_document_url, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:When ]
[name:publications_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:When ]
[name:record_css_selector, type:Object, file:null, step:Then ]
[name:rows_hash, type:Object, file:null, step:When ]
[name:rows_hash, type:Object, file:null, step:Then ]
[name:select, type:Object, file:null, step:When ]
[name:show, type:Admin/organisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:When ]
[name:show, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:When ]
[name:show, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:When ]
[name:show, type:TopicsController, file:alphagov_whitehall/app/controllers/admin/topics_controller.rb, step:When ]
[name:show, type:TopicsController, file:alphagov_whitehall/app/controllers/topics_controller.rb, step:When ]
[name:show, type:CorporateInformationPagesController, file:alphagov_whitehall/app/controllers/admin/corporate_information_pages_controller.rb, step:When ]
[name:show, type:CorporateInformationPagesController, file:alphagov_whitehall/app/controllers/corporate_information_pages_controller.rb, step:When ]
[name:slug, type:NewsArticleType, file:alphagov_whitehall/app/models/news_article_type.rb, step:When ]
[name:slug, type:PublicationType, file:alphagov_whitehall/app/models/publication_type.rb, step:When ]
[name:slug, type:SpeechType, file:alphagov_whitehall/app/models/speech_type.rb, step:When ]
[name:slug, type:WorldLocationType, file:alphagov_whitehall/app/models/world_location_type.rb, step:When ]
[name:slug, type:WorldwideOfficeType, file:alphagov_whitehall/app/models/worldwide_office_type.rb, step:When ]
[name:slug, type:WorldwideServiceType, file:alphagov_whitehall/app/models/worldwide_service_type.rb, step:When ]
[name:slug, type:AnnouncementFilterOption, file:alphagov_whitehall/lib/whitehall/announcement_filter_option.rb, step:When ]
[name:slug, type:PublicationFilterOption, file:alphagov_whitehall/lib/whitehall/publication_filter_option.rb, step:When ]
[name:slug, type:WhipOrganisation, file:alphagov_whitehall/lib/whitehall/whip_organisation.rb, step:When ]
[name:stringify_keys, type:Object, file:null, step:When ]
[name:text, type:Object, file:null, step:When ]
[name:title, type:DetailedGuide, file:alphagov_whitehall/app/models/detailed_guide.rb, step:When ]
[name:within, type:Object, file:null, step:Then ]

Referenced pages: 29
alphagov_whitehall/app/views/admin/organisations/_form.html.erb
alphagov_whitehall/app/views/admin/organisations/_sidebar.html.erb
alphagov_whitehall/app/views/admin/organisations/edit.html.erb
alphagov_whitehall/app/views/admin/organisations/show.html.erb
alphagov_whitehall/app/views/admin/shared/_govdelivery_fields.html.erb
alphagov_whitehall/app/views/admin/shared/_mainstream_link_fields.html.erb
alphagov_whitehall/app/views/announcements/_list_description.html.erb
alphagov_whitehall/app/views/classifications/_list_description.html.erb
alphagov_whitehall/app/views/contacts/_contact.html.erb
alphagov_whitehall/app/views/corporate_information_pages/show.html.erb
alphagov_whitehall/app/views/corporate_information_pages/show_worldwide_organisation.html.erb
alphagov_whitehall/app/views/organisations/_corporate_information.html.erb
alphagov_whitehall/app/views/organisations/_header.html.erb
alphagov_whitehall/app/views/organisations/_organisations_logo_list.html.erb
alphagov_whitehall/app/views/organisations/_works_with.html.erb
alphagov_whitehall/app/views/organisations/about.html.erb
alphagov_whitehall/app/views/organisations/index.html.erb
alphagov_whitehall/app/views/organisations/show.html.erb
alphagov_whitehall/app/views/people/_person.html.erb
alphagov_whitehall/app/views/policies/_list_description.html.erb
alphagov_whitehall/app/views/publications/_list_description.html.erb
alphagov_whitehall/app/views/shared/_available_languages.html.erb
alphagov_whitehall/app/views/shared/_featured_news.html.erb
alphagov_whitehall/app/views/shared/_feeds.html.erb
alphagov_whitehall/app/views/shared/_recently_updated.html.erb
alphagov_whitehall/app/views/shared/_recently_updated_documents.html.erb
alphagov_whitehall/app/views/shared/_social_media_accounts.html.erb
alphagov_whitehall/app/views/topics/show.html.erb
alphagov_whitehall/app/views/worldwide_organisations/_header.html.erb

