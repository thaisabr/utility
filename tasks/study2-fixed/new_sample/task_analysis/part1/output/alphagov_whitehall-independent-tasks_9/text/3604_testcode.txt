Given /^the organisation "([^"]*)" exists$/ do |name|
  create(:ministerial_department, name: name)
end
Given /^a (topic|document series|mainstream category) with the slug "([^"]*)" exists$/ do |type, slug|
  o = create(type.parameterize.underscore)
  o.update_attributes!(slug: slug)
end
When /^I import the following data as CSV as "([^"]*)" for "([^"]*)":$/ do |document_type, organisation_name, data|
  organisation = Organisation.find_by_name(organisation_name) || create(:organisation, name: organisation_name)
  import_data_as_document_type_for_organisation(data, document_type, organisation)
end
Then /^the import should fail with errors about an unrecognised policy$/ do
  assert page.has_content?("Import failed")
  assert page.has_content?("Unable to find Policy with slug 'non-existent-policy'")

  assert_equal 0, Edition.count
end
Then /^the import succeeds creating (\d+) detailed guidance document$/ do |n|
  assert_equal [], Import.last.import_errors
  assert_equal :succeeded, Import.last.status
  assert_equal 1, Import.last.documents.where(document_type: DetailedGuide.name).to_a.size
end
Then /^the imported detailed guidance document has the following associations:$/ do |expected_table|
  detailed_guide_document = Import.last.documents.where(document_type: DetailedGuide.name).first
  edition = detailed_guide_document.editions.first
  expected_table.hashes.each do |row|
    assert_equal edition.send(row["Name"].to_sym).map(&:slug), row["Slugs"].split(/, +/)
  end
end
  def import_data_as_document_type_for_organisation(data, document_type, organisation)
    make_sure_data_importer_user_exists
    Import.use_separate_connection

    with_import_csv_file(data) do |path|
      visit new_admin_import_path
      select document_type, from: 'Type'
      attach_file 'CSV File', path
      select organisation.name, from: 'Default organisation'
      click_button 'Save'
      click_button 'Run'

      run_last_import

      visit current_path
    end
    Import.find(current_path.match(/admin\/imports\/(\d+)\Z/)[1])
  end
  def run_last_import
    Import.last.perform
  end
  def with_import_csv_file(data)
    tf = Tempfile.new('csv_import')
    tf << data
    tf.close
    yield tf.path
    tf.unlink
  end
  def make_sure_data_importer_user_exists
    unless User.find_by_name('Automatic Data Importer')
      create(:importer, name: 'Automatic Data Importer')
    end
  end
Given /^I am (?:a|an) (writer|editor|admin|GDS editor|importer)(?: called "([^"]*)")?$/ do |role, name|
  @user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Editor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  when 'importer'
    create(:importer)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    Edition::AuditTrail.whodunnit = user
    super(user) # warden
  end
Given /^a published detailed guide "([^"]*)" for the organisation "([^"]*)"$/ do |title, organisation|
  organisation = create(:organisation, name: organisation)
  create(:published_detailed_guide, title: title, organisations: [organisation])
end
Given /^a document series "([^"]*)" exists$/ do |name|
  create(:document_series, name: name)
end
Given /^a topic called "([^"]*)" exists$/ do |name|
  create_topic(name: name)
end
def create_topic(options = {})
  visit admin_root_path
  click_link "Topics"
  click_link "Create topic"
  fill_in "Name", with: options[:name] || "topic-name"
  fill_in "Description", with: options[:description] || "topic-description"
  (options[:related_classifications] || []).each do |related_name|
    select related_name, from: "Related topics"
  end
  click_button "Save"
end
