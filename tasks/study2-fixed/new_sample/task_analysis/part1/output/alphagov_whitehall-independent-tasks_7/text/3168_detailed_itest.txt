Classes: 1
[name:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Then ]

Methods: 56
[name:about, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:null]
[name:about, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:null]
[name:all_announcements_link, type:Object, file:null, step:null]
[name:announcements_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:null]
[name:create, type:Object, file:null, step:Given ]
[name:css_classes, type:PromotionalFeature, file:alphagov_whitehall/app/models/promotional_feature.rb, step:null]
[name:find_by_name!, type:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Then ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:id, type:AttachmentsPresenter, file:alphagov_whitehall/app/presenters/attachments_presenter.rb, step:null]
[name:id, type:FeaturePresenter, file:alphagov_whitehall/app/presenters/feature_presenter.rb, step:null]
[name:index, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:null]
[name:index, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:null]
[name:items, type:HomePageList, file:alphagov_whitehall/app/models/home_page_list.rb, step:null]
[name:items, type:PromotionalFeature, file:alphagov_whitehall/app/models/promotional_feature.rb, step:null]
[name:mainstream_category_path, type:MainstreamCategoryRoutesHelper, file:alphagov_whitehall/app/helpers/mainstream_category_routes_helper.rb, step:null]
[name:name, type:Person, file:alphagov_whitehall/app/models/person.rb, step:null]
[name:name, type:RolePresenter, file:alphagov_whitehall/app/presenters/role_presenter.rb, step:null]
[name:name, type:DataMigration, file:alphagov_whitehall/lib/whitehall/data_migration.rb, step:null]
[name:organisation, type:EditionFilter, file:alphagov_whitehall/app/models/admin/edition_filter.rb, step:null]
[name:organisation, type:FatalityNoticeRow, file:alphagov_whitehall/lib/whitehall/uploader/fatality_notice_row.rb, step:null]
[name:organisation, type:Row, file:alphagov_whitehall/lib/whitehall/uploader/row.rb, step:null]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:Then ]
[name:person, type:Appointment, file:alphagov_whitehall/app/models/edition/appointment.rb, step:null]
[name:person, type:RoleAppointmentPresenter, file:alphagov_whitehall/app/presenters/role_appointment_presenter.rb, step:null]
[name:policies_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:null]
[name:promotional_feature_items, type:PromotionalFeature, file:alphagov_whitehall/app/models/promotional_feature.rb, step:null]
[name:public_document_path, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:null]
[name:public_document_url, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:null]
[name:publications_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:null]
[name:refute, type:Object, file:null, step:Then ]
[name:show, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:null]
[name:show, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:null]
[name:show, type:CorporateInformationPagesController, file:alphagov_whitehall/app/controllers/admin/corporate_information_pages_controller.rb, step:null]
[name:show, type:CorporateInformationPagesController, file:alphagov_whitehall/app/controllers/corporate_information_pages_controller.rb, step:null]
[name:show, type:TopicsController, file:alphagov_whitehall/app/controllers/admin/topics_controller.rb, step:null]
[name:show, type:TopicsController, file:alphagov_whitehall/app/controllers/topics_controller.rb, step:null]
[name:slug, type:EmailSignup, file:alphagov_whitehall/app/models/email_signup.rb, step:null]
[name:slug, type:NewsArticleType, file:alphagov_whitehall/app/models/news_article_type.rb, step:null]
[name:slug, type:PublicationType, file:alphagov_whitehall/app/models/publication_type.rb, step:null]
[name:slug, type:SpeechType, file:alphagov_whitehall/app/models/speech_type.rb, step:null]
[name:slug, type:WorldLocationType, file:alphagov_whitehall/app/models/world_location_type.rb, step:null]
[name:slug, type:WorldwideOfficeType, file:alphagov_whitehall/app/models/worldwide_office_type.rb, step:null]
[name:slug, type:WorldwideServiceType, file:alphagov_whitehall/app/models/worldwide_service_type.rb, step:null]
[name:slug, type:AnnouncementFilterOption, file:alphagov_whitehall/lib/whitehall/announcement_filter_option.rb, step:null]
[name:slug, type:PublicationFilterOption, file:alphagov_whitehall/lib/whitehall/publication_filter_option.rb, step:null]
[name:slug, type:WhipOrganisation, file:alphagov_whitehall/lib/whitehall/whip_organisation.rb, step:null]
[name:text, type:Object, file:null, step:null]
[name:title, type:PromotionalFeature, file:alphagov_whitehall/app/models/promotional_feature.rb, step:null]
[name:title, type:CorporateInformationPage, file:alphagov_whitehall/app/models/corporate_information_page.rb, step:null]
[name:title, type:TitleExtractor, file:alphagov_whitehall/app/models/email_signup/title_extractor.rb, step:null]
[name:title, type:AttachmentsPresenter, file:alphagov_whitehall/app/presenters/attachments_presenter.rb, step:null]
[name:title, type:FeaturePresenter, file:alphagov_whitehall/app/presenters/feature_presenter.rb, step:null]
[name:title, type:PromotionalFeatureItemPresenter, file:alphagov_whitehall/app/presenters/promotional_feature_item_presenter.rb, step:null]
[name:title, type:Row, file:alphagov_whitehall/lib/whitehall/uploader/row.rb, step:null]
[name:title, type:StatisticalDataSetRow, file:alphagov_whitehall/lib/whitehall/uploader/statistical_data_set_row.rb, step:null]
[name:title, type:DetailedGuide, file:alphagov_whitehall/app/models/detailed_guide.rb, step:null]

Referenced pages: 28
alphagov_whitehall/app/views/announcements/_list_description.html.erb
alphagov_whitehall/app/views/classifications/_list_description.html.erb
alphagov_whitehall/app/views/contacts/_contact.html.erb
alphagov_whitehall/app/views/corporate_information_pages/show.html.erb
alphagov_whitehall/app/views/corporate_information_pages/show_worldwide_organisation.html.erb
alphagov_whitehall/app/views/mainstream_categories/_list_description.html.erb
alphagov_whitehall/app/views/organisations/_corporate_information.html.erb
alphagov_whitehall/app/views/organisations/_featured_items.html.erb
alphagov_whitehall/app/views/organisations/_header.html.erb
alphagov_whitehall/app/views/organisations/_organisations_logo_list.html.erb
alphagov_whitehall/app/views/organisations/_promotional_feature_item.html.erb
alphagov_whitehall/app/views/organisations/_works_with.html.erb
alphagov_whitehall/app/views/organisations/about.html.erb
alphagov_whitehall/app/views/organisations/index.html.erb
alphagov_whitehall/app/views/organisations/show-executive-office.html.erb
alphagov_whitehall/app/views/organisations/show.html.erb
alphagov_whitehall/app/views/people/_person.html.erb
alphagov_whitehall/app/views/policies/_list_description.html.erb
alphagov_whitehall/app/views/publications/_list_description.html.erb
alphagov_whitehall/app/views/shared/_available_languages.html.erb
alphagov_whitehall/app/views/shared/_featured.html.erb
alphagov_whitehall/app/views/shared/_feeds.html.erb
alphagov_whitehall/app/views/shared/_heading.html.erb
alphagov_whitehall/app/views/shared/_recently_updated.html.erb
alphagov_whitehall/app/views/shared/_recently_updated_documents.html.erb
alphagov_whitehall/app/views/shared/_social_media_accounts.html.erb
alphagov_whitehall/app/views/topics/show.html.erb
alphagov_whitehall/app/views/worldwide_organisations/_header.html.erb

