Classes: 14
[name:Attachment, file:alphagov_whitehall/app/models/attachment.rb, step:Given ]
[name:Attachment, file:alphagov_whitehall/app/models/attachment.rb, step:Then ]
[name:Consultation, file:alphagov_whitehall/app/models/consultation.rb, step:When ]
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Given ]
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:When ]
[name:File, file:null, step:When ]
[name:FileUtils, file:null, step:Given ]
[name:GDS, file:null, step:Given ]
[name:GDS, file:null, step:When ]
[name:Publication, file:alphagov_whitehall/app/models/publication.rb, step:When ]
[name:Rails, file:null, step:When ]
[name:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]
[name:Whitehall, file:alphagov_whitehall/features/support/whitehall.rb, step:Given ]
[name:Whitehall, file:alphagov_whitehall/lib/whitehall.rb, step:Given ]

Methods: 59
[name:assert_equal, type:Object, file:null, step:Then ]
[name:assert_final_path, type:PathHelper, file:alphagov_whitehall/features/support/path_helper.rb, step:When ]
[name:assert_not_equal, type:Object, file:null, step:Then ]
[name:attach_file, type:Object, file:null, step:When ]
[name:attachment_data, type:AttachmentsController, file:alphagov_whitehall/app/controllers/attachments_controller.rb, step:When ]
[name:attachment_data, type:AttachmentsController, file:alphagov_whitehall/app/controllers/attachments_controller.rb, step:Then ]
[name:attachments, type:AttachmentsPresenter, file:alphagov_whitehall/app/presenters/attachments_presenter.rb, step:Given ]
[name:attachments, type:ConsultationRow, file:alphagov_whitehall/lib/whitehall/uploader/consultation_row.rb, step:Given ]
[name:attachments, type:DetailedGuideRow, file:alphagov_whitehall/lib/whitehall/uploader/detailed_guide_row.rb, step:Given ]
[name:attachments, type:NewsArticleRow, file:alphagov_whitehall/lib/whitehall/uploader/news_article_row.rb, step:Given ]
[name:attachments, type:PublicationRow, file:alphagov_whitehall/lib/whitehall/uploader/publication_row.rb, step:Given ]
[name:attachments, type:StatisticalDataSetRow, file:alphagov_whitehall/lib/whitehall/uploader/statistical_data_set_row.rb, step:Given ]
[name:attachments, type:AttachmentsPresenter, file:alphagov_whitehall/app/presenters/attachments_presenter.rb, step:When ]
[name:attachments, type:ConsultationRow, file:alphagov_whitehall/lib/whitehall/uploader/consultation_row.rb, step:When ]
[name:attachments, type:DetailedGuideRow, file:alphagov_whitehall/lib/whitehall/uploader/detailed_guide_row.rb, step:When ]
[name:attachments, type:NewsArticleRow, file:alphagov_whitehall/lib/whitehall/uploader/news_article_row.rb, step:When ]
[name:attachments, type:PublicationRow, file:alphagov_whitehall/lib/whitehall/uploader/publication_row.rb, step:When ]
[name:attachments, type:StatisticalDataSetRow, file:alphagov_whitehall/lib/whitehall/uploader/statistical_data_set_row.rb, step:When ]
[name:attachments, type:Object, file:null, step:When ]
[name:attachments, type:Object, file:null, step:Then ]
[name:clean_uploads_root, type:Whitehall, file:alphagov_whitehall/features/support/whitehall.rb, step:Given ]
[name:clean_uploads_root, type:Whitehall, file:alphagov_whitehall/lib/whitehall.rb, step:Given ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:click_on, type:Object, file:null, step:When ]
[name:create, type:Object, file:null, step:Given ]
[name:create, type:SpeechType, file:alphagov_whitehall/app/models/speech_type.rb, step:Given ]
[name:edit, type:Admin/publicationsController, file:alphagov_whitehall/app/controllers/admin/publications_controller.rb, step:When ]
[name:edit, type:Admin/consultationsController, file:alphagov_whitehall/app/controllers/admin/consultations_controller.rb, step:When ]
[name:ensure_path, type:PathHelper, file:alphagov_whitehall/features/support/path_helper.rb, step:When ]
[name:filename, type:Object, file:null, step:Then ]
[name:fill_in, type:Object, file:null, step:When ]
[name:fill_in_change_note_if_required, type:DocumentHelper, file:alphagov_whitehall/features/support/document_helper.rb, step:When ]
[name:first, type:AttachmentsPresenter, file:alphagov_whitehall/app/presenters/attachments_presenter.rb, step:Given ]
[name:first, type:AttachmentsPresenter, file:alphagov_whitehall/app/presenters/attachments_presenter.rb, step:When ]
[name:first, type:AttachmentsPresenter, file:alphagov_whitehall/app/presenters/attachments_presenter.rb, step:Then ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:has_css?, type:Object, file:null, step:When ]
[name:has_no_css?, type:Object, file:null, step:Then ]
[name:incoming_uploads_root, type:Whitehall, file:alphagov_whitehall/features/support/whitehall.rb, step:Given ]
[name:incoming_uploads_root, type:Whitehall, file:alphagov_whitehall/lib/whitehall.rb, step:Given ]
[name:join, type:Object, file:null, step:When ]
[name:last, type:Publication, file:alphagov_whitehall/app/models/publication.rb, step:When ]
[name:last, type:Consultation, file:alphagov_whitehall/app/models/consultation.rb, step:When ]
[name:log_out, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:When ]
[name:login_as, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:Given ]
[name:logout, type:Object, file:null, step:When ]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:Then ]
[name:page, type:Paginator, file:alphagov_whitehall/app/presenters/api/paginator.rb, step:When ]
[name:pdf_attachment, type:DocumentHelper, file:alphagov_whitehall/features/support/document_helper.rb, step:When ]
[name:publish, type:DocumentHelper, file:alphagov_whitehall/features/support/document_helper.rb, step:When ]
[name:record_css_selector, type:Object, file:null, step:When ]
[name:refute, type:Object, file:null, step:When ]
[name:refute_flash_alerts_exist, type:DocumentHelper, file:alphagov_whitehall/features/support/document_helper.rb, step:When ]
[name:show, type:Admin/publicationsController, file:alphagov_whitehall/app/controllers/admin/publications_controller.rb, step:When ]
[name:title, type:Attachment, file:alphagov_whitehall/app/models/attachment.rb, step:Then ]
[name:upload_new_attachment, type:AttachmentHelper, file:alphagov_whitehall/features/support/attachment_helper.rb, step:When ]
[name:url, type:Object, file:null, step:Then ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 12
alphagov_whitehall/app/views/admin/consultations/_additional_significant_fields.html.erb
alphagov_whitehall/app/views/admin/consultations/_date_fields.html.erb
alphagov_whitehall/app/views/admin/consultations/_edition.html.erb
alphagov_whitehall/app/views/admin/consultations/_form.html.erb
alphagov_whitehall/app/views/admin/consultations/_required_fields_for_imported_editions.html.erb
alphagov_whitehall/app/views/admin/publications/_additional_significant_fields.html.erb
alphagov_whitehall/app/views/admin/publications/_edition.html.erb
alphagov_whitehall/app/views/admin/publications/_form.html.erb
alphagov_whitehall/app/views/admin/publications/_publication_date_fields.html.erb
alphagov_whitehall/app/views/admin/publications/_required_fields_for_imported_editions.html.erb
alphagov_whitehall/app/views/documents/_attachment.html.erb
alphagov_whitehall/app/views/documents/_attachments.html.erb

