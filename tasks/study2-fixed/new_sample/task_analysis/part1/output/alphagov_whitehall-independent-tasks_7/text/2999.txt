Feature: Speed tagging editions
I want to be able to tag new editions (especially but not exclusively imported editions) using a slimmed down version of the edit screen.
Specifically, the page:
- should only present policies which are associated with the org of the doc being imported
- and only present them once, even if they are associated with multiple orgs associated with the doc
- should only present ministers which are associated with the org of the doc being imported
- should present mandatory data elements for that document type. (i.e. speech type, publication subtype)
- should include first published at fields for news articles and statistical data sets
- should include opening and closing dates for consulations
- should include delivered on date for speeches
- should include publication date for publications
Background:
Given I am a writer
Scenario: Speed tagging a newly imported publication
When I go to speed tag a newly imported publication "Beard length statistics 2012"
Then I should have to select the publication sub-type
Then I should be able to set the first published date
