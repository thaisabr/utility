When /^I click the (.*) button$/ do |button_name|
      case button_name
      when "I can't decide"
	find("#cant_decide_btn").click
      when "I can't decide submit"
        page.evaluate_script('window.alert = function() { return true; }') # prevent javascript alerts from popping up
	find(".cd_submit_button").click
      when "add new idea"
	find(".add_idea_button").click
      when "flag submit"
        page.evaluate_script('window.alert = function() { return true; }')
	find("#facebox .flag_submit_button").click
      when "idea auto activation toggle"
	find(".toggle_autoactivate_status").click
      end
end
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
Then /^(?:|I )should see "([^\"]*)"(?: within "([^\"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if defined?(Spec::Rails::Matchers)
      page.should have_content(text)
    else
      assert page.has_content?(text)
    end
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)
    case page_name

    when /the homepage/i
      root_path
    when /the sign up page/i
      new_user_path
    when /the sign in page/i
      new_session_path
    when /the password reset request page/i
      new_password_path

    when /the question show page/i
      question_path
    when /the question create page/i
      new_question_path
    when /the questions index page/i
      questions_path
    
    when /the Cast Votes page for '([^'].*)'/i
	"/"+ $1
    when /the Just Created Cast Votes page for '([^'].*)'/i
	"/"+ $1 + "?just_created=true"
    when /the View Results page for '([^'].*)'/i
	"/"+ $1 + "/results"
    when /the Admin page for '([^'].*)'/i
	"/"+ $1 + "/admin"
    when /the Control Panel page/i
	 admin_path

    when /the Deactivate page for the saved (.*) choice/
	 @earl = Earl.find_by_question_id(@question_id)
	 choice = ($1 == "left") ? @left_choice : @right_choice
	 deactivate_question_choice_path(:question_id => @earl, :id => choice.id)
    
    when /the Activate page for the saved (.*) choice/
	 @earl = Earl.find_by_question_id(@question_id)
	 choice = ($1 == "left") ? @left_choice : @right_choice
	 activate_question_choice_path(:question_id => @earl, :id => choice.id)

    
    when /the Idea Detail page for the saved left choice/i
	 @earl = Earl.find_by_question_id(@question_id)
	 question_choice_path(:question_id => @earl, :id => @left_choice.id)
    when /the Idea Detail page for the saved right choice/i
	 @earl = Earl.find_by_question_id(@question_id)
	 question_choice_path(:question_id => @earl, :id => @right_choice.id)
    # Add more page name => path mappings here

    else
      raise "Can't find mapping from \"#{page_name}\" to a path."
    end
  end
Given /^an idea marketplace quickly exists with url '(.*)' and (.*) ideas$/ do |url, num_ideas|
	ideas = ""
	(1..num_ideas.to_i).to_a.reverse.each do |n|
	  ideas += "Idea ##{n}" + "\n"
	end
	q = Question.create(Factory.attributes_for(:question, :ideas => ideas))
	e = Factory.create(:earl, :name => url, :question_id => q.id)
end
When /^I sign in as "(.*)\/(.*)"$/ do |email, password|
  When %{I go to the sign in page}
  And %{I fill in "Email" with "#{email}"}
  And %{I fill in "Password" with "#{password}"}
  And %{I press "Log In"}
end
