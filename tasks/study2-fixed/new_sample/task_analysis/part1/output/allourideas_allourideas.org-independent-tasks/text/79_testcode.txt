Given /^an idea marketplace quickly exists with url '([^\']*)'$/ do |url|
	q = Question.create(Factory.attributes_for(:question))
	e = Factory.create(:earl, :name => url, :question_id => q.id)
end
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )follow "([^\"]*)"(?: within "([^\"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
Then /^(?:|I )should see "([^\"]*)"(?: within "([^\"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if defined?(Spec::Rails::Matchers)
      page.should have_content(text)
    else
      assert page.has_content?(text)
    end
  end
end
Then /^(?:|I )should not see "([^\"]*)"(?: within "([^\"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if defined?(Spec::Rails::Matchers)
      page.should have_no_content(text)
    else
      assert page.has_no_content?(text)
    end
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)
    path = case page_name

    when /the homepage/i
      root_path
    when /the sign up page/i
      new_user_path
    when /the sign in page/i
      new_session_path
    when /the password reset request page/i
      new_password_path

    when /the question show page/i
      question_path
    when /the question create page/i
      new_question_path
    when /the questions index page/i
      questions_path
    
    when /the Cast Votes page for '([^'].*)'/i
	"/"+ $1
    when /the Crossfade Cast Votes page for '([^'].*)'/i
	"/"+ $1 + "?crossfade=true"
    when /the Just Created Cast Votes page for '([^'].*)'/i
	"/"+ $1 + "?just_created=true"
    when /the View Results page for '([^'].*)'/i
	"/"+ $1 + "/results"
    when /the Admin page for '([^'].*)'/i
	"/"+ $1 + "/admin"
    when /the Owner Add Photos page for '([^'].*)'/i
	"/"+ $1 + "/addphotos"
    when /the Control Panel All page/i
	 admin_path + "?all=true"
    when /the Control Panel page/i
	 admin_path
    when /the WIDGET Cast Votes page for '([^'].*)'/i
	"/"+ $1 + '?widget&width=450&height=410'

    when /the Deactivate page for the saved (.*) choice/
	 @earl = Earl.find_by_question_id(@question_id)
	 choice = ($1 == "left") ? @left_choice : @right_choice
	 deactivate_question_choice_path(:question_id => @earl, :id => choice.id)
    
    when /the Activate page for the saved (.*) choice/
	 @earl = Earl.find_by_question_id(@question_id)
	 choice = ($1 == "left") ? @left_choice : @right_choice
	 activate_question_choice_path(:question_id => @earl, :id => choice.id)

    
    when /the Idea Detail page for the saved left choice(.*)?/i
	 @earl = Earl.find_by_question_id(@question_id)
	 url_opts = {:question_id => @earl, :id => @left_choice.id}
	 if($1 =~ /with login reminder/i)
		 url_opts.merge!(:login_reminder => true)
	 end
	 question_choice_path(url_opts)
    when /the Idea Detail page for the saved right choice/i
	 @earl = Earl.find_by_question_id(@question_id)
	 question_choice_path(:question_id => @earl, :id => @right_choice.id)
    # Add more page name => path mappings here

    else
      raise "Can't find mapping from \"#{page_name}\" to a path."
    end
    if @photocracy_mode
	path += (path.include?("?")) ? "&" : "?"
        path += "photocracy_mode=true"
    end 
    path
  end
When /^I click on the left choice$/ do
	When "I follow \"leftside\""
	Capybara.default_wait_time = 10
	Then "I should see \"You chose\" within \".tellmearea\""
end
When /^I click the (.*) button$/ do |button_name|
      case button_name
      when "I can't decide"
        find("#cant_decide_btn").click
      when "I can't decide submit"
        page.evaluate_script('window.alert = function() { return true; }') # prevent javascript alerts from popping up
      	find(".cd_submit_button").click
      when "add new idea"
      	find(".add_idea_button").click
      when "flag submit"
        page.evaluate_script('window.alert = function() { return true; }')
      	find("#facebox .flag_submit_button").click
      when "WIDGET flag submit"
        page.evaluate_script('window.alert = function() { return true; }')
      	find(".flag_submit_button").click
      when "photocracy flag submit"
        page.evaluate_script('window.alert = function() { return true; }')
	find(".flag_submit_button").click
      when "add new photo"
      	find("#add_photo_button_for_dialog").click
      when "idea auto activation toggle"
      	find(".toggle_autoactivate_status").click
      end
end
When /^I pick "(.*)"$/ do |radio_label|
	case radio_label 
	when "I like both ideas"
    find('.cd_box a.like_both').click
	when "Other"
           When "I choose \"cant_decide_reason_user_other\""
	end
end
Then /^the vote count should be (.*)$/ do |num_votes|
	Then "I should see \"#{num_votes}\" within \"#votes_count\""
end
Given /^I save the current appearance lookup?$/ do 
	#The above doesn't work with selenium, for some unknown reason. Fall back to using jquery: 
        begin
	  @appearance_lookup = page.locate('#appearance_lookup')[:value].to_s
	rescue Capybara::ElementNotFound
          @appearance_lookup = page.evaluate_script("$('#appearance_lookup').val()").to_s
	end
	if @appearance_lookup.blank?
	  raise Capybara::ElementNotFound
	end
end
Then /^the current appearance lookup should (not )?match the saved appearance lookup$/ do |negation|
        begin
	  @new_appearance_lookup = page.locate('#appearance_lookup')[:value].to_s
	rescue Capybara::ElementNotFound
          @new_appearance_lookup = page.evaluate_script("$('#appearance_lookup').val()").to_s
	end
	if @new_appearance_lookup.blank?
	  raise Capybara::ElementNotFound
	end

        if negation.blank?
	   @new_appearance_lookup.should == @appearance_lookup
        else
	   @new_appearance_lookup.should_not == @appearance_lookup
        end
end
When /^I upload a photo$/ do
   photo = "#{Rails.root}/db/seed-images/princeton/#{rand(31)+1}.jpg"
   When "I click the add new photo button"
   #And "I press \"choose_file\""
   And "I attach the file \"#{photo}\" to \"new_idea\" within \"#add_photo\""
   sleep(5) # I'd like to change this to some sort of visual feedback, not sure what that would be
end
