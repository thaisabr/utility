Given /^idea marketplace '(.*)' has enabled idea autoactivation$/ do |url|
 @earl = Earl.find(url)
 @question = Question.find(@earl.question_id)
 @question.it_should_autoactivate_ideas = true
 @question.save
end
Given /^I sign in as the admin for '(.*)'$/ do |url|
  earl = Earl.find(url)
  Given "I sign in as \"#{earl.user.email}/password\""
end
When /^I upload a photo$/ do
   photo = "#{Rails.root}/db/seed-images/princeton/#{rand(31)+1}.jpg"
   When "I click the add new photo button"
   And "I press \"choose_file\""
   And "I attach the file \"#{photo}\" to \"new_idea\""
   sleep(5) # I'd like to change this to some sort of visual feedback, not sure what that would be
end
Given /^(?:a clear email queue|no emails have been sent)$/ do
  reset_mailer
end
Then /^(?:I|they|"([^"]*?)") should receive (an|no|\d+) emails?$/ do |address, amount|
  unread_emails_for(address).size.should == parse_email_count(amount)
end
When /^(?:I|they|"([^"]*?)") opens? the email$/ do |address|
  open_email(address)
end
Then /^(?:I|they) should see "([^"]*?)" in the email subject$/ do |text|
  current_email.should have_subject(text)
end
Then /^(?:I|they) should see "([^"]*?)" in the email body$/ do |text|
  current_email.body.should include(text)
end
When /^(?:I|they) click the first link in the email$/ do
  click_first_link_in_email
end
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^\"]*)"(?: within "([^\"]*)")?$/ do |button, selector|
  with_scope(selector) do
    click_button(button)
  end
end
When /^(?:|I )fill in "([^\"]*)" with "([^\"]*)"(?: within "([^\"]*)")?$/ do |field, value, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
When /^(?:|I )check "([^\"]*)"(?: within "([^\"]*)")?$/ do |field, selector|
  with_scope(selector) do
    check(field)
  end
end
Then /^(?:|I )should see "([^\"]*)"(?: within "([^\"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if defined?(Spec::Rails::Matchers)
      page.should have_content(text)
    else
      assert page.has_content?(text)
    end
  end
end
Then /^(?:|I )should not see "([^\"]*)"(?: within "([^\"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if defined?(Spec::Rails::Matchers)
      page.should have_no_content(text)
    else
      assert page.has_no_content?(text)
    end
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)
    path = case page_name

    when /the homepage/i
      root_path
    when /the sign up page/i
      new_user_path
    when /the sign in page/i
      new_session_path
    when /the password reset request page/i
      new_password_path

    when /the question show page/i
      question_path
    when /the question create page/i
      new_question_path
    when /the questions index page/i
      questions_path
    
    when /the Cast Votes page for '([^'].*)'/i
	"/"+ $1
    when /the Just Created Cast Votes page for '([^'].*)'/i
	"/"+ $1 + "?just_created=true"
    when /the View Results page for '([^'].*)'/i
	"/"+ $1 + "/results"
    when /the Admin page for '([^'].*)'/i
	"/"+ $1 + "/admin"
    when /the Control Panel page/i
	 admin_path
    when /the WIDGET Cast Votes page for '([^'].*)'/i
	"/"+ $1 + '?widget&width=450&height=410'
	#"widget.allourideas.org/"+ $1 + '?width=450&height=410'
	 #cattr_accessor :widget_view_path

    when /the Deactivate page for the saved (.*) choice/
	 @earl = Earl.find_by_question_id(@question_id)
	 choice = ($1 == "left") ? @left_choice : @right_choice
	 deactivate_question_choice_path(:question_id => @earl, :id => choice.id)
    
    when /the Activate page for the saved (.*) choice/
	 @earl = Earl.find_by_question_id(@question_id)
	 choice = ($1 == "left") ? @left_choice : @right_choice
	 activate_question_choice_path(:question_id => @earl, :id => choice.id)

    
    when /the Idea Detail page for the saved left choice(.*)?/i
	 @earl = Earl.find_by_question_id(@question_id)
	 url_opts = {:question_id => @earl, :id => @left_choice.id}
	 if($1 =~ /with login reminder/i)
		 url_opts.merge!(:login_reminder => true)
	 end
	 question_choice_path(url_opts)
    when /the Idea Detail page for the saved right choice/i
	 @earl = Earl.find_by_question_id(@question_id)
	 question_choice_path(:question_id => @earl, :id => @right_choice.id)
    # Add more page name => path mappings here

    else
      raise "Can't find mapping from \"#{page_name}\" to a path."
    end
    if @photocracy_mode
	path += (path.include?("?")) ? "&" : "?"
        path += "photocracy_mode=true"
    end 
    path
  end
Given /^an idea marketplace quickly exists with url '([^\']*)' and admin '(.*)\/(.*)'$/ do |url, email, password|
	u = Factory.create(:email_confirmed_user, :email => email, :password => password, :password_confirmation => password)
	Given "an idea marketplace quickly exists with url '#{url}'"
	e = Earl.last
	e.user = u
	e.save
end
Given /^a photocracy idea marketplace quickly exists with url '([^\']*)' and admin '(.*)\/(.*)'$/ do |url, email, password|
	u = Factory.create(:email_confirmed_user, :email => email, :password => password, :password_confirmation => password)

	Given "a photocracy idea marketplace quickly exists with url '#{url}'"
	e = Earl.last
	e.user = u
	e.save
end
When /^I sign in as "(.*)\/(.*)"$/ do |email, password|
  When %{I go to the sign in page}
  And %{I fill in "Email" with "#{email}"}
  And %{I fill in "Password" with "#{password}"}
  And %{I press "Log In"}
end
When /^I upload an idea titled '(.*)'$/ do |ideatext|
	When "I click the add new idea button"
	And "I fill in \"new_idea_field\" with \"#{ideatext}\""
	find("#submit_btn").click
end
Then /^the vote count should be (.*)$/ do |num_votes|
	Then "I should see \"#{num_votes}\" within \"#votes_count\""
end
Then /^the idea count should be (.*)$/ do |num_ideas|
	Then "I should see \"#{num_ideas}\" within \"#item_count\""
end
Given /^I save the current (.*) choices?$/ do |side|
	if @photocracy_mode
	   Capybara.ignore_hidden_elements = false
	   @question_id = page.locate('#choose_file')[:question_id].to_i
	   Capybara.ignore_hidden_elements = true 
	else
	   @question_id = page.locate('#leftside')[:rel].to_i
	end


	
        begin
	  @prompt_id = page.locate('#prompt_id')[:value].to_s
	  #The above doesn't work with selenium, for some unknown reason. Fall back to using jquery: 
	rescue Capybara::ElementNotFound
          @prompt_id = page.evaluate_script("$('#prompt_id').val()").to_i
	end
	if @prompt_id.blank?
	  raise Capybara::ElementNotFound
	end

        @prompt = Prompt.find(@prompt_id, :params => {:question_id => @question_id})

	if side == "left" or side == "two"
          @left_choice = Choice.find(@prompt.left_choice_id, :params => {:question_id => @question_id})
	end
	if side == "right" or side == "two"
          @right_choice = Choice.find(@prompt.right_choice_id, :params => {:question_id => @question_id})
	end
end
When /^I close the facebox$/ do
        page.evaluate_script("$.facebox.close();")
end
