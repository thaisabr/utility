When /^I click on the left photo$/ do
	When "I follow \"leftside\""
end
When /^I click the flag link for the (.*) choice$/ do |side|
	if side == "left"
		When "I follow \"left_flag\""
        else
		When "I follow \"right_flag\""
	end

end
When /^I click the (.*) button$/ do |button_name|
      case button_name
      when "I can't decide"
        find("#cant_decide_btn").click
      when "I can't decide submit"
        page.evaluate_script('window.alert = function() { return true; }') # prevent javascript alerts from popping up
      	find(".cd_submit_button").click
      when "add new idea"
      	find(".add_idea_button").click
      when "flag submit"
        page.evaluate_script('window.alert = function() { return true; }')
      	find("#facebox .flag_submit_button").click
      when "WIDGET flag submit"
        page.evaluate_script('window.alert = function() { return true; }')
      	find(".flag_submit_button").click
      when "photocracy flag submit"
        page.evaluate_script('window.alert = function() { return true; }')
	find(".flag_submit_button").click
      when "add new photo"
      	find("#add_photo_button_for_dialog").click
      when "idea auto activation toggle"
      	find(".toggle_autoactivate_status").click
      end
end
When /^I pick "(.*)"$/ do |radio_label|
	case radio_label 
	when "I like both ideas"
           When "I choose \"cant_decide_reason_like_both\""
	when "Other"
           When "I choose \"cant_decide_reason_user_other\""
	end
end
Given /^I save the current (.*) (choices|photos)?$/ do |side,type|
	puts "the type is #{type}"
	if type == "photos"
	   @photocracy_mode = true
           set_active_resource_credentials
	end
	if @photocracy_mode
	   Capybara.ignore_hidden_elements = false
	   @question_id = page.locate('#choose_file')[:question_id].to_i
	   Capybara.ignore_hidden_elements = true 
	else
	   @question_id = page.locate('#leftside')[:rel].to_i
	end
	
	@earl = Earl.find_by_question_id(@question_id)
        begin
	  @prompt_id = page.locate('#prompt_id')[:value].to_s
	  #The above doesn't work with selenium, for some unknown reason. Fall back to using jquery: 
	rescue Capybara::ElementNotFound
          @prompt_id = page.evaluate_script("$('#prompt_id').val()").to_i
	end
	if @prompt_id.blank?
	  raise Capybara::ElementNotFound
	end

        @prompt = Prompt.find(@prompt_id, :params => {:question_id => @question_id})

	if side == "left" or side == "two"
          @left_choice = Choice.find(@prompt.left_choice_id, :params => {:question_id => @question_id})
	end
	if side == "right" or side == "two"
          @right_choice = Choice.find(@prompt.right_choice_id, :params => {:question_id => @question_id})
	end
end
Then /^I should see thumbnails of the two saved choices in the voting history area$/ do
	  @first_thumbnail = page.locate('#your_votes li a')
	  @second_thumbnail = page.locate('#your_votes li a:last')

	  @first_url = @first_thumbnail[:href]
	  @second_url = @second_thumbnail[:href]

	  @first_url.should include(question_choice_path(@earl.name, @left_choice.id))
	  @second_url.should include(question_choice_path(@earl.name, @right_choice.id))
end
Then /^I should not see thumbnails of the two saved choices in the voting history area$/ do
	  lambda {page.locate('#your_votes li a')}.should raise_error(Capybara::ElementNotFound)
	  lambda {page.locate('#your_votes li a:last')}.should raise_error(Capybara::ElementNotFound)
end
Then /^the left thumbnail should be a winner$/ do
	page.locate('#your_votes li a img')[:class].should include("winner")
end
Then /^the left thumbnail should be a loser$/ do
	page.locate('#your_votes li a img')[:class].should include("loser")
end
Then /^the right thumbnail should be a loser$/ do
	page.locate('#your_votes li a:last img')[:class].should include("loser")
end
def set_active_resource_credentials
   puts "Changing credentials to something"
    if @photocracy_mode
       username = PHOTOCRACY_USERNAME
       password = PHOTOCRACY_PASSWORD
    else
       username = PAIRWISE_USERNAME
       password = PAIRWISE_PASSWORD
    end
    active_resource_classes = [Choice, Density, Prompt, Question, Session]
    active_resource_classes.each do |klass|
      klass.user = username
      klass.password = password
    end
end
Given /^a photocracy idea marketplace quickly exists with url '([^\']*)'$/ do |url|
	photos = []
	3.times do |i|
		p = Photo.create! :image => ActionController::TestUploadedFile.new("#{Rails.root}/db/seed-images/princeton/#{i+1}.jpg", "image/jpg")

		photos << p.id
	end
	
	q = Question.create(Factory.attributes_for(:question, :ideas => photos.join("\n")))
	e = Factory.create(:earl, :name => url, :question_id => q.id)
end
Given /^idea marketplace '(.*)' has enabled "([^\"]*)"$/ do |url, setting|
	e = Earl.find(url)
	case setting.downcase
	when "flag as inappropriate"
		e.flag_enabled = true
	end
	e.save!
end
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )fill in "([^\"]*)" with "([^\"]*)"(?: within "([^\"]*)")?$/ do |field, value, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)
    path = case page_name

    when /the homepage/i
      root_path
    when /the sign up page/i
      new_user_path
    when /the sign in page/i
      new_session_path
    when /the password reset request page/i
      new_password_path

    when /the question show page/i
      question_path
    when /the question create page/i
      new_question_path
    when /the questions index page/i
      questions_path
    
    when /the Cast Votes page for '([^'].*)'/i
	"/"+ $1
    when /the Just Created Cast Votes page for '([^'].*)'/i
	"/"+ $1 + "?just_created=true"
    when /the View Results page for '([^'].*)'/i
	"/"+ $1 + "/results"
    when /the Admin page for '([^'].*)'/i
	"/"+ $1 + "/admin"
    when /the Control Panel page/i
	 admin_path
    when /the WIDGET Cast Votes page for '([^'].*)'/i
	"/"+ $1 + '?widget&width=450&height=410'

    when /the Deactivate page for the saved (.*) choice/
	 @earl = Earl.find_by_question_id(@question_id)
	 choice = ($1 == "left") ? @left_choice : @right_choice
	 deactivate_question_choice_path(:question_id => @earl, :id => choice.id)
    
    when /the Activate page for the saved (.*) choice/
	 @earl = Earl.find_by_question_id(@question_id)
	 choice = ($1 == "left") ? @left_choice : @right_choice
	 activate_question_choice_path(:question_id => @earl, :id => choice.id)

    
    when /the Idea Detail page for the saved left choice(.*)?/i
	 @earl = Earl.find_by_question_id(@question_id)
	 url_opts = {:question_id => @earl, :id => @left_choice.id}
	 if($1 =~ /with login reminder/i)
		 url_opts.merge!(:login_reminder => true)
	 end
	 question_choice_path(url_opts)
    when /the Idea Detail page for the saved right choice/i
	 @earl = Earl.find_by_question_id(@question_id)
	 question_choice_path(:question_id => @earl, :id => @right_choice.id)
    # Add more page name => path mappings here

    else
      raise "Can't find mapping from \"#{page_name}\" to a path."
    end
    if @photocracy_mode
	path += (path.include?("?")) ? "&" : "?"
        path += "photocracy_mode=true"
    end 
    path
  end
