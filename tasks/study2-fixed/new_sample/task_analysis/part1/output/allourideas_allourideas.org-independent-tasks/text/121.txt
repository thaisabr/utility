Feature: Add photo to marketplace
In order to have photos from the community be considered
A user
Should be able to add their own photo while voting
Background:
Given a photocracy idea marketplace quickly exists with url 'test' and admin 'test@example.com/password'
And I sign in as "test@example.com/password"
And no emails have been sent
@photocracy @selenium
Scenario: Unmoderated marketplace
Given idea marketplace 'test' has enabled idea autoactivation
And I am on the Cast Votes page for 'test'
When I upload a photo
Then I should see "Your photo will appear soon."
Then "test@example.com" should receive an email
When "test@example.com" opens the email
Then they should see "[Photocracy] photo added to question: test name" in the email subject
And they should see "Someone has uploaded a new photo to your question" in the email body
And they should see "http://photocracy.org/test/choices/" in the email body
And they should see "Based on your settings, we have auto-activated the photo" in the email body
When they click the first link in the email
Then I should see "Activated"
@photocracy
@photocracy @selenium
Scenario: Moderated marketplace
Given I am on the Cast Votes page for 'test'
When I upload a photo
Then I should see "Your photo has been submitted for review. It will appear soon."
Then "test@example.com" should receive an email
When "test@example.com" opens the email
Then they should see "[Photocracy] photo added to question: test name" in the email subject
And they should see "Someone has uploaded a new photo to your question" in the email body
And they should see "If you want others to be able to vote on this photo, please activate it by visiting the following url:" in the email body
When they click the first link in the email
Then I should see "Deactivated"
Feature: Flag as inappropriate
In order to prevent unsavory materials from being displayed on our site
A user
Should be able to flag a photo as inappropriate
Background:
Given a photocracy idea marketplace quickly exists with url 'test' and admin 'test@example.com/password'
And idea marketplace 'test' has enabled "flag as inappropriate"
And I am on the Cast Votes page for 'test'
And I save the current two photos
And no emails have been sent
Scenario: User submits a flag request
When I click the flag link for the left choice
And I fill in "inappropriate_reason" with "Because it's offensive"
And I click the photocracy flag submit button
Then I should not see thumbnails of the two saved choices in the voting history area
Then the saved left choice should not be active
And "test@example.com" should receive an email
When "test@example.com" opens the email
Then they should see "[Photocracy] Possible inappropriate photo flagged by user" in the email subject
And they should see "Because it's offensive" in the email body
Feature: Ignore votes immediately after skips
In order to prevent fraud by selective voting
A user
Should have their votes immediately after a skip marked invalid
Background:
Given an idea marketplace quickly exists with url 'test'
And I am on the Cast Votes page for 'test'
@selenium
Scenario: Votes should appear counted, but not actually count
When I click the I can't decide button
And I pick "I like both ideas"
And I click the I can't decide submit button
Then I should see "You couldn't decide." within ".tellmearea"
And the vote count should be 0
When I click on the left choice
Then I should see "You chose" within ".tellmearea"
And the vote count should be 1
When I go to the Cast Votes page for 'test'
Then the vote count should be 0
When I go to the View Results page for 'test'
Then I should see "50"
And I should not see "67"
And I should not see "66"
And I should not see "33"
