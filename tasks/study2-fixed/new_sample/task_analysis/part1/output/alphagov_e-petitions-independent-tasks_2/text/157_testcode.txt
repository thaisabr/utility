Given /^I am logged in as a moderator$/ do
  @user = FactoryGirl.create(:moderator_user)
  step "the admin user is logged in"
end
When(/^I look at the next petition on my list$/) do
  @petition = FactoryGirl.create(:sponsored_petition, :with_additional_details, :action => "Petition 1")
  visit admin_petition_url(@petition)
end
When(/^I reject the petition with a reason code "([^"]*)"$/) do |reason_code|
  choose "Reject"
  select reason_code, :from => :petition_rejection_code
  click_button "Email petition creator"
end
When(/^I change the rejection status of the petition with a reason code "([^"]*)"$/) do |reason_code|
  click_on 'Change rejection reason'
  select reason_code, :from => :petition_rejection_code
  click_button "Email petition creator"
end
Then /^the petition is not available for signing$/ do
  visit petition_url(@petition)
  expect(page).not_to have_css("a", :text => "Sign")
end
Then /^the petition is still available for searching or viewing$/ do
  step %{I search for "Rejected petitions" with "#{@petition.action}"}
  step %{I should see the petition "#{@petition.action}"}
  step %{I view the petition}
  step %{I should see the petition details}
end
Then /^the petition is not available for searching or viewing$/ do
  step %{I search for "Rejected petitions" with "#{@petition.action}"}
  step %{I should not see the petition "#{@petition.action}"}
end
Then /^the petition will still show up in the back\-end reporting$/ do
  visit admin_petitions_url
  step %{I should see the petition "#{@petition.action}"}
end
Then(/^the creator should receive a (libel\/profanity )?rejection notification email$/) do |petition_is_libellous|
  @petition.reload
  steps %Q(
    Then "#{@petition.creator_signature.email}" should receive an email
    When they open the email
    Then they should see "We rejected the petition you created" in the email body
    And they should see "#{rejection_description(@petition.rejection.code).gsub(/<.*?>/,' ').split.last}" in the email body
    And they should see /We rejected your petition/ in the email subject
  )
  if petition_is_libellous
    step %{they should not see "#{petition_url(@petition)}" in the email body}
  else
    step %{they should see "#{petition_url(@petition)}" in the email body}
  end
end
Then /^I see relevant reason descriptions when I browse different reason codes$/ do
  choose "Reject"
  select "Duplicate petition", :from => :petition_rejection_code
  expect(page).to have_content "already a petition"
  select "Confidential, libellous, false, defamatory or references a court case", :from => :petition_rejection_code
  expect(page).to have_content "It included confidential, libellous, false or defamatory information, or a reference to a case which is active in the UK courts."
end
Given(/^a petition "([^"]*)" has been rejected( with the reason "([^"]*)")?$/) do |petition_action, reason_or_not, reason|
  reason_text = reason.nil? ? "It doesn't make any sense" : reason
  @petition = FactoryGirl.create(:rejected_petition,
    :action => petition_action,
    :rejection_code => "irrelevant",
    :rejection_details => reason_text)
end
When /^(?:|I )go to (.+)$/ do |page_name|
  visit url_to(page_name)
end
  def url_to(page_name)
    case page_name

    when /^the home\s?page$/
      home_url

    when /^the help page$/
      help_url

    when /^the privacy page$/
      privacy_url

    when /^the feedback page$/
      feedback_url

    when /^the new petition page$/
      new_petition_url

    when /^the petition page for "([^\"]*)"$/
      petition_url(Petition.find_by(action: $1))

    when /^the archived petitions page$/
      archived_petitions_url

    when /^the archived petitions search results page$/
      search_archived_petitions_url

    when /^the archived petition page for "([^\"]*)"$/
      archived_petition_url(ArchivedPetition.find_by(title: $1))

    when /^the new signature page for "([^\"]*)"$/
      new_petition_signature_url(Petition.find_by(action: $1))

    when /^the search results page$/
      search_url

    when /^the Admin (.*)$/i
      admin_url($1)

    when /^the local petitions results page$/
      local_petition_url(@my_constituency.slug)

    else
      begin
        page_name =~ /^the (.*) page$/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('url').join('_').to_sym)
      rescue NoMethodError, ArgumentError
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Given /^the admin user is logged in$/ do
  visit admin_login_url
  fill_in("Email", :with => @user.email)
  fill_in("Password", :with => "Letmein1!")
  click_button("Sign in")
end
When(/^I search for "([^"]*)" with "([^"]*)"$/) do |facet, term|
  step %{I browse to see only "#{facet}" petitions}
  step %{I fill in "#{term}" as my search term}
  step %{I press "Search"}
end
Then /^I should see the petition "([^"]*)"$/ do |petition_action|
  expect(page).to have_link(petition_action)
end
Then /^I should not see the petition "([^"]*)"$/ do |petition_action|
  expect(page).not_to have_link(petition_action)
end
When(/^I view the petition$/) do
  if @petition.is_a?(ArchivedPetition)
    visit archived_petition_url(@petition)
  else
    visit petition_url(@petition)
  end
end
Then(/^I should see the petition details$/) do
  if @petition.is_a?(ArchivedPetition)
    expect(page).to have_content(@petition.title)
    expect(page).to have_content(@petition.description)
  else
    expect(page).to have_content(@petition.action)
    expect(page).to have_content(@petition.additional_details)
    expect(page).to have_content(@petition.background)
  end
end
Then /^(?:I|they|"([^"]*?)") should receive (an|no|\d+) emails?$/ do |address, amount|
  expect(unread_emails_for(address).size).to eq parse_email_count(amount)
end
When /^(?:I|they|"([^"]*?)") opens? the email$/ do |address|
  open_email(address)
end
Then /^(?:I|they) should see \/([^"]*?)\/ in the email subject$/ do |text|
  expect(current_email).to have_subject(Regexp.new(text))
end
Then /^(?:I|they) should see "([^"]*?)" in the email body$/ do |text|
  expect(current_email.default_part_body.to_s).to include(text)
end
Then /^(?:I|they) should not see "([^"]*?)" in the email body$/ do |text|
  expect(current_email.default_part_body.to_s).not_to match(Regexp.new(text))
end
When(/^I browse to see only "([^"]*)" petitions$/) do |facet|
  step "I go to the petitions page"
  within :css, '#other-search-lists' do
    click_on facet
  end
end
When(/^I fill in "(.*?)" as my search term$/) do |search_term|
  fill_in :search, with: search_term
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    click_button(button)
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
