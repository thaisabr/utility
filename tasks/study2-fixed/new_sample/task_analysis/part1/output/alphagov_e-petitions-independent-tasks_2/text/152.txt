Feature: Searching for signatures as Terry
In order to easily find out if someone's signed a petition
As Terry
I would like to be able to enter an email address, and see all signatures associated with it
Scenario: Validating a pending signature
Given 1 petition with a pending signature by "bob@example.com"
And I am logged in as a moderator
When I search for petitions signed by "bob@example.com" from the admin hub
Then I should see the email address is pending
When I click the validate button
Then I should see the email address is validated
