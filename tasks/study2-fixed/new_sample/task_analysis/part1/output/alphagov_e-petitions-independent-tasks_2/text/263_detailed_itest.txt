Classes: 13
[name:ConstituencyApi, file:alphagov_e-petitions/app/lib/constituency_api.rb, step:Given ]
[name:ConstituencyApi, file:alphagov_e-petitions/features/support/constituency_api.rb, step:Given ]
[name:FactoryGirl, file:null, step:Given ]
[name:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:Given ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:Given ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/petition_creator/petition.rb, step:Given ]
[name:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:Then ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:Then ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/petition_creator/petition.rb, step:Then ]
[name:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:null]
[name:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:null]
[name:Petition, file:alphagov_e-petitions/app/models/staged/petition_creator/petition.rb, step:null]
[name:XPathHelpers, file:null, step:Then ]

Methods: 65
[name:Integer, type:Object, file:null, step:Then ]
[name:ago, type:Object, file:null, step:Given ]
[name:all, type:Object, file:null, step:Then ]
[name:capture_model, type:Object, file:null, step:Then ]
[name:check, type:PetitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:null]
[name:check, type:PetitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:null]
[name:check, type:PetitionsController, file:alphagov_e-petitions/app/controllers/petitions_controller.rb, step:null]
[name:click_on, type:Object, file:null, step:When ]
[name:count, type:Object, file:null, step:Then ]
[name:eq, type:Object, file:null, step:Then ]
[name:fetch, type:Object, file:null, step:Given ]
[name:fetch, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find_by!, type:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:Given ]
[name:find_by!, type:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:Given ]
[name:find_by!, type:Petition, file:alphagov_e-petitions/app/models/staged/petition_creator/petition.rb, step:Given ]
[name:find_by!, type:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:Then ]
[name:find_by!, type:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:Then ]
[name:find_by!, type:Petition, file:alphagov_e-petitions/app/models/staged/petition_creator/petition.rb, step:Then ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_selector, type:Object, file:null, step:Then ]
[name:have_text, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:Given ]
[name:id, type:Object, file:null, step:Then ]
[name:index, type:PagesController, file:alphagov_e-petitions/app/controllers/pages_controller.rb, step:null]
[name:index, type:PetitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:null]
[name:index, type:PetitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:null]
[name:index, type:PetitionsController, file:alphagov_e-petitions/app/controllers/petitions_controller.rb, step:null]
[name:index, type:Archived/petitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:null]
[name:map, type:Object, file:null, step:Then ]
[name:match, type:Object, file:null, step:Then ]
[name:model!, type:Object, file:null, step:Then ]
[name:mp, type:Object, file:null, step:Given ]
[name:name, type:AdminUser, file:alphagov_e-petitions/app/models/admin_user.rb, step:Given ]
[name:name, type:Stages, file:alphagov_e-petitions/app/models/staged/petition_creator/stages.rb, step:Given ]
[name:name, type:Stages, file:alphagov_e-petitions/app/models/staged/petition_signer/stages.rb, step:Given ]
[name:name, type:Object, file:null, step:Then ]
[name:new, type:PetitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:null]
[name:new, type:PetitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:null]
[name:new, type:PetitionsController, file:alphagov_e-petitions/app/controllers/petitions_controller.rb, step:null]
[name:nil?, type:Object, file:null, step:Given ]
[name:not_to, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:Then ]
[name:present?, type:Object, file:null, step:Given ]
[name:raise, type:Object, file:null, step:Given ]
[name:rejected?, type:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:null]
[name:rejected?, type:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:null]
[name:rejected?, type:Petition, file:alphagov_e-petitions/app/models/staged/petition_creator/petition.rb, step:null]
[name:reverse, type:Object, file:null, step:Then ]
[name:show, type:PetitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:null]
[name:show, type:PetitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:null]
[name:show, type:PetitionsController, file:alphagov_e-petitions/app/controllers/petitions_controller.rb, step:null]
[name:show, type:Archived/petitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:null]
[name:signatures, type:Object, file:null, step:Then ]
[name:sort, type:Object, file:null, step:Then ]
[name:start_date, type:Object, file:null, step:Given ]
[name:stub_constituency, type:Object, file:null, step:Given ]
[name:text, type:Object, file:null, step:Then ]
[name:times, type:Object, file:null, step:Given ]
[name:to, type:Object, file:null, step:Then ]
[name:validate!, type:Signature, file:alphagov_e-petitions/app/models/signature.rb, step:Given ]
[name:validated, type:Object, file:null, step:Then ]
[name:where, type:Object, file:null, step:Then ]
[name:within, type:Object, file:null, step:When ]
[name:within, type:Object, file:null, step:Then ]

Referenced pages: 18
alphagov_e-petitions/app/views/archived/petitions/index.html.erb
alphagov_e-petitions/app/views/archived/petitions/show.html.erb
alphagov_e-petitions/app/views/pages/index.html.erb
alphagov_e-petitions/app/views/petitions/_action_help.html.erb
alphagov_e-petitions/app/views/petitions/_closed_petition_show.html.erb
alphagov_e-petitions/app/views/petitions/_open_petition_show.html.erb
alphagov_e-petitions/app/views/petitions/_rejected_petition_show.html.erb
alphagov_e-petitions/app/views/petitions/_threshold_details.html.erb
alphagov_e-petitions/app/views/petitions/_trending_petition.html.erb
alphagov_e-petitions/app/views/petitions/check.html.erb
alphagov_e-petitions/app/views/petitions/check_results.html.erb
alphagov_e-petitions/app/views/petitions/index.html.erb
alphagov_e-petitions/app/views/petitions/new.html.erb
alphagov_e-petitions/app/views/petitions/search/_filter_nav.html.erb
alphagov_e-petitions/app/views/petitions/search/_result_list_footer.html.erb
alphagov_e-petitions/app/views/petitions/search/_results.html.erb
alphagov_e-petitions/app/views/petitions/search/_results_for_pre_create_petition.html.erb
alphagov_e-petitions/app/views/petitions/show.html.erb

