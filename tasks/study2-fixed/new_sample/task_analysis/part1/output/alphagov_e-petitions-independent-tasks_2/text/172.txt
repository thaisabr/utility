Feature: Moderator respond to petition
In order to allow or prevent the signing of petitions
As a moderator
I want to respond to a petition, editing, accepting, rejecting or re-assigning it
Scenario: Moderator rejects petition with a suitable reason code
Given I am logged in as a moderator
When I look at the next petition on my list
And I reject the petition with a reason code "Not the government’s responsibility"
Then the petition is not available for signing
But the petition is still available for searching or viewing
Scenario: Moderator rejects petition with a suitable reason code and text
Given I am logged in as a moderator
When I look at the next petition on my list
And I reject the petition with a reason code "Duplicate petition" and some explanatory text
Then the explanation is displayed on the petition for viewing by the public
And the creator should receive a rejection notification email
Scenario: Moderator rejects petition with a reason code which precludes public searching or viewing
Given I am logged in as a moderator
When I look at the next petition on my list
And I reject the petition with a reason code "Confidential, libellous, false or defamatory (will be hidden)"
And the creator should receive a libel/profanity rejection notification email
And the petition is not available for searching or viewing
But the petition will still show up in the back-end reporting
Scenario: Moderator rejects and hides previously rejected (and public) petition
And I am logged in as a moderator
And a petition "actually libellous" has been rejected with the reason "duplicate"
When I go to the admin petition page for "actually libellous"
And I change the rejection status of the petition with a reason code "Confidential, libellous, false or defamatory (will be hidden)"
Then the petition is not available for searching or viewing
But the petition will still show up in the back-end reporting
Feature: Terry (or Sheila) takes down a petition
In order to remove petitions that have been published by mistake
As Terry or Sheila
I want to reject a petition after it has been published, which sends the standard rejection email out to the creator and removes the petition from the site
Background:
Given a petition "Mistakenly published petition"
Scenario: A sysadmin can take down a petition
Given I am logged in as a sysadmin
When I view all petitions
And I follow "Mistakenly published petition"
And I take down the petition with a reason code "Duplicate petition"
Then the petition is not available for signing
And I should not be able to take down the petition
Scenario: A moderator can take down a petition
Given I am logged in as a moderator
When I view all petitions
And I follow "Mistakenly published petition"
And I take down the petition with a reason code "Duplicate petition"
Then the petition is not available for signing
And I should not be able to take down the petition
Feature: As Charlie
In order to have an issue discussed in parliament
I want to be able to create a petition and verify my email address.
Scenario: Charlie creates a petition
Given I start a new petition
And I fill in the petition details
And I press "Preview petition"
And I press "This looks good"
And I fill in my details
When I press "Continue"
Then the markup should be valid
And I am asked to review my email address
When I press "Yes – this is my email address"
Then a petition should exist with action: "The wombats of wimbledon rock.", state: "pending"
And there should be a "pending" signature with email "womboid@wimbledon.com" and name "Womboid Wibbledon"
And "Womboid Wibbledon" wants to be notified about the petition's progress
And "womboid@wimbledon.com" should be emailed a link for gathering support from sponsors
Scenario: Charlie creates a petition with invalid postcode SW14 9RQ
Given I start a new petition
And I fill in the petition details
And I press "Preview petition"
And I press "This looks good"
And I fill in my details with postcode "SW14 9RQ"
And I press "Continue"
Then I should not see the text "Your constituency is"
@javascript
Scenario: Charlie tries to submit an invalid petition
Given I am on the new petition page
When I press "Preview petition"
Then I should see "Action must be completed"
And I should see "Background must be completed"
When I am allowed to make the petition action too long
When I fill in "What do you want us to do?" with "012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789Blah"
And I fill in "Background" with "This text is longer than 300 characters. 012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789"
And I fill in "Additional details" with "This text is longer than 500 characters. 012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789"
And I press "Preview petition"
Then I should see "Action is too long"
And I should see "Background is too long"
And I should see "Additional details is too long"
When I fill in "What do you want us to do?" with "The wombats of wimbledon rock."
And I fill in "Background" with "Give half of Wimbledon rock to wombats!"
And I fill in "Additional details" with "The racial tensions between the wombles and the wombats are heating up.  Racial attacks are a regular occurrence and the death count is already in 5 figures.  The only resolution to this crisis is to give half of Wimbledon common to the Wombats and to recognise them as their own independent state."
And I press "Preview petition"
Then I should see a heading called "Check your petition"
And I should see "The wombats of wimbledon rock."
And I expand "More details"
And I should see "The racial tensions between the wombles and the wombats are heating up.  Racial attacks are a regular occurrence and the death count is already in 5 figures.  The only resolution to this crisis is to give half of Wimbledon common to the Wombats and to recognise them as their own independent state."
And I press "Go back and make changes"
And the "What do you want us to do?" field should contain "The wombats of wimbledon rock."
And the "Background" field should contain "Give half of Wimbledon rock to wombats!"
And the "Additional details" field should contain "The racial tensions between the wombles and the wombats are heating up. Racial attacks are a regular occurrence and the death count is already in 5 figures. The only resolution to this crisis is to give half of Wimbledon common to the Wombats and to recognise them as their own independent state."
And I press "Preview petition"
And I press "This looks good"
Then I should see a heading called "Sign your petition"
When I press "Continue"
Then I should see "Name must be completed"
And I should see "Email must be completed"
And I should see "You must be a British citizen"
And I should see "Postcode must be completed"
When I fill in my details
And I press "Continue"
Then I should see a heading called "Make sure this is right"
And I press "Back"
And I fill in "Name" with "Mr. Wibbledon"
And I press "Continue"
Then I should see a heading called "Make sure this is right"
When I fill in "Email" with ""
And I press "Yes – this is my email address"
Then I should see "Email must be completed"
When I fill in "Email" with "womboid@wimbledon.com"
And I press "Yes – this is my email address"
Then a petition should exist with action: "The wombats of wimbledon rock.", state: "pending"
Then there should be a "pending" signature with email "womboid@wimbledon.com" and name "Mr. Wibbledon"
