Feature: Joe views an archived petition
In order to see what petitions were created in the past
As Joe, a member of the general public
I want to be able to view archived petitions
Scenario: Joe views an archived petition
Given an archived petition "Spend more money on Defence"
When I view the petition
Then I should see the petition details
And I should see "Spend more money on Defence - Petitions" in the browser page title
And I should see the vote count, closed and open dates
And I should see "This petition has been archived It was submitted during the 2010–2015 Conservative – Liberal Democrat coalition government"
Scenario: Joe views an archived petition at the old url and is redirected
Given an archived petition "Spend more money on Defence"
When I view the petition at the old url
Then I should be redirected to the archived url
And I should see "Spend more money on Defence - Petitions" in the browser page title
And I should see the vote count, closed and open dates
And I should see "This petition has been archived It was submitted during the 2010–2015 Conservative – Liberal Democrat coalition government"
Scenario: Joe sees a 'closed' message when viewing an archived petition
Given an archived petition "Spend more money on Defence"
When I view the petition
Then I should see "This petition has been archived It was submitted during the 2010–2015 Conservative – Liberal Democrat coalition government"
