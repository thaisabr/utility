When /^I decide to sign the petition$/ do
  visit petition_url(@petition)
  click_link "Sign this petition"
end
When /^I try to sign$/ do
  click_button "Continue"
end
Then /^I am told to check my inbox to complete signing$/ do
  expect(page).to have_title("Thank you")
  expect(page).to have_content("Check your email")
end
When(/^I confirm my email address$/) do
  steps %Q(
    And I open the email with subject "Please confirm your email address"
    When I click the first link in the email
  )
end
When(/^I fill in my details(?: with email "([^"]+)")?$/) do |email_address|
  email_address ||= "womboid@wimbledon.com"
  steps %Q(
    When I fill in "Name" with "Womboid Wibbledon"
    And I fill in "Email" with "#{email_address}"
    And I check "I am a British citizen or UK resident"
    And I fill in my postcode with "SW14 9RQ"
    And I select "United Kingdom" from "Location"
  )
end
When(/^I fill in my postcode with "(.*?)"$/) do |postcode|
  step %{I fill in "Postcode" with "#{postcode}"}
  sanitized_postcode = PostcodeSanitizer.call(postcode)
  fixture_file = sanitized_postcode == "N11TY" ? "single" : "no_results"
  stub_api_request_for(sanitized_postcode).to_return(api_response(:ok, fixture_file))
end
Then(/^I am asked to review my email address$/) do
  expect(page).to have_content 'Make sure this is right'
  expect(page).to have_field('Email')
end
When(/^I change my email address to "(.*?)"$/) do |email_address|
  fill_in 'Email', with: email_address
end
When(/^I say I am happy with my email address$/) do
  click_on "Yes – this is my email address"
end
Then /^(?:I|they|"([^"]*?)") should receive (an|no|\d+) emails?$/ do |address, amount|
  expect(unread_emails_for(address).size).to eq parse_email_count(amount)
end
Then(/^#{capture_model} should be #{capture_model}$/) do |a, b|
  expect(model!(a)).to eq model!(b)
end
Then(/^#{capture_model} should (?:be|have) (?:an? )?#{capture_predicate}$/) do |name, predicate|
  if model!(name).respond_to?("has_#{predicate.gsub(' ', '_')}")
    expect(model!(name)).to send("have_#{predicate.gsub(' ', '_')}")
  else
    expect(model!(name)).to send("be_#{predicate.gsub(' ', '_')}")
  end
end
Given(/^a(n)? ?(pending|validated|sponsored|flagged|open)? petition "([^"]*)"$/) do |a_or_an, state, petition_action|
  petition_args = {
    :action => petition_action,
    :closed_at => 1.day.from_now,
    :state => state || "open"
  }
  @petition = FactoryGirl.create(:open_petition, petition_args)
end
Then(/^I can click on a link to return to the petition$/) do
  expect(page).to have_css("a[href*='/petitions/#{@petition.id}']")
end
Then(/^I should see my constituency "([^"]*)"/) do |constituency|
  expect(page).to have_text(constituency)
end
Then(/^I should see my MP/) do
  signature = Signature.find_by(email: "womboidian@wimbledon.com",
                                 postcode: "N11TY",
                                 name: "Womboid Wibbledon",
                                 petition_id: @petition.id)
  expect(page).to have_text(signature.constituency.mp_name)
end
Then(/^I can click on a link to visit my MP$/) do
  signature = Signature.find_by(email: "womboidian@wimbledon.com",
                                 postcode: "N11TY",
                                 name: "Womboid Wibbledon",
                                 petition_id: @petition.id)
  expect(page).to have_css("a[href*='#{signature.constituency.mp_url}']")
end
When /^(?:|I )go to (.+)$/ do |page_name|
  visit url_to(page_name)
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    expect(page).to have_content(text)
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def url_to(page_name)
    case page_name

    when /^the home\s?page$/
      home_url

    when /^the help page$/
      help_url

    when /^the privacy page$/
      privacy_url

    when /^the feedback page$/
      feedback_url

    when /^the new petition page$/
      new_petition_url

    when /^the petition page for "([^\"]*)"$/
      petition_url(Petition.find_by(action: $1))

    when /^the archived petitions page$/
      archived_petitions_url

    when /^the archived petitions search results page$/
      search_archived_petitions_url

    when /^the archived petition page for "([^\"]*)"$/
      archived_petition_url(ArchivedPetition.find_by(title: $1))

    when /^the new signature page for "([^\"]*)"$/
      new_petition_signature_url(Petition.find_by(action: $1))

    when /^the search results page$/
      search_url

    when /^the Admin (.*)$/i
      admin_url($1)

    when /^the local petitions results page$/
      local_petition_url(@my_constituency.slug)

    else
      begin
        page_name =~ /^the (.*) page$/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('url').join('_').to_sym)
      rescue NoMethodError, ArgumentError
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Then /^I should (not |)see "([^\"]*)" in the ((?!email\b).*)$/ do |see_or_not, text, section_name|
  if section_name == 'browser page title'
    if see_or_not.blank?
      expect(page).to have_title(text.to_s)
    else
      expect(page).to have_no_title(text.to_s)
    end
  else
    within_section(section_name) do
      if see_or_not.blank?
        expect(page).to have_content(text.to_s)
      else
        expect(page).to have_no_content(text.to_s)
      end
    end
  end
end
  def within_section(section_name)
    within xpath_of_section(section_name) do
      yield
    end
  end
  def xpath_of_section(section_name, prefix = "//")
    case section_name

    # Non site-specific based
    when /"([^\"]*)" fieldset/
      "#{prefix}fieldset#{XPathHelpers.class_matching($1.downcase.gsub(/\s/, '_'))}"

    # Sitewide
    when /^single h1$/
      expect(page).to have_xpath("//h1", :count => 1)
      "#{prefix}h1"

    else
      raise "Can't find mapping from \"#{section_name}\" to a section."
    end
  end
Then /^I should see an error$/ do
  expect(page).to have_css(".error-message", :text => /.+/)
end
When /^(?:I|they|"([^"]*?)") opens? the email with subject "([^"]*?)"$/ do |address, subject|
  open_email(address, :with_subject => subject)
end
When /^(?:I|they) click the first link in the email$/ do
  visit links_in_email(current_email).first
end
When /^(?:|I )fill in "([^"]*)" with "([^"]*)"(?: within "([^"]*)")?$/ do |field, value, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
When /^(?:|I )select "([^"]*)" from "([^"]*)"(?: within "([^"]*)")?$/ do |value, field, selector|
  with_scope(selector) do
    select(value, :from => field)
  end
end
When /^(?:|I )check "([^"]*)"(?: within "([^"]*)")?$/ do |field, selector|
  with_scope(selector) do
    check(field)
  end
end
When(/^I fill in my postcode with "(.*?)"$/) do |postcode|
  step %{I fill in "Postcode" with "#{postcode}"}
  sanitized_postcode = PostcodeSanitizer.call(postcode)
  fixture_file = sanitized_postcode == "N11TY" ? "single" : "no_results"
  stub_api_request_for(sanitized_postcode).to_return(api_response(:ok, fixture_file))
end
