Feature: Moderator respond to petition
In order to allow or prevent the signing of petitions
As a moderator
I want to respond to a petition, editing, accepting, rejecting or re-assigning it
Scenario: Moderator rejects petition with a suitable reason code
Given I am logged in as a moderator
When I look at the next petition on my list
And I reject the petition with a reason code "Not the Government/Parliament’s responsibility"
Then the petition is not available for signing
But the petition is still available for searching or viewing
Scenario: Moderator rejects petition with a reason code which precludes public searching or viewing
Given I am logged in as a moderator
When I look at the next petition on my list
And I reject the petition with a reason code "Confidential, libellous, false, defamatory or references a court case"
And the creator should receive a libel/profanity rejection notification email
And the petition is not available for searching or viewing
But the petition will still show up in the back-end reporting
Scenario: Moderator rejects and hides previously rejected (and public) petition
And I am logged in as a moderator
And a petition "actually libellous" has been rejected with the reason "duplicate"
When I go to the admin petition page for "actually libellous"
And I change the rejection status of the petition with a reason code "Confidential, libellous, false, defamatory or references a court case"
Then the petition is not available for searching or viewing
But the petition will still show up in the back-end reporting
