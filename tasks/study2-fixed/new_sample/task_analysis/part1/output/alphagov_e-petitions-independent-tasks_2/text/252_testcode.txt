When(/^I browse to see only "([^"]*)" petitions$/) do |facet|
  step "I go to the petitions page"
  within :css, '#other-search-lists' do
    click_on facet
  end
end
When(/^I search for "([^"]*)" with "([^"]*)"$/) do |facet, term|
  step %{I browse to see only "#{facet}" petitions}
  step %{I fill in "#{term}" as my search term}
  step %{I press "Search"}
end
Then(/^I should see my search term "(.*?)" filled in the search field$/) do |search_term|
  expect(page).to have_field('q', with: search_term)
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    expect(page).to have_content(text)
  end
end
Then /^(?:|I )should not see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    expect(page).to have_no_content(text)
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
Then /^I should see the following search results:$/ do |values_table|
  values_table.raw.each do |row|
    row.each do |column|
      expect(page).to have_content(column)
    end
  end
end
Then(/^I should see the following ordered list of petitions:$/) do |table|
  actual_petitions = page.all(:css, '.search-results ol li a').map(&:text)
  expected_petitions = table.raw.flatten
  expect(actual_petitions).to eq(expected_petitions)
end
Given(/^#{capture_model} exists?(?: with #{capture_fields})?$/) do |name, fields|
  create_model(name, fields)
end
Then(/^#{capture_model} should be #{capture_model}$/) do |a, b|
  expect(model!(a)).to eq model!(b)
end
Given /^the(?: date is the| time is) "([^"]*)"$/ do |description|
  travel_to description.in_time_zone
end
Given /^a petition "([^"]*)" exists with a signature count of (\d+)$/ do |petition_action, count|
    @petition = FactoryGirl.create(:open_petition, action: petition_action)
    @petition.update_attribute(:signature_count, count)
end
Given /^a ?(open|closed)? petition "([^"]*)" exists and has received a government response (\d+) days ago$/ do |state, petition_action, parliament_response_days_ago |
  petition_attributes = {
    action: petition_action,
    closed_at: state == 'closed' ? 1.day.ago : 6.months.from_now,
    response_summary: 'Response Summary',
    response_details: 'Government Response',
    government_response_at: parliament_response_days_ago.to_i.days.ago
  }
  FactoryGirl.create(:responded_petition, petition_attributes)
end
Given(/^a petition "(.*?)" exists and hasn't passed the threshold for a ?(response|debate)?$/) do |action, response_or_debate|
  FactoryGirl.create(:open_petition, action: action)
end
Given(/^a petition "(.*?)" exists and passed the threshold for a response less than a day ago$/) do |action|
  FactoryGirl.create(:open_petition, action: action, response_threshold_reached_at: 2.hours.ago)
end
Given(/^a petition "(.*?)" exists and passed the threshold for a response (\d+) days? ago$/) do |action, amount|
  FactoryGirl.create(:open_petition, action: action, response_threshold_reached_at: amount.days.ago)
end
Given(/^a petition "(.*?)" passed the threshold for a debate less than a day ago and has no debate date set$/) do |action|
  petition = FactoryGirl.create(:open_petition, action: action, debate_threshold_reached_at: 2.hours.ago)
  petition.debate_outcome = nil
end
Given(/^a petition "(.*?)" passed the threshold for a debate (\d+) days? ago and has no debate date set$/) do |action, amount|
  petition = FactoryGirl.create(:open_petition, action: action, debate_threshold_reached_at: amount.days.ago)
  petition.debate_outcome = nil
end
Given /^a petition "([^"]*)" has been rejected( with the reason "([^"]*)")?$/ do |petition_action, reason_or_not, reason|
  reason_text = reason.nil? ? "It doesn't make any sense" : reason
  @petition = FactoryGirl.create(:rejected_petition,
    :action => petition_action,
    :rejection_code => "irrelevant",
    :rejection_details => reason_text)
end
When /^I view all petitions from the home page$/ do
  visit home_path
  click_link "View all"
end
Given(/^a petition "(.*?)" has been debated (\d+) days ago?$/) do |petition_action, debated_days_ago|
  @petition = FactoryGirl.create(:debated_petition,
    action: petition_action,
    debated_on: debated_days_ago.days.ago.to_date,
    overview: 'Everyone was in agreement, this petition must be made law!',
    transcript_url: 'http://transcripts.parliament.example.com/2.html',
    video_url: 'http://videos.parliament.example.com/2.avi'
  )
  @petition.update(debate_outcome_at: debated_days_ago.days.ago)
end
When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    click_button(button)
  end
end
  def path_to(page_name)
    case page_name

    when /^the home\s?page$/
      '/'

    when /^the help page$/
      help_path

    when /^the feedback page$/
      feedback_path

    when /^the new petition page$/
      new_petition_path

    when /^the petition page for "([^\"]*)"$/
      petition_path(Petition.find_by(action: $1))

    when /^the archived petitions page$/
      archived_petitions_path

    when /^the archived petitions search results page$/
      search_archived_petitions_path

    when /^the archived petition page for "([^\"]*)"$/
      archived_petition_path(ArchivedPetition.find_by(title: $1))

    when /^the new signature page for "([^\"]*)"$/
      new_petition_signature_path(Petition.find_by(action: $1))

    when /^the search results page$/
      search_path

    when /^the Admin (.*)$/i
      admin_path($1)

    when /^the local petitions results page$/
      local_petitions_path

    else
      begin
        page_name =~ /^the (.*) page$/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue NoMethodError, ArgumentError
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
When(/^I fill in "(.*?)" as my search term$/) do |search_term|
  fill_in :search, with: search_term
end
