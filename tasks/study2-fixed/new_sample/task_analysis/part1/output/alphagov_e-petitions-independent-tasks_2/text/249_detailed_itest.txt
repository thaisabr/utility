Classes: 4
[name:FactoryGirl, file:null, step:Given ]
[name:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:null]
[name:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:null]
[name:Petition, file:alphagov_e-petitions/app/models/staged/petition_creator/petition.rb, step:null]

Methods: 27
[name:check, type:PetitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:null]
[name:check, type:PetitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:null]
[name:check, type:PetitionsController, file:alphagov_e-petitions/app/controllers/petitions_controller.rb, step:null]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_css, type:Object, file:null, step:Then ]
[name:have_link, type:Object, file:null, step:Then ]
[name:have_no_css, type:Object, file:null, step:Then ]
[name:index, type:PagesController, file:alphagov_e-petitions/app/controllers/pages_controller.rb, step:null]
[name:index, type:PetitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:null]
[name:index, type:PetitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:null]
[name:index, type:PetitionsController, file:alphagov_e-petitions/app/controllers/petitions_controller.rb, step:null]
[name:index, type:Archived/petitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:null]
[name:new, type:PetitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:null]
[name:new, type:PetitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:null]
[name:new, type:PetitionsController, file:alphagov_e-petitions/app/controllers/petitions_controller.rb, step:null]
[name:page, type:Object, file:null, step:Then ]
[name:rejected?, type:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:null]
[name:rejected?, type:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:null]
[name:rejected?, type:Petition, file:alphagov_e-petitions/app/models/staged/petition_creator/petition.rb, step:null]
[name:show, type:PetitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:null]
[name:show, type:PetitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:null]
[name:show, type:PetitionsController, file:alphagov_e-petitions/app/controllers/petitions_controller.rb, step:null]
[name:show, type:Archived/petitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:null]
[name:times, type:Object, file:null, step:Given ]
[name:to, type:Object, file:null, step:Then ]
[name:to_not, type:Object, file:null, step:Then ]
[name:within, type:Object, file:null, step:Then ]

Referenced pages: 20
alphagov_e-petitions/app/views/archived/petitions/index.html.erb
alphagov_e-petitions/app/views/archived/petitions/show.html.erb
alphagov_e-petitions/app/views/pages/_home_debated_petitions.html.erb
alphagov_e-petitions/app/views/pages/_home_responded_petitions.html.erb
alphagov_e-petitions/app/views/pages/index.html.erb
alphagov_e-petitions/app/views/petitions/_action_help.html.erb
alphagov_e-petitions/app/views/petitions/_closed_petition_show.html.erb
alphagov_e-petitions/app/views/petitions/_open_petition_show.html.erb
alphagov_e-petitions/app/views/petitions/_rejected_petition_show.html.erb
alphagov_e-petitions/app/views/petitions/_threshold_details.html.erb
alphagov_e-petitions/app/views/petitions/_trending_petition.html.erb
alphagov_e-petitions/app/views/petitions/check.html.erb
alphagov_e-petitions/app/views/petitions/check_results.html.erb
alphagov_e-petitions/app/views/petitions/index.html.erb
alphagov_e-petitions/app/views/petitions/new.html.erb
alphagov_e-petitions/app/views/petitions/search/_filter_nav.html.erb
alphagov_e-petitions/app/views/petitions/search/_result_list_footer.html.erb
alphagov_e-petitions/app/views/petitions/search/_results.html.erb
alphagov_e-petitions/app/views/petitions/search/_results_for_pre_create_petition.html.erb
alphagov_e-petitions/app/views/petitions/show.html.erb

