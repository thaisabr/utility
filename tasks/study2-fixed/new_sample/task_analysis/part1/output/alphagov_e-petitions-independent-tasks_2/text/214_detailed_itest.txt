Classes: 6
[name:FactoryGirl, file:null, step:Given ]
[name:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:Given ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:Given ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/petition_creator/petition.rb, step:Given ]
[name:Site, file:alphagov_e-petitions/app/models/site.rb, step:Given ]
[name:Site, file:alphagov_e-petitions/spec/support/site.rb, step:Given ]

Methods: 9
[name:capture_model, type:Object, file:null, step:Then ]
[name:current_path, type:Object, file:null, step:Then ]
[name:eq, type:Object, file:null, step:Then ]
[name:from_now, type:Object, file:null, step:Given ]
[name:maximum_number_of_sponsors, type:Site, file:alphagov_e-petitions/app/models/site.rb, step:Given ]
[name:maximum_number_of_sponsors, type:Site, file:alphagov_e-petitions/spec/support/site.rb, step:Given ]
[name:model!, type:Object, file:null, step:Then ]
[name:sponsor_token, type:Object, file:null, step:When ]
[name:to, type:Object, file:null, step:Then ]

Referenced pages: 0

