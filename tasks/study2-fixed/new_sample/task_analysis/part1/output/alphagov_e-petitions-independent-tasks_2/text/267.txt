Feature: Moderator updates petition scheduled debate date
Scenario: Updating petition scheduled debate date
Given an open petition "More money for charities" with some signatures
And I am logged in as a moderator
When I view all petitions
And I follow "More money for charities"
And I follow "Proposed debate date"
And I fill in "Scheduled debate date" with "06/12/2015"
And I press "Email 6 signatures"
Then I should see "Email will be sent overnight"
And the petition creator should have been emailed about the scheduled debate
And all the signatories of the petition should have been emailed about the scheduled debate
Feature: Moderator respond to petition
In order to allow or prevent the signing of petitions
As a moderator
I want to respond to a petition, editing, accepting, rejecting or re-assigning it
Scenario: Moderator edits petition before publishing
Given I am logged in as a moderator
And I visit a sponsored petition with action: "wee need to save our plaanet", that has background: "Reduce polootion" and additional details: "Enforce Kyotoe protocol in more countries"
And I follow "Edit petition"
Then I am on the admin petition edit details page for "wee need to save our plaanet"
And the markup should be valid
And the "Action" field should contain "wee need to save our plaanet"
And the "Background" field should contain "Reduce polootion"
And the "Additional details" field should contain "Enforce Kyotoe protocol in more countries"
Then I fill in "Action" with "We need to save our planet"
And I fill in "Background" with "Reduce pollution"
And I fill in "Additional details" with "Enforce Kyoto Protocol in more countries"
And I press "Save"
Then I am on the admin petition page for "We need to save our planet"
And I should see "We need to save our planet"
And I should see "Reduce pollution"
And I should see "Enforce Kyoto Protocol in more countries"
Scenario: Moderator edits and tries to save an invalid petition
Given I am logged in as a moderator
And I visit a sponsored petition with action: "wee need to save our plaanet", that has background: "Reduce polootion" and additional details: "Enforce Kyotoe protocol in more countries"
And I follow "Edit petition"
Then I fill in "Action" with ""
And I fill in "Background" with ""
And I fill in "Additional details" with ""
And I press "Save"
Then I should see "Action must be completed"
And I should see "Background must be completed"
Scenario: Moderator cancel editing petition
Given I am logged in as a moderator
And I visit a sponsored petition with action: "Blah", that has background: "Blah" and additional details: "Blah"
And I follow "Edit petition"
Then I am on the admin petition edit details page for "Blah"
When I follow "Cancel"
Then I am on the admin petition page for "Blah"
Feature: Providing debate outcome information
In order to keep petition supporters up-to-date on what parliament said about their petition
As an admin user
I want to store information about debates on the petition
Scenario: Adding debate outcome infromation
Given an open petition "Ban Badger Baiting" with some signatures
And I am logged in as a sysadmin
When I am on the admin all petitions page
And I follow "Ban Badger Baiting"
And I follow "Debate outcome"
Then I should be on the admin debate outcomes form page for "Ban Badger Baiting"
And the markup should be valid
When I press "Email 6 signatures"
Then the petition should not have debate details
And I should see an error
When I fill in the debate outcome details
And press "Email 6 signatures"
Then the petition should have the debate details I provided
And the petition creator should have been emailed about the debate
And all the signatories of the petition should have been emailed about the debate
