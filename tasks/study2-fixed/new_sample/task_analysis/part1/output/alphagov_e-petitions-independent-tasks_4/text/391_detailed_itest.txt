Classes: 10
[name:ConstituencyApi, file:alphagov_e-petitions/app/lib/constituency_api.rb, step:Given ]
[name:FactoryGirl, file:null, step:When ]
[name:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:Then ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:Then ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/petition_creator/petition.rb, step:Then ]
[name:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:When ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:When ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/petition_creator/petition.rb, step:When ]
[name:Signature, file:alphagov_e-petitions/app/models/signature.rb, step:Then ]
[name:Signature, file:alphagov_e-petitions/app/models/staged/base/signature.rb, step:Then ]

Methods: 57
[name:be_present, type:Object, file:null, step:When ]
[name:be_sponsor, type:Object, file:null, step:When ]
[name:be_truthy, type:Object, file:null, step:Then ]
[name:blank?, type:Object, file:null, step:Given ]
[name:capture_fields, type:Object, file:null, step:Then ]
[name:capture_model, type:Object, file:null, step:Then ]
[name:check, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_first_link_in_email, type:Object, file:null, step:When ]
[name:click_on, type:Object, file:null, step:When ]
[name:create_model, type:Object, file:null, step:Then ]
[name:creator_signature, type:Object, file:null, step:Then ]
[name:eq, type:Object, file:null, step:Then ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find_by, type:Signature, file:alphagov_e-petitions/app/models/signature.rb, step:Then ]
[name:find_by, type:Signature, file:alphagov_e-petitions/app/models/staged/base/signature.rb, step:Then ]
[name:first, type:Object, file:null, step:When ]
[name:for_email, type:Sponsor, file:alphagov_e-petitions/app/models/sponsor.rb, step:When ]
[name:from_now, type:Object, file:null, step:When ]
[name:gsub, type:Object, file:null, step:Given ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:Given ]
[name:have_field, type:Object, file:null, step:Then ]
[name:have_no_content, type:Object, file:null, step:Given ]
[name:have_no_title, type:Object, file:null, step:Given ]
[name:have_text, type:Object, file:null, step:Then ]
[name:have_title, type:Object, file:null, step:Given ]
[name:have_xpath, type:Object, file:null, step:Given ]
[name:model!, type:Object, file:null, step:Then ]
[name:new, type:PetitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:Given ]
[name:new, type:PetitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:Given ]
[name:new, type:PetitionsController, file:alphagov_e-petitions/app/controllers/petitions_controller.rb, step:Given ]
[name:notify_by_email?, type:Object, file:null, step:Then ]
[name:open_email, type:Object, file:null, step:When ]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:Given ]
[name:raise, type:Object, file:null, step:Given ]
[name:reload, type:Object, file:null, step:Then ]
[name:select, type:Object, file:null, step:Given ]
[name:show, type:SponsorsController, file:alphagov_e-petitions/app/controllers/sponsors_controller.rb, step:null]
[name:signatures, type:Object, file:null, step:When ]
[name:sponsor_token, type:Object, file:null, step:When ]
[name:state, type:Object, file:null, step:Then ]
[name:state, type:PetitionSearch, file:alphagov_e-petitions/app/decorators/petition_search.rb, step:Then ]
[name:stub_request, type:Object, file:null, step:Given ]
[name:to, type:Object, file:null, step:Then ]
[name:to, type:Object, file:null, step:When ]
[name:to, type:Object, file:null, step:Given ]
[name:to_return, type:Object, file:null, step:Given ]
[name:to_s, type:Object, file:null, step:Given ]
[name:with_scope, type:WebSteps, file:alphagov_e-petitions/features/step_definitions/web_steps.rb, step:Given ]
[name:with_scope, type:WebSteps, file:alphagov_e-petitions/features/step_definitions/web_steps.rb, step:When ]
[name:within, type:Object, file:null, step:Given ]
[name:within_section, type:Sections, file:alphagov_e-petitions/features/support/sections.rb, step:Given ]
[name:xpath_of_section, type:Sections, file:alphagov_e-petitions/features/support/sections.rb, step:Given ]

Referenced pages: 2
alphagov_e-petitions/app/views/petitions/new.html.erb
alphagov_e-petitions/app/views/sponsors/show.html.erb

