Feature: Joe searches for an archived petition
In order to see what petitions were created in the past
As Joe, a member of the general public
I want to be able to search archived petitions
Background:
Given the following archived petitions exist:
| title                    | state    | signature_count| opened_at  | closed_at  | created_at |
| Wombles are great        | open     | 835            | 2012-01-01 | 2013-01-01 | 2012-01-01 |
| The Wombles of Wimbledon | open     | 243            | 2011-04-01 | 2012-04-01 | 2011-04-01 |
| Common People            | open     | 639            | 2014-10-01 | 2015-03-31 | 2014-10-01 |
| Eavis vs the Wombles     | rejected |                |            |            | 2011-01-01 |
Scenario: Searching for petitions
When I go to the archived petitions page
And I fill in "search" with "Wombles"
And I press "Search"
Then I should be on the archived petitions search results page
And I should see "Search results - archived e-petitions" in the browser page title
And I should see /for "Wombles"/
But I should see the following search results table:
| Eavis vs the Wombles (Rejected)   | –   | –          |
| The Wombles of Wimbledon (Closed) | 243 | 01/04/2012 |
| Wombles are great (Closed)        | 835 | 01/01/2013 |
And the search results table should have the caption /Archived e-petitions/
And the markup should be valid
Scenario: Paging through archived petitions
Given 21 archived petitions exist with title: "International development spending"
When I go to the archived petitions page
And I fill in "search" with "spending"
And I press "Search"
Then I should be on the archived petitions search results page
And I should see 20 petitions
Then I follow "Next" within ".//*[contains(@class, 'title_pagination_row')]"
And I should see 1 petition
Then I follow "Previous" within ".//*[contains(@class, 'title_pagination_row')]"
And I should see 20 petitions
Feature: Joe views an archived petition
In order to see what petitions were created in the past
As Joe, a member of the general public
I want to be able to view archived petitions
Scenario: Joe views an archived petition
Given an archived petition "Spend more money on Defence"
When I view the petition
Then I should see the petition details
And I should see "Spend more money on Defence - e-petitions" in the browser page title
And I should see the vote count, closed and open dates
And I should see "This e-petition is now closed"
Scenario: Joe views an archived petition at the old url and is redirected
Given an archived petition "Spend more money on Defence"
When I view the petition at the old url
Then I should be redirected to the archived url
And I should see "Spend more money on Defence - e-petitions" in the browser page title
And I should see the vote count, closed and open dates
And I should see "This e-petition is now closed"
Scenario: Joe views an archived petition containing urls, email addresses and html tags
Given an archived petition exists with title: "Defence review", description: "<i>We<i> like http://www.google.com and bambi@gmail.com"
When I go to the archived petition page for "Defence review"
And I should see "<i>We<i>"
And I should see a link called "http://www.google.com" linking to "http://www.google.com"
And I should see a link called "bambi@gmail.com" linking to "mailto:bambi@gmail.com"
Scenario: Joe sees reason for rejection if appropriate
Given an archived petition "Please bring back Eldorado" has been rejected with the reason "<i>We<i> like http://www.google.com and bambi@gmail.com"
When I view the petition
Then I should see the petition details
And I should see the reason for rejection
And I should see "<i>We<i>"
And I should see a link called "http://www.google.com" linking to "http://www.google.com"
And I should see a link called "bambi@gmail.com" linking to "mailto:bambi@gmail.com"
And I cannot sign the petition
Scenario: Joe cannot sign an archived petition
Given an archived petition "Spend more money on Defence"
When I view the petition
Then I should see the petition details
And I cannot sign the petition
Scenario: Joe sees a 'closed' message when viewing an archived petition
Given an archived petition "Spend more money on Defence"
When I view the petition
Then I should see "This e-petition is now closed"
