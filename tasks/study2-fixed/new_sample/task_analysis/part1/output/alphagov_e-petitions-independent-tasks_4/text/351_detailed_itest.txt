Classes: 9
[name:FactoryGirl, file:null, step:And ]
[name:FactoryGirl, file:null, step:Given ]
[name:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:And ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:And ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/petition_creator/petition.rb, step:And ]
[name:Rails, file:null, step:Given ]
[name:Site, file:alphagov_e-petitions/app/models/site.rb, step:Given ]
[name:Site, file:alphagov_e-petitions/app/models/site.rb, step:And ]
[name:Site, file:alphagov_e-petitions/app/models/site.rb, step:Then ]

Methods: 69
[name:be_present, type:Object, file:null, step:Given ]
[name:be_sponsor, type:Object, file:null, step:Given ]
[name:build, type:Object, file:null, step:And ]
[name:check, type:PetitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:null]
[name:check, type:PetitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:null]
[name:check, type:PetitionsController, file:alphagov_e-petitions/app/controllers/petitions_controller.rb, step:null]
[name:check, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_first_link_in_email, type:Object, file:null, step:Given ]
[name:click_on, type:Object, file:null, step:Given ]
[name:count, type:Object, file:null, step:Then ]
[name:default_part_body, type:Object, file:null, step:Then ]
[name:driver, type:Object, file:null, step:Given ]
[name:each, type:Browseable, file:alphagov_e-petitions/app/models/concerns/browseable.rb, step:Given ]
[name:eq, type:Object, file:null, step:Then ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:find_by, type:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:And ]
[name:find_by, type:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:And ]
[name:find_by, type:Petition, file:alphagov_e-petitions/app/models/staged/petition_creator/petition.rb, step:And ]
[name:first, type:Object, file:null, step:Given ]
[name:for_email, type:Sponsor, file:alphagov_e-petitions/app/models/sponsor.rb, step:Given ]
[name:help, type:StaticPagesController, file:alphagov_e-petitions/app/controllers/static_pages_controller.rb, step:null]
[name:home, type:StaticPagesController, file:alphagov_e-petitions/app/controllers/static_pages_controller.rb, step:null]
[name:include, type:Object, file:null, step:Then ]
[name:index, type:PetitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:null]
[name:index, type:PetitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:null]
[name:index, type:PetitionsController, file:alphagov_e-petitions/app/controllers/petitions_controller.rb, step:null]
[name:instance, type:Site, file:alphagov_e-petitions/app/models/site.rb, step:Given ]
[name:join, type:Object, file:null, step:Given ]
[name:match, type:Object, file:null, step:Then ]
[name:maximum_number_of_sponsors, type:Site, file:alphagov_e-petitions/app/models/site.rb, step:And ]
[name:maximum_number_of_sponsors, type:Site, file:alphagov_e-petitions/app/models/site.rb, step:Given ]
[name:new, type:PetitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:null]
[name:new, type:PetitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:null]
[name:new, type:PetitionsController, file:alphagov_e-petitions/app/controllers/petitions_controller.rb, step:null]
[name:not, type:Object, file:null, step:Then ]
[name:not_to, type:Object, file:null, step:Then ]
[name:open_email, type:Object, file:null, step:Given ]
[name:open_last_email_for, type:Object, file:null, step:Then ]
[name:options, type:Object, file:null, step:Given ]
[name:page, type:Object, file:null, step:Given ]
[name:page, type:Object, file:null, step:Then ]
[name:read_emails_for, type:Object, file:null, step:Given ]
[name:select, type:Object, file:null, step:Given ]
[name:show, type:PetitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:null]
[name:show, type:PetitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:null]
[name:show, type:PetitionsController, file:alphagov_e-petitions/app/controllers/petitions_controller.rb, step:null]
[name:show, type:SponsorsController, file:alphagov_e-petitions/app/controllers/sponsors_controller.rb, step:null]
[name:signatures, type:Object, file:null, step:Given ]
[name:sponsor_token, type:Object, file:null, step:Given ]
[name:sponsors, type:Object, file:null, step:And ]
[name:sponsors, type:Object, file:null, step:Then ]
[name:status_code, type:Object, file:null, step:Then ]
[name:stub_constituency_from_file, type:Object, file:null, step:Given ]
[name:stub_no_constituencies, type:Object, file:null, step:Given ]
[name:subject, type:Object, file:null, step:Then ]
[name:supporting_sponsors_count, type:Object, file:null, step:Given ]
[name:threshold_for_moderation, type:Site, file:alphagov_e-petitions/app/models/site.rb, step:Given ]
[name:threshold_for_moderation, type:Site, file:alphagov_e-petitions/app/models/site.rb, step:Then ]
[name:times, type:Object, file:null, step:And ]
[name:title, type:Object, file:null, step:Then ]
[name:to, type:Object, file:null, step:Then ]
[name:to, type:Object, file:null, step:Given ]
[name:to_s, type:Object, file:null, step:Then ]
[name:unread_emails_for, type:Object, file:null, step:Given ]
[name:update!, type:Object, file:null, step:Given ]
[name:where, type:Object, file:null, step:Then ]
[name:with_scope, type:WebSteps, file:alphagov_e-petitions/features/step_definitions/web_steps.rb, step:Given ]
[name:within, type:Object, file:null, step:Given ]

Referenced pages: 19
alphagov_e-petitions/app/views/local_petitions/_search.html.erb
alphagov_e-petitions/app/views/petitions/_action_help.html.erb
alphagov_e-petitions/app/views/petitions/_trending_petition.html.erb
alphagov_e-petitions/app/views/petitions/check.html.erb
alphagov_e-petitions/app/views/petitions/check_results.html.erb
alphagov_e-petitions/app/views/petitions/index.html.erb
alphagov_e-petitions/app/views/petitions/new.html.erb
alphagov_e-petitions/app/views/petitions/show.html.erb
alphagov_e-petitions/app/views/search/_filter_nav.html.erb
alphagov_e-petitions/app/views/search/_result_list_footer.html.erb
alphagov_e-petitions/app/views/search/_results.html.erb
alphagov_e-petitions/app/views/search/_results_for_pre_create_petition.html.erb
alphagov_e-petitions/app/views/search/_results_table.html.erb
alphagov_e-petitions/app/views/shared/_about_petitions.html.erb
alphagov_e-petitions/app/views/shared/_notification.html.erb
alphagov_e-petitions/app/views/shared/_share_petition.html.erb
alphagov_e-petitions/app/views/sponsors/show.html.erb
alphagov_e-petitions/app/views/static_pages/help.html.erb
alphagov_e-petitions/app/views/static_pages/home.html.erb

