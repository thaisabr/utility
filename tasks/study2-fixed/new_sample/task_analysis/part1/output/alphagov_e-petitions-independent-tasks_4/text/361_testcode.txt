Given(/^#{capture_model} exists?(?: with #{capture_fields})?$/) do |name, fields|
  create_model(name, fields)
end
Given(/^the following #{capture_plural_factory} exists?:?$/) do |plural_factory, table|
  create_models_from_table(plural_factory, table)
end
Then(/^#{capture_model} should be #{capture_model}$/) do |a, b|
  expect(model!(a)).to eq model!(b)
end
When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    click_button(button)
  end
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
When /^(?:|I )fill in "([^"]*)" with "([^"]*)"(?: within "([^"]*)")?$/ do |field, value, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)
    case page_name

    when /^the home\s?page$/
      '/'

    when /^the help page$/
      help_path

    when /^the feedback page$/
      feedback_path

    when /^the new petition page$/
      new_petition_path

    when /^the petition page for "([^\"]*)"$/
      petition_path(Petition.find_by(title: $1))

    when /^the archived petitions page$/
      archived_petitions_path

    when /^the archived petitions search results page$/
      search_archived_petitions_path

    when /^the archived petition page for "([^\"]*)"$/
      archived_petition_path(ArchivedPetition.find_by(title: $1))

    when /^the new signature page for "([^\"]*)"$/
      new_petition_signature_path(Petition.find_by(title: $1))

    when /^the search results page$/
      search_path

    when /^the Admin (.*)$/i
      admin_path($1)

    else
      begin
        page_name =~ /^the (.*) page$/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue NoMethodError, ArgumentError
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Then /^I should see (\d+) petitions?$/ do |number|
  expect(page).to have_xpath( "//ol[count(li)=#{number.to_i}]" )
end
Then /^I should be asked to search for a new petition$/ do
  expect(page).to have_css("form input[name=q]")
end
