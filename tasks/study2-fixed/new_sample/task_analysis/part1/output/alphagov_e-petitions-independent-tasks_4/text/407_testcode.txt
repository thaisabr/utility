When(/^I am allowed to make the petition title too long$/) do
  # NOTE: we do this to remove the maxlength attribtue on the petition
  # title input because any modern browser/driver will not let us enter
  # values longer than maxlength and so we can't test our JS validation
  page.execute_script "$('#petition_title').attr('maxlength', '');"
end
When /^I fill in sponsor emails$/ do
  fill_in "Sponsor emails", :with => "test1@test.com\ntest2@test.com\ntest3@test.com\ntest4@test.com\ntest5@test.com"
end
When /^I start a new petition/ do
  steps %Q(
    Given I am on the new petition page
    Then I should see "Create a new e-petition - e-petitions" in the browser page title
    And I should be connected to the server via an ssl connection
  )
end
When /^I fill in the petition details/ do
  steps %Q(
    When I fill in "Title" with "The wombats of wimbledon rock."
    And I fill in "Action" with "Give half of Wimbledon rock to wombats!"
    And I fill in "Description" with "The racial tensions between the wombles and the wombats are heating up. Racial attacks are a regular occurrence and the death count is already in 5 figures. The only resolution to this crisis is to give half of Wimbledon common to the Wombats and to recognise them as their own independent state."
  )
end
Then /^I should see my constituency "([^"]*)"/ do |constituency|
  expect(page).to have_text(constituency)  
end
Then /^I should not see the text "([^"]*)"/ do |text|
  expect(page).to_not have_text(text)  
end
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    click_button(button)
  end
end
When /^(?:|I )fill in "([^"]*)" with "([^"]*)"(?: within "([^"]*)")?$/ do |field, value, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    expect(page).to have_content(text)
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)
    case page_name

    when /^the home\s?page$/
      '/'

    when /^the accessibility page$/
      accessibility_path

    when /^the how e-Petitions works page$/
      how_it_works_path

    when /^the terms and conditions page$/
      terms_and_conditions_path

    when /^the cookies page$/
      cookies_path

    when /^the privacy policy page$/
      privacy_policy_path

    when /^the feedback page$/
      feedback_path

    when /^the new petition page$/
      new_petition_path

    when /^the petition page for "([^\"]*)"$/
      petition_path(Petition.find_by(title: $1))

    when /^the new signature page for "([^\"]*)"$/
      new_petition_signature_path(Petition.find_by(title: $1))

    when /^the search results page$/
      search_path

    when /^the Admin (.*)$/i
      admin_path($1)

    else
      begin
        page_name =~ /^the (.*) page$/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue NoMethodError, ArgumentError
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
When /^I confirm my email address$/ do
  steps %Q(
    And I open the email with text "confirm your email address"
    When I click the first link in the email
    Then I should see "Thank you"
  )
end
When(/^I fill in my details(?: with email "([^"]+)")?$/) do |email_address|
  email_address ||= "womboid@wimbledon.com"
  steps %Q(
    When I fill in "Name" with "Womboid Wibbledon"
    And I fill in "Email" with "#{email_address}"
    And I check "Yes, I am a British citizen or UK resident"
    And I fill in my postcode with "SW14 9RQ"
    And I select "United Kingdom" from "Country"
  )
end
When(/^I fill in my details with postcode "(.*?)"?$/) do |postcode|
  steps %Q(
    When I fill in "Name" with "Womboid Wibbledon"
    And I fill in "Email" with "womboid@wimbledon.com"
    And I check "Yes, I am a British citizen or UK resident"
    And I fill in my postcode with "#{postcode}"
    And I select "United Kingdom" from "Country"
  )
end
When(/^I fill in my postcode with "(.*?)"$/) do |postcode|
  step %{I fill in "Postcode" with "#{postcode}"}

  api_url = "http://data.parliament.uk/membersdataplatform/services/mnis/Constituencies"
  body = "<Constituencies/>"
  if postcode == "N1 1TY"
    body = "<Constituencies>
              <Constituency><Name>Islington South and Finsbury</Name></Constituency>
            </Constituencies>"
  end
  stub_request(:get, "#{ api_url }/#{postcode.gsub(/\s+/, '')}/").to_return(status: 200, body: body)
end
Then /^"([^"]*)" wants to be notified about the petition's progress$/ do |name|
  expect(Signature.find_by(name: name).notify_by_email?).to be_truthy
end
Given(/^#{capture_model} exists?(?: with #{capture_fields})?$/) do |name, fields|
  create_model(name, fields)
end
Then(/^#{capture_model} should be #{capture_model}$/) do |a, b|
  expect(model!(a)).to eq model!(b)
end
Then /^(?:I|they|"([^"]*?)") should receive (an|no|\d+) emails?$/ do |address, amount|
  expect(unread_emails_for(address).size).to eq parse_email_count(amount)
end
Then(/^I should see a fieldset called "(.*?)"$/) do |legend|
  expect(page).to have_xpath("//fieldset/legend[contains(., '#{legend}')]", visible: false)
end
When /^(?:|I )fill in "([^"]*)" with "([^"]*)"(?: within "([^"]*)")?$/ do |field, value, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
When /^(?:|I )select "([^"]*)" from "([^"]*)"(?: within "([^"]*)")?$/ do |value, field, selector|
  with_scope(selector) do
    select(value, :from => field)
  end
end
When /^(?:|I )check "([^"]*)"(?: within "([^"]*)")?$/ do |field, selector|
  with_scope(selector) do
    check(field)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    expect(page).to have_content(text)
  end
end
Then /^I should (not |)see "([^\"]*)" in the ((?!email\b).*)$/ do |see_or_not, text, section_name|
  if section_name == 'browser page title'
    if see_or_not.blank?
      expect(page).to have_title(text.to_s)
    else
      expect(page).to have_no_title(text.to_s)
    end
  else
    within_section(section_name) do
      if see_or_not.blank?
        expect(page).to have_content(text.to_s)
      else
        expect(page).to have_no_content(text.to_s)
      end
    end
  end
end
  def within_section(section_name)
    within xpath_of_section(section_name) do
      yield
    end
  end
  def xpath_of_section(section_name, prefix = "//")
    case section_name

    # Non site-specific based
    when /"([^\"]*)" fieldset/
      "#{prefix}fieldset[contains(@class, '#{$1.downcase.gsub(/\s/, '_')}')]"

    # Sitewide
    when /^single h1$/
      expect(page).to have_xpath("//h1", :count => 1)
      "#{prefix}h1"

    when 'search results table'
      "#{prefix}div[contains(@class, 'petition_list')]//table"

    # Admin specific
    when 'the admin nav'
      "//*[@id='admin_nav']"

    when 'admin index table'
      "#{prefix}table[contains(@class, 'admin_index')]"

    when 'admin report table'
      "#{prefix}table[contains(@class, 'admin_report')]"

    else
      raise "Can't find mapping from \"#{section_name}\" to a section."
    end
  end
When /^(?:I|they|"([^"]*?)") opens? the email with text "([^"]*?)"$/ do |address, text|
  open_email(address, :with_text => text)
end
When /^(?:I|they) click the first link in the email$/ do
  click_first_link_in_email
end
When(/^I fill in my postcode with "(.*?)"$/) do |postcode|
  step %{I fill in "Postcode" with "#{postcode}"}

  api_url = "http://data.parliament.uk/membersdataplatform/services/mnis/Constituencies"
  body = "<Constituencies/>"
  if postcode == "N1 1TY"
    body = "<Constituencies>
              <Constituency><Name>Islington South and Finsbury</Name></Constituency>
            </Constituencies>"
  end
  stub_request(:get, "#{ api_url }/#{postcode.gsub(/\s+/, '')}/").to_return(status: 200, body: body)
end
