When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)
    case page_name

    when /^the home\s?page$/
      '/'

    when /^the how e-Petitions works page$/
      how_it_works_path

    when /^the help page$/
      help_path

    when /^the feedback page$/
      feedback_path

    when /^the new petition page$/
      new_petition_path

    when /^the petition page for "([^\"]*)"$/
      petition_path(Petition.find_by(title: $1))

    when /^the new signature page for "([^\"]*)"$/
      new_petition_signature_path(Petition.find_by(title: $1))

    when /^the search results page$/
      search_path

    when /^the Admin (.*)$/i
      admin_path($1)

    else
      begin
        page_name =~ /^the (.*) page$/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue NoMethodError, ArgumentError
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Then(/^#{capture_model} should be #{capture_model}$/) do |a, b|
  expect(model!(a)).to eq model!(b)
end
Then /^I should (not |)see "([^\"]*)" in the ((?!email\b).*)$/ do |see_or_not, text, section_name|
  if section_name == 'browser page title'
    if see_or_not.blank?
      expect(page).to have_title(text.to_s)
    else
      expect(page).to have_no_title(text.to_s)
    end
  else
    within_section(section_name) do
      if see_or_not.blank?
        expect(page).to have_content(text.to_s)
      else
        expect(page).to have_no_content(text.to_s)
      end
    end
  end
end
  def within_section(section_name)
    within xpath_of_section(section_name) do
      yield
    end
  end
  def xpath_of_section(section_name, prefix = "//")
    case section_name

    # Non site-specific based
    when /"([^\"]*)" fieldset/
      "#{prefix}fieldset[contains(@class, '#{$1.downcase.gsub(/\s/, '_')}')]"

    # Sitewide
    when /^single h1$/
      expect(page).to have_xpath("//h1", :count => 1)
      "#{prefix}h1"

    when 'search results table'
      "#{prefix}div[contains(@class, 'petition_list')]//table"

    # Admin specific
    when 'the admin nav'
      "//*[@id='admin_nav']"

    when 'admin index table'
      "#{prefix}table[contains(@class, 'admin_index')]"

    when 'admin report table'
      "#{prefix}table[contains(@class, 'admin_report')]"

    else
      raise "Can't find mapping from \"#{section_name}\" to a section."
    end
  end
