When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)
    case page_name

    when /^the home\s?page$/
      '/'

    when /^the help page$/
      help_path

    when /^the feedback page$/
      feedback_path

    when /^the new petition page$/
      new_petition_path

    when /^the petition page for "([^\"]*)"$/
      petition_path(Petition.find_by(title: $1))

    when /^the archived petitions page$/
      archived_petitions_path

    when /^the archived petitions search results page$/
      search_archived_petitions_path

    when /^the archived petition page for "([^\"]*)"$/
      archived_petition_path(ArchivedPetition.find_by(title: $1))

    when /^the new signature page for "([^\"]*)"$/
      new_petition_signature_path(Petition.find_by(title: $1))

    when /^the search results page$/
      search_path

    when /^the Admin (.*)$/i
      admin_path($1)

    else
      begin
        page_name =~ /^the (.*) page$/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue NoMethodError, ArgumentError
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Then /^I should see the following admin index table:$/ do |values_table|
  actual_table = find(:css, 'table').all(:css, 'tr').map { |row| row.all(:css, 'th, td').map { |cell| cell.text.strip } }
  values_table.diff!(actual_table)
end
Then /^I should see (\d+) rows? in the admin index table$/ do |number|
  expect(page).to have_xpath( "//table[@class='admin_index' and count(tr)=#{number.to_i + 1}]" )
end
Given(/^#{capture_model} exists?(?: with #{capture_fields})?$/) do |name, fields|
  create_model(name, fields)
end
Then(/^#{capture_model} should be #{capture_model}$/) do |a, b|
  expect(model!(a)).to eq model!(b)
end
Given /^I am logged in as a moderator$/ do
  @user = FactoryGirl.create(:moderator_user)
  step "the admin user is logged in"
end
Given /^the petition "([^"]*)" has (\d+) validated signatures$/ do |title, no_validated|
  petition = Petition.find_by(title: title)
  (no_validated - 1).times { petition.signatures << FactoryGirl.create(:validated_signature) }
end
And (/^the petition "([^"]*)" has reached maximum amount of sponsors$/) do |title|
  petition = Petition.find_by(title: title)
  AppConfig.sponsor_count_max.times { petition.sponsors.build(FactoryGirl.attributes_for(:sponsor)) }
end
And (/^the petition "([^"]*)" has (\d+) pending sponsors$/) do |title, sponsors|
  petition = Petition.find_by(title: title)
  sponsors.times { petition.sponsors.build(FactoryGirl.attributes_for(:sponsor)) }
end
When /^I select the option to view "([^"]*)" petitions$/ do |option|
  choose option
  click_button "Go"
end
Given /^the admin user is logged in$/ do
  visit admin_login_path
  fill_in("Email", :with => @user.email)
  fill_in("Password", :with => "Letmein1!")
  click_button("Log in")
end
