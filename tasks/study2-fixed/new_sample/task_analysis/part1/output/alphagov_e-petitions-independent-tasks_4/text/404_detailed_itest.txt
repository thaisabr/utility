Classes: 14
[name:ArchivedPetition, file:alphagov_e-petitions/app/models/archived_petition.rb, step:When ]
[name:ArchivedPetition, file:alphagov_e-petitions/app/models/archived_petition.rb, step:Then ]
[name:FactoryGirl, file:null, step:Given ]
[name:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:Given ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:Given ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/petition_creator/petition.rb, step:Given ]
[name:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:When ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:When ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/petition_creator/petition.rb, step:When ]
[name:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:Then ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:Then ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/petition_creator/petition.rb, step:Then ]
[name:Regexp, file:null, step:Then ]
[name:UserSessionsController, file:alphagov_e-petitions/app/controllers/admin/user_sessions_controller.rb, step:null]

Methods: 94
[name:blank?, type:Object, file:null, step:Then ]
[name:capture_fields, type:Object, file:null, step:Given ]
[name:capture_model, type:Object, file:null, step:Given ]
[name:capture_model, type:Object, file:null, step:Then ]
[name:capture_plural_factory, type:Object, file:null, step:Given ]
[name:capture_predicate, type:Object, file:null, step:Then ]
[name:check, type:PetitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:When ]
[name:check, type:PetitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:When ]
[name:check, type:PetitionsController, file:alphagov_e-petitions/app/controllers/petitions_controller.rb, step:When ]
[name:check, type:PetitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:null]
[name:check, type:PetitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:null]
[name:check, type:PetitionsController, file:alphagov_e-petitions/app/controllers/petitions_controller.rb, step:null]
[name:check, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_link, type:Object, file:null, step:Then ]
[name:click_link, type:Object, file:null, step:Given ]
[name:create_model, type:Object, file:null, step:Given ]
[name:create_models_from_table, type:Object, file:null, step:Given ]
[name:creator_signature, type:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:Then ]
[name:creator_signature, type:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:Then ]
[name:creator_signature, type:Petition, file:alphagov_e-petitions/app/models/staged/petition_creator/petition.rb, step:Then ]
[name:current_path, type:Object, file:null, step:Then ]
[name:each, type:Object, file:null, step:Then ]
[name:email, type:Object, file:null, step:Given ]
[name:email, type:Object, file:null, step:null]
[name:eq, type:Object, file:null, step:Then ]
[name:fill_in, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:gsub, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:When ]
[name:have_css, type:Object, file:null, step:Then ]
[name:have_no_content, type:Object, file:null, step:Then ]
[name:have_no_title, type:Object, file:null, step:Then ]
[name:have_title, type:Object, file:null, step:Then ]
[name:have_xpath, type:Object, file:null, step:Then ]
[name:have_xpath, type:Object, file:null, step:When ]
[name:help, type:StaticPagesController, file:alphagov_e-petitions/app/controllers/static_pages_controller.rb, step:When ]
[name:help, type:StaticPagesController, file:alphagov_e-petitions/app/controllers/static_pages_controller.rb, step:null]
[name:home, type:StaticPagesController, file:alphagov_e-petitions/app/controllers/static_pages_controller.rb, step:When ]
[name:home, type:StaticPagesController, file:alphagov_e-petitions/app/controllers/static_pages_controller.rb, step:null]
[name:how_it_works, type:StaticPagesController, file:alphagov_e-petitions/app/controllers/static_pages_controller.rb, step:When ]
[name:how_it_works, type:StaticPagesController, file:alphagov_e-petitions/app/controllers/static_pages_controller.rb, step:null]
[name:index, type:Archived/petitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:When ]
[name:index, type:PetitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:When ]
[name:index, type:PetitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:When ]
[name:index, type:PetitionsController, file:alphagov_e-petitions/app/controllers/petitions_controller.rb, step:When ]
[name:index, type:PetitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:null]
[name:index, type:PetitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:null]
[name:index, type:PetitionsController, file:alphagov_e-petitions/app/controllers/petitions_controller.rb, step:null]
[name:model!, type:Object, file:null, step:Then ]
[name:name, type:AdminUser, file:alphagov_e-petitions/app/models/admin_user.rb, step:Then ]
[name:name, type:Stages, file:alphagov_e-petitions/app/models/staged/petition_creator/stages.rb, step:Then ]
[name:name, type:Stages, file:alphagov_e-petitions/app/models/staged/petition_signer/stages.rb, step:Then ]
[name:new, type:PetitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:When ]
[name:new, type:PetitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:When ]
[name:new, type:PetitionsController, file:alphagov_e-petitions/app/controllers/petitions_controller.rb, step:When ]
[name:new, type:SignaturesController, file:alphagov_e-petitions/app/controllers/signatures_controller.rb, step:When ]
[name:new, type:SignaturesController, file:alphagov_e-petitions/app/controllers/signatures_controller.rb, step:null]
[name:new, type:PetitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:null]
[name:new, type:PetitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:null]
[name:new, type:PetitionsController, file:alphagov_e-petitions/app/controllers/petitions_controller.rb, step:null]
[name:new, type:Admin/userSessionsController, file:alphagov_e-petitions/app/controllers/admin/user_sessions_controller.rb, step:null]
[name:not_to, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:When ]
[name:password, type:Object, file:null, step:null]
[name:raise, type:Object, file:null, step:Then ]
[name:raw, type:Object, file:null, step:Then ]
[name:respond_to?, type:Object, file:null, step:Then ]
[name:send, type:Object, file:null, step:Then ]
[name:show, type:Archived/petitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:When ]
[name:show, type:PetitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:When ]
[name:show, type:PetitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:When ]
[name:show, type:PetitionsController, file:alphagov_e-petitions/app/controllers/petitions_controller.rb, step:When ]
[name:show, type:Archived/petitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:null]
[name:show, type:PetitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:null]
[name:show, type:PetitionsController, file:alphagov_e-petitions/app/controllers/archived/petitions_controller.rb, step:null]
[name:show, type:PetitionsController, file:alphagov_e-petitions/app/controllers/petitions_controller.rb, step:null]
[name:squish, type:Object, file:null, step:Then ]
[name:strftime, type:Object, file:null, step:Then ]
[name:to, type:Object, file:null, step:Then ]
[name:to, type:Object, file:null, step:When ]
[name:to_i, type:Object, file:null, step:Then ]
[name:to_s, type:Object, file:null, step:Then ]
[name:with_scope, type:WebSteps, file:alphagov_e-petitions/features/step_definitions/web_steps.rb, step:When ]
[name:with_scope, type:WebSteps, file:alphagov_e-petitions/features/step_definitions/web_steps.rb, step:Then ]
[name:with_scope, type:WebSteps, file:alphagov_e-petitions/features/step_definitions/web_steps.rb, step:Given ]
[name:within, type:Object, file:null, step:When ]
[name:within, type:Object, file:null, step:Then ]
[name:within, type:Object, file:null, step:Given ]
[name:within_section, type:Sections, file:alphagov_e-petitions/features/support/sections.rb, step:Then ]
[name:xpath_of_section, type:Sections, file:alphagov_e-petitions/features/support/sections.rb, step:Then ]

Referenced pages: 20
alphagov_e-petitions/app/views/admin/user_sessions/new.html.erb
alphagov_e-petitions/app/views/archived/petitions/index.html.erb
alphagov_e-petitions/app/views/archived/petitions/show.html.erb
alphagov_e-petitions/app/views/petitions/_pre_creation_search_form.html.erb
alphagov_e-petitions/app/views/petitions/_trending_petition.html.erb
alphagov_e-petitions/app/views/petitions/check.html.erb
alphagov_e-petitions/app/views/petitions/check_results.html.erb
alphagov_e-petitions/app/views/petitions/index.html.erb
alphagov_e-petitions/app/views/petitions/new.html.erb
alphagov_e-petitions/app/views/petitions/show.html.erb
alphagov_e-petitions/app/views/search/_result_list_footer.html.erb
alphagov_e-petitions/app/views/search/_result_list_header.html.erb
alphagov_e-petitions/app/views/search/_result_tabs.html.erb
alphagov_e-petitions/app/views/search/_results.html.erb
alphagov_e-petitions/app/views/search/_results_for_pre_create_petition.html.erb
alphagov_e-petitions/app/views/search/_results_table.html.erb
alphagov_e-petitions/app/views/signatures/new.html.erb
alphagov_e-petitions/app/views/static_pages/help.html.erb
alphagov_e-petitions/app/views/static_pages/home.html.erb
alphagov_e-petitions/app/views/static_pages/how_it_works.html.erb

