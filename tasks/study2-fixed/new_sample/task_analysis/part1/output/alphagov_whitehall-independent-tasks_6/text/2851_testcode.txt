When /^I import the following data as CSV as "([^"]*)" for "([^"]*)":$/ do |document_type, organisation_name, data|
  organisation = Organisation.find_by_name(organisation_name) || create(:organisation, name: organisation_name)
  import_data_as_document_type_for_organisation(data, document_type, organisation)
end
Then /^the import succeeds, creating (\d+) imported speech(?:es)? with "([^"]*)" speech type and with no deliverer set$/ do |edition_count, speech_type_slug|
  speech_type = SpeechType.find_by_slug(speech_type_slug)
  assert_equal edition_count.to_i, Edition.imported.count

  edition = Edition.imported.first
  assert_kind_of Speech, edition
  assert_equal speech_type, edition.speech_type
  assert_nil edition.role_appointment
end
Then /^I can't make the imported (?:publication|speech|news article|consultation) into a draft edition yet$/ do
  visit_edition_admin Edition.imported.last.title

  assert page.has_css?('input[type=submit][disabled=disabled][value="Convert to draft"]')
end
Then /^I can make the imported (?:publication|speech|news article|consultation) into a draft edition$/ do
  edition = Edition.imported.last
  visit_edition_admin edition.title

  click_on 'Convert to draft'

  edition.reload
  assert edition.draft?
end
Then /^the imported speech's organisation is set to "([^"]*)"$/ do |organisation_name|
  assert_equal organisation_name, Edition.imported.last.organisations.first.name
end
When /^I set the imported speech's type to "([^"]*)"$/ do |speech_type|
  begin_editing_document Edition.imported.last.title
  select speech_type, from: 'Speech type'
  click_on 'Save'
end
When /^I set the deliverer of the speech to "([^"]*)" from the "([^"]*)"$/ do |person_name, organisation_name|
  person = find_or_create_person(person_name)
  organisation = create(:ministerial_department, name: organisation_name)
  role = create(:role, organisations: [organisation])
  create(:role_appointment, role: role, person: person)

  begin_editing_document Edition.imported.last.title
  select person_name, from: 'Speaker'
  click_on 'Save'
end
Then /^the speech's organisation is set to "([^"]*)"$/ do |organisation_name|
  assert_equal Edition.last.organisations, [Organisation.find_by_name(organisation_name)]
end
  def import_data_as_document_type_for_organisation(data, document_type, organisation)
    make_sure_data_importer_user_exists
    Import.use_separate_connection

    with_import_csv_file(data) do |path|
      visit new_admin_import_path
      select document_type, from: 'Type'
      attach_file 'CSV File', path
      select organisation.name, from: 'Default organisation'
      click_button 'Save'
      click_button 'Run'

      visit current_path
    end
    Import.find(current_path.match(/admin\/imports\/(\d+)\Z/)[1])
  end
  def begin_editing_document(title)
    visit_edition_admin title
    click_link "Edit draft"
  end
  def visit_edition_admin(title, scope = :scoped)
    document = Edition.send(scope).find_by_title(title)
    visit admin_edition_path(document)
  end
  def select(value, options={})
    if options.has_key?(:from)
      element = find(:select, options[:from], visible: :all).find(:option, value, visible: :all)
      if element.visible?
        from = options.delete(:from)
        find(:select, from, options).find(:option, value, options).select_option
      else
        select_from_chosen(value, options)
      end
    else
      find(:option, value, options).select_option
    end
  end
  def find_or_create_person(name)
    find_person(name) || create_person(name)
  end
  def with_import_csv_file(data)
    tf = Tempfile.new('csv_import')
    tf << data
    tf.close
    yield tf.path
    tf.unlink
  end
  def make_sure_data_importer_user_exists
    unless User.find_by_name('Automatic Data Importer')
      create(:importer, name: 'Automatic Data Importer')
    end
  end
  def visit_edition_admin(title, scope = :scoped)
    document = Edition.send(scope).find_by_title(title)
    visit admin_edition_path(document)
  end
  def select_from_chosen(value, options={})
    field = find_field(options[:from], visible: false, match: :first)
    option_value = page.evaluate_script("$(\"##{field[:id]} option:contains('#{value}')\").val()")
    page.execute_script("value = ['#{option_value}']\; if ($('##{field[:id]}').val()) {$.merge(value, $('##{field[:id]}').val())}")
    option_value = page.evaluate_script("value")
    page.execute_script("$('##{field[:id]}').val(#{option_value})")
    page.execute_script("$('##{field[:id]}').trigger('liszt:updated').trigger('change')")
  end
  def create_person(name, attributes = {})
    create(:person, split_person_name(name).merge(attributes))
  end
  def find_person(name)
    Person.where(split_person_name(name)).first
  end
  def split_person_name(name)
    if match = /^(\w+)\s*(.*?)$/.match(name)
      forename, surname = match.captures
      { title: nil, forename: forename, surname: surname, letters: nil }
    else
      raise "couldn't split \"#{name}\""
    end
  end
  def admin_edition_path(edition, *args)
    polymorphic_path([:admin, edition], *args)
  end
Given /^I am (?:a|an) (writer|editor|admin|GDS editor|importer)(?: called "([^"]*)")?$/ do |role, name|
  @user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Depteditor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  when 'importer'
    create(:importer)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    Edition::AuditTrail.whodunnit = user
    super(user) # warden
  end
When /^I draft a new authored article "([^"]*)"$/ do |title|
  begin_drafting_speech title: title
  select 'Authored article', from: "Speech type"
end
  def begin_drafting_speech(options)
    organisation = create(:ministerial_department)
    person = create_person("Colonel Mustard")
    role = create(:ministerial_role, name: "Attorney General", organisations: [organisation])
    role_appointment = create(:role_appointment, person: person, role: role, started_at: Date.parse('2010-01-01'))
    speech_type = SpeechType::Transcript
    begin_drafting_document options.merge(type: 'speech', summary: "Some summary of the content")
    select speech_type.singular_name, from: "Speech type"
    select "Colonel Mustard, Attorney General", from: "Speaker"
    select_date 1.day.ago.to_s, from: "Delivered on"
    fill_in "Location", with: "The Drawing Room"
  end
  def create_person(name, attributes = {})
    create(:person, split_person_name(name).merge(attributes))
  end
  def begin_drafting_document(options)
    if Organisation.count == 0
      create(:organisation)
    end
    visit admin_root_path
    # Make sure the dropdown is visible first, otherwise Capybara won't see the links
    find('li.create-new a', text: 'New document').click
    within 'li.create-new' do
      click_link options[:type].humanize
    end

    within 'form' do
      fill_in "edition_title", with: options[:title]
      fill_in "edition_body", with: options.fetch(:body, "Any old iron")
      fill_in "edition_summary", with: options.fetch(:summary, 'one plus one euals two!')
      fill_in_change_note_if_required

      unless options[:type] == 'world_location_news_article'
        set_lead_organisation_on_document(Organisation.first)
      end

      if options[:alternative_format_provider]
        select options[:alternative_format_provider].name, from: "edition_alternative_format_provider_id"
      end
      if options[:primary_mainstream_category]
        select options[:primary_mainstream_category].title, from: "Primary detailed guidance category"
      end
    end
  end
  def set_lead_organisation_on_document(organisation, order = 1)
    select organisation.name, from: "edition_lead_organisation_ids_#{order}"
  end
  def fill_in_change_note_if_required
    if has_css?("textarea[name='edition[change_note]']")
      fill_in "edition_change_note", with: "changes"
    end
  end
