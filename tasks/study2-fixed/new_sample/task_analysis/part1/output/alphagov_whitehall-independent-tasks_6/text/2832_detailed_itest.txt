Classes: 1
[name:THE_DOCUMENT, file:null, step:Then ]

Methods: 6
[name:has_css?, type:Object, file:null, step:Then ]
[name:index, type:HistoriesController, file:alphagov_whitehall/app/controllers/histories_controller.rb, step:null]
[name:index, type:HistoricAppointmentsController, file:alphagov_whitehall/app/controllers/historic_appointments_controller.rb, step:null]
[name:parameterize, type:Object, file:null, step:When ]
[name:record_css_selector, type:Object, file:null, step:Then ]
[name:show, type:HistoriesController, file:alphagov_whitehall/app/controllers/histories_controller.rb, step:null]

Referenced pages: 10
alphagov_whitehall/app/views/historic_appointments/_role_appointment.html.erb
alphagov_whitehall/app/views/historic_appointments/_role_appointments_list.html.erb
alphagov_whitehall/app/views/historic_appointments/index.html.erb
alphagov_whitehall/app/views/histories/10_downing_street.html.erb
alphagov_whitehall/app/views/histories/11_downing_street.html.erb
alphagov_whitehall/app/views/histories/1_horse_guards_road.html.erb
alphagov_whitehall/app/views/histories/index.html.erb
alphagov_whitehall/app/views/histories/king_charles_street.html.erb
alphagov_whitehall/app/views/histories/lancaster_house.html.erb
alphagov_whitehall/app/views/shared/_heading.html.erb

