Given /^a topic called "([^"]*)" exists$/ do |name|
  create(:topic, name: name)
end
Given /^a topic called "([^"]*)" with description "([^"]*)"$/ do |name, description|
  create(:topic, name: name, description: description)
end
Given /^the topic "([^"]*)" contains some policies$/ do |topic_name|
  @topic = create(:topic, name: topic_name)
  5.times do create(:published_policy, topics: [@topic]); end
  2.times do create(:draft_policy,     topics: [@topic]); end
end
Given /^the topic "([^"]*)" is related to the topic "([^"]*)"$/ do |name, related_name|
  related_topic = create(:topic, name: related_name)
  topic = Topic.find_by_name(name)
  topic.update_attributes!(related_classifications: [related_topic])
end
When /^I edit the topic "([^"]*)" to have description "([^"]*)"$/ do |name, description|
  visit admin_root_path
  click_link "Topics"
  click_link name
  click_on "Edit"
  fill_in "Description", with: description
  click_button "Save"
end
When /^I visit the list of topics$/ do
  visit topics_path
end
When /^I visit the "([^"]*)" topic$/ do |name|
  topic = Topic.find_by_name!(name)
  visit topic_path(topic)
end
Then /^I should be able to delete the topic "([^"]*)"$/ do |name|
  visit admin_topics_path
  click_link name
  click_on 'Edit'
  click_button 'Delete'
end
Then /^I should see published policies belonging to the "([^"]*)" topic$/ do |name|
  topic = Topic.find_by_name!(name)
  actual_editions = records_from_elements(Edition, page.all(".policy")).sort_by(&:id)
  expected_editions = topic.policies.published.all.sort_by(&:id)
  assert_equal expected_editions, actual_editions
end
Then /^I should see a link to the related topic "([^"]*)"$/ do |related_name|
  related_topic = Topic.find_by_name(related_name)
  assert page.has_css?(".related-topics a[href='#{topic_path(related_topic)}']", text: related_name)
end
When(/^I feature one of the policies on the topic$/) do
  @policy = @topic.policies.published.last
  visit admin_topic_path(@topic)
  click_on 'Features'

  within record_css_selector(@policy) do
    click_link "Feature"
  end
  attach_file "Select an image to be shown when featuring", jpg_image
  fill_in :classification_featuring_alt_text, with: "An accessible description of the image"
  click_button "Save"
end
Then(/^I should see the policy featured on the public topic page$/) do
  visit topic_path(@topic)
  within('section.featured-news') do
    assert page.has_content?(@policy.title)
  end
end
  def jpg_image
    Rails.root.join("test/fixtures/minister-of-funk.960x640.jpg")
  end
When /^I force publish (#{THE_DOCUMENT})$/ do |edition|
  visit_edition_admin edition.title, :draft
  click_link "Edit draft"
  fill_in_change_note_if_required
  click_button "Save"
  publish(force: true)
end
Then /^I should see (#{THE_DOCUMENT})$/ do |edition|
  assert has_css?(record_css_selector(edition))
end
  def visit_edition_admin(title, scope = :scoped)
    document = Edition.send(scope).find_by_title(title)
    visit admin_edition_path(document)
  end
  def fill_in_change_note_if_required
    if has_css?("textarea[name='edition[change_note]']")
      fill_in "edition_change_note", with: "changes"
    end
  end
  def publish(options = {})
    if options[:force]
      click_link "Force publish"
      page.has_css?("#forcePublishModal", visible: true)
      within '#forcePublishModal' do
        fill_in 'reason', with: "because"
        click_button 'Force publish'
      end
      unless options[:ignore_errors]
        refute_flash_alerts_exist
      end
    else
      click_button "Publish"
      unless options[:ignore_errors]
        refute_flash_alerts_exist
      end
    end
  end
  def refute_flash_alerts_exist
    refute has_css?(".flash.alert")
  end
  def admin_edition_path(edition, *args)
    polymorphic_path([:admin, edition], *args)
  end
Given /^I am (?:a|an) (writer|editor|admin|GDS editor|importer)(?: called "([^"]*)")?$/ do |role, name|
  @user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Depteditor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  when 'importer'
    create(:importer)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    Edition::AuditTrail.whodunnit = user
    super(user) # warden
  end
Given(/^a topical event called "(.*?)" with description "(.*?)"$/) do |name, description|
  create(:topical_event,name: name, description: description)
end
When /^I draft a new speech "([^"]*)" relating it to topical event "([^"]*)"$/ do |speech_name, topical_event_name|
  begin_drafting_speech title: speech_name
  select topical_event_name, from: "Topical events"
  click_button "Save"
end
When /^I draft a new news article "([^"]*)" relating it to topical event "([^"]*)"$/ do |news_article_title, topical_event_name|
  begin_drafting_news_article title: news_article_title
  select topical_event_name, from: "Topical events"
  click_button "Save"
end
When /^I draft a new publication "([^"]*)" relating it to topical event "([^"]*)"$/ do |publication_title, topical_event_name|
  begin_drafting_publication publication_title
  select topical_event_name, from: "Topical events"
  click_button "Save"
end
When /^I draft a new consultation "([^"]*)" relating it to topical event "([^"]*)"$/ do |consultation_title, topical_event_name|
  begin_drafting_consultation title: consultation_title
  select topical_event_name, from: "Topical events"
  click_button "Save"
end
Then /^(#{THE_DOCUMENT}) shows it is related to the topical event "([^"]*)" on its public page$/ do |edition, topical_event_name|
  topical_event = TopicalEvent.find_by_name!(topical_event_name)
  visit public_document_path(edition)
  assert page.has_css?(".meta a", text: topical_event.name)
end
When /^I feature the news article "([^"]*)" for topical event "([^"]*)" with image "([^"]*)"$/ do |news_article_title, topical_event_name, image_filename|
  topical_event = TopicalEvent.find_by_name!(topical_event_name)
  visit admin_topical_event_classification_featurings_path(topical_event)
  news_article = NewsArticle.find_by_title(news_article_title)
  within record_css_selector(news_article) do
    click_link "Feature"
  end
  attach_file "Select an image to be shown when featuring", Rails.root.join("test/fixtures/#{image_filename}")
  fill_in :classification_featuring_alt_text, with: "An accessible description of the image"
  click_button "Save"
end
  def begin_drafting_news_article(options)
    begin_drafting_document(options.merge(type: "news_article"))
    fill_in_news_article_fields
  end
  def begin_drafting_consultation(options)
    begin_drafting_document(options.merge(type: "consultation"))
    select_date 10.days.from_now.to_s, from: "Opening Date"
    select_date 40.days.from_now.to_s, from: "Closing Date"
  end
  def begin_drafting_publication(title)
    policy = create(:policy)
    begin_drafting_document type: 'publication', title: title, summary: "Some summary of the content", alternative_format_provider: create(:alternative_format_provider)
    fill_in_publication_fields
    select policy.title, from: "Related policies"
  end
  def begin_drafting_speech(options)
    organisation = create(:ministerial_department)
    person = create_person("Colonel Mustard")
    role = create(:ministerial_role, name: "Attorney General", organisations: [organisation])
    role_appointment = create(:role_appointment, person: person, role: role, started_at: Date.parse('2010-01-01'))
    speech_type = SpeechType::Transcript
    begin_drafting_document options.merge(type: 'speech', summary: "Some summary of the content")
    select speech_type.name, from: "Type"
    select "Colonel Mustard, Attorney General", from: "Speaker"
    select_date 1.day.ago.to_s, from: "Delivered on"
    fill_in "Location", with: "The Drawing Room"
  end
  def select(value, options={})
    if options.has_key?(:from)
      element = find(:select, options[:from], visible: :all).find(:option, value, visible: :all)
      if element.visible?
        from = options.delete(:from)
        find(:select, from, options).find(:option, value, options).select_option
      else
        select_from_chosen(value, options)
      end
    else
      find(:option, value, options).select_option
    end
  end
  def begin_drafting_document(options)
    if Organisation.count == 0
      create(:organisation)
    end
    visit admin_root_path
    # Make sure the dropdown is visible first, otherwise Capybara won't see the links
    if options[:type] == "detailed_guide"
      visit new_detailed_guides_page
    else
      find('li.create-new a', text: 'New document').click
      within 'li.create-new' do
        click_link options[:type].humanize
      end
    end

    within 'form' do
      fill_in "edition_title", with: options[:title]
      fill_in "edition_body", with: options.fetch(:body, "Any old iron")
      fill_in "edition_summary", with: options.fetch(:summary, 'one plus one euals two!')
      fill_in_change_note_if_required

      unless options[:type] == 'world_location_news_article'
        set_lead_organisation_on_document(Organisation.first)
      end

      if options[:alternative_format_provider]
        select options[:alternative_format_provider].name, from: "edition_alternative_format_provider_id"
      end
      if options[:primary_mainstream_category]
        select options[:primary_mainstream_category].title, from: "Primary detailed guidance category"
      end
    end
  end
  def fill_in_news_article_fields
    select "News story", from: "News article type"
  end
  def fill_in_publication_fields
    select_date "2010-01-01", from: "First published"
    select "Research and analysis", from: "edition_publication_type_id"
    fill_in "HTML version title", with: "HTML version title"
    fill_in "HTML version text", with: "HTML version text"
  end
  def create_person(name, attributes = {})
    create(:person, split_person_name(name).merge(attributes))
  end
  def select_from_chosen(value, options={})
    field = find_field(options[:from], visible: false, match: :first)
    option_value = page.evaluate_script("$(\"##{field[:id]} option:contains('#{value}')\").val()")
    page.execute_script("value = ['#{option_value}']\; if ($('##{field[:id]}').val()) {$.merge(value, $('##{field[:id]}').val())}")
    option_value = page.evaluate_script("value")
    page.execute_script("$('##{field[:id]}').val(#{option_value})")
    page.execute_script("$('##{field[:id]}').trigger('liszt:updated').trigger('change')")
  end
  def new_detailed_guides_page
    new_admin_detailed_guide_path
  end
  def set_lead_organisation_on_document(organisation, order = 1)
    select organisation.name, from: "edition_lead_organisation_ids_#{order}"
  end
  def fill_in_change_note_if_required
    if has_css?("textarea[name='edition[change_note]']")
      fill_in "edition_change_note", with: "changes"
    end
  end
  def split_person_name(name)
    if match = /^(\w+)\s*(.*?)$/.match(name)
      forename, surname = match.captures
      { title: nil, forename: forename, surname: surname, letters: nil }
    else
      raise "couldn't split \"#{name}\""
    end
  end
  def public_document_path(edition, options = {})
    document_path(edition, options)
  end
