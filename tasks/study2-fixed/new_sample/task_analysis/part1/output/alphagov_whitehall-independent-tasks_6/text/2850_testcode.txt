Given /^a published document "([^"]*)" exists$/ do |title|
  create(:published_policy, title: title)
end
Given(/^a published document collection "([^"]*)" exists$/) do |title|
  @document_collection = create(:published_document_collection, :with_group, title: title)
end
Given(/^a published publication called "(.*?)" in the document collection "(.*?)"$/) do |publication_title, collection_title|
  @publication = create(:published_publication, title: publication_title)
  @document_collection = create(:published_document_collection,
    title: collection_title,
    groups: [build(:document_collection_group, documents: [@publication.document])]
  )
  @group = @document_collection.groups.first
end
When(/^I draft a new document collection called "(.*?)"$/) do |title|
  visit new_admin_document_collection_path
  within ".edition-form" do
    fill_in "Title",   with: title
    fill_in "Summary", with: "a summary"
    fill_in "Body",    with: "a body"

    click_on "Save"
  end
  @document_collection = DocumentCollection.find_by_title!(title)
end
When(/^I add the document "(.*?)" to the document collection$/) do |document_title|
  doc_edition = Edition.find_by_title!(document_title)
  refute @document_collection.nil?, "No document collection to act on."

  visit admin_document_collection_path(@document_collection)
  click_on "Edit draft"
  click_on "Collection documents"

  fill_in 'title', with: document_title
  click_on 'Find'
  find('li.ui-menu-item').click
  click_on 'Add'

  within ('section.group') do
    assert page.has_content? doc_edition.title
  end

  # assert @document_collection.groups.first.documents.include?(doc_edition.document), 'Document has not been added to the collection'
end
When(/^I remove the document "(.*?)" from the document collection$/) do |document_title|
  # doc_edition = Edition.find_by_title!(document_title)
  refute @document_collection.nil?, "No document collection to act on."

  visit admin_document_collection_path(@document_collection)
  click_on "Create new edition to edit"
  click_on "Collection documents"

  check document_title
  click_on "Remove"
  click_on "Document"
  check "edition_minor_change"
  click_on "Save"
  @document_collection = @document_collection.reload.document.latest_edition
end
Then(/^I see that the document "(.*?)" is part of the document collection$/) do |document_title|
  assert_document_is_part_of_document_collection(document_title)
end
Then(/^I see that the document "(.*?)" is not part of the document collection$/) do |document_title|
  refute_document_is_part_of_document_collection(document_title)
end
When(/^I visit the old document series url "(.*?)"$/) do |url|
  visit url
end
Then(/^I should be redirected to the "(.*?)" document collection$/) do |title|
  dc = DocumentCollection.find_by_title(title)
  assert_equal public_document_path(dc), page.current_path
end
Then(/^I can see in the preview that "(.*?)" is part of the document collection$/) do |document_title|
  visit admin_document_collection_path(@document_collection)
  visit_link_href "Preview on website"
  assert_document_is_part_of_document_collection(document_title)
end
Given(/^a published publication called "(.*?)" in a published document collection$/) do |publication_title|
  @publication = create(:published_publication, title: publication_title)
  @document_collection = create(:published_document_collection,
    groups: [build(:document_collection_group, documents: [@publication.document])]
  )
  @group = @document_collection.groups.first
end
When(/^I redraft the document collection and remove "(.*?)" from it$/) do |document_title|
  refute @document_collection.nil?, "No document collection to act on."

  visit admin_document_collection_path(@document_collection)
  click_on "Create new edition to edit"
  click_on "Collection documents"

  check document_title
  click_on "Remove"
  click_on "Document"
  check "edition_minor_change"
  click_on "Save"
  @document_collection = @document_collection.reload.document.latest_edition
end
Then(/^I can see in the preview that "(.*?)" does not appear$/) do |document_title|
  visit_link_href "Preview on website"
  refute_document_is_part_of_document_collection(document_title)
end
  def assert_document_is_part_of_document_collection(document_title)
    within '#document_collection' do
      assert page.has_content? document_title
    end
  end
  def refute_document_is_part_of_document_collection(document_title)
    within '#document_collection' do
      refute page.has_content? document_title
    end
  end
  def visit_link_href(link_text)
    visit(find('a', text: link_text)[:href])
  end
Given /^I am (?:a|an) (writer|editor|admin|GDS editor|importer)(?: called "([^"]*)")?$/ do |role, name|
  @user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Depteditor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  when 'importer'
    create(:importer)
  end
  login_as @user
end
Given /^I am (?:an?) (writer|editor) in the organisation "([^"]*)"$/ do |role, organisation_name|
  organisation = Organisation.find_by_name(organisation_name) || create(:organisation, name: organisation_name)
  @user = case role
  when "writer"
    create(:policy_writer, name: "Wally Writer", organisation: organisation)
  when "editor"
    create(:departmental_editor, name: "Eddie Depteditor", organisation: organisation)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    Edition::AuditTrail.whodunnit = user
    super(user) # warden
  end
Given /^a (topic|mainstream category) with the slug "([^"]*)" exists$/ do |type, slug|
  o = create(type.parameterize.underscore)
  o.update_attributes!(slug: slug)
end
When /^I import the following data as CSV as "([^"]*)" for "([^"]*)":$/ do |document_type, organisation_name, data|
  organisation = Organisation.find_by_name(organisation_name) || create(:organisation, name: organisation_name)
  import_data_as_document_type_for_organisation(data, document_type, organisation)
end
Then /^the import succeeds creating (\d+) detailed guidance document$/ do |n|
  assert_equal :finished, Import.last.status
  assert_equal [], Import.last.import_errors
  assert_equal n.to_i, Import.last.documents.where(document_type: DetailedGuide.name).to_a.size
end
Then /^the imported detailed guidance document has the following associations:$/ do |expected_table|
  detailed_guide_document = Import.last.documents.where(document_type: DetailedGuide.name).first
  edition = detailed_guide_document.editions.first
  expected_table.hashes.each do |row|
    assert_equal edition.send(row["Name"].to_sym).map(&:slug), row["Slugs"].split(/, +/)
  end
end
  def import_data_as_document_type_for_organisation(data, document_type, organisation)
    make_sure_data_importer_user_exists
    Import.use_separate_connection

    with_import_csv_file(data) do |path|
      visit new_admin_import_path
      select document_type, from: 'Type'
      attach_file 'CSV File', path
      select organisation.name, from: 'Default organisation'
      click_button 'Save'
      click_button 'Run'

      visit current_path
    end
    Import.find(current_path.match(/admin\/imports\/(\d+)\Z/)[1])
  end
  def with_import_csv_file(data)
    tf = Tempfile.new('csv_import')
    tf << data
    tf.close
    yield tf.path
    tf.unlink
  end
  def make_sure_data_importer_user_exists
    unless User.find_by_name('Automatic Data Importer')
      create(:importer, name: 'Automatic Data Importer')
    end
  end
  def select(value, options={})
    if options.has_key?(:from)
      element = find(:select, options[:from], visible: :all).find(:option, value, visible: :all)
      if element.visible?
        from = options.delete(:from)
        find(:select, from, options).find(:option, value, options).select_option
      else
        select_from_chosen(value, options)
      end
    else
      find(:option, value, options).select_option
    end
  end
  def select_from_chosen(value, options={})
    field = find_field(options[:from], visible: false, match: :first)
    option_value = page.evaluate_script("$(\"##{field[:id]} option:contains('#{value}')\").val()")
    page.execute_script("value = ['#{option_value}']\; if ($('##{field[:id]}').val()) {$.merge(value, $('##{field[:id]}').val())}")
    option_value = page.evaluate_script("value")
    page.execute_script("$('##{field[:id]}').val(#{option_value})")
    page.execute_script("$('##{field[:id]}').trigger('liszt:updated').trigger('change')")
  end
Given /^a published detailed guide "([^"]*)" for the organisation "([^"]*)"$/ do |title, organisation|
  organisation = create(:organisation, name: organisation)
  create(:published_detailed_guide, title: title, organisations: [organisation])
end
