Feature: Suzie views a petition
In order to read a petition and potentially sign it
As Suzie the signer
I want to view a petition of my choice from a list, seeing the vote count, closed and open dates, along with the reason for rejection if applicable
Scenario: Suzie does not see the creator when viewing a closed petition
Given a petition "Spend more money on Defence" has been closed
When I view the petition
Then I should not see the petition creator
