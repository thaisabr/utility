Given /^a petition "([^"]*)" has been closed$/ do |petition_action|
  @petition = FactoryGirl.create(:open_petition, :action => petition_action, :closed_at => 1.day.ago)
end
When /^I view the petition$/ do
  if @petition.is_a?(ArchivedPetition)
    visit archived_petition_path(@petition)
  else
    visit petition_path(@petition)
  end
end
Then(/^I should not see the petition creator$/) do
  expect(page).not_to have_css("li.meta-created-by", :text => "Created by " + @petition.creator_signature.name)
end
