Feature: Moderator respond to petition
In order to allow or prevent the signing of petitions
As a moderator
I want to respond to a petition, editing, accepting, rejecting or re-assigning it
Scenario: Moderator rejects and hides previously rejected (and public) petition
And I am logged in as a moderator
And a petition "actually libellous" has been rejected with the reason "duplicate"
When I go to the admin petition page for "actually libellous"
And I change the rejection status of the petition with a reason code "Confidential, libellous, false or defamatory statements (will be hidden)"
And the petition is not available for searching or viewing
But the petition will still show up in the back-end reporting
Feature: A moderator user updates records notes
In order to record thoughts about a petition that are not for public consumption
As any moderator user
I want to be able add notes to any petition
Background:
Given I am logged in as a moderator
Scenario: Adding notes to an open petition
Given an open petition exists with action: "Solidarity with the Unions"
When I am on the admin all petitions page
And I follow "Solidarity with the Unions"
And I follow "Notes"
Then I should see a "Notes" textarea field
And the markup should be valid
When I fill in "Notes" with "I think we can debate this, will check with unions select committee first"
And I press "Save"
Then I should be on the admin petition page for "Solidarity with the Unions"
And a petition should exist with action: "Solidarity with the Unions", admin_notes: "I think we can debate this, will check with unions select committee first"
Scenario: Adding notes to an in moderation petition
Given an sponsored petition exists with action: "Solidarity with the Unions"
When I am on the admin todolist page
And I follow "Solidarity with the Unions"
And I follow "Notes"
Then I should see a "Notes" textarea field
And the markup should be valid
When I fill in "Notes" with "I think we can debate this, will check with unions select committee first"
And I press "Save"
Then I should be on the admin petition page for "Solidarity with the Unions"
And a petition should exist with action: "Solidarity with the Unions", admin_notes: "I think we can debate this, will check with unions select committee first"
Feature: Maggie searches for a petition by id
In order to quickly find a petition and view the contents
As Maggie
I want to enter an id and be taken to the petition for that id, or shown an error if it doesn't exist
Scenario: A user sees the show page if the petition needs moderation
Given a sponsored petition "Loose benefits!"
And I am logged in as a moderator
When I search for a petition by id
Then I am on the admin petition page for "Loose benefits!"
Scenario: A user sees the show page if the petition is visible
Given a petition "Duplicate" has been rejected
And I am logged in as a moderator
When I search for a petition by id
Then I am on the admin petition page for "Duplicate"
Scenario: A moderator user show page if the petition needs moderation
Given a sponsored petition "Loose benefits!"
And I am logged in as a moderator
When I search for a petition by id
Then I am on the admin petition page for "Loose benefits!"
Scenario: A moderator user show response page if the petition is open
Given an open petition "Fun times!"
And I am logged in as a moderator
When I search for a petition by id
Then I am on the admin petition page for "Fun times!"
Feature: Terry (or Sheila) takes down a petition
In order to remove petitions that have been published by mistake
As Terry or Sheila
I want to reject a petition after it has been published, which sends the standard rejection email out to the creator and removes the petition from the site
Background:
Given a petition "Mistakenly published petition"
Scenario: A sysadmin can take down a petition
Given I am logged in as a sysadmin
When I view all petitions
And I follow "Mistakenly published petition"
And I take down the petition with a reason code "Duplicate of an existing petition"
Then the petition is not available for signing
And I should not be able to take down the petition
Scenario: A moderator can take down a petition
Given I am logged in as a moderator
When I view all petitions
And I follow "Mistakenly published petition"
And I take down the petition with a reason code "Duplicate of an existing petition"
Then the petition is not available for signing
And I should not be able to take down the petition
Feature: Threshold list
In order to see and action petitions that require a response
As a moderator or sysadmin user
I want to see a list of petitions that have exceeded the signature threshold count
Background:
Given I am logged in as a moderator
And the date is the "21 April 2011 12:00"
And the threshold for a parliamentary debate is "5"
And an open petition "p1" exists with action: "Petition 1", closed_at: "1 January 2012"
And the petition "Petition 1" has 25 validated signatures
And an open petition "p2" exists with action: "Petition 2", closed_at: "20 August 2011"
And the petition "Petition 2" has 4 validated signatures
And an open petition "p3" exists with action: "Petition 3", closed_at: "20 September 2011"
And the petition "Petition 3" has 5 validated signatures
And a closed petition "p4" exists with action: "Petition 4", closed_at: "20 April 2011"
And the petition "Petition 4" has 10 validated signatures
And an open petition "p5" exists with action: "Petition 5"
And a closed petition "p6" exists with action: "Petition 6", closed_at: "21 April 2011"
Scenario: Threshold petitions are paginated
Given I am logged in as a sysadmin
And 20 petitions exist with a signature count of 6
When I go to the admin threshold page
And I follow "Next"
Then I should see 3 rows in the admin index table
And I follow "Previous"
And I should see 20 rows in the admin index table
Scenario: A moderator user can view the details of a petition and form fields
When I go to the admin threshold page
And I follow "Petition 1"
Then I should see "01-01-2012"
When I follow "Government response"
Then I should see a "Response summary" textarea field
And I should see a "Response" textarea field
And I should see a "Email signees" checkbox field
Scenario: A moderator user updates the public response to a petition
Given the time is "3 Dec 2010 01:00"
When I go to the admin threshold page
And I follow "Petition 1"
And I follow "Government response"
And I fill in "Response summary" with "Ready yourselves"
And I fill in "Response" with "Parliament here it comes. This is a long text."
And I check "Email signees"
And I press "Save"
Then I should be on the admin petition page for "Petition 1"
And the petition with action: "Petition 1" should have requested a government response email after "2010-12-03 01:00:00"
And the response summary to "Petition 1" should be publicly viewable on the petition page
And the response to "Petition 1" should be publicly viewable on the petition page
And the petition signatories of "Petition 1" should receive a response notification email
Scenario: A moderator user unsuccessfully tries to update the public response to a petition
Given the time is "3 Dec 2010 01:00"
When I go to the admin threshold page
And I follow "Petition 1"
And I follow "Government response"
And I check "Email signees"
And I press "Save"
Then I should see "must be completed when email signees is checked"
And the petition with action: "Petition 1" should not have requested a government response email
And the petition signatories of "Petition 1" should not receive a response notification email
