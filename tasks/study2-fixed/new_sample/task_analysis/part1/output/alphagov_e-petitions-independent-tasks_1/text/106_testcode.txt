Given(/^(\d+) petitions? signed by "([^"]*)"$/) do |petition_count, name_or_email|
  petition_count.times do
    if name_or_email =~ /\A[^@]+@[^@]+\z/
      attrs = { email: name_or_email }
    else
      attrs = { name: name_or_email }
    end

    attrs[:petition] = FactoryGirl.create(:open_petition)
    FactoryGirl.create(:signature, attrs)
  end
end
When(/^I search for petitions signed by "([^"]*)"( from the admin hub)?$/) do |name_or_email, from_the_hub|
  if from_the_hub.blank?
    visit admin_petitions_url
  else
    visit admin_root_url
  end

  if name_or_email =~ /\A[^@]+@[^@]+\z/
    query = name_or_email
  else
    query = %["#{name_or_email}"]
  end

  fill_in "Search", :with => query
  click_button 'Search'
end
Then(/^I should see (\d+) petitions? associated with the (?:name|email address)$/) do |petition_count|
  expect(page).to have_css("tbody tr", :count => petition_count)
end
Given /^I am logged in as a moderator$/ do
  @user = FactoryGirl.create(:moderator_user)
  step "the admin user is logged in"
end
Given /^the admin user is logged in$/ do
  visit admin_login_url
  fill_in("Email", :with => @user.email)
  fill_in("Password", :with => "Letmein1!")
  click_button("Sign in")
end
