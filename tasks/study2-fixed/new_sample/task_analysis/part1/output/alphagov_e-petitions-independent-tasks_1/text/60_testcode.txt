Given /^(?:|I )am on (.+)$/ do |page_name|
  visit url_to(page_name)
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    expect(page).to have_content(text)
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def url_to(page_name)
    case page_name

    when /^the home\s?page$/
      home_url

    when /^the help page$/
      help_url

    when /^the privacy page$/
      privacy_url

    when /^the feedback page$/
      feedback_url

    when /^the new petition page$/
      new_petition_url

    when /^the petition page$/
      petition_url(@petition.id)

    when /^the petition page for "([^\"]*)"$/
      petition_url(Petition.find_by(action: $1))

    when /^the archived petitions page$/
      archived_petitions_url

    when /^the archived petitions search results page$/
      search_archived_petitions_url

    when /^the archived petition page for "([^\"]*)"$/
      archived_petition_url(ArchivedPetition.find_by(title: $1))

    when /^the new signature page$/
      new_petition_signature_url(@petition.id)

    when /^the new signature page for "([^\"]*)"$/
      new_petition_signature_url(Petition.find_by(action: $1))

    when /^the search results page$/
      search_url

    when /^the Admin (.*)$/i
      admin_url($1)

    when /^the local petitions results page$/
      local_petition_url(@my_constituency.slug)

    when /^the all local petitions results page$/
      all_local_petition_url(@my_constituency.slug)

    else
      begin
        page_name =~ /^the (.*) page$/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('url').join('_').to_sym)
      rescue NoMethodError, ArgumentError
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Given(/^a constituency "(.*?)"(?: with MP "(.*?)")? is found by postcode "(.*?)"$/) do |constituency_name, mp_name, postcode|
  @constituencies ||= {}
  constituency = @constituencies[constituency_name]

  if constituency.nil?
    mp_name = mp_name.present? ? mp_name : 'Rye Tonnemem-Burr MP'
    constituency = FactoryGirl.create(:constituency, name: constituency_name, mp_name: mp_name, mp_date: 3.years.ago)
    @constituencies[constituency.name] = constituency
  end

  for_postcode = @constituencies[postcode]

  if for_postcode.nil?
    @constituencies[postcode] = constituency
  elsif for_postcode == constituency
    # noop
  else
    raise "Postcode #{postcode} registered for constituency #{for_postcode.name} already, can't reassign to #{constituency.name}"
  end
end
Given(/^(a|few|some|many) constituents? in "(.*?)" supports? "(.*?)"$/) do |how_many, constituency, petition_action|
  petition = Petition.find_by!(action: petition_action)
  constituency = @constituencies.fetch(constituency)
  how_many =
    case how_many
    when 'a' then 1
    when 'few' then 3
    when 'some' then 5
    when 'many' then 10
    end

  how_many.times do
    FactoryGirl.create(:pending_signature, petition: petition, constituency_id: constituency.external_id).validate!
  end
end
When(/^I search for petitions local to me in "(.*?)"$/) do |postcode|
  @my_constituency = @constituencies.fetch(postcode)

  if @constituency_api_down
    stub_any_api_request.to_return(api_response(:internal_server_error))
  else
    sanitized_postcode = PostcodeSanitizer.call(postcode)

    if @mp_passed_away
      stub_api_request_for(sanitized_postcode).to_return(api_response(:ok) {
        <<-XML.strip
          <Constituencies>
            <Constituency>
              <Constituency_Id>#{@my_constituency.external_id}</Constituency_Id>
              <Name>#{@my_constituency.name}</Name>
              <ONSCode>#{@my_constituency.ons_code}</ONSCode>
              <RepresentingMembers>
                <RepresentingMember>
                  <Member_Id>#{@my_constituency.mp_id}</Member_Id>
                  <Member>#{@my_constituency.mp_name}</Member>
                  <StartDate>#{@my_constituency.mp_date.iso8601}</StartDate>
                  <EndDate>#{1.day.ago.iso8601}</EndDate>
                </RepresentingMember>
            </Constituency>
          </Constituencies>
        XML
      })
    else
      stub_api_request_for(sanitized_postcode).to_return(api_response(:ok) {
        <<-XML.strip
          <Constituencies>
            <Constituency>
              <Constituency_Id>#{@my_constituency.external_id}</Constituency_Id>
              <Name>#{@my_constituency.name}</Name>
              <ONSCode>#{@my_constituency.ons_code}</ONSCode>
              <RepresentingMembers>
                <RepresentingMember>
                  <Member_Id>#{@my_constituency.mp_id}</Member_Id>
                  <Member>#{@my_constituency.mp_name}</Member>
                  <StartDate>#{@my_constituency.mp_date.iso8601}</StartDate>
                  <EndDate xsi:nil="true"
                           xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>
                </RepresentingMember>
            </Constituency>
          </Constituencies>
        XML
      })
    end
  end

  within :css, '.local-to-you' do
    fill_in "UK postcode", with: postcode
    click_on "Search"
  end
end
Then(/^I should see that my fellow constituents support "(.*?)"$/) do |petition_action|
  petition = Petition.find_by!(action: petition_action)
  all_signature_count = petition.signatures.validated.count
  local_signature_count = petition.signatures.validated.where(constituency_id: @my_constituency.external_id).count
  within :css, '.local-petitions' do
    within ".//*#{XPathHelpers.class_matching('petition-item')}[.//a[.='#{petition_action}']]" do
      expect(page).to have_text("#{local_signature_count} #{'signature'.pluralize(local_signature_count)} from #{@my_constituency.name}")
      expect(page).to have_text("#{all_signature_count} #{'signature'.pluralize(all_signature_count)} total")
    end
  end
end
Then(/^I should not see that my fellow constituents support "(.*?)"$/) do |petition_action|
  within :css, '.local-petitions' do |list|
    expect(list).not_to have_selector(".//*#{XPathHelpers.class_matching('petition-item')}[a[.='#{petition_action}']]")
  end
end
Then(/^the petitions I see should be ordered by my fellow constituents level of support$/) do
  within :css, '.local-petitions ol' do
    petitions = page.all(:css, '.petition-item')
    my_constituents_signature_counts = petitions.map { |petition| Integer(petition.text.match(/(\d+) signatures? from/)[1]) }
    expect(my_constituents_signature_counts).to eq my_constituents_signature_counts.sort.reverse
  end
end
Then(/^I should see a link to the MP for my constituency$/) do
  expect(page).to have_link(@my_constituency.mp_name, href: @my_constituency.mp_url)
end
Then(/^I should see a link to view all local petitions$/) do
  expect(page).to have_link("View all popular petitions in #{@my_constituency.name}", href: all_local_petition_path(@my_constituency))
end
Then(/^I should see a link to view open local petitions$/) do
  expect(page).to have_link("View open popular petitions in #{@my_constituency.name}", href: local_petition_path(@my_constituency))
end
When(/^I click the view all local petitions$/) do
  click_on "View all popular petitions in #{@my_constituency.name}"
end
Then(/^I should see that closed petitions are identified$/) do
  expect(page).to have_text("now closed")
end
Then(/^#{capture_model} should be #{capture_model}$/) do |a, b|
  expect(model!(a)).to eq model!(b)
end
Then /^I should (not |)see "([^\"]*)" in the ((?!email\b).*)$/ do |see_or_not, text, section_name|
  if section_name == 'browser page title'
    if see_or_not.blank?
      expect(page).to have_title(text.to_s)
    else
      expect(page).to have_no_title(text.to_s)
    end
  else
    within_section(section_name) do
      if see_or_not.blank?
        expect(page).to have_content(text.to_s)
      else
        expect(page).to have_no_content(text.to_s)
      end
    end
  end
end
  def within_section(section_name)
    within xpath_of_section(section_name) do
      yield
    end
  end
  def xpath_of_section(section_name, prefix = "//")
    case section_name

    # Non site-specific based
    when /"([^\"]*)" fieldset/
      "#{prefix}fieldset#{XPathHelpers.class_matching($1.downcase.gsub(/\s/, '_'))}"

    # Sitewide
    when /^single h1$/
      expect(page).to have_xpath("//h1", :count => 1)
      "#{prefix}h1"

    else
      raise "Can't find mapping from \"#{section_name}\" to a section."
    end
  end
Given(/^an? (open|closed|rejected) petition "(.*?)" with some (fraudulent)? ?signatures$/) do |state, petition_action, signature_state|
  petition_closed_at = state == 'closed' ? 1.day.ago : nil
  petition_state = state == 'closed' ? 'open' : state
  petition_args = {
    action: petition_action,
    open_at: 3.months.ago,
    closed_at: petition_closed_at
  }
  @petition = FactoryGirl.create(:"#{state}_petition", petition_args)
  signature_state ||= "validated"
  5.times { FactoryGirl.create(:"#{signature_state}_signature", petition: @petition) }
end
