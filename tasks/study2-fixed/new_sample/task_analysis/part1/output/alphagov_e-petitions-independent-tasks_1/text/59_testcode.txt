Given /^(?:|I )am on (.+)$/ do |page_name|
  visit url_to(page_name)
end
Then(/^I should get a download with the filename "([^\"]*)"$/) do |filename|
  expect(page.response_headers['Content-Disposition']).to include("attachment; filename=#{filename}")
end
  def url_to(page_name)
    case page_name

    when /^the home\s?page$/
      home_url

    when /^the help page$/
      help_url

    when /^the privacy page$/
      privacy_url

    when /^the feedback page$/
      feedback_url

    when /^the new petition page$/
      new_petition_url

    when /^the petition page$/
      petition_url(@petition.id)

    when /^the petition page for "([^\"]*)"$/
      petition_url(Petition.find_by(action: $1))

    when /^the archived petitions page$/
      archived_petitions_url

    when /^the archived petitions search results page$/
      search_archived_petitions_url

    when /^the archived petition page for "([^\"]*)"$/
      archived_petition_url(ArchivedPetition.find_by(title: $1))

    when /^the new signature page$/
      new_petition_signature_url(@petition.id)

    when /^the new signature page for "([^\"]*)"$/
      new_petition_signature_url(Petition.find_by(action: $1))

    when /^the search results page$/
      search_url

    when /^the Admin (.*)$/i
      admin_url($1)

    when /^the local petitions results page$/
      local_petition_url(@my_constituency.slug)

    when /^the local petitions JSON page$/
      local_petition_url(@my_constituency.slug, :json)

    when /^the all local petitions results page$/
      all_local_petition_url(@my_constituency.slug)

    when /^the all local petitions JSON page$/
      all_local_petition_url(@my_constituency.slug, :json)

    else
      begin
        page_name =~ /^the (.*) page$/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('url').join('_').to_sym)
      rescue NoMethodError, ArgumentError
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Given(/^a constituency "(.*?)"(?: with MP "(.*?)")? is found by postcode "(.*?)"$/) do |constituency_name, mp_name, postcode|
  @constituencies ||= {}
  constituency = @constituencies[constituency_name]

  if constituency.nil?
    mp_name = mp_name.present? ? mp_name : 'Rye Tonnemem-Burr MP'
    constituency = FactoryGirl.create(:constituency, name: constituency_name, mp_name: mp_name, mp_date: 3.years.ago)
    @constituencies[constituency.name] = constituency
  end

  for_postcode = @constituencies[postcode]

  if for_postcode.nil?
    @constituencies[postcode] = constituency
  elsif for_postcode == constituency
    # noop
  else
    raise "Postcode #{postcode} registered for constituency #{for_postcode.name} already, can't reassign to #{constituency.name}"
  end
end
Given(/^(a|few|some|many) constituents? in "(.*?)" supports? "(.*?)"$/) do |how_many, constituency, petition_action|
  petition = Petition.find_by!(action: petition_action)
  constituency = @constituencies.fetch(constituency)
  how_many =
    case how_many
    when 'a' then 1
    when 'few' then 3
    when 'some' then 5
    when 'many' then 10
    end

  how_many.times do
    FactoryGirl.create(:pending_signature, petition: petition, constituency_id: constituency.external_id).validate!
  end
end
When(/^I search for petitions local to me in "(.*?)"$/) do |postcode|
  @my_constituency = @constituencies.fetch(postcode)

  if @constituency_api_down
    stub_any_api_request.to_return(api_response(:internal_server_error))
  else
    sanitized_postcode = PostcodeSanitizer.call(postcode)

    if @mp_passed_away
      stub_api_request_for(sanitized_postcode).to_return(api_response(:ok) {
        <<-XML.strip
          <Constituencies>
            <Constituency>
              <Constituency_Id>#{@my_constituency.external_id}</Constituency_Id>
              <Name>#{@my_constituency.name}</Name>
              <ONSCode>#{@my_constituency.ons_code}</ONSCode>
              <RepresentingMembers>
                <RepresentingMember>
                  <Member_Id>#{@my_constituency.mp_id}</Member_Id>
                  <Member>#{@my_constituency.mp_name}</Member>
                  <StartDate>#{@my_constituency.mp_date.iso8601}</StartDate>
                  <EndDate>#{1.day.ago.iso8601}</EndDate>
                </RepresentingMember>
            </Constituency>
          </Constituencies>
        XML
      })
    else
      stub_api_request_for(sanitized_postcode).to_return(api_response(:ok) {
        <<-XML.strip
          <Constituencies>
            <Constituency>
              <Constituency_Id>#{@my_constituency.external_id}</Constituency_Id>
              <Name>#{@my_constituency.name}</Name>
              <ONSCode>#{@my_constituency.ons_code}</ONSCode>
              <RepresentingMembers>
                <RepresentingMember>
                  <Member_Id>#{@my_constituency.mp_id}</Member_Id>
                  <Member>#{@my_constituency.mp_name}</Member>
                  <StartDate>#{@my_constituency.mp_date.iso8601}</StartDate>
                  <EndDate xsi:nil="true"
                           xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>
                </RepresentingMember>
            </Constituency>
          </Constituencies>
        XML
      })
    end
  end

  within :css, '.local-to-you' do
    fill_in "UK postcode", with: postcode
    click_on "Search"
  end
end
When(/^I click the view all local petitions$/) do
  click_on "View all popular petitions in #{@my_constituency.name}"
end
When(/^I click the JSON link$/) do
  click_on "JSON"
end
Then(/^the JSON should be valid$/) do
  expect { JSON.parse(page.body) }.not_to raise_error
end
When(/^I click the CSV link$/) do
  click_on "CSV"
end
Then(/^#{capture_model} should be #{capture_model}$/) do |a, b|
  expect(model!(a)).to eq model!(b)
end
Given(/^an? (open|closed|rejected) petition "(.*?)" with some (fraudulent)? ?signatures$/) do |state, petition_action, signature_state|
  petition_closed_at = state == 'closed' ? 1.day.ago : nil
  petition_state = state == 'closed' ? 'open' : state
  petition_args = {
    action: petition_action,
    open_at: 3.months.ago,
    closed_at: petition_closed_at
  }
  @petition = FactoryGirl.create(:"#{state}_petition", petition_args)
  signature_state ||= "validated"
  5.times { FactoryGirl.create(:"#{signature_state}_signature", petition: @petition) }
end
