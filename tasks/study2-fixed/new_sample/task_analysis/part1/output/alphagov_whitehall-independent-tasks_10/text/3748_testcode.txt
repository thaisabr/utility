When /^I import the following data as CSV as "([^"]*)" for "([^"]*)":$/ do |document_type, organisation_name, data|
  organisation = Organisation.find_by_name(organisation_name) || create(:organisation, name: organisation_name)
  import_data_as_document_type_for_organisation(data, document_type, organisation)
end
Then /^the import succeeds, creating (\d+) imported publications? for "([^"]*)" with no publication date$/ do |edition_count, organisation_name|
  organisation = Organisation.find_by_name(organisation_name)
  assert_equal edition_count.to_i, Edition.imported.count

  edition = Edition.imported.first
  assert_kind_of Publication, edition
  assert_equal organisation, edition.organisations.first
  assert_nil edition.publication_date
end
Then /^the import succeeds, creating (\d+) imported speech(?:es)? with "([^"]*)" speech type and with no deliverer set$/ do |edition_count, speech_type_slug|
  speech_type = SpeechType.find_by_slug(speech_type_slug)
  assert_equal edition_count.to_i, Edition.imported.count

  edition = Edition.imported.first
  assert_kind_of Speech, edition
  assert_equal speech_type, edition.speech_type
  assert_nil edition.role_appointment
end
Then /^the import succeeds, creating (\d+) imported speech(?:es)? for "([^"]*)" with no delivered on date$/ do |edition_count, organisation_name|
  organisation = Organisation.find_by_name(organisation_name)
  assert_equal edition_count.to_i, Edition.imported.count

  edition = Edition.imported.first
  assert_kind_of Speech, edition
  assert_equal organisation, edition.organisations.first
  assert_nil edition.delivered_on
end
Then /^the import succeeds, creating (\d+) imported news articles? for "([^"]*)" with "([^"]*)" news article type$/ do |edition_count, organisation_name, news_article_type_slug|
  organisation = Organisation.find_by_name(organisation_name)
  news_article_type  = NewsArticleType.find_by_slug(news_article_type_slug)
  assert_equal edition_count.to_i, Edition.imported.count

  edition = Edition.imported.first
  assert_kind_of NewsArticle, edition
  assert_equal organisation, edition.organisations.first
  assert_equal news_article_type, edition.news_article_type
end
Then /^the import succeeds, creating (\d+) imported news articles? for "([^"]*)" with no first published date$/ do |edition_count, organisation_name|
  organisation = Organisation.find_by_name(organisation_name)
  assert_equal edition_count.to_i, Edition.imported.count

  edition = Edition.imported.first
  assert_kind_of NewsArticle, edition
  assert_equal organisation, edition.organisations.first
  assert_nil edition.first_published_at
end
Then /^the import succeeds, creating (\d+) imported consultations? for "([^"]*)" with no opening or closing date$/ do |edition_count, organisation_name|
  organisation = Organisation.find_by_name(organisation_name)
  assert_equal edition_count.to_i, Edition.imported.count

  edition = Edition.imported.first
  assert_kind_of Consultation, edition
  assert_equal organisation, edition.organisations.first
  assert_nil edition.opening_on
  assert_nil edition.closing_on
end
Then /^I can't make the imported (?:publication|speech|news article|consultation) into a draft edition yet$/ do
  visit_document_preview Edition.imported.last.title

  assert page.has_css?('input[type=submit][disabled=disabled][value="Convert to draft"]')
end
When /^I set the imported publication's publication date to "([^"]*)"$/ do |new_publication_date|
  begin_editing_document Edition.imported.last.title
  select_date "Publication date", with: new_publication_date
  click_on 'Save'
end
When /^I set the imported news article's first published date to "([^"]*)"$/ do |new_first_published_date|
  begin_editing_document Edition.imported.last.title
  select_date "First published at", with: new_first_published_date
  click_on 'Save'
end
Then /^I can make the imported (?:publication|speech|news article|consultation) into a draft edition$/ do
  edition = Edition.imported.last
  visit_document_preview edition.title

  click_on 'Convert to draft'

  edition.reload
  assert edition.draft?
end
When /^I set the imported speech's delivered on date to "([^"]*)"$/ do |new_delivered_on_date|
  begin_editing_document Edition.imported.last.title
  select_date "Delivered on", with: new_delivered_on_date
  click_on 'Save'
end
When /^I set the deliverer of the speech to "([^"]*)" from the "([^"]*)"$/ do |person_name, organisation_name|
  person = find_or_create_person(person_name)
  organisation = create(:ministerial_department, name: organisation_name)
  role = create(:role, organisations: [organisation])
  create(:role_appointment, role: role, person: person)

  begin_editing_document Edition.imported.last.title
  select person_name, from: 'Delivered by'
  click_on 'Save'
end
When /^I set the imported consultation's opening date to "([^"]*)"$/ do |new_opening_date|
  begin_editing_document Edition.imported.last.title
  select_date "Opening Date", with: new_opening_date
  click_on 'Save'
end
When /^I set the imported consultation's closing date to "([^"]*)"$/ do |new_closing_date|
  begin_editing_document Edition.imported.last.title
  select_date "Closing Date", with: new_closing_date
  click_on 'Save'
end
  def import_data_as_document_type_for_organisation(data, document_type, organisation)
    make_sure_data_importer_user_exists
    Import.use_separate_connection

    with_import_csv_file(data) do |path|
      visit new_admin_import_path
      select document_type, from: 'Type'
      attach_file 'CSV File', path
      select organisation.name, from: 'Default organisation'
      click_button 'Save'
      click_button 'Run'

      run_last_import

      visit current_path
    end
    Import.find(current_path.match(/admin\/imports\/(\d+)\Z/)[1])
  end
  def begin_editing_document(title)
    visit_document_preview title
    click_link "Edit"
  end
  def visit_document_preview(title, scope = :scoped)
    document = Edition.send(scope).find_by_title(title)
    visit admin_edition_path(document)
  end
  def find_or_create_person(name)
    find_person(name) || create_person(name)
  end
  def run_last_import
    Import.last.perform
  end
  def with_import_csv_file(data)
    tf = Tempfile.new('csv_import')
    tf << data
    tf.close
    yield tf.path
    tf.unlink
  end
  def make_sure_data_importer_user_exists
    unless User.find_by_name('Automatic Data Importer')
      create(:importer, name: 'Automatic Data Importer')
    end
  end
  def visit_document_preview(title, scope = :scoped)
    document = Edition.send(scope).find_by_title(title)
    visit admin_edition_path(document)
  end
  def create_person(name)
    create(:person, split_person_name(name))
  end
  def find_person(name)
    Person.where(split_person_name(name)).first
  end
  def split_person_name(name)
    if match = /^(\w+)\s*(.*?)$/.match(name)
      forename, surname = match.captures
      { title: nil, forename: forename, surname: surname, letters: nil }
    else
      raise "couldn't split \"#{name}\""
    end
  end
  def admin_edition_path(edition, *args)
    polymorphic_path([:admin, edition], *args)
  end
Given /^I am (?:a|an) (writer|editor|admin|GDS editor|importer)(?: called "([^"]*)")?$/ do |role, name|
  @user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Editor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  when 'importer'
    create(:importer)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    PaperTrail.whodunnit = user
    super(user) # warden
  end
Given /^a person called "([^"]*)"$/ do |name|
  create_person(name)
end
  def create_person(name)
    create(:person, split_person_name(name))
  end
Given /^"([^"]*)" is the "([^"]*)" for the "([^"]*)"$/ do |person_name, ministerial_role, organisation_name|
  create_role_appointment(person_name, ministerial_role, organisation_name, 2.years.ago)
end
  def create_role_appointment(person_name, role_name, organisation_name, timespan)
    person = find_or_create_person(person_name)
    organisation = Organisation.find_by_name(organisation_name) || create(:ministerial_department, name: organisation_name)
    role = MinisterialRole.create!(name: role_name)
    organisation.ministerial_roles << role

    if timespan.is_a?(Hash)
      started_at = timespan.keys.first
      ended_at = timespan.values.first
    else
      started_at = timespan
      ended_at = nil
    end

    create(:role_appointment, role: role, person: person, started_at: started_at, ended_at: ended_at)
  end
