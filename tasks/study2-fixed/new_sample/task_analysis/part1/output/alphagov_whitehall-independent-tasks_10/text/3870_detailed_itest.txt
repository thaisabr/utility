Classes: 8
[name:GDS, file:null, step:Given ]
[name:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Given ]
[name:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Then ]
[name:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:When ]
[name:PaperTrail, file:alphagov_whitehall/config/initializers/paper_trail.rb, step:Given ]
[name:Person, file:alphagov_whitehall/app/models/person.rb, step:Then ]
[name:THE_DOCUMENT, file:null, step:Then ]
[name:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]

Methods: 110
[name:about, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:When ]
[name:about, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:When ]
[name:about, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:Then ]
[name:about, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:Then ]
[name:address, type:Object, file:null, step:When ]
[name:address, type:Object, file:null, step:Then ]
[name:ago, type:Object, file:null, step:Then ]
[name:announcements_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:When ]
[name:announcements_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:Then ]
[name:captures, type:Object, file:null, step:Given ]
[name:captures, type:Object, file:null, step:Then ]
[name:click_on, type:Object, file:null, step:When ]
[name:click_on, type:Object, file:null, step:Then ]
[name:contact_form_url, type:Object, file:null, step:When ]
[name:contact_form_url, type:Object, file:null, step:Then ]
[name:contact_numbers, type:Object, file:null, step:When ]
[name:contact_numbers, type:Object, file:null, step:Then ]
[name:create, type:Object, file:null, step:Given ]
[name:create_person, type:PersonHelper, file:alphagov_whitehall/features/support/person_helper.rb, step:Given ]
[name:current_person, type:Role, file:alphagov_whitehall/app/models/role.rb, step:Then ]
[name:current_person, type:RolePresenter, file:alphagov_whitehall/app/presenters/role_presenter.rb, step:Then ]
[name:current_person_name, type:Role, file:alphagov_whitehall/app/models/role.rb, step:Then ]
[name:description, type:Object, file:null, step:When ]
[name:description, type:Object, file:null, step:Then ]
[name:destroyable?, type:Role, file:alphagov_whitehall/app/models/role.rb, step:Then ]
[name:each, type:EditionCollectionPresenter, file:alphagov_whitehall/app/presenters/edition_collection_presenter.rb, step:Then ]
[name:edit, type:Admin/rolesController, file:alphagov_whitehall/app/controllers/admin/roles_controller.rb, step:Then ]
[name:email, type:Object, file:null, step:When ]
[name:email, type:Object, file:null, step:Then ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find_by_name, type:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Given ]
[name:find_by_name!, type:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Then ]
[name:find_by_name!, type:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:When ]
[name:find_person, type:PersonHelper, file:alphagov_whitehall/features/support/person_helper.rb, step:Then ]
[name:first, type:Object, file:null, step:Then ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:When ]
[name:id, type:Object, file:null, step:Then ]
[name:index, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:When ]
[name:index, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:When ]
[name:index, type:PublicationsController, file:alphagov_whitehall/app/controllers/admin/publications_controller.rb, step:When ]
[name:index, type:PublicationsController, file:alphagov_whitehall/app/controllers/publications_controller.rb, step:When ]
[name:index, type:Admin/rolesController, file:alphagov_whitehall/app/controllers/admin/roles_controller.rb, step:Then ]
[name:index, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:Then ]
[name:index, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:Then ]
[name:index, type:PublicationsController, file:alphagov_whitehall/app/controllers/admin/publications_controller.rb, step:Then ]
[name:index, type:PublicationsController, file:alphagov_whitehall/app/controllers/publications_controller.rb, step:Then ]
[name:login_as, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:Given ]
[name:management_roles, type:Object, file:null, step:Then ]
[name:ministerial_roles, type:PublicationRow, file:alphagov_whitehall/lib/whitehall/uploader/publication_row.rb, step:Then ]
[name:name, type:Person, file:alphagov_whitehall/app/models/person.rb, step:When ]
[name:name, type:RolePresenter, file:alphagov_whitehall/app/presenters/role_presenter.rb, step:When ]
[name:name, type:DataMigration, file:alphagov_whitehall/lib/whitehall/data_migration.rb, step:When ]
[name:name, type:Role, file:alphagov_whitehall/app/models/role.rb, step:Then ]
[name:name, type:Person, file:alphagov_whitehall/app/models/person.rb, step:Then ]
[name:name, type:RolePresenter, file:alphagov_whitehall/app/presenters/role_presenter.rb, step:Then ]
[name:name, type:DataMigration, file:alphagov_whitehall/lib/whitehall/data_migration.rb, step:Then ]
[name:new, type:Admin/rolesController, file:alphagov_whitehall/app/controllers/admin/roles_controller.rb, step:Then ]
[name:new, type:Admin/roleAppointmentsController, file:alphagov_whitehall/app/controllers/admin/role_appointments_controller.rb, step:Then ]
[name:organisation_names, type:Role, file:alphagov_whitehall/app/models/role.rb, step:Then ]
[name:page, type:DetailedGuidePresenter, file:alphagov_whitehall/app/presenters/api/detailed_guide_presenter.rb, step:Then ]
[name:person, type:Appointment, file:alphagov_whitehall/app/models/edition/appointment.rb, step:When ]
[name:person, type:RoleAppointmentPresenter, file:alphagov_whitehall/app/presenters/role_appointment_presenter.rb, step:When ]
[name:person, type:Appointment, file:alphagov_whitehall/app/models/edition/appointment.rb, step:Then ]
[name:person, type:RoleAppointmentPresenter, file:alphagov_whitehall/app/presenters/role_appointment_presenter.rb, step:Then ]
[name:policies_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:When ]
[name:policies_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:Then ]
[name:postcode, type:Object, file:null, step:When ]
[name:postcode, type:Object, file:null, step:Then ]
[name:public_document_path, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:When ]
[name:public_document_path, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:Then ]
[name:public_document_url, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:When ]
[name:public_document_url, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:Then ]
[name:publications_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:When ]
[name:publications_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:Then ]
[name:raise, type:Object, file:null, step:Given ]
[name:raise, type:Object, file:null, step:Then ]
[name:record_css_selector, type:Object, file:null, step:Then ]
[name:roles, type:Object, file:null, step:Then ]
[name:select, type:Object, file:null, step:When ]
[name:select, type:Object, file:null, step:Then ]
[name:select_date, type:Object, file:null, step:Then ]
[name:show, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:When ]
[name:show, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:When ]
[name:show, type:CorporateInformationPagesController, file:alphagov_whitehall/app/controllers/admin/corporate_information_pages_controller.rb, step:When ]
[name:show, type:CorporateInformationPagesController, file:alphagov_whitehall/app/controllers/corporate_information_pages_controller.rb, step:When ]
[name:show, type:TopicsController, file:alphagov_whitehall/app/controllers/admin/topics_controller.rb, step:When ]
[name:show, type:TopicsController, file:alphagov_whitehall/app/controllers/topics_controller.rb, step:When ]
[name:show, type:DocumentSeriesController, file:alphagov_whitehall/app/controllers/admin/document_series_controller.rb, step:When ]
[name:show, type:DocumentSeriesController, file:alphagov_whitehall/app/controllers/document_series_controller.rb, step:When ]
[name:show, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:Then ]
[name:show, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:Then ]
[name:show, type:CorporateInformationPagesController, file:alphagov_whitehall/app/controllers/admin/corporate_information_pages_controller.rb, step:Then ]
[name:show, type:CorporateInformationPagesController, file:alphagov_whitehall/app/controllers/corporate_information_pages_controller.rb, step:Then ]
[name:show, type:TopicsController, file:alphagov_whitehall/app/controllers/admin/topics_controller.rb, step:Then ]
[name:show, type:TopicsController, file:alphagov_whitehall/app/controllers/topics_controller.rb, step:Then ]
[name:show, type:DocumentSeriesController, file:alphagov_whitehall/app/controllers/admin/document_series_controller.rb, step:Then ]
[name:show, type:DocumentSeriesController, file:alphagov_whitehall/app/controllers/document_series_controller.rb, step:Then ]
[name:split_person_name, type:PersonHelper, file:alphagov_whitehall/features/support/person_helper.rb, step:Given ]
[name:split_person_name, type:PersonHelper, file:alphagov_whitehall/features/support/person_helper.rb, step:Then ]
[name:text, type:Object, file:null, step:When ]
[name:text, type:Object, file:null, step:Then ]
[name:title, type:DetailedGuide, file:alphagov_whitehall/app/models/detailed_guide.rb, step:When ]
[name:title, type:DetailedGuide, file:alphagov_whitehall/app/models/detailed_guide.rb, step:Then ]
[name:to_s, type:Role, file:alphagov_whitehall/app/models/role.rb, step:Then ]
[name:traffic_commissioner_roles, type:Object, file:null, step:Then ]
[name:type, type:Role, file:alphagov_whitehall/app/models/role.rb, step:Then ]
[name:visit_organisation, type:Paths, file:alphagov_whitehall/features/support/paths.rb, step:When ]
[name:visit_organisation, type:Paths, file:alphagov_whitehall/features/support/paths.rb, step:Then ]
[name:where, type:Person, file:alphagov_whitehall/app/models/person.rb, step:Then ]

Referenced pages: 27
alphagov_whitehall/app/views/admin/editions/_govspeak_help.html.erb
alphagov_whitehall/app/views/admin/role_appointments/_form.html.erb
alphagov_whitehall/app/views/admin/role_appointments/_list.html.erb
alphagov_whitehall/app/views/admin/role_appointments/new.html.erb
alphagov_whitehall/app/views/admin/roles/_form.html.erb
alphagov_whitehall/app/views/admin/roles/edit.html.erb
alphagov_whitehall/app/views/admin/roles/index.html.erb
alphagov_whitehall/app/views/admin/roles/new.html.erb
alphagov_whitehall/app/views/announcements/_list_description.html.erb
alphagov_whitehall/app/views/corporate_information_pages/show.html.erb
alphagov_whitehall/app/views/document_series/show.html.erb
alphagov_whitehall/app/views/documents/_filter_form.html.erb
alphagov_whitehall/app/views/documents/_filter_results.html.erb
alphagov_whitehall/app/views/organisations/_header.html.erb
alphagov_whitehall/app/views/organisations/_news.html.erb
alphagov_whitehall/app/views/organisations/_organisations_logo_list.html.erb
alphagov_whitehall/app/views/organisations/_social_media_accounts.html.erb
alphagov_whitehall/app/views/organisations/about.html.erb
alphagov_whitehall/app/views/organisations/index.html.erb
alphagov_whitehall/app/views/organisations/show.html.erb
alphagov_whitehall/app/views/people/_person.html.erb
alphagov_whitehall/app/views/policies/_list_description.html.erb
alphagov_whitehall/app/views/publications/_list_description.html.erb
alphagov_whitehall/app/views/publications/index.html.erb
alphagov_whitehall/app/views/shared/_recently_updated.html.erb
alphagov_whitehall/app/views/topics/_list_description.html.erb
alphagov_whitehall/app/views/topics/show.html.erb

