Classes: 8
[name:CorporateInformationPage, file:alphagov_whitehall/app/models/corporate_information_page.rb, step:null]
[name:CorporateInformationPage, file:alphagov_whitehall/test/factories/corporate_information_page.rb, step:null]
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:null]
[name:NewsArticle, file:alphagov_whitehall/app/models/news_article.rb, step:When ]
[name:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:When ]
[name:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Given ]
[name:Rails, file:null, step:When ]
[name:THE_DOCUMENT, file:null, step:Then ]

Methods: 91
[name:about, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:null]
[name:about, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:null]
[name:admin_edition_path, type:EditionRoutesHelper, file:alphagov_whitehall/app/helpers/admin/edition_routes_helper.rb, step:null]
[name:admin_edition_path, type:EditionRoutesHelper, file:alphagov_whitehall/app/helpers/admin/edition_routes_helper.rb, step:When ]
[name:admin_edition_url, type:EditionRoutesHelper, file:alphagov_whitehall/app/helpers/admin/edition_routes_helper.rb, step:null]
[name:admin_edition_url, type:EditionRoutesHelper, file:alphagov_whitehall/app/helpers/admin/edition_routes_helper.rb, step:When ]
[name:admin_supporting_page_path, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:null]
[name:admin_supporting_page_path, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:When ]
[name:admin_supporting_page_url, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:null]
[name:admin_supporting_page_url, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:When ]
[name:announcements_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:null]
[name:attach_file, type:Object, file:null, step:When ]
[name:captures, type:Object, file:null, step:When ]
[name:chiefs_of_staff, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:null]
[name:chiefs_of_staff, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:null]
[name:classify, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:constantize, type:Object, file:null, step:Given ]
[name:create, type:Object, file:null, step:Given ]
[name:document_class, type:DocumentHelper, file:alphagov_whitehall/features/support/document_helper.rb, step:Given ]
[name:edit, type:Admin/organisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:null]
[name:edit, type:Admin/corporateInformationPagesController, file:alphagov_whitehall/app/controllers/admin/corporate_information_pages_controller.rb, step:null]
[name:edit, type:Admin/editionOrganisationsController, file:alphagov_whitehall/app/controllers/admin/edition_organisations_controller.rb, step:null]
[name:edit, type:Admin/documentSeriesController, file:alphagov_whitehall/app/controllers/admin/document_series_controller.rb, step:null]
[name:feature, type:Object, file:null, step:null]
[name:feature, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:fill_in_person_name, type:PersonHelper, file:alphagov_whitehall/features/support/person_helper.rb, step:When ]
[name:find_by_name!, type:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:When ]
[name:find_by_name!, type:Organisation, file:alphagov_whitehall/app/models/organisation.rb, step:Given ]
[name:find_by_title, type:NewsArticle, file:alphagov_whitehall/app/models/news_article.rb, step:When ]
[name:gsub, type:Object, file:null, step:Given ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:index, type:Admin/editionsController, file:alphagov_whitehall/app/controllers/admin/editions_controller.rb, step:null]
[name:index, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:null]
[name:index, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:null]
[name:index, type:Admin/editionsController, file:alphagov_whitehall/app/controllers/admin/editions_controller.rb, step:When ]
[name:join, type:Object, file:null, step:When ]
[name:name, type:Person, file:alphagov_whitehall/app/models/person.rb, step:null]
[name:name, type:RolePresenter, file:alphagov_whitehall/app/presenters/role_presenter.rb, step:null]
[name:name, type:DataMigration, file:alphagov_whitehall/lib/whitehall/data_migration.rb, step:null]
[name:name, type:Person, file:alphagov_whitehall/app/models/person.rb, step:Given ]
[name:name, type:RolePresenter, file:alphagov_whitehall/app/presenters/role_presenter.rb, step:Given ]
[name:name, type:DataMigration, file:alphagov_whitehall/lib/whitehall/data_migration.rb, step:Given ]
[name:new, type:Admin/corporateInformationPagesController, file:alphagov_whitehall/app/controllers/admin/corporate_information_pages_controller.rb, step:null]
[name:new, type:Admin/documentSeriesController, file:alphagov_whitehall/app/controllers/admin/document_series_controller.rb, step:null]
[name:new, type:Admin/policiesController, file:alphagov_whitehall/app/controllers/admin/policies_controller.rb, step:null]
[name:new, type:Admin/publicationsController, file:alphagov_whitehall/app/controllers/admin/publications_controller.rb, step:null]
[name:new, type:Admin/newsArticlesController, file:alphagov_whitehall/app/controllers/admin/news_articles_controller.rb, step:null]
[name:new, type:Admin/consultationsController, file:alphagov_whitehall/app/controllers/admin/consultations_controller.rb, step:null]
[name:new, type:Admin/speechesController, file:alphagov_whitehall/app/controllers/admin/speeches_controller.rb, step:null]
[name:new, type:Admin/detailedGuidesController, file:alphagov_whitehall/app/controllers/admin/detailed_guides_controller.rb, step:null]
[name:new, type:Admin/internationalPrioritiesController, file:alphagov_whitehall/app/controllers/admin/international_priorities_controller.rb, step:null]
[name:new, type:Admin/caseStudiesController, file:alphagov_whitehall/app/controllers/admin/case_studies_controller.rb, step:null]
[name:new, type:Admin/statisticalDataSetsController, file:alphagov_whitehall/app/controllers/admin/statistical_data_sets_controller.rb, step:null]
[name:new, type:Admin/policiesController, file:alphagov_whitehall/app/controllers/admin/policies_controller.rb, step:When ]
[name:new, type:Admin/publicationsController, file:alphagov_whitehall/app/controllers/admin/publications_controller.rb, step:When ]
[name:new, type:Admin/newsArticlesController, file:alphagov_whitehall/app/controllers/admin/news_articles_controller.rb, step:When ]
[name:new, type:Admin/consultationsController, file:alphagov_whitehall/app/controllers/admin/consultations_controller.rb, step:When ]
[name:new, type:Admin/speechesController, file:alphagov_whitehall/app/controllers/admin/speeches_controller.rb, step:When ]
[name:new, type:Admin/detailedGuidesController, file:alphagov_whitehall/app/controllers/admin/detailed_guides_controller.rb, step:When ]
[name:new, type:Admin/internationalPrioritiesController, file:alphagov_whitehall/app/controllers/admin/international_priorities_controller.rb, step:When ]
[name:new, type:Admin/caseStudiesController, file:alphagov_whitehall/app/controllers/admin/case_studies_controller.rb, step:When ]
[name:new, type:Admin/statisticalDataSetsController, file:alphagov_whitehall/app/controllers/admin/statistical_data_sets_controller.rb, step:When ]
[name:policies_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:null]
[name:public_document_path, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:null]
[name:public_document_url, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:null]
[name:publications_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:null]
[name:published_at, type:Edition, file:alphagov_whitehall/app/models/edition.rb, step:null]
[name:raise, type:Object, file:null, step:When ]
[name:record_css_selector, type:Object, file:null, step:When ]
[name:record_css_selector, type:Object, file:null, step:Then ]
[name:show, type:Admin/organisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:null]
[name:show, type:OrganisationsController, file:alphagov_whitehall/app/controllers/admin/organisations_controller.rb, step:null]
[name:show, type:OrganisationsController, file:alphagov_whitehall/app/controllers/organisations_controller.rb, step:null]
[name:show, type:Admin/documentSeriesController, file:alphagov_whitehall/app/controllers/admin/document_series_controller.rb, step:null]
[name:show, type:CorporateInformationPagesController, file:alphagov_whitehall/app/controllers/admin/corporate_information_pages_controller.rb, step:null]
[name:show, type:CorporateInformationPagesController, file:alphagov_whitehall/app/controllers/corporate_information_pages_controller.rb, step:null]
[name:show, type:TopicsController, file:alphagov_whitehall/app/controllers/admin/topics_controller.rb, step:null]
[name:show, type:TopicsController, file:alphagov_whitehall/app/controllers/topics_controller.rb, step:null]
[name:split_person_name, type:PersonHelper, file:alphagov_whitehall/features/support/person_helper.rb, step:When ]
[name:summary, type:Policy, file:alphagov_whitehall/app/models/policy.rb, step:null]
[name:title, type:Edition, file:alphagov_whitehall/app/models/edition.rb, step:null]
[name:title, type:CorporateInformationPage, file:alphagov_whitehall/app/models/corporate_information_page.rb, step:null]
[name:title, type:ConsultationUploader, file:alphagov_whitehall/lib/consultation_uploader.rb, step:null]
[name:title, type:Policy, file:alphagov_whitehall/app/models/policy.rb, step:null]
[name:type, type:Edition, file:alphagov_whitehall/app/models/edition.rb, step:null]
[name:underscore, type:Object, file:null, step:Given ]
[name:visit_people_admin, type:PersonSteps, file:alphagov_whitehall/features/step_definitions/person_steps.rb, step:When ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 58
alphagov_whitehall/app/views/admin/case_studies/_form.html.erb
alphagov_whitehall/app/views/admin/consultations/_additional_significant_fields.html.erb
alphagov_whitehall/app/views/admin/consultations/_edition.html.erb
alphagov_whitehall/app/views/admin/consultations/_form.html.erb
alphagov_whitehall/app/views/admin/corporate_information_pages/_form.html.erb
alphagov_whitehall/app/views/admin/corporate_information_pages/edit.html.erb
alphagov_whitehall/app/views/admin/corporate_information_pages/new.html.erb
alphagov_whitehall/app/views/admin/detailed_guides/_edition.html.erb
alphagov_whitehall/app/views/admin/detailed_guides/_form.html.erb
alphagov_whitehall/app/views/admin/document_series/_form.html.erb
alphagov_whitehall/app/views/admin/document_series/edit.html.erb
alphagov_whitehall/app/views/admin/document_series/new.html.erb
alphagov_whitehall/app/views/admin/document_series/show.html.erb
alphagov_whitehall/app/views/admin/edition_organisations/edit.html.erb
alphagov_whitehall/app/views/admin/editions/_attachment_fields.html.erb
alphagov_whitehall/app/views/admin/editions/_govspeak_help.html.erb
alphagov_whitehall/app/views/admin/editions/index.html.erb
alphagov_whitehall/app/views/admin/international_priorities/_form.html.erb
alphagov_whitehall/app/views/admin/news_articles/_form.html.erb
alphagov_whitehall/app/views/admin/organisations/_contacts_form.html.erb
alphagov_whitehall/app/views/admin/organisations/_form.html.erb
alphagov_whitehall/app/views/admin/organisations/_sidebar.html.erb
alphagov_whitehall/app/views/admin/organisations/edit.html.erb
alphagov_whitehall/app/views/admin/organisations/show.html.erb
alphagov_whitehall/app/views/admin/organisations/tabs/_about_us.html.erb
alphagov_whitehall/app/views/admin/organisations/tabs/_contacts.html.erb
alphagov_whitehall/app/views/admin/organisations/tabs/_corporate_information_pages.html.erb
alphagov_whitehall/app/views/admin/organisations/tabs/_details.html.erb
alphagov_whitehall/app/views/admin/organisations/tabs/_document_series.html.erb
alphagov_whitehall/app/views/admin/organisations/tabs/_documents.html.erb
alphagov_whitehall/app/views/admin/organisations/tabs/_people.html.erb
alphagov_whitehall/app/views/admin/policies/_form.html.erb
alphagov_whitehall/app/views/admin/publications/_additional_significant_fields.html.erb
alphagov_whitehall/app/views/admin/publications/_edition.html.erb
alphagov_whitehall/app/views/admin/publications/_form.html.erb
alphagov_whitehall/app/views/admin/speeches/_form.html.erb
alphagov_whitehall/app/views/admin/statistical_data_sets/_form.html.erb
alphagov_whitehall/app/views/announcements/_list_description.html.erb
alphagov_whitehall/app/views/corporate_information_pages/show.html.erb
alphagov_whitehall/app/views/documents/_attachment.html.erb
alphagov_whitehall/app/views/documents/_attachments.html.erb
alphagov_whitehall/app/views/documents/_recently_changed_documents.html.erb
alphagov_whitehall/app/views/organisations/_grouped_list.html.erb
alphagov_whitehall/app/views/organisations/_header.html.erb
alphagov_whitehall/app/views/organisations/_news.html.erb
alphagov_whitehall/app/views/organisations/_organisations_logo_list.html.erb
alphagov_whitehall/app/views/organisations/_organisations_name_list.html.erb
alphagov_whitehall/app/views/organisations/_social_media_accounts.html.erb
alphagov_whitehall/app/views/organisations/about.html.erb
alphagov_whitehall/app/views/organisations/external.html.erb
alphagov_whitehall/app/views/organisations/index.html.erb
alphagov_whitehall/app/views/organisations/show.html.erb
alphagov_whitehall/app/views/policies/_list_description.html.erb
alphagov_whitehall/app/views/publications/_list_description.html.erb
alphagov_whitehall/app/views/shared/_recently_updated.html.erb
alphagov_whitehall/app/views/topics/_list_description.html.erb
alphagov_whitehall/app/views/topics/_related_topics.html.erb
alphagov_whitehall/app/views/topics/show.html.erb

