Classes: 4
[name:GDS, file:null, step:Given ]
[name:PaperTrail, file:alphagov_whitehall/config/initializers/paper_trail.rb, step:Given ]
[name:Publication, file:alphagov_whitehall/app/models/publication.rb, step:When ]
[name:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]

Methods: 35
[name:admin_edition_path, type:EditionRoutesHelper, file:alphagov_whitehall/app/helpers/admin/edition_routes_helper.rb, step:Given ]
[name:admin_edition_url, type:EditionRoutesHelper, file:alphagov_whitehall/app/helpers/admin/edition_routes_helper.rb, step:Given ]
[name:admin_supporting_page_path, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:Given ]
[name:admin_supporting_page_url, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:Given ]
[name:classify, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:constantize, type:Object, file:null, step:Given ]
[name:create, type:Object, file:null, step:Given ]
[name:document, type:Object, file:null, step:Given ]
[name:document_class, type:DocumentHelper, file:alphagov_whitehall/features/support/document_helper.rb, step:Given ]
[name:document_sources, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find_by_title!, type:Publication, file:alphagov_whitehall/app/models/publication.rb, step:When ]
[name:gsub, type:Object, file:null, step:Given ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:has_field?, type:Object, file:null, step:When ]
[name:has_field?, type:Object, file:null, step:Then ]
[name:index, type:Admin/editionsController, file:alphagov_whitehall/app/controllers/admin/editions_controller.rb, step:Given ]
[name:login_as, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:Given ]
[name:name, type:Person, file:alphagov_whitehall/app/models/person.rb, step:Given ]
[name:name, type:RolePresenter, file:alphagov_whitehall/app/presenters/role_presenter.rb, step:Given ]
[name:name, type:DataMigration, file:alphagov_whitehall/lib/whitehall/data_migration.rb, step:Given ]
[name:new, type:Admin/policiesController, file:alphagov_whitehall/app/controllers/admin/policies_controller.rb, step:Given ]
[name:new, type:Admin/publicationsController, file:alphagov_whitehall/app/controllers/admin/publications_controller.rb, step:Given ]
[name:new, type:Admin/newsArticlesController, file:alphagov_whitehall/app/controllers/admin/news_articles_controller.rb, step:Given ]
[name:new, type:Admin/fatalityNoticesController, file:alphagov_whitehall/app/controllers/admin/fatality_notices_controller.rb, step:Given ]
[name:new, type:Admin/consultationsController, file:alphagov_whitehall/app/controllers/admin/consultations_controller.rb, step:Given ]
[name:new, type:Admin/speechesController, file:alphagov_whitehall/app/controllers/admin/speeches_controller.rb, step:Given ]
[name:new, type:Admin/detailedGuidesController, file:alphagov_whitehall/app/controllers/admin/detailed_guides_controller.rb, step:Given ]
[name:new, type:Admin/internationalPrioritiesController, file:alphagov_whitehall/app/controllers/admin/international_priorities_controller.rb, step:Given ]
[name:new, type:Admin/caseStudiesController, file:alphagov_whitehall/app/controllers/admin/case_studies_controller.rb, step:Given ]
[name:new, type:Admin/statisticalDataSetsController, file:alphagov_whitehall/app/controllers/admin/statistical_data_sets_controller.rb, step:Given ]
[name:underscore, type:Object, file:null, step:Given ]
[name:within, type:Object, file:null, step:Then ]

Referenced pages: 19
alphagov_whitehall/app/views/admin/case_studies/_form.html.erb
alphagov_whitehall/app/views/admin/consultations/_additional_significant_fields.html.erb
alphagov_whitehall/app/views/admin/consultations/_edition.html.erb
alphagov_whitehall/app/views/admin/consultations/_form.html.erb
alphagov_whitehall/app/views/admin/detailed_guides/_edition.html.erb
alphagov_whitehall/app/views/admin/detailed_guides/_form.html.erb
alphagov_whitehall/app/views/admin/editions/index.html.erb
alphagov_whitehall/app/views/admin/fatality_notices/_form.html.erb
alphagov_whitehall/app/views/admin/international_priorities/_form.html.erb
alphagov_whitehall/app/views/admin/news_articles/_form.html.erb
alphagov_whitehall/app/views/admin/policies/_form.html.erb
alphagov_whitehall/app/views/admin/publications/_additional_significant_fields.html.erb
alphagov_whitehall/app/views/admin/publications/_edition.html.erb
alphagov_whitehall/app/views/admin/publications/_form.html.erb
alphagov_whitehall/app/views/admin/speeches/_form.html.erb
alphagov_whitehall/app/views/admin/statistical_data_sets/_form.html.erb
alphagov_whitehall/app/views/admin/statistical_data_sets/_list.html.erb
alphagov_whitehall/app/views/documents/_attachment.html.erb
alphagov_whitehall/app/views/documents/_attachments.html.erb

