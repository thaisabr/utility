Feature: As Charlie
In order to have an issue discussed in parliament
I want to be able to create a petition and verify my email address.
Background:
Given a department exists with name: "Cabinet Office", description: "Where cabinets do their paperwork"
And a department exists with name: "Department for International Development", description: "A large portion of the UK population cannot intonate their words properly. This department is responsible for developing this."
Scenario: Charlie creates our petition
Given I am on the new petition page
Then I should see "Create a new e-petition - e-petitions" in the browser page title
And I should be connected to the server via an ssl connection
When I fill in "Title" with "The wombats of wimbledon rock."
And I fill in "Action" with "Give half of Wimbledon rock to wombats!"
And I select "Department for International Development" from "Department"
And I fill in "Description" with "The racial tensions between the wombles and the wombats are heating up. Racial attacks are a regular occurrence and the death count is already in 5 figures. The only resolution to this crisis is to give half of Wimbledon common to the Wombats and to recognise them as their own independent state."
And I select "3 months" from "Time to collect signatures"
And I press "Next"
And I fill in my details
And I press "Next"
And I fill in sponsor emails
And I press "Next"
And I check "I agree to the Terms & Conditions"
Then the markup should be valid
When I press "Submit"
Then a petition should exist with title: "The wombats of wimbledon rock.", state: "pending", duration: "3"
And there should be a "pending" signature with email "womboid@wimbledon.com" and name "Womboid Wibbledon"
And "Womboid Wibbledon" wants to be notified about the petition's progress
And "womboid@wimbledon.com" should receive 1 email
When I confirm my email address
Then a petition should exist with title: "The wombats of wimbledon rock.", state: "validated"
And there should be a "validated" signature with email "womboid@wimbledon.com" and name "Womboid Wibbledon"
@javascript
Scenario: Charlie tries to submit an invalid petition
Given I am on the new petition page
Then I should see a fieldset called "Petition Details"
When I press "Next"
Then I should see "Title must be completed"
And I should see "Action must be completed"
And I should see "Department must be completed"
And I should see "Description must be completed"
When I am allowed to make the petition title too long
When I fill in "Title" with "012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789Blah"
And I fill in "Action" with "This text is longer than 200 characters. 012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789"
And I fill in "Description" with "This text is longer than 1000 characters. 012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789"
And I press "Next"
Then I should see "Title is too long."
And I should see "Description is too long."
And I should see "Action is too long."
When I fill in "Title" with "The wombats of wimbledon rock."
And I select "Department for International Development" from "Department"
And I fill in "Action" with "Give half of Wimbledon rock to wombats!"
And I fill in "Description" with "The racial tensions between the wombles and the wombats are heating up.  Racial attacks are a regular occurrence and the death count is already in 5 figures.  The only resolution to this crisis is to give half of Wimbledon common to the Wombats and to recognise them as their own independent state."
And I press "Next"
Then I should see a fieldset called "Your Details"
When I press "Next"
Then I should see "Name must be completed"
And I should see "Email must be completed"
And I should see "You must be a British citizen"
And I should see "Address must be completed"
And I should see "Town must be completed"
And I should see "Postcode must be completed"
When I fill in my details with email "wimbledon@womble.com" and confirmation "uncleb@wimbledon.com"
And I press "Next"
And I should see "Email should match confirmation"
When I fill in my details
And I press "Next"
Then I should see a fieldset called "Sponsor email addresses"
And I fill in sponsor emails
And I press "Next"
Then I should see a fieldset called "Submit Petition"
And I press "Submit"
Then I should see "You must accept the terms and conditions."
When I check "I agree to the Terms & Conditions"
And I press "Back"
And I press "Back"
And I fill in "Name" with "Mr. Wibbledon"
And I press "Next"
And I press "Next"
And I press "Submit"
Then a petition should exist with title: "The wombats of wimbledon rock.", state: "pending"
Then there should be a "pending" signature with email "womboid@wimbledon.com" and name "Mr. Wibbledon"
Feature: Moderator respond to petition
In order to allow or prevent the signing of petitions
As a moderator
I want to respond to a petition for my department, accepting, rejecting or re-assigning it
Scenario: Accesing the petitions page
Given a sponsored petition exists with title: "More money for charities"
And I am logged in as a sysadmin
When I go to the admin moderate petitions page for "More money for charities"
Then I should be connected to the server via an ssl connection
And the markup should be valid
Scenario: Moderator rejects petition but with no reason code
Given I am logged in as an admin
And a sponsored petition exists with title: "Rupert Murdoch is on the run"
When I go to the Admin moderate petitions page for "Rupert Murdoch is on the run"
And I reject the petition with a reason code "-- Select a rejection code --"
Then a petition should exist with title: "Rupert Murdoch is on the run", state: "sponsored"
And I should see "can't be blank"
Feature: Maggie searches for a petition by id
In order to quickly find a petition and view the contents
As Maggie
I want to enter an id and be taken to the petition for that id, or shown an error if it doesn't exist
Scenario:
Given a set of petitions for the "Treasury"
And I am logged in as a moderator for the "Cabinet Office"
When I search for a petition by id
Then I should see the petition for viewing only
Scenario: A user from the same department sees the edit page if the petition needs moderation
Given a sponsored petition "Loose benefits!" belonging to the "Treasury"
And I am logged in as a moderator for the "Treasury"
When I search for a petition by id
Then I should see the petition for editing
Scenario: A threshold user sees the edit page if the petition needs moderation
Given a sponsored petition "Loose benefits!" belonging to the "Treasury"
And I am logged in as a threshold user
When I search for a petition by id
Then I should see the petition for editing
Scenario:
Given I am logged in as a threshold user
When I search for a petition by id
Then I should be taken back to the id search form with an error
Feature: Dashboard todo list
In order to see priority items
I can see a list of validated petitions on my todo list that need moderation
Background:
Given a department "DFID" exists with name: "DFID"
And a department "Treasury" exists with name: "Treasury"
And a department "Home Office" exists with name: "Home Office"
And a petition "p1" exists with title: "Petition 1", department: department "DFID", state: "sponsored", created_at: "2009-02-10"
And an open petition "p2" exists with title: "Petition 2", department: department "DFID", created_at: "2008-10-09"
And a petition "p3" exists with title: "Petition 3", department: department "Treasury", state: "sponsored", created_at: "2010-11-11"
And a petition "p4" exists with title: "Petition 4", department: department "Treasury", state: "sponsored", created_at: "2010-01-01"
And a rejected petition "p5" exists with title: "Petition 5", department: department "Home Office", created_at: "2007-01-01"
And a petition "p6" exists with title: "Petition 6", department: department "Treasury", state: "validated", created_at: "2010-01-01"
Scenario: Pending petitions are paginated
Given I am logged in as a sysadmin
And 20 petitions exist with state: "sponsored"
When I go to the admin todolist page
And I follow "Next"
Then I should see 3 rows in the admin index table
And I follow "Previous"
And I should see 20 rows in the admin index table
Feature: Charlie is notified to get an MP
As Charlie, creator of a petition with some sponsors
In order to ensure that I am up to date with how many of my sponsors have signed
I want to be emailed when they sign with a countdown to the threshold
Background:
Given I have created an e-petition with sponsors
Scenario: Charlie is emailed about sponsor support before passing the threshold
When a sponsor supports my e-petition
Then I should receive a sponsor support notification email
And the sponsor support notification email should include the countdown to the threshold
Scenario: Charlie is emailed about moderation upon hitting the sponsor support threshold
Given I only need one more sponsor to support my e-petition
When a sponsor supports my e-petition
Then I should receive a sponsor support notification email
And the sponsor support notification email should tell me about my e-petition going into moderation
Scenario: Charlie is no longer emailed about sponsor support after passing the threshold
Given I have enough support from sponsors for my e-petition
When a sponsor supports my e-petition
Then I should not receive a sponsor support notification email
