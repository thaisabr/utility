Classes: 24
[name:Department, file:alphagov_e-petitions/app/models/department.rb, step:Given ]
[name:Department, file:alphagov_e-petitions/app/models/department.rb, step:And ]
[name:Department, file:alphagov_e-petitions/app/models/department.rb, step:Then ]
[name:FactoryGirl, file:null, step:Given ]
[name:FactoryGirl, file:null, step:And ]
[name:FactoryGirl, file:null, step:When ]
[name:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:Given ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:Given ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/petition.rb, step:Given ]
[name:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:And ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:And ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/petition.rb, step:And ]
[name:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:When ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:When ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/petition.rb, step:When ]
[name:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:Then ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:Then ]
[name:Petition, file:alphagov_e-petitions/app/models/staged/petition.rb, step:Then ]
[name:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:null]
[name:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:null]
[name:Petition, file:alphagov_e-petitions/app/models/staged/petition.rb, step:null]
[name:Signature, file:alphagov_e-petitions/app/models/signature.rb, step:Then ]
[name:SignaturesController, file:alphagov_e-petitions/app/controllers/signatures_controller.rb, step:null]
[name:UserSessionsController, file:alphagov_e-petitions/app/controllers/admin/user_sessions_controller.rb, step:null]

Methods: 103
[name:ago, type:Object, file:null, step:Given ]
[name:all, type:Object, file:null, step:Then ]
[name:all, type:Department, file:alphagov_e-petitions/app/models/department.rb, step:Then ]
[name:all, type:Department, file:alphagov_e-petitions/app/models/department.rb, step:Given ]
[name:be_nil, type:Object, file:null, step:Then ]
[name:be_truthy, type:Object, file:null, step:Then ]
[name:blank?, type:Object, file:null, step:Then ]
[name:capture_fields, type:Object, file:null, step:Then ]
[name:capture_model, type:Object, file:null, step:Then ]
[name:capture_predicate, type:Object, file:null, step:Then ]
[name:check, type:Object, file:null, step:When ]
[name:choose, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:Then ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_first_link_in_email, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:create_model, type:Object, file:null, step:Then ]
[name:creator_signature, type:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:null]
[name:creator_signature, type:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:null]
[name:creator_signature, type:Petition, file:alphagov_e-petitions/app/models/staged/petition.rb, step:null]
[name:current_email, type:Object, file:null, step:When ]
[name:default_part_body, type:Object, file:null, step:When ]
[name:diff!, type:Object, file:null, step:Then ]
[name:each, type:Object, file:null, step:Then ]
[name:each, type:Object, file:null, step:Given ]
[name:edit, type:Admin/petitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:null]
[name:edit_internal_response, type:Admin/petitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:null]
[name:email, type:Object, file:null, step:Given ]
[name:email, type:Object, file:null, step:null]
[name:eq, type:Object, file:null, step:Then ]
[name:fill_in, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:find, type:Object, file:null, step:Then ]
[name:find_by, type:Signature, file:alphagov_e-petitions/app/models/signature.rb, step:Then ]
[name:find_field, type:Object, file:null, step:Then ]
[name:find_or_create_by, type:Department, file:alphagov_e-petitions/app/models/department.rb, step:Given ]
[name:find_or_create_by, type:Department, file:alphagov_e-petitions/app/models/department.rb, step:And ]
[name:flatten, type:Object, file:null, step:Then ]
[name:from_now, type:Object, file:null, step:Given ]
[name:gsub, type:Object, file:null, step:Then ]
[name:gsub, type:Object, file:null, step:When ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:When ]
[name:have_css, type:Object, file:null, step:Then ]
[name:have_link, type:Object, file:null, step:Then ]
[name:have_no_content, type:Object, file:null, step:Then ]
[name:have_no_title, type:Object, file:null, step:Then ]
[name:have_title, type:Object, file:null, step:Then ]
[name:have_xpath, type:Object, file:null, step:Then ]
[name:have_xpath, type:Object, file:null, step:When ]
[name:include, type:Object, file:null, step:When ]
[name:index, type:Admin/petitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:null]
[name:map, type:Object, file:null, step:Then ]
[name:model!, type:Object, file:null, step:Then ]
[name:name, type:AdminUser, file:alphagov_e-petitions/app/models/admin_user.rb, step:Then ]
[name:name, type:StagedPetitionCreator, file:alphagov_e-petitions/app/models/staged_petition_creator.rb, step:Then ]
[name:name, type:AdminUser, file:alphagov_e-petitions/app/models/admin_user.rb, step:Given ]
[name:name, type:StagedPetitionCreator, file:alphagov_e-petitions/app/models/staged_petition_creator.rb, step:Given ]
[name:new, type:PetitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:When ]
[name:new, type:PetitionsController, file:alphagov_e-petitions/app/controllers/petitions_controller.rb, step:When ]
[name:new, type:Admin/searchesController, file:alphagov_e-petitions/app/controllers/admin/searches_controller.rb, step:null]
[name:new, type:Admin/userSessionsController, file:alphagov_e-petitions/app/controllers/admin/user_sessions_controller.rb, step:null]
[name:new, type:SignaturesController, file:alphagov_e-petitions/app/controllers/signatures_controller.rb, step:null]
[name:nil?, type:Object, file:null, step:And ]
[name:nil?, type:Object, file:null, step:Given ]
[name:not_to, type:Object, file:null, step:Then ]
[name:notify_by_email?, type:Object, file:null, step:Then ]
[name:open_email, type:Object, file:null, step:When ]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:When ]
[name:parse_email_count, type:Object, file:null, step:Then ]
[name:password, type:Object, file:null, step:null]
[name:present?, type:Object, file:null, step:Given ]
[name:raise, type:Object, file:null, step:Then ]
[name:raise, type:Object, file:null, step:When ]
[name:raw, type:Object, file:null, step:Then ]
[name:respond_to?, type:Object, file:null, step:Then ]
[name:search, type:SearchController, file:alphagov_e-petitions/app/controllers/search_controller.rb, step:null]
[name:select, type:Object, file:null, step:When ]
[name:select, type:Object, file:null, step:Then ]
[name:send, type:Object, file:null, step:Then ]
[name:show, type:PetitionsController, file:alphagov_e-petitions/app/controllers/admin/petitions_controller.rb, step:null]
[name:show, type:PetitionsController, file:alphagov_e-petitions/app/controllers/petitions_controller.rb, step:null]
[name:size, type:Object, file:null, step:Then ]
[name:state_label, type:Petition, file:alphagov_e-petitions/app/models/petition.rb, step:null]
[name:state_label, type:Petition, file:alphagov_e-petitions/app/models/staged/base/petition.rb, step:null]
[name:state_label, type:Petition, file:alphagov_e-petitions/app/models/staged/petition.rb, step:null]
[name:strip, type:Object, file:null, step:Then ]
[name:text, type:Object, file:null, step:Then ]
[name:times, type:Object, file:null, step:Given ]
[name:to, type:Object, file:null, step:Then ]
[name:to, type:Object, file:null, step:When ]
[name:to_s, type:Object, file:null, step:Then ]
[name:to_s, type:Object, file:null, step:When ]
[name:unread_emails_for, type:Object, file:null, step:Then ]
[name:with_scope, type:WebSteps, file:alphagov_e-petitions/features/step_definitions/web_steps.rb, step:When ]
[name:with_scope, type:WebSteps, file:alphagov_e-petitions/features/step_definitions/web_steps.rb, step:Then ]
[name:within, type:Object, file:null, step:When ]
[name:within, type:Object, file:null, step:Then ]
[name:within_section, type:Sections, file:alphagov_e-petitions/features/support/sections.rb, step:Then ]
[name:xpath_of_section, type:Sections, file:alphagov_e-petitions/features/support/sections.rb, step:Then ]
[name:xpath_of_section, type:Sections, file:alphagov_e-petitions/features/support/sections.rb, step:When ]

Referenced pages: 21
alphagov_e-petitions/app/views/admin/petitions/_internal_response.html.erb
alphagov_e-petitions/app/views/admin/petitions/_petition_details.html.erb
alphagov_e-petitions/app/views/admin/petitions/_published_petition_details.html.erb
alphagov_e-petitions/app/views/admin/petitions/_reject.html.erb
alphagov_e-petitions/app/views/admin/petitions/edit.html.erb
alphagov_e-petitions/app/views/admin/petitions/edit_internal_response.html.erb
alphagov_e-petitions/app/views/admin/petitions/edit_response.html.erb
alphagov_e-petitions/app/views/admin/petitions/index.html.erb
alphagov_e-petitions/app/views/admin/searches/new.html.erb
alphagov_e-petitions/app/views/admin/user_sessions/new.html.erb
alphagov_e-petitions/app/views/petitions/new.html.erb
alphagov_e-petitions/app/views/petitions/show.html.erb
alphagov_e-petitions/app/views/search/_result_list_footer.html.erb
alphagov_e-petitions/app/views/search/_result_list_header.html.erb
alphagov_e-petitions/app/views/search/_result_tabs.html.erb
alphagov_e-petitions/app/views/search/_results.html.erb
alphagov_e-petitions/app/views/search/_results_table.html.erb
alphagov_e-petitions/app/views/search/search.html.erb
alphagov_e-petitions/app/views/signatures/_form.html.erb
alphagov_e-petitions/app/views/signatures/_terms_and_conditions.html.erb
alphagov_e-petitions/app/views/signatures/new.html.erb

