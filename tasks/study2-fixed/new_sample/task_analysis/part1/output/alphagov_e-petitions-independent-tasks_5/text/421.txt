Feature: As Charlie
In order to have an issue discussed in parliament
I want to be able to create a petition and verify my email address.
Background:
Given a department exists with name: "Cabinet Office", description: "Where cabinets do their paperwork"
And a department exists with name: "Department for International Development", description: "A large portion of the UK population cannot intonate their words properly. This department is responsible for developing this."
Scenario: Charlie creates our petition
Given I am on the new petition page
Then I should see "Create a new e-petition - e-petitions" in the browser page title
And I should be connected to the server via an ssl connection
When I fill in "Title" with "The wombats of wimbledon rock."
And I fill in "Action" with "Give half of Wimbledon rock to wombats!"
And I fill in "Description" with "The racial tensions between the wombles and the wombats are heating up. Racial attacks are a regular occurrence and the death count is already in 5 figures. The only resolution to this crisis is to give half of Wimbledon common to the Wombats and to recognise them as their own independent state."
And I press "Next"
And I fill in my details
And I press "Next"
And I fill in sponsor emails
And I press "Next"
And I press "Next"
Then the markup should be valid
When I press "Submit"
Then a petition should exist with title: "The wombats of wimbledon rock.", state: "pending"
And there should be a "pending" signature with email "womboid@wimbledon.com" and name "Womboid Wibbledon"
And "Womboid Wibbledon" wants to be notified about the petition's progress
And "womboid@wimbledon.com" should receive 1 email
When I confirm my email address
Then a petition should exist with title: "The wombats of wimbledon rock.", state: "validated"
And there should be a "validated" signature with email "womboid@wimbledon.com" and name "Womboid Wibbledon"
@javascript
Scenario: Charlie tries to submit an invalid petition
Given I am on the new petition page
Then I should see a fieldset called "Petition Details"
When I press "Next"
Then I should see "Title must be completed"
And I should see "Action must be completed"
And I should see "Description must be completed"
When I am allowed to make the petition title too long
When I fill in "Title" with "012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789Blah"
And I fill in "Action" with "This text is longer than 200 characters. 012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789"
And I fill in "Description" with "This text is longer than 1000 characters. 012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789012345678911234567892123456789312345678941234567895123456789"
And I press "Next"
Then I should see "Title is too long."
And I should see "Description is too long."
And I should see "Action is too long."
When I fill in "Title" with "The wombats of wimbledon rock."
And I fill in "Action" with "Give half of Wimbledon rock to wombats!"
And I fill in "Description" with "The racial tensions between the wombles and the wombats are heating up.  Racial attacks are a regular occurrence and the death count is already in 5 figures.  The only resolution to this crisis is to give half of Wimbledon common to the Wombats and to recognise them as their own independent state."
And I press "Next"
Then I should see a fieldset called "Your Details"
When I press "Next"
Then I should see "Name must be completed"
And I should see "Email must be completed"
And I should see "You must be a British citizen"
And I should see "Postcode must be completed"
When I fill in my details
And I press "Next"
Then I should see a fieldset called "Sponsor email addresses"
And I fill in sponsor emails
And I press "Next"
Then I should see a fieldset called "Review Petition"
And I should see "The wombats of wimbledon rock."
And I should see "The racial tensions between the wombles and the wombats are heating up.  Racial attacks are a regular occurrence and the death count is already in 5 figures.  The only resolution to this crisis is to give half of Wimbledon common to the Wombats and to recognise them as their own independent state."
And I press "Back"
And I press "Back"
And I fill in "Name" with "Mr. Wibbledon"
And I press "Next"
And I press "Next"
And I press "Next"
Then I should see a fieldset called "Make sure this is right"
When I fill in "Email" with ""
And I press "Submit"
Then I should see "Email must be completed"
When I fill in "Email" with "womboid@wimbledon.com"
And I press "Submit"
Then a petition should exist with title: "The wombats of wimbledon rock.", state: "pending"
Then there should be a "pending" signature with email "womboid@wimbledon.com" and name "Mr. Wibbledon"
