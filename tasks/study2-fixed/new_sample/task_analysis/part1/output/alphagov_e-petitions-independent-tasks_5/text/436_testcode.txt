When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    expect(page).to have_content(text)
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)
    case page_name

    when /^the home\s?page$/
      '/'

    when /^the accessibility page$/
      accessibility_path

    when /^the how e-Petitions works page$/
      how_it_works_path

    when /^the terms and conditions page$/
      terms_and_conditions_path

    when /^the privacy policy page$/
      privacy_policy_path

    when /^the crown copyright page$/
      crown_copyright_path

    when /^the FAQ page$/
      faq_path

    when /^the feedback page$/
      feedback_path

    when /^the new petition page$/
      new_petition_path

    when /^the petition page for "([^\"]*)"$/
      petition_path(Petition.find_by(title: $1))

    when /^the new signature page for "([^\"]*)"$/
      new_petition_signature_path(Petition.find_by(title: $1))

    when /^the search results page$/
      search_path

    when /^the departments page$/
      departments_path
    when /^the department info(rmation)? page$/
      info_departments_path

    when /^the Admin (.*)$/i
      admin_path($1)

    else
      begin
        page_name =~ /^the (.*) page$/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue NoMethodError, ArgumentError
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Then /^I should see an? "([^\"]*)" (\S+) field$/ do |name, type|
  field = find_field(name)
end
Given /^I am logged in as a threshold user$/ do
  @user = FactoryGirl.create(:threshold_user)
  step "the admin user is logged in"
end
Given /^the(?: date is the| time is) "([^"]*)"$/ do |description|
  time = Chronic.parse(description, :now => Time.now)
  raise "Chronic could not parse #{description}" unless time
  Timecop.travel time.utc
end
Given(/^#{capture_model} exists?(?: with #{capture_fields})?$/) do |name, fields|
  create_model(name, fields)
end
Given /^the petition "([^"]*)" has (\d+) validated signatures$/ do |title, no_validated|
  petition = Petition.find_by(title: title)
  (no_validated - 1).times { petition.signatures << FactoryGirl.create(:validated_signature) }
end
And /^all petitions have had their signatures counted$/ do
  Petition.update_all_signature_counts
end
Given /^the admin user is logged in$/ do
  visit admin_login_path
  fill_in("Email", :with => @user.email)
  fill_in("Password", :with => "Letmein1!")
  click_button("Log in")
end
