Feature: Viewing organisations
Scenario: Organisation page should show any chief professional officers
Given the "Department of Health" organisation is associated with chief professional officers
When I visit the "Department of Health" organisation
Then I should be able to view all chief professional officers for the "Department of Health" organisation
