Feature: Administering Organisations
Scenario: Managing social media links
Given I am an admin called "Jane"
And the organisation "Ministry of Pop" exists
And a social media service "Twooter"
And a social media service "Facebark"
When I add a "Twooter" social media link "http://twooter.com/beards-in-france" to the organisation
And I add a "Facebark" social media link "http://facebark.com/beards-in-france" with the title "Beards on Facebark!" to the organisation
Then the "Twooter" social link should be shown on the public website for the organisation
And the "Facebark" social link called "Beards on Facebark!" should be shown on the public website for the organisation
Feature: Administering worldwide organisation
As a citizen interested in UK gov activity around the world, I want there to be profiles of the world organisation (eg embassies, DFID offices, UKTI branches) in each worldwide location, so I can see which organisation are active in each location and read more about them.
Acceptance criteria:
* Each world organisation has:
* a unique name e.g. "British Embassy in Madrid" and a URL "/world/offices/british-embassy-in-madrid" which is generated from the name
* a text short summary and markdown long description.
* multiple social media links (like orgs)
* multiple sets of office information (like orgs)
* with the addition of a list of services (chosen from a set) that the office provides
* a logo formatted name (always using the standard HMG crest for now)
* Each world organisation can be associated with 1+ world locations, and shows on the world locations page to which they are associated (see mock up on the [ticket](https://www.pivotaltracker.com/story/show/41026113))
* Each can have corporate information pages (like orgs)
Background:
Given I am a GDS editor
Scenario: Managing social media links
Given a worldwide organisation "Department of Beards in France"
Given a social media service "Twooter"
When I add a "Twooter" social media link "http://twooter.com/beards-in-france" to the worldwide organisation
Then the "Twooter" social link should be shown on the public website for the worldwide organisation
