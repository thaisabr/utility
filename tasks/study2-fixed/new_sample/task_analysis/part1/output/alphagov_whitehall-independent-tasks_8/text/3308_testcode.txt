Given /^the organisation "([^"]*)" exists$/ do |name|
  create(:ministerial_department, name: name)
end
When /^I feature the news article "([^"]*)" for "([^"]*)" with image "([^"]*)"$/ do |news_article_title, organisation_name, image_filename|
  organisation = Organisation.find_by_name!(organisation_name)
  visit admin_organisation_path(organisation)
  click_link "Featured documents"
  locale = Locale.find_by_language_name("English")
  news_article = LocalisedModel.new(NewsArticle, locale.code).find_by_title(news_article_title)
  fill_in 'title', with: news_article_title.split.first
  click_link 'Everyone'
  within record_css_selector(news_article) do
    click_link "Feature"
  end
  attach_file "Select an image to be shown when featuring", Rails.root.join("test/fixtures/#{image_filename}")
  fill_in :alt_text, with: "An accessible description of the image"
  click_button "Save"
end
When /^I stop featuring the news article "([^"]*)" for "([^"]*)"$/ do |news_article_title, organisation_name|
  organisation = Organisation.find_by_name!(organisation_name)
  visit features_admin_organisation_path(organisation)
  locale = Locale.find_by_language_name("English")
  news_article = LocalisedModel.new(NewsArticle, locale.code).find_by_title(news_article_title)
  within record_css_selector(news_article) do
    click_on "Unfeature"
  end
end
When /^I order the featured items in the "([^"]*)" organisation as:$/ do |name, table|
  organisation = Organisation.find_by_name!(name)
  visit features_admin_organisation_path(organisation)
  order_features_from(table)
end
Then /^there should be nothing featured on the home page of "([^"]*)"$/ do |name|
  visit_organisation name
  find(featured_documents_selector).all('article').should be_empty
end
  def order_features_from(table)
    table.raw.each_with_index do |(title), index|
      fill_in title, with: index
    end
    click_button "Update feature order"
  end
  def visit_organisation(name)
    organisation = Organisation.find_by_name!(name)
    visit organisation_path(organisation)
  end
Given /^a published (publication|policy|news article|consultation) "([^"]*)" was produced by the "([^"]*)" organisation$/ do |document_type, title, organisation_name|
  organisation = Organisation.find_by_name!(organisation_name)
  create("published_#{document_class(document_type).name.underscore}".to_sym, title: title, organisations: [organisation])
end
Then /^I should see (#{THE_DOCUMENT})$/ do |edition|
  assert has_css?(record_css_selector(edition))
end
  def document_class(type)
    type = 'edition' if type == 'document'
    type.gsub(" ", "_").classify.constantize
  end
When /^I order the featured items of the (?:world location|international delegation) "([^"]*)" to:$/ do |name, table|
  world_location = WorldLocation.find_by_name!(name)
  visit features_admin_world_location_path(world_location)
  order_features_from(table)
end
