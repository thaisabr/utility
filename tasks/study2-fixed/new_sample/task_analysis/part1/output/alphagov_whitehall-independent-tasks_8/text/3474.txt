Feature: Administering world location information
Background:
Given I am an admin
Scenario: Featuring shows the correct translation of the article on world location page
Given a world location "Jamestopia" exists in both english and french
And there is a news article "Beards" in english ("Barbes" in french) related to the world location
When I feature "Barbes" on the french "Jamestopia" page
Then I should see "Barbes" as the title of the feature on the french "Jamestopia" page
And I should see "Barbes" as the title of the featured item on the french "Jamestopia" admin page
