Classes: 11
[name:Edition, file:alphagov_whitehall/app/models/edition.rb, step:Given ]
[name:GDS, file:null, step:Given ]
[name:Locale, file:alphagov_whitehall/lib/locale.rb, step:When ]
[name:LocalisedModel, file:alphagov_whitehall/lib/localised_model.rb, step:When ]
[name:NewsArticle, file:alphagov_whitehall/app/models/news_article.rb, step:When ]
[name:Rails, file:null, step:When ]
[name:THE_DOCUMENT, file:null, step:Then ]
[name:User, file:alphagov_whitehall/app/models/user.rb, step:Given ]
[name:WorldLocation, file:alphagov_whitehall/app/models/world_location.rb, step:Given ]
[name:WorldLocation, file:alphagov_whitehall/app/models/world_location.rb, step:Then ]
[name:WorldLocation, file:alphagov_whitehall/app/models/world_location.rb, step:When ]

Methods: 32
[name:add_translation_to_world_location, type:Object, file:null, step:Given ]
[name:announcements_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:When ]
[name:attach_file, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:Then ]
[name:click_link, type:Object, file:null, step:When ]
[name:create, type:Object, file:null, step:Given ]
[name:feature_news_article_in_world_location, type:WorldLocationSteps, file:alphagov_whitehall/features/step_definitions/world_location_steps.rb, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find_by_language_name, type:Locale, file:alphagov_whitehall/lib/locale.rb, step:When ]
[name:find_by_name!, type:WorldLocation, file:alphagov_whitehall/app/models/world_location.rb, step:Then ]
[name:find_by_name!, type:WorldLocation, file:alphagov_whitehall/app/models/world_location.rb, step:When ]
[name:find_by_title, type:CorporateInformationPageType, file:alphagov_whitehall/app/models/corporate_information_page_type.rb, step:When ]
[name:has_css?, type:Object, file:null, step:Then ]
[name:join, type:Object, file:null, step:When ]
[name:last, type:WorldLocation, file:alphagov_whitehall/app/models/world_location.rb, step:Given ]
[name:login_as, type:GdsSsoHelper, file:alphagov_whitehall/features/support/gds_sso_helper.rb, step:Given ]
[name:new, type:LocalisedModel, file:alphagov_whitehall/lib/localised_model.rb, step:When ]
[name:policies_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:When ]
[name:public_document_path, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:When ]
[name:public_document_url, type:PublicDocumentRoutesHelper, file:alphagov_whitehall/app/helpers/public_document_routes_helper.rb, step:When ]
[name:publications_filter_path, type:FilterRoutesHelper, file:alphagov_whitehall/app/helpers/filter_routes_helper.rb, step:When ]
[name:record_css_selector, type:Object, file:null, step:When ]
[name:record_css_selector, type:Object, file:null, step:Then ]
[name:show, type:Admin/worldLocationsController, file:alphagov_whitehall/app/controllers/admin/world_locations_controller.rb, step:When ]
[name:show, type:WorldLocationsController, file:alphagov_whitehall/app/controllers/admin/world_locations_controller.rb, step:When ]
[name:show, type:WorldLocationsController, file:alphagov_whitehall/app/controllers/api/world_locations_controller.rb, step:When ]
[name:show, type:WorldLocationsController, file:alphagov_whitehall/app/controllers/world_locations_controller.rb, step:When ]
[name:title, type:WorldwidePriority, file:alphagov_whitehall/app/models/worldwide_priority.rb, step:When ]
[name:url, type:WorldwidePriority, file:alphagov_whitehall/app/models/worldwide_priority.rb, step:When ]
[name:within, type:Object, file:null, step:When ]
[name:worldwide_organisation, type:WorldwideOfficesController, file:alphagov_whitehall/app/controllers/admin/worldwide_offices_controller.rb, step:When ]

Referenced pages: 12
alphagov_whitehall/app/views/admin/world_locations/show.html.erb
alphagov_whitehall/app/views/announcements/_list_description.html.erb
alphagov_whitehall/app/views/contacts/_contact.html.erb
alphagov_whitehall/app/views/policies/_list_description.html.erb
alphagov_whitehall/app/views/publications/_list_description.html.erb
alphagov_whitehall/app/views/shared/_available_languages.html.erb
alphagov_whitehall/app/views/shared/_featured_news.html.erb
alphagov_whitehall/app/views/shared/_feeds.html.erb
alphagov_whitehall/app/views/shared/_recently_updated.html.erb
alphagov_whitehall/app/views/shared/_recently_updated_documents.html.erb
alphagov_whitehall/app/views/world_locations/show.html.erb
alphagov_whitehall/app/views/worldwide_organisations/_worldwide_organisation.html.erb

