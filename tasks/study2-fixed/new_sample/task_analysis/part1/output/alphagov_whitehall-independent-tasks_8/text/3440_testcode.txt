Given /^I am (?:a|an) (writer|editor|admin|GDS editor|importer)(?: called "([^"]*)")?$/ do |role, name|
  @user = case role
  when "writer"
    create(:policy_writer, name: (name || "Wally Writer"))
  when "editor"
    create(:departmental_editor, name: (name || "Eddie Depteditor"))
  when "admin"
    create(:user)
  when "GDS editor"
    create(:gds_editor)
  when 'importer'
    create(:importer)
  end
  login_as @user
end
  def login_as(user)
    GDS::SSO.test_user = user
    Edition::AuditTrail.whodunnit = user
    super(user) # warden
  end
Given /^two cabinet ministers "([^"]*)" and "([^"]*)"$/ do |person1, person2|
  create(:role_appointment, person: create(:person, forename: person1), role: create(:ministerial_role, cabinet_member: true))
  create(:role_appointment, person: create(:person, forename: person2), role: create(:ministerial_role, cabinet_member: true))
end
When /^I order the cabinet ministers "([^"]*)", "([^"]*)"$/ do |role1, role2|
  visit admin_cabinet_ministers_path
  [role1, role2].each_with_index do |role, index|
    fill_in(role, with: index)
  end
  click_button "Save"
end
Then /^I should see "([^"]*)", "([^"]*)" in that order on the ministers page$/ do |person1, person2|
  visit ministers_page
  actual = all(".person .current-appointee").map {|elem| elem.text}
  assert_equal [person1, person2], actual
end
  def ministers_page
    ministerial_roles_path
  end
Then /^I should see (#{THE_DOCUMENT})$/ do |edition|
  assert has_css?(record_css_selector(edition))
end
