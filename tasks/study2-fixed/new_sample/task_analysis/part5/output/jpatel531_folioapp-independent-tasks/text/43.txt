Feature: submitting to opportunities
In order to get my work out there
As a creative
I want to submit to organisations' opportunities
And send them a piece of my work
Background:
Given I am a user
Given I have works
And I am on the profile of an organisation who have published an opportunity
And I click the 'Submit To Opportunity' link
@javascript
Scenario: filling in opportunity details
When I select which work and add a message
And I click the submit button
And I go on my homepage
Then I should see a thumbnail of my submission along with an acceptance status
Feature: creating opportunities
In order to attract talented creatives
As an organisation
I want to create a submissions opportunity
And specify a title, image, description and deadline
Background:
Given I am a user
And I am a member of an organisation
And I am on my organisation profile
And I click the 'Opportunities' link of my profile
And I specify that I wish to create an opportunity
@javascript
Scenario: filling in opportunity details
When I give my opportunity a title, image, description and deadline, and requirements
And I click submit
Then I should see my opportunity on my organisation page
Feature: creating an organisation
In order to create opportunities and receive submissions
As a user
I want to create an organisation
Background:
Given I am a user
And on my homepage
And I click 'Create Organisation'
And I specify its name, profile-image, network and organisation type
@javascript
Scenario: Having entered these details
Then I should see these details on my newly created profile
And on my own profile I should be able to see my profile role
