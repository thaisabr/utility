Given /^I have an? "(.*)" file with content:$/ do |file, text|
  File.open(file, 'w') do |f|
    f.write(text)
  end
end
Given /^I have a configuration file with "(.*)" set to "(.*)"$/ do |key, value|
  File.open('_config.yml', 'w') do |f|
    f.write("#{key}: #{value}\n")
  end
end
When /^I run jekyll$/ do
  run_jekyll_build
end
Then /^the (.*) directory should +exist$/ do |dir|
  assert File.directory?(dir), "The directory \"#{dir}\" does not exist"
end
Then /^I should see "(.*)" in "(.*)"$/ do |text, file|
  assert_match Regexp.new(text), file_contents(file)
end
def run_jekyll_build(build_args = "")
  if !run_jekyll("build #{build_args}", jekyll_output_file) || build_args.eql?("--verbose")
    puts jekyll_run_output
  end
end
def file_contents(path)
  File.open(path) do |file|
    file.readlines.join # avoid differences with \n and \r\n line endings
  end
end
def jekyll_output_file
  JEKYLL_COMMAND_OUTPUT_FILE
end
def jekyll_run_output
  File.read(jekyll_output_file)
end
def run_jekyll(args, output_file)
  command = "#{JEKYLL_PATH} #{args} > #{jekyll_output_file} 2>&1"
  system command
end
