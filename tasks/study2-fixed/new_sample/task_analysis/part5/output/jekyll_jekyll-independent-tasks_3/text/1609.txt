Feature: Draft Posts
As a hacker who likes to blog
I want to be able to preview drafts locally
In order to see if they look alright before publishing
Scenario: Use page.path variable
Given I have a configuration file with "permalink" set to "none"
And I have a _drafts directory
And I have the following draft:
| title  | date      | layout  | content        |
| Recipe | 2009-03-27 | simple | Post path: {{ page.path }} |
When I run jekyll with drafts
Then the _site directory should exist
And I should see "Post path: _drafts/recipe.textile" in "_site/recipe.html"
