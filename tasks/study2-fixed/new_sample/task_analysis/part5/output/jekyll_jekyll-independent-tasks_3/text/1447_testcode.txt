Given /^I have an? "(.*)" page(?: with (.*) "(.*)")? that contains "(.*)"$/ do |file, key, value, text|
  File.open(file, 'w') do |f|
    f.write <<EOF
---
#{key || 'layout'}: #{value || 'nil'}
---
#{text}
EOF
  end
end
Given /^I have an? "(.*)" file with content:$/ do |file, text|
  File.open(file, 'w') do |f|
    f.write(text)
  end
end
Given /^I have fixture collections$/ do
  FileUtils.cp_r File.join(JEKYLL_SOURCE_DIR, "test", "source", "_methods"), source_dir
end
When /^I run jekyll(.*)$/ do |args|
  status = run_jekyll(args)
  if !status || args.include?("--verbose")
    puts jekyll_run_output
  end
end
Then /^the (.*) directory should +exist$/ do |dir|
  assert File.directory?(dir), "The directory \"#{dir}\" does not exist"
end
Then /^I should see "(.*)" in "(.*)"$/ do |text, file|
  assert_match Regexp.new(text, Regexp::MULTILINE), file_contents(file)
end
def source_dir(*files)
  File.join(TEST_DIR, *files)
end
def jekyll_run_output
  File.read(jekyll_output_file)
end
def run_jekyll(args)
  system "#{JEKYLL_PATH} #{args} --trace > #{jekyll_output_file} 2>&1"
end
def file_contents(path)
  File.open(path) do |file|
    file.readlines.join # avoid differences with \n and \r\n line endings
  end
end
def jekyll_output_file
  JEKYLL_COMMAND_OUTPUT_FILE
end
