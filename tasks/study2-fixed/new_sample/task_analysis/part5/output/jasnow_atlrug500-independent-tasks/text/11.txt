Feature: Visit Web Site
As a visitor to the website
I want to see something on the homepage
so I can know that the site is working
Scenario: Check stuff on home page
When I go to the home page
Then I should see "Go to Meetings"
Then I should see "Find Presentations"
Then I should see "Learn Ruby"
Then I should see the image "Atlanta Ruby Logo"
Then I should see "Find a Job"
Then I should see "Hire Someone"
Then I should see "Meetup"
Then I should see "@atlrug"
Then I should see "Wiki"
Then I should see "Linkedin"
Then I should see "Slack/Tech404/Rails"
Then I should see "Platinum Sponsors"
Then I should see "Pardot"
Then I should see "Mandrill"
