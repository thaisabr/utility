Given /^I have an? "(.*)" page(?: with (.*) "(.*)")? that contains "(.*)"$/ do |file, key, value, text|
  File.open(file, 'w') do |f|
    f.write <<EOF
---
#{key || 'layout'}: #{value || 'nil'}
---
#{text}
EOF
  end
end
Given /^I have an? (.*) (layout|theme) that contains "(.*)"$/ do |name, type, text|
  folder = if type == 'layout'
    '_layouts'
  else
    '_theme'
  end
  File.open(File.join(folder, name + '.html'), 'w') do |f|
    f.write(text)
  end
end
Given /^I have an? (.*) directory$/ do |dir|
  FileUtils.mkdir_p(dir)
end
Given /^I have the following (draft|post)s?(?: (in|under) "([^"]+)")?:$/ do |status, direction, folder, table|
  table.hashes.each do |post|
    title = slug(post['title'])
    ext = post['type'] || 'textile'
    before, after = location(folder, direction)

    if "draft" == status
      folder_post = '_drafts'
      filename = "#{title}.#{ext}"
    elsif "post" == status
      parsed_date = Time.xmlschema(post['date']) rescue Time.parse(post['date'])
      folder_post = '_posts'
      filename = "#{parsed_date.strftime('%Y-%m-%d')}-#{title}.#{ext}"
    end

    path = File.join(before, folder_post, after, filename)

    matter_hash = {}
    %w(title layout tag tags category categories published author path date permalink).each do |key|
      matter_hash[key] = post[key] if post[key]
    end
    matter = matter_hash.map { |k, v| "#{k}: #{v}\n" }.join.chomp

    content = if post['input'] && post['filter']
      "{{ #{post['input']} | #{post['filter']} }}"
    else
      post['content']
    end

    File.open(path, 'w') do |f|
      f.write <<EOF
---
#{matter}
---
#{content}
EOF
    end
  end
end
Given /^I have a configuration file with "(.*)" set to "(.*)"$/ do |key, value|
  File.open('_config.yml', 'w') do |f|
    f.write("#{key}: #{value}\n")
  end
end
When /^I run jekyll$/ do
  run_jekyll
end
Then /^the (.*) directory should exist$/ do |dir|
  assert File.directory?(dir), "The directory \"#{dir}\" does not exist"
end
Then /^I should see "(.*)" in "(.*)"$/ do |text, file|
  assert Regexp.new(text).match(File.open(file).readlines.join)
end
Then /^the "(.*)" file should not exist$/ do |file|
  assert !File.exists?(file)
end
def run_jekyll(opts = {})
  command = JEKYLL_PATH.clone
  command << " build"
  command << " --drafts" if opts[:drafts]
  command << " >> /dev/null 2>&1" if opts[:debug].nil?
  system command
end
def slug(title)
  title.downcase.gsub(/[^\w]/, " ").strip.gsub(/\s+/, '-')
end
def location(folder, direction)
  if folder
    before = folder if direction == "in"
    after = folder if direction == "under"
  end
  [before || '.', after || '.']
end
