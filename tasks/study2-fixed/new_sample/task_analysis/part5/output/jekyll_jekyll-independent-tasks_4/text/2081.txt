Feature: Post data
As a hacker who likes to blog
I want to be able to embed data into my posts
In order to make the posts slightly dynamic
Scenario Outline: Use post.path variable
Given I have a <dir>/_posts directory
And I have the following post in "<dir>":
| title | type | date | content |
| my-post | html | 4/12/2013 | Source path: {{ page.path }} |
When I run jekyll
Then the _site directory should exist
And I should see "Source path: <path_prefix>_posts/2013-04-12-my-post.html" in "_site/<dir>/2013/04/12/my-post.html"
Examples:
| dir | path_prefix |
| .   |             |
| dir | dir/        |
| dir/nested | dir/nested/ |
Feature: Site data
As a hacker who likes to blog
I want to be able to embed data into my site
In order to make the site slightly dynamic
Scenario Outline: Use page.path variable in a page
Given I have a <dir> directory
And I have a "<path>" page that contains "Source path: {{ page.path }}"
When I run jekyll
Then the _site directory should exist
And I should see "Source path: <path>" in "_site/<path>"
Examples:
| dir | path |
| .   | index.html |
| dir | dir/about.html |
| dir/nested | dir/nested/page.html |
