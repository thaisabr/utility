Given /^I have a blank site in "(.*)"$/ do |path|
  FileUtils.mkdir_p(path)
end
Given /^I have an? "(.*)" page(?: with (.*) "(.*)")? that contains "(.*)"$/ do |file, key, value, text|
  File.open(file, 'w') do |f|
    f.write <<EOF
---
#{key || 'layout'}: #{value || 'nil'}
---
#{text}
EOF
  end
end
Given /^I have a configuration file with:$/ do |table|
  File.open('_config.yml', 'w') do |f|
    table.hashes.each do |row|
      f.write("#{row["key"]}: #{row["value"]}\n")
    end
  end
end
When /^I run jekyll$/ do
  run_jekyll
end
Then /^the (.*) directory should +exist$/ do |dir|
  assert File.directory?(dir), "The directory \"#{dir}\" does not exist"
end
Then /^the (.*) directory should not exist$/ do |dir|
  assert !File.directory?(dir), "The directory \"#{dir}\" exists"
end
Then /^I should see "(.*)" in "(.*)"$/ do |text, file|
  assert Regexp.new(text).match(File.open(file).readlines.join)
end
Then /^the "(.*)" file should +exist$/ do |file|
  assert File.file?(file), "The file \"#{file}\" does not exist"
end
Then /^the "(.*)" file should not exist$/ do |file|
  assert !File.exists?(file), "The file \"#{file}\" exists"
end
def run_jekyll(opts = {})
  command = JEKYLL_PATH.clone
  command << " build"
  command << " --drafts" if opts[:drafts]
  command << " >> /dev/null 2>&1" if opts[:debug].nil?
  system command
end
