Feature:  Download (for backup) and upload (for restore) all connections in N3
In order to safeguard the content of a wontology,
as an administrator, I want
to be able to download and upload the system's connections.
@not_for_selenium
Scenario: Download connections.n3
Given there are 4 existing individuals like "indiv"
And there is an existing connection "indiv1" "child_of" "indiv0"
And there is an existing connection "indiv2" "child_of" "indiv1"
And there is an existing connection "indiv3" "child_of" "indiv2"
When I go to the path "/w/connections.n3"
Then the response should contain 6 matches of "indiv"
Feature:  Download (for backup) and upload (for restore) all items in YAML
In order to safeguard the content of a wontology,
as an administrator, I want
to be able to download and upload the system's items.
@not_for_selenium
Scenario: Download items.yml
Given there are 3 existing individuals like "anItem"
When I go to the path "/w/items.yaml"
Then the response should contain 9 matches of "anItem"
Feature:  Edit individual items through non-Ajax pages
In order to create a wontology,
as a contributor, I want
to be able to change the information for existing items.
Scenario: When I try to delete an item, it deletes
Given there is 1 existing property like "propFamily"
And I am on the show items page for "propFamily0"
And I follow "Delete this item", accepting confirmation
When I try to go to the show items page for "propFamily0"
Then I should see "doesn't exist"
Scenario: Delete links for user items, not for built-in items
Given there is 1 existing individual like "anItem"
When I go to the index items page
Then there should be an item container for "anItem0" including the tag "a[href="/w/items/?anItem0?"][onclick*="delete"]"
And there should not be an item container for "sub_property_of" including the tag "a[href="/w/items/?sub_property_of?"][onclick*="delete"]"
Scenario: Edit links for user items, not for built-in items
Given there is 1 existing category like "aCategory"
When I go to the index items page
Then there should be an item container for "aCategory0" including the tag "a[href="/w/items/?aCategory0?/edit"]"
And there should not be an item container for "parent_of" including the tag "a[href="/w/items/?sub_property_of?/edit"]"
