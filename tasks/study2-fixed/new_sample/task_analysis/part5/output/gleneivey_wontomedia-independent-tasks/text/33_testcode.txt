When /^I (am on|go to|try to go to) the (\S+) (\S+) page$/ do |fu, action,
                                                                   controller|
  if action == "index"
    visit "/w/#{controller}/"
  else
    visit "/w/#{controller}/#{action}"
  end
  if !@browser.nil?
    selenium.get_eval( "window.focus();" )
    Kernel.sleep(1.5)
  end
end
When /^(?:|I )fill in "([^\"]*)" with "(.*)"$/ do |field, value|
  while value =~ /\\([0-7]{3})/ do
    octal = Regexp.last_match[1]
    str = " "
    str[0]= octal.oct                 # Ruby 1.8
#    str.setbyte(0, octal.oct)        # Ruby 1.9
    value.sub!(/\\#{octal}/, str)
  end
  fill_in(field, :with => value)
end
When /^(?:|I )select "([^\"]*)" from "([^\"]*)"$/ do |value, field|
  select(value, :from => field)
end
Then /^(?:|I )should see "([^\"]*)"$/ do |text|
  if defined?(Spec::Rails::Matchers)
    response.should contain(text)
  else
    assert_contain text
  end
end
When /^I pause$/ do
  if !@browser.nil?
    Kernel.sleep(1.0)
  end
end
When /^(?:|I )press "([^\"]*)"$/ do |button|
  click_button(button)
  if !@browser.nil?
    Kernel.sleep(1.5)
  end
end
When /^I type the "(.+)" special key$/ do |key_string|
  key = case key_string
        when /^Back$/     then   8
        when /^Tab$/      then   9
        when /^Enter$/    then  10    # Note: Firefox ignores, grrr...
        when /^Escape$/   then  27
        when /^Delete$/   then 127
        when /^Up$/       then 224
        when /^Down$/     then 225
        when /^Left$/     then 226
        when /^Right$/    then 227
        else                   151    # asterisk, as good a default as any...
        end
  selenium.key_press_native(key)
end
When /^I put the focus on the "([^\"]+)" element$/ do |element_name|
  selenium.focus(element_name)
end
Then /^the focus is on (the|a) "([^\"]+)" element$/ do |
    article, element|

  if article == "the"
    assert selenium.is_element_present(element),
      "No such element as '#{element}'."
  end

  Kernel.sleep(0.1)   # ensure browser finishes procesing JS from last event
  result = selenium.get_eval "window.document.activeElement.id;"

  if article == "a"
    assert result =~ Regexp.new(element),
      "Element '#{result}' currently has focus, doesn't match '#{element}'."
  else
    assert result == element,
      "Element '#{result}' currently has focus instead of '#{element}'."
  end
end
