Given /^I have an? "(.*)" page(?: with (.*) "(.*)")? that contains "(.*)"$/ do |file, key, value, text|
  File.open(file, 'w') do |f|
    f.write <<EOF
---
#{key || 'layout'}: #{value || 'nil'}
---
#{text}
EOF
    f.close
  end
end
Given /^I have an? "(.*)" file that contains "(.*)"$/ do |file, text|
  File.open(file, 'w') do |f|
    f.write(text)
    f.close
  end
end
Given /^I have a (.*) layout that contains "(.*)"$/ do |layout, text|
  File.open(File.join('_layouts', layout + '.html'), 'w') do |f|
    f.write(text)
    f.close
  end
end
Given /^I have a (.*) directory$/ do |dir|
  FileUtils.mkdir_p(dir)
end
Given /^I have the following posts?(?: (.*) "(.*)")?:$/ do |direction, folder, table|
  table.hashes.each do |post|
    date = Date.parse(post['date']).strftime('%Y-%m-%d')
    title = post['title'].downcase.gsub(/[^\w]/, " ").strip.gsub(/\s+/, '-')

    if direction && direction == "in"
      before = folder || '.'
    elsif direction && direction == "under"
      after = folder || '.'
    end

    path = File.join(before || '.', '_posts', after || '.', "#{date}-#{title}.#{post['type'] || 'textile'}")

    matter_hash = {}
    %w(title layout tag tags category categories published author).each do |key|
      matter_hash[key] = post[key] if post[key]
    end
    matter = matter_hash.map { |k, v| "#{k}: #{v}\n" }.join.chomp

    content = post['content']
    if post['input'] && post['filter']
      content = "{{ #{post['input']} | #{post['filter']} }}"
    end

    File.open(path, 'w') do |f|
      f.write <<EOF
---
#{matter}
---
#{content}
EOF
      f.close
    end
  end
end
Given /^I have a configuration file with "(.*)" set to "(.*)"$/ do |key, value|
  File.open('_config.yml', 'w') do |f|
    f.write("#{key}: #{value}")
    f.close
  end
end
When /^I run jekyll$/ do
  run_jekyll
end
Then /^the (.*) directory should exist$/ do |dir|
  assert File.directory?(dir)
end
Then /^I should see "(.*)" in "(.*)"$/ do |text, file|
  assert_match Regexp.new(text), File.open(file).readlines.join
end
Then /^the "(.*)" file should exist$/ do |file|
  assert File.file?(file)
end
Then /^the "(.*)" file should not exist$/ do |file|
  assert !File.exists?(file)
end
def run_jekyll(opts = {})
  command = JEKYLL_PATH
  command << " >> /dev/null 2>&1" if opts[:debug].nil?
  system command
end
