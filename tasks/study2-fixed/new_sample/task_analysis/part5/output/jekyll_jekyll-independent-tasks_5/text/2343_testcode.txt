Given /^I have an "(.*)" page(?: with (.*) "(.*)")? that contains "(.*)"$/ do |file, key, value, text|
  File.open(file, 'w') do |f|
    f.write <<EOF
---
#{key || 'layout'}: #{value || 'nil'}
---
#{text}
EOF
    f.close
  end
end
Given /^I have a configuration file with "(.*)" set to "(.*)"$/ do |key, value|
  File.open('_config.yml', 'w') do |f|
    f.write("#{key}: #{value}")
    f.close
  end
end
When /^I run jekyll$/ do
  run_jekyll
end
Then /^the (.*) directory should exist$/ do |dir|
  assert File.directory?(dir)
end
Then /^I should see "(.*)" in "(.*)"$/ do |text, file|
  assert_match Regexp.new(text), File.open(file).readlines.join
end
def run_jekyll(opts = {})
  command = JEKYLL_PATH
  command << " >> /dev/null 2>&1" if opts[:debug].nil?
  system command
end
