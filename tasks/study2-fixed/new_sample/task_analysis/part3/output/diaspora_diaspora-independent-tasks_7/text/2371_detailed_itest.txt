Classes: 14
[name:Factory, file:null, step:Given ]
[name:Invitation, file:diaspora_diaspora/app/models/invitation.rb, step:Given ]
[name:Request, file:diaspora_diaspora/app/models/request.rb, step:Then ]
[name:URI, file:null, step:Then ]
[name:User, file:diaspora_diaspora/app/models/api/v0/serializers/user.rb, step:Given ]
[name:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:User, file:diaspora_diaspora/lib/diaspora/user.rb, step:Given ]
[name:User, file:diaspora_diaspora/app/models/api/v0/serializers/user.rb, step:Then ]
[name:User, file:diaspora_diaspora/app/models/user.rb, step:Then ]
[name:User, file:diaspora_diaspora/lib/diaspora/user.rb, step:Then ]
[name:User, file:diaspora_diaspora/app/models/api/v0/serializers/user.rb, step:When ]
[name:User, file:diaspora_diaspora/app/models/user.rb, step:When ]
[name:User, file:diaspora_diaspora/lib/diaspora/user.rb, step:When ]
[name:aspects_controller, file:diaspora_diaspora/app/controllers/aspects_controller.rb, step:Then ]

Methods: 110
[name:Factory, type:Object, file:null, step:Given ]
[name:are_you_sure, type:Object, file:null, step:Then ]
[name:aspects, type:Aspect, file:diaspora_diaspora/lib/stream/aspect.rb, step:Given ]
[name:aspects, type:Base, file:diaspora_diaspora/lib/stream/base.rb, step:Given ]
[name:aspects, type:Object, file:null, step:Given ]
[name:assert_equal, type:Object, file:null, step:Then ]
[name:attach_recipient!, type:Invitation, file:diaspora_diaspora/app/models/invitation.rb, step:Given ]
[name:be_nil, type:Object, file:null, step:Then ]
[name:button, type:Object, file:null, step:Then ]
[name:button creation, type:Object, file:null, step:Then ]
[name:button hidden, type:Object, file:null, step:Then ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:Then ]
[name:click_link, type:Object, file:null, step:When ]
[name:connect_users, type:Object, file:null, step:Given ]
[name:create, type:Object, file:null, step:Given ]
[name:create!, type:Object, file:null, step:Given ]
[name:create!, type:Invitation, file:diaspora_diaspora/app/models/invitation.rb, step:Given ]
[name:current_url, type:Object, file:null, step:Then ]
[name:each, type:AdminRack, file:diaspora_diaspora/lib/admin_rack.rb, step:Given ]
[name:edit, type:ProfilesController, file:diaspora_diaspora/app/controllers/profiles_controller.rb, step:Then ]
[name:edit, type:UsersController, file:diaspora_diaspora/app/controllers/api/v0/users_controller.rb, step:Then ]
[name:edit, type:UsersController, file:diaspora_diaspora/app/controllers/users_controller.rb, step:Then ]
[name:edit, type:AspectsController, file:diaspora_diaspora/app/controllers/aspects_controller.rb, step:Then ]
[name:evaluate_script, type:Object, file:null, step:Then ]
[name:evaluate_script, type:Object, file:null, step:When ]
[name:execute_script, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find, type:Object, file:null, step:Then ]
[name:find_by_email, type:User, file:diaspora_diaspora/app/models/api/v0/serializers/user.rb, step:Then ]
[name:find_by_email, type:User, file:diaspora_diaspora/app/models/user.rb, step:Then ]
[name:find_by_email, type:User, file:diaspora_diaspora/lib/diaspora/user.rb, step:Then ]
[name:find_by_email, type:User, file:diaspora_diaspora/app/models/api/v0/serializers/user.rb, step:When ]
[name:find_by_email, type:User, file:diaspora_diaspora/app/models/user.rb, step:When ]
[name:find_by_email, type:User, file:diaspora_diaspora/lib/diaspora/user.rb, step:When ]
[name:find_by_email, type:User, file:diaspora_diaspora/app/models/api/v0/serializers/user.rb, step:Given ]
[name:find_by_email, type:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:find_by_email, type:User, file:diaspora_diaspora/lib/diaspora/user.rb, step:Given ]
[name:find_by_text, type:Object, file:null, step:Then ]
[name:first, type:Object, file:null, step:Given ]
[name:first, type:Object, file:null, step:Then ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:has_no_content?, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_no_content, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:Then ]
[name:index, type:PhotosController, file:diaspora_diaspora/app/controllers/activity_streams/photos_controller.rb, step:Then ]
[name:index, type:PhotosController, file:diaspora_diaspora/app/controllers/photos_controller.rb, step:Then ]
[name:index, type:ContactsController, file:diaspora_diaspora/app/controllers/contacts_controller.rb, step:Then ]
[name:index, type:TagFollowingsController, file:diaspora_diaspora/app/controllers/tag_followings_controller.rb, step:Then ]
[name:index, type:ServicesController, file:diaspora_diaspora/app/controllers/services_controller.rb, step:Then ]
[name:index, type:LikesController, file:diaspora_diaspora/app/controllers/likes_controller.rb, step:Then ]
[name:invitation_token, type:Object, file:null, step:Then ]
[name:last, type:Object, file:null, step:Then ]
[name:new, type:ConversationsController, file:diaspora_diaspora/app/controllers/conversations_controller.rb, step:Then ]
[name:new, type:AspectsController, file:diaspora_diaspora/app/controllers/aspects_controller.rb, step:Then ]
[name:next_page_path, type:StreamHelper, file:diaspora_diaspora/app/helpers/stream_helper.rb, step:Then ]
[name:next_photo, type:PhotosController, file:diaspora_diaspora/app/controllers/photos_controller.rb, step:Then ]
[name:page, type:Object, file:null, step:Then ]
[name:page, type:Object, file:null, step:When ]
[name:path, type:FixedRequest, file:diaspora_diaspora/lib/rack/fixed_request.rb, step:Then ]
[name:path_to, type:Paths, file:diaspora_diaspora/features/support/paths.rb, step:Then ]
[name:person, type:Object, file:null, step:Then ]
[name:photos, type:Object, file:null, step:Then ]
[name:post, type:Object, file:null, step:When ]
[name:post, type:Object, file:null, step:Given ]
[name:posts, type:Aspect, file:diaspora_diaspora/lib/stream/aspect.rb, step:Then ]
[name:posts, type:Base, file:diaspora_diaspora/lib/stream/base.rb, step:Then ]
[name:posts, type:CommunitySpotlight, file:diaspora_diaspora/lib/stream/community_spotlight.rb, step:Then ]
[name:posts, type:FollowedTag, file:diaspora_diaspora/lib/stream/followed_tag.rb, step:Then ]
[name:posts, type:Mention, file:diaspora_diaspora/lib/stream/mention.rb, step:Then ]
[name:posts, type:Multi, file:diaspora_diaspora/lib/stream/multi.rb, step:Then ]
[name:posts, type:Person, file:diaspora_diaspora/lib/stream/person.rb, step:Then ]
[name:posts, type:Public, file:diaspora_diaspora/lib/stream/public.rb, step:Then ]
[name:posts, type:Tag, file:diaspora_diaspora/lib/stream/tag.rb, step:Then ]
[name:previous_photo, type:PhotosController, file:diaspora_diaspora/app/controllers/photos_controller.rb, step:Then ]
[name:profile, type:Object, file:null, step:Given ]
[name:raise, type:Object, file:null, step:Then ]
[name:respond_to?, type:Object, file:null, step:Then ]
[name:rows_hash, type:Object, file:null, step:Given ]
[name:send, type:Object, file:null, step:Then ]
[name:sender, type:Object, file:null, step:Then ]
[name:services.index.really_disconnect, type:Object, file:null, step:Then ]
[name:show, type:HomeController, file:diaspora_diaspora/app/controllers/home_controller.rb, step:Then ]
[name:show, type:TagsController, file:diaspora_diaspora/app/controllers/api/v0/tags_controller.rb, step:Then ]
[name:show, type:TagsController, file:diaspora_diaspora/app/controllers/tags_controller.rb, step:Then ]
[name:show, type:PhotosController, file:diaspora_diaspora/app/controllers/activity_streams/photos_controller.rb, step:Then ]
[name:show, type:PhotosController, file:diaspora_diaspora/app/controllers/photos_controller.rb, step:Then ]
[name:show, type:PostsController, file:diaspora_diaspora/app/controllers/posts_controller.rb, step:Then ]
[name:silence_warnings, type:Object, file:null, step:Given ]
[name:silence_warnings, type:Object, file:null, step:When ]
[name:sleep, type:Object, file:null, step:Given ]
[name:sleep, type:Object, file:null, step:When ]
[name:split, type:Object, file:null, step:Given ]
[name:spotlight, type:ContactsController, file:diaspora_diaspora/app/controllers/contacts_controller.rb, step:Then ]
[name:text, type:Photo, file:diaspora_diaspora/app/models/activity_streams/photo.rb, step:Then ]
[name:text, type:Photo, file:diaspora_diaspora/app/models/photo.rb, step:Then ]
[name:update_attributes!, type:Object, file:null, step:Given ]
[name:wait_until, type:Object, file:null, step:When ]
[name:where, type:User, file:diaspora_diaspora/app/models/api/v0/serializers/user.rb, step:Given ]
[name:where, type:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:where, type:User, file:diaspora_diaspora/lib/diaspora/user.rb, step:Given ]
[name:where, type:Object, file:null, step:Given ]
[name:where, type:Request, file:diaspora_diaspora/app/models/request.rb, step:Then ]
[name:with_scope, type:WebSteps, file:diaspora_diaspora/features/step_definitions/web_steps.rb, step:Given ]
[name:with_scope, type:WebSteps, file:diaspora_diaspora/features/step_definitions/web_steps.rb, step:When ]
[name:with_scope, type:WebSteps, file:diaspora_diaspora/features/step_definitions/web_steps.rb, step:Then ]
[name:within, type:Object, file:null, step:Given ]
[name:within, type:Object, file:null, step:Then ]

Referenced pages: 43
diaspora_diaspora/app/views/aspects/edit.html.haml
diaspora_diaspora/app/views/aspects/new.haml
diaspora_diaspora/app/views/comments/_comment.html.haml
diaspora_diaspora/app/views/comments/_comments.html.haml
diaspora_diaspora/app/views/community_spotlight/_user.html.haml
diaspora_diaspora/app/views/contacts/_aspect_listings.haml
diaspora_diaspora/app/views/contacts/index.html.haml
diaspora_diaspora/app/views/contacts/index.mobile.haml
diaspora_diaspora/app/views/contacts/spotlight.haml
diaspora_diaspora/app/views/conversations/new.haml
diaspora_diaspora/app/views/home/show.html.haml
diaspora_diaspora/app/views/home/show.mobile.haml
diaspora_diaspora/app/views/likes/_likes_container.haml
diaspora_diaspora/app/views/likes/index.html.haml
diaspora_diaspora/app/views/people/_index.html.haml
diaspora_diaspora/app/views/people/_person.html.haml
diaspora_diaspora/app/views/photos/_index.html.haml
diaspora_diaspora/app/views/photos/_new_photo.haml
diaspora_diaspora/app/views/photos/_new_profile_photo.haml
diaspora_diaspora/app/views/photos/_photo.haml
diaspora_diaspora/app/views/photos/edit.html.haml
diaspora_diaspora/app/views/photos/show.html.haml
diaspora_diaspora/app/views/photos/show.mobile.haml
diaspora_diaspora/app/views/posts/_photo.html.haml
diaspora_diaspora/app/views/posts/show.html.haml
diaspora_diaspora/app/views/posts/show.mobile.haml
diaspora_diaspora/app/views/profiles/edit.haml
diaspora_diaspora/app/views/reshares/_reshare.haml
diaspora_diaspora/app/views/services/index.html.haml
diaspora_diaspora/app/views/shared/_add_remove_services.haml
diaspora_diaspora/app/views/shared/_author_info.html.haml
diaspora_diaspora/app/views/shared/_contact_list.html.haml
diaspora_diaspora/app/views/shared/_contact_sidebar.html.haml
diaspora_diaspora/app/views/shared/_public_explain.haml
diaspora_diaspora/app/views/shared/_publisher.html.haml
diaspora_diaspora/app/views/shared/_settings_nav.haml
diaspora_diaspora/app/views/shared/_stream.haml
diaspora_diaspora/app/views/shared/_stream_element.html.haml
diaspora_diaspora/app/views/status_messages/_status_message.html.haml
diaspora_diaspora/app/views/tags/_followed_tags_listings.haml
diaspora_diaspora/app/views/tags/show.haml
diaspora_diaspora/app/views/tags/show.mobile.haml
diaspora_diaspora/app/views/users/edit.html.haml

