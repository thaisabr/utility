Given(/^A petition exists and show all signatures is enabled$/) do
  @action_page = FactoryGirl.create(:petition_show_all_signatures).action_page
  expect(@action_page.petition.show_all_signatures).to be_truthy
end
When(/^I visit an action page$/) do
  visit "/action/#{@action_page.title.downcase.gsub(" ", "-")}"
  expect(page).to have_content "All Signatures"
end
Then(/^I should receive a CSV file$/) do
  current_path.should == "/petition/#{@action_page.petition.id}/signatures.csv"
  page.response_headers['Content-Type'].should == "application/octet-stream"
  page.response_headers['Content-Disposition'].should == "attachment"
end
Given (/^I am on "(.*)"$/) do |path|
  visit path
end
When (/^I go to "(.*)"$/) do |path|
  visit path
end
When /^I press "([^\"]*)"$/ do |button|
  click_button(button)
end
When /^I click "([^\"]*)"$/ do |link|
  click_link(link)
end
When /^I fill in "([^\"]*)" with "([^\"]*)"$/ do |field, value|
  fill_in(field.gsub(' ', '_'), :with => value)
end
When /^I fill in "([^\"]*)" for "([^\"]*)"$/ do |value, field|
  fill_in(field.gsub(' ', '_'), :with => value)
end
When /^I fill in the following:$/ do |fields|
  fields.rows_hash.each do |name, value|
    When %{I fill in "#{name}" with "#{value}"}
  end
end
When /^I select "([^\"]*)" from "([^\"]*)"$/ do |value, field|
  select(value, :from => field)
end
When /^I check "([^\"]*)"$/ do |field|
  check(field)
end
When /^I uncheck "([^\"]*)"$/ do |field|
  uncheck(field)
end
When /^I choose "([^\"]*)"$/ do |field|
  choose(field)
end
Then /^I should see "([^\"]*)"$/ do |text|
  page.should have_content(text)
end
Then /^I should see \/([^\/]*)\/$/ do |regexp|
  regexp = Regexp.new(regexp)
  page.should have_content(regexp)
end
Then /^I should not see "([^\"]*)"$/ do |text|
  page.should_not have_content(text)
end
Then /^I should not see \/([^\/]*)\/$/ do |regexp|
  regexp = Regexp.new(regexp)
  page.should_not have_content(regexp)
end
Then /^the "([^\"]*)" field should contain "([^\"]*)"$/ do |field, value|
  find_field(field).value.should =~ /#{value}/
end
Then /^the "([^\"]*)" field should not contain "([^\"]*)"$/ do |field, value|
  find_field(field).value.should_not =~ /#{value}/
end
Then /^the "([^\"]*)" checkbox should be checked$/ do |label|
  find_field(label).should be_checked
end
Then /^the "([^\"]*)" checkbox should not be checked$/ do |label|
  find_field(label).should_not be_checked
end
Then(/^"(.*?)" should be selected from "(.*?)"$/) do |value, field|
  find_field(field).find('option[selected]').text == value
end
Then /^I should be on (.+)$/ do |page_name|
  current_path.should == path_to(page_name)
end
Then /^page should have (.+) message "([^\"]*)"$/ do |type, text|
  page.has_css?("p.#{type}", :text => text, :visible => true)
end
