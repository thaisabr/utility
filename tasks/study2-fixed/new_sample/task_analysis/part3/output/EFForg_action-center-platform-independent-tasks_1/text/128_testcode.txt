When (/^I go to "(.*)"$/) do |path|
  visit path
end
When /^I click "([^\"]*)"$/ do |link|
  click_link(link)
end
When /^I check "([^\"]*)"$/ do |field|
  check(field)
end
Then(/^I should see "(.*?)" within "(.*?)"$/) do |text, container|
  find(container).should have_content(text)
end
Given(/^I exist as an activist$/) do
  create_activist_user
end
Given(/^I am logged in$/) do
  sign_in
end
def create_activist_user
  create_visitor
  delete_user
  @user = FactoryGirl.create(:activist_user, email: @visitor[:email], password: @visitor[:password])
end
def sign_in
  visit '/login'
  fill_in "Email", :with => @visitor[:email]
  fill_in "Password", :with => @visitor[:password]

  click_button "Sign in"
end
def create_visitor
  @visitor ||= { name: "Test User",
    email: "me@example.com",
    password: "strong passwords defeat lobsters covering wealth",
    password_confirmation: "strong passwords defeat lobsters covering wealth" }
end
def delete_user
  @user ||= User.find_by_email(@visitor[:email])
  @user.destroy unless @user.nil?
end
