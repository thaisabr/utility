Classes: 5
[name:Invitation, file:diaspora_diaspora/app/models/invitation.rb, step:Given ]
[name:Request, file:diaspora_diaspora/app/models/request.rb, step:Then ]
[name:URI, file:null, step:Then ]
[name:User, file:diaspora_diaspora/app/models/user.rb, step:Then ]
[name:User, file:diaspora_diaspora/lib/diaspora/user.rb, step:Then ]

Methods: 57
[name:Factory, type:Object, file:null, step:Given ]
[name:are_you_sure, type:Object, file:null, step:Then ]
[name:aspects, type:Object, file:null, step:Given ]
[name:assert_equal, type:Object, file:null, step:Then ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:Then ]
[name:click_link, type:Object, file:null, step:When ]
[name:create, type:Conversation, file:diaspora_diaspora/app/models/conversation.rb, step:Given ]
[name:create_invitee, type:Invitation, file:diaspora_diaspora/app/models/invitation.rb, step:Given ]
[name:current_url, type:Object, file:null, step:Then ]
[name:each, type:Object, file:null, step:Given ]
[name:edit, type:ProfilesController, file:diaspora_diaspora/app/controllers/profiles_controller.rb, step:Then ]
[name:edit, type:UsersController, file:diaspora_diaspora/app/controllers/users_controller.rb, step:Then ]
[name:find_by_email, type:User, file:diaspora_diaspora/app/models/user.rb, step:Then ]
[name:find_by_email, type:User, file:diaspora_diaspora/lib/diaspora/user.rb, step:Then ]
[name:find_by_text, type:Object, file:null, step:Then ]
[name:first, type:Object, file:null, step:Then ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:has_no_content?, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_no_content, type:Object, file:null, step:Then ]
[name:id, type:Fake, file:diaspora_diaspora/lib/fake.rb, step:Given ]
[name:id, type:Fake, file:diaspora_diaspora/lib/fake.rb, step:Then ]
[name:index, type:LikesController, file:diaspora_diaspora/app/controllers/likes_controller.rb, step:Then ]
[name:index, type:PhotosController, file:diaspora_diaspora/app/controllers/activity_streams/photos_controller.rb, step:Then ]
[name:index, type:PhotosController, file:diaspora_diaspora/app/controllers/photos_controller.rb, step:Then ]
[name:invitation_token, type:Object, file:null, step:Then ]
[name:invite_user, type:Object, file:null, step:Given ]
[name:next_page_path, type:StreamHelper, file:diaspora_diaspora/app/helpers/stream_helper.rb, step:Then ]
[name:next_photo, type:PhotosController, file:diaspora_diaspora/app/controllers/photos_controller.rb, step:Then ]
[name:page, type:Object, file:null, step:Then ]
[name:path, type:Object, file:null, step:Then ]
[name:path_to, type:Paths, file:diaspora_diaspora/features/support/paths.rb, step:Then ]
[name:person, type:Object, file:null, step:Then ]
[name:posts, type:Object, file:null, step:Then ]
[name:previous_photo, type:PhotosController, file:diaspora_diaspora/app/controllers/photos_controller.rb, step:Then ]
[name:raise, type:Object, file:null, step:Then ]
[name:respond_to?, type:Object, file:null, step:Then ]
[name:rows_hash, type:Object, file:null, step:Given ]
[name:send, type:Object, file:null, step:Then ]
[name:sender, type:Object, file:null, step:Then ]
[name:show, type:HomeController, file:diaspora_diaspora/app/controllers/home_controller.rb, step:Then ]
[name:show, type:TagsController, file:diaspora_diaspora/app/controllers/tags_controller.rb, step:Then ]
[name:show, type:PhotosController, file:diaspora_diaspora/app/controllers/activity_streams/photos_controller.rb, step:Then ]
[name:show, type:PhotosController, file:diaspora_diaspora/app/controllers/photos_controller.rb, step:Then ]
[name:show, type:PostsController, file:diaspora_diaspora/app/controllers/posts_controller.rb, step:Then ]
[name:silence_warnings, type:Object, file:null, step:Given ]
[name:silence_warnings, type:Object, file:null, step:Then ]
[name:sleep, type:Object, file:null, step:Given ]
[name:sleep, type:Object, file:null, step:Then ]
[name:text, type:Photo, file:diaspora_diaspora/app/models/activity_streams/photo.rb, step:Then ]
[name:text, type:Photo, file:diaspora_diaspora/app/models/photo.rb, step:Then ]
[name:where, type:Request, file:diaspora_diaspora/app/models/request.rb, step:Then ]
[name:with_scope, type:WebSteps, file:diaspora_diaspora/features/step_definitions/web_steps.rb, step:Given ]
[name:with_scope, type:WebSteps, file:diaspora_diaspora/features/step_definitions/web_steps.rb, step:Then ]
[name:with_scope, type:WebSteps, file:diaspora_diaspora/features/step_definitions/web_steps.rb, step:When ]
[name:within, type:Object, file:null, step:Given ]

Referenced pages: 25
diaspora_diaspora/app/views/comments/_comment.html.haml
diaspora_diaspora/app/views/comments/_comments.haml
diaspora_diaspora/app/views/home/show.html.haml
diaspora_diaspora/app/views/home/show.mobile.haml
diaspora_diaspora/app/views/likes/_likes_container.haml
diaspora_diaspora/app/views/likes/index.html.haml
diaspora_diaspora/app/views/people/_index.html.haml
diaspora_diaspora/app/views/photos/_index.html.haml
diaspora_diaspora/app/views/photos/_new_photo.haml
diaspora_diaspora/app/views/photos/_new_profile_photo.haml
diaspora_diaspora/app/views/photos/_photo.haml
diaspora_diaspora/app/views/photos/edit.html.haml
diaspora_diaspora/app/views/photos/show.html.haml
diaspora_diaspora/app/views/photos/show.mobile.haml
diaspora_diaspora/app/views/posts/show.html.haml
diaspora_diaspora/app/views/posts/show.mobile.haml
diaspora_diaspora/app/views/profiles/edit.html.haml
diaspora_diaspora/app/views/reshares/_reshare.haml
diaspora_diaspora/app/views/shared/_author_info.html.haml
diaspora_diaspora/app/views/shared/_settings_nav.haml
diaspora_diaspora/app/views/shared/_stream.haml
diaspora_diaspora/app/views/shared/_stream_element.html.haml
diaspora_diaspora/app/views/status_messages/_status_message.haml
diaspora_diaspora/app/views/tags/show.haml
diaspora_diaspora/app/views/users/edit.html.haml

