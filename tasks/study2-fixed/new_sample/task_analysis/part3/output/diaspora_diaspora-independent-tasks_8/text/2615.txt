Feature: invitation acceptance
Scenario: accept invitation from admin
Given I have been invited by an admin
And I am on my acceptance form page
And I fill in the following:
| Username              | ohai           |
| Email                 | woot@sweet.com |
| user_password         | secret         |
| Password confirmation | secret         |
And I press "Sign up"
Then I should be on the getting started page
And I should see "getting_started_logo"
And I fill in the following:
| profile_first_name | O             |
| profile_last_name  | Hai           |
| tags               | #beingawesome |
And I press "Save and continue"
Then I should see "Profile updated"
And I should see "Would you like to find your Facebook friends on Diaspora?"
And I should not see "Here are the people who are waiting for you:"
Scenario: accept invitation from user
Given I have been invited by a user
And I am on my acceptance form page
And I fill in the following:
| Username              | ohai           |
| Email                 | woot@sweet.com |
| user_password         | secret         |
| Password confirmation | secret         |
And I press "Sign up"
Then I should be on the getting started page
And I should see "getting_started_logo"
And I fill in the following:
| profile_first_name | O     |
| profile_last_name  | Hai   |
| tags               | #tags |
And I press "Save and continue"
Then I should see "Profile updated"
And I should see "Would you like to find your Facebook friends on Diaspora?"
When I follow "Skip"
Then I should be on the aspects page
Feature: infinite scroll
In order to browse without disruption
As medium-sized internet grazing animal
I want the stream to infinite scroll
Background:
Given many posts from alice for bob
When I sign in as "bob@bob.bob"
Scenario: on the main stream post created time
When I follow "posted"
Then I should see 15 posts
And I should see "alice - 15 - #seeded"
When I scroll down
Then I should see 30 posts
And I should see "alice - 30 - #seeded"
When I scroll down
Then I should see 40 posts
And I should see "alice - 40 - #seeded"
When I scroll down
Then I should see "No more"
When I follow "generic"
And I wait for the ajax to finish
Then I should see 15 posts
And I should see "alice - 15 - #seeded"
When I scroll down
Then I should see 30 posts
And I should see "alice - 30 - #seeded"
When I scroll down
Then I should see 40 posts
And I should see "alice - 40 - #seeded"
When I scroll down
Then I should see "No more"
Feature: User manages contacts
In order to share with a limited group
As a User
I want to create new aspects
Scenario: Editing the aspect memberships of a contact from the aspect edit facebox
Given I am signed in
And I have 2 contacts
And I have an aspect called "Cat People"
When I am on the contacts page
And I follow "Cat People"
And I follow "Edit Cat People"
And I wait for the ajax to finish
And I press the first ".contact_list .button"
And I wait for the ajax to finish
Then I should have 1 contact in "Cat People"
When I press the first ".contact_list .button"
And I wait for the ajax to finish
Then I should have 0 contacts in "Cat People"
