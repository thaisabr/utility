Given /^(?:I am signed in|I sign in)$/ do
  automatic_login
  confirm_login
end
When /^I try to sign in manually$/ do
  manual_login
end
  def automatic_login
    @me ||= FactoryGirl.create(:user_with_aspect, :getting_started => false)
    page.driver.visit(new_integration_sessions_path(:user_id => @me.id))
    click_button "Login"
  end
  def manual_login
    visit login_page
    login_as @me.username, @me.password
  end
  def confirm_login
    wait_until { page.has_content?("#{@me.first_name} #{@me.last_name}") }
  end
  def login_page
    path_to "the new user session page"
  end
  def login_as(user, pass)
    fill_in 'user_username', :with=>user
    fill_in 'user_password', :with=>pass
    click_button :submit
  end
  def path_to(page_name)
    case page_name
      when /^the home(?: )?page$/
        stream_path
      when /^step (\d)$/
        if $1.to_i == 1
          getting_started_path
        else
          getting_started_path(:step => $1)
        end
      when /^the tag page for "([^\"]*)"$/
        tag_path($1)
      when /^its ([\w ]+) page$/
        send("#{$1.gsub(/\W+/, '_')}_path", @it)
      when /^the ([\w ]+) page$/
        send("#{$1.gsub(/\W+/, '_')}_path")
      when /^my edit profile page$/
        edit_profile_path
      when /^my profile page$/
        person_path(@me.person)
      when /^my acceptance form page$/
        invite_code_path(InvitationCode.first)
      when /^the requestors profile$/
        person_path(Request.where(:recipient_id => @me.person.id).first.sender)
      when /^"([^\"]*)"'s page$/
        person_path(User.find_by_email($1).person)
      when /^my account settings page$/
        edit_user_path
      when /^my new profile page$/
        person_path(@me.person,  :ex => true)
      when /^the new stream$/
        stream_path(:ex => true)
      when /^forgot password page$/
          new_user_password_path
      when /^"(\/.*)"/
        $1
      else
        raise "Can't find mapping from \"#{page_name}\" to a path."
    end
  end
When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
Then /^(?:|I )should be on (.+)$/ do |page_name|
  wait_until(3) do
    current_path = URI.parse(current_url).path
    current_path == path_to(page_name)
  end

  if current_path.respond_to? :should
    current_path.should == path_to(page_name)
  else
    assert_equal path_to(page_name), current_path
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
And /^I preemptively confirm the alert$/ do
  page.evaluate_script("window.confirm = function() { return true; }")
end
When /^(.*) in the modal window$/ do |action|
  within('#facebox') do
    step action
  end
end
Then /^I should see a flash message containing "(.+)"$/ do |text|
  flash_message_containing? text
end
  def flash_message_containing?(text)
    flash_message.should have_content(text)
  end
  def flash_message(selector=".message")
    selector = "#flash_#{selector}" unless selector == ".message"
    find(selector)
  end
