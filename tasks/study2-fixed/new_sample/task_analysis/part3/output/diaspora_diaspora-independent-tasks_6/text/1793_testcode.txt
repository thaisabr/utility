And /^I expand the publisher$/ do
 click_publisher
end
When /^I append "([^"]*)" to the publisher$/ do |stuff|
  elem = find('#status_message_fake_text')
  elem.native.send_keys(' ' + stuff)

  wait_until do
    find('#status_message_text').value.include?(stuff)
  end
end
When /^I click to delete the first uploaded photo$/ do
  page.execute_script('$("#photodropzone").find(".x").first().click()')
end
When /^I wait for the ajax to finish$/ do
  wait_for_ajax_to_finish
end
When /^I have turned off jQuery effects$/ do
  evaluate_script("$.fx.off = true")
end
When /^I attach the file "([^\"]*)" to hidden element "([^\"]*)"(?: within "([^\"]*)")?$/ do |path, field, selector|
  page.execute_script <<-JS
    $("#{selector || 'body'}").find("input[name=#{field}]").css({opacity: 1});
  JS

  if selector
    step "I attach the file \"#{Rails.root.join(path).to_s}\" to \"#{field}\" within \"#{selector}\""
  else
    step "I attach the file \"#{Rails.root.join(path).to_s}\" to \"#{field}\""
  end

  page.execute_script <<-JS
    $("#{selector || 'body'}").find("input[name=#{field}]").css({opacity: 0});
  JS
end
  def click_publisher
    page.execute_script('
      $("#publisher").removeClass("closed");
      $("#publisher").find("#status_message_fake_text").focus();
    ')
  end
  def wait_for_ajax_to_finish(wait_time=30)
    wait_until(wait_time) do
      evaluate_script("$.active") == 0
    end
  end
Then /^I should not see an uploaded image within the photo drop zone$/ do
  all("#photodropzone img").should be_empty
end
When /^I select "([^"]*)" on the aspect dropdown$/ do |text|
  page.execute_script(
    "$('#publisher .dropdown .dropdown_list')
      .find('li').each(function(i,el){
      var elem = $(el);
      if ('" + text + "' == $.trim(elem.text()) ) {
        elem.click();
      }});")
end
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    silence_warnings { 
      sleep 1 if button == "Share"
      click_button(button) } # ruby 1.9 produces a warning about UTF8 from rack-util
  end
end
Then /^(?:|I )should see (\".+?\"[\s]*)(?:[\s]+within[\s]* "([^"]*)")?$/ do |vars,selector|
  vars.scan(/"([^"]+?)"/).flatten.each do |text|
    with_scope(selector) do
      if page.respond_to? :should
        page.should have_content(text)
      else
        assert page.has_content?(text)
      end
    end
  end
end
Then /^(?:|I )should not see (\".+?\"[\s]*)(?:[\s]+within[\s]* "([^"]*)")?$/ do |vars,selector|
  vars.scan(/"([^"]+?)"/).flatten.each do |text|
    with_scope(selector) do
      if page.respond_to? :should
        page.should have_no_content(text)
      else
        assert page.has_no_content?(text)
      end
    end
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)
    case page_name
      when /^the home(?: )?page$/
        stream_path
      when /^step (\d)$/
        if $1.to_i == 1
          getting_started_path
        else
          getting_started_path(:step => $1)
        end
      when /^the tag page for "([^\"]*)"$/
        tag_path($1)
      when /^its ([\w ]+) page$/
        send("#{$1.gsub(/\W+/, '_')}_path", @it)
      when /^the ([\w ]+) page$/
        send("#{$1.gsub(/\W+/, '_')}_path")
      when /^my edit profile page$/
        edit_profile_path
      when /^my profile page$/
        person_path(@me.person)
      when /^my acceptance form page$/
        invite_code_path(InvitationCode.first)
      when /^the requestors profile$/
        person_path(Request.where(:recipient_id => @me.person.id).first.sender)
      when /^"([^\"]*)"'s page$/
        person_path(User.find_by_email($1).person)
      when /^my account settings page$/
        edit_user_path
      when /^my new profile page$/
        person_path(@me.person,  :ex => true)
      when /^the new stream$/
        stream_path(:ex => true)
      when /^forgot password page$/
          new_user_password_path
      when /^"(\/.*)"/
        $1
      else
        raise "Can't find mapping from \"#{page_name}\" to a path."
    end
  end
When /^I select only "([^"]*)" aspect$/ do |aspect_name|
  within('#aspect_nav') do
    click_link 'Aspects'
    click_link 'Select all' if has_link? 'Select all'
  end

  step %{I wait for the ajax to finish}

  within('#aspect_nav') do
    click_link 'Deselect all' if has_link? 'Deselect all'
  end

  step %{I wait for the ajax to finish}

  within('#aspect_nav') do
    click_link aspect_name
  end

  step %{I wait for the ajax to finish}
end
Given /^a user with username "([^\"]*)"$/ do |username|
  create_user(:email => username + "@" + username + '.' + username, :username => username)
end
Given /^I have following aspect[s]?:$/ do |fields|
  fields.raw.each do |field|
    step %{I have an aspect called "#{field[0]}"}
  end
end
When /^I (?:sign|log) in as "([^"]*)"$/ do |email|
  @me = User.find_by_email(email)
  @me.password ||= 'password'
  automatic_login
  confirm_login
end
  def automatic_login
    @me ||= FactoryGirl.create(:user_with_aspect, :getting_started => false)
    page.driver.visit(new_integration_sessions_path(:user_id => @me.id))
    click_button "Login"
  end
  def confirm_login
    wait_until { page.has_content?("#{@me.first_name} #{@me.last_name}") }
  end
When /^(?:|I )attach the file "([^"]*)" to "([^"]*)"(?: within "([^"]*)")?$/ do |path, field, selector|
  with_scope(selector) do
    attach_file(field, path)
  end
end
Given /^I have an aspect called "([^\"]*)"$/ do |aspect_name|
  @me.aspects.create!(:name => aspect_name)
  @me.reload
end
