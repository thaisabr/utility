Feature: Creating a new post
Background:
Given a user with username "bob"
And I sign in as "bob@bob.bob"
And I trumpet
Scenario: Mention a contact
Given a user named "Alice Smith" with email "alice@alice.alice"
And a user with email "bob@bob.bob" is connected with "alice@alice.alice"
And I trumpet
And I wait for the ajax to finish
And I type "@a" to mention "Alice Smith"
And I start the framing process
Then the post should mention "Alice Smith"
When I finalize my frame
Then the post should mention "Alice Smith"
