Given /^I visit alice's invitation code url$/ do
  @alice ||= Factory(:user, :username => 'alice', :getting_started => false)
  invite_code  = InvitationCode.find_or_create_by_user_id(@alice.id)
  visit invite_code_path(invite_code)
end
When /^I fill in the new user form$/ do
  step 'I fill in "user_username" with "ohai"'
  step 'I fill in "user_email" with "ohai@example.com"'
  step 'I fill in "user_password" with "secret"'
end
And /^I should be able to friend Alice$/ do
  alice = User.find_by_username 'alice'
  step 'I should see "Add contact"'
  step "I should see \"#{alice.name}\""
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    silence_warnings { 
      sleep 1 if button == "Share"
      click_button(button) } # ruby 1.9 produces a warning about UTF8 from rack-util
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
Then /^I should see the "(.*)" message$/ do |message|
  text = case message
           when "alice is excited"
             @alice ||= Factory(:user, :username => "Alice")
             I18n.translate('invitation_codes.excited', :name => @alice.name)
           when "welcome to diaspora"
             I18n.translate('users.getting_started.well_hello_there')
           when 'you are safe for work'
             I18n.translate('profiles.edit.you_are_safe_for_work')
           when 'you are nsfw'
             I18n.translate('profiles.edit.you_are_nsfw')
           else
             raise "muriel, you don't have that message key, add one here"
           end

  page.should have_content(text)
end
When /^(?:|I )fill in "([^"]*)" with "([^"]*)"(?: within "([^"]*)")?$/ do |field, value, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_content(text)
    else
      assert page.has_content?(text)
    end
  end
end
