And /^I expand the publisher$/ do
 click_publisher
end
Then /^(?:|I )should see a "([^\"]*)"(?: within "([^\"]*)")?$/ do |selector, scope_selector|
  with_scope(scope_selector) do
    current_scope.should have_css selector
  end
end
  def click_publisher
    page.execute_script('
     $("#publisher").removeClass("closed");
     $("#publisher").find("#status_message_fake_text").focus();
    ')
  end
  def with_scope(locator)
    if locator
      within(locator, match: :first) do
        yield
      end
    else
      yield
    end
  end
When /^I click the publisher and post "([^"]*)"$/ do |text|
  click_and_post(text)
end
  def click_and_post(text)
    click_publisher
    make_post(text)
  end
  def make_post(text)
    write_in_publisher(text)
    submit_publisher
  end
  def click_publisher
    page.execute_script('
     $("#publisher").removeClass("closed");
     $("#publisher").find("#status_message_fake_text").focus();
    ')
  end
  def write_in_publisher(txt)
    fill_in 'status_message_fake_text', with: txt
  end
  def submit_publisher
    txt = find('#publisher #status_message_fake_text').value
    find('#publisher .creation').click
    page.should have_content(txt) unless page.has_css?('.nsfw-shield')
  end
Given /^I have several oEmbed data in cache$/ do
  scenarios = {
    "photo" => {
      "oembed_data" => {
        "trusted_endpoint_url" => "__!SPOOFED!__",
        "version" => "1.0",
        "type" => "photo",
        "title" => "ZB8T0193",
        "width" => "240",
        "height" => "160",
        "url" => "http://farm4.static.flickr.com/3123/2341623661_7c99f48bbf_m.jpg"
      },
      "link_url" => 'http://www.flickr.com/photos/bees/2341623661',
      "oembed_get_request" => "http://www.flickr.com/services/oembed/?format=json&frame=1&iframe=1&maxheight=420&maxwidth=420&url=http://www.flickr.com/photos/bees/2341623661",
    },

    "unsupported" => {
      "oembed_data" => {},
      "oembed_get_request" => 'http://www.we-do-not-support-oembed.com/index.html',
      "link_url" => 'http://www.we-do-not-support-oembed.com/index.html',
      "discovery_data" => 'no LINK tag!',
    },

    "secure_video" => {
      "oembed_data" => {
        "version" => "1.0",
        "type" => "video",
        "width" => 425,
        "height" => 344,
        "title" => "Amazing Nintendo Facts",
        "html" => "<object width=\"425\" height=\"344\">
            <param name=\"movie\" value=\"http://www.youtube.com/v/M3r2XDceM6A&fs=1\"></param>
            <param name=\"allowFullScreen\" value=\"true\"></param>
            <param name=\"allowscriptaccess\" value=\"always\"></param>
            <embed src=\"http://www.youtube.com/v/M3r2XDceM6A&fs=1\"
	            type=\"application/x-shockwave-flash\" width=\"425\" height=\"344\"
	            allowscriptaccess=\"always\" allowfullscreen=\"true\"></embed>
          </object>",
        "thumbnail_url" => "http://i2.ytimg.com/vi/M3r2XDceM6A/hqdefault.jpg",
        "thumbnail_height" => 360,
        "thumbnail_width" => 480,
      },
      "link_url" => "http://youtube.com/watch?v=M3r2XDceM6A&format=json",
      "oembed_get_request" => "http://www.youtube.com/oembed?scheme=https&format=json&frame=1&iframe=1&maxheight=420&maxwidth=420&url=http://youtube.com/watch?v=M3r2XDceM6A",
    },

    "unsecure_video" => {
      "oembed_data" => {
        "version" => "1.0",
        "type" => "video",
        "title" => "This is a video from an unsecure source",
        "html" => "<object width=\"425\" height=\"344\">
            <param name=\"movie\" value=\"http://www.youtube.com/v/M3r2XDceM6A&fs=1\"></param>
            <param name=\"allowFullScreen\" value=\"true\"></param>
            <param name=\"allowscriptaccess\" value=\"always\"></param>
            <embed src=\"http://www.youtube.com/v/M3r2XDceM6A&fs=1\"
	            type=\"application/x-shockwave-flash\" width=\"425\" height=\"344\"
	            allowscriptaccess=\"always\" allowfullscreen=\"true\"></embed>
          </object>",
        "thumbnail_url" => "http://i2.ytimg.com/vi/M3r2XDceM6A/hqdefault.jpg",
        "thumbnail_height" => 360,
        "thumbnail_width" => 480,
      },
      "link_url" => "http://myrichtube.com/watch?v=M3r2XDceM6A&format=json",
      "discovery_data" => '<link rel="alternate" type="application/json+oembed" href="http://www.mytube.com/oembed?format=json&frame=1&iframe=1&maxheight=420&maxwidth=420&url=http://mytube.com/watch?v=M3r2XDceM6A" />',
      "oembed_get_request" => "http://www.mytube.com/oembed?format=json&frame=1&iframe=1&maxheight=420&maxwidth=420&url=http://mytube.com/watch?v=M3r2XDceM6A",
    },

    "secure_rich" => {
      "oembed_data" => {
        "version" => "1.0",
        "type" => "rich",
        "width" => 425,
        "height" => 344,
        "title" => "Amazing Nintendo Facts",
        "html" => "<object width=\"425\" height=\"344\">
            <param name=\"movie\" value=\"http://www.youtube.com/v/M3r2XDceM6A&fs=1\"></param>
            <param name=\"allowFullScreen\" value=\"true\"></param>
            <param name=\"allowscriptaccess\" value=\"always\"></param>
            <embed src=\"http://www.youtube.com/v/M3r2XDceM6A&fs=1\"
	            type=\"application/x-shockwave-flash\" width=\"425\" height=\"344\"
	            allowscriptaccess=\"always\" allowfullscreen=\"true\"></embed>
          </object>",
        "thumbnail_url" => "http://i2.ytimg.com/vi/M3r2XDceM6A/hqdefault.jpg",
        "thumbnail_height" => 360,
        "thumbnail_width" => 480,
      },
      "link_url" => "http://yourichtube.com/watch?v=M3r2XDceM6A&format=json",
      "oembed_get_request" => "http://www.youtube.com/oembed?scheme=https&format=json&frame=1&iframe=1&maxheight=420&maxwidth=420&url=http://youtube.com/watch?v=M3r2XDceM6A",
    },

    "unsecure_rich" => {
      "oembed_data" => {
        "version" => "1.0",
        "type" => "rich",
        "title" => "This is a video from an unsecure source",
        "html" => "<object width=\"425\" height=\"344\">
            <param name=\"movie\" value=\"http://www.youtube.com/v/M3r2XDceM6A&fs=1\"></param>
            <param name=\"allowFullScreen\" value=\"true\"></param>
            <param name=\"allowscriptaccess\" value=\"always\"></param>
            <embed src=\"http://www.youtube.com/v/M3r2XDceM6A&fs=1\"
	            type=\"application/x-shockwave-flash\" width=\"425\" height=\"344\"
	            allowscriptaccess=\"always\" allowfullscreen=\"true\"></embed>
          </object>",
        "thumbnail_url" => "http://i2.ytimg.com/vi/M3r2XDceM6A/hqdefault.jpg",
        "thumbnail_height" => 360,
        "thumbnail_width" => 480,
      },
      "link_url" => "http://mytube.com/watch?v=M3r2XDceM6A&format=json",
      "discovery_data" => '<link rel="alternate" type="application/json+oembed" href="http://www.mytube.com/oembed?format=json&frame=1&iframe=1&maxheight=420&maxwidth=420&url=http://mytube.com/watch?v=M3r2XDceM6A" />',
      "oembed_get_request" => "http://www.mytube.com/oembed?format=json&frame=1&iframe=1&maxheight=420&maxwidth=420&url=http://mytube.com/watch?v=M3r2XDceM6A",
    },
  }
  scenarios.each do |type, data|
    unless type=='unsupported'
      url = data['oembed_get_request'].split('?')[0]
      store_data = data['oembed_data'].merge('trusted_endpoint_url' => url)
      oembed = OEmbedCache.new(:url => data['link_url']);
      oembed.data = store_data
      oembed.save!
    end
  end
end
Then /^I should see a video player$/ do
  visit aspects_path
  find('.post-content .oembed')
  find('.stream_container').should have_css('.post-content .oembed img')
end
Then /^I should not see a video player$/ do
  find('.stream_container').should_not have_css('.post-content .oembed img')
end
Given /^(?:|[tT]hat )?following user[s]?(?: exist[s]?)?:$/ do |table|
  table.hashes.each do |hash|
    if hash.has_key? "username" and hash.has_key? "email"
      step %{a user named "#{hash['username']}" with email "#{hash['email']}"}
    elsif hash.has_key? "username"
      step %{a user with username "#{hash['username']}"}
    elsif hash.has_key? "email"
      step %{a user with email "#{hash['email']}"}
    end
  end
end
When /^I (?:sign|log) in as "([^"]*)"$/ do |email|
  @me = User.find_by_email(email)
  @me.password ||= 'password'
  automatic_login
  confirm_login
end
  def automatic_login
    @me ||= FactoryGirl.create(:user_with_aspect, :getting_started => false)
    visit(new_integration_sessions_path(:user_id => @me.id))
    click_button "Login"
  end
  def confirm_login
    page.has_content?("#{@me.first_name} #{@me.last_name}")
  end
Given /^(?:|I )am on (.+)$/ do |page_name|
  navigate_to(page_name)
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
Then /^(?:|I )should see (\".+?\"[\s]*)(?:[\s]+within[\s]* "([^"]*)")?$/ do |vars, selector|
  vars.scan(/"([^"]+?)"/).flatten.each do |text|
    with_scope(selector) do
      current_scope.should have_content(text)
    end
  end
end
  def navigate_to(page_name)
    path = path_to(page_name)
    unless path.is_a?(Hash)
      visit(path)
    else
      visit(path[:path])
      await_elem = path[:special_elem]
      find(await_elem.delete(:selector), await_elem)
    end
  end
  def path_to(page_name)
    case page_name
      when /^person_photos page$/
         person_photos_path(@me.person)
      when /^the home(?: )?page$/
        stream_path
      when /^step (\d)$/
        if $1.to_i == 1
          getting_started_path
        else
          getting_started_path(:step => $1)
        end
      when /^the tag page for "([^\"]*)"$/
        tag_path($1)
      when /^its ([\w ]+) page$/
        send("#{$1.gsub(/\W+/, '_')}_path", @it)
      when /^the ([\w ]+) page$/
        send("#{$1.gsub(/\W+/, '_')}_path")
      when /^my edit profile page$/
        edit_profile_path
      when /^my profile page$/
        person_path(@me.person)
      when /^my acceptance form page$/
        invite_code_path(InvitationCode.first)
      when /^the requestors profile$/
        person_path(Request.where(:recipient_id => @me.person.id).first.sender)
      when /^"([^\"]*)"'s page$/
        p = User.find_by_email($1).person
        { path: person_path(p),
          # '.diaspora_handle' on desktop, '.description' on mobile
          special_elem: { selector: '.diaspora_handle, .description', text: p.diaspora_handle }
        }
      when /^"([^\"]*)"'s photos page$/
        p = User.find_by_email($1).person
        person_photos_path p
      when /^my account settings page$/
        edit_user_path
      when /^my new profile page$/
        person_path(@me.person,  :ex => true)
      when /^the new stream$/
        stream_path(:ex => true)
      when /^forgot password page$/
          new_user_password_path
      when /^"(\/.*)"/
        $1
      else
        raise "Can't find mapping from \"#{page_name}\" to a path."
    end
  end
Given /^a user with email "([^\"]*)"$/ do |email|
  create_user(:email => email)
end
Given /^a user with username "([^\"]*)"$/ do |username|
  create_user(:email => username + "@" + username + '.' + username, :username => username)
end
Given /^a user named "([^\"]*)" with email "([^\"]*)"$/ do |name, email|
  first, last = name.split
  user = create_user(:email => email, :username => "#{first}_#{last}")
  user.profile.update_attributes!(:first_name => first, :last_name => last) if first
end
  def create_user(overrides={})
    default_attrs = {
        :password => 'password',
        :password_confirmation => 'password',
        :getting_started => false
    }

    user = FactoryGirl.create(:user, default_attrs.merge(overrides))
    add_standard_aspects(user)
    user
  end
  def add_standard_aspects(user)
    user.aspects.create(:name => "Besties")
    user.aspects.create(:name => "Unicorns")
  end
