Feature: following and being followed
Background:
Given following users exist:
| email             |
| bob@bob.bob       |
| alice@alice.alice |
When I sign in as "bob@bob.bob"
And I am on "alice@alice.alice"'s page
And I add the person to my "Besties" aspect
When I am on the home page
And I expand the publisher
And I fill in the following:
| status_message_fake_text    | I am following you    |
Scenario: seeing public posts of someone you follow
Given I sign in as "alice@alice.alice"
And I am on the home page
And I expand the publisher
And I fill in the following:
| status_message_fake_text    | I am ALICE    |
And I press the first ".toggle" within "#publisher"
And I press the first ".public" within "#publisher"
And I press "Share"
Then I should see "I am ALICE" within "#main_stream"
And I sign out
When I sign in as "bob@bob.bob"
And I am on "alice@alice.alice"'s page
Then I should see "I am ALICE"
When I am on the home page
Then I should see "I am ALICE"
Feature: Keyboard navigation
In order not to have to move my hand to the mouse
As a user
I want to be able to navigate the stream by keyboard
Background:
Given many posts from alice for bob
And I sign in as "bob@bob.bob"
Scenario: navigate downwards
When I am on the home page
And I press the "J" key somewhere
Then post 1 should be highlighted
And I should have navigated to the highlighted post
When I press the "J" key somewhere
Then post 2 should be highlighted
And I should have navigated to the highlighted post
Given I expand the publisher
When I press the "J" key in the publisher
Then post 2 should be highlighted
And I close the publisher
Feature: Mentions
As user
I want to mention another user and have a link to them
To show people that this person exsists.
Scenario: A user mentions another user at the end of a post
Given following users exist:
| username     | email             |
| Bob Jones    | bob@bob.bob       |
| Alice Smith  | alice@alice.alice |
And a user with email "bob@bob.bob" is connected with "alice@alice.alice"
When I sign in as "alice@alice.alice"
And I am on the home page
And I expand the publisher
When I fill in the following:
| status_message_fake_text  | @Bo  |
And I click on the first user in the mentions dropdown list
And I press "Share"
Then I should see "Bob Jones" within ".stream_element"
When I follow "Bob Jones"
Then I should see "Bob Jones"
Feature: mentioning a contact from their profile page
In order to enlighten humanity for the good of society
As a rock star
I want to mention someone more cool than the average bear
Background:
Given I am on the home page
And following users exist:
| username   |
| bob        |
| alice      |
When I sign in as "bob@bob.bob"
And a user with username "bob" is connected with "alice"
And I have following aspects:
| PostingTo            |
| NotPostingThingsHere |
Scenario: mentioning while posting to all aspects
Given I am on "alice@alice.alice"'s page
And I want to mention her from the profile
And I append "I am eating a yogurt" to the publisher
And I press "Share" in the mention modal
Then I should see a flash message indicating success
When I am on the aspects page
And I follow "PostingTo" within "#aspects_list"
Then I should see "I am eating a yogurt"
When I am on the aspects page
And I follow "NotPostingThingsHere" within "#aspects_list"
Then I should see "I am eating a yogurt"
Scenario: mentioning while posting to just one aspect
Given I am on "alice@alice.alice"'s page
And I want to mention her from the profile
And I press the aspect dropdown in the mention modal
And I toggle the aspect "NotPostingThingsHere" in the mention modal
And I press the aspect dropdown in the mention modal
And I append "I am eating a yogurt" to the publisher
And I press "Share" in the mention modal
Then I should see a flash message indicating success
When I am on the aspects page
And I select only "PostingTo" aspect
Then I should see "I am eating a yogurt"
When I am on the aspects page
And I select only "NotPostingThingsHere" aspect
Then I should not see "I am eating a yogurt"
Feature: preview posts in the stream
In order to test markdown without posting
As a user
I want to see a preview of my posts in the stream
Background:
Given following users exist:
| username     | email             |
| Bob Jones    | bob@bob.bob       |
| Alice Smith  | alice@alice.alice |
Scenario: preview a photo with text
Given I expand the publisher
And I attach "spec/fixtures/button.png" to the publisher
When I fill in the following:
| status_message_fake_text    | Look at this dog    |
And I press "Preview"
Then I should see a "img" within ".stream_element div.photo_attachments"
And I should see "Look at this dog" within ".stream_element"
And I close the publisher
Scenario: preview a post with mentions
Given I expand the publisher
And I mention Alice in the publisher
And I press "Preview"
And I follow "Alice Smith"
And I confirm the alert
Then I should see "Alice Smith"
Scenario: preview a post on tag page
Given there is a user "Samuel Beckett" who's tagged "#rockstar"
When I search for "#rockstar"
Then I should be on the tag page for "rockstar"
And I should see "Samuel Beckett"
Given I expand the publisher
When I fill in the following:
| status_message_fake_text    | This preview rocks    |
And I press "Preview"
Then "This preview rocks" should be post 1
And the first post should be a preview
And I close the publisher
Scenario: preview a post with the poll
Given I expand the publisher
When I fill in the following:
| status_message_fake_text    | I am eating yogurt    |
And I press the element "#poll_creator"
When I fill in the following:
| status_message_fake_text    | I am eating yogurt |
| poll_question               | What kind of yogurt do you like? |
And I fill in the following for the options:
| normal |
| not normal  |
And I press "Preview"
Then I should see a ".poll_form" within ".stream_element"
And I should see a "form" within ".stream_element"
And I close the publisher
Feature: posting from the main page
In order to enlighten humanity for the good of society
As a rock star
I want to tell the world I am eating a yogurt
Background:
Given following users exist:
| username   |
| bob        |
| alice      |
And I sign in as "bob@bob.bob"
And a user with username "bob" is connected with "alice"
Given I have following aspects:
| PostingTo            |
| NotPostingThingsHere |
Scenario: post a photo without text
Given I expand the publisher
And I attach "spec/fixtures/button.png" to the publisher
Then I should see an uploaded image within the photo drop zone
When I press "Share"
Then I should see a "img" within ".stream_element div.photo_attachments"
When I go to the aspects page
Then I should see a "img" within ".stream_element div.photo_attachments"
When I log out
And I sign in as "alice@alice.alice"
And I go to "bob@bob.bob"'s page
Then I should see a "img" within ".stream_element div.photo_attachments"
Scenario: back out of uploading a picture to a post with text
Given I expand the publisher
And I have turned off jQuery effects
When I write the status message "I am eating a yogurt"
And I attach "spec/fixtures/button.png" to the publisher
And I click to delete the first uploaded photo
Then I should not see an uploaded image within the photo drop zone
And the publisher should be expanded
And I close the publisher
Scenario: back out of uploading a picture when another has been attached
Given I expand the publisher
And I have turned off jQuery effects
When I write the status message "I am eating a yogurt"
And I attach "spec/fixtures/button.png" to the publisher
And I attach "spec/fixtures/button.png" to the publisher
And I click to delete the first uploaded photo
Then I should see an uploaded image within the photo drop zone
And the publisher should be expanded
And I close the publisher
Feature: posting from own profile page
In order to be all cool and stuff
I want to post from my profile page
Background:
Given I am on the home page
And a user with username "alice"
When I sign in as "alice@alice.alice"
Given I have following aspects:
| Family |
| Work   |
Scenario: back out of posting a photo-only post
Given I expand the publisher
And I have turned off jQuery effects
And I attach "spec/fixtures/button.png" to the publisher
And I click to delete the first uploaded photo
Then I should not see an uploaded image within the photo drop zone
And I close the publisher
