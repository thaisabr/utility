Feature: Viewing my activity on the steam mobile page
In order to navigate Diaspora*
As a mobile user
I want to view my activity stream
Background:
Given a user with username "alice"
And "alice@alice.alice" has a public post with text "Hello! I am #newhere"
And I sign in as "alice@alice.alice" on the mobile website
Scenario: Show post on my activity
When I click on selector "a.like-action.inactive"
And I open the drawer
And I follow "My activity"
Then I should see "My activity"
And I should see "Hello! I am #newhere" within ".ltr"
Feature: Not safe for work
Background:
Given a nsfw user with email "tommy@nsfw.example.com"
And a user with email "laura@office.example.com"
And a user with email "laura@office.example.com" is connected with "tommy@nsfw.example.com"
Scenario: Resharing a nsfw post with a poll
Given "tommy@nsfw.example.com" has a public post with text "Sexy Senators Gone Wild!" and a poll
When I sign in as "laura@office.example.com"
And I toggle all nsfw posts
And I follow "Reshare"
And I confirm the alert
Then I should see a "a.reshare-action.active"
When I go to the home page
Then I should not see "Sexy Senators Gone Wild!"
And I should not see "What do you think about 1 ninjas?"
And I should have 2 nsfw posts
Scenario: Resharing a nsfw post with a location
Given "tommy@nsfw.example.com" has a public post with text "Sexy Senators Gone Wild!" and a location
When I sign in as "laura@office.example.com"
And I toggle all nsfw posts
And I follow "Reshare"
And I confirm the alert
Then I should see a "a.reshare-action.active"
When I go to the home page
Then I should not see "Sexy Senators Gone Wild!"
And I should not see "Posted from:"
And I should have 2 nsfw posts
Scenario: Resharing a nsfw post with a picture
Given "tommy@nsfw.example.com" has a public post with text "Sexy Senators Gone Wild!" and a picture
When I sign in as "laura@office.example.com"
And I toggle all nsfw posts
And I follow "Reshare"
And I confirm the alert
Then I should see a "a.reshare-action.active"
When I go to the home page
Then I should not see "Sexy Senators Gone Wild!"
And I should not see any picture in my stream
And I should have 2 nsfw posts
Feature: reactions mobile post
In order to navigate Diaspora*
As a mobile user
I want to react to posts
Background:
Given following users exist:
| username    | email             |
| Bob Jones   | bob@bob.bob       |
| Alice Smith | alice@alice.alice |
Scenario: like on a mobile post
When I should see "No reactions" within ".show_comments"
And I click on selector "span.show_comments"
And I click on selector "a.like-action.inactive"
Then I should see a "a.like-action.active"
When I go to the stream page
And I should see "1 reaction" within ".show_comments"
And I click on selector "a.show_comments"
Then I should see "1" within ".like_count"
Scenario: comment and delete a mobile post
When I click on selector "a.comment-action.inactive"
And I fill in the following:
| text            | is that a poodle?    |
And I press "Comment"
Then I should see "is that a poodle?"
When I go to the stream page
And I should see "1 reaction" within ".show_comments"
And I click on selector "a.show_comments"
And I should see "1" within ".comment_count"
When I click on selector "a.comment-action"
And I click on selector "a.remove"
And I confirm the alert
Then I should not see "1 reaction" within ".show_comments"
Feature: resharing from the mobile
In order to make Diaspora more viral
As a mobile user
I want to reshare my friend's post
Background:
Given following users exist:
| username    | email             |
| Bob Jones   | bob@bob.bob       |
| Alice Smith | alice@alice.alice |
| Eve Doe     | eve@eve.eve       |
Scenario: Resharing a post from a single post page
And I click on selector "a.reshare-action.inactive"
And I confirm the alert
Then I should see a "a.reshare-action.active"
When I go to the stream page
Then I should see "Reshared via" within ".reshare_via"
Scenario: Resharing a post from a single post page that is reshared
Given the post with text "reshare this!" is reshared by "eve@eve.eve"
And a user with email "alice@alice.alice" is connected with "eve@eve.eve"
And I click on the first selector "a.reshare-action.inactive"
And I confirm the alert
Then I should see a "a.reshare-action.active"
When I go to the stream page
Then I should see "Reshared via" within ".reshare_via"
