When /^I toggle the mobile view$/ do
  visit("/mobile/toggle")
end
Given /^I visit the mobile publisher page$/ do
  visit("/status_messages/new.mobile")
end
When /^I visit the mobile search page$/ do
  visit("/people.mobile")
end
When /^I open the drawer$/ do
  find("#menu-badge").click
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
Then /^(?:|I )should see (\".+?\"[\s]*)(?:[\s]+within[\s]* "([^"]*)")?$/ do |vars, selector|
  vars.scan(/"([^"]+?)"/).flatten.each do |text|
    with_scope(selector) do
      current_scope.should have_content(text)
    end
  end
end
  def with_scope(locator)
    if locator
      within(locator, match: :first) do
        yield
      end
    else
      yield
    end
  end
And /^I click on selector "([^"]*)"$/ do |selector|
  find(selector).click
end
When /^I search for "([^\"]*)"$/ do |search_term|
  fill_in "q", :with => search_term
  find_field("q").native.send_key(:enter)
  have_content(search_term)
end
Given /^(?:|[tT]hat )?following user[s]?(?: exist[s]?)?:$/ do |table|
  table.hashes.each do |hash|
    if hash.has_key? "username" and hash.has_key? "email"
      step %{a user named "#{hash['username']}" with email "#{hash['email']}"}
    elsif hash.has_key? "username"
      step %{a user with username "#{hash['username']}"}
    elsif hash.has_key? "email"
      step %{a user with email "#{hash['email']}"}
    end
  end
end
Given /^a user with email "([^"]*)" is connected with "([^"]*)"$/ do |arg1, arg2|
  user1 = User.where(:email => arg1).first
  user2 = User.where(:email => arg2).first
  connect_users(user1, user1.aspects.where(:name => "Besties").first, user2, user2.aspects.where(:name => "Besties").first)
end
When /^I (?:sign|log) in as "([^"]*)"$/ do |email|
  @me = User.find_by_email(email)
  @me.password ||= 'password'
  automatic_login
  confirm_login
end
  def automatic_login
    @me ||= FactoryGirl.create(:user_with_aspect, :getting_started => false)
    visit(new_integration_sessions_path(:user_id => @me.id))
    click_button "Login"
  end
  def confirm_login
    page.has_content?("#{@me.first_name} #{@me.last_name}")
  end
And /^Alice has a post mentioning Bob$/ do
  alice = User.find_by_email 'alice@alice.alice'
  bob = User.find_by_email 'bob@bob.bob'
  aspect = alice.aspects.where(:name => "Besties").first
  alice.post(:status_message, :text => "@{Bob Jones; #{bob.person.diaspora_handle}}", :to => aspect)
end
Given /^"([^"]*)" has a public post with text "([^"]*)"$/ do |email, text|
  user = User.find_by_email(email)
  user.post(:status_message, :text => text, :public => true, :to => user.aspect_ids)
end
Given /^a user with email "([^\"]*)"$/ do |email|
  create_user(:email => email)
end
Given /^a user with username "([^\"]*)"$/ do |username|
  create_user(:email => username + "@" + username + '.' + username, :username => username)
end
Given /^a user named "([^\"]*)" with email "([^\"]*)"$/ do |name, email|
  first, last = name.split
  user = create_user(:email => email, :username => "#{first}_#{last}")
  user.profile.update_attributes!(:first_name => first, :last_name => last) if first
end
  def create_user(overrides={})
    default_attrs = {
        :password => 'password',
        :password_confirmation => 'password',
        :getting_started => false
    }

    user = FactoryGirl.create(:user, default_attrs.merge(overrides))
    add_standard_aspects(user)
    user
  end
  def add_standard_aspects(user)
    user.aspects.create(:name => "Besties")
    user.aspects.create(:name => "Unicorns")
  end
