Classes: 10
[name:Block, file:diaspora_diaspora/app/models/block.rb, step:Given ]
[name:BlocksController, file:diaspora_diaspora/app/controllers/blocks_controller.rb, step:Given ]
[name:FactoryGirl, file:null, step:Given ]
[name:Hash, file:null, step:Given ]
[name:InvitationCode, file:diaspora_diaspora/app/models/invitation_code.rb, step:Given ]
[name:Request, file:diaspora_diaspora/lib/diaspora/federated/request.rb, step:Given ]
[name:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:UsersController, file:diaspora_diaspora/app/controllers/admin/users_controller.rb, step:Given ]
[name:UsersController, file:diaspora_diaspora/app/controllers/users_controller.rb, step:Given ]
[name:aspects_controller, file:diaspora_diaspora/app/controllers/aspects_controller.rb, step:Given ]

Methods: 68
[name:activity, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:Given ]
[name:aspect_ids, type:Aspect, file:diaspora_diaspora/lib/stream/aspect.rb, step:Given ]
[name:aspect_ids, type:Base, file:diaspora_diaspora/lib/stream/base.rb, step:Given ]
[name:automatic_login, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:bookmarklet, type:StatusMessagesController, file:diaspora_diaspora/app/controllers/status_messages_controller.rb, step:Given ]
[name:btn, type:Object, file:null, step:Given ]
[name:btn btn-block, type:Object, file:null, step:Given ]
[name:btn btn-danger, type:Object, file:null, step:Given ]
[name:button, type:Object, file:null, step:Given ]
[name:changelog_url, type:ApplicationHelper, file:diaspora_diaspora/app/helpers/application_helper.rb, step:Given ]
[name:click, type:Object, file:null, step:When ]
[name:click_button, type:Object, file:null, step:Given ]
[name:confirm_login_mobile, type:UserCukeHelpers, file:diaspora_diaspora/features/support/user_cuke_helpers.rb, step:Given ]
[name:create_user, type:Object, file:null, step:Given ]
[name:current_scope, type:Object, file:null, step:Then ]
[name:delete, type:Object, file:null, step:Given ]
[name:diaspora_handle, type:Object, file:null, step:Given ]
[name:each, type:Object, file:null, step:Then ]
[name:edit, type:ProfilesController, file:diaspora_diaspora/app/controllers/profiles_controller.rb, step:Given ]
[name:edit, type:UsersController, file:diaspora_diaspora/app/controllers/admin/users_controller.rb, step:Given ]
[name:edit, type:UsersController, file:diaspora_diaspora/app/controllers/users_controller.rb, step:Given ]
[name:export_photos, type:UsersController, file:diaspora_diaspora/app/controllers/admin/users_controller.rb, step:Given ]
[name:export_photos, type:UsersController, file:diaspora_diaspora/app/controllers/users_controller.rb, step:Given ]
[name:export_profile, type:UsersController, file:diaspora_diaspora/app/controllers/admin/users_controller.rb, step:Given ]
[name:export_profile, type:UsersController, file:diaspora_diaspora/app/controllers/users_controller.rb, step:Given ]
[name:find, type:Object, file:null, step:Given ]
[name:find, type:Object, file:null, step:When ]
[name:find_by_email, type:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:first, type:InvitationCode, file:diaspora_diaspora/app/models/invitation_code.rb, step:Given ]
[name:first, type:Object, file:null, step:Given ]
[name:flatten, type:Object, file:null, step:Then ]
[name:getting_started, type:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:has_css?, type:Object, file:null, step:Given ]
[name:have_content, type:Object, file:null, step:Then ]
[name:id, type:Object, file:null, step:Given ]
[name:index, type:PhotosController, file:diaspora_diaspora/app/controllers/photos_controller.rb, step:Given ]
[name:index, type:ContactsController, file:diaspora_diaspora/app/controllers/contacts_controller.rb, step:Given ]
[name:index, type:ServicesController, file:diaspora_diaspora/app/controllers/services_controller.rb, step:Given ]
[name:index, type:StatisticsController, file:diaspora_diaspora/app/controllers/statistics_controller.rb, step:Given ]
[name:index, type:TermsController, file:diaspora_diaspora/app/controllers/terms_controller.rb, step:Given ]
[name:is_a?, type:Object, file:null, step:Given ]
[name:liked, type:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:local_or_remote_person_path, type:PeopleHelper, file:diaspora_diaspora/app/helpers/people_helper.rb, step:Given ]
[name:mentioned, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:Given ]
[name:mentioned, type:User, file:diaspora_diaspora/app/models/user.rb, step:Given ]
[name:multi, type:StreamsController, file:diaspora_diaspora/app/controllers/streams_controller.rb, step:Given ]
[name:navigate_to, type:Paths, file:diaspora_diaspora/features/support/paths.rb, step:Given ]
[name:navigate_to, type:Paths, file:diaspora_diaspora/features/support/paths.rb, step:When ]
[name:page, type:Object, file:null, step:Given ]
[name:path_to, type:Paths, file:diaspora_diaspora/features/support/paths.rb, step:Given ]
[name:person, type:Object, file:null, step:Given ]
[name:person, type:PostPresenter, file:diaspora_diaspora/app/presenters/post_presenter.rb, step:Given ]
[name:post, type:Object, file:null, step:Given ]
[name:raise, type:Object, file:null, step:Given ]
[name:scan, type:Object, file:null, step:Then ]
[name:send, type:Object, file:null, step:Given ]
[name:sender, type:Object, file:null, step:Given ]
[name:show, type:TagsController, file:diaspora_diaspora/app/controllers/tags_controller.rb, step:Given ]
[name:show, type:InvitationCodesController, file:diaspora_diaspora/app/controllers/invitation_codes_controller.rb, step:Given ]
[name:show, type:PhotosController, file:diaspora_diaspora/app/controllers/photos_controller.rb, step:Given ]
[name:spotlight, type:ContactsController, file:diaspora_diaspora/app/controllers/contacts_controller.rb, step:Given ]
[name:strip_exif, type:Block, file:diaspora_diaspora/app/models/block.rb, step:Given ]
[name:text, type:Photo, file:diaspora_diaspora/app/models/photo.rb, step:Given ]
[name:to_i, type:Object, file:null, step:Given ]
[name:toggle_mobile, type:HomeController, file:diaspora_diaspora/app/controllers/home_controller.rb, step:Given ]
[name:where, type:Request, file:diaspora_diaspora/lib/diaspora/federated/request.rb, step:Given ]
[name:with_scope, type:WebSteps, file:diaspora_diaspora/features/step_definitions/web_steps.rb, step:Then ]
[name:within, type:Object, file:null, step:Then ]

Referenced pages: 55
diaspora_diaspora/app/views/aspects/_aspect_listings.haml
diaspora_diaspora/app/views/aspects/_aspect_stream.haml
diaspora_diaspora/app/views/aspects/_no_contacts_message.haml
diaspora_diaspora/app/views/aspects/_no_posts_message.haml
diaspora_diaspora/app/views/contacts/_aspect_listings.haml
diaspora_diaspora/app/views/contacts/_header.html.haml
diaspora_diaspora/app/views/contacts/_sidebar.html.haml
diaspora_diaspora/app/views/contacts/index.html.haml
diaspora_diaspora/app/views/contacts/index.mobile.haml
diaspora_diaspora/app/views/contacts/spotlight.haml
diaspora_diaspora/app/views/devise/passwords/new.haml
diaspora_diaspora/app/views/devise/passwords/new.mobile.haml
diaspora_diaspora/app/views/home/default.haml
diaspora_diaspora/app/views/home/show.haml
diaspora_diaspora/app/views/people/_index.html.haml
diaspora_diaspora/app/views/people/_person.html.haml
diaspora_diaspora/app/views/photos/_index.html.haml
diaspora_diaspora/app/views/photos/_new_profile_photo.haml
diaspora_diaspora/app/views/photos/_new_profile_photo.mobile.haml
diaspora_diaspora/app/views/photos/_photo.haml
diaspora_diaspora/app/views/photos/edit.html.haml
diaspora_diaspora/app/views/photos/show.mobile.haml
diaspora_diaspora/app/views/profiles/_edit.haml
diaspora_diaspora/app/views/profiles/_edit_public.haml
diaspora_diaspora/app/views/profiles/edit.haml
diaspora_diaspora/app/views/profiles/edit.mobile.haml
diaspora_diaspora/app/views/publisher/_aspect_dropdown.html.haml
diaspora_diaspora/app/views/publisher/_publisher.html.haml
diaspora_diaspora/app/views/services/_add_remove_services.haml
diaspora_diaspora/app/views/services/index.html.haml
diaspora_diaspora/app/views/shared/_donatepod.html.haml
diaspora_diaspora/app/views/shared/_invitations.haml
diaspora_diaspora/app/views/shared/_links.haml
diaspora_diaspora/app/views/shared/_public_explain.haml
diaspora_diaspora/app/views/shared/_right_sections.html.haml
diaspora_diaspora/app/views/shared/_settings_nav.haml
diaspora_diaspora/app/views/shared/_stream.haml
diaspora_diaspora/app/views/statistics/_statistic.haml
diaspora_diaspora/app/views/statistics/_statistics.haml
diaspora_diaspora/app/views/statistics/statistics.html.haml
diaspora_diaspora/app/views/statistics/statistics.mobile.haml
diaspora_diaspora/app/views/status_messages/bookmarklet.html.haml
diaspora_diaspora/app/views/status_messages/bookmarklet.mobile.haml
diaspora_diaspora/app/views/streams/main_stream.html.haml
diaspora_diaspora/app/views/streams/main_stream.mobile.haml
diaspora_diaspora/app/views/tags/_followed_tags_listings.haml
diaspora_diaspora/app/views/tags/show.haml
diaspora_diaspora/app/views/tags/show.mobile.haml
diaspora_diaspora/app/views/terms/default.haml
diaspora_diaspora/app/views/users/_close_account_modal.haml
diaspora_diaspora/app/views/users/edit.html.haml
diaspora_diaspora/app/views/users/edit.mobile.haml
diaspora_diaspora/app/views/users/getting_started.haml
diaspora_diaspora/app/views/users/getting_started.mobile.haml
diaspora_diaspora/app/views/users/privacy_settings.html.haml

