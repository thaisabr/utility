Given(/^I haven't signed up as an HBX user$/) do
end
When(/^I visit the Employer portal$/) do
  @browser.goto("http://localhost:3000/")
  @browser.a(text: /Employer Portal/).wait_until_present
  @browser.a(text: /Employer Portal/).click
  screenshot("employer_start")
  @browser.a(text: /Create account/).wait_until_present
  @browser.a(text: /Create account/).click
end
And(/^I sign up with valid user data$/) do
  @browser.text_field(name: "user[password_confirmation]").wait_until_present
  @browser.text_field(name: "user[email]").set("trey.evans#{rand(100)}@dc.gov")
  @browser.text_field(name: "user[password]").set("12345678")
  @browser.text_field(name: "user[password_confirmation]").set("12345678")
  screenshot("employer_create_account")
  @browser.input(value: /Create account/).click
end
Then(/^I should see a successful sign up message$/) do
  Watir::Wait.until(30) { @browser.element(text: /Welcome! Your account has been created./).present? }
  screenshot("employer_sign_up_welcome")
  expect(@browser.element(text: /Welcome! Your account has been created./).visible?).to be_truthy
end
And(/^I should see an initial form to enter information about my Employer and myself$/) do
  @browser.text_field(name: "organization[first_name]").wait_until_present
  @browser.text_field(name: "organization[first_name]").set("Doe")
  @browser.text_field(name: "organization[last_name]").set("John")
  @browser.text_field(name: "jq_datepicker_ignore_organization[dob]").set("10/11/1982")
  @browser.text_field(name: "organization[first_name]").click

  @browser.text_field(name: "organization[legal_name]").set("Turner Agency, Inc")
  @browser.text_field(name: "organization[dba]").set("Turner Agency, Inc")
  @browser.text_field(name: "organization[fein]").set("123456999")
  input_field = @browser.divs(class: "selectric-interaction-choice-control-organization-entity-kind").first
  input_field.click
  input_field.li(text: /C Corporation/).click
  @browser.text_field(name: "organization[office_locations_attributes][0][address_attributes][address_1]").set("100 North Street")
  @browser.text_field(name: "organization[office_locations_attributes][0][address_attributes][address_2]").set("Suite 990")
  @browser.text_field(name: "organization[office_locations_attributes][0][address_attributes][city]").set("Washington")
  input_field = @browser.divs(class: "selectric-interaction-choice-control-organization-office-locations-attributes-0-address-attributes-state").first
  input_field.click
  input_field.li(text: /DC/).click
  @browser.text_field(name: "organization[office_locations_attributes][0][address_attributes][zip]").set("20002")
  @browser.text_field(name: "organization[office_locations_attributes][0][phone_attributes][area_code]").set("678")
  @browser.text_field(name: "organization[office_locations_attributes][0][phone_attributes][number]").set("1230987")
  screenshot("employer_portal_person_data_new")
  @browser.button(class: "interaction-click-control-create-employer").click
end
When(/^I click on the Employees tab$/) do
  @browser.refresh
  @browser.a(text: /Employees/).wait_until_present
  scroll_then_click(@browser.a(text: /Employees/))
end
Then(/^I should see the employee family roster$/) do
  @browser.a(text: /Add Employee/).wait_until_present
  screenshot("employer_census_family")
  expect(@browser.a(text: /Add Employee/).visible?).to be_truthy
end
And(/^It should default to active tab$/) do
  @browser.radio(id: "terminated_no").wait_until_present
  expect(@browser.radio(id: "terminated_no").set?).to be_truthy
  expect(@browser.radio(id: "terminated_yes").set?).to be_falsey
  expect(@browser.radio(id: "family_waived").set?).to be_falsey
  expect(@browser.radio(id: "family_all").set?).to be_falsey
end
When(/^I click on add employee button$/) do
  @browser.a(text: /Add Employee/).wait_until_present
  @browser.a(text: /Add Employee/).click
end
Then(/^I should see a form to enter information about employee, address and dependents details$/) do
  @browser.element(class: /interaction-click-control-create-employee/).wait_until_present
  screenshot("create_census_employee")
  # Census Employee
  @browser.text_field(class: /interaction-field-control-census-employee-first-name/).wait_until_present
  @browser.text_field(class: /interaction-field-control-census-employee-first-name/).set("John")
  @browser.text_field(class: /interaction-field-control-census-employee-middle-name/).set("K")
  @browser.text_field(class: /interaction-field-control-census-employee-last-name/).set("Doe")
  @browser.text_field(class: /interaction-field-control-census-employee-name-sfx/).set("Jr")
  @browser.text_field(class: /interaction-field-control-census-employee-dob/).set("01/01/1980")
  @browser.text_field(class: /interaction-field-control-census-employee-ssn/).set("786120965")
  #@browser.radio(class: /interaction-choice-control-value-radio-male/).set
  @browser.radio(id: /radio_male/).fire_event("onclick")
  @browser.text_field(class: /interaction-field-control-census-employee-hired-on/).set("10/10/2014")
  @browser.checkbox(class: /interaction-choice-control-value-census-employee-is-business-owner/).set
  input_field = @browser.divs(class: /selectric-wrapper/).first
  input_field.click
  input_field.li(text: /Silver PPO Group/).click
  # Address
  @browser.text_field(class: /interaction-field-control-census-employee-address-attributes-address-1/).wait_until_present
  @browser.text_field(class: /interaction-field-control-census-employee-address-attributes-address-1/).set("1026 Potomac")
  @browser.text_field(class: /interaction-field-control-census-employee-address-attributes-address-2/).set("apt abc")
  @browser.text_field(class: /interaction-field-control-census-employee-address-attributes-city/).set("Alpharetta")
  select_state = @browser.divs(text: /SELECT STATE/).last
  select_state.click
  scroll_then_click(@browser.li(text: /GA/))
  @browser.text_field(class: /interaction-field-control-census-employee-address-attributes-zip/).set("30228")
  email_kind = @browser.divs(text: /SELECT KIND/).last
  email_kind.click
  @browser.li(text: /home/).click
  @browser.text_field(class: /interaction-field-control-census-employee-email-attributes-address/).set("trey.john@dc.gov")

  @browser.a(text: /Add Family Member/).click
  @browser.div(id: /dependent_info/).wait_until_present
  @browser.text_field(id: /census_employee_census_dependents_attributes_\d+_first_name/).set("Mary")
  @browser.text_field(id: /census_employee_census_dependents_attributes_\d+_middle_name/).set("K")
  @browser.text_field(id: /census_employee_census_dependents_attributes_\d+_last_name/).set("Doe")
  @browser.text_field(id: /census_employee_census_dependents_attributes_\d+_dob/).set("10/12/2012")
  @browser.text_field(id: /census_employee_census_dependents_attributes_\d+_ssn/).set("321321321")
  @browser.label(for: /census_employee_census_dependents_attributes_\d+_gender_female/).click
  input_field = @browser.divs(class: "selectric-wrapper").last
  input_field.click
  input_field.li(text: /Child/).click

  screenshot("create_census_employee_with_data")
  @browser.element(class: /interaction-click-control-create-employee/).click
end
And(/^I should see employer census family created success message$/) do
  Watir::Wait.until(30) {  @browser.text.include?("Census Employee is successfully created.") }
  screenshot("employer_census_new_family_success_message")
  @browser.refresh
  @browser.a(text: /Employees/).wait_until_present
  @browser.a(text: /Employees/).click
  @browser.a(text: /John K Doe Jr/).wait_until_present
  expect(@browser.a(text: /John K Doe Jr/).visible?).to be_truthy
  expect(@browser.a(text: /Edit/).visible?).to be_truthy
  expect(@browser.a(text: /Terminate/).visible?).to be_truthy
end
When(/^I click on Edit family button for a census family$/) do
  @browser.a(text: /Edit/).wait_until_present
  @browser.a(text: /Edit/).click
end
Then(/^I should see a form to update the contents of the census employee$/) do
  #Organization.where(legal_name: 'Turner Agency, Inc').first.employer_profile.census_employees.first.delink_employee_role!
  @browser.button(value: /Update Employee/).wait_until_present
  @browser.text_field(id: /jq_datepicker_ignore_census_employee_dob/).set("01/01/1980")
  @browser.text_field(id: /census_employee_ssn/).set("786120965")
  @browser.text_field(id: /census_employee_first_name/).set("Patrick")
  select_state = @browser.divs(text: /GA/).last
  select_state.click
  scroll_then_click(@browser.li(text: /VA/))
  #@browser.text_field(id: /census_employee_address_attributes_state/).set("VA")
  @browser.text_field(id: /census_employee_census_dependents_attributes_\d+_first_name/).set("Mariah")
  screenshot("update_census_employee_with_data")
  @browser.button(value: /Update Employee/).click
end
And(/^I should see employer census family updated success message$/) do
  Watir::Wait.until(30) {  @browser.text.include?("Census Employee is successfully updated.") }
end
When(/^I go to the benefits tab I should see plan year information$/) do
  @browser.a(text: /Benefits/).wait_until_present
  @browser.a(text: /Benefits/).click
end
And(/^I should see a button to create new plan year$/) do
  @browser.a(text: /Add Plan Year/).wait_until_present
  screenshot("employer_plan_year")
  @browser.a(text: /Add Plan Year/).click
end
And(/^I should be able to enter plan year, benefits, relationship benefits with high FTE$/) do
#Plan Year
  @browser.text_field(id: "jq_datepicker_ignore_plan_year_open_enrollment_start_on").wait_until_present
  screenshot("employer_add_plan_year")
  @browser.text_field(id: "jq_datepicker_ignore_plan_year_open_enrollment_start_on").set("91/96/2017")
  @browser.h3(text: /Plan Year/).click
  expect(@browser.text.include?("Open Enrollment Start Date: Invalid date format!")).to be_truthy
  # happy path
  start_on_field = @browser.div(class: /selectric-wrapper/, text: /SELECT START ON/i)
  start_on_field.click
  start_on_field.li(index: 1).click
  @browser.h4(text: /Recommend Date/).wait_until_present
  expect(@browser.text.include?("employer initial application earliest submit on")).to be_truthy
  @browser.text_field(name: "plan_year[fte_count]").click
  @browser.text_field(name: "plan_year[fte_count]").set("235")
  @browser.text_field(name: "plan_year[pte_count]").set("15")
  @browser.text_field(name: "plan_year[msp_count]").set("3")
  # Benefit Group
  @browser.text_field(name: "plan_year[benefit_groups_attributes][0][title]").set("Silver PPO Group")
  elected_field = @browser.div(class: /selectric-wrapper/, text: /Select Plan Offerings/)
  elected_field.click
  elected_field.li(text: /All plans from a given carrier/).click
  sleep(1)
  input_field = @browser.div(class: /selectric-wrapper/, text: /SELECT CARRIER/)
  input_field.click
  sleep(1)
  input_field.li(text: /CareFirst/).click
  ref_plan = @browser.divs(class: /selectric-wrapper/, text: /SELECT REFERENCE PLAN/).last
  ref_plan.click
  ref_plan.li(index: 5).click # select plan from list.
  # Relationship Benefit
  @browser.text_field(name: "plan_year[benefit_groups_attributes][0][relationship_benefits_attributes][0][premium_pct]").set(51)
  @browser.checkboxes(id: 'plan_year_benefit_groups_attributes_0_relationship_benefits_attributes_0_offered').first.set(true)
  @browser.text_field(name: "plan_year[benefit_groups_attributes][0][relationship_benefits_attributes][3][premium_pct]").set(15)
  @browser.checkboxes(id: 'plan_year_benefit_groups_attributes_0_relationship_benefits_attributes_3_offered').first.set(true)
  @browser.checkboxes(id: 'plan_year_benefit_groups_attributes_0_relationship_benefits_attributes_1_offered').first.set(false)
  @browser.checkboxes(id: 'plan_year_benefit_groups_attributes_0_relationship_benefits_attributes_2_offered').first.set(false)
  screenshot("employer_add_plan_year_info")
  @browser.button(value: /Create Plan Year/).click
end
And(/^I should see a success message after clicking on create plan year button$/) do
  Watir::Wait.until(30) {  @browser.text.include?("Plan Year successfully created.") }
  screenshot("employer_plan_year_success_message")
  # TimeKeeper.set_date_of_record_unprotected!("2014-11-01")
  # Organization.where(legal_name: 'Turner Agency, Inc').first.employer_profile.plan_years.first.update(start_on: '2015-01-01', end_on: '2015-12-31', open_enrollment_start_on: '2014-11-01', open_enrollment_end_on: '2014-11-30')
end
When(/^I enter filter in plan selection page$/) do
  Watir::Wait.until(30) { @browser.element(text: /Filter Results/).present? }
  # @browser.a(text: /All Filters/).wait_until_present
  # @browser.a(text: /All Filters/).click
  @browser.checkboxes(class: /plan-type-selection-filter/).first.set(true)
  @browser.element(class: /apply-btn/, text: /Apply/).wait_until_present
  @browser.element(class: /apply-btn/, text: /Apply/).click
end
When(/^I enter combined filter in plan selection page$/) do
  #@browser.a(text: /All Filters/).wait_until_present
  #@browser.a(text: /All Filters/).click
  # @browser.checkboxes(class: /plan-type-selection-filter/).first.wait_until_present
  # @browser.checkboxes(class: /plan-type-selection-filter/).first.set(false)
  # Nationwide
  # @browser.checkboxes(class: /plan-metal-network-selection-filter/).first.set(true)
  #@browser.checkbox(class: /checkbox-custom interaction-choice-control-value-checkbox-5/).set(true)

  # Platinum
  @browser.execute_script(
    'arguments[0].scrollIntoView();',
    @browser.element(:text => /Choose a healthcare plan/)
  )
  @browser.checkboxes(class: /plan-metal-level-selection-filter/).first.set(true)
  @browser.checkboxes(class: /plan-type-selection-filter/).first.set(false)
  @browser.checkboxes(class: /plan-type-selection-filter/).last.set(true)
  @browser.text_field(class: /plan-metal-deductible-from-selection-filter/).set("1000")
  @browser.text_field(class: /plan-metal-deductible-to-selection-filter/).set("3900")
  @browser.text_field(class: /plan-metal-premium-from-selection-filter/).set("5")
  @browser.text_field(class: /plan-metal-premium-to-selection-filter/).set("250")
  @browser.element(class: /apply-btn/, text: /Apply/).click
end
Then(/^I should see the combined filter results$/) do
  @browser.divs(class: /plan-row/).select(&:visible?).first do |plan|
    expect(plan.text.include?("BlueChoice Plus HSA/HRA $3500")).to eq true
    expect(plan.text.include?("Bronze")).to eq true
    expect(plan.element(text: "$126.18").visible?).to eq true
  end
end
When(/^I go to the benefits tab$/) do
  @browser.element(class: /interaction-click-control-benefits/).wait_until_present
  @browser.element(class: /interaction-click-control-benefits/).click
end
Then(/^I should see the plan year$/) do
  @browser.element(class: /interaction-click-control-publish-plan-year/).wait_until_present
end
When(/^I click on publish plan year$/) do
  @browser.element(class: /interaction-click-control-publish-plan-year/).wait_until_present
  @browser.element(class: /interaction-click-control-publish-plan-year/).click
end
Then(/^I should see Publish Plan Year Modal with warnings$/) do

  @browser.element(class: /modal-body/).wait_until_present

  modal = @browser.div(class: /modal-dialog/)
  warnings= modal.ul(class: /application-warnings/)
  # TODO:  Add visible? to the next line.  This test is not valid.
  expect(warnings.element(text: /number of full time equivalents (FTEs) exceeds maximum allowed/i)).to be_truthy
end
Then(/^I click on the Cancel button$/) do
  modal = @browser.div(class: 'modal-dialog')
  modal.a(class: 'interaction-click-control-cancel').click
end
Then(/^I should be on the Plan Year Edit page with warnings$/) do
  @browser.element(id: /plan_year/).present?
  warnings= @browser.div(class: 'alert-plan-year')
  # TODO:  Add visible? to the next line.  This test is not valid.
  expect(warnings.element(text: /number of full time equivalents (FTEs) exceeds maximum allowed/i)).to be_truthy
end
Then(/^I update the FTE field with valid input and save plan year$/) do
  @browser.text_field(name: "plan_year[fte_count]").set("10")
  scroll_then_click(@browser.element(class: 'interaction-click-control-create-plan-year'))
end
Then(/^I should see a plan year successfully saved message$/) do
  @browser.element(class: /mainmenu/).wait_until_present
  # TODO:  Add visible? to the next line.  This test is not valid.
  expect(@browser.element(text: /Plan Year successfully saved/)).to be_truthy
end
  def screenshot(name = nil)
    if @take_screens
      shot_count = @screen_count.to_s.rjust(3, "0")
      f_name = name.nil? ? shot_count : "#{shot_count}_#{name}"
      @browser.screenshot.save("tmp/#{f_name}.png")
      @screen_count = @screen_count + 1
    end
  end
def scroll_then_click(element)
  scroll_into_view(element).click
  element
end
def scroll_into_view(element)
  @browser.execute_script(
    'arguments[0].scrollIntoView(false);',
    element
  )
  element
end
Given(/^I do not exist as a user$/) do
end
Given(/^I have an existing employee record$/) do
end
Given(/^I have an existing person record$/) do
end
When(/^I go to the employee account creation page$/) do
  @browser.goto("http://localhost:3000/")
  @browser.a(text: "Employee Portal").wait_until_present
  screenshot("start")
  scroll_then_click(@browser.a(text: "Employee Portal"))
  @browser.a(text: "Create account").wait_until_present
  screenshot("employee_portal")
  scroll_then_click(@browser.a(text: "Create account"))
end
When(/^I enter my new account information$/) do
  @browser.text_field(name: "user[password_confirmation]").wait_until_present
  screenshot("create_account")
  @email = "swhite#{rand(100)}@example.com"
  @password = "12345678"
  @browser.text_field(name: "user[email]").set(@email)
  @browser.text_field(name: "user[password]").set(@password)
  @browser.text_field(name: "user[password_confirmation]").set(@password)
  scroll_then_click(@browser.input(value: "Create account"))
end
Then(/^I should be logged in$/) do
  @browser.a(href: /consumer.employee.search/).wait_until_present
  screenshot("logged_in_welcome")
  expect(@browser.a(href: /consumer.employee.search/).visible?).to be_truthy
end
When (/^(.*) logs? out$/) do |someone|
  sleep 2
  scroll_then_click(@browser.element(class: /interaction-click-control-logout/))
  @browser.element(class: /interaction-click-control-logout/).wait_while_present
end
When(/^I go to register as an employee$/) do
  @browser.element(class: /interaction-click-control-continue/).wait_until_present
  scroll_then_click(@browser.element(class: /interaction-click-control-continue/))
end
Then(/^I should see the employee search page$/) do
  @browser.text_field(class: /interaction-field-control-person-first-name/).wait_until_present
  screenshot("employer_search")
  expect(@browser.text_field(class: /interaction-field-control-person-first-name/).visible?).to be_truthy
end
When(/^I enter the identifying info of (.*)$/) do |named_person|
  person = people[named_person]
  @browser.text_field(class: /interaction-field-control-person-first-name/).set(person[:first_name])
  @browser.text_field(name: "person[last_name]").set(person[:last_name])
  @browser.text_field(name: "jq_datepicker_ignore_person[dob]").set(person[:dob])
  scroll_then_click(@browser.label(:text=> /FIRST NAME/))
  @browser.text_field(name: "person[ssn]").set(person[:ssn])
  @browser.radio(id: /radio_male/).fire_event("onclick")
  screenshot("information_entered")
  @browser.element(class: /interaction-click-control-continue/).wait_until_present
  scroll_then_click(@browser.element(class: /interaction-click-control-continue/))
end
Then(/^I should see the matching employee record form$/) do
  @browser.element(text: /Turner Agency/).wait_until_present
  screenshot("employer_search_results")
  expect(@browser.element(text: /Turner Agency/).visible?).to be_truthy
end
When(/^I accept the matched employer$/) do
  scroll_then_click(@browser.input(value: /This is my employer/))
  @browser.input(name: "person[emails_attributes][0][address]").wait_until_present
  screenshot("update_personal_info")
end
When(/^I complete the matching employee form$/) do
  @browser.text_field(name: "person[addresses_attributes][0][address_1]").set("84 I st")
  @browser.text_field(name: "person[addresses_attributes][0][address_2]").set("Suite 201")
  @browser.text_field(name: "person[addresses_attributes][0][city]").set("Herndon")

  input_field = @browser.divs(class: "selectric-interaction-choice-control-person-addresses-attributes-0-state").first
  input_field.click
  input_field.li(text: /VA/).click
  @browser.text_field(name: "person[addresses_attributes][0][zip]").set("20171")

  @browser.text_field(name: "person[phones_attributes][0][full_phone_number]").set("2025551234")
  scroll_then_click(@browser.text_field(name: "person[emails_attributes][1][address]"))
  screenshot("personal_info_complete")
  # scroll_then_click(@browser.button(class: /interaction-click-control-continue/))  # TODO cant find interaction element
  @browser.button(id: /btn-continue/).wait_until_present
  scroll_then_click(@browser.button(id: /btn-continue/))
end
Then(/^I should see the dependents page$/) do
  @browser.a(text: /Add Member/).wait_until_present
  screenshot("dependents_page")
  expect(@browser.a(text: /Add Member/).visible?).to be_truthy
end
Then(/^I should see (.*) dependents*$/) do |n|
  n = n.to_i
  expect(@browser.li(class: "dependent_list", index: n)).not_to exist
  expect(@browser.li(class: "dependent_list", index: n - 1)).to exist
end
When(/^I click continue on the dependents page$/) do
  scroll_then_click(@browser.element(class: /interaction-click-control-continue/, id: /btn-continue/))
end
Then(/^I should see the group selection page$/) do
  @browser.form(action: /group_selection\/create/).wait_until_present
  screenshot("group_selection")
end
When(/^I click continue on the group selection page$/) do
  @browser.element(class: /interaction-click-control-continue/, id: /btn-continue/).wait_until_present
  @browser.execute_script("$('.interaction-click-control-continue').trigger('click')")
  #scroll_then_click(@browser.element(class: /interaction-click-control-continue/, id: /btn-continue/))
end
Then(/^I should see the plan shopping welcome page$/) do
  @browser.element(text: /Filter Results/i).wait_until_present
  # @browser.h3(text: /Select a Plan/).wait_until_present
  screenshot("plan_shopping_welcome")
  expect(@browser.element(text: /Choose a healthcare plan/i).visible?).to be_truthy
  # expect(@browser.h3(text: /Select a Plan/).visible?).to be_truthy
end
Then(/^I should see the list of plans$/) do
  @browser.a(text: /Select/).wait_until_present
  screenshot("plan_shopping")
end
When(/^I select a plan on the plan shopping page$/) do
  @browser.execute_script(
    'arguments[0].scrollIntoView();',
    @browser.element(:text => /Choose a healthcare plan/)
  )
  scroll_then_click(@browser.a(text: /Select/))
end
Then(/^I should see the coverage summary page$/) do
  @browser.element(class: /interaction-click-control-purchase/).wait_until_present
  screenshot("summary_page")
  expect(@browser.element(text: /Confirm Your Plan Selection/i).visible?).to be_truthy
end
When(/^I click on purchase button on the coverage summary page$/) do
  @browser.execute_script('$(".interaction-click-control-purchase").trigger("click")')
  #scroll_then_click(@browser.element(class: /interaction-click-control-purchase/))
end
And(/^I click on continue button on the purchase confirmation pop up$/) do
  @browser.element(class: /interaction-click-control-continue/).wait_until_present
  @browser.element(class: /interaction-click-control-continue/).click
end
Then(/^I should see the "my account" page$/) do
  @browser.element(text: /Your Life Events/i).wait_until_present
  screenshot("my_account_page")
  expect(@browser.element(text: /Your Life Events/i).visible?).to be_truthy
end
When(/^I visit consumer profile homepage$/) do
  visit "/consumer_profiles/home"
end
Then(/^I should see the "YOUR LIFE EVENTS" section/) do
  @browser.element(text: /YOUR LIFE EVENTS/i).wait_until_present
  screenshot("your_life_events")
  expect(@browser.element(text: /YOUR LIFE EVENTS/i).visible?).to be_truthy
end
When(/^I click on the plans tab$/) do
  @browser.element(class: /interaction-click-control-plans/).wait_until_present
  scroll_then_click(@browser.element(class: /interaction-click-control-plans/))
end
Then(/^I should see my plan/) do
  @browser.element(text: /plan name/i).wait_until_present
  screenshot("my_plan")
  expect(@browser.element(text: /plan name/i).visible?).to be_truthy
end
When(/^I should see a published success message$/) do
  @browser.element(class: /mainmenu/).wait_until_present
  expect(@browser.element(text: /Plan Year successfully published/).visible?).to be_truthy
end
  def screenshot(name = nil)
    if @take_screens
      shot_count = @screen_count.to_s.rjust(3, "0")
      f_name = name.nil? ? shot_count : "#{shot_count}_#{name}"
      @browser.screenshot.save("tmp/#{f_name}.png")
      @screen_count = @screen_count + 1
    end
  end
def scroll_then_click(element)
  scroll_into_view(element).click
  element
end
def people
  {
    "Soren White" => {
      first_name: "Soren",
      last_name: "White",
      dob: "08/13/1979",
      ssn: "670991234",
    },
    "Patrick Doe" => {
      first_name: "Patrick",
      last_name: "Doe",
      dob: "01/01/1980",
      ssn: "786120965",
    }
  }
end
