Feature: show photos
Background:
Given following users exist:
| username      | email               |
| Bob Jones     | bob@bob.bob         |
| Alice Smith   | alice@alice.alice   |
| Robert Grimm  | robert@grimm.grimm  |
Scenario: see my own photos
When I am on "robert@grimm.grimm"'s page
And I follow "View all" within ".image_list"
Then I should be on person_photos page
