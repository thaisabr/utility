Feature: preview posts in the stream
In order to test markdown without posting
As a user
I want to see a preview of my posts in the stream
Background:
Given following users exist:
| username     | email             |
| Bob Jones    | bob@bob.bob       |
| Alice Smith  | alice@alice.alice |
Scenario: preview a post with the poll
Given I expand the publisher
When I fill in the following:
| status_message_fake_text    | I am eating yogurt    |
And I press the element "#poll_creator"
When I fill in the following:
| status_message_fake_text    | I am eating yogurt |
| poll_question               | What kind of yogurt do you like? |
And I fill in the following for the options:
| normal |
| not normal  |
And I press "Preview"
Then I should see a ".poll_form" within ".stream_element"
And I should see a "form" within ".stream_element"
