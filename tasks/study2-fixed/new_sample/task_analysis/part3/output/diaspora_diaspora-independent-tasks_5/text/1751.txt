Feature: posting
In order to take over humanity for the good of society
As a rock star
I want to see what humanity is saying about particular tags
Background:
Given following users exist:
| username   |
| bob        |
| alice      |
Scenario: can post a message from the tag page
Then I should see "#boss" within "#publisher"
And I post "#boss from the tag page"
And I search for "#boss"
Then I should see "#boss from the tag page"
Scenario: see a tag that I am following and I post over there
When I go to the home page
And I follow "#boss"
And I post "#boss from the #boss tag page"
Then I should see "#boss from the #boss tag page" within "body"
