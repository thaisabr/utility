Feature: oembed
In order to make videos easy accessible
As a user
I want the links in my posts be replaced by their oEmbed representation
Background:
Given following user exists:
| username    | email             |
| Alice Smith | alice@alice.alice |
Scenario: Post an unsecure video link
Given I expand the publisher
When I fill in the following:
| status_message_fake_text    | http://mytube.com/watch?v=M3r2XDceM6A&format=json    |
And I press "Share"
And I follow "My Aspects"
Then I should not see a video player
And I should see "http://mytube.com/watch?v=M3r2XDceM6A&format=json" within ".stream_element"
Scenario: Post an unsecure rich-typed link
Given I expand the publisher
When I fill in the following:
| status_message_fake_text    | http://myrichtube.com/watch?v=M3r2XDceM6A&format=json    |
And I press "Share"
And I follow "My Aspects"
Then I should not see a video player
And I should see "http://myrichtube.com/watch?v=M3r2XDceM6A&format=json" within ".stream_element"
Feature: Blocking a user from the stream
Background:
Given following users exist:
| username    | email             |
| Bob Jones   | bob@bob.bob       |
| Alice Smith | alice@alice.alice |
Scenario: Blocking a user from the profile page
When I am on the home page
And I follow "Alice Smith"
When I click on the profile block button
And I confirm the alert
And I am on the home page
Then I should not see any posts in my stream
