Feature: creating and editing skins
Scenario: Admins should be able to approve public skins
Given the unapproved public skin "public skin"
And I am logged in as an admin
When I go to admin's skins page
And I check "public skin"
And I submit
Then I should see "The following skins were updated: public skin"
When I follow "Approved Skins"
Then I should see "public skin" within "table#approved"
Scenario: Admins should be able to unapprove public skins, which should also remove them from preferences
Given "skinuser" is using the approved public skin "public skin" with css "#title { text-decoration: blink;}"
And I unapprove the skin "public skin"
Then I should see "The following skins were updated: public skin"
And I should see "public skin" within "table#unapproved"
When I am logged in as "skinuser"
And I am on skinuser's preferences page
Then "Default" should be selected within "preference_skin_id"
And I should not see "#title"
And I should not see "text-decoration: blink;"
Scenario: Create a complex replacement skin
Given I have loaded site skins
And I am logged in as "skinner"
And I set up the skin "Complex"
And I select "replace archive skin entirely" from "skin_role"
And I check "add_site_parents"
And I submit
Then I should see a create confirmation message
