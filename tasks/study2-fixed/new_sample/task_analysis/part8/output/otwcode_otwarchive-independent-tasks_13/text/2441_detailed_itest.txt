Classes: 43
[name:AbuseReport, file:otwcode_otwarchive/app/models/abuse_report.rb, step:null]
[name:AbuseReport, file:otwcode_otwarchive/app/models/abuse_report.rb, step:When ]
[name:AdminPost, file:otwcode_otwarchive/app/models/admin_post.rb, step:null]
[name:AdminPost, file:otwcode_otwarchive/app/models/admin_post.rb, step:When ]
[name:ArchiveFaq, file:otwcode_otwarchive/app/models/archive_faq.rb, step:null]
[name:ArchiveFaq, file:otwcode_otwarchive/app/models/archive_faq.rb, step:When ]
[name:ArchiveFaqsController, file:otwcode_otwarchive/app/controllers/archive_faqs_controller.rb, step:null]
[name:ArchiveFaqsController, file:otwcode_otwarchive/app/controllers/archive_faqs_controller.rb, step:When ]
[name:BookmarksController, file:otwcode_otwarchive/app/controllers/bookmarks_controller.rb, step:null]
[name:BookmarksController, file:otwcode_otwarchive/app/controllers/bookmarks_controller.rb, step:When ]
[name:Category, file:otwcode_otwarchive/app/models/category.rb, step:Given ]
[name:Collection, file:otwcode_otwarchive/app/models/collection.rb, step:null]
[name:Collection, file:otwcode_otwarchive/app/models/collection.rb, step:When ]
[name:DEFAULT_PASSWORD, file:null, step:Given ]
[name:Factory, file:null, step:Given ]
[name:Fandom, file:otwcode_otwarchive/app/models/fandom.rb, step:Given ]
[name:Feedback, file:otwcode_otwarchive/app/models/feedback.rb, step:null]
[name:Feedback, file:otwcode_otwarchive/app/models/feedback.rb, step:When ]
[name:KnownIssue, file:otwcode_otwarchive/app/models/known_issue.rb, step:null]
[name:KnownIssue, file:otwcode_otwarchive/app/models/known_issue.rb, step:When ]
[name:Rating, file:otwcode_otwarchive/app/models/rating.rb, step:Given ]
[name:RelatedWorksController, file:otwcode_otwarchive/app/controllers/related_works_controller.rb, step:null]
[name:RelatedWorksController, file:otwcode_otwarchive/app/controllers/related_works_controller.rb, step:When ]
[name:Series, file:otwcode_otwarchive/app/models/series.rb, step:null]
[name:Series, file:otwcode_otwarchive/app/models/series.rb, step:When ]
[name:SkinsController, file:otwcode_otwarchive/app/controllers/admin/skins_controller.rb, step:null]
[name:SkinsController, file:otwcode_otwarchive/app/controllers/skins_controller.rb, step:null]
[name:SkinsController, file:otwcode_otwarchive/app/controllers/admin/skins_controller.rb, step:When ]
[name:SkinsController, file:otwcode_otwarchive/app/controllers/skins_controller.rb, step:When ]
[name:Tag, file:otwcode_otwarchive/app/models/tag.rb, step:null]
[name:Tag, file:otwcode_otwarchive/app/models/tag.rb, step:When ]
[name:User, file:otwcode_otwarchive/app/models/user.rb, step:Given ]
[name:User, file:otwcode_otwarchive/vendor/plugins/rails_indexes/test/fixtures/app/models/user.rb, step:Given ]
[name:UserSession, file:otwcode_otwarchive/app/models/user_session.rb, step:Given ]
[name:UserSession, file:otwcode_otwarchive/app/models/user_session.rb, step:null]
[name:UserSessionsController, file:otwcode_otwarchive/app/controllers/user_sessions_controller.rb, step:null]
[name:Warning, file:otwcode_otwarchive/app/models/warning.rb, step:Given ]
[name:Work, file:otwcode_otwarchive/app/models/work.rb, step:null]
[name:Work, file:otwcode_otwarchive/app/models/work.rb, step:When ]
[name:WorksController, file:otwcode_otwarchive/app/controllers/static/works_controller.rb, step:null]
[name:WorksController, file:otwcode_otwarchive/app/controllers/works_controller.rb, step:null]
[name:WorksController, file:otwcode_otwarchive/app/controllers/static/works_controller.rb, step:When ]
[name:WorksController, file:otwcode_otwarchive/app/controllers/works_controller.rb, step:When ]

Methods: 237
[name:.participant_confirm_delete, type:Object, file:null, step:null]
[name:.participant_confirm_delete, type:Object, file:null, step:When ]
[name:about, type:OrphansController, file:otwcode_otwarchive/app/controllers/orphans_controller.rb, step:null]
[name:about, type:OrphansController, file:otwcode_otwarchive/app/controllers/orphans_controller.rb, step:When ]
[name:activate, type:AdminUsersController, file:otwcode_otwarchive/app/controllers/admin/admin_users_controller.rb, step:Given ]
[name:activate, type:UsersController, file:otwcode_otwarchive/app/controllers/users_controller.rb, step:Given ]
[name:activate, type:User, file:otwcode_otwarchive/app/models/user.rb, step:Given ]
[name:are you sure you want to delete this claim?, type:Object, file:null, step:null]
[name:are you sure you want to delete this claim?, type:Object, file:null, step:When ]
[name:are you sure?, type:Object, file:null, step:null]
[name:are you sure?, type:Object, file:null, step:When ]
[name:assignment_notification, type:Collection, file:otwcode_otwarchive/app/models/collection.rb, step:null]
[name:assignment_notification, type:Collection, file:otwcode_otwarchive/app/models/collection.rb, step:When ]
[name:check, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:When ]
[name:collection_names, type:Work, file:otwcode_otwarchive/app/models/work.rb, step:null]
[name:collection_names, type:Work, file:otwcode_otwarchive/app/models/work.rb, step:When ]
[name:comment, type:Object, file:null, step:null]
[name:comment, type:Object, file:null, step:When ]
[name:count, type:User, file:otwcode_otwarchive/app/models/user.rb, step:Given ]
[name:count, type:User, file:otwcode_otwarchive/vendor/plugins/rails_indexes/test/fixtures/app/models/user.rb, step:Given ]
[name:count_visible_comments, type:AdminPost, file:otwcode_otwarchive/app/models/admin_post.rb, step:null]
[name:count_visible_comments, type:AdminPost, file:otwcode_otwarchive/app/models/admin_post.rb, step:When ]
[name:create, type:FeedbacksController, file:otwcode_otwarchive/app/controllers/feedbacks_controller.rb, step:null]
[name:create, type:FeedbacksController, file:otwcode_otwarchive/app/controllers/feedbacks_controller.rb, step:When ]
[name:download_url_for_work, type:WorksHelper, file:otwcode_otwarchive/app/helpers/works_helper.rb, step:null]
[name:download_url_for_work, type:WorksHelper, file:otwcode_otwarchive/app/helpers/works_helper.rb, step:When ]
[name:edit, type:ArchiveFaqsController, file:otwcode_otwarchive/app/controllers/archive_faqs_controller.rb, step:null]
[name:edit, type:KnownIssuesController, file:otwcode_otwarchive/app/controllers/known_issues_controller.rb, step:null]
[name:edit, type:WorksController, file:otwcode_otwarchive/app/controllers/static/works_controller.rb, step:null]
[name:edit, type:WorksController, file:otwcode_otwarchive/app/controllers/works_controller.rb, step:null]
[name:edit, type:CollectionsController, file:otwcode_otwarchive/app/controllers/collections_controller.rb, step:null]
[name:edit, type:CollectionsController, file:otwcode_otwarchive/app/controllers/static/collections_controller.rb, step:null]
[name:edit, type:AdminPostsController, file:otwcode_otwarchive/app/controllers/admin_posts_controller.rb, step:null]
[name:edit, type:BookmarksController, file:otwcode_otwarchive/app/controllers/bookmarks_controller.rb, step:null]
[name:edit, type:SkinsController, file:otwcode_otwarchive/app/controllers/admin/skins_controller.rb, step:null]
[name:edit, type:SkinsController, file:otwcode_otwarchive/app/controllers/skins_controller.rb, step:null]
[name:edit, type:SeriesController, file:otwcode_otwarchive/app/controllers/series_controller.rb, step:null]
[name:edit, type:ArchiveFaqsController, file:otwcode_otwarchive/app/controllers/archive_faqs_controller.rb, step:When ]
[name:edit, type:KnownIssuesController, file:otwcode_otwarchive/app/controllers/known_issues_controller.rb, step:When ]
[name:edit, type:SkinsController, file:otwcode_otwarchive/app/controllers/admin/skins_controller.rb, step:When ]
[name:edit, type:SkinsController, file:otwcode_otwarchive/app/controllers/skins_controller.rb, step:When ]
[name:edit, type:WorksController, file:otwcode_otwarchive/app/controllers/static/works_controller.rb, step:When ]
[name:edit, type:WorksController, file:otwcode_otwarchive/app/controllers/works_controller.rb, step:When ]
[name:edit, type:CollectionsController, file:otwcode_otwarchive/app/controllers/collections_controller.rb, step:When ]
[name:edit, type:CollectionsController, file:otwcode_otwarchive/app/controllers/static/collections_controller.rb, step:When ]
[name:edit, type:AdminPostsController, file:otwcode_otwarchive/app/controllers/admin_posts_controller.rb, step:When ]
[name:edit, type:BookmarksController, file:otwcode_otwarchive/app/controllers/bookmarks_controller.rb, step:When ]
[name:edit, type:SeriesController, file:otwcode_otwarchive/app/controllers/series_controller.rb, step:When ]
[name:edit_tags, type:WorksController, file:otwcode_otwarchive/app/controllers/static/works_controller.rb, step:null]
[name:edit_tags, type:WorksController, file:otwcode_otwarchive/app/controllers/works_controller.rb, step:null]
[name:edit_tags, type:WorksController, file:otwcode_otwarchive/app/controllers/static/works_controller.rb, step:When ]
[name:edit_tags, type:WorksController, file:otwcode_otwarchive/app/controllers/works_controller.rb, step:When ]
[name:feed, type:TagsController, file:otwcode_otwarchive/app/controllers/tags_controller.rb, step:null]
[name:feed, type:TagsController, file:otwcode_otwarchive/app/controllers/tags_controller.rb, step:When ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find, type:UserSession, file:otwcode_otwarchive/app/models/user_session.rb, step:Given ]
[name:find_or_create_by_name_and_canonical, type:Warning, file:otwcode_otwarchive/app/models/warning.rb, step:Given ]
[name:find_or_create_by_name_and_canonical, type:Rating, file:otwcode_otwarchive/app/models/rating.rb, step:Given ]
[name:find_or_create_by_name_and_canonical, type:Fandom, file:otwcode_otwarchive/app/models/fandom.rb, step:Given ]
[name:find_or_create_by_name_and_canonical, type:Category, file:otwcode_otwarchive/app/models/category.rb, step:Given ]
[name:gift_notification, type:Collection, file:otwcode_otwarchive/app/models/collection.rb, step:null]
[name:gift_notification, type:Collection, file:otwcode_otwarchive/app/models/collection.rb, step:When ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:has_no_content?, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:Then ]
[name:have_no_content, type:Object, file:null, step:Then ]
[name:index, type:ArchiveFaqsController, file:otwcode_otwarchive/app/controllers/archive_faqs_controller.rb, step:null]
[name:index, type:KnownIssuesController, file:otwcode_otwarchive/app/controllers/known_issues_controller.rb, step:null]
[name:index, type:WorksController, file:otwcode_otwarchive/app/controllers/static/works_controller.rb, step:null]
[name:index, type:WorksController, file:otwcode_otwarchive/app/controllers/works_controller.rb, step:null]
[name:index, type:TagsController, file:otwcode_otwarchive/app/controllers/tags_controller.rb, step:null]
[name:index, type:TagWranglingsController, file:otwcode_otwarchive/app/controllers/tag_wranglings_controller.rb, step:null]
[name:index, type:CollectionsController, file:otwcode_otwarchive/app/controllers/collections_controller.rb, step:null]
[name:index, type:CollectionsController, file:otwcode_otwarchive/app/controllers/static/collections_controller.rb, step:null]
[name:index, type:CollectionParticipantsController, file:otwcode_otwarchive/app/controllers/collection_participants_controller.rb, step:null]
[name:index, type:CollectionItemsController, file:otwcode_otwarchive/app/controllers/collection_items_controller.rb, step:null]
[name:index, type:FandomsController, file:otwcode_otwarchive/app/controllers/fandoms_controller.rb, step:null]
[name:index, type:FandomsController, file:otwcode_otwarchive/app/controllers/static/fandoms_controller.rb, step:null]
[name:index, type:BookmarksController, file:otwcode_otwarchive/app/controllers/bookmarks_controller.rb, step:null]
[name:index, type:AdminPostsController, file:otwcode_otwarchive/app/controllers/admin_posts_controller.rb, step:null]
[name:index, type:SkinsController, file:otwcode_otwarchive/app/controllers/admin/skins_controller.rb, step:null]
[name:index, type:SkinsController, file:otwcode_otwarchive/app/controllers/skins_controller.rb, step:null]
[name:index, type:MediaController, file:otwcode_otwarchive/app/controllers/media_controller.rb, step:null]
[name:index, type:MediaController, file:otwcode_otwarchive/app/controllers/static/media_controller.rb, step:null]
[name:index, type:PseudsController, file:otwcode_otwarchive/app/controllers/pseuds_controller.rb, step:null]
[name:index, type:SeriesController, file:otwcode_otwarchive/app/controllers/series_controller.rb, step:null]
[name:index, type:ArchiveFaqsController, file:otwcode_otwarchive/app/controllers/archive_faqs_controller.rb, step:When ]
[name:index, type:KnownIssuesController, file:otwcode_otwarchive/app/controllers/known_issues_controller.rb, step:When ]
[name:index, type:SkinsController, file:otwcode_otwarchive/app/controllers/admin/skins_controller.rb, step:When ]
[name:index, type:SkinsController, file:otwcode_otwarchive/app/controllers/skins_controller.rb, step:When ]
[name:index, type:WorksController, file:otwcode_otwarchive/app/controllers/static/works_controller.rb, step:When ]
[name:index, type:WorksController, file:otwcode_otwarchive/app/controllers/works_controller.rb, step:When ]
[name:index, type:TagsController, file:otwcode_otwarchive/app/controllers/tags_controller.rb, step:When ]
[name:index, type:TagWranglingsController, file:otwcode_otwarchive/app/controllers/tag_wranglings_controller.rb, step:When ]
[name:index, type:CollectionsController, file:otwcode_otwarchive/app/controllers/collections_controller.rb, step:When ]
[name:index, type:CollectionsController, file:otwcode_otwarchive/app/controllers/static/collections_controller.rb, step:When ]
[name:index, type:CollectionParticipantsController, file:otwcode_otwarchive/app/controllers/collection_participants_controller.rb, step:When ]
[name:index, type:CollectionItemsController, file:otwcode_otwarchive/app/controllers/collection_items_controller.rb, step:When ]
[name:index, type:FandomsController, file:otwcode_otwarchive/app/controllers/fandoms_controller.rb, step:When ]
[name:index, type:FandomsController, file:otwcode_otwarchive/app/controllers/static/fandoms_controller.rb, step:When ]
[name:index, type:BookmarksController, file:otwcode_otwarchive/app/controllers/bookmarks_controller.rb, step:When ]
[name:index, type:AdminPostsController, file:otwcode_otwarchive/app/controllers/admin_posts_controller.rb, step:When ]
[name:index, type:MediaController, file:otwcode_otwarchive/app/controllers/media_controller.rb, step:When ]
[name:index, type:MediaController, file:otwcode_otwarchive/app/controllers/static/media_controller.rb, step:When ]
[name:index, type:PseudsController, file:otwcode_otwarchive/app/controllers/pseuds_controller.rb, step:When ]
[name:index, type:SeriesController, file:otwcode_otwarchive/app/controllers/series_controller.rb, step:When ]
[name:link_to_tag_bookmarks, type:BookmarksHelper, file:otwcode_otwarchive/app/helpers/bookmarks_helper.rb, step:null]
[name:link_to_tag_bookmarks, type:BookmarksHelper, file:otwcode_otwarchive/app/helpers/bookmarks_helper.rb, step:When ]
[name:list_challenges, type:CollectionsController, file:otwcode_otwarchive/app/controllers/collections_controller.rb, step:null]
[name:list_challenges, type:CollectionsController, file:otwcode_otwarchive/app/controllers/static/collections_controller.rb, step:null]
[name:list_challenges, type:CollectionsController, file:otwcode_otwarchive/app/controllers/collections_controller.rb, step:When ]
[name:list_challenges, type:CollectionsController, file:otwcode_otwarchive/app/controllers/static/collections_controller.rb, step:When ]
[name:manage, type:ArchiveFaqsController, file:otwcode_otwarchive/app/controllers/archive_faqs_controller.rb, step:null]
[name:manage, type:ChaptersController, file:otwcode_otwarchive/app/controllers/chapters_controller.rb, step:null]
[name:manage, type:SeriesController, file:otwcode_otwarchive/app/controllers/series_controller.rb, step:null]
[name:manage, type:ArchiveFaqsController, file:otwcode_otwarchive/app/controllers/archive_faqs_controller.rb, step:When ]
[name:manage, type:ChaptersController, file:otwcode_otwarchive/app/controllers/chapters_controller.rb, step:When ]
[name:manage, type:SeriesController, file:otwcode_otwarchive/app/controllers/series_controller.rb, step:When ]
[name:name, type:Tag, file:otwcode_otwarchive/app/models/tag.rb, step:null]
[name:name, type:Collection, file:otwcode_otwarchive/app/models/collection.rb, step:null]
[name:name, type:Tag, file:otwcode_otwarchive/app/models/tag.rb, step:When ]
[name:name, type:Collection, file:otwcode_otwarchive/app/models/collection.rb, step:When ]
[name:navigate, type:WorksController, file:otwcode_otwarchive/app/controllers/static/works_controller.rb, step:null]
[name:navigate, type:WorksController, file:otwcode_otwarchive/app/controllers/works_controller.rb, step:null]
[name:navigate, type:WorksController, file:otwcode_otwarchive/app/controllers/static/works_controller.rb, step:When ]
[name:navigate, type:WorksController, file:otwcode_otwarchive/app/controllers/works_controller.rb, step:When ]
[name:new, type:UserSessionsController, file:otwcode_otwarchive/app/controllers/user_sessions_controller.rb, step:null]
[name:new, type:UsersController, file:otwcode_otwarchive/app/controllers/users_controller.rb, step:null]
[name:new, type:UsersController, file:otwcode_otwarchive/vendor/plugins/rails_indexes/test/fixtures/app/controllers/users_controller.rb, step:null]
[name:new, type:PasswordsController, file:otwcode_otwarchive/app/controllers/passwords_controller.rb, step:null]
[name:new, type:ArchiveFaqsController, file:otwcode_otwarchive/app/controllers/archive_faqs_controller.rb, step:null]
[name:new, type:AbuseReportsController, file:otwcode_otwarchive/app/controllers/abuse_reports_controller.rb, step:null]
[name:new, type:KnownIssuesController, file:otwcode_otwarchive/app/controllers/known_issues_controller.rb, step:null]
[name:new, type:AdminPostsController, file:otwcode_otwarchive/app/controllers/admin_posts_controller.rb, step:null]
[name:new, type:ChaptersController, file:otwcode_otwarchive/app/controllers/chapters_controller.rb, step:null]
[name:new, type:WorksController, file:otwcode_otwarchive/app/controllers/static/works_controller.rb, step:null]
[name:new, type:WorksController, file:otwcode_otwarchive/app/controllers/works_controller.rb, step:null]
[name:new, type:CollectionsController, file:otwcode_otwarchive/app/controllers/collections_controller.rb, step:null]
[name:new, type:CollectionsController, file:otwcode_otwarchive/app/controllers/static/collections_controller.rb, step:null]
[name:new, type:FeedbacksController, file:otwcode_otwarchive/app/controllers/feedbacks_controller.rb, step:null]
[name:new, type:ExternalWorksController, file:otwcode_otwarchive/app/controllers/external_works_controller.rb, step:null]
[name:new, type:SkinsController, file:otwcode_otwarchive/app/controllers/admin/skins_controller.rb, step:null]
[name:new, type:SkinsController, file:otwcode_otwarchive/app/controllers/skins_controller.rb, step:null]
[name:new, type:PseudsController, file:otwcode_otwarchive/app/controllers/pseuds_controller.rb, step:null]
[name:new, type:WorksController, file:otwcode_otwarchive/app/controllers/static/works_controller.rb, step:When ]
[name:new, type:WorksController, file:otwcode_otwarchive/app/controllers/works_controller.rb, step:When ]
[name:new, type:FeedbacksController, file:otwcode_otwarchive/app/controllers/feedbacks_controller.rb, step:When ]
[name:new, type:SkinsController, file:otwcode_otwarchive/app/controllers/admin/skins_controller.rb, step:When ]
[name:new, type:SkinsController, file:otwcode_otwarchive/app/controllers/skins_controller.rb, step:When ]
[name:new, type:ArchiveFaqsController, file:otwcode_otwarchive/app/controllers/archive_faqs_controller.rb, step:When ]
[name:new, type:KnownIssuesController, file:otwcode_otwarchive/app/controllers/known_issues_controller.rb, step:When ]
[name:new, type:AdminPostsController, file:otwcode_otwarchive/app/controllers/admin_posts_controller.rb, step:When ]
[name:new, type:AbuseReportsController, file:otwcode_otwarchive/app/controllers/abuse_reports_controller.rb, step:When ]
[name:new, type:ChaptersController, file:otwcode_otwarchive/app/controllers/chapters_controller.rb, step:When ]
[name:new, type:CollectionsController, file:otwcode_otwarchive/app/controllers/collections_controller.rb, step:When ]
[name:new, type:CollectionsController, file:otwcode_otwarchive/app/controllers/static/collections_controller.rb, step:When ]
[name:new, type:ExternalWorksController, file:otwcode_otwarchive/app/controllers/external_works_controller.rb, step:When ]
[name:new, type:PseudsController, file:otwcode_otwarchive/app/controllers/pseuds_controller.rb, step:When ]
[name:page, type:Object, file:null, step:Then ]
[name:parent_name, type:Collection, file:otwcode_otwarchive/app/models/collection.rb, step:null]
[name:parent_name, type:Collection, file:otwcode_otwarchive/app/models/collection.rb, step:When ]
[name:recipients, type:Work, file:otwcode_otwarchive/app/models/work.rb, step:null]
[name:recipients, type:Work, file:otwcode_otwarchive/app/models/work.rb, step:When ]
[name:respond_to?, type:Prompt, file:otwcode_otwarchive/app/models/prompt.rb, step:Then ]
[name:series, type:Object, file:null, step:null]
[name:series, type:Object, file:null, step:When ]
[name:show, type:ArchiveFaqsController, file:otwcode_otwarchive/app/controllers/archive_faqs_controller.rb, step:null]
[name:show, type:CollectionsController, file:otwcode_otwarchive/app/controllers/collections_controller.rb, step:null]
[name:show, type:CollectionsController, file:otwcode_otwarchive/app/controllers/static/collections_controller.rb, step:null]
[name:show, type:WorksController, file:otwcode_otwarchive/app/controllers/static/works_controller.rb, step:null]
[name:show, type:WorksController, file:otwcode_otwarchive/app/controllers/works_controller.rb, step:null]
[name:show, type:PseudsController, file:otwcode_otwarchive/app/controllers/pseuds_controller.rb, step:null]
[name:show, type:SerialWorksController, file:otwcode_otwarchive/app/controllers/serial_works_controller.rb, step:null]
[name:show, type:ChallengeClaimsController, file:otwcode_otwarchive/app/controllers/challenge_claims_controller.rb, step:null]
[name:show, type:AdminPostsController, file:otwcode_otwarchive/app/controllers/admin_posts_controller.rb, step:null]
[name:show, type:UsersController, file:otwcode_otwarchive/app/controllers/users_controller.rb, step:null]
[name:show, type:UsersController, file:otwcode_otwarchive/vendor/plugins/rails_indexes/test/fixtures/app/controllers/users_controller.rb, step:null]
[name:show, type:BookmarksController, file:otwcode_otwarchive/app/controllers/bookmarks_controller.rb, step:null]
[name:show, type:SerialWorksController, file:otwcode_otwarchive/app/controllers/serial_works_controller.rb, step:When ]
[name:show, type:CollectionsController, file:otwcode_otwarchive/app/controllers/collections_controller.rb, step:When ]
[name:show, type:CollectionsController, file:otwcode_otwarchive/app/controllers/static/collections_controller.rb, step:When ]
[name:show, type:WorksController, file:otwcode_otwarchive/app/controllers/static/works_controller.rb, step:When ]
[name:show, type:WorksController, file:otwcode_otwarchive/app/controllers/works_controller.rb, step:When ]
[name:show, type:PseudsController, file:otwcode_otwarchive/app/controllers/pseuds_controller.rb, step:When ]
[name:show, type:ChallengeClaimsController, file:otwcode_otwarchive/app/controllers/challenge_claims_controller.rb, step:When ]
[name:show, type:AdminPostsController, file:otwcode_otwarchive/app/controllers/admin_posts_controller.rb, step:When ]
[name:show, type:UsersController, file:otwcode_otwarchive/app/controllers/users_controller.rb, step:When ]
[name:show, type:UsersController, file:otwcode_otwarchive/vendor/plugins/rails_indexes/test/fixtures/app/controllers/users_controller.rb, step:When ]
[name:show, type:BookmarksController, file:otwcode_otwarchive/app/controllers/bookmarks_controller.rb, step:When ]
[name:show_multiple, type:WorksController, file:otwcode_otwarchive/app/controllers/static/works_controller.rb, step:null]
[name:show_multiple, type:WorksController, file:otwcode_otwarchive/app/controllers/works_controller.rb, step:null]
[name:show_multiple, type:WorksController, file:otwcode_otwarchive/app/controllers/static/works_controller.rb, step:When ]
[name:show_multiple, type:WorksController, file:otwcode_otwarchive/app/controllers/works_controller.rb, step:When ]
[name:single_comment, type:Object, file:null, step:null]
[name:single_comment, type:Object, file:null, step:When ]
[name:subscribe, type:Object, file:null, step:null]
[name:subscribe, type:Object, file:null, step:When ]
[name:summary, type:Feedback, file:otwcode_otwarchive/app/models/feedback.rb, step:null]
[name:summary, type:Work, file:otwcode_otwarchive/app/models/work.rb, step:null]
[name:summary, type:Series, file:otwcode_otwarchive/app/models/series.rb, step:null]
[name:summary, type:Feedback, file:otwcode_otwarchive/app/models/feedback.rb, step:When ]
[name:summary, type:Work, file:otwcode_otwarchive/app/models/work.rb, step:When ]
[name:summary, type:Series, file:otwcode_otwarchive/app/models/series.rb, step:When ]
[name:syn_string, type:Tag, file:otwcode_otwarchive/app/models/tag.rb, step:null]
[name:syn_string, type:Tag, file:otwcode_otwarchive/app/models/tag.rb, step:When ]
[name:title, type:ArchiveFaq, file:otwcode_otwarchive/app/models/archive_faq.rb, step:null]
[name:title, type:KnownIssue, file:otwcode_otwarchive/app/models/known_issue.rb, step:null]
[name:title, type:AdminPost, file:otwcode_otwarchive/app/models/admin_post.rb, step:null]
[name:title, type:Work, file:otwcode_otwarchive/app/models/work.rb, step:null]
[name:title, type:Collection, file:otwcode_otwarchive/app/models/collection.rb, step:null]
[name:title, type:Series, file:otwcode_otwarchive/app/models/series.rb, step:null]
[name:title, type:Work, file:otwcode_otwarchive/app/models/work.rb, step:When ]
[name:title, type:ArchiveFaq, file:otwcode_otwarchive/app/models/archive_faq.rb, step:When ]
[name:title, type:KnownIssue, file:otwcode_otwarchive/app/models/known_issue.rb, step:When ]
[name:title, type:AdminPost, file:otwcode_otwarchive/app/models/admin_post.rb, step:When ]
[name:title, type:Collection, file:otwcode_otwarchive/app/models/collection.rb, step:When ]
[name:title, type:Series, file:otwcode_otwarchive/app/models/series.rb, step:When ]
[name:tos, type:HomeController, file:otwcode_otwarchive/app/controllers/home_controller.rb, step:null]
[name:tos, type:HomeController, file:otwcode_otwarchive/app/controllers/home_controller.rb, step:When ]
[name:tos_faq, type:HomeController, file:otwcode_otwarchive/app/controllers/home_controller.rb, step:null]
[name:tos_faq, type:HomeController, file:otwcode_otwarchive/app/controllers/home_controller.rb, step:When ]
[name:translated_post, type:Object, file:null, step:null]
[name:translated_post, type:Object, file:null, step:When ]
[name:url_for, type:Object, file:null, step:null]
[name:url_for, type:Object, file:null, step:When ]
[name:with_scope, type:WebSteps, file:otwcode_otwarchive/features/step_definitions/web_steps.rb, step:When ]
[name:with_scope, type:WebSteps, file:otwcode_otwarchive/features/step_definitions/web_steps.rb, step:Then ]
[name:within, type:Object, file:null, step:When ]
[name:work, type:Object, file:null, step:null]
[name:work, type:Object, file:null, step:When ]
[name:wrangle, type:TagWranglingsController, file:otwcode_otwarchive/app/controllers/tag_wranglings_controller.rb, step:null]
[name:wrangle, type:TagsController, file:otwcode_otwarchive/app/controllers/tags_controller.rb, step:null]
[name:wrangle, type:TagWranglingsController, file:otwcode_otwarchive/app/controllers/tag_wranglings_controller.rb, step:When ]
[name:wrangle, type:TagsController, file:otwcode_otwarchive/app/controllers/tags_controller.rb, step:When ]

Referenced pages: 127
otwcode_otwarchive/app/views/abuse_reports/new.html.erb
otwcode_otwarchive/app/views/admin/_admin_nav.html.erb
otwcode_otwarchive/app/views/admin/_admin_options.html.erb
otwcode_otwarchive/app/views/admin_posts/_admin_index.html.erb
otwcode_otwarchive/app/views/admin_posts/_admin_post_form.html.erb
otwcode_otwarchive/app/views/admin_posts/edit.html.erb
otwcode_otwarchive/app/views/admin_posts/index.html.erb
otwcode_otwarchive/app/views/admin_posts/new.html.erb
otwcode_otwarchive/app/views/admin_posts/show.html.erb
otwcode_otwarchive/app/views/archive_faqs/_admin_index.html.erb
otwcode_otwarchive/app/views/archive_faqs/_archive_faq_form.html.erb
otwcode_otwarchive/app/views/archive_faqs/_archive_faq_order.html.erb
otwcode_otwarchive/app/views/archive_faqs/edit.html.erb
otwcode_otwarchive/app/views/archive_faqs/index.html.erb
otwcode_otwarchive/app/views/archive_faqs/manage.html.erb
otwcode_otwarchive/app/views/archive_faqs/new.html.erb
otwcode_otwarchive/app/views/archive_faqs/show.html.erb
otwcode_otwarchive/app/views/bookmarks/_bookmark.html.erb
otwcode_otwarchive/app/views/bookmarks/_bookmark_form.html.erb
otwcode_otwarchive/app/views/bookmarks/_bookmark_full_info.html.erb
otwcode_otwarchive/app/views/bookmarks/_bookmark_owner_navi.html.erb
otwcode_otwarchive/app/views/bookmarks/_bookmark_user_info.html.erb
otwcode_otwarchive/app/views/bookmarks/_bookmarks.html.erb
otwcode_otwarchive/app/views/bookmarks/edit.html.erb
otwcode_otwarchive/app/views/bookmarks/index.html.erb
otwcode_otwarchive/app/views/bookmarks/show.html.erb
otwcode_otwarchive/app/views/challenge_claims/show.html.erb
otwcode_otwarchive/app/views/challenge_signups/_show_prompt.html.erb
otwcode_otwarchive/app/views/chapters/_chapter.html.erb
otwcode_otwarchive/app/views/chapters/_chapter_form.html.erb
otwcode_otwarchive/app/views/chapters/_chapter_management.html.erb
otwcode_otwarchive/app/views/chapters/manage.html.erb
otwcode_otwarchive/app/views/chapters/new.html.erb
otwcode_otwarchive/app/views/collection_items/_item_form.html.erb
otwcode_otwarchive/app/views/collection_items/index.html.erb
otwcode_otwarchive/app/views/collection_participants/_add_participants_form.html.erb
otwcode_otwarchive/app/views/collection_participants/_participant_form.html.erb
otwcode_otwarchive/app/views/collection_participants/index.html.erb
otwcode_otwarchive/app/views/collections/_bookmarks_module.html.erb
otwcode_otwarchive/app/views/collections/_collection_blurb.html.erb
otwcode_otwarchive/app/views/collections/_filters.html.erb
otwcode_otwarchive/app/views/collections/_form.html.erb
otwcode_otwarchive/app/views/collections/_header.html.erb
otwcode_otwarchive/app/views/collections/_works_module.html.erb
otwcode_otwarchive/app/views/collections/edit.html.erb
otwcode_otwarchive/app/views/collections/index.html.erb
otwcode_otwarchive/app/views/collections/list_challenges.html.erb
otwcode_otwarchive/app/views/collections/new.html.erb
otwcode_otwarchive/app/views/collections/show.html.erb
otwcode_otwarchive/app/views/comments/_comment_form.html.erb
otwcode_otwarchive/app/views/comments/_comment_thread.html.erb
otwcode_otwarchive/app/views/comments/_commentable.html.erb
otwcode_otwarchive/app/views/comments/_confirm_delete.html.erb
otwcode_otwarchive/app/views/comments/_single_comment.html.erb
otwcode_otwarchive/app/views/external_works/_external_work_blurb.html.erb
otwcode_otwarchive/app/views/external_works/new.html.erb
otwcode_otwarchive/app/views/fandoms/index.html.erb
otwcode_otwarchive/app/views/feedbacks/new.html.erb
otwcode_otwarchive/app/views/gifts/_gift_search.html.erb
otwcode_otwarchive/app/views/home/_tos.html.erb
otwcode_otwarchive/app/views/home/tos.html.erb
otwcode_otwarchive/app/views/home/tos_faq.html.erb
otwcode_otwarchive/app/views/known_issues/_admin_index.html.erb
otwcode_otwarchive/app/views/known_issues/_known_issues_form.html.erb
otwcode_otwarchive/app/views/known_issues/edit.html.erb
otwcode_otwarchive/app/views/known_issues/index.html.erb
otwcode_otwarchive/app/views/known_issues/new.html.erb
otwcode_otwarchive/app/views/kudos/_kudos.html.erb
otwcode_otwarchive/app/views/media/index.html.erb
otwcode_otwarchive/app/views/orphans/about.html.erb
otwcode_otwarchive/app/views/passwords/new.html.erb
otwcode_otwarchive/app/views/pseuds/_byline.html.erb
otwcode_otwarchive/app/views/pseuds/_pseud_blurb.html.erb
otwcode_otwarchive/app/views/pseuds/_pseuds_form.html.erb
otwcode_otwarchive/app/views/pseuds/index.html.erb
otwcode_otwarchive/app/views/pseuds/new.html.erb
otwcode_otwarchive/app/views/pseuds/show.html.erb
otwcode_otwarchive/app/views/series/_series_blurb.html.erb
otwcode_otwarchive/app/views/series/_series_order.html.erb
otwcode_otwarchive/app/views/series/edit.html.erb
otwcode_otwarchive/app/views/series/index.html.erb
otwcode_otwarchive/app/views/series/manage.html.erb
otwcode_otwarchive/app/views/skins/_form.html.erb
otwcode_otwarchive/app/views/skins/_skin_navigation.html.erb
otwcode_otwarchive/app/views/skins/_skin_style_block.html.erb
otwcode_otwarchive/app/views/skins/_skin_top_navigation.html.erb
otwcode_otwarchive/app/views/skins/_wizard_form.html.erb
otwcode_otwarchive/app/views/skins/edit.html.erb
otwcode_otwarchive/app/views/skins/index.html.erb
otwcode_otwarchive/app/views/skins/new.html.erb
otwcode_otwarchive/app/views/skins/new_wizard.html.erb
otwcode_otwarchive/app/views/tag_wranglings/index.html.erb
otwcode_otwarchive/app/views/tags/_search_form.html.erb
otwcode_otwarchive/app/views/tags/edit.html.erb
otwcode_otwarchive/app/views/tags/index.html.erb
otwcode_otwarchive/app/views/tags/new.html.erb
otwcode_otwarchive/app/views/tags/search.html.erb
otwcode_otwarchive/app/views/tags/show.html.erb
otwcode_otwarchive/app/views/tags/wrangle.html.erb
otwcode_otwarchive/app/views/user_sessions/_openid.html.erb
otwcode_otwarchive/app/views/user_sessions/_passwd.html.erb
otwcode_otwarchive/app/views/user_sessions/new.html.erb
otwcode_otwarchive/app/views/users/_contents.html.erb
otwcode_otwarchive/app/views/users/_header.html.erb
otwcode_otwarchive/app/views/users/_legal.html.erb
otwcode_otwarchive/app/views/users/_openid.html.erb
otwcode_otwarchive/app/views/users/_passwd.html.erb
otwcode_otwarchive/app/views/users/new.html.erb
otwcode_otwarchive/app/views/users/show.html.erb
otwcode_otwarchive/app/views/works/_edit_multiple_controls.html.erb
otwcode_otwarchive/app/views/works/_filters.html.erb
otwcode_otwarchive/app/views/works/_meta.html.erb
otwcode_otwarchive/app/views/works/_series_links.html.erb
otwcode_otwarchive/app/views/works/_standard_form.html.erb
otwcode_otwarchive/app/views/works/_work_blurb.html.erb
otwcode_otwarchive/app/views/works/_work_footer.html.erb
otwcode_otwarchive/app/views/works/_work_header.html.erb
otwcode_otwarchive/app/views/works/_work_tags_form.html.erb
otwcode_otwarchive/app/views/works/edit.html.erb
otwcode_otwarchive/app/views/works/edit_multiple.html.erb
otwcode_otwarchive/app/views/works/edit_tags.html.erb
otwcode_otwarchive/app/views/works/index.html.erb
otwcode_otwarchive/app/views/works/navigate.html.erb
otwcode_otwarchive/app/views/works/new.html.erb
otwcode_otwarchive/app/views/works/new_import.html.erb
otwcode_otwarchive/app/views/works/show.html.erb
otwcode_otwarchive/app/views/works/show_multiple.html.erb

