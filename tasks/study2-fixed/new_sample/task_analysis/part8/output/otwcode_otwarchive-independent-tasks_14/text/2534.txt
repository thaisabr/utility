Feature: Prompt Meme Challenge
In order to have an archive full of works
As a humble user
I want to create a prompt meme and post to it
Scenario: Add more requests button disappears correctly from signup show page
Given I have standard challenge tags setup
And I am logged in as "mod1"
When I set up a basic promptmeme "Battle 12"
And I follow "Challenge Settings"
When I fill in multi-prompt challenge options
When I sign up for Battle 12 with combination D
And I add prompt 3
Then I should see "Add another prompt"
When I add prompt 4
Then I should not see "Add another prompt"
Scenario: Add more requests button disappears correctly from signup show page
Given I have standard challenge tags setup
And I am logged in as "mod1"
When I set up a basic promptmeme "Battle 12"
And I follow "Challenge Settings"
When I fill in multi-prompt challenge options
When I sign up for Battle 12 with combination D
And I add prompt 3
Then I should see "Add another prompt"
When I add prompt 4
And I follow "Request 4"
Then I should not see "Add another prompt"
Scenario: Mod deletes a prompt that doesn't fit the challenge rules
Given I have Battle 12 prompt meme fully set up
When I am logged in as "myname1"
When I sign up for Battle 12 with combination C
When I am logged in as "mod1"
When I delete the prompt by "myname1"
Then I should see "Prompt was deleted"
And I should see "Prompts for Battle 12"
And I should not see "Signups for Battle 12"
Scenario: Signup can be deleted after response has been posted
Given I have Battle 12 prompt meme fully set up
When I am logged in as "myname1"
When I sign up for Battle 12 with combination B
And I am logged in as "myname4"
And I claim a prompt from "Battle 12"
When I fulfill my claim
When I am logged in as "myname1"
And I delete my signup for "Battle 12"
Then I should see "Challenge signup was deleted."
When I view the work "Fulfilled Story"
Then I should see "This work is part of an ongoing challenge and will be revealed soon! You can find details here: Battle 12"
And I should not see "Stargate Atlantis"
When I am logged in as "myname4"
And I view the work "Fulfilled Story"
Then I should see "This work is part of an ongoing challenge and will be revealed soon! You can find details here: Battle 12"
And I should see "Stargate Atlantis"
Scenario: Prompt can be removed after response has been posted
Given I have Battle 12 prompt meme fully set up
When I am logged in as "myname1"
When I sign up for Battle 12 with combination B
And I am logged in as "myname4"
And I claim a prompt from "Battle 12"
When I fulfill my claim
When I am logged in as "myname1"
And I delete my prompt in "Battle 12"
Then I should see "Prompt was deleted."
When I view the work "Fulfilled Story"
Then I should see "This work is part of an ongoing challenge and will be revealed soon! You can find details here: Battle 12"
And I should not see "Stargate Atlantis"
When I am logged in as "myname4"
And I view the work "Fulfilled Story"
Then I should see "This work is part of an ongoing challenge and will be revealed soon! You can find details here: Battle 12"
And I should see "Stargate Atlantis"
Scenario: When maintainer deletes signup, its prompts disappear from the collection
Given I have Battle 12 prompt meme fully set up
Given I have added a co-moderator "mod2" to collection "Battle 12"
When I am logged in as "myname1"
When I sign up for Battle 12 with combination A
When I am logged in as "mod2"
When I delete the signup by "myname1"
When I view prompts for "Battle 12"
Then I should not see "myname1"
Scenario: When maintainer deletes signup, as a prompter the signup disappears from my dashboard
Given I have Battle 12 prompt meme fully set up
Given I have added a co-moderator "mod2" to collection "Battle 12"
When I am logged in as "myname1"
When I sign up for Battle 12 with combination A
When I am logged in as "mod2"
When I delete the signup by "myname1"
When I am logged in as "myname1"
When I go to my signups page
Then I should see "My Signups (0)"
And I should not see "Battle 12"
Scenario: Delete a signup, claims should also be deleted
Given I have Battle 12 prompt meme fully set up
When I am logged in as "myname1"
When I sign up for Battle 12 with combination B
And I am logged in as "myname4"
And I claim a prompt from "Battle 12"
When I am logged in as "myname1"
And I delete my signup for "Battle 12"
Then I should see "Challenge signup was deleted."
When I am logged in as "myname4"
And I go to my claims page
Then I should see "My Claims (0)"
Scenario: Delete a prompt, claims should also be deleted
Given I have Battle 12 prompt meme fully set up
When I am logged in as "myname1"
When I sign up for Battle 12 with combination B
And I am logged in as "myname4"
And I claim a prompt from "Battle 12"
When I am logged in as "myname1"
And I delete my prompt in "Battle 12"
Then I should see "Prompt was deleted."
When I am logged in as "myname4"
And I go to my claims page
Then I should see "My Claims (0)"
Scenario: Fic shows what prompt it is fulfilling when mod views it
Given I have Battle 12 prompt meme fully set up
Given everyone has signed up for Battle 12
When I am logged in as "mod1"
When I claim a prompt from "Battle 12"
When I start to fulfill my claim
And I fill in "Work Title" with "Fulfilled Story-thing"
And I fill in "content" with "This is an exciting story about Atlantis, but in a different universe this time"
When I press "Preview"
And I press "Post"
When I view the work "Fulfilled Story-thing"
Then I should see "In response to a prompt by: myname4"
And I should see "Fandom: Stargate Atlantis"
And I should see "Anonymous" within ".byline"
And I should not see "mod1" within ".byline"
Scenario: check that completed ficlet is unrevealed
Given I have Battle 12 prompt meme fully set up
Given everyone has signed up for Battle 12
When mod fulfills claim
When I am logged in as "myname4"
When I view the work "Fulfilled Story-thing"
Then I should not see "In response to a prompt by: myname4"
And I should not see "Fandom: Stargate Atlantis"
And I should not see "Anonymous"
And I should not see "mod1"
And I should see "This work is part of an ongoing challenge and will be revealed soon! You can find details here: Battle 12"
Scenario: Story is anon when challenge is revealed
Given I have Battle 12 prompt meme fully set up
Given everyone has signed up for Battle 12
When I am logged in as "myname4"
When I claim a prompt from "Battle 12"
When I close signups for "Battle 12"
When I am logged in as "myname4"
When I fulfill my claim
When mod fulfills claim
When I reveal the "Battle 12" challenge
When I am logged in as "myname4"
When I view the work "Fulfilled Story-thing"
Then I should see "In response to a prompt by: myname4"
And I should see "Fandom: Stargate Atlantis"
And I should see "Collections: Battle 12"
And I should see "Anonymous" within ".byline"
And I should not see "mod1" within ".byline"
And I should see "Alternate Universe - Historical"
Feature: Create and Edit Series
In order to view a user's series
As a reader
The index needs to load properly, even for authors with more than ArchiveConfig.ITEMS_PER_PAGE series
Scenario: View user's series index
Given I am logged in as "whoever" with password "whatever"
And I add the work "grumble" to series "polarbears"
When I go to whoever's series page
Then I should see "whoever's Series"
And I should see "polarbears"
Scenario: Series index for maaany series
Given I am logged in as "whoever" with password "whatever"
And I add the work "grumble" to "32" series "penguins"
When I go to whoever's series page
Then I should see "penguins30"
When I follow "Next"
Then I should see "penguins0"
