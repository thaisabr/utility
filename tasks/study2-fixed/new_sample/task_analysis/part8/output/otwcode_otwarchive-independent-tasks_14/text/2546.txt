Feature: Edit preferences
In order to have an archive full of users
As a humble user
I want to fill out my preferences
Scenario: View and edit preferences - viewing history, personal details, view entire work
Given the following activated user exists
| login         | password   |
| editname      | password   |
When I go to editname's user page
And I follow "Profile"
Then I should not see "My email address"
And I should not see "My birthday"
When I am logged in as "editname" with password "password"
Then I should see "Hi, editname!"
And I should see "Log out"
When I post the work "This has two chapters"
And I follow "Add Chapter"
And I fill in "content" with "Secondy chapter"
And I press "Preview"
And I press "Post Chapter"
Then I should not see "Secondy chapter"
When I follow "editname"
Then I should see "My Dashboard"
And I should see "My History"
And I should see "My Preferences"
And I should see "Profile"
When I follow "My Preferences"
Then I should see "Update My Preferences"
And I should see "Edit My Profile"
And I should see "Orphan My Works"
When I follow "Edit My Profile"
Then I should see "Change Password"
When I follow "editname"
Then I should see "My Dashboard"
When I follow "Profile"
Then I should see "Set My Preferences"
When I follow "Set My Preferences"
Then I should see "Update My Preferences"
And I should see "Edit My Profile"
When I uncheck "Enable Viewing History"
And I check "Always view entire work by default"
And I check "Display Email Address"
And I check "Display Date of Birth"
And I press "Update"
Then I should see "Your preferences were successfully updated"
When I follow "editname"
Then I should see "My Dashboard"
And I should not see "My History"
When I go to the works page
And I follow "This has two chapters"
Then I should see "Secondy chapter"
When I follow "Log out"
And I go to editname's user page
And I follow "Profile"
Then I should see "My email address"
And I should see "My birthday"
When I go to the works page
And I follow "This has two chapters"
Then I should not see "Secondy chapter"
Feature: Edit profile
In order to have a presence on the archive
As a humble user
I want to fill out and edit my profile
Background:
Given the following activated user exists
| login    | password   | email  	   |
| editname | password   | bar@ao3.org  |
Scenario: Edit profile - changing email address requires reauthenticating
When I follow "Change Email"
And I fill in "New Email" with "blah"
And I fill in "Confirm New Email" with "blah"
And I press "Change Email"
Then I should see "You must enter your password"
And 0 emails should be delivered
Scenario: Edit profile - changing email address - entering an incorrect password
When I enter an incorrect password
Then I should see "Your password was incorrect"
And 0 emails should be delivered
