Feature: Prompt Meme Challenge
In order to have an archive full of works
As a humble user
I want to create a prompt meme and post to it
Scenario: Add more requests button disappears correctly from signup show page
Given I have standard challenge tags setup
And I am logged in as "mod1"
When I set up a basic promptmeme "Battle 12"
And I follow "Challenge Settings"
When I fill in multi-prompt challenge options
When I sign up for Battle 12 with combination D
And I add prompt 3
Then I should see "Add another prompt"
When I add prompt 4
Then I should not see "Add another prompt"
Scenario: Add more requests button disappears correctly from signup show page
Given I have standard challenge tags setup
And I am logged in as "mod1"
When I set up a basic promptmeme "Battle 12"
And I follow "Challenge Settings"
When I fill in multi-prompt challenge options
When I sign up for Battle 12 with combination D
And I add prompt 3
Then I should see "Add another prompt"
When I add prompt 4
And I follow "Request 4"
Then I should not see "Add another prompt"
Scenario: Claims count should be correct, shows fulfilled claims as well
Given I have Battle 12 prompt meme fully set up
When I am logged in as "myname1"
When I sign up for Battle 12 with combination B
And I am logged in as "myname4"
And I claim a prompt from "Battle 12"
When I fulfill my claim
When I am on my user page
Then I should see "My Claims (1)"
Scenario: All the rest of the unrefactored stuff
Given I have Battle 12 prompt meme fully set up
Given everyone has signed up for Battle 12
When I am logged in as "myname4"
When I claim a prompt from "Battle 12"
When I close signups for "Battle 12"
When I am logged in as "myname4"
When I fulfill my claim
When I am logged in as "mod1"
When I claim a prompt from "Battle 12"
When I am on my user page
Then I should see "My Claims (1)"
When I follow "My Claims"
Then I should see "Your Claims"
And I should not see "In Battle 12"
And I should see "Writing For" within "#claims_table"
And I should see "myname4" within "#claims_table"
When I follow "Post To Fulfill"
And I fill in "Work Title" with "Fulfilled Story-thing"
And I select "Not Rated" from "Rating"
And I check "No Archive Warnings Apply"
And I fill in "content" with "This is an exciting story about Atlantis, but in a different universe this time"
When I press "Preview"
And I press "Post"
Then I should see "Work was successfully posted"
When I view the work "Fulfilled Story-thing"
Then I should see "In response to a prompt by: myname4"
And I should see "Fandom: Stargate Atlantis"
And I should see "Anonymous" within ".byline"
And I should see "For myname4"
And I should not see "mod1" within ".byline"
And I should see "Alternate Universe - Historical"
When I am on my user page
Then I should see "My Claims (1)"
When I go to "Battle 12" collection's page
And I follow "Claims"
Then I should see "mod1" within "#fulfilled_claims"
And I should not see "mod1" within "#unfulfilled_claims"
When I follow "Prompts"
And I follow "Show Claims"
Then I should not see "Claimed by: myname4"
And I should not see "Claimed by: mod1"
And I should not see "Claimed by: (Anonymous)"
When I follow "Show Filled"
Then I should see "Claimed by: myname4"
And I should see "Claimed by: mod1"
And I should not see "Claimed by: (Anonymous)"
When I follow "Log out"
And I am logged in as "myname4"
When I go to "Battle 12" collection's page
And I follow "Prompts (8)"
And I follow "Show Claims"
And I follow "Show Filled"
Then I should not see "Claimed by: myname4"
And I should not see "Claimed by: mod1"
And I should see "Claimed by: (Anonymous)"
When I view the work "Fulfilled Story-thing"
Then I should not see "In response to a prompt by: myname4"
And I should not see "Fandom: Stargate Atlantis"
And I should not see "Anonymous"
And I should not see "mod1"
And I should not see "For myname4"
And I should not see "Alternate Universe - Historical"
And I should see "This work is part of an ongoing challenge and will be revealed soon! You can find details here: Battle 12"
When I am logged in as "mod1"
When I go to "Battle 12" collection's page
And I follow "Settings"
And I uncheck "Is this collection currently unrevealed?"
And I press "Submit"
Then I should see "Collection was successfully updated"
And 2 emails should be delivered
When I am logged in as "myname4"
When I view the work "Fulfilled Story-thing"
Then I should see "In response to a prompt by: myname4"
And I should see "Fandom: Stargate Atlantis"
And I should see "Collections: Battle 12"
And I should see "Anonymous" within ".byline"
And I should see "For myname4"
And I should not see "mod1" within ".byline"
And I should see "Alternate Universe - Historical"
When I am logged in as "mod1"
When I go to "Battle 12" collection's page
And I follow "Settings"
And I uncheck "Is this collection currently anonymous?"
And I press "Submit"
Then I should see "Collection was successfully updated"
Then 2 emails should be delivered
When I am logged in as "myname4"
When I go to "Battle 12" collection's page
And I follow "Prompts (8)"
And I follow "Show Claims"
Then I should not see "Claimed by: myname4"
And I should not see "Claimed by: mod1"
And I should not see "Claimed by: (Anonymous)"
When I follow "Show Filled"
Then I should see "Claimed by: myname4"
And I should see "Claimed by: mod1"
And I should not see "Claimed by: (Anonymous)"
When I go to "Battle 12" collection's page
And I follow "Prompts (8)"
When I press "Claim"
Then I should see "New claim made."
And I should not see "myname2"
And I should see "Claims (3)"
When I follow "Prompts"
Then I should not see "myname2" within "#main"
When I am on my user page
And I follow "My Claims"
Then I should not see "myname2"
When I follow "Anonymous"
Then I should not see "myname2"
And I should see "Anonymous"
When I follow "Log out"
And I am logged in as "myname4"
And I go to the collections page
And I follow "Battle 12"
And I follow "Claims"
Then I should see "mod1" within "#fulfilled_claims"
And I should see "myname4" within "#fulfilled_claims"
When I follow "Prompts ("
Then I should see "Claim"
When I press "Claim"
Then I should see "New claim made"
When I follow "Post New"
When I fill in the basic work information for "Existing work"
And I check "Battle 12 (Anonymous)"
And I press "Preview"
Then I should see "Draft was successfully created"
And I should see "In response to a prompt by: Anonymous"
And 2 emails should be delivered
When I view the work "Existing work"
Then I should find "draft"
When I go to "Battle 12" collection's page
And I follow "Claims"
Then I should see "myname4" within "#fulfilled_claims"
And I should see "Response posted on"
And I should see "Not yet approved"
When I follow "Response posted on"
Then I should see "Existing work"
And I should find "draft"
When I am on my user page
And I follow "My Drafts"
And all emails have been delivered
Then I should see "Existing work"
And "Issue 2259" is fixed
When I follow "Post Draft"
Then 1 email should be delivered
Then I should see "Your work was successfully posted"
And I should see "In response to a prompt by: Anonymous"
When I go to "Battle 12" collection's page
And I follow "Claims"
Then I should see "myname4" within "#fulfilled_claims"
And I should see "Response posted on"
When I follow "Response posted on"
Then I should see "Existing work"
And I should not find "draft"
When I am logged in as "myname1"
And I go to "Battle 12" collection's page
And I follow "Prompts ("
Then I should see "Claim"
When I press "Claim"
Then I should see "New claim made"
When I post the work "Here's one I made earlier"
And I edit the work "Here's one I made earlier"
And I check "Battle 12 (Anonymous)"
And I press "Preview"
Then I should find "draft"
And I should see "In response to a prompt by: Anonymous"
When I press "Update"
Then I should see "Work was successfully updated"
And I should not find "draft"
And I should see "In response to a prompt by: Anonymous"
When I go to "Battle 12" collection's page
And I follow "Claims"
Then I should see "myname1" within "#fulfilled_claims"
And I should see "Response posted on"
And I should see "Not yet approved"
