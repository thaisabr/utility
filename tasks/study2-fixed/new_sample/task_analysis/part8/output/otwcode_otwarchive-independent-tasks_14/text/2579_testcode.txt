Given /^I have standard challenge tags setup$/ do
  Given "I have no tags"
    And "basic tags"
    And %{a canonical fandom "Stargate Atlantis"}
    And %{a canonical fandom "Stargate SG-1"}
    And %{a canonical character "John Sheppard"}
    And %{a canonical freeform "Alternate Universe - Historical"}
    And %{a canonical freeform "Alternate Universe - High School"}
    And %{a canonical freeform "Something else weird"}
    And %{a canonical freeform "My extra tag"}
end
Given /^I have Battle 12 prompt meme fully set up$/ do
  Given %{I am logged in as "mod1"}
    And "I have standard challenge tags setup"
  When "I set up Battle 12 promptmeme collection"
  When "I fill in Battle 12 challenge options"
  When %{I follow "Log out"}
end
When /^I set up an?(?: ([^"]*)) promptmeme "([^\"]*)"(?: with name "([^"]*)")?$/ do |type, title, name|
  When %{I am logged in as "mod1"}
  visit new_collection_path
  if name.nil?
    fill_in("collection_name", :with => "promptcollection")
  else
    fill_in("collection_name", :with => name)
  end
  fill_in("collection_title", :with => title)
  if type == "anon"
    check("Is this collection currently unrevealed?")
    check("Is this collection currently anonymous?")
  end
  select("Prompt Meme", :from => "challenge_type")
  click_button("Submit")
  Then "I should see \"Collection was successfully created\""
  check("prompt_meme_signup_open")
  fill_in("prompt_meme_requests_num_allowed", :with => ArchiveConfig.PROMPT_MEME_PROMPTS_MAX)
  fill_in("prompt_meme_requests_num_required", :with => 1)
  fill_in("prompt_meme_request_restriction_attributes_fandom_num_required", :with => 1)
  fill_in("prompt_meme_request_restriction_attributes_fandom_num_allowed", :with => 2)
  click_button("Submit")
  Then "I should see \"Challenge was successfully created\""
end
When /^I fill in unlimited prompt challenge options$/ do
  When "I fill in prompt meme challenge options"
    And %{I check "prompt_meme_request_restriction_attributes_character_restrict_to_fandom"}
    And %{I fill in "prompt_meme_requests_num_allowed" with "50"}
    And %{I press "Submit"}
end
When /^I fill in multi-prompt challenge options$/ do
  When "I fill in prompt meme challenge options"
    And %{I fill in "prompt_meme_requests_num_allowed" with "4"}
    And %{I press "Submit"}
end
When /^I sign up for Battle 12 with combination A$/ do
  When %{I start signing up for Battle 12}
    And %{I check the 1st checkbox with the value "Stargate Atlantis"}
    And %{I check the 2nd checkbox with the value "Stargate Atlantis"}
    And %{I fill in the 1st field with id matching "freeform_tagnames" with "Alternate Universe - Historical"}
    click_button "Submit"
end
When /^I sign up for Battle 12 with combination B$/ do
  When %{I start signing up for Battle 12}
    And %{I check the 1st checkbox with the value "Stargate SG-1"}
    And %{I check the 2nd checkbox with the value "Stargate Atlantis"}
    And %{I check the 1st checkbox with id matching "anonymous"}
    And %{I check the 2nd checkbox with id matching "anonymous"}
    And %{I fill in the 1st field with id matching "freeform_tagnames" with "Alternate Universe - High School, Something else weird"}
    click_button "Submit"
end
When /^I sign up for Battle 12 with combination D$/ do
  When %{I start signing up for Battle 12}
    And %{I check the 1st checkbox with the value "Stargate Atlantis"}
    And %{I check the 2nd checkbox with the value "Stargate Atlantis"}
    click_button "Submit"
end
When /^I add prompt (\d+)$/ do |number|
  When %{I follow "Add another prompt"}
  Then %{I should see "Request #{number}"}
  When %{I check the 1st checkbox with the value "Stargate Atlantis"}
    And %{I press "Submit"}
  Then %{I should see "Signup was successfully updated"}
end
When /^I add prompt (\d+) with SG-1$/ do |number|
  When %{I follow "Add another prompt"}
  Then %{I should see "Request #{number}"}
  When %{I check the 1st checkbox with the value "Stargate SG-1"}
    And %{I press "Submit"}
  Then %{I should see "Signup was successfully updated"}
end
When /^I add 34 prompts$/ do
  @index = 3
  while @index < 35
    When "I add prompt #{@index}"
    @index = @index + 1
  end
end
When /^I add 24 prompts$/ do
  @index = 3
  while @index < 25
    When "I add prompt #{@index}"
    @index = @index + 1
  end
end
When /^I view prompts for "([^\"]*)"$/ do |title|
  visit collection_path(Collection.find_by_title(title))
  When %{I follow "Prompts ("}
end
Given /^I am logged in as "([^\"]*)" with password "([^\"]*)"$/ do |login, password|
  Given "I am logged out"
  user = User.find_by_login(login)
  if user.blank?
    user = Factory.create(:user, {:login => login, :password => password})
    user.activate
  else
    user.password = password
    user.password_confirmation = password
    user.save
  end
  visit login_path
  fill_in "User name", :with => login
  fill_in "Password", :with => password
  check "Remember me"
  click_button "Log in"
  assert UserSession.find
end
Given /^I am logged in as "([^\"]*)"$/ do |login|
  Given %{I am logged in as "#{login}" with password "#{DEFAULT_PASSWORD}"}
end
Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    click_button(button)
  end
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_content(text)
    else
      assert page.has_content?(text)
    end
  end
end
Then /^(?:|I )should not see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_no_content(text)
    else
      assert page.has_no_content?(text)
    end
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)
    case page_name

    when /the home\s?page/
      '/'

    # the following are examples using path_to_pickle

    when /^#{capture_model}(?:'s)? page$/                           # eg. the forum's page
      path_to_pickle $1

    when /^#{capture_model}(?:'s)? #{capture_model}(?:'s)? page$/   # eg. the forum's post's page
      path_to_pickle $1, $2

    when /^#{capture_model}(?:'s)? #{capture_model}'s (.+?) page$/  # eg. the forum's post's comments page
      path_to_pickle $1, $2, :extra => $3                           #  or the forum's post's edit page

    when /^#{capture_model}(?:'s)? (.+?) page$/                     # eg. the forum's posts page
      path_to_pickle $1, :extra => $2                               #  or the forum's edit page

    # Add more mappings here.

    when /^the login page$/i
      new_user_session_path
    when /^(.*)'s user page$/i
      user_path(:id => $1)
    when /^(.*)'s user url$/i
      user_url(:id => $1).sub("http://www.example.com", ArchiveConfig.APP_URL)
    when /^(.*)'s works page$/i
      user_works_path(:user_id => $1)
    when /^the "(.*)" work page/
      work_path(Work.find_by_title($1))
    when /^the work page with title (.*)/
      work_path(Work.find_by_title($1))
    when /^(.*)'s bookmarks page$/i
      user_bookmarks_path(:user_id => $1)
    when /^(.*)'s pseuds page$/i
      user_pseuds_path(:user_id => $1)
    when /^(.*)'s reading page$/i
      user_readings_path(:user_id => $1)
    when /^(.*)'s series page$/i
      user_series_index_path(:user_id => $1)
    when /^(.*)'s preferences page$/i
      user_preferences_path(:user_id => $1)
    when /^the subscriptions page for "(.*)"$/i
      user_subscriptions_path(:user_id => $1)
    when /my user page/
      user_path(User.current_user)
    when /my preferences page/
      user_preferences_path(User.current_user)
    when /my bookmarks page/
      user_bookmarks_path(User.current_user)
    when /my subscriptions page/
      user_subscriptions_path(User.current_user)      
    when /the import page/
      new_work_path(:import => 'true')
    when /the work-skins page/
      skins_path(:work_skins => true)
    when /^(.*)'s skin page/
      skins_path(:user_id => $1)
    when /^"(.*)" skin page/
      skin_path(Skin.find_by_title($1))
    when /^"(.*)" edit skin page/
      edit_skin_path(Skin.find_by_title($1))
    when /^"(.*)" collection's page$/i                      # e.g. when I go to "Collection name" collection's page
      collection_path(Collection.find_by_title($1))
    when /^"(.*)" collection's url$/i                      # e.g. when I go to "Collection name" collection's url
      collection_url(Collection.find_by_title($1)).sub("http://www.example.com", ArchiveConfig.APP_URL)
    when /^"(.*)" gift exchange edit page$/i
      edit_collection_gift_exchange_path(Collection.find_by_title($1))
    when /^"(.*)" collection's static page$/i
      static_collection_path(Collection.find_by_title($1))
    when /^the works tagged "(.*)"$/i
      tag_works_path(Tag.find_by_name($1))
    when /^the url for works tagged "(.*)"$/i
      tag_works_url(Tag.find_by_name($1)).sub("http://www.example.com", ArchiveConfig.APP_URL)
    when /^the works tagged "(.*)" in collection "(.*)"$/i
      collection_tag_works_path(Collection.find_by_title($2), Tag.find_by_name($1))
    when /^the url for works tagged "(.*)" in collection "(.*)"$/i
      collection_tag_works_url(Collection.find_by_title($2), Tag.find_by_name($1)).sub("http://www.example.com", ArchiveConfig.APP_URL)
    when /^the admin-posts page$/i
      admin_posts_path
    when /^the FAQ reorder page$/i
      manage_archive_faqs_path
    when/^the support page$/i
      new_feedback_report_path
      
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
When /^I sort by fandom$/ do
  When "I follow \"Sort by fandom\""
end
