Given /^I am logged in as "([^\"]*)"$/ do |login|
  Given %{I am logged in as "#{login}" with password "#{DEFAULT_PASSWORD}"}
end
When /^I create a skin to change the accent color$/ do
  visit new_skin_url
  When %{I follow "Use Wizard Instead?"}
    And %{I fill in "Title" with "Shiny"}
    And %{I fill in "Accent color" with "blue"}
    And %{I press "Create"}
  Then %{I should see "Skin was created successfully"}
  When %{I press "Use"}
end
Then /^I should see a different accent color on the dashboard and work meta$/ do
  Then %{I should see "#dashboard ul, #main dl.meta {background-color: blue;}" within "style"}
end
