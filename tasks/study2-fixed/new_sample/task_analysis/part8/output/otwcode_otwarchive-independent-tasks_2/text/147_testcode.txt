Given /^basic tags$/ do
  ratings = [ArchiveConfig.RATING_DEFAULT_TAG_NAME,
             ArchiveConfig.RATING_GENERAL_TAG_NAME,
             ArchiveConfig.RATING_TEEN_TAG_NAME,
             ArchiveConfig.RATING_MATURE_TAG_NAME,
             ArchiveConfig.RATING_EXPLICIT_TAG_NAME]
  ratings.each do |rating|
    Rating.find_or_create_by_name_and_canonical(rating, true)
  end
  Warning.find_or_create_by_name_and_canonical("No Archive Warnings Apply", true)
  Warning.find_or_create_by_name_and_canonical("Choose Not To Use Archive Warnings", true)
  Fandom.find_or_create_by_name_and_canonical("No Fandom", true)
  Category.find_or_create_by_name_and_canonical("Other", true)
  Category.find_or_create_by_name_and_canonical("F/F", true)
  Category.find_or_create_by_name_and_canonical("Multi", true)
  Category.find_or_create_by_name_and_canonical("M/F", true)
  Category.find_or_create_by_name_and_canonical("M/M", true)
end
Given /^I am logged in as an admin$/ do
  step("I am logged out")
  admin = Admin.find_by_login("testadmin")
  if admin.blank?
    admin = FactoryGirl.create(:admin, login: "testadmin", password: "testadmin", email: "testadmin@example.org")
  end
  visit admin_login_path
  fill_in "Admin user name", with: "testadmin"
  fill_in "Admin password", with: "testadmin"
  click_button "Log in as admin"
  step("I should see \"Successfully logged in\"")
end
Given /^basic languages$/ do
  Language.default
  german = Language.find_or_create_by_short_and_name_and_support_available("DE", "Deutsch", true)
  de = Locale.new
  de.iso = 'de'
  de.name = 'Deutsch'
  de.language_id = german.id
  de.save!
end
Given /^I am logged in as "([^\"]*)"$/ do |login|
  step(%{I am logged in as "#{login}" with password "#{DEFAULT_PASSWORD}"})
end
When /^I post (?:a|the) work "([^\"]*)"(?: with fandom "([^\"]*)")?(?: with freeform "([^\"]*)")?(?: with category "([^\"]*)")?(?: (?:in|to) (?:the )?collection "([^\"]*)")?(?: as a gift (?:for|to) "([^\"]*)")?$/ do |title, fandom, freeform, category, collection, recipient|  
  # If the work is already a draft then visit the preview page and post it
  work = Work.find_by_title(title)
  if work
    visit preview_work_url(work)
    click_button("Post")
  else
    # Note: this will match the above regexp and work just fine even if all the options are blank!
    step %{I set up the draft "#{title}" with fandom "#{fandom}" with freeform "#{freeform}" with category "#{category}" in collection "#{collection}" as a gift to "#{recipient}"}
    click_button("Post Without Preview")
  end
  Work.tire.index.refresh
end
When /^I view the work "([^\"]*)"(?: in (full|chapter-by-chapter) mode)?$/ do |work, mode|
  work = Work.find_by_title!(work)
  visit work_url(work)
  step %{I follow "Entire Work"} if mode == "full"
  step %{I follow "Chapter by Chapter"} if mode == "chapter-by-chapter"
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    click_button(button)
  end
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
When /^(?:|I )select "([^"]*)" from "([^"]*)"(?: within "([^"]*)")?$/ do |value, field, selector|
  with_scope(selector) do
    select(value, :from => field)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    page.should have_content(text)
  end
end
Then /^(?:|I )should not see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_no_content(text)
    else
      assert page.has_no_content?(text)
    end
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
Given /^I am logged in as "([^\"]*)" with password "([^\"]*)"$/ do |login, password|
  step("I am logged out")
  user = User.find_by_login(login)
  if user.blank?
    user = FactoryGirl.create(:user, {:login => login, :password => password})
    user.activate
  else
    user.password = password
    user.password_confirmation = password
    user.save
  end
  visit login_path
  fill_in "User name", :with => login
  fill_in "Password", :with => password
  check "Remember Me"
  click_button "Log In"
  assert UserSession.find
end
Given /^I am logged out$/ do
  visit logout_path
  assert !UserSession.find
  visit admin_logout_path
  assert !AdminSession.find
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    page.should have_content(text)
  end
end
When /^I set up (?:a|the) draft "([^\"]*)"(?: with fandom "([^\"]*)")?(?: with freeform "([^\"]*)")?(?: with category "([^\"]*)")?(?: (?:in|to|with) (?:the )?collection "([^\"]*)")?(?: as a gift (?:for|to) "([^\"]*)")?$/ do |title, fandom, freeform, category, collection, recipient|
  step %{basic tags}
  visit new_work_path
  step %{I fill in the basic work information for "#{title}"}
  check(category.blank? ? DEFAULT_CATEGORY : category)
  fill_in("Fandoms", with: (fandom.blank? ? DEFAULT_FANDOM : fandom))
  fill_in("Additional Tags", with: (freeform.blank? ? DEFAULT_FREEFORM : freeform))
  unless collection.blank?
    c = Collection.find_by_title(collection)
    fill_in("Collections", with: c.name)
  end
  fill_in("work_recipients", with: "#{recipient}") unless recipient.blank?
end
When /^I fill in the basic work information for "([^\"]*)"$/ do |title|
  step %{I fill in basic work tags}
  check(DEFAULT_WARNING)
  fill_in("Work Title", with: title)
  fill_in("content", with: DEFAULT_CONTENT)
end
When /^I fill in basic work tags$/ do
  select(DEFAULT_RATING, :from => "Rating")
  fill_in("Fandoms", :with => DEFAULT_FANDOM)
  fill_in("Additional Tags", :with => DEFAULT_FREEFORM)
end
