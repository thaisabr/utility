Feature: Create bookmarks
In order to have an archive full of bookmarks
As a humble user
I want to bookmark some works
Scenario: Adding bookmarks to closed collections (Issue 3083)
Given I am logged in as "moderator" with password "password"
And I have a closed collection "Unsolved Mysteries" with name "unsolved_mysteries"
And I have a closed collection "Rescue 911" with name "rescue_911"
And I am logged in as "moderator" with password "password"
And I post the work "Hooray for Homicide"
And I post the work "Sing a Song of Murder"
And I go to "Unsolved Mysteries" collection's page
Then I view the work "Hooray for Homicide"
And I follow "Bookmark"
And I fill in "bookmark_collection_names" with "unsolved_mysteries"
And I press "Create"
And I should see "Bookmark was successfully created"
And I follow "Edit"
And I fill in "bookmark_collection_names" with "rescue_911"
And I press "Update"
Then I should see "Bookmark was successfully updated"
Then I view the work "Sing a Song of Murder"
And I follow "Bookmark"
And I press "Create"
And I should see "Bookmark was successfully created"
And I follow "Add To Collection"
And I fill in "collection_names" with "unsolved_mysteries"
And I press "Add"
And I should see "Added to collection(s): Unsolved Mysteries"
When I follow "Edit"
And I fill in "bookmark_notes" with "This is my edited bookmark"
And I press "Update"
Then I should see "Bookmark was successfully updated."
And I am logged out
Then I am logged in as "RobertStack" with password "password"
And I view the work "Sing a Song of Murder"
And I follow "Bookmark"
And I fill in "bookmark_collection_names" with "rescue_911"
And I press "Create"
And I should see "Sorry! We couldn't save this bookmark because:"
And I should see "The collection rescue_911 is not currently open."
Then I view the work "Hooray for Homicide"
And I follow "Bookmark"
And I press "Create"
And I should see "Bookmark was successfully created"
And I follow "Add To Collection"
And I fill in "collection_names" with "rescue_911"
And I press "Add"
And I should see "We couldn't add your submission to the following collection(s): Rescue 911 is closed to new submissions."
And I follow "Edit"
And I fill in "bookmark_collection_names" with "rescue_911"
And I press "Update"
And I should see "We couldn't add your submission to the following collections: Rescue 911 is closed to new submissions."
And I am logged out
Then I open the collection with the title "Rescue 911"
And I am logged out
Then I am logged in as "Scott" with password "password"
And I view the work "Sing a Song of Murder"
And I follow "Bookmark"
And I fill in "bookmark_collection_names" with "rescue_911"
And I press "Create"
And I should see "Bookmark was successfully created"
And I am logged out
When I close the collection with the title "Rescue 911"
And I am logged in as "Scott" with password "password"
And I view the work "Sing a Song of Murder"
And I follow "Edit Bookmark"
And I fill in "bookmark_notes" with "This is a user editing a closed collection bookmark"
And I press "Edit"
Then I should see "Bookmark was successfully updated."
Feature: Collectible items
As a user
I want to add my items to collections
Scenario: Add my work to a moderated collection (Add to Collections button)
Given I am logged in as "moderator" with password "password"
Given I have a moderated collection "Various Penguins"
And I am logged in as "Scott" with password "password"
And I post the work "Blabla"
When I add my work to the collection
Then I should see "until it has been approved by a moderator."
When I go to "Various Penguins" collection's page
Then I should see "Works (0)"
And I should not see "Blabla"
Scenario: Add my bookmark to a moderated collection
Given I have a moderated collection "Various Penguins"
And I am logged in as a random user
And I have a bookmark for "Tundra penguins"
When I add my bookmark to the collection "Various_Penguins"
Then I should see "until it has been approved by a moderator."
When I go to "Various Penguins" collection's page
Then I should see "Bookmarks (0)"
And I should not see "Tundra penguins"
Feature: Collection
In order to have a collection full of curated works
As a collection maintainer
I want to add and invite works to my collection
Scenario: Invite a work to a collection where a user auto-approves inclusion
Given I am logged in as "Scott" with password "password"
And I go to Scott's preferences page
And I check "preference_automatically_approve_collections"
And I press "Update"
And I post the work "A Death in Hong Kong" with fandom "Murder She Wrote"
When I have the collection "scotts collection" with name "scotts_collection"
And I am logged in as "moderator" with password "password"
And I view the work "A Death in Hong Kong"
And I follow "Add To Collections"
And I fill in "collection_names" with "scotts_collection"
And I press "Add"
And I should see "Added to collection(s): scotts collection."
And 1 email should be delivered to "Scott"
And the email should contain "you have previously elected to allow"
When I am logged in as "moderator" with password "password"
And I go to "scotts collection" collection's page
And I follow "Manage Items"
And I follow "Approved"
Then I should see "A Death in Hong Kong"
Scenario: Invite a work to a collection where a users approves inclusion
Given I am logged in as "Scott" with password "password"
And I go to Scott's preferences page
And I uncheck "preference_automatically_approve_collections"
And I press "Update"
And I post the work "Murder in Milan" with fandom "Murder She Wrote"
When I have the collection "scotts collection" with name "scotts_collection"
And I am logged in as "moderator" with password "password"
And I view the work "Murder in Milan"
And I follow "Add To Collections"
And I fill in "collection_names" with "scotts_collection"
And I press "Add"
And I should see "This work has been Invited to your collection (scotts collection)."
And 1 email should be delivered to "Scott"
And the email should contain "If in future you would prefer to automatically approve requests to add your"
When I go to "scotts collection" collection's page
And I should see "Works (0)"
And I follow "Manage Items"
And I follow "Invited"
And I should see "Murder in Milan"
And I should see "Works listed here have been invited to this collection. Once a work's creator has approved inclusion in this collection, the work will be moved to 'Approved'."
When I am logged in as "Scott" with password "password"
And I accept the invitation for my work in the collection "scotts collection"
And I press "Submit"
And I should not see "Murder in Milan"
And I follow "Approved"
And I should see "Murder in Milan"
When I am logged in as "moderator" with password "password"
And I am on "scotts collection" collection's page
And I follow "Manage Items"
And I should not see "Murder in Milan"
And I follow "Approved"
Then I should see "Murder in Milan"
Feature: Edit preferences
In order to have an archive full of users
As a humble user
I want to fill out my preferences
Scenario: Ensure all Preference options are available
Given the following activated user exists
| login         | password   |
| scott         | password   |
When I am logged in as "scott" with password "password"
And I go to scott's user page
And I follow "Preferences"
Then I should see "Set My Preferences"
And I should see "Show my email address to other people."
And I should see "Show my date of birth to other people."
And I should see "Hide my work from search engines when possible."
And I should see "Hide the share buttons on my work."
And I should see "Show me adult content without checking."
And I should see "Show the whole work by default."
And I should see "Hide warnings (you can still choose to show them)."
And I should see "Hide additional tags (you can still choose to show them)."
And I should see "Hide work skins (you can still choose to show them)."
And I should see "Your site skin"
And I should see "Your time zone"
And I should see "Browser page title format"
And I should see "Don't show me any hit counts."
And I should see "Don't show me hits on my works."
And I should see "Don't show other people hits on my works."
And I should see "Turn off emails about comments."
And I should see "Turn off messages to your inbox about comments."
And I should see "Turn off copies of your own comments."
And I should see "Turn off emails about kudos."
And I should see "Turn off admin emails."
And I should see "Automatically agree to your work being collected by others in the Archive."
And I should see "Turn off emails from collections."
And I should see "Turn off inbox messages from collections."
And I should see "Turn off emails about gift works."
And I should see "Turn on Viewing History."
And I should see "Turn the new user help banner back on."
And I should see "Turn off the banner showing on every page."
