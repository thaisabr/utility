Feature: Import Works
In order to have an archive full of works
As an author
I want to create new works by importing them
Scenario: Importing a new multichapter work with backdating should have correct chapter index dates
Given basic tags
And the following activated user exists
| login          | password    |
| cosomeone      | something   |
And I am logged in as "cosomeone" with password "something"
And I set my time zone to "UTC"
When I go to the import page
And I fill in "urls" with
"""
http://rebecca2525.dreamwidth.org/3506.html
http://rebecca2525.dreamwidth.org/4024.html
"""
And I choose "import_multiple_chapters"
When I press "Import"
Then I should see "Preview"
When I press "Post"
Then I should see "Published:2000-01-10"
Then I should see "Completed:2000-01-22"
When I follow "Chapter Index"
Then I should see "1. Chapter 1 (2000-01-10)"
Then I should see "2. Importing Test Part 2 (2000-01-22)"
