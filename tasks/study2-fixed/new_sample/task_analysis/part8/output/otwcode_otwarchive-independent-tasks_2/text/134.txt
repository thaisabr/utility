Feature: Pseuds
Scenario: pseud creation and playing with the default pseud
Given I am logged in as "myself" with password "password"
And I go to myself's pseuds page
Then I should see "Default Pseud" within "div#main.pseuds-index"
When I follow "Edit"
Then I should see "You cannot change the pseud that matches your user name"
And the "pseud_is_default" checkbox should be checked
And the "pseud_is_default" checkbox should be disabled
When I follow "Back To Pseuds"
And I follow "New Pseud"
And I fill in "Name" with "Me"
And I check "pseud_is_default"
And I fill in "Description" with "Something's cute"
And I press "Create"
Then I should see "Pseud was successfully created."
When I follow "Edit Pseud"
Then I should see "Me"
And the "Make this name default" checkbox should not be disabled
And the "Make this name default" checkbox should be checked
When I follow "Back To Pseuds"
And I follow "edit_myself"
Then the "pseud_is_default" checkbox should not be checked
And the "pseud_is_default" checkbox should not be disabled
When I follow "Back To Pseuds"
And I follow "Me"
And I follow "Edit Pseud"
And I uncheck "Make this name default"
And I press "Update"
Then I should see "Pseud was successfully updated."
When I follow "Edit Pseud"
Then the "Make this name default" checkbox should not be checked
When I follow "Back To Pseuds"
And I follow "edit_myself"
Then the "pseud_is_default" checkbox should be checked
And the "pseud_is_default" checkbox should be disabled
Feature:
In order to correct mistakes or reflect my evolving personality
As a registered user
I should be able to change my user name
Scenario: Changing my user name with one pseud changes that pseud
Given I have no users
And I am logged in as "oldusername" with password "password"
When I visit the change username page for oldusername
And I fill in "New user name" with "newusername"
And I fill in "Password" with "password"
And I press "Change User Name"
Then I should get confirmation that I changed my username
And I should see "Hi, newusername"
When I go to my pseuds page
Then I should not see "oldusername"
When I follow "Edit"
Then I should see "You cannot change the pseud that matches your user name"
Then the "pseud_is_default" checkbox should be checked
And the "pseud_is_default" checkbox should be disabled
Scenario: Changing only the capitalization of my user name with one pseud changes that pseud's capitalization
Given I have no users
And I am logged in as "uppercrust" with password "password"
When I visit the change username page for uppercrust
And I fill in "New user name" with "Uppercrust"
And I fill in "Password" with "password"
And I press "Change User Name"
Then I should get confirmation that I changed my username
And I should see "Hi, Uppercrust"
When I go to my pseuds page
Then I should not see "uppercrust"
When I follow "Edit"
Then I should see "You cannot change the pseud that matches your user name"
Then the "pseud_is_default" checkbox should be checked
And the "pseud_is_default" checkbox should be disabled
