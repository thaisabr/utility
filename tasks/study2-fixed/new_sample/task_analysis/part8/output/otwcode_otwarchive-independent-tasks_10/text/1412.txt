Feature: Create and Edit Series
In order to view series created by a user
As a reader
The index needs to load properly, even for authors with more than ArchiveConfig.ITEMS_PER_PAGE series
Scenario: Three ways to add a work to a series
Given the following activated user exists
| login         | password   |
| author        | password   |
And a warning exists with name: "Choose Not To Use Archive Warnings", canonical: true
When I am logged in as "author" with password "password"
And I go to the new work page
And I select "Not Rated" from "Rating"
And I check "Choose Not To Use Archive Warnings"
And I fill in "Fandoms" with "My Little Pony"
And I check "series-options-show"
And I fill in "work_series_attributes_title" with "Ponies"
And I fill in "Work Title" with "Sweetie Belle"
And I fill in "content" with "First little pony is all alone"
And I press "Preview"
Then I should see "Part 1 of the Ponies series"
And I should see "Draft was successfully created"
When I press "Post"
Then I should see "Work was successfully posted"
And I should see "Part 1 of the Ponies series" within "div#series"
And I should see "Part 1 of the Ponies series" within "dd.series"
When I view the series "Ponies"
Then I should see "Sweetie Belle"
When I go to the new work page
And I select "Not Rated" from "Rating"
And I check "Choose Not To Use Archive Warnings"
And I fill in "Fandoms" with "My Little Pony"
And I select "Ponies" from "work_series_attributes_id"
And I fill in "Work Title" with "Starsong"
And I fill in "content" with "Second little pony want to make friends"
And I press "Preview"
Then I should see "Part 2 of the Ponies series"
When I press "Post"
And I view the series "Ponies"
Then I should see "Sweetie Belle"
And I should see "Starsong"
When I go to the new work page
And I select "Not Rated" from "Rating"
And I check "Choose Not To Use Archive Warnings"
And I fill in "Fandoms" with "My Little Pony"
And I fill in "Work Title" with "Rainbow Dash"
And I fill in "content" with "Third little pony is a little shy"
And I press "Preview"
And I press "Post"
When I view the series "Ponies"
Then I should not see "Rainbow Dash"
When I edit the work "Rainbow Dash"
And I select "Ponies" from "work_series_attributes_id"
And I press "Preview"
And I press "Update"
And I view the series "Ponies"
And I follow "Rainbow Dash"
Then I should see "Part 3 of the Ponies series"
When I follow "«"
Then I should see "Starsong"
When I follow "«"
Then I should see "Sweetie Belle"
When I follow "»"
Then I should see "Starsong"
When I view the series "Ponies"
And I follow "Edit"
And I fill in "Series Description" with "This is a series about ponies. Of course"
And I fill in "Series Notes" with "I wrote this under the influence of coffee! And pink chocolate."
And I press "Update"
Then I should see "Series was successfully updated."
And I should see "This is a series about ponies. Of course" within "blockquote.userstuff"
And I should see "I wrote this under the influence of coffee! And pink chocolate." within "dl.series"
And I should see "Complete: No"
When I follow "Edit"
And I check "series_complete"
And I press "Update"
Then I should see "Complete: Yes"
When I edit the work "Rainbow Dash"
Then the "series-options-show" checkbox should be checked
And I should see "Ponies" within "fieldset#series-options"
When I fill in "work_series_attributes_title" with "Black Beauty"
And I press "Preview"
Then I should see "Part 3 of the Ponies series" within "dd.series"
When I press "Update"
Then I should see "Part 1 of the Black Beauty series" within "dd.series"
And I should see "Part 3 of the Ponies series" within "dd.series"
And I should see "Part 1 of the Black Beauty series" within "div#series"
And I should see "Part 3 of the Ponies series" within "div#series"
Scenario: Three ways to add a work to a series: a user with more than one pseud
Given the following activated user exists
| login         | password   |
| author        | password   |
And a warning exists with name: "Choose Not To Use Archive Warnings", canonical: true
When I am logged in as "author" with password "password"
And "author" creates the default pseud "Pointless Pseud"
When I go to the new work page
And I select "Not Rated" from "Rating"
And I check "Choose Not To Use Archive Warnings"
And I fill in "Fandoms" with "My Little Pony"
And I select "Pointless Pseud" from "work_author_attributes_ids_"
And I check "series-options-show"
And I fill in "work_series_attributes_title" with "Ponies"
And I fill in "Work Title" with "Sweetie Belle"
And I fill in "content" with "First little pony is all alone"
And I press "Preview"
Then I should see "Part 1 of the Ponies series"
And I should see "Draft was successfully created"
When I press "Post"
Then I should see "Work was successfully posted"
And I should see "Pointless Pseud"
And I should see "Part 1 of the Ponies series" within "div#series"
And I should see "Part 1 of the Ponies series" within "dd.series"
When I view the series "Ponies"
Then I should see "Sweetie Belle"
When I go to the new work page
And I select "Not Rated" from "Rating"
And I check "Choose Not To Use Archive Warnings"
And I fill in "Fandoms" with "My Little Pony"
And I select "Ponies" from "work_series_attributes_id"
And I fill in "Work Title" with "Starsong"
And I fill in "content" with "Second little pony want to make friends"
And I press "Preview"
Then I should see "Part 2 of the Ponies series"
When I press "Post"
And I view the series "Ponies"
Then I should see "Sweetie Belle"
And I should see "Starsong"
When I go to the new work page
And I select "Not Rated" from "Rating"
And I check "Choose Not To Use Archive Warnings"
And I fill in "Fandoms" with "My Little Pony"
And I fill in "Work Title" with "Rainbow Dash"
And I fill in "content" with "Third little pony is a little shy"
And I press "Preview"
And I press "Post"
When I view the series "Ponies"
Then I should not see "Rainbow Dash"
When I edit the work "Rainbow Dash"
And I select "Ponies" from "work_series_attributes_id"
And I press "Preview"
And I press "Update"
And I view the series "Ponies"
And I follow "Rainbow Dash"
Then I should see "Part 3 of the Ponies series"
When I follow "«"
Then I should see "Starsong"
When I follow "«"
Then I should see "Sweetie Belle"
When I follow "»"
Then I should see "Starsong"
When I view the series "Ponies"
And I follow "Edit"
And I fill in "Series Description" with "This is a series about ponies. Of course"
And I fill in "Series Notes" with "I wrote this under the influence of coffee! And pink chocolate."
And I press "Update"
Then I should see "Series was successfully updated."
And I should see "This is a series about ponies. Of course" within "blockquote.userstuff"
And I should see "I wrote this under the influence of coffee! And pink chocolate." within "dl.series"
And I should see "Complete: No"
When I follow "Edit"
And I check "series_complete"
And I press "Update"
Then I should see "Complete: Yes"
When I edit the work "Rainbow Dash"
Then the "series-options-show" checkbox should be checked
And I should see "Ponies" within "fieldset#series-options"
When I fill in "work_series_attributes_title" with "Black Beauty"
And I press "Preview"
Then I should see "Part 3 of the Ponies series" within "dd.series"
When I press "Update"
Then I should see "Part 1 of the Black Beauty series" within "dd.series"
And I should see "Part 3 of the Ponies series" within "dd.series"
And I should see "Part 1 of the Black Beauty series" within "div#series"
And I should see "Part 3 of the Ponies series" within "div#series"
Scenario: Rename a series
Given I am logged in as a random user
When I add the work "WALL-E" to series "Robots"
Then I should see "Part 1 of the Robots series" within "div#series"
And I should see "Part 1 of the Robots series" within "dd.series"
When I view the series "Robots"
And I follow "Edit"
And I fill in "Series Title" with "Many a Robot"
And I press "Update"
Then I should see "Series was successfully updated."
And I should see "Many a Robot"
When I view the work "WALL-E"
Then I should see "Part 1 of the Many a Robot series" within "div#series"
Feature: Leave kudos
In order to show appreciation
As a reader
I want to leave kudos
Background:
Given the following activated users exist
| login          | email           |
| myname1        | myname1@foo.com |
| myname2        | myname2@foo.com |
| myname3        | myname3@foo.com |
Scenario: post kudos
Given I am logged in as "myname2"
And all emails have been delivered
And I view the work "Awesome Story"
Then I should not see "left kudos on this work"
When I press "Kudos ♥"
Then I should see "myname2 left kudos on this work!"
And 0 emails should be delivered
When kudos are sent
Then 1 email should be delivered to "myname1@foo.com"
And the email should contain "myname2"
And the email should contain "left kudos"
And the email should contain "."
And all emails have been delivered
When I press "Kudos ♥"
Then I should see "You have already left kudos here. :)"
And I should not see "myname2 and myname2 left kudos on this work!"
When I log out
And I press "Kudos ♥"
Then I should see "Thank you for leaving kudos!"
And I should see "myname2 as well as a guest left kudos on this work!"
When I press "Kudos ♥"
Then I should see "You have already left kudos here. :)"
When kudos are sent
Then 1 email should be delivered to "myname1@foo.com"
And the email should contain "A guest"
And the email should contain "left kudos"
And the email should contain "."
When I am logged in as "myname3"
And I view the work "Awesome Story"
And I press "Kudos ♥"
Then I should see "myname3 and myname2 as well as a guest left kudos on this work!"
When I am logged in as "myname1"
And I view the work "Awesome Story"
Then I should not see "Kudos ♥"
Scenario: kudos on a multi-chapter work
Given I am logged in as "myname1"
And I post the chaptered work "Epic Saga"
And I follow "Add Chapter"
And I fill in "content" with "third chapter is a draft"
And I press "Preview"
When I am logged in as "myname3"
And I view the work "Epic Saga"
And I press "Kudos ♥"
Then I should see kudos on every chapter
When I am logged in as "myname1"
And I view the work "Epic Saga"
Then I should see kudos on every chapter but the draft
Scenario: deleting pseud and user after creating kudos should orphan them
Given I am logged in as "myname3"
When "myname3" creates the default pseud "foobar"
And I view the work "Awesome Story"
And I press "Kudos ♥"
Then I should see "foobar (myname3) left kudos on this work!"
When "myname3" creates the default pseud "barfoo"
And I am on myname3's pseuds page
And I follow "delete_foobar"
And I view the work "Awesome Story"
When "myname3" deletes their account
And I view the work "Awesome Story"
And "issue 2198" is fixed
Scenario: batched kudos email
Given I am logged in as "myname1"
And I post the work "Another Awesome Story"
And all emails have been delivered
And the kudos queue is cleared
And I am logged in as "myname2"
And I leave kudos on "Awesome Story"
And I leave kudos on "Another Awesome Story"
And I am logged in as "someone_else"
And I leave kudos on "Awesome Story"
And I leave kudos on "Another Awesome Story"
And I am logged out
And I leave kudos on "Awesome Story"
And I leave kudos on "Another Awesome Story"
When kudos are sent
Then 1 email should be delivered to "myname1@foo.com"
And the email should contain "myname2"
And the email should contain "someone_else"
And the email should contain "guest"
And the email should contain "Awesome Story"
And the email should contain "Another Awesome Story"
Feature: Collectible items
As a user
I want to add my items to collections
Scenario: Add my bookmark to a collection
Given I have a collection "Various Penguins"
And I am logged in as a random user
And I have a bookmark for "Tundra penguins"
When I add my bookmark to the collection
Then I should see "Added"
When I go to "Various Penguins" collection's page
Then I should see "Bookmarks (1)" within "#dashboard"
And I should see "Tundra penguins"
Feature: Private bookmarks
In order to have an archive full of bookmarks
As a humble user
I want to bookmark some works privately
Scenario: private bookmarks on public and restricted works
Given the following activated users exist
| login           | password   |
| workauthor      | password   |
| bookmarker      | password   |
| otheruser       | password   |
And a fandom exists with name: "Stargate SG-1", canonical: true
And I am logged in as "workauthor" with password "password"
And I post the locked work "Secret Masterpiece"
And I post the work "Public Masterpiece"
When I log out
And I am logged in as "bookmarker" with password "password"
And I view the work "Secret Masterpiece"
And I follow "Bookmark"
And I check "bookmark_rec"
And I check "bookmark_private"
And I press "Create"
Then I should see "Bookmark was successfully created"
And I should see the "title" text "Restricted"
And I should not see "Rec"
And I should see "Private Bookmark"
And I should see "0"
When I view the work "Public Masterpiece"
And I follow "Bookmark"
And I check "bookmark_rec"
And I check "bookmark_private"
And I press "Create"
Then I should see "Bookmark was successfully created"
And I should not see the "title" text "Restricted"
And I should not see "Rec"
And I should see "Private Bookmark"
And I should see "0"
When I go to the bookmarks page
Then I should not see "Secret Masterpiece"
And I should not see "Public Masterpiece"
When I am on bookmarker's bookmarks page
And I should see "2 Bookmarks by bookmarker"
Then I should see "Public Masterpiece"
And I should see "Secret Masterpiece"
When I log out
And I go to the bookmarks page
Then I should not see "Secret Masterpiece"
And I should not see "Public Masterpiece"
And I should not see "bookmarker"
When I go to bookmarker's bookmarks page
Then I should not see "Secret Masterpiece"
And I should not see "Public Masterpiece"
When I view the work "Public Masterpiece"
Then I should not see "Bookmarks:"
And I should not see "Bookmarks:1"
When I am logged in as "otheruser" with password "password"
And I go to the bookmarks page
Then I should not see "Secret Masterpiece"
And I should not see "Public Masterpiece"
When I go to bookmarker's bookmarks page
Then I should not see "Secret Masterpiece"
And I should not see "Public Masterpiece"
When I go to the works page
Then I should see "Public Masterpiece"
And I should not see "Secret Masterpiece"
And I should not see "Bookmarks:"
And I should not see "Bookmarks: 1"
When I log out
And I am logged in as "workauthor" with password "password"
And I go to the bookmarks page
Then I should not see "Secret Masterpiece"
And I should not see "Public Masterpiece"
When I go to bookmarker's bookmarks page
Then I should not see "Secret Masterpiece"
And I should not see "Public Masterpiece"
When I am logged in as "otheruser" with password "password"
And I view the work "Public Masterpiece"
And I follow "Bookmark"
And I check "bookmark_rec"
And I press "Create"
Then I should see "Bookmark was successfully created"
When I log out
And I go to the bookmarks page
Then I should not see "Secret Masterpiece"
And I should see "Public Masterpiece"
And I should not see "bookmarker"
And I should see "otheruser"
When I go to bookmarker's bookmarks page
Then I should not see "Secret Masterpiece"
And I should not see "Public Masterpiece"
When I go to the works page
Then I should not see "Secret Masterpiece"
And I should see "Public Masterpiece"
And I should not see "Bookmarks: 2"
When I view the work "Public Masterpiece"
Then I should not see "Bookmarks:2"
And I should see "Bookmarks:1"
When I follow "1"
Then I should see "List of Bookmarks"
And I should see "Public Masterpiece"
And I should see "otheruser"
And I should not see "bookmarker"
Feature:
In order to correct mistakes or reflect my evolving personality
As a registered user
I should be able to delete my account
Scenario: delete a user with a collection
Given I have an orphan account
When I am logged in as "moderator" with password "password"
And all emails have been delivered
And I create the collection "fake"
And I go to the collections page
Then I should see "fake"
And I should see "moderator" within "#main"
When I go to moderator's user page
And I follow "Profile"
Then I should see "Delete My Account"
When I follow "Delete My Account"
Then I should see "You have 1 collection(s) under the following pseuds: moderator."
When I choose "Change my pseud to 'orphan' and attach to the orphan account"
And I press "Save"
Then I should see "You have successfully deleted your account."
And 0 emails should be delivered
And I should not see "Log Out"
And I should see "Log In"
When I go to the collections page
Then I should see "fake"
Feature: Edit Works
In order to have an archive full of works
As an author
I want to edit existing works
Scenario: You can't edit a work unless you're logged in and it's your work
Given I have loaded the fixtures
When I view the work "First work"
Then I should not see "Edit"
Given I am logged in as "testuser" with password "testuser"
And all search indexes are updated
When I view the work "fourth"
Then I should not see "Edit"
When I am on testuser's works page
Then I should see "Edit"
When I follow "First work"
Then I should see "first fandom"
And I should see "Edit"
And I should not see "new tag"
When I follow "Edit"
Then I should see "Edit Work"
When I fill in "work_freeform" with "new tag"
And I fill in "content" with "first chapter content"
And I press "Preview"
Then I should see "Preview"
And I should see "Fandom: first fandom"
And I should see "first chapter content"
When I press "Update"
Then I should see "Work was successfully updated."
And I should see "Additional Tags: new tag"
When all search indexes are updated
And I go to testuser's works page
Then I should see "First work"
And I should see "first fandom"
And I should see "new tag"
When I edit the work "First work"
And I follow "Add Chapter"
And I fill in "content" with "second chapter content"
And I press "Preview"
Then I should see "This is a draft showing what this chapter will look like when it's posted to the Archive."
And I should see "second chapter content"
When I press "Post"
Then I should see "Chapter was successfully posted."
And I should not see "first chapter content"
And I should see "second chapter content"
When I edit the work "First work"
Then I should not see "chapter content"
When I follow "1"
And I fill in "content" with "first chapter new content"
And I press "Preview"
Then I should see "first chapter new content"
When I press "Update"
Then I should see "Chapter was successfully updated."
And I should see "first chapter new content"
And I should not see "second chapter content"
When I edit the work "First Work"
And I follow "2"
And I fill in "content" with "second chapter new content"
And I press "Preview"
And I press "Cancel"
Then I should see "second chapter content"
When I go to testuser's works page
And I follow "Edit"
And I select "testy" from "work_author_attributes_ids_"
And I unselect "testuser" from "work_author_attributes_ids_"
And I press "Post Without Preview"
Then I should see "testy"
And I should not see "testuser,"
