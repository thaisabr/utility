Given(/^#{capture_model} exists?(?: with #{capture_fields})?$/) do |name, fields|
  create_model(name, fields)
end
Given /^I am logged in as "([^\"]*)" with password "([^\"]*)"$/ do |login, password|
  step("I am logged out")
  user = User.find_by_login(login)
  if user.blank?
    user = FactoryGirl.create(:user, {:login => login, :password => password})
    user.activate
  else
    user.password = password
    user.password_confirmation = password
    user.save
  end
  visit login_path
  fill_in "User name", :with => login
  fill_in "Password", :with => password
  check "Remember Me"
  click_button "Log In"
  assert UserSession.find
end
Given /^I am logged in as "([^\"]*)"$/ do |login|
  step(%{I am logged in as "#{login}" with password "#{DEFAULT_PASSWORD}"})
end
Given /^I log out$/ do
  step(%{I follow "Log Out"})
end
Given /^a synonym "([^\"]*)" of the tag "([^\"]*)"$/ do |synonym, merger|
  merger = Tag.find_by_name(merger)
  merger_type = merger.type

  synonym = merger_type.classify.constantize.find_or_create_by_name(synonym)
  synonym.merger = merger
  synonym.save
end
Given /^the tag wrangler "([^\"]*)" with password "([^\"]*)" is wrangler of "([^\"]*)"$/ do |user, password, fandomname|
  tw = User.find_by_login(user)
  if tw.blank?
    tw = FactoryGirl.create(:user, {:login => user, :password => password})
    tw.activate
  else
    tw.password = password
    tw.password_confirmation = password
    tw.save
  end
  tw.tag_wrangler = '1'
  visit logout_path
  assert !UserSession.find
  visit login_path
  fill_in "User name", :with => user
  fill_in "Password", :with => password
  check "Remember Me"
  click_button "Log In"
  assert UserSession.find
  fandom = Fandom.find_or_create_by_name_and_canonical(fandomname, true)
  visit tag_wranglers_url
  fill_in "tag_fandom_string", :with => fandomname
  click_button "Assign"
end
When /^I view the tag "([^\"]*)"$/ do |tag|
  tag = Tag.find_by_name!(tag)
  visit tag_url(tag)
end
When /^I post the comment "([^"]*)" on the tag "([^"]*)"$/ do |comment_text, tag|
  step "I set up the comment \"#{comment_text}\" on the tag \"#{tag}\""
  click_button("Comment")
end
When /^I view tag wrangling discussions$/ do
  step %{I follow "Tag Wrangling"}
  step %{I follow "Discussion"}
end
When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    click_button(button)
  end
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
When /^(?:|I )fill in "([^"]*)" with "([^"]*)"(?: within "([^"]*)")?$/ do |field, value, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
When /^(?:|I )check "([^"]*)"(?: within "([^"]*)")?$/ do |field, selector|
  with_scope(selector) do
    check(field)
  end
end
When /^(?:|I )uncheck "([^"]*)"(?: within "([^"]*)")?$/ do |field, selector|
  with_scope(selector) do
    uncheck(field)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_content(text)
    else
      assert page.has_content?(text)
    end
  end
end
Then /^(?:|I )should not see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_no_content(text)
    else
      assert page.has_no_content?(text)
    end
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)
    case page_name

    when /the home\s?page/
      '/'
    when /^the search bookmarks page$/i
      Bookmark.tire.index.refresh
      search_bookmarks_path
    when /^the search tags page$/i
      Tag.tire.index.refresh
      search_tags_path
    when /^the search works page$/i
      Work.tire.index.refresh
      search_works_path      
    when /^the search people page$/i
      Pseud.tire.index.refresh
      search_people_path

    # the following are examples using path_to_pickle

    when /^#{capture_model}(?:'s)? page$/                           # eg. the forum's page
      path_to_pickle $1

    when /^#{capture_model}(?:'s)? #{capture_model}(?:'s)? page$/   # eg. the forum's post's page
      path_to_pickle $1, $2

    when /^#{capture_model}(?:'s)? #{capture_model}'s (.+?) page$/  # eg. the forum's post's comments page
      path_to_pickle $1, $2, :extra => $3                           #  or the forum's post's edit page

    when /^#{capture_model}(?:'s)? (.+?) page$/                     # eg. the forum's posts page
      path_to_pickle $1, :extra => $2                               #  or the forum's edit page

    # Add more mappings here.

    when /^the tagsets page$/i
      tag_sets_path
    when /^the login page$/i
      new_user_session_path
    when /^(.*)'s user page$/i
      user_path(:id => $1)
    when /^(.*)'s user url$/i
      user_url(:id => $1).sub("http://www.example.com", ArchiveConfig.APP_URL)
    when /^(.*)'s works page$/i
      Work.tire.index.refresh
      user_works_path(:user_id => $1)
    when /^the "(.*)" work page/
      work_path(Work.find_by_title($1))
    when /^the work page with title (.*)/
      work_path(Work.find_by_title($1))
    when /^(.*)'s bookmarks page$/i
      Bookmark.tire.index.refresh
      user_bookmarks_path(:user_id => $1)
    when /^(.*)'s pseuds page$/i
      user_pseuds_path(:user_id => $1)
    when /^(.*)'s reading page$/i
      user_readings_path(:user_id => $1)
    when /^(.*)'s series page$/i
      user_series_index_path(:user_id => $1)
    when /^(.*)'s preferences page$/i
      user_preferences_path(:user_id => $1)
    when /^the subscriptions page for "(.*)"$/i
      user_subscriptions_path(:user_id => $1)
    when /^(.*)'s profile page$/i
      user_profile_path(:user_id => $1)
    when /my pseuds page/
      user_pseuds_path(User.current_user)
    when /my user page/
      user_path(User.current_user)
    when /my preferences page/
      user_preferences_path(User.current_user)
    when /my bookmarks page/
      Bookmark.tire.index.refresh
      user_bookmarks_path(User.current_user)
    when /my works page/
      Work.tire.index.refresh
      user_works_path(User.current_user)
    when /my subscriptions page/
      user_subscriptions_path(User.current_user)      
    when /my profile page/
      user_profile_path(User.current_user)
    when /my claims page/
      user_claims_path(User.current_user)
    when /my signups page/
      user_signups_path(User.current_user)
    when /the import page/
      new_work_path(:import => 'true')
    when /the work-skins page/
      skins_path(:work_skins => true)
    when /^(.*)'s skin page/
      skins_path(:user_id => $1)
    when /^"(.*)" skin page/
      skin_path(Skin.find_by_title($1))
    when /^"(.*)" edit skin page/
      edit_skin_path(Skin.find_by_title($1))
    when /^"(.*)" collection's page$/i                      # e.g. when I go to "Collection name" collection's page
      collection_path(Collection.find_by_title($1))
    when /^the "(.*)" signups page$/i                      # e.g. when I go to "Collection name" signup page
      collection_signups_path(Collection.find_by_title($1))
    when /^the "(.*)" requests page$/i                      # e.g. when I go to "Collection name" signup page
      collection_requests_path(Collection.find_by_title($1))
    when /^"(.*)" collection's url$/i                      # e.g. when I go to "Collection name" collection's url
      collection_url(Collection.find_by_title($1)).sub("http://www.example.com", ArchiveConfig.APP_URL)
    when /^"(.*)" gift exchange edit page$/i
      edit_collection_gift_exchange_path(Collection.find_by_title($1))
    when /^"(.*)" collection's static page$/i
      static_collection_path(Collection.find_by_title($1))
    when /^the works tagged "(.*)"$/i
      Work.tire.index.refresh
      tag_works_path(Tag.find_by_name($1))
    when /^the url for works tagged "(.*)"$/i
      Work.tire.index.refresh
      tag_works_url(Tag.find_by_name($1)).sub("http://www.example.com", ArchiveConfig.APP_URL)
    when /^the works tagged "(.*)" in collection "(.*)"$/i
      Work.tire.index.refresh
      collection_tag_works_path(Collection.find_by_title($2), Tag.find_by_name($1))
    when /^the url for works tagged "(.*)" in collection "(.*)"$/i
      Work.tire.index.refresh
      collection_tag_works_url(Collection.find_by_title($2), Tag.find_by_name($1)).sub("http://www.example.com", ArchiveConfig.APP_URL)
    when /^the admin-posts page$/i
      admin_posts_path
    when /^the admin-settings page$/i
      admin_settings_path      
    when /^the admin-notices page$/i
      notify_admin_users_path
    when /^the FAQ reorder page$/i
      manage_archive_faqs_path
    when /^the support page$/i
      new_feedback_report_path
    when /^the new tag ?set page$/i
      new_tag_set_path
    when /^the "(.*)" tag ?set edit page$/i
      edit_tag_set_path(OwnedTagSet.find_by_title($1))    
    when /^the "(.*)" tag ?set page$/i
      tag_set_path(OwnedTagSet.find_by_title($1))
      
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Given(/^all emails? (?:have|has) been delivered$/) do
  ActionMailer::Base.deliveries.clear
  ActionMailer::Base.deliveries.should be_empty # Sanity check, ftw
end
Given(/^(\d)+ emails? should be delivered$/) do |count|
  emails.size.should == count.to_i
end
When(/^(?:I|they) follow "([^"]*?)" in #{capture_email}$/) do |link, email_ref|
  visit_in_email(email(email_ref).html_part, link)
end
Then(/^(\d)+ emails? should be delivered to (.*)$/) do |count, to|
  emails("to: \"#{email_for(to)}\"").size.should == count.to_i
end
Then(/^#{capture_email} should contain "(.*)"$/) do |email_ref, text|
  if email(email_ref).multipart?
    email(email_ref).text_part.body.should =~ /#{text}/
    email(email_ref).html_part.body.should =~ /#{text}/
  else
    email(email_ref).body.should =~ /#{text}/
  end
end
  def email_for(to)
    case to
  
    # add your own name => email address mappings here
  
    when /^#{capture_model}$/
      model($1).email

    when /^"([^@]*)"$/
      user = User.find_by_login($1)
      user.email 

    when /^"(.*)"$/
      $1
  
    else
      to
    end
  end
Given /^I am logged in as "([^\"]*)" with password "([^\"]*)"$/ do |login, password|
  step("I am logged out")
  user = User.find_by_login(login)
  if user.blank?
    user = FactoryGirl.create(:user, {:login => login, :password => password})
    user.activate
  else
    user.password = password
    user.password_confirmation = password
    user.save
  end
  visit login_path
  fill_in "User name", :with => login
  fill_in "Password", :with => password
  check "Remember Me"
  click_button "Log In"
  assert UserSession.find
end
Given /^I am logged out$/ do
  visit logout_path
  assert !UserSession.find
  visit admin_logout_path
  assert !AdminSession.find
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
When /^I set up the comment "([^"]*)" on the tag "([^"]*)"$/ do |comment_text, tag|
  tag = Tag.find_by_name!(tag)
  visit tag_url(tag)
  click_link(" comment")
  fill_in("Comment", :with => comment_text)
end
