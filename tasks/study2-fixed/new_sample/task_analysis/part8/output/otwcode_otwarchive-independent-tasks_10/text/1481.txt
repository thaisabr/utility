Feature: Comment on tag
As a tag wrangler
I'd like to comment on a tag'
Scenario: Unedited tag does not show on discussion page
Given the following activated tag wranglers exist
| login     |
| dizmo     |
| Enigel    |
And a fandom exists with name: "Stargate Atlantis", canonical: true
When I am logged in as "Enigel"
When I view the tag "Stargate Atlantis"
When I am logged in as "dizmo"
When I view tag wrangling discussions
Then I should not see "Stargate Atlantis"
Scenario: Issue 2185: email notifications for tag commenting; TO DO: replies to comments
Given the following activated tag wranglers exist
| login       | password      | email             |
| dizmo       | wrangulator   | dizmo@example.org |
| Enigel      | wrangulator   | enigel@example.org|
| Cesy        | wrangulator   | cesy@example.org|
And a fandom exists with name: "Eroica", canonical: true
And a fandom exists with name: "Doctor Who", canonical: true
And the tag wrangler "Enigel" with password "wrangulator" is wrangler of "Eroica"
And the tag wrangler "Cesy" with password "wrangulator" is wrangler of "Doctor Who"
And the tag wrangler "dizmo" with password "wrangulator" is wrangler of "Doctor Who"
When I am logged in as "Enigel" with password "wrangulator"
And I go to Enigel's user page
And I follow "Preferences"
And I uncheck "Turn off copies of your own comments"
And I press "Update"
And I log out
When I am logged in as "Cesy" with password "wrangulator"
And I go to Cesy's user page
And I follow "Preferences"
And I check "Turn off copies of your own comments"
And I press "Update"
And all emails have been delivered
And I view the tag "Eroica"
And I follow "0 comments"
And I fill in "Comment" with "really clever stuff"
And I press "Comment"
Then I should see "Comment created"
And 1 email should be delivered to "enigel@example.org"
And the email should contain "really clever stuff"
And the email should contain "Cesy"
And the email should contain "left the following comment on"
And the email should contain "the tag"
When I follow "Go to the thread starting from this comment" in the email
And I fill in "User name:" with "dizmo"
And I fill in "Password:" with "wrangulator"
And I press "Log In"
Then I should see "Reading Comments on Eroica"
And I should see "really clever stuff"
And I log out
When I follow "Read all comments on Eroica" in the email
And I fill in "User name:" with "Cesy"
And I fill in "Password:" with "wrangulator"
And I press "Log In"
Then I should see "Reading Comments on Eroica"
And I should see "really clever stuff"
And I log out
When I follow "Reply to this comment" in the email
And I fill in "User name:" with "Enigel"
And I fill in "Password:" with "wrangulator"
And I press "Log In"
Then I should see "Reading Comments on Eroica"
And I should see "really clever stuff"
And all emails have been delivered
And I am logged in as "Enigel" with password "wrangulator"
When I view the tag "Doctor Who"
And I follow "0 comments"
And I fill in "Comment" with "really clever stuff"
And I press "Comment"
Then I should see "Comment created"
And 1 email should be delivered to "cesy@example.org"
And 1 email should be delivered to "dizmo@example.org"
And 1 email should be delivered to "enigel@example.org"
When I follow "Edit"
And all emails have been delivered
And I press "Update"
Then I should see "Comment was successfully updated"
And 3 emails should be delivered
Scenario: comments on synonym fandoms should be received by the wrangler of the canonical merger
Given the following activated tag wranglers exist
| login       | password      | email             |
| dizmo       | wrangulator   | dizmo@example.org |
| Enigel      | wrangulator   | enigel@example.org|
And a fandom exists with name: "Doctor Who", canonical: true
And the tag wrangler "Enigel" with password "wrangulator" is wrangler of "Doctor Who"
And a synonym "Dr Who" of the tag "Doctor Who"
When I am logged in as "dizmo" with password "wrangulator"
And I post the comment "Heads up" on the tag "Dr Who"
Then 1 email should be delivered to "enigel@example.org"
