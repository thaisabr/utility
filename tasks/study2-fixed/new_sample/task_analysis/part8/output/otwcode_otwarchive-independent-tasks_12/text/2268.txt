Feature: Orphan work
In order to have an archive full of works
As an author
I want to orphan works
Scenario: Orphan a single work, using the default orphan_account
Given I have an orphan account
And the following activated user exists
| login         | password   |
| orphaneer     | password   |
And I am logged in as "orphaneer" with password "password"
And I post the work "Shenanigans"
When I view the work "Shenanigans"
Then I should see "Edit"
When I follow "Edit"
Then I should see "Edit Work"
And I should see "Orphan Work"
When I follow "Orphan Work"
Then I should see "Read More About The Orphaning Process"
When I choose "Take my pseud off as well"
And I press "Yes, I'm sure"
Then I should see "Orphaning was successful."
When I follow "Works"
Then I should not see "Shenanigans"
When I view the work "Shenanigans"
Then I should see "orphan_account"
And I should not see "Delete"
When I post the work "Shenanigans2"
When I view the work "Shenanigans2"
Then I should see "Edit"
When I follow "Edit"
Then I should see "Edit Work"
And I should see "Orphan Work"
When I follow "Orphan Work"
Then I should see "Read More About The Orphaning Process"
When I choose "Leave a copy of my pseud on"
And I press "Yes, I'm sure"
Then I should see "Orphaning was successful."
When I follow "Works"
Then I should not see "Shenanigans2"
When I view the work "Shenanigans2"
Then I should see "orphaneer (orphan_account)"
And I should not see "Delete"
Feature: Orphan pseud
In order to have an archive full of works
As an author
I want to orphan all works under one pseud
Scenario: Orphan all works belonging to one pseud
Given I have an orphan account
And the following activated user exists
| login         | password   |
| orphanpseud   | password   |
And I am logged in as "orphanpseud" with password "password"
When I post the work "Shenanigans"
And I post the work "Shenanigans 2"
When I follow "orphanpseud" within ".byline"
Then I should see "Shenanigans 2 by orphanpseud"
When I follow "Back To Pseuds"
Then I should see "orphanpseud"
And I should see "2 works"
When I follow "Orphan Works"
Then I should see "Orphan Works"
When I choose "Take my pseud off as well"
And I press "Yes, I'm sure"
Then I should see "Orphaning was successful."
When I view the work "Shenanigans"
Then I should see "orphan_account"
And I should not see "orphanpseud" within ".userstuff"
When I view the work "Shenanigans 2"
Then I should see "orphan_account"
And I should not see "orphanpseud" within ".userstuff"
Scenario: Orphan all works belonging to one pseud, add a copy of the pseud to the orphan_account
Given I have an orphan account
And the following activated user exists
| login         | password   |
| orphanpseud   | password   |
And I am logged in as "orphanpseud" with password "password"
When I post the work "Shenanigans"
When I post the work "Shenanigans 2"
When I follow "orphanpseud" within ".byline"
Then I should see "Shenanigans by orphanpseud"
And I should see "Shenanigans 2 by orphanpseud"
When I follow "Back To Pseuds"
Then I should see "orphanpseud"
And I should see "2 works"
When I follow "Orphan Works"
Then I should see "Orphan Works"
When I choose "Leave a copy of my pseud on"
And I press "Yes, I'm sure"
Then I should see "Orphaning was successful."
When I view the work "Shenanigans"
Then I should see "orphanpseud (orphan_account)"
And I should not see "orphanpseud" within ".userstuff"
When I view the work "Shenanigans 2"
Then I should see "orphanpseud (orphan_account)"
And I should not see "orphanpseud" within ".userstuff"
Feature: Orphan account
In order to have an archive full of works
As an author
I want to orphan all works in my account
Scenario: Orphan all works belonging to a user
Given I have an orphan account
And the following activated user exists
| login         | password   |
| orphaneer     | password   |
And I am logged in as "orphaneer" with password "password"
When I post the work "Shenanigans"
And I post the work "Shenanigans 2"
And I post the work "Shenanigans - the early years"
When I go to orphaneer's user page
Then I should see "Recent works"
And I should see "Shenanigans"
And I should see "Shenanigans 2"
And I should see "Shenanigans - the early years"
When I follow "Preferences"
When I follow "Orphan My Works"
Then I should see "Orphan Works"
And I should see "Are you sure you want to"
When I choose "Take my pseud off as well"
And I press "Yes, I'm sure"
Then I should see "Orphaning was successful."
When I view the work "Shenanigans"
Then I should see "orphan_account"
And I should not see "orphaneer" within ".userstuff"
When I view the work "Shenanigans 2"
Then I should see "orphan_account"
And I should not see "orphaneer" within ".userstuff"
When I view the work "Shenanigans - the early years"
Then I should see "orphan_account"
And I should not see "orphaneer" within ".userstuff"
Scenario: Orphan all works belonging to a user, add a copy of the pseud to the orphan_account
Given I have an orphan account
And the following activated user exists
| login         | password   |
| orphaneer     | password   |
And I am logged in as "orphaneer" with password "password"
When I post the work "Shenanigans"
When I post the work "Shenanigans 2"
When I post the work "Shenanigans - the early years"
When I go to orphaneer's user page
Then I should see "Recent works"
And I should see "Shenanigans"
And I should see "Shenanigans 2"
And I should see "Shenanigans - the early years"
When I follow "Preferences"
When I follow "Orphan My Works"
Then I should see "Orphan Works"
And I should see "Are you sure you want to"
When I choose "Leave a copy of my pseud on"
And I press "Yes, I'm sure"
Then I should see "Orphaning was successful."
When I view the work "Shenanigans"
Then I should see "orphaneer (orphan_account)"
And I should not see "orphaneer" within ".userstuff"
When I view the work "Shenanigans 2"
Then I should see "orphaneer (orphan_account)"
And I should not see "orphaneer" within ".userstuff"
When I view the work "Shenanigans - the early years"
Then I should see "orphaneer (orphan_account)"
And I should not see "orphaneer" within ".userstuff"
