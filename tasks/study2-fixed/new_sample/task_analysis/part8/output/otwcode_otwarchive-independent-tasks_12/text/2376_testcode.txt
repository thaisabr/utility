Given /^basic tags$/ do
  Warning.find_or_create_by_name_and_canonical("No Archive Warnings Apply", true)
  Warning.find_or_create_by_name_and_canonical("Choose Not To Use Archive Warnings", true)
  Rating.find_or_create_by_name_and_canonical("Not Rated", true)
  Rating.find_or_create_by_name_and_canonical("Explicit", true)
  Fandom.find_or_create_by_name_and_canonical("No Fandom", true)
  Category.find_or_create_by_name_and_canonical("Other", true)
  Category.find_or_create_by_name_and_canonical("F/F", true)
  Category.find_or_create_by_name_and_canonical("Multi", true)
  Category.find_or_create_by_name_and_canonical("M/F", true)
  Category.find_or_create_by_name_and_canonical("M/M", true)
end
Given /^I am logged in as "([^\"]*)"$/ do |login|
  Given %{I am logged in as "#{login}" with password "#{DEFAULT_PASSWORD}"}
end
Given /^I am logged in as a random user$/ do
  Given "I am logged out"
  name = "testuser#{User.count + 1}"
  user = Factory.create(:user, :login => name, :password => DEFAULT_PASSWORD)
  user.activate
  visit login_path
  fill_in "User name", :with => name
  fill_in "Password", :with => DEFAULT_PASSWORD
  check "Remember me"
  click_button "Log in"
  assert UserSession.find
end
Given /^I am logged out$/ do
  visit logout_path
  assert !UserSession.find
  visit admin_logout_path
  assert !AdminSession.find
end
When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    click_button(button)
  end
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
When /^(?:|I )fill in "([^"]*)" with "([^"]*)"(?: within "([^"]*)")?$/ do |field, value, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_content(text)
    else
      assert page.has_content?(text)
    end
  end
end
Then /^(?:|I )should not see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_no_content(text)
    else
      assert page.has_no_content?(text)
    end
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)
    case page_name

    when /the home\s?page/
      '/'

    # the following are examples using path_to_pickle

    when /^#{capture_model}(?:'s)? page$/                           # eg. the forum's page
      path_to_pickle $1

    when /^#{capture_model}(?:'s)? #{capture_model}(?:'s)? page$/   # eg. the forum's post's page
      path_to_pickle $1, $2

    when /^#{capture_model}(?:'s)? #{capture_model}'s (.+?) page$/  # eg. the forum's post's comments page
      path_to_pickle $1, $2, :extra => $3                           #  or the forum's post's edit page

    when /^#{capture_model}(?:'s)? (.+?) page$/                     # eg. the forum's posts page
      path_to_pickle $1, :extra => $2                               #  or the forum's edit page

    # Add more mappings here.

    when /^the tagsets page$/i
      tag_sets_path
    when /^the login page$/i
      new_user_session_path
    when /^(.*)'s user page$/i
      user_path(:id => $1)
    when /^(.*)'s user url$/i
      user_url(:id => $1).sub("http://www.example.com", ArchiveConfig.APP_URL)
    when /^(.*)'s works page$/i
      user_works_path(:user_id => $1)
    when /^the "(.*)" work page/
      work_path(Work.find_by_title($1))
    when /^the work page with title (.*)/
      work_path(Work.find_by_title($1))
    when /^(.*)'s bookmarks page$/i
      user_bookmarks_path(:user_id => $1)
    when /^(.*)'s pseuds page$/i
      user_pseuds_path(:user_id => $1)
    when /^(.*)'s reading page$/i
      user_readings_path(:user_id => $1)
    when /^(.*)'s series page$/i
      user_series_index_path(:user_id => $1)
    when /^(.*)'s preferences page$/i
      user_preferences_path(:user_id => $1)
    when /^the subscriptions page for "(.*)"$/i
      user_subscriptions_path(:user_id => $1)
    when /^(.*)'s profile page$/i
      user_profile_path(:user_id => $1)
    when /my pseuds page/
      user_pseuds_path(User.current_user)
    when /my user page/
      user_path(User.current_user)
    when /my preferences page/
      user_preferences_path(User.current_user)
    when /my bookmarks page/
      user_bookmarks_path(User.current_user)
    when /my subscriptions page/
      user_subscriptions_path(User.current_user)      
    when /my profile page/
      user_profile_path(User.current_user)
    when /my claims page/
      user_claims_path(User.current_user)
    when /my signups page/
      user_signups_path(User.current_user)
    when /the import page/
      new_work_path(:import => 'true')
    when /the work-skins page/
      skins_path(:work_skins => true)
    when /^(.*)'s skin page/
      skins_path(:user_id => $1)
    when /^"(.*)" skin page/
      skin_path(Skin.find_by_title($1))
    when /^"(.*)" edit skin page/
      edit_skin_path(Skin.find_by_title($1))
    when /^"(.*)" collection's page$/i                      # e.g. when I go to "Collection name" collection's page
      collection_path(Collection.find_by_title($1))
    when /^the "(.*)" signups page$/i                      # e.g. when I go to "Collection name" signup page
      collection_signups_path(Collection.find_by_title($1))
    when /^the "(.*)" requests page$/i                      # e.g. when I go to "Collection name" signup page
      collection_requests_path(Collection.find_by_title($1))
    when /^"(.*)" collection's url$/i                      # e.g. when I go to "Collection name" collection's url
      collection_url(Collection.find_by_title($1)).sub("http://www.example.com", ArchiveConfig.APP_URL)
    when /^"(.*)" gift exchange edit page$/i
      edit_collection_gift_exchange_path(Collection.find_by_title($1))
    when /^"(.*)" collection's static page$/i
      static_collection_path(Collection.find_by_title($1))
    when /^the works tagged "(.*)"$/i
      tag_works_path(Tag.find_by_name($1))
    when /^the url for works tagged "(.*)"$/i
      tag_works_url(Tag.find_by_name($1)).sub("http://www.example.com", ArchiveConfig.APP_URL)
    when /^the works tagged "(.*)" in collection "(.*)"$/i
      collection_tag_works_path(Collection.find_by_title($2), Tag.find_by_name($1))
    when /^the url for works tagged "(.*)" in collection "(.*)"$/i
      collection_tag_works_url(Collection.find_by_title($2), Tag.find_by_name($1)).sub("http://www.example.com", ArchiveConfig.APP_URL)
    when /^the admin-posts page$/i
      admin_posts_path
    when /^the admin-settings page$/i
      admin_settings_path      
    when /^the admin-notices page$/i
      notify_admin_users_path
    when /^the FAQ reorder page$/i
      manage_archive_faqs_path
    when /^the support page$/i
      new_feedback_report_path
    when /^the new tag ?set page$/i
      new_tag_set_path
    when /^the "(.*)" tag ?set edit page$/i
      edit_tag_set_path(OwnedTagSet.find_by_title($1))    
    when /^the "(.*)" tag ?set page$/i
      tag_set_path(OwnedTagSet.find_by_title($1))    
      
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
When /^I fill in "([^\"]*)" with$/ do |field, value|
  fill_in(field, :with => value)
end
Then /^I should see the text with tags "(.*)"$/ do |text|
  page.body.should =~ /#{text}/m
end
Given /^the chaptered work(?: with ([\d]+) chapters)?(?: with ([\d]+) comments?)? "([^"]*)"$/ do |n_chapters, n_comments, title|
  Given %{I am logged in as a random user}
  And %{I post the work "#{title}"}
  work = Work.find_by_title!(title)
  visit work_url(work)
  n_chapters ||= 2
  (n_chapters.to_i - 1).times do |i|
    When %{I follow "Add Chapter"}
    fill_in("content", :with => "Yet another chapter.")
    click_button("Post without preview")
  end
  And %{I am logged out}
  n_comments ||= 0
  n_comments.to_i.times do |i|
    Given %{I am logged in as a random user}
    And %{I post the comment "Bla bla" on the work "#{title}"}
    And %{I am logged out}
  end
end
When /^I view the work "([^\"]*)"(?: in (full|chapter-by-chapter) mode)?$/ do |work, mode|
  work = Work.find_by_title!(work)
  visit work_url(work)
  When %{I follow "Entire Work"} if mode == "full"
  When %{I follow "View chapter by chapter"} if mode == "chapter-by-chapter"
end
When /^I post the chaptered work "([^\"]*)"$/ do |title|
  When %{I post the work "#{title}"}
  When %{I follow "Add Chapter"}
  fill_in("content", :with => "Another Chapter.")
  click_button("Preview")
  When %{I press "Post Chapter"}
end
When /^I post the work "([^\"]*)"$/ do |title|
  When %{I post the work "#{title}" with fandom "#{DEFAULT_FANDOM}" with freeform "#{DEFAULT_FREEFORM}" with category "#{DEFAULT_CATEGORY}"}
end
Given /^I am logged out as an admin$/ do
  visit admin_logout_path
  assert !AdminSession.find
end
Given /^guest downloading is off$/ do
  Given "I am logged in as an admin"
  visit(admin_settings_path)
  check("Turn off downloading for guests")
  click_button("Update")
end
Given /^guest downloading is on$/ do
  Given "I am logged in as an admin"
  visit(admin_settings_path)
  uncheck("Turn off downloading for guests")
  click_button("Update")
end
