Feature: Search Tags
In order to find tags
As a user
I want to use tag search
Scenario: Search for fandom with slash in name
Given I have no tags
And a fandom exists with name: "first/fandom", canonical: false
And the tag indexes are updated
When I am on the search tags page
And I fill in "tag_search" with "first"
And I press "Search tags"
Then I should see "1 Found"
And I should see the tag search result "Fandom: first/fandom (0)"
Scenario: Search for fandom with period in name
Given I have no tags
And a fandom exists with name: "first.fandom", canonical: false
And the tag indexes are updated
When I am on the search tags page
And I fill in "tag_search" with "first.fandom"
And I press "Search tags"
Then I should see "1 Found"
And I should see the tag search result "Fandom: first.fandom (0)"
When I follow "first.fandom"
Then I should see "This tag belongs to the Fandom Category"
When I am on the search tags page
And I fill in "tag_search" with "first"
And I press "Search tags"
Then I should see "0 Found"
And I should not see "Fandom: first.fandom (0)"
Feature: Comment on tag
As a tag wrangler
I'd like to comment on a tag'
Scenario: Comments pagination for a tag with slashes in the name
Given a tag "hack/sign" with 34 comments
And I am logged in as a tag wrangler
When I post the comment "And now things should not break!" on the tag "hack/sign"
Then I should see "Comment created"
When I follow "Next" within ".pagination"
Then I should see "And now things should not break!"
Scenario: Comments pagination for a tag with periods in the name
Given a period-containing tag "sign.me" with 34 comments
And I am logged in as a tag wrangler
When I post the comment "And now things should not break!" on the tag "sign.me"
Then I should see "Comment created"
When I follow "Next" within ".pagination"
Then I should see "And now things should not break!"
