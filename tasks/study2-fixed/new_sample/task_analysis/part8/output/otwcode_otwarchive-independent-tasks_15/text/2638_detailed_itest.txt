Classes: 6
[name:Admin, file:otwcode_otwarchive/app/models/admin.rb, step:Given ]
[name:AdminPost, file:otwcode_otwarchive/app/models/admin_post.rb, step:null]
[name:AdminPost, file:otwcode_otwarchive/app/models/admin_post.rb, step:When ]
[name:AdminSession, file:otwcode_otwarchive/app/models/admin_session.rb, step:null]
[name:ArchiveFaq, file:otwcode_otwarchive/app/models/archive_faq.rb, step:null]
[name:Factory, file:null, step:Given ]

Methods: 24
[name:blank?, type:Prompt, file:otwcode_otwarchive/app/models/prompt.rb, step:Given ]
[name:click_button, type:Object, file:null, step:Given ]
[name:click_button, type:Object, file:null, step:When ]
[name:click_link, type:Object, file:null, step:When ]
[name:fill_in, type:Object, file:null, step:Given ]
[name:fill_in, type:Object, file:null, step:When ]
[name:find_by_login, type:Admin, file:otwcode_otwarchive/app/models/admin.rb, step:Given ]
[name:has_content?, type:Object, file:null, step:Then ]
[name:have_content, type:Object, file:null, step:Then ]
[name:manage, type:ArchiveFaqsController, file:otwcode_otwarchive/app/controllers/archive_faqs_controller.rb, step:When ]
[name:new, type:AdminSessionsController, file:otwcode_otwarchive/app/controllers/admin_sessions_controller.rb, step:null]
[name:new, type:ArchiveFaqsController, file:otwcode_otwarchive/app/controllers/archive_faqs_controller.rb, step:null]
[name:new, type:AdminPostsController, file:otwcode_otwarchive/app/controllers/admin_posts_controller.rb, step:null]
[name:new, type:AdminPostsController, file:otwcode_otwarchive/app/controllers/admin_posts_controller.rb, step:When ]
[name:page, type:Object, file:null, step:Then ]
[name:respond_to?, type:Prompt, file:otwcode_otwarchive/app/models/prompt.rb, step:Then ]
[name:title, type:ArchiveFaq, file:otwcode_otwarchive/app/models/archive_faq.rb, step:null]
[name:title, type:AdminPost, file:otwcode_otwarchive/app/models/admin_post.rb, step:null]
[name:title, type:AdminPost, file:otwcode_otwarchive/app/models/admin_post.rb, step:When ]
[name:to_i, type:Object, file:null, step:When ]
[name:to_i, type:Object, file:null, step:Given ]
[name:with_scope, type:WebSteps, file:otwcode_otwarchive/features/step_definitions/web_steps.rb, step:When ]
[name:with_scope, type:WebSteps, file:otwcode_otwarchive/features/step_definitions/web_steps.rb, step:Then ]
[name:within, type:Object, file:null, step:When ]

Referenced pages: 8
otwcode_otwarchive/app/views/admin/_admin_nav.html.erb
otwcode_otwarchive/app/views/admin_posts/_admin_post_form.html.erb
otwcode_otwarchive/app/views/admin_posts/new.html.erb
otwcode_otwarchive/app/views/admin_sessions/new.html.erb
otwcode_otwarchive/app/views/archive_faqs/_archive_faq_form.html.erb
otwcode_otwarchive/app/views/archive_faqs/_archive_faq_order.html.erb
otwcode_otwarchive/app/views/archive_faqs/manage.html.erb
otwcode_otwarchive/app/views/archive_faqs/new.html.erb

