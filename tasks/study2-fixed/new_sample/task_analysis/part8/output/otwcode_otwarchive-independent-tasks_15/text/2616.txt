Feature: Prompt Meme Challenge
In order to have an archive full of works
As a humble user
I want to create a prompt meme and post to it
Scenario: View signups in the dashboard
Given I have Battle 12 prompt meme fully set up
And I am logged in as "myname1"
When I sign up for Battle 12 with combination A
When I am on my user page
Then I should see "My Signups (1)"
When I follow "My Signups (1)"
Then I should see "Battle 12"
And I should see "Edit"
And I should see "Delete"
Scenario: User is participating in a prompt meme and a gift exchange at once, clicks "Post to fulfill" on the prompt meme and sees the right boxes ticked
Given I have Battle 12 prompt meme fully set up
And everyone has signed up for Battle 12
Given I have created the gift exchange "My Gift Exchange"
And I have opened signup for the gift exchange "My Gift Exchange"
And everyone has signed up for the gift exchange "My Gift Exchange"
And I have generated matches for "My Gift Exchange"
And I have sent assignments for "My Gift Exchange"
When I am logged in as "myname3"
And I claim a prompt from "Battle 12"
When I start to fulfill my claim
Then the "Battle 12 (myname4) -  - Stargate Atlantis" checkbox should be checked
And the "My Gift Exchange (myname2)" checkbox should not be checked
And the "Battle 12 (myname4) -  - Stargate Atlantis" checkbox should be disabled
And the "My Gift Exchange (myname2)" checkbox should be disabled
Scenario: User posts to fulfill direct from Post New
Given I have Battle 12 prompt meme fully set up
And everyone has signed up for Battle 12
When I am logged in as "myname3"
And I claim a prompt from "Battle 12"
And I follow "Post New"
Then the "Battle 12 (myname4) -  - Stargate Atlantis" checkbox should not be checked
And the "Battle 12 (myname4) -  - Stargate Atlantis" checkbox should not be disabled
