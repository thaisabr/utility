Feature: General notice banner
Scenario: Banner is blank until admin sets it
When I am on the home page
Then I should not see "Hide this banner"
When I am logged in as "newname"
Then I should not see "Hide this banner"
Scenario: Admin can change banner
When an admin sets a custom banner notice
And I am logged in as "ordinaryuser"
Then the banner notice for a logged-in user should be set to "Custom notice"
Scenario: User can turn off banner, and it stays off when logging out and in again
When an admin sets a custom banner notice
When I am logged in as "newname"
When I am on my user page
When I follow "Hide this banner"
Then I should not see "Custom notice words"
When I am logged out
And I am logged in as "newname" with password "password"
Then I should not see "Custom notice words"
When I am on newname's user page
Then I should not see "Custom notice words"
Scenario: logged out user can also see banner
When an admin sets a custom banner notice
Then the banner notice for a logged-out user should be set to "Custom notice"
Scenario: logged out user hides banner
When an admin sets a custom banner notice
When I am logged out
When I am on the works page
When I follow "Hide this banner"
Then I should not see "Custom notice words"
Feature: First login help banner
Scenario: Turn off first login help banner having viewed it
Given the following activated user exists
| login         | password   |
| newname       | password   |
And I am logged in as "newname" with password "password"
Then I should see "Hi, newname!"
And I should see "Log out"
When I am on newname's user page
Then I should see "It looks like you've just logged into the archive for the first time"
When I follow "Learn some tips and tricks."
Then I should see "Here are some tips to help you get started."
And I should see "To log in, locate and fill in the log in link"
When I follow "Dismiss this message permanently"
Then I should not see "It looks like you've just logged into the archive for the first time"
When I am logged out
And I am logged in as "newname" with password "password"
Then I should not see "It looks like you've just logged into the archive for the first time"
When I am on newname's user page
Then I should not see "It looks like you've just logged into the archive for the first time"
Scenario: Turn off first login help banner without having viewed it
Given the following activated user exists
| login         | password   |
| newname2      | password   |
And I am logged in as "newname2" with password "password"
Then I should see "Hi, newname2!"
And I should see "Log out"
When I am on newname2's user page
Then I should see "It looks like you've just logged into the archive for the first time"
When I follow "Dismiss this message permanently"
Then I should not see "It looks like you've just logged into the archive for the first time"
When I am logged out
And I am logged in as "newname2" with password "password"
Then I should not see "It looks like you've just logged into the archive for the first time"
When I am on newname2's user page
Then I should not see "It looks like you've just logged into the archive for the first time"
