Given(/^#{capture_model} exists?(?: with #{capture_fields})?$/) do |name, fields|
  create_model(name, fields)
end
Given /^I am logged in as a random user$/ do
  step("I am logged out")
  name = "testuser#{User.count + 1}"
  user = FactoryGirl.create(:user, :login => name, :password => DEFAULT_PASSWORD)
  user.activate
  visit login_path
  fill_in "User name", :with => name
  fill_in "Password", :with => DEFAULT_PASSWORD
  check "Remember me"
  click_button "Log In"
  assert UserSession.find unless @javascript
end
Given /^the tag wrangler "([^\"]*)" with password "([^\"]*)" is wrangler of "([^\"]*)"$/ do |user, password, fandomname|
  tw = User.find_by_login(user)
  if tw.blank?
    tw = FactoryGirl.create(:user, {:login => user, :password => password})
    tw.activate
  else
    tw.password = password
    tw.password_confirmation = password
    tw.save
  end
  tw.tag_wrangler = '1'
  visit logout_path
  assert !UserSession.find
  visit login_path
  fill_in "User name", :with => user
  fill_in "Password", :with => password
  check "Remember Me"
  click_button "Log In"
  assert UserSession.find
  fandom = Fandom.find_or_create_by_name_and_canonical(fandomname, true)
  visit tag_wranglers_url
  fill_in "tag_fandom_string", :with => fandomname
  click_button "Assign"
end
When /^I view the tag "([^\"]*)"$/ do |tag|
  tag = Tag.find_by_name!(tag)
  visit tag_url(tag)
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    page.should have_content(text)
  end
end
Then /^(?:|I )should not see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_no_content(text)
    else
      assert page.has_no_content?(text)
    end
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
Given /^I am logged in as an admin$/ do
  step("I am logged out")
  admin = Admin.find_by_login("testadmin")
  if admin.blank?
    admin = FactoryGirl.create(:admin, login: "testadmin", password: "testadmin", email: "testadmin@example.org")
  end
  visit admin_login_path
  fill_in "Admin user name", with: "testadmin"
  fill_in "Admin password", with: "testadmin"
  click_button "Log in as admin"
  step("I should see \"Successfully logged in\"")
end
Given /^I am logged out$/ do
  visit logout_path
  assert UserSession.find.nil? unless @javascript
  visit admin_logout_path
  assert AdminSession.find.nil? unless @javascript
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    page.should have_content(text)
  end
end
