Feature: Admin Actions to manage users
In order to manage user accounts
As an an admin
I want to be able to look up and edit individual users
Scenario: A admin can activate a user account
Given the user "mrparis" exists and is not activated
And I am logged in as an admin
When I go to the abuse administration page for "mrparis"
And I press "Activate User Account"
Then I should see "User Account Activated"
And the user "mrparis" should be activated
Scenario: A admin can send an activation email for a user account
Given the following users exist
| login        | password    | email                 | activation_code |
| torres       | something   | torres@starfleet.org  | fake_code       |
And I am logged in as an admin
And all emails have been delivered
When I go to the abuse administration page for "torres"
And I press "Send Activation Email"
Then I should see "Activation email sent"
And 1 email should be delivered to "torres"
