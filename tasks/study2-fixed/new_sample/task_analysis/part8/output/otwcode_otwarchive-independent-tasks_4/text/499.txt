Feature: Leave kudos
In order to show appreciation
As a reader
I want to leave kudos
Background:
Given the following activated users exist
| login          | email           |
| myname1        | myname1@foo.com |
| myname2        | myname2@foo.com |
| myname3        | myname3@foo.com |
Scenario: post kudos
Given I am logged in as "myname2"
And all emails have been delivered
And I view the work "Awesome Story"
Then I should not see "left kudos on this work"
When I press "Kudos ♥"
Then I should see "myname2 left kudos on this work!"
And 0 emails should be delivered
When kudos are sent
Then 1 email should be delivered to "myname1@foo.com"
And the email should contain "myname2"
And the email should contain "left kudos"
And the email should contain "."
And all emails have been delivered
When I press "Kudos ♥"
Then I should see "You have already left kudos here. :)"
And I should not see "myname2 and myname2 left kudos on this work!"
When I log out
And I press "Kudos ♥"
Then I should see "Thank you for leaving kudos!"
And I should see "myname2 as well as 1 guest left kudos on this work!"
When I press "Kudos ♥"
Then I should see "You have already left kudos here. :)"
When kudos are sent
Then 1 email should be delivered to "myname1@foo.com"
And the email should contain "A guest"
And the email should contain "left kudos"
And the email should contain "."
When I am logged in as "myname3"
And I view the work "Awesome Story"
And I press "Kudos ♥"
Then I should see "myname3 and myname2 as well as 1 guest left kudos on this work!"
When I am logged in as "myname1"
And I view the work "Awesome Story"
Then I should not see "Kudos ♥"
Scenario: batched kudos email
Given I am logged in as "myname1"
And I post the work "Another Awesome Story"
And all emails have been delivered
And the kudos queue is cleared
And I am logged in as "myname2"
And I leave kudos on "Awesome Story"
And I leave kudos on "Another Awesome Story"
And I am logged in as "someone_else"
And I leave kudos on "Awesome Story"
And I leave kudos on "Another Awesome Story"
And I am logged out
And I leave kudos on "Awesome Story"
And I leave kudos on "Another Awesome Story"
When kudos are sent
Then 1 email should be delivered to "myname1@foo.com"
And the email should contain "myname2"
And the email should contain "someone_else"
And the email should contain "guest"
And the email should contain "Awesome Story"
And the email should contain "Another Awesome Story"
