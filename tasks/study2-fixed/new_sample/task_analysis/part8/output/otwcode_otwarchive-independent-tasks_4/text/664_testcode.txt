Given /^I have posted an admin post$/ do
  step("I am logged in as an admin")
    step("I make an admin post")
    step("I am logged out as an admin")
end
Given /^I am logged in as "([^\"]*)"$/ do |login|
  step(%{I am logged in as "#{login}" with password "#{DEFAULT_PASSWORD}"})
end
Given /^I am logged in$/ do
  step(%{I am logged in as "#{DEFAULT_USER}"})
end
When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end
When /^(?:|I )press "([^"]*)"(?: within "([^"]*)")?$/ do |button, selector|
  with_scope(selector) do
    click_button(button)
  end
end
When /^(?:|I )follow "([^"]*)"(?: within "([^"]*)")?$/ do |link, selector|
  with_scope(selector) do
    click_link(link)
  end
end
When /^(?:|I )fill in "([^"]*)" with "([^"]*)"(?: within "([^"]*)")?$/ do |field, value, selector|
  with_scope(selector) do
    fill_in(field, :with => value)
  end
end
When /^(?:|I )check "([^"]*)"(?: within "([^"]*)")?$/ do |field, selector|
  with_scope(selector) do
    check(field)
  end
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    page.should have_content(text)
  end
end
Then /^(?:|I )should not see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    if page.respond_to? :should
      page.should have_no_content(text)
    else
      assert page.has_no_content?(text)
    end
  end
end
  def with_scope(locator)
    locator ? within(locator) { yield } : yield
  end
  def path_to(page_name)
    case page_name

    when /the home\s?page/
      '/'
    when /^the search bookmarks page$/i
      Bookmark.tire.index.refresh
      search_bookmarks_path
    when /^the search tags page$/i
      Tag.tire.index.refresh
      search_tags_path
    when /^the search works page$/i
      Work.tire.index.refresh
      search_works_path      
    when /^the search people page$/i
      Pseud.tire.index.refresh
      search_people_path
    when /^the bookmarks page$/i
      bookmarks_path

    # the following are examples using path_to_pickle

    when /^#{capture_model}(?:'s)? page$/                           # eg. the forum's page
      path_to_pickle $1

    when /^#{capture_model}(?:'s)? #{capture_model}(?:'s)? page$/   # eg. the forum's post's page
      path_to_pickle $1, $2

    when /^#{capture_model}(?:'s)? #{capture_model}'s (.+?) page$/  # eg. the forum's post's comments page
      path_to_pickle $1, $2, :extra => $3                           #  or the forum's post's edit page

    when /^#{capture_model}(?:'s)? (.+?) page$/                     # eg. the forum's posts page
      path_to_pickle $1, :extra => $2                               #  or the forum's edit page

    # Add more mappings here.

    when /^the tagsets page$/i
      tag_sets_path
    when /^the login page$/i
      new_user_session_path
    when /^account creation page$/i
      new_user_path
    when /^invite requests page$/i
      invite_requests_path
    when /^(.*)'s user page$/i
      user_path(:id => $1)
    when /^(.*)'s user url$/i
      user_url(:id => $1).sub("http://www.example.com", "http://#{ArchiveConfig.APP_HOST}")
    when /^(.*)'s works page$/i
      Work.tire.index.refresh
      user_works_path(:user_id => $1)
    when /^the "(.*)" work page/
      work_path(Work.find_by_title($1))
    when /^the work page with title (.*)/
      work_path(Work.find_by_title($1))
    when /^(.*)'s bookmarks page$/i
      Bookmark.tire.index.refresh
      user_bookmarks_path(:user_id => $1)
    when /^(.*)'s pseuds page$/i
      user_pseuds_path(:user_id => $1)
    when /^(.*)'s invitations page$/i
      user_invitations_path(:user_id => $1)
    when /^(.*)'s reading page$/i
      user_readings_path(:user_id => $1)
    when /^(.*)'s series page$/i
      user_series_index_path(:user_id => $1)
    when /^(.*)'s stats page$/i
      user_stats_path(:user_id => $1)
    when /^(.*)'s preferences page$/i
      user_preferences_path(:user_id => $1)
    when /^(.*)'s related works page$/i
      user_related_works_path(:user_id => $1)
    when /^the subscriptions page for "(.*)"$/i
      user_subscriptions_path(:user_id => $1)
    when /^(.*)'s profile page$/i
      user_profile_path(:user_id => $1)
    when /my pseuds page/
      user_pseuds_path(User.current_user)
    when /my user page/
      user_path(User.current_user)
    when /my preferences page/
      user_preferences_path(User.current_user)
    when /my bookmarks page/
      Bookmark.tire.index.refresh
      user_bookmarks_path(User.current_user)
    when /my works page/
      Work.tire.index.refresh
      user_works_path(User.current_user)
    when /my subscriptions page/
      user_subscriptions_path(User.current_user)   
    when /my stats page/
      user_stats_path(User.current_user)   
    when /my profile page/
      user_profile_path(User.current_user)
    when /my claims page/
      user_claims_path(User.current_user)
    when /my signups page/
      user_signups_path(User.current_user)
    when /my related works page/
      user_related_works_path(User.current_user)
    when /my inbox page/
      user_inbox_path(User.current_user)
    when /the import page/
      new_work_path(:import => 'true')
    when /the work-skins page/
      skins_path(:skin_type => "WorkSkin")
    when /^(.*)'s skins page/
      skins_path(:user_id => $1)
    when /^"(.*)" skin page/
      skin_path(Skin.find_by_title($1))
    when /^"(.*)" edit skin page/
      edit_skin_path(Skin.find_by_title($1))
    when /^"(.*)" collection's page$/i                         # e.g. when I go to "Collection name" collection's page
      collection_path(Collection.find_by_title($1))
    when /^the "(.*)" signups page$/i                          # e.g. when I go to the "Collection name" signup page
      collection_signups_path(Collection.find_by_title($1))
    when /^the "(.*)" requests page$/i                         # e.g. when I go to the "Collection name" signup page
      collection_requests_path(Collection.find_by_title($1))
    when /^the "(.*)" assignments page$/i                      # e.g. when I go to the "Collection name" assignments page
      collection_assignments_path(Collection.find_by_title($1))
    when /^"(.*)" collection's url$/i                          # e.g. when I go to "Collection name" collection's url
      collection_url(Collection.find_by_title($1)).sub("http://www.example.com", "http://#{ArchiveConfig.APP_HOST}")
    when /^"(.*)" gift exchange edit page$/i
      edit_collection_gift_exchange_path(Collection.find_by_title($1))
    when /^"(.*)" gift exchange matching page$/i
      collection_potential_matches_path(Collection.find_by_title($1))
    when /^the works tagged "(.*)"$/i
      Work.tire.index.refresh
      tag_works_path(Tag.find_by_name($1))
    when /^the bookmarks tagged "(.*)"$/i
      Bookmark.tire.index.refresh
      tag_bookmarks_path(Tag.find_by_name($1))
    when /^the url for works tagged "(.*)"$/i
      Work.tire.index.refresh
      tag_works_url(Tag.find_by_name($1)).sub("http://www.example.com", "http://#{ArchiveConfig.APP_HOST}")
    when /^the bookmarks in collection "(.*)"$/i
      Bookmark.tire.index.refresh
      collection_bookmarks_path(Collection.find_by_title($1))
    when /^the works tagged "(.*)" in collection "(.*)"$/i
      Work.tire.index.refresh
      collection_tag_works_path(Collection.find_by_title($2), Tag.find_by_name($1))
    when /^the url for works tagged "(.*)" in collection "(.*)"$/i
      Work.tire.index.refresh
      collection_tag_works_url(Collection.find_by_title($2), Tag.find_by_name($1)).sub("http://www.example.com", "http://#{ArchiveConfig.APP_HOST}")
    when /^the tag comments? page for "(.*)"$/i
      tag_comments_path(Tag.find_by_name($1))
    when /^the admin-posts page$/i
      admin_posts_path
    when /^the admin-settings page$/i
      admin_settings_path      
    when /^the admin-notices page$/i
      notify_admin_users_path
    when /^the FAQ reorder page$/i
      manage_archive_faqs_path
    when /^the Wrangling Guidelines reorder page$/i
      manage_wrangling_guidelines_path
    when /^the tos page$/i
      tos_path
    when /^the faq page$/i
      archive_faqs_path
    when /^the wrangling guidelines page$/i
      wrangling_guidelines_path
    when /^the support page$/i
      new_feedback_report_path
    when /^the new tag ?set page$/i
      new_tag_set_path
    when /^the "(.*)" tag ?set edit page$/i
      edit_tag_set_path(OwnedTagSet.find_by_title($1))    
    when /^the "(.*)" tag ?set page$/i
      tag_set_path(OwnedTagSet.find_by_title($1))
      
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue Object => e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
When /^I edit the work "([^\"]*)"$/ do |work|
  work = Work.find_by_title!(work)
  visit edit_work_url(work)
end
When /^I post the work "([^\"]*)"$/ do |title|
  step %{I post the work "#{title}" with fandom "#{DEFAULT_FANDOM}" with freeform "#{DEFAULT_FREEFORM}" with category "#{DEFAULT_CATEGORY}"}
end
When /^I post a work with category "([^\"]*)"$/ do |category|
  step %{I post the work "#{DEFAULT_TITLE}" with fandom "#{DEFAULT_FANDOM}" with freeform "#{DEFAULT_FREEFORM}" with category "#{category}"}
end
When /^I view the "([^\"]*)" works index$/ do |tag|
  visit works_path(:tag_id => tag.to_param)
end
Given /^basic tags$/ do
  Warning.find_or_create_by_name_and_canonical("No Archive Warnings Apply", true)
  Warning.find_or_create_by_name_and_canonical("Choose Not To Use Archive Warnings", true)
  Rating.find_or_create_by_name_and_canonical("Not Rated", true)
  Rating.find_or_create_by_name_and_canonical("Explicit", true)
  Fandom.find_or_create_by_name_and_canonical("No Fandom", true)
  Category.find_or_create_by_name_and_canonical("Other", true)
  Category.find_or_create_by_name_and_canonical("F/F", true)
  Category.find_or_create_by_name_and_canonical("Multi", true)
  Category.find_or_create_by_name_and_canonical("M/F", true)
  Category.find_or_create_by_name_and_canonical("M/M", true)
end
Given /^I have (?:a|the) hidden collection "([^\"]*)" with name "([^\"]*)"$/ do |title, name|
  step %{I am logged in as "moderator"}
  step %{I set up the collection "#{title}" with name "#{name}"}
  step %{I check "This collection is unrevealed"}
  step %{I submit}

  step "I am logged out"
end
Given /^I have (?:an|the) anonymous collection "([^\"]*)" with name "([^\"]*)"$/ do |title, name|
  step %{I am logged in as "moderator"}
  step %{I set up the collection "#{title}" with name "#{name}"}
  step %{I check "This collection is anonymous"}
  step %{I submit}

  step "I am logged out"
end
Given /^all search indexes are updated$/ do
  [Work, Bookmark, Pseud, Tag].each do |klass|
    klass.import
    klass.tire.index.refresh
  end
end
Given /^I am logged in as an admin$/ do
  step("I am logged out")
  admin = Admin.find_by_login("testadmin")
  if admin.blank?
    admin = FactoryGirl.create(:admin, :login => "testadmin", :password => "testadmin", :email => "testadmin@example.org")
  end
  visit admin_login_path
  fill_in "Admin user name", :with => "testadmin"
  fill_in "Admin password", :with => "testadmin"
  click_button "Log in as admin"
  step("I should see \"Successfully logged in\"")
end
Given /^I am logged out as an admin$/ do
  visit admin_logout_path
  assert !AdminSession.find
end
When /^I make an admin post$/ do
  visit new_admin_post_path
  fill_in("admin_post_title", :with => "Default Admin Post")
  fill_in("content", :with => "Content of the admin post.")
  click_button("Post")
end
Given /^I am logged in as "([^\"]*)" with password "([^\"]*)"$/ do |login, password|
  step("I am logged out")
  user = User.find_by_login(login)
  if user.blank?
    user = FactoryGirl.create(:user, {:login => login, :password => password})
    user.activate
  else
    user.password = password
    user.password_confirmation = password
    user.save
  end
  visit login_path
  fill_in "User name", :with => login
  fill_in "Password", :with => password
  check "Remember Me"
  click_button "Log In"
  assert UserSession.find
end
Given /^I am logged in as "([^\"]*)"$/ do |login|
  step(%{I am logged in as "#{login}" with password "#{DEFAULT_PASSWORD}"})
end
Given /^I am logged out$/ do
  visit logout_path
  assert !UserSession.find
  visit admin_logout_path
  assert !AdminSession.find
end
When /^I post the work "([^\"]*)" with fandom "([^\"]*)" with freeform "([^\"]*)" with category "([^\"]*)"$/ do |title, fandom, freeform, category|
  work = Work.find_by_title(title)
  if work.blank?
    step %{the draft "#{title}" with fandom "#{fandom}" with freeform "#{freeform}" with category "#{category}"}
    work = Work.find_by_title(title)
  end
  visit preview_work_url(work)
  click_button("Post")
  step "I should see \"Work was successfully posted.\""
  Work.tire.index.refresh
end
When /^I set up (?:a|the) collection "([^"]*)"(?: with name "([^"]*)")?$/ do |title, name|
  visit new_collection_url
  fill_in("collection_name", :with => name.blank? ? title.gsub(/[^\w]/, '_') : name)
  fill_in("collection_title", :with => title)
end
When /^(?:|I )check "([^"]*)"(?: within "([^"]*)")?$/ do |field, selector|
  with_scope(selector) do
    check(field)
  end
end
When /^I submit$/ do
  step %{I submit with the 1st button}
end
Then /^(?:|I )should see "([^"]*)"(?: within "([^"]*)")?$/ do |text, selector|
  with_scope(selector) do
    page.should have_content(text)
  end
end
When /^the draft "([^\"]*)"(?: with fandom "([^\"]*)")?(?: with freeform "([^\"]*)")?(?: with category "([^\"]*)")?$/ do |title, fandom, freeform, category|
  step "basic tags"
  visit new_work_url
  step %{I fill in the basic work information for "#{title}"}
  check(category.nil? ? DEFAULT_CATEGORY : category)
  fill_in("Fandoms", :with => fandom.nil? ? DEFAULT_FANDOM : fandom)
  fill_in("Additional Tags", :with => freeform.nil? ? DEFAULT_FREEFORM : freeform)
  click_button("Preview")
end
When /^I submit with the (\d+)(?:st|nd|rd|th) button$/ do |index|
  page.all("p.submit input[type='submit']")[(index.to_i-1)].click
end
When /^I fill in the basic work information for "([^\"]*)"$/ do |title|
  step %{I fill in basic work tags}
  check(DEFAULT_WARNING)
  fill_in("Work Title", :with => title)
  fill_in("content", :with => DEFAULT_CONTENT)
end
When /^I fill in basic work tags$/ do
  select(DEFAULT_RATING, :from => "Rating")
  fill_in("Fandoms", :with => DEFAULT_FANDOM)
  fill_in("Additional Tags", :with => DEFAULT_FREEFORM)
end
