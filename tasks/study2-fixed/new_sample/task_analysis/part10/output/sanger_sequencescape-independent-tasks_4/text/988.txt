Feature: Printing new plate barcodes
Background:
Given I am logged in as "user"
And the plate barcode webservice returns "1234569"
And the "96 Well Plate" barcode printer "xyz" exists
And I freeze time at "Mon Jul 12 10:23:58 UTC 2010"
Given user "jack" exists with barcode "ID100I"
Scenario Outline: Creating plates
Given I am on the new plate page
And the plate barcode webservice returns "1234570"
When I select "<plate_purpose>" from "Plate purpose"
And I fill in "User barcode" with "2470000100730"
And I select "xyz" from "Barcode printer"
And Pmb has the required label templates
And Pmb is up and running
And I press "Submit"
Then I should see "Created plates and printed barcodes"
And I should be on the new plate page
Examples:
| plate_purpose       |
| Pico Standard       |
| Pulldown            |
| Pico Assay Plates   |
| Dilution Plates     |
| Gel Dilution Plates |
| Stock Plate         |
Scenario Outline: Create plates only from the proper parent plate or from scratch
Given a plate with purpose "<parent_plate_purpose>" and barcode "1221234567841" exists
And the plate barcode webservice returns "1234570"
And a plate with purpose "Cherrypicked" and barcode "1220001454858" exists
And I am on the new plate page
Then I should see "Create Plates"
When I select "<plate_purpose>" from "Plate purpose"
And I fill in "User barcode" with "2470000100730"
And I select "xyz" from "Barcode printer"
And Pmb has the required label templates
And Pmb is up and running
And I press "Submit"
Then I should see "Created plates and printed barcodes"
And I should be on the new plate page
When I select "<plate_purpose>" from "Plate purpose"
And I fill in the field labeled "Source plates" with "1220001454858"
And I fill in "User barcode" with "2470000100730"
And I select "xyz" from "Barcode printer"
And I press "Submit"
Then I should see "Scanned plate 1220001454858 has a purpose Cherrypicked not valid"
And I should be on the new plate page
Examples:
| plate_purpose         | parent_plate_purpose |
| Working dilution      | Stock Plate          |
| Pico dilution         | Working dilution     |
| Pico Assay Plates     | Pico dilution        |
