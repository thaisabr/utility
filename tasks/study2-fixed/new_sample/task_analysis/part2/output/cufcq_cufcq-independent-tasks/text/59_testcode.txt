Given /^(?:|I )am on the (.+)$/ do |page_name|
  visit( path_to(page_name))
end
  def path_to(page_name)
    case page_name

    when /^CSCI Department Page$/ then '/departments'
    when /^Instructors page$/ then '/instructors'
    when /^Courses page$/ then '/courses'
    when /^an FCQ$/ then '/fcqs'

    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /^the (.*) page$/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue NoMethodError, ArgumentError
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
Then(/^I should see undergraduate FCQs$/) do
  pending # express the regexp above with the code you wish you had
end
Then(/^I should not see graduate FCQs$/) do
  pending # express the regexp above with the code you wish you had
end
When(/^I filter to Undergraduate only$/) do
  pending # express the regexp above with the code you wish you had
end
Then /^(?:|I )will see the graph show (.+)$/ do |graph_data|
  assert page.has_xpath?('//*', :text => graph_data)
end
