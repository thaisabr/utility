When /^(.*) within (.*[^:])$/ do |step_text, parent|
  with_scope(parent) { step step_text }
end
When /^(?:|I )go to (.+)$/ do |page_name|
  verified_visit path_to(page_name)
end
When /^(?:|I )uncheck "([^"]*)"$/ do |field|
  uncheck(field)
end
When /^I reload the page$/ do
  page.evaluate_script 'window.location.reload()'
end
  def with_scope(locator)
    locator ? within(*selector_for(locator)) { yield } : yield
  end
def verified_visit(path)
  visit path
  verify_current_path(path)
end
  def path_to(page_name)
    case page_name
    when /the root path/
      "/"
    when /the home\s?page/
      '/home'
    when /my classes page/
      '/my_classes'
    when /getting started page/
      '/getting_started'
    when /my home\s?page/
      '/home'
    when /my preferences/
      "/users/#{User.find_by_login(@cuke_current_username).id}/preferences"
    when /user list/
      '/users'
    when /the districts page/
      portal_districts_path
    when /the pick signup page/
      '/pick_signup'
    when /the student signup page/
      '/portal/students/signup'
    when /to the link tool/
      '/linktool'
    when /the current settings edit page/
      "/admin/settings/#{Admin::Settings.default_settings.id}/edit"
    when /the current settings show page/
      "/admin/settings/#{Admin::Settings.default_settings.id}/show"
    when /the researcher reports page and click usage report/
      WebMock.stub_request(:post, /report_learners\/_search$/).
        to_return(:status => 200, :body => { "hits" => { "hits" => [] } }.to_json,
          :headers => {'Content-Type'=>'application/json'})
      "/report/learner?commit=Usage+Report"
    when /the researcher reports page and click details report/
      WebMock.stub_request(:post, /report_learners\/_search$/).
        to_return(:status => 200, :body => { "hits" => { "hits" => [] } }.to_json,
          :headers => {'Content-Type'=>'application/json'})
      "/report/learner?commit=Details+Report"
    when /the class page for "(.*)"/
      "/portal/classes/#{Portal::Clazz.find_by_name($1).id}"
    when /the class edit page for "([^"]*)"/
        clazz = Portal::Clazz.find_by_name($1)
        edit_portal_clazz_path(clazz)
    when /the clazz create page/
      new_portal_clazz_path
    when /the user preferences page for the user "(.*)"/
      user = User.find_by_login $1
      preferences_user_path user
    when /the switch page/
      switch_user_path User.find_by_login(@cuke_current_username)
    when /the requirements page/
      "/requirements/"
    when /the about page/
      "/about"
    when /the admin create notice page/
      "/admin/site_notices/new"
    when /the site notices index page/
      "/admin/site_notices"
    when /the password reset page/
      "/change_password/0"
    when /the edit security questions page for the user "(.*)"/
      user = User.find_by_login $1
      edit_user_security_questions_path user
    # accept paths too:
    when /the route (.+)/
      $1
    when /\/[\S+\/]+/
      page_name
    when /the class edit page for "(.+)"/
      portal_clazz = Portal::Clazz.find_by_name $1
      "/portal/classes/#{portal_clazz.id}/edit"
    when /"Student Roster" page for "(.+)"/
      portal_clazz = Portal::Clazz.find_by_name $1
      "/portal/classes/#{portal_clazz.id}/roster"
    when /the full status page for "(.+)"/
      portal_clazz = Portal::Clazz.find_by_name $1
      "/portal/classes/#{portal_clazz.id}/fullstatus"
    when /Manage Class Page/
      "/portal/classes/manage"
    when /Recent Activity Page/
      "/recent_activity"
    when /the search instructional materials page/
      "/search"
    when /browse materials page for "(.+)"/
      eresource = ExternalActivity.find_by_name $1
      "/browse/eresources/#{eresource.id}"
    when /material preview page for "(.+)"/
      eresource = ExternalActivity.find_by_name $1
      slug = eresource.name.parameterize
      "/resources/#{eresource.id}/#{slug}"
    when /Instructional Materials page for "(.+)"/
      portal_clazz = Portal::Clazz.find_by_name $1
      "/portal/classes/#{portal_clazz.id}/materials"
    when /report of offering "(.+)"/
      eresource = ExternalActivity.find_by_name($1)
      offering = Portal::Offering.find_by_runnable_id eresource.id
      "/portal/offerings/#{offering.id}/report"
    when /the Help Page/
      "/help"
    when /the projects index page/
      "/admin/projects"
    when /the materials collection index page/
      "/materials_collections"
    when /the show page for materials collection "([^"]+)"/
      collection = MaterialsCollection.find_by_name($1).id
      "/materials_collections/#{collection}"
    when /the interactives index page/
      "/interactives"
    when /the show page for interactive "([^"]+)"/
      interactive = Interactive.find_by_name($1).id
      "/interactives/#{interactive}"
    when /the edit page of interactive "([^"]+)"/
      interactive = Interactive.find_by_name($1).id
      "/interactives/#{interactive}/edit"
    when /the signin page/
      "/auth/login"
    when /the new material page/
      "/eresources/new"
    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /^the (.*) page$/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue NoMethodError, ArgumentError
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
  def selector_for(locator)
    case locator

    when "the page"
      "html > body"
    when "left panel for class navigation"
      "div#clazzes_nav"
    when "the popup"
      "div.ui-window div.content"
    when "the select for Instructional Materials"
      "material_select"
    when "activity button list of Instructional Material page"
      "table.materials"
    when "the first recent activity on the recent activity page"
      [:xpath, "//div[@class=\"recent_activity_container\"]/div[1]"]
    when "the activity table"
      [:xpath, "//div[@class = 'progress_div webkit_scrollbars']/table"]
    when "suggestion box"
      "div#suggestions"
    when "result box"
      "div.results_container"
    when "the student list on the student roster page"
      "div#students_listing"
    when "the lightbox in focus"
      "div.ui-window.lightbox.lightbox_focused"
    when "the navigation menu"
      "div#clazzes_nav"
    when "header login box"
      "div.header-login-box"
    when "content box in change password page"
      "div#content"
    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #  when /^the (notice|error|info) flash$/
    #    ".flash.#{$1}"

    # You can also return an array to use a different selector
    # type, like:
    #
    #  when /the header/
    #    [:xpath, "//header"]

    # This allows you to provide a quoted selector as the scope
    # for "within" steps as was previously the default for the
    # web steps:
    when /^"(.+)"$/
      $1
    when /^(#.+)$/
      $1

    else
      raise "Can't find mapping from \"#{locator}\" to a selector.\n" +
        "Now, go and add a mapping in #{__FILE__}"
    end
  end
And /^the Manage class list state starts saving$/ do
  page.execute_script("SaveManageClassListState();")
end
And /^the modal for saving manage classes dissappears$/ do
  page.has_no_css?('div.invisible_modal.show')
end
Given /^the database has been seeded$/ do
  ## see lib/mock_data/create_fake_data.rb and lib/mock_data/default_data.yml for more information...
end
Given /^The default settings and jnlp resources exist using factories$/ do
  generate_default_settings_and_jnlps_with_factories
end
Given /^I am logged in with the username ([^"]*)$/ do |username|
  login_as(username)
end
def login_as(username)
  visit "/login/#{username}"
  expect(page).to have_content(username)
  @cuke_current_username = username
end
