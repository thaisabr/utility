Given /^I am a mentor teacher$/ do
  password = "test"
  teacher = FactoryGirl.create(:mentor_teacher)
  user = FactoryGirl.create(:user,  
                            :password => password,
                            :owner => teacher
                            )             
  login(user.email, password)
end
Given /we are currently in a semester/ do
  Semester.create!(
    :name => Semester::FALL, 
    :year => "2012", 
    :start_date => Date.today - 10, 
    :end_date => Date.today + 10,
    :registration_deadline => Deadline.new(
      :title => "Registraiton Deadline",
      :summary => "You must have you preferences selected by this deadline",
      :due_date => Date.new(2012, 1, 16),
    ),
    :status => Semester::PUBLIC,
  )
end
Then /^(?:|I )should (not )?be located at "([^"]*)"$/ do |should_not_be, page_name|
  current_path = URI.parse(current_url).path
  if current_path.respond_to? :should
    if should_not_be 
      current_path.should_not == page_name 
    else
      current_path.should == page_name
    end
  else
    if should_not_be
       assert_not_equal(page_name, current_path) 
    else
      assert_equal(page_name, current_path)
    end
   
  end
end
def login(email, password)
  visit '/login'
  fill_in "Email", :with => email
  fill_in "Password", :with => password
  click_button "Login"
  page.should have_content('Login successful')
end 
When /^(?:|I )go to (.+)$/ do |page_path|
  visit page_path
end
Then /^(?:|I )should see "([^"]*)"$/ do |text|
  if page.respond_to? :should
    page.should have_content(text)
  else
    assert page.has_content?(text)
  end
end
